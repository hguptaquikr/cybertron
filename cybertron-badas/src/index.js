import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, hashHistory } from 'react-router';
import { ReduxAsyncConnect } from 'redux-connect';
import injectTapEventPlugin from 'react-tap-event-plugin';
import createStore from './store/createStore';
import './vendor/base.css';
import routes from './routes';

injectTapEventPlugin();

const store = createStore();

ReactDOM.render(
  <Provider store={store}>
    <Router
      render={(props) => <ReduxAsyncConnect {...props} />}
      history={hashHistory}
      routes={routes}
      onUpdate={() => window.scrollTo(0, 0)}
    />
  </Provider>,
  document.getElementById('root'),
);
