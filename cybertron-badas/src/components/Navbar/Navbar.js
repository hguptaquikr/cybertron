import React from 'react';
import { AppBar } from 'material-ui';
import LoadingBar from './LoadingBar';
import badass from './badass.gif';

const Navbar = ({ router }) => (
  <div>
    <LoadingBar />
    <AppBar
      zDepth={0}
      title="BADAS"
      titleStyle={{ cursor: 'pointer' }}
      onTitleTouchTap={() => router.push('/')}
    />
    <img src={badass} alt="" className="logo" />
  </div>
);

Navbar.propTypes = {
  router: React.PropTypes.object.isRequired,
};

export default Navbar;
