import React from 'react';
import { default as ReduxLoadingBar } from 'react-redux-loading-bar';

const LoadingBar = () => (
  <div
    style={{
      position: 'fixed',
      width: '1024px',
      zIndex: 11112,
      top: '0',
    }}
  >
    <ReduxLoadingBar style={{ backgroundColor: '#ff4081' }} />
  </div>
);

export default LoadingBar;
