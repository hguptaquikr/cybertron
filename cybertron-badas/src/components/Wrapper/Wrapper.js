import React from 'react';
import { withRouter } from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Paper } from 'material-ui';
import Navbar from '../../components/Navbar/Navbar';
import './wrapper.css';

class Wrapper extends React.Component {
  getChildContext() {
    return {
      location: this.props.location,
    };
  }

  render() {
    const { children, router } = this.props;
    return (
      <MuiThemeProvider>
        <Paper id="wrapper" className="wrapper">
          <Navbar router={router} />
          {children}
        </Paper>
      </MuiThemeProvider>
    );
  }
}

Wrapper.childContextTypes = {
  location: React.PropTypes.object.isRequired,
};

Wrapper.propTypes = {
  children: React.PropTypes.object.isRequired,
  location: React.PropTypes.object.isRequired,
  router: React.PropTypes.object.isRequired,
};

export default withRouter(Wrapper);
