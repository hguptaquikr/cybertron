import React from 'react';
import './notFound.css';

const NotFound = () => (
  <div className="page text-center">
    <h1 style={{ marginTop: '100px' }}>
      <div style={{ fontSize: '200px' }}>404</div>
      <div style={{ fontSize: '40px' }}>Page Not Found</div>
    </h1>
  </div>
);

export default NotFound;
