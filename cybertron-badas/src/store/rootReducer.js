import { combineReducers } from 'redux';
import { reducer as reduxAsyncConnect } from 'redux-connect';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';
import authReducer from '../auth/authDuck';
import templateReducer from '../template/templateDuck';

export default combineReducers({
  reduxAsyncConnect,
  loadingBar,
  auth: authReducer,
  template: templateReducer,
});
