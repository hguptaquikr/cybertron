import { createStore, compose, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { loadingBarMiddleware } from 'react-redux-loading-bar';
import api from '../utils/api';
import asyncMiddleware from './asyncMiddleware';
import rootReducer from './rootReducer';

const middlewares = [
  thunkMiddleware.withExtraArgument({ api }),
  asyncMiddleware,
  loadingBarMiddleware({ promiseTypeSuffixes: ['REQUEST', 'SUCCESS', 'FAILURE'] }),
].filter(Boolean);

const storeEnhancers = [
  applyMiddleware(...middlewares),
  __LOCAL__ && window.devToolsExtension && window.devToolsExtension(),
].filter(Boolean);

export default (initialState) => createStore(
  rootReducer,
  initialState,
  compose(...storeEnhancers),
);
