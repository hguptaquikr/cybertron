export default () => (next) => (action) => {
  const { types, promise, payload } = action;

  if (!promise) {
    return next(action);
  }

  const [REQUEST, SUCCESS, FAILURE] = types;

  next({
    type: REQUEST,
    payload,
  });

  return promise()
    .then((response) => {
      next({
        type: SUCCESS,
        payload: {
          ...payload,
          response,
        },
      });
      return response;
    })
    .catch((response) => {
      next({
        type: FAILURE,
        payload: {
          ...payload,
          error: response.error,
        },
      });
      window.alert('There was an error!'); // eslint-disable-line
      return Promise.reject(response.error);
    });
};
