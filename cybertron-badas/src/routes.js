import React from 'react';
import { Route, IndexRoute } from 'react-router';
import authService from './auth/authService';
import Wrapper from './components/Wrapper/Wrapper';
import NotFound from './components/NotFound/NotFound';
import TemplateList from './template/TemplateList/TemplateList';
import TemplateForm from './template/TemplateForm/TemplateForm';
import TemplatePreview from './template/TemplatePreview/TemplatePreview';
import TemplateFill from './template/TemplateFill/TemplateFill';
import Login from './auth/Login/Login';

const requireAuth = (nextState, replace) => {
  if (!authService.isLoggedIn()) {
    replace({ pathname: '/login/' });
  }
};

export default (
  <Route path="/" component={Wrapper}>

    <IndexRoute component={TemplateList} onEnter={requireAuth} />

    <Route path="/create" component={TemplateForm} onEnter={requireAuth} />

    <Route path="/:id/edit" component={TemplateForm} onEnter={requireAuth} />

    <Route path="/:id/fill" component={TemplateFill} onEnter={requireAuth} />

    <Route path="/preview" component={TemplatePreview} onEnter={requireAuth} />

    <Route path="/login/" component={Login} />

    <Route path="*" component={NotFound} />

  </Route>
);
