const GET_TEMPLATES_REQUEST = 'GET_TEMPLATES_REQUEST';
const GET_TEMPLATES_SUCCESS = 'GET_TEMPLATES_SUCCESS';
const GET_TEMPLATES_FAILURE = 'GET_TEMPLATES_FAILURE';
const POST_TEMPLATE_REQUEST = 'POST_TEMPLATE_REQUEST';
const POST_TEMPLATE_SUCCESS = 'POST_TEMPLATE_SUCCESS';
const POST_TEMPLATE_FAILURE = 'POST_TEMPLATE_FAILURE';
const PUT_TEMPLATE_REQUEST = 'PUT_TEMPLATE_REQUEST';
const PUT_TEMPLATE_SUCCESS = 'PUT_TEMPLATE_SUCCESS';
const PUT_TEMPLATE_FAILURE = 'PUT_TEMPLATE_FAILURE';
const DELETE_TEMPLATE_REQUEST = 'DELETE_TEMPLATE_REQUEST';
const DELETE_TEMPLATE_SUCCESS = 'DELETE_TEMPLATE_SUCCESS';
const DELETE_TEMPLATE_FAILURE = 'DELETE_TEMPLATE_FAILURE';
const SELECT_TEMPLATE = 'SELECT_TEMPLATE';

const initialState = {
  list: [],
  selected: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_TEMPLATES_REQUEST:
      return {
        ...state,
        list: [],
      };
    case GET_TEMPLATES_SUCCESS:
      return {
        ...state,
        list: action.payload.response.map((template) => ({
          ...template,
          fields: JSON.parse(template.fields),
        })),
      };
    case GET_TEMPLATES_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      };
    case SELECT_TEMPLATE:
      return {
        ...state,
        selected: action.payload.selected,
      };
    default:
      return state;
  }
};

export const getList = () => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_TEMPLATES_REQUEST,
      GET_TEMPLATES_SUCCESS,
      GET_TEMPLATES_FAILURE,
    ],
    promise: () => api.get('/templates/'),
  });

export const createTemplate = (template) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      POST_TEMPLATE_REQUEST,
      POST_TEMPLATE_SUCCESS,
      POST_TEMPLATE_FAILURE,
    ],
    promise: () => api.post('/templates/', template),
  });

export const updateTemplate = (template) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      PUT_TEMPLATE_REQUEST,
      PUT_TEMPLATE_SUCCESS,
      PUT_TEMPLATE_FAILURE,
    ],
    promise: () => api.put(`/templates/${template.id}/`, template),
  });

export const deleteTemplate = (id) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      DELETE_TEMPLATE_REQUEST,
      DELETE_TEMPLATE_SUCCESS,
      DELETE_TEMPLATE_FAILURE,
    ],
    promise: () => api.del(`/templates/${id}/`),
  });

export const selectTemplate = (id) => (dispatch, getState) => {
  const { template } = getState();
  dispatch({
    type: SELECT_TEMPLATE,
    payload: {
      selected: template.list.find((t) => +t.id === +id) || {},
    },
  });
};

export const updateTemplateFields = (model) => (dispatch, getState) => {
  const { templates: { selected: selectedTemplate } } = getState();
  selectedTemplate.fields = selectedTemplate.fields.map((field) => ({
    ...field,
    value: model[field.name],
  }));
  dispatch({
    type: SELECT_TEMPLATE,
    payload: { selected: selectedTemplate },
  });
};
