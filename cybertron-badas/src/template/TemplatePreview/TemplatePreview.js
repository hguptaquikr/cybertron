import React from 'react';
import { connect } from 'react-redux';
import { RaisedButton } from 'material-ui';
import TinyMCE from 'react-tinymce';
import moment from 'moment';
import './templatePreview.css';

const formatedFieldValue = (field) => {
  switch (field.type) {
    case 'price':
      return (+field.value).toLocaleString('en-IN');
    case 'date':
      return moment(field.value).format('dddd, MMMM Do, YYYY');
    default:
      return field.value;
  }
};

class TemplatePreview extends React.Component {
  state = {
    content: '',
  }

  componentWillMount() {
    const { template: { selected: template } } = this.props;
    const fields = template.fields || [];
    let { content } = template;

    fields.forEach((field) => {
      const re = new RegExp(`{{\\s*${field.name}\\s*\\|.*?}}`, 'g');
      content = content.replace(re, formatedFieldValue(field));
    });

    this.setState({ content });
  }

  printTemplate = () => {
    window.tinyMCE.activeEditor.execCommand('print');
  }

  render() {
    const { content } = this.state;

    return (
      <div className="page">
        <RaisedButton
          className="page__floating-button"
          label="PRINT"
          onTouchTap={this.printTemplate}
          secondary
        />
        <TinyMCE
          content={content}
          config={{
            plugins: [
              'advlist autolink lists link image charmap print preview hr anchor pagebreak',
              'searchreplace wordcount visualblocks visualchars code fullscreen',
              'insertdatetime media nonbreaking save table contextmenu directionality',
              'emoticons template paste textcolor colorpicker textpattern imagetools',
            ],
            menubar: false,
            toolbar: false,
            height: 600,
            readonly: true,
            pagebreak_separator: '<div style="page-break-before: always;"></div>',
          }}
        />
      </div>
    );
  }
}

TemplatePreview.propTypes = {
  template: React.PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  template: state.template,
});

export default connect(
  mapStateToProps,
)(TemplatePreview);
