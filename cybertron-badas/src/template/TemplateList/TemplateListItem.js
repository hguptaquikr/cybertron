import React from 'react';
import { ListItem, IconButton } from 'material-ui';
import ModeEdit from 'material-ui/svg-icons/editor/mode-edit';
import ActionDelete from 'material-ui/svg-icons/action/delete';

const TemplateListItem = ({ template, showTemplate, editTemplate, deleteTemplate, isAdmin }) => (
  <ListItem
    onTouchTap={showTemplate}
    primaryText={template.title}
    secondaryText={template.description}
    rightIconButton={
      isAdmin ? (
        <div>
          <IconButton onTouchTap={editTemplate}>
            <ModeEdit />
          </IconButton>
          <IconButton onTouchTap={deleteTemplate}>
            <ActionDelete />
          </IconButton>
        </div>
      ) : null
    }
  />
);

TemplateListItem.propTypes = {
  template: React.PropTypes.object.isRequired,
  showTemplate: React.PropTypes.func.isRequired,
  editTemplate: React.PropTypes.func.isRequired,
  deleteTemplate: React.PropTypes.func.isRequired,
  isAdmin: React.PropTypes.bool.isRequired,
};

export default TemplateListItem;
