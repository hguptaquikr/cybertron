import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { withRouter } from 'react-router';
import { List, RaisedButton } from 'material-ui';
import * as templateActionCreators from '../../template/templateDuck';
import TemplateListItem from './TemplateListItem';

class TemplateList extends React.Component {
  componentDidMount() {
    const { templateActions } = this.props;
    templateActions.getList();
  }

  createTemplate = (e) => {
    const { templateActions, router } = this.props;
    e.preventDefault();
    templateActions.selectTemplate();
    router.push('/create/');
  }

  showTemplate = (id) => (e) => {
    const { router } = this.props;
    e.preventDefault();
    router.push(`/${id}/fill/`);
  }

  editTemplate = (id) => (e) => {
    const { templateActions, router } = this.props;
    e.preventDefault();
    templateActions.selectTemplate(id);
    router.push(`/${id}/edit/`);
  }

  deleteTemplate = (id) => (e) => {
    const { templateActions } = this.props;
    e.preventDefault();
    // eslint-disable-next-line
    if (window.confirm('Are you sure you want to delete?')) {
      templateActions.deleteTemplate(id)
        .then(() => templateActions.getList());
    }
  }

  render() {
    const { template, auth: { user } } = this.props;
    return (
      <div>
        {
          user.can_edit ? (
            <RaisedButton
              className="page__floating-button"
              label="CREATE"
              onTouchTap={this.createTemplate}
              secondary
            />
          ) : null
        }
        <List style={{ paddingTop: 0, paddingBottom: 0 }}>
          {
            template.list.length ? (
              template.list.map((t) => (
                <TemplateListItem
                  key={t.id}
                  template={t}
                  showTemplate={this.showTemplate(t.id)}
                  editTemplate={this.editTemplate(t.id)}
                  deleteTemplate={this.deleteTemplate(t.id)}
                  isAdmin={user.can_edit}
                />
              ))
            ) : null
          }
        </List>
      </div>
    );
  }
}

TemplateList.propTypes = {
  template: React.PropTypes.object.isRequired,
  auth: React.PropTypes.object.isRequired,
  templateActions: React.PropTypes.object.isRequired,
  router: React.PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  template: state.template,
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  templateActions: bindActionCreators(templateActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
)(TemplateList);
