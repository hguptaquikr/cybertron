import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { withRouter } from 'react-router';
import { Form } from 'formsy-react';
import { RaisedButton } from 'material-ui';
import * as templateActionCreators from '../../template/templateDuck';
import TemplateFormField from './TemplateFormField';

class TemplateFill extends React.Component {
  componentDidMount() {
    const { params, templateActions } = this.props;
    templateActions.selectTemplate(params.id);
  }

  showPreview = (model) => {
    const { templateActions, router } = this.props;
    templateActions.updateTemplateFields(model);
    router.push('/preview/');
  }

  render() {
    const { template: { selected: template } } = this.props;
    return (
      <div className="page">
        <h4 className="page__title">{template.title}</h4>
        <p>{template.description}</p>
        <Form onValidSubmit={this.showPreview}>
          {
            template.fields ? (
              template.fields.map((field) => (
                <TemplateFormField key={field.name} field={field} />
              ))
            ) : null
          }
          <RaisedButton
            label="PREVIEW"
            type="submit"
            secondary
            fullWidth
          />
        </Form>
      </div>
    );
  }
}

TemplateFill.propTypes = {
  template: React.PropTypes.object.isRequired,
  templateActions: React.PropTypes.object.isRequired,
  params: React.PropTypes.object.isRequired,
  router: React.PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  template: state.template,
});

const mapDispatchToProps = (dispatch) => ({
  templateActions: bindActionCreators(templateActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
)(TemplateFill);
