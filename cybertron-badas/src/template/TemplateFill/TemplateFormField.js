import React from 'react';
import { FormsyText, FormsyDate } from 'formsy-material-ui';
import moment from 'moment';

const formatDate = (date) =>
  moment(date).format('dddd, MMMM Do, YYYY');

const TemplateFormField = ({ field }) => {
  if (!field.type) return null;
  switch (field.type) {
    case 'number':
      return (
        <FormsyText
          name={field.name}
          hintText={field.description}
          floatingLabelText={field.name}
          validations="isNumeric"
          validationError="Enter a valid number"
          fullWidth
          required
        />
      );
    case 'price':
      return (
        <FormsyText
          name={field.name}
          hintText={field.description}
          floatingLabelText={field.name}
          validations="isNumeric"
          validationError="Enter a valid number (without commas)"
          fullWidth
          required
        />
      );
    case 'date':
      return (
        <FormsyDate
          name={field.name}
          hintText={field.description}
          floatingLabelText={field.name}
          formatDate={formatDate}
          fullWidth
          required
        />
      );
    default:
      return (
        <FormsyText
          name={field.name}
          hintText={field.description}
          floatingLabelText={field.name}
          fullWidth
          required
        />
      );
  }
};

TemplateFormField.propTypes = {
  field: React.PropTypes.object.isRequired,
};

export default TemplateFormField;
