import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { withRouter } from 'react-router';
import TinyMCE from 'react-tinymce';
import { Form } from 'formsy-react';
import { FormsyText } from 'formsy-material-ui';
import { RaisedButton } from 'material-ui';
import * as templateActionCreators from '../../template/templateDuck';

class TemplateForm extends React.Component {
  state = {
    templateContent: this.props.template.selected.content || '',
  }

  onSubmit = (model) => {
    const {
      template: { selected: template },
      templateActions,
      router,
    } = this.props;
    const templateData = {
      ...template,
      ...model,
      content: this.state.templateContent,
    };
    if (templateData.id) {
      templateActions.updateTemplate(templateData)
        .then(() => router.push('/'));
    } else {
      templateActions.createTemplate(templateData)
        .then(() => router.push('/'));
    }
  }

  handleEditorChange = (e) => {
    e.preventDefault();
    this.setState({ templateContent: e.target.getContent() });
  }

  render() {
    const { template: { selected: template } } = this.props;

    return (
      <div className="page">
        <Form onValidSubmit={this.onSubmit}>
          <FormsyText
            name="title"
            required
            hintText="Enter Agreement Title"
            floatingLabelText="Title"
            fullWidth
            value={template.title}
          />
          <FormsyText
            name="description"
            required
            hintText="Enter Agreement Description"
            floatingLabelText="Description"
            fullWidth
            value={template.description}
          />
          <div className="page__content">
            <TinyMCE
              content={template.content}
              config={{
                plugins: [
                  'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                  'searchreplace wordcount visualblocks visualchars code fullscreen',
                  'insertdatetime media nonbreaking save table contextmenu directionality',
                  'emoticons template paste textcolor colorpicker textpattern imagetools',
                ],
                toolbar1: 'print preview | insertfile undo redo | styleselect | bold italic |' +
                ' alignleft aligncenter alignright alignjustify | bullist numlist outdent ' +
                'indent | link image media | forecolor backcolor',
                height: 350,
                pagebreak_separator: '<div style="page-break-before: always;"></div>',
              }}
              onChange={this.handleEditorChange}
            />
          </div>
          <RaisedButton
            label="SUBMIT"
            type="submit"
            primary
            fullWidth
          />
        </Form>
      </div>
    );
  }
}

TemplateForm.propTypes = {
  templateActions: React.PropTypes.object.isRequired,
  template: React.PropTypes.object.isRequired,
  router: React.PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  template: state.template,
});

const mapDispatchToProps = (dispatch) => ({
  templateActions: bindActionCreators(templateActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
)(TemplateForm);
