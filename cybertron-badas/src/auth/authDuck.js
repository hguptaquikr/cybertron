import authService from './authService';

const POST_USER_LOGIN_REQUEST = 'POST_USER_LOGIN_REQUEST';
const POST_USER_LOGIN_SUCCESS = 'POST_USER_LOGIN_SUCCESS';
const POST_USER_LOGIN_FAILURE = 'POST_USER_LOGIN_FAILURE';

const initialState = {
  user: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case POST_USER_LOGIN_REQUEST:
      return state;
    case POST_USER_LOGIN_SUCCESS:
      return {
        ...state,
        user: action.payload.response.user_details,
      };
    case POST_USER_LOGIN_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      };
    default:
      return state;
  }
};

export const login = (credentials) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      POST_USER_LOGIN_REQUEST,
      POST_USER_LOGIN_SUCCESS,
      POST_USER_LOGIN_FAILURE,
    ],
    promise: () => api.post('/login/', credentials),
  }).then((response) => {
    authService.setUser(response.user_details);
    return response;
  });
