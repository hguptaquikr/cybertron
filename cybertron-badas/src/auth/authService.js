export default {
  user: null,

  isLoggedIn() {
    return !!this.getUser();
  },

  getUser() {
    return this.user;
  },

  setUser(user) {
    this.user = user;
  },

  unsetUser() {
    this.user = null;
  },
};
