import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { withRouter } from 'react-router';
import { Form } from 'formsy-react';
import { FormsyText } from 'formsy-material-ui';
import { RaisedButton } from 'material-ui';
import * as authActionCreators from '../../auth/authDuck';
import './login.css';

class Login extends React.Component {
  login = (credentials) => {
    const { authActions, router } = this.props;
    authActions.login(credentials).then(() => router.push('/'));
  }

  render() {
    return (
      <div className="page login">
        <h4 className="page__title text-center">
          Business and Development<br />Agreement System
        </h4>
        <Form className="text-center" onValidSubmit={this.login}>
          <FormsyText
            name="username"
            hintText="ramkumar"
            floatingLabelText="Username"
            fullWidth
            required
          /><br />
          <FormsyText
            name="password"
            floatingLabelText="Password"
            type="password"
            fullWidth
            required
          /><br />
          <RaisedButton
            label="LOGIN"
            type="submit"
            fullWidth
            secondary
          />
        </Form>
      </div>
    );
  }
}

Login.propTypes = {
  authActions: React.PropTypes.object.isRequired,
  router: React.PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  authActions: bindActionCreators(authActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
)(Login);
