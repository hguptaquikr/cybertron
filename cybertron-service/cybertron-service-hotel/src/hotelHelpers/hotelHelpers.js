export const makeHotel = (props) => {
  const hotel = {};

  hotel.id = props.id || '';
  hotel.bookingIds = props.bookingIds || [];
  hotel.roomIds = props.roomIds || [];
  hotel.roomTypeIds = props.roomTypeIds || [];
  hotel.name = props.name || '';
  hotel.images = props.images || [];
  hotel.area = props.area || {};

  return hotel;
};

export const makeHotelMap = (hotels) =>
  hotels.reduce((acc, hotel) => ({
    ...acc,
    [hotel.id]: makeHotel(hotel),
  }), {});
