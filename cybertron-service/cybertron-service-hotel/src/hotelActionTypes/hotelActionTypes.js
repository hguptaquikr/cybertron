export const SET_HOTELS = 'cybertron-service-hotel/SET_HOTELS';
export const UPDATE_HOTELS = 'cybertron-service-hotel/UPDATE_HOTELS';
export const DELETE_HOTELS = 'cybertron-service-hotel/DELETE_HOTELS';
export const RESET_STATE = 'cybertron-service-hotel/RESET_STATE';
