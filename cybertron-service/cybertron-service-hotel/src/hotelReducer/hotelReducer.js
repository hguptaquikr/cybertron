import omitKeys from 'cybertron-utils/omitKeys';
import * as hotelActionTypes from '../hotelActionTypes/hotelActionTypes';
import * as hotelHelpers from '../hotelHelpers';

export const initialState = {};

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case hotelActionTypes.SET_HOTELS: {
      const hotelMap = hotelHelpers.makeHotelMap(payload.hotels);

      return {
        ...state,
        ...hotelMap,
      };
    }

    case hotelActionTypes.UPDATE_HOTELS: {
      const hotelMap = payload.hotels
        .reduce((acc, hotel) => {
          if (!state[hotel.id]) {
            throw new Error('Invalid Hotel ID');
          }
          return {
            ...acc,
            [hotel.id]: hotelHelpers.makeHotelMap({
              ...state[hotel.id],
              ...hotel,
            }),
          };
        }, {});

      return {
        ...state,
        ...hotelMap,
      };
    }

    case hotelActionTypes.DELETE_HOTELS: {
      const hotelMap = omitKeys(state, payload.hotelIds);

      return hotelMap;
    }

    case hotelActionTypes.RESET_STATE: {
      return payload.resetState;
    }

    default:
      return state;
  }
};
