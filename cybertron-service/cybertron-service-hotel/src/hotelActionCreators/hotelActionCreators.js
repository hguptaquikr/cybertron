import * as hotelActionTypes from '../hotelActionTypes';
import * as hotelReducer from '../hotelReducer';

export const setHotels = (hotels) => ({
  type: hotelActionTypes.SET_HOTELS,
  payload: {
    hotels,
  },
});

export const updateHotels = (hotels) => ({
  type: hotelActionTypes.UPDATE_HOTELS,
  payload: {
    hotels,
  },
});

export const deleteHotels = (hotelIds) => ({
  type: hotelActionTypes.DELETE_HOTELS,
  payload: {
    hotelIds,
  },
});

export const resetState = (state = hotelReducer.initialState) => ({
  type: hotelActionTypes.RESET_STATE,
  payload: {
    resetState: state,
  },
});
