import * as roomStayActionTypes from '../roomStayActionTypes';
import * as roomStayReducer from '../roomStayReducer';

export const setRoomStays = (roomStays) => ({
  type: roomStayActionTypes.SET_ROOM_STAYS,
  payload: {
    roomStays,
  },
});

export const updateRoomStays = (roomStays) => ({
  type: roomStayActionTypes.UPDATE_ROOM_STAYS,
  payload: {
    roomStays,
  },
});

export const deleteRoomStays = (roomStayIds) => ({
  type: roomStayActionTypes.DELETE_ROOM_STAYS,
  payload: {
    roomStayIds,
  },
});

export const resetState = (state = roomStayReducer.initialState) => ({
  type: roomStayActionTypes.RESET_STATE,
  payload: {
    resetState: state,
  },
});
