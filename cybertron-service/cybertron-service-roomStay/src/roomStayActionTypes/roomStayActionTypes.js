export const SET_ROOM_STAYS = 'cybertron-service-roomStay/SET_ROOM_STAYS';
export const UPDATE_ROOM_STAYS = 'cybertron-service-roomStay/UPDATE_ROOM_STAYS';
export const DELETE_ROOM_STAYS = 'cybertron-service-roomStay/DELETE_ROOM_STAYS';
export const RESET_STATE = 'cybertron-service-roomStay/RESET_STATE';
