import * as roomStayConstants from '../roomStayConstants';

export const makeRoomStay = (props) => {
  const roomStay = {};

  roomStay.id = props.id || '';
  roomStay.type = roomStayConstants.type[props.type] || '';
  roomStay.roomNumbers = props.roomNumbers || [];
  roomStay.maxOccupancy = props.maxOccupancy || {};

  return roomStay;
};

export const makeRoomStayMap = (roomStays) =>
  roomStays.reduce((acc, roomStay) => ({
    ...acc,
    [roomStay.id]: makeRoomStay(roomStay),
  }), {});
