import omitKeys from 'cybertron-utils/omitKeys';
import * as roomStayActionTypes from '../roomStayActionTypes';
import * as roomStayHelpers from '../roomStayHelpers';

export const initialState = {};

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case roomStayActionTypes.SET_ROOM_STAYS: {
      const roomStayMap = roomStayHelpers.makeRoomStayMap(payload.roomStays);

      return {
        ...state,
        ...roomStayMap,
      };
    }

    case roomStayActionTypes.UPDATE_ROOM_STAYS: {
      const roomStayMap = payload.roomStays
        .reduce((acc, roomStay) => {
          if (!state[roomStay.id]) {
            throw new Error('Invalid RoomStay ID');
          }
          return {
            ...acc,
            [roomStay.id]: roomStayHelpers.makeRoomStayMap({
              ...state[roomStay.id],
              ...roomStay,
            }),
          };
        }, {});

      return {
        ...state,
        ...roomStayMap,
      };
    }

    case roomStayActionTypes.DELETE_ROOM_STAYS: {
      const roomStayMap = omitKeys(state, payload.roomStayIds);

      return roomStayMap;
    }

    case roomStayActionTypes.RESET_STATE: {
      return payload.initialState;
    }

    default:
      return state;
  }
};
