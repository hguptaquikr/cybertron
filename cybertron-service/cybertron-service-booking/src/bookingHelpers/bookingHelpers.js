import * as bookingConstants from '../bookingConstants';

export const makeBooking = (props) => {
  const booking = {};

  booking.id = props.id || '';
  booking.checkIn = props.checkIn || '';
  booking.checkOut = props.checkOut || '';
  booking.status = bookingConstants.status[props.status] || '';
  booking.source = props.source || {};
  booking.creator = props.creator || {};
  booking.specialRequest = props.specialRequest || '';
  booking.gstin = props.gstin || '';
  booking.roomStays = props.roomStays || {};
  booking.guestIds = props.guestIds || [];
  booking.price = props.price || {};
  booking.paymentCategory = props.paymentCategory || '';

  return booking;
};

export const makeBookingMap = (bookings) =>
  bookings.reduce((acc, booking) => ({
    ...acc,
    [booking.id]: makeBooking(booking),
  }), {});
