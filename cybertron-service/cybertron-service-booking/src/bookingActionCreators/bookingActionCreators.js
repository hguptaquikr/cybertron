import * as bookingActionTypes from '../bookingActionTypes';
import * as bookingReducer from '../bookingReducer';

export const setBookings = (bookings) => ({
  type: bookingActionTypes.SET_BOOKINGS,
  payload: {
    bookings,
  },
});

export const updateBookings = (bookings) => ({
  type: bookingActionTypes.UPDATE_BOOKINGS,
  payload: {
    bookings,
  },
});

export const deleteBookings = (bookingIds) => ({
  type: bookingActionTypes.DELETE_BOOKINGS,
  payload: {
    bookingIds,
  },
});

export const resetState = (state = bookingReducer.initialState) => ({
  type: bookingActionTypes.RESET_STATE,
  payload: {
    resetState: state,
  },
});
