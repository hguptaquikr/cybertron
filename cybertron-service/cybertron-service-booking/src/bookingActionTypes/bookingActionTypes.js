export const SET_BOOKINGS = 'cybertron-service-booking/SET_BOOKINGS';
export const UPDATE_BOOKINGS = 'cybertron-service-booking/UPDATE_BOOKINGS';
export const DELETE_BOOKINGS = 'cybertron-service-booking/DELETE_BOOKINGS';
export const RESET_STATE = 'cybertron-service-booking/RESET_STATE';
