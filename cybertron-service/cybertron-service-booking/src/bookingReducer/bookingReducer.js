import omitKeys from 'cybertron-utils/omitKeys';
import * as bookingActionTypes from '../bookingActionTypes/bookingActionTypes';
import * as bookingHelpers from '../bookingHelpers';

export const initialState = {};

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case bookingActionTypes.SET_BOOKINGS: {
      const bookingMap = bookingHelpers.makeBookingMap(payload.bookings);

      return {
        ...state,
        ...bookingMap,
      };
    }

    case bookingActionTypes.UPDATE_BOOKINGS: {
      const bookingMap = payload.bookings
        .reduce((acc, booking) => {
          if (!state[booking.id]) {
            throw new Error('Invalid Booking ID');
          }
          return {
            ...acc,
            [booking.id]: bookingHelpers.makeBooking({
              ...state[booking.id],
              ...booking,
            }),
          };
        }, {});

      return {
        ...state,
        ...bookingMap,
      };
    }

    case bookingActionTypes.DELETE_BOOKINGS: {
      const bookingMap = omitKeys(state, payload.bookingIds);

      return bookingMap;
    }

    case bookingActionTypes.RESET_STATE: {
      return payload.resetState;
    }

    default:
      return state;
  }
};
