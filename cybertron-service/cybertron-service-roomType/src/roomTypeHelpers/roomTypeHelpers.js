import * as roomTypeConstants from '../roomTypeConstants';

export const makeRoomType = (props) => {
  const roomType = {};

  roomType.id = props.id || '';
  roomType.type = roomTypeConstants.type[props.type] || '';
  roomType.roomNumbers = props.roomNumbers || [];
  roomType.maxOccupancy = props.maxOccupancy || {
    adults: '',
    children: '',
  };

  return roomType;
};

export const makeRoomTypeMap = (roomTypes) =>
  roomTypes.reduce((acc, roomType) => ({
    ...acc,
    [roomType.id]: makeRoomType(roomType),
  }), {});
