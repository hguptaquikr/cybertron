import * as roomTypeActionTypes from '../roomTypeActionTypes';
import * as roomTypeReducer from '../roomTypeReducer';

export const setRoomTypes = (roomTypes) => ({
  type: roomTypeActionTypes.SET_ROOM_TYPES,
  payload: {
    roomTypes,
  },
});

export const updateRoomTypes = (roomTypes) => ({
  type: roomTypeActionTypes.UPDATE_ROOM_TYPES,
  payload: {
    roomTypes,
  },
});

export const deleteRoomTypes = (roomTypeIds) => ({
  type: roomTypeActionTypes.DELETE_ROOM_TYPES,
  payload: {
    roomTypeIds,
  },
});

export const resetState = (state = roomTypeReducer.initialState) => ({
  type: roomTypeActionTypes.RESET_STATE,
  payload: {
    resetState: state,
  },
});
