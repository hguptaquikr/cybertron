import omitKeys from 'cybertron-utils/omitKeys';
import * as roomTypeActionTypes from '../roomTypeActionTypes';
import * as roomTypeHelpers from '../roomTypeHelpers';

export const initialState = {};

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case roomTypeActionTypes.SET_ROOM_TYPES: {
      const roomTypeMap = roomTypeHelpers.makeRoomTypeMap(payload.roomTypes);

      return {
        ...state,
        ...roomTypeMap,
      };
    }

    case roomTypeActionTypes.UPDATE_ROOM_TYPES: {
      const roomTypeMap = payload.roomTypes
        .reduce((acc, roomType) => {
          if (!state[roomType.id]) {
            throw new Error('Invalid Room Type ID');
          }
          return {
            ...acc,
            [roomType.id]: roomTypeHelpers.makeRoomType({
              ...state[roomType.id],
              ...roomType,
            }),
          };
        }, {});

      return {
        ...state,
        ...roomTypeMap,
      };
    }

    case roomTypeActionTypes.DELETE_ROOM_TYPES: {
      const roomTypeMap = omitKeys(state, payload.roomTypeIds);

      return roomTypeMap;
    }

    case roomTypeActionTypes.RESET_STATE: {
      return payload.initialState;
    }

    default:
      return state;
  }
};
