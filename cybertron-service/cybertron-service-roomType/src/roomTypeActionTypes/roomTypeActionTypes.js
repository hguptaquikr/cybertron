export const SET_ROOM_TYPES = 'cybertron-service-roomType/SET_ROOM_TYPES';
export const UPDATE_ROOM_TYPES = 'cybertron-service-roomType/UPDATE_ROOM_TYPES';
export const DELETE_ROOM_TYPES = 'cybertron-service-roomType/DELETE_ROOM_TYPES';
export const RESET_STATE = 'cybertron-service-roomType/RESET_STATE';
