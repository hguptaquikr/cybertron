export const type = {
  ACACIA: 'ACACIA',
  OAK: 'OAK',
  MAPLE: 'MAPLE',
  MAHOGANY: 'MAHOGANY',
};
