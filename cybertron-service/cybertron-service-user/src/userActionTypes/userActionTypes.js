export const SET_USERS = 'cybertron-service-user/SET_USERS';
export const UPDATE_USERS = 'cybertron-service-user/UPDATE_USERS';
export const DELETE_USERS = 'cybertron-service-user/DELETE_USERS';
export const RESET_STATE = 'cybertron-service-user/RESET_STATE';
