import omitKeys from 'cybertron-utils/omitKeys';
import * as userActionTypes from '../userActionTypes';
import * as userHelpers from '../userHelpers';

export const initialState = {};

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case userActionTypes.SET_USERS: {
      const userMap = userHelpers.makeUserMap(payload.users);

      return {
        ...state,
        ...userMap,
      };
    }

    case userActionTypes.UPDATE_USERS: {
      const userMap = payload.users
        .reduce((acc, user) => {
          if (!state[user.id]) {
            throw new Error('Invalid User ID');
          }
          return {
            ...acc,
            [user.id]: userHelpers.makeUser({
              ...state[user.id],
              ...user,
            }),
          };
        }, {});

      return {
        ...state,
        ...userMap,
      };
    }

    case userActionTypes.DELETE_USERS: {
      const userMap = omitKeys(state, payload.userIds);

      return userMap;
    }

    case userActionTypes.RESET_STATE: {
      return payload.resetState;
    }

    default:
      return state;
  }
};
