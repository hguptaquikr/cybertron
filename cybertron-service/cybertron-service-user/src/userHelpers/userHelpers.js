export const makeUser = (props) => {
  const user = {};

  user.id = props.id || '';
  user.name = {
    first: props.name.first || '',
    middle: props.name.middle || '',
    last: props.name.last || '',
  };
  user.mobile = props.mobile || '';
  user.email = props.email || '';
  user.type = props.type || '';
  user.hotelIds = props.hotelIds || [];
  user.permissions = props.permissions || [];

  return user;
};

export const makeUserMap = (users) =>
  users.reduce((acc, user) => ({
    ...acc,
    [user.id]: makeUser(user),
  }), {});
