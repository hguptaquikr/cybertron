import * as userActionTypes from '../userActionTypes';
import * as userReducer from '../userReducer';

export const setUsers = (users) => ({
  type: userActionTypes.SET_USERS,
  payload: {
    users,
  },
});

export const updateUsers = (users) => ({
  type: userActionTypes.UPDATE_USERS,
  payload: {
    users,
  },
});

export const deleteUsers = (userIds) => ({
  type: userActionTypes.DELETE_USERS,
  payload: {
    userIds,
  },
});

export const resetState = (state = userReducer.initialState) => ({
  type: userActionTypes.RESET_STATE,
  payload: {
    resetState: state,
  },
});
