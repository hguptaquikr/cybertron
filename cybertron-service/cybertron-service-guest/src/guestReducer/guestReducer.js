import omitKeys from 'cybertron-utils/omitKeys';
import * as guestActionTypes from '../guestActionTypes';
import * as guestHelpers from '../guestHelpers';

export const initialState = {};

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case guestActionTypes.SET_GUESTS: {
      const guestMap = guestHelpers.makeGuestMap(payload.guests);

      return {
        ...state,
        ...guestMap,
      };
    }

    case guestActionTypes.UPDATE_GUESTS: {
      const guestMap = payload.guests
        .reduce((acc, guest) => {
          if (!state[guest.id]) {
            throw new Error('Invalid Guest ID');
          }
          return {
            ...acc,
            [guest.id]: guestHelpers.makeGuest({
              ...state[guest.id],
              ...guest,
            }),
          };
        }, {});

      return {
        ...state,
        ...guestMap,
      };
    }

    case guestActionTypes.DELETE_GUESTS: {
      const guestMap = omitKeys(state, payload.guestIds);

      return guestMap;
    }

    case guestActionTypes.RESET_STATE: {
      return payload.resetState;
    }

    default:
      return state;
  }
};
