export const SET_GUESTS = 'cybertron-service-guest/SET_GUESTS';
export const UPDATE_GUESTS = 'cybertron-service-guest/UPDATE_GUESTS';
export const DELETE_GUESTS = 'cybertron-service-guest/DELETE_GUESTS';
export const RESET_STATE = 'cybertron-service-guest/RESET_STATE';
