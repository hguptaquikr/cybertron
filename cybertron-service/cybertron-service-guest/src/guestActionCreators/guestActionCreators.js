import * as guestActionTypes from '../guestActionTypes';
import * as guestReducer from '../guestReducer';

export const setGuests = (guests) => ({
  type: guestActionTypes.SET_GUESTS,
  payload: {
    guests,
  },
});

export const updateGuests = (guests) => ({
  type: guestActionTypes.UPDATE_GUESTS,
  payload: {
    guests,
  },
});

export const deleteGuests = (guestIds) => ({
  type: guestActionTypes.DELETE_GUESTS,
  payload: {
    guestIds,
  },
});

export const resetState = (state = guestReducer.initialState) => ({
  type: guestActionTypes.RESET_STATE,
  payload: {
    resetState: state,
  },
});
