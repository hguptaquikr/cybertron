import * as guestConstants from '../guestConstants';

export const makeGuest = (props) => {
  const guest = {};

  guest.id = props.id || '';
  guest.email = props.email || '';
  guest.firstName = props.firstName || '';
  guest.lastName = props.lastName || '';
  guest.phone = props.phone || '';
  guest.age = props.age || '';
  guest.gender = guestConstants.gender[props.gender] || '';
  guest.nationality = props.nationality || '';
  guest.roomStayId = props.roomStayId || '';
  guest.checkIn = props.checkIn || '';
  guest.checkOut = props.checkOut || '';

  return guest;
};

export const makeGuestMap = (guests) =>
  guests.reduce((acc, guest) => ({
    ...acc,
    [guest.id]: makeGuest(guest),
  }), {});
