export const makeRoom = (props) => {
  const room = {};

  room.id = props.id || '';
  room.type = props.type || '';
  room.number = props.number || '';
  room.floor = props.floor || '';
  room.building = props.building || '';

  return room;
};

export const makeRoomMap = (rooms) =>
  rooms.reduce((acc, room) => ({
    ...acc,
    [room.id]: makeRoom(room),
  }), {});
