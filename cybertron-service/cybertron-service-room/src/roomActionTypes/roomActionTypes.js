export const SET_ROOMS = 'cybertron-service-room/SET_ROOMS';
export const UPDATE_ROOMS = 'cybertron-service-room/UPDATE_ROOMS';
export const DELETE_ROOMS = 'cybertron-service-room/DELETE_ROOMS';
export const RESET_STATE = 'cybertron-service-room/RESET_STATE';
