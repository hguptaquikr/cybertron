import omitKeys from 'cybertron-utils/omitKeys';
import * as roomActionTypes from '../roomActionTypes';
import * as roomHelpers from '../roomHelpers';

export const initialState = {};

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case roomActionTypes.SET_ROOMS: {
      const roomMap = roomHelpers.makeRoomMap(payload.rooms);

      return {
        ...state,
        ...roomMap,
      };
    }

    case roomActionTypes.UPDATE_ROOMS: {
      const roomMap = payload.rooms
        .reduce((acc, room) => {
          if (!state[room.id]) {
            throw new Error('Invalid Room ID');
          }
          return {
            ...acc,
            [room.id]: roomHelpers.makeRoom({
              ...state[room.id],
              ...room,
            }),
          };
        }, {});

      return {
        ...state,
        ...roomMap,
      };
    }

    case roomActionTypes.DELETE_ROOMS: {
      const roomMap = omitKeys(state, payload.roomIds);

      return roomMap;
    }

    case roomActionTypes.RESET_STATE: {
      return payload.resetState;
    }

    default:
      return state;
  }
};
