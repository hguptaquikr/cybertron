import * as roomActionTypes from '../roomActionTypes';
import * as roomReducer from '../roomReducer';

export const setRooms = (rooms) => ({
  type: roomActionTypes.SET_ROOMS,
  payload: {
    rooms,
  },
});

export const updateRooms = (rooms) => ({
  type: roomActionTypes.UPDATE_ROOMS,
  payload: {
    rooms,
  },
});

export const deleteRooms = (roomIds) => ({
  type: roomActionTypes.DELETE_ROOMS,
  payload: {
    roomIds,
  },
});

export const resetState = (state = roomReducer.initialState) => ({
  type: roomActionTypes.RESET_STATE,
  payload: {
    resetState: state,
  },
});
