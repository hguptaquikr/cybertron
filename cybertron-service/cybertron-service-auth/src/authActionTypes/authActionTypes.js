export const SET_TOKEN = 'cybertron-service-auth/SET_TOKEN';
export const REMOVE_TOKEN = 'cybertron-service-auth/REMOVE_TOKEN';
export const SET_USER = 'cybertron-service-auth/SET_USER';
export const REMOVE_USER = 'cybertron-service-auth/REMOVE_USER';
