import * as userHelpers from 'cybertron-service-user/lib/userHelpers';
import * as authActionTypes from '../authActionTypes';

export const initialState = {
  token: '',
  user: {},
};

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case authActionTypes.SET_TOKEN:
      return {
        ...state,
        token: payload.token,
      };

    case authActionTypes.REMOVE_TOKEN:
      return {
        ...state,
        token: '',
      };

    case authActionTypes.SET_USER:
      return {
        ...state,
        user: userHelpers.makeUser(payload.user),
      };

    case authActionTypes.REMOVE_USER:
      return {
        ...state,
        user: {},
      };

    default:
      return state;
  }
};
