import * as authActionTypes from '../authActionTypes';

export const setToken = (token) => {
  if (typeof window !== 'undefined' && window.localStorage) {
    window.localStorage.setItem('auth_token', token);
  }

  return {
    type: authActionTypes.SET_TOKEN,
    payload: {
      token,
    },
  };
};

export const removeToken = () => {
  if (typeof window !== 'undefined' && window.localStorage) {
    window.localStorage.removeItem('auth_token');
  }

  return {
    type: authActionTypes.REMOVE_TOKEN,
    payload: undefined,
  };
};

export const setUser = (user) => {
  if (typeof window !== 'undefined' && window.localStorage) {
    window.localStorage.setItem('auth_user', JSON.stringify(user));
  }

  return {
    type: authActionTypes.SET_USER,
    payload: {
      user,
    },
  };
};

export const removeUser = () => {
  if (typeof window !== 'undefined' && window.localStorage) {
    window.localStorage.removeItem('auth_user');
  }

  return {
    type: authActionTypes.REMOVE_USER,
    payload: undefined,
  };
};
