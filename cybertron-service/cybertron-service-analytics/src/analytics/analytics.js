export default {
  config: {},
  globalProperties: {},
  platformProperties: {},
  init({ platform }) {
    this.config.platform = platform;
    this.config.platformType = platform === 'mobileWeb' || platform === 'desktop' ? 'web' : 'app';

    // Set web common platformProperties
    if (this.config.platformType === 'web') {
      this.platformProperties.isMobile = platform === 'mobileWeb';
    }
  },

  // Events
  track(eventName, properties) {
    const analyticsProperties = {
      ...this.globalProperties,
      ...properties,
      ...this.platformProperties,
    };
    if (this.config.platformType === 'web') {
      window.analytics.track(eventName, analyticsProperties);
    }
  },

  page(eventName, properties) {
    const analyticsProperties = {
      ...this.globalProperties,
      ...properties,
      ...this.platformProperties,
    };
    if (this.config.platformType === 'web') {
      window.analytics.page(`${eventName} Page Viewed`, analyticsProperties);
    }
  },
};
