import format from 'date-fns/format';

const formatDate = {
  toQuery: (date) => format(date, 'YYYY-MM-DD'),
  toView: (date) => format(date, 'ddd, D MMM'),
  to: (date, formatString) => format(date, formatString),
};

export default formatDate;
