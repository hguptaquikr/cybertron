import React from 'react'; // eslint-disable-line

const reactChildrenByType = (children) => {
  const childrenByType = {};
  React.Children.forEach(children, (child) => {
    childrenByType[child.type] = child;
  });
  return childrenByType;
};

export default reactChildrenByType;
