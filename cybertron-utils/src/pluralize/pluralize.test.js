import pluralize from './pluralize';

describe('pluralize', () => {
  it('returns the plural form if the amount is 0 (or falsy)', () => {
    const plural = pluralize(0, 'transformer');
    expect(plural).toEqual('transformers');
  });

  it('returns the singular form if the amount is 1', () => {
    const plural = pluralize(1, 'transformer');
    expect(plural).toEqual('transformer');
  });

  it('returns the plural form if the amount is greater than 1', () => {
    const plural = pluralize(2, 'transformer');
    expect(plural).toEqual('transformers');
  });

  it('supports custom plural forms', () => {
    const plural = pluralize(2, 'mouse', 'mice');
    expect(plural).toEqual('mice');
  });
});
