import winston from 'winston';
import makeLoggerTransports from '../_helpers/makeLoggerTransports';

const logger = new winston.Logger({
  transports: makeLoggerTransports('logger'),
  exitOnError: false,
});

export default logger;
