import winston from 'winston';
import mkdirp from 'mkdirp';

const { APP_ENV } = process.env;

const makeTransport = (type) => {
  const transports = [];

  if (APP_ENV === 'staging' || APP_ENV === 'production') {
    const projectPath = process.cwd().split('/').slice(-3).join('/');
    const logPath = `/var/log/${projectPath}`;

    // create the log directory if it doesn't exist
    mkdirp(logPath, (err) => {
      if (err) {
        // eslint-disable-next-line
        console.error('[ERROR] Failed to create the log directory', err);
      } else {
        // eslint-disable-next-line
        console.log('[INFO] Logging Folder Created at', logPath);
      }
    });

    if (type === 'request') {
      transports.push(
        new winston.transports.File({
          name: `file_${type}`,
          filename: `${logPath}/request.log`,
        }),
      );
    } else {
      transports.push(
        new winston.transports.File({
          name: `file_${type}_info`,
          level: 'info',
          filename: `${logPath}/info.log`,
        }),
      );
      transports.push(
        new winston.transports.File({
          name: `file_${type}_error`,
          level: 'error',
          filename: `${logPath}/error.log`,
        }),
      );
    }
  } else {
    transports.push(
      new winston.transports.Console({
        name: `console_${type}`,
        colorize: true,
      }),
    );
  }

  return transports;
};

export default makeTransport;
