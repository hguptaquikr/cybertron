import expressWinston from 'express-winston';
import makeLoggerTransports from '../_helpers/makeLoggerTransports';

const middlewareConfig = {
  meta: false,
  colorize: true,
  expressFormat: false,
  msg: 'HTTP {{res.statusCode}} {{req.method}} {{req.url}} {{res.responseTime}}ms',
};

if (process.env.NODE_ENV === 'production') {
  middlewareConfig.meta = true;
  middlewareConfig.colorize = false;
  middlewareConfig.expressFormat = true;
}

const expressLoggerMiddleware = expressWinston.logger({
  transports: makeLoggerTransports('request'),
  ...middlewareConfig,
});

export default expressLoggerMiddleware;
