/* eslint-disable no-console */
const fs = require('fs');
const path = require('path');

const ignoreFiles = [
  '.DS_Store',
  '_helpers',
  'generate.js',
];

const utils = fs.readdirSync(__dirname)
  .filter((util) => !ignoreFiles.includes(util));

// generate import root per util
utils.forEach((util) => {
  const file = path.resolve(__dirname, `../${util}.js`);
  const content = `module.exports = require('./lib/${util}');\n`;
  fs.writeFile(file, content, (err) => {
    if (err) throw err;
    console.log(`import root generated: ${util}.js `);
  });
});

// generate .gitignore
const gitignoreContent = utils.reduce((accContent, util) =>
  `${accContent.trim()}\n/${util}.js`, '');
fs.writeFile(path.resolve(__dirname, '../.gitignore'), `${gitignoreContent}\n`, (err) => {
  if (err) throw err;
  console.log('.gitignore generated');
});
