/* eslint-disable no-param-reassign */
import request from './request';

const makeRequest = (baseUrl = '', options = {}) => ({
  raw: request,

  get(url, data, opts = options) {
    opts.method = 'GET';
    return request(`${baseUrl}${url}`, data, opts);
  },

  post(url, data, opts = options) {
    opts.method = 'POST';
    return request(`${baseUrl}${url}`, data, opts);
  },

  put(url, data, opts = options) {
    opts.method = 'PUT';
    return request(`${baseUrl}${url}`, data, opts);
  },

  delete(url, data, opts = options) {
    opts.method = 'DELETE';
    return request(`${baseUrl}${url}`, data, opts);
  },
});

export default makeRequest;
