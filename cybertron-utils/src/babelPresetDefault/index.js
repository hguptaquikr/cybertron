const env = process.env.BABEL_ENV || process.env.NODE_ENV;

function buildPreset(context, opts) {
  const options = opts || {};
  return {
    presets: [
      ['env', env !== 'test' ? {
        targets: options.targets || { uglify: true },
        loose: options.loose || true,
        modules: options.modules || false,
        useBuiltIns: options.useBuiltIns || 'entry',
      } : {}],
      'react',
      'stage-1',
    ].filter(Boolean),
    plugins: [
      'react-loadable/babel',
      ['styled-components', {
        displayName: env !== 'production',
        ssr: env === 'production',
      }],
      env === 'production' && 'transform-react-remove-prop-types',
    ].filter(Boolean),
  };
}

module.exports = buildPreset;
