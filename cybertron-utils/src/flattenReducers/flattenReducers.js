import omitKeys from '../omitKeys';

const flattenReducers = (enhancements, base) =>
  (state, action) => {
    const enhancementKeys = Object.keys(enhancements);
    const nextEnhancementState = enhancementKeys.reduce((accNextEnhancementState, key) => {
      const enhancementState = state ? state[key] : state;
      const enhancementReducer = enhancements[key];
      return {
        ...accNextEnhancementState,
        [key]: enhancementReducer(enhancementState, action),
      };
    }, {});

    const baseState = state ? omitKeys(state, enhancementKeys) : state;
    const nextBaseState = base(baseState, action);

    return {
      ...nextBaseState,
      ...nextEnhancementState,
    };
  };

export default flattenReducers;
