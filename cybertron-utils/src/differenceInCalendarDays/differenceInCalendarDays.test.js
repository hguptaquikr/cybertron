import differenceInCalendarDays from './differenceInCalendarDays';

describe('differenceInCalendarDays', () => {
  it('returns the number of calendar days between the given dates', () => {
    const days = differenceInCalendarDays(
      new Date(2012, 6 /* Jul */, 2, 18, 0),
      new Date(2011, 6 /* Jul */, 2, 6, 0),
    );
    expect(days).toEqual(366);
  });

  it('returns a negative number if the first date is smaller', () => {
    const days = differenceInCalendarDays(
      new Date(2011, 6 /* Jul */, 2, 6, 0),
      new Date(2012, 6 /* Jul */, 2, 18, 0),
    );
    expect(days).toEqual(-366);
  });

  it('returns 0 if both the dates are same', () => {
    const days = differenceInCalendarDays(
      new Date(2014, 8 /* Sep */, 5, 0, 0),
      new Date(2014, 8 /* Sep */, 5, 0, 0),
    );
    expect(days).toEqual(0);
  });

  it('returns NaN if the first date is an invalid date', () => {
    const days = differenceInCalendarDays(
      new Date(NaN),
      new Date(2017, 0 /* Jan */, 1),
    );
    expect(days).toEqual(NaN);
  });

  it('returns NaN if the second date is an invalid date', () => {
    const days = differenceInCalendarDays(
      new Date(2017, 0 /* Jan */, 1),
      new Date(NaN),
    );
    expect(days).toEqual(NaN);
  });

  it('returns NaN if both dates are invalid dates', () => {
    const days = differenceInCalendarDays(
      new Date(NaN),
      new Date(NaN),
    );
    expect(days).toEqual(NaN);
  });
});
