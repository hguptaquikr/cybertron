import omitKeys from './omitKeys';

const dragon = {
  name: 'fluffykins',
  type: 'ancient',
  attack: 'ice',
};

describe('omitKeys', () => {
  it('omits single key', () => {
    const actual = omitKeys(dragon, ['type']);
    const expected = {
      name: 'fluffykins',
      attack: 'ice',
    };
    expect(actual).toEqual(expected);
  });

  it('omits multiple keys', () => {
    const actual = omitKeys(dragon, ['type', 'attack']);
    const expected = {
      name: 'fluffykins',
    };
    expect(actual).toEqual(expected);
  });

  it('ignores missing keys', () => {
    const actual = omitKeys(dragon, ['weakness', 'size']);
    const expected = { ...dragon };
    expect(actual).toEqual(expected);
  });

  it('ignores empty object', () => {
    const actual = omitKeys({}, ['alpha', 'beta']);
    const expected = {};
    expect(actual).toEqual(expected);
  });

  it('ignores empty keys', () => {
    const actual = omitKeys(dragon, []);
    const expected = { ...dragon };
    expect(actual).toEqual(expected);
  });
});
