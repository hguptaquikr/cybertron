const omitKeys = (object = {}, keysToOmit = []) =>
  Object.keys(object).reduce((acc, key) => {
    if (keysToOmit.includes(key)) {
      return acc;
    }
    return {
      ...acc,
      [key]: object[key],
    };
  }, {});

export default omitKeys;
