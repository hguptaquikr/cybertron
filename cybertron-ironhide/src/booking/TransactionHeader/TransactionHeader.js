import React from 'react';
import Flex from 'leaf-ui/Flex/web';
import Card from 'leaf-ui/Card/web';
import Text from 'leaf-ui/Text/web';
import Button from 'leaf-ui/Button/web';
import Spacer from 'leaf-ui/Spacer/web';

class TransactionHeader extends React.Component {
  state = {};

  render() {
    return (
      <Spacer padding={[4]}>
        <Card>
          <Flex justifyContent="space-between" alignItems="center">
            <Flex flexDirection="column">
              <Spacer margin={[0, 0, 1, 0]}>
                <Text size="xxxl" weight="bold">All Transactions Summary</Text>
              </Spacer>
            </Flex>
            <Flex justifyContent="space-between">
              <Spacer margin={[0, 2, 0, 0]}>
                <Button kind="outlined" size="small">RECORD PAYMENT</Button>
              </Spacer>
              <Button size="small" onClick={this.onCheckIn}>ADD CHARGE</Button>
            </Flex>
          </Flex>
        </Card>
      </Spacer>
    );
  }
}

export default TransactionHeader;
