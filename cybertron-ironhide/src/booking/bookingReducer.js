import flattenReducers from 'cybertron-utils/flattenReducers';
import * as bookingServiceReducer from 'cybertron-service-booking/lib/bookingReducer';
import * as bookingActionTypes from './bookingActionTypes';

const initialState = {
  ids: [],
};

const bookingReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case bookingActionTypes.SET_BOOKING_IDS: {
      return {
        ...state,
        ids: payload.bookingIds,
      };
    }

    default:
      return state;
  }
};

export default flattenReducers({
  byId: bookingServiceReducer.reducer,
}, bookingReducer);
