import * as bookingActionCreators from 'cybertron-service-booking/lib/bookingActionCreators';
import * as roomStayActionCreators from 'cybertron-service-roomStay/lib/roomStayActionCreators';
import * as bookingActionTypes from './bookingActionTypes';

export * from 'cybertron-service-booking/lib/bookingActionCreators';

export const setBookingIds = (bookingIds) => (dispatch, getState) =>
  dispatch({
    type: bookingActionTypes.SET_BOOKING_IDS,
    payload: {
      bookingIds: [...getState().$booking.ids, ...bookingIds],
    },
  });

export const getBooking = (bookingId) =>
  (dispatch, getState, { request }) =>
    dispatch({
      type: bookingActionTypes.GET_BOOKING,
      promise: request.get(`/api/crs/v1/booking/${bookingId}`)
        .then((res) => {
          dispatch(bookingActionCreators.setBookings([res]));
          dispatch(setBookingIds([res.id]));
          dispatch(roomStayActionCreators.setRoomStays(res.roomStays));
          return res;
        }),
    });
