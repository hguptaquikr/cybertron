import React from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { graphql, compose } from 'react-apollo';
import Flex from 'leaf-ui/Flex/web';
import Text from 'leaf-ui/Text/web';
import Spacer from 'leaf-ui/Spacer/web';
import Form from 'leaf-ui/Form/web';
import Card from 'leaf-ui/Card/web';
import BookingConfig from '../BookingConfig/BookingConfig';
import PriceSummary from '../../price/PriceSummary/PriceSummary';
import RoomConfig from '../../room/RoomConfig/RoomConfig';
import RoomSection from '../../room/RoomConfig/RoomSection';

class NewBooking extends React.Component {
  state = {
    requiredRooms: {},
  }

  handleAddRoom = (roomType) => {
    this.setState((prevState) => ({
      requiredRooms: {
        ...prevState.requiredRooms,
        [roomType]: prevState.requiredRooms[roomType] ? prevState.requiredRooms[roomType] + 1 : 1,
      },
    }));
  }

  handleRemoveRoom = (roomType) => {
    this.setState((prevState) => ({
      requiredRooms: {
        ...prevState.requiredRooms,
        [roomType]: prevState.requiredRooms[roomType] ? prevState.requiredRooms[roomType] - 1 : 0,
      },
    }));
  }

  render() {
    const { $newBooking } = this.props;
    const {
      requiredRooms,
    } = this.state;

    return (
      <Form
        validationSchema={
          Form.validation.object().shape({
            checkInDate: Form.validation.string().required(),
            checkOutDate: Form.validation.string().required(),
            bookingType: Form.validation.string().required(),
          })
        }
        onSubmit={this.createBooking}
        render={(props) => (
          <React.Fragment>
            <Spacer padding={[4]}>
              <Card>
                <Flex flexDirection="column">
                  <Spacer margin={[0, 0, 1, 0]}>
                    <Text size="xxxl" weight="bold">
                      New Booking
                    </Text>
                  </Spacer>
                </Flex>
              </Card>
            </Spacer>
            <Spacer margin={[4]}>
              <Flex>
                <Spacer margin={[0, 1, 0, 0]}>
                  <Flex flexDirection="column" flex="3">
                    <Spacer margin={[0, 0, 1, 0]}>
                      <BookingConfig
                        onDatesChange={$newBooking.refetch}
                        values={props.values}
                      />
                    </Spacer>
                    <RoomConfig
                      roomTypes={$newBooking.loading ? [] : $newBooking.hotelById.roomTypes}
                      values={props.values}
                      rows={requiredRooms}
                      handleAddRoom={this.handleAddRoom}
                      onRoomConfigChange={$newBooking.refetch}
                    />
                    <RoomSection
                      roomTypes={$newBooking.loading ? [] : $newBooking.hotelById.roomTypes}
                      values={props.values}
                      rows={requiredRooms}
                      handleRemoveRoom={this.handleRemoveRoom}
                      onRoomConfigChange={$newBooking.refetch}
                    />
                  </Flex>
                </Spacer>
                <Flex flexDirection="column" flex="1">
                  {
                    !$newBooking.loading ? (
                      <PriceSummary
                        roomTypes={$newBooking.hotelById.roomTypes}
                        isValid={props.isValid}
                        values={props.values}
                        rows={requiredRooms}
                      />
                    ) : null
                  }
                </Flex>
              </Flex>
            </Spacer>
          </React.Fragment>
        )}
      />
    );
  }
}

NewBooking.propTypes = {
  $newBooking: PropTypes.object,
};

export default compose(
  graphql(gql`
    query newBooking(
      $hotelId: ID!,
      $fromDate: String!,
      $toDate: String!,
      $roomConfig: String!,
    ) {
      hotelById(id: $hotelId) {
        id
        name
        roomTypes {
          name
          maxOccupancy {
            adults
            children
          }
          price (
            fromDate: $fromDate,
            toDate: $toDate,
            roomConfig: $roomConfig
          ) {
            date
            ratePlans {
              name
              preTax
              postTax
              tax
            }
          }
          availability (
            fromDate: $fromDate,
            toDate: $toDate,
            roomConfig: $roomConfig,
            channel: "web",
            subChannel: "direct"
          ) {
            count
          }
        }
      }
    }
  `, {
    name: '$newBooking',
    options: {
      variables: {
        hotelId: '0001661',
        fromDate: '2018-06-22',
        toDate: '2018-06-23',
        roomConfig: '1-0',
      },
    },
  }),
)(NewBooking);
