import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import Form from 'leaf-ui/Form/web';
import TextInput from 'leaf-ui/TextInput/web';
import Modal from 'leaf-ui/Modal/web';
import Flex from 'leaf-ui/Flex/web';
import Text from 'leaf-ui/Text/web';
import Button from 'leaf-ui/Button/web';
import * as tabMutations from '../../tab/tabMutations';

class EditBookingOwner extends React.Component {
  closeEditBookingOwner = () => {
    const { tabIndex, flowIndex, removeFlow } = this.props;
    removeFlow({
      variables: {
        tabIndex,
        flowIndex,
      },
    });
  }

  confirmBooking = () => {
    console.log('this.props', this.props);
    setTimeout(this.closeEditBookingOwner, 1000);
  }

  render() {
    return (
      <Modal
        isOpen
        onClose={this.closeEditBookingOwner}
      >
        <Form
          validationSchema={
            Form.validation.object().shape({
              guestName: Form.validation.string().required(),
            })
          }
          onSubmit={this.confirmBooking}
        >
          <Form.Form>
            <Modal.Header>
              <Text size="xxxl">
                Enter Booking Owner Details
              </Text>
            </Modal.Header>
            <Modal.Content>
              <Flex flexDirection="column">
                <Flex justifyContent="space-between">
                  <TextInput
                    name="guestName"
                    label="Guest Name"
                  />
                  <TextInput
                    name="contact"
                    label="Contact No"
                  />
                </Flex>
                <Flex>
                  <TextInput
                    name="email"
                    label="Email Address"
                  />
                </Flex>
                <Flex>
                  <TextInput
                    name="specialRequest"
                    label="Special Request"
                  />
                </Flex>
              </Flex>
            </Modal.Content>
            <Modal.Footer>
              <Flex flexDirection="row-reverse">
                <Button
                  size="small"
                  type="submit"
                >
                  CREATE BOOKING
                </Button>
              </Flex>
            </Modal.Footer>
          </Form.Form>
        </Form>
      </Modal>
    );
  }
}

EditBookingOwner.propTypes = {
  tabIndex: PropTypes.string,
  flowIndex: PropTypes.string,
  removeFlow: PropTypes.func.isRequired,
};

export default compose(
  graphql(tabMutations.REMOVE_FLOW, { name: 'removeFlow' }),
)(EditBookingOwner);
