import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Card from 'leaf-ui/Card/web';
import Flex from 'leaf-ui/Flex/web';
import Text from 'leaf-ui/Text/web';
import TextInput from 'leaf-ui/TextInput/web';
import Spacer from 'leaf-ui/Spacer/web';
import Select from 'leaf-ui/Select/web';
import Form from 'leaf-ui/Form/web';

class BookingConfig extends React.Component {
  state = {};

  render() {
    const {
      className,
      onDatesChange,
    } = this.props;

    const bookingTypeOptions = ['WalkIn', 'Pay at hotel', 'Direct', 'Corporate'].map((type) => ({
      label: type,
      value: type,
    }));

    return (
      <Spacer padding={[5]} className={className}>
        <Card>
          <Form.Form>
            <Flex>
              <Flex flexDirection="column" justifyContent="space-between" flex="1">
                <Text>Booking Type</Text>
                <Select
                  name="bookingType"
                  label="Booking Type"
                  options={bookingTypeOptions}
                  defaultSelected={bookingTypeOptions[0]}
                  onChange={() => {}}
                />
              </Flex>
              <Flex flexDirection="column" justifyContent="space-between" flex="1">
                <Text>Stay Dates</Text>
                <Flex justifyContent="space-between">
                  <TextInput
                    name="checkInDate"
                    label="Check in Date"
                    defaultValue="2018-06-22"
                    onBlur={(e) => onDatesChange({ fromDate: e.target.value })}
                  />
                  <TextInput
                    name="checkOutDate"
                    label="Check Out Date"
                    defaultValue="2018-06-23"
                    onBlur={(e) => onDatesChange({ toDate: e.target.value })}
                  />
                </Flex>
              </Flex>
            </Flex>
          </Form.Form>
        </Card>
      </Spacer>
    );
  }
}

BookingConfig.propTypes = {
  className: PropTypes.string,
  onDatesChange: PropTypes.func,
};

const mapStateToProps = (state) => ({
  booking: state.$booking,
});

export default connect(
  mapStateToProps,
)(BookingConfig);
