import { withFilter } from 'graphql-subscriptions';
import pubsub from '../bootstrap/pubsub';

const typeDefs = `
  extend type Subscription {
    bookingCreated(hotelId: ID!): Booking!
  }
`;

const resolvers = {
  Subscription: {
    bookingCreated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator('bookingCreated'),
        (payload, args) => payload.hotelId === args.hotelId,
      ),
      resolve: (payload, args, context, info) =>
        info.mergeInfo.delegateToSchema({
          schema: info.schema,
          operation: 'query',
          fieldName: 'bookingById',
          args: {
            id: payload.bookingId,
          },
          context,
          info,
        }),
    },
  },
};

export default {
  typeDefs,
  resolvers,
};
