import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import Form from 'leaf-ui/Form/web';
import TextInput from 'leaf-ui/TextInput/web';
import Modal from 'leaf-ui/Modal/web';
import Flex from 'leaf-ui/Flex/web';
import Text from 'leaf-ui/Text/web';
import Spacer from 'leaf-ui/Spacer/web';
import Button from 'leaf-ui/Button/web';
import * as tabMutations from '../../tab/tabMutations';
import * as tabHelpers from '../../tab/tabHelpers';

class CancelBooking extends React.Component {
  closeCancelBooking = () => {
    const { tabIndex, flowIndex, removeFlow } = this.props;
    removeFlow({
      variables: {
        tabIndex,
        flowIndex,
      },
    });
  }

  confirmCancelBooking = () => {
    setTimeout(this.closeCancelBooking, 1000);
  }

  render() {
    return (
      <Modal
        isOpen
        onClose={this.closeCancelBooking}
        container={() => tabHelpers.makeFlowId(this.props)}
      >
        <Form
          validationSchema={
            Form.validation.object().shape({
              comment: Form.validation.string().required(),
            })
          }
          onSubmit={this.confirmCancelBooking}
        >
          <Form.Form>
            <Modal.Header>
              <Text size="xxxl">
                Cancel Booking
              </Text>
            </Modal.Header>
            <Modal.Content>
              <Text size="l" error="invalid leaf color">
                Are you sure about cancelling this booking?
              </Text>
              <Spacer margin={[2, 1, 5, 0]}>
                <TextInput
                  name="comment"
                  label="Enter your comments"
                />
              </Spacer>
            </Modal.Content>
            <Modal.Footer>
              <Flex flexDirection="row-reverse">
                <Button type="submit">
                  CANCEL BOOKING
                </Button>
              </Flex>
            </Modal.Footer>
          </Form.Form>
        </Form>
      </Modal>
    );
  }
}

CancelBooking.propTypes = {
  tabIndex: PropTypes.string,
  flowIndex: PropTypes.string,
  removeFlow: PropTypes.func.isRequired,
};

export default compose(
  graphql(tabMutations.REMOVE_FLOW, { name: 'removeFlow' }),
)(CancelBooking);
