import React from 'react';
import { graphql, compose } from 'react-apollo';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as bookingHelpers from 'cybertron-service-booking/lib/bookingHelpers';
import isEmpty from 'cybertron-utils/isEmpty';
import Flex from 'leaf-ui/Flex/web';
import Card from 'leaf-ui/Card/web';
import Text from 'leaf-ui/Text/web';
import Button from 'leaf-ui/Button/web';
import Spacer from 'leaf-ui/Spacer/web';
import * as guestModel from '../../guest/guestModel';
import BookingIndicators from '../BookingIndicators/BookingIndicators';
import * as tabQueries from '../../tab/tabQueries';
import * as tabMutations from '../../tab/tabMutations';

class BookingHeader extends React.Component {
  openCancelBooking = () => {
    const { $selectedTabIndex, addFlow } = this.props;
    addFlow({
      variables: {
        tabIndex: $selectedTabIndex.selectedTabIndex,
        flow: {
          component: 'CancelBooking',
        },
      },
    });
  }

  render() {
    const {
      booking,
      bookingId,
    } = this.props;
    return (
      <Spacer padding={[4]}>
        <Card>
          <Flex justifyContent="space-between" alignItems="center">
            <Flex flexDirection="column">
              <Spacer margin={[0, 0, 1, 0]}>
                <Text size="xxxl" weight="bold">
                  {
                    !isEmpty(booking.byId[bookingId]) ?
                    guestModel.getFullName(booking.byId[bookingId].creator.name)
                    : <Text>New Booking</Text>
                  }
                </Text>
              </Spacer>
              <BookingIndicators bookingId={bookingId} />
            </Flex>
            <Flex justifyContent="space-between">
              <Spacer margin={[0, 2, 0, 0]}>
                <Button
                  kind="outlined"
                  size="small"
                >
                  MARK NO SHOW
                </Button>
              </Spacer>
              <Spacer margin={[0, 2, 0, 0]}>
                <Button
                  kind="outlined"
                  size="small"
                  onClick={this.openCancelBooking}
                >
                  CANCEL BOOKING
                </Button>
              </Spacer>
              <Button
                size="small"
              >
                CHECK IN
              </Button>
            </Flex>
          </Flex>
        </Card>
      </Spacer>
    );
  }
}

BookingHeader.propTypes = {
  booking: PropTypes.object,
  bookingId: PropTypes.string,
  $selectedTabIndex: PropTypes.object,
  addFlow: PropTypes.func,
};

const mapStateToProps = (state) => ({
  booking: state.$booking || bookingHelpers.makeBooking({}),
  $selectedTabIndex: PropTypes.object,
  addFlow: PropTypes.func.isRequired,
});

export default compose(
  connect(mapStateToProps),
  graphql(tabQueries.SELECTED_TAB_INDEX, { name: '$selectedTabIndex' }),
  graphql(tabMutations.ADD_FLOW, { name: 'addFlow' }),
)(BookingHeader);
