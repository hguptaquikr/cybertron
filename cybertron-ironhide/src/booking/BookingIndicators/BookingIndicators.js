import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as bookingHelpers from 'cybertron-service-booking/lib/bookingHelpers';
import isEmpty from 'cybertron-utils/isEmpty';
import Flex from 'leaf-ui/Flex/web';
import Card from 'leaf-ui/Card/web';
import Tag from 'leaf-ui/Tag/web';
import Spacer from 'leaf-ui/Spacer/web';

class BookingIndicators extends React.Component {
  state = {};

  render() {
    const {
      booking,
      bookingId,
    } = this.props;
    return (
      <Card>
        <Flex flexDirection="column">
          {
              !isEmpty(booking.byId[bookingId]) ?
                <Flex>
                  <Spacer margin={[0, 1, 0, 0]}>
                    <Tag
                      color="primary"
                      kind="outlined"
                      shape="capsular"
                    >
                      {booking.byId[bookingId].paymentCategory}
                    </Tag>
                  </Spacer>
                  <Spacer margin={[0, 1, 0, 0]}>
                    <Tag
                      color="lagoon"
                      kind="outlined"
                      shape="capsular"
                    >
                      {booking.byId[bookingId].status}
                    </Tag>
                  </Spacer>
                  <Tag
                    color="blue"
                    kind="outlined"
                    shape="capsular"
                  >
                    {booking.byId[bookingId].source.type}
                  </Tag>
                </Flex>
               : null
              }
        </Flex>
      </Card>
    );
  }
}

BookingIndicators.propTypes = {
  booking: PropTypes.object,
  bookingId: PropTypes.string,
};

const mapStateToProps = (state) => ({
  booking: state.$booking || bookingHelpers.makeBooking({}),
});

export default connect(
  mapStateToProps,
)(BookingIndicators);
