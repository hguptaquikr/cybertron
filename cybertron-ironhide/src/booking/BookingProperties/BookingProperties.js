import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as bookingHelpers from 'cybertron-service-booking/lib/bookingHelpers';
import format from 'date-fns/format';
import differenceInCalendarDays from 'date-fns/difference_in_calendar_days';
import pluralize from 'cybertron-utils/pluralize';
import Card from 'leaf-ui/Card/web';
import Text from 'leaf-ui/Text/web';
import Spacer from 'leaf-ui/Spacer/web';
import Flex from 'leaf-ui/Flex/web';
import Link from 'leaf-ui/Link/web';
import { dateFormat } from '../../core/constants';

class BookingProperties extends React.Component {
  state = {};

  render() {
    const {
      className,
      booking,
      bookingId,
    } = this.props;

    return (
      <Spacer padding={[5]} className={className}>
        <Card>
          <Spacer margin={[0, 0, 4, 0]}>
            <Text size="xxl" weight="bold">Booking Details</Text>
          </Spacer>
          <Spacer margin={[0, 0, 4, 0]}>
            <Flex>
              <Spacer margin={[0, 3, 0, 0]}>
                <Flex flexDirection="column">
                  <Spacer margin={[0, 0, 2, 0]}>
                    <Text size="xs" color="slate">Check In</Text>
                  </Spacer>
                  <Text size="m">
                    {format(booking.byId[bookingId].checkIn, dateFormat.view)}
                  </Text>
                </Flex>
              </Spacer>
              <Spacer margin={[0, 3, 0, 0]}>
                <Flex flexDirection="column">
                  <Spacer margin={[0, 0, 2, 0]}>
                    <Text size="xs" color="leaf">Nights</Text>
                  </Spacer>
                  <Text size="xs" color="leaf">
                    {pluralize(differenceInCalendarDays(booking.byId[bookingId].checkOut, booking.byId[bookingId].checkIn), 'Night', true)}
                  </Text>
                </Flex>
              </Spacer>
              <Spacer margin={[0, 3, 0, 0]}>
                <Flex flexDirection="column">
                  <Spacer margin={[0, 0, 2, 0]}>
                    <Text size="xs" color="slate">Check Out</Text>
                  </Spacer>
                  <Text size="m">
                    {format(booking.byId[bookingId].checkOut, dateFormat.view)}
                  </Text>
                </Flex>
              </Spacer>
            </Flex>
          </Spacer>
          <Flex flexDirection="column">
            <Spacer margin={[0, 0, 4, 0]}>
              <Flex>
                <Flex flexDirection="column" flex="1">
                  <Spacer margin={[0, 0, 2, 0]}>
                    <Text size="xs" color="slate">Source of Booking</Text>
                  </Spacer>
                  <Text size="m">
                    {booking.byId[bookingId].source.name}
                  </Text>
                  <Text size="m"> ID -
                    {booking.byId[bookingId].source.referenceNumber}
                  </Text>
                </Flex>
                <Flex flexDirection="column" flex="1">
                  <Spacer margin={[0, 0, 2, 0]}>
                    <Text size="xs" color="slate">Primary Guest Details</Text>
                  </Spacer>
                  <Text size="m">
                    {booking.byId[bookingId].creator.phone}
                  </Text>
                  <Text size="m">
                    {booking.byId[bookingId].creator.email}
                  </Text>
                </Flex>
              </Flex>
            </Spacer>
            <Flex>
              <Flex flexDirection="column" flex="1">
                <Spacer margin={[0, 0, 2, 0]}>
                  <Text size="xs" color="slate">Special Request</Text>
                </Spacer>
                <Text size="m">
                  {booking.byId[bookingId].specialRequest}
                </Text>
              </Flex>
              <Flex flexDirection="column" flex="1">
                <Spacer margin={[0, 0, 2, 0]}>
                  <Text size="xs" color="slate">GSTIN</Text>
                </Spacer>
                <Text size="m">
                  {booking.byId[bookingId].gstin}
                </Text>
                <Flex>
                  <Link>
                    Change GSTIN
                  </Link>
                </Flex>
              </Flex>
            </Flex>
          </Flex>
        </Card>
      </Spacer>
    );
  }
}

BookingProperties.propTypes = {
  booking: PropTypes.object,
  bookingId: PropTypes.string,
  className: PropTypes.string,
};

const mapStateToProps = (state) => ({
  booking: state.$booking || bookingHelpers.makeBooking({}),
});

export default connect(
  mapStateToProps,
)(BookingProperties);
