import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Flex from 'leaf-ui/Flex/web';
import Spacer from 'leaf-ui/Spacer/web';
import BookingHeader from '../BookingHeader/BookingHeader';
import BookingProperties from '../BookingProperties/BookingProperties';
import PriceDetails from '../../price/PriceDetails/PriceDetails';
import RoomStayList from '../../room/RoomStayList/RoomStayList';
import * as bookingActionCreators from '../../booking/bookingActionCreators';

class BookingDetails extends React.Component {
  componentDidMount() {
    const { bookingActions, bookingId } = this.props;
    bookingActions.getBooking(bookingId);
  }

  render() {
    const { bookingId } = this.props;
    return (
      <React.Fragment>
        <BookingHeader bookingId={bookingId} />
        <Spacer margin={[4]}>
          <Flex>
            <Spacer margin={[0, 1, 0, 0]}>
              <Flex flexDirection="column" flex="3">
                <Spacer margin={[0, 0, 1, 0]}>
                  <BookingProperties bookingId={bookingId} />
                </Spacer>
                <RoomStayList bookingId={bookingId} />
              </Flex>
            </Spacer>
            <Flex flexDirection="column" flex="1">
              <PriceDetails bookingId={bookingId} />
            </Flex>
          </Flex>
        </Spacer>
      </React.Fragment>
    );
  }
}

BookingDetails.propTypes = {
  bookingActions: PropTypes.object,
  bookingId: PropTypes.string,
};

const mapStateToProps = (state) => ({
  // bookingId: ownProps.viewId.split('/').pop(),
  bookingId: 'B1',
  $booking: state.$booking.byId.B1,
  $guest: state.$guest,
});

const mapDispatchToProps = (dispatch) => ({
  bookingActions: bindActionCreators(bookingActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BookingDetails);
