import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import isEmpty from 'cybertron-utils/isEmpty';
import Flex from 'leaf-ui/Flex/web';
import Spacer from 'leaf-ui/Spacer/web';
import TransactionHeader from '../TransactionHeader/TransactionHeader';
import ChargeDetails from '../../charge/ChargeDetails/ChargeDetails';
import InvoiceDetails from '../../charge/InvoiceDetails/InvoiceDetails';
import PaymentDetails from '../../payment/PaymentDetails/PaymentDetails';
import * as bookingActionCreators from '../../booking/bookingActionCreators';

class TransactionDetails extends React.Component {
  state = {
  };

  componentDidMount() {
    const { bookingActions, bookingId } = this.props;
    bookingActions.getBooking(bookingId);
  }

  render() {
    const {
      bookingId,
      $booking,
    } = this.props;

    const thisBooking = !isEmpty($booking.byId[bookingId]) ? $booking.byId[bookingId] : {};

    return (
      <React.Fragment>
        <TransactionHeader bookingId={bookingId} booking={thisBooking} />
        <Spacer margin={[4]}>
          <Flex>
            <Flex flexDirection="column" flex="3">
              <Spacer margin={[0, 0, 1, 0]}>
                <ChargeDetails />
              </Spacer>
              <Spacer margin={[0, 0, 1, 0]}>
                <PaymentDetails />
              </Spacer>
              <InvoiceDetails />
            </Flex>
          </Flex>
        </Spacer>
      </React.Fragment>
    );
  }
}

TransactionDetails.propTypes = {
  bookingId: PropTypes.string,
  $booking: PropTypes.object,
  bookingActions: PropTypes.object,
};

const mapStateToProps = (state) => ({
  $booking: state.$booking,
  $guest: state.guest,
});

const mapDispatchToProps = (dispatch) => ({
  bookingActions: bindActionCreators(bookingActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TransactionDetails);
