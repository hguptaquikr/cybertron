import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as bookingHelpers from 'cybertron-service-booking/lib/bookingHelpers';
import { graphql, compose } from 'react-apollo';
import Card from 'leaf-ui/Card/web';
import Text from 'leaf-ui/Text/web';
import Price from 'leaf-ui/Price/web';
import Spacer from 'leaf-ui/Spacer/web';
import Flex from 'leaf-ui/Flex/web';
import Divider from 'leaf-ui/Divider/web';
import Button from 'leaf-ui/Button/web';
import Link from 'leaf-ui/Link/web';
import * as tabMutations from '../../tab/tabMutations';
import * as tabQueries from '../../tab/tabQueries';

class PriceDetails extends React.Component {
  state = {};

  render() {
    const {
      className,
      bookingId,
      booking,
      addTab,
      addFlow,
      $selectedTabIndex,
    } = this.props;

    const prices = booking.byId[bookingId].price;

    return (
      <Spacer padding={[5]} className={className}>
        <Card>
          <Flex flexDirection="column">
            <Spacer margin={[0, 0, 3, 0]}>
              <Flex justifyContent="space-between">
                <Text color="slate">Room Tariff</Text>
                <Text color="slate"><Price>{prices.basePrice}</Price></Text>
              </Flex>
            </Spacer>
            <Spacer margin={[0, 0, 3, 0]}>
              <Flex justifyContent="space-between">
                <Text color="slate">Room Discount ({prices.discountPercentage}%)</Text>
                <Text color="leaf">-<Price>{prices.discountPrice}</Price></Text>
              </Flex>
            </Spacer>
            <Spacer margin={[0, 0, 3, 0]}>
              <Flex justifyContent="space-between">
                <Text color="slate">Advance</Text>
                <Text color="slate"><Price>{prices.taxPrice}</Price></Text>
              </Flex>
            </Spacer>
          </Flex>
          <Spacer margin={[0, 0, 3, 0]}>
            <Divider type="dashed" />
          </Spacer>
          <Flex justifyContent="space-between">
            <Spacer margin={[0, 3, 0, 0]}>
              <Flex flexDirection="column">
                <Spacer margin={[0, 0, 1, 0]}>
                  <Text size="xl" weight="bold">Total Payable</Text>
                </Spacer>
                <Text size="xs" color="rock">Incl. of all taxes</Text>
              </Flex>
            </Spacer>
            <Text size="xxl" weight="bold"><Price>{prices.sellingPrice}</Price></Text>
          </Flex>
          <Flex flexDirection="column">
            <Spacer margin={[3, 0]}>
              <Flex>
                <Link
                  onClick={() => {
                    addTab({
                      variables: {
                          tab: {
                            flows: [{
                              component: 'TransactionDetails',
                              props: { bookingId: 'B1' },
                            }],
                          },
                        },
                    });
                  }}
                >
                  Transaction Details
                </Link>
              </Flex>
            </Spacer>
            <Button
              onClick={() => {
                addFlow({
                  variables: {
                    tabIndex: $selectedTabIndex.selectedTabIndex,
                    flow: {
                      component: 'RecordPayment',
                      props: { bookingId: 1 },
                    },
                  },
                });
              }}
            >
              RECORD PAYMENT
            </Button>
          </Flex>
        </Card>
      </Spacer>
    );
  }
}

PriceDetails.propTypes = {
  className: PropTypes.string,
  booking: PropTypes.object,
  bookingId: PropTypes.string,
  addTab: PropTypes.func.isRequired,
  addFlow: PropTypes.func.isRequired,
  $selectedTabIndex: PropTypes.object,
};

const mapStateToProps = (state) => ({
  booking: state.$booking || bookingHelpers.makeBooking({}),
});

export default compose(
  connect(mapStateToProps),
  graphql(tabMutations.ADD_TAB, { name: 'addTab' }),
  graphql(tabMutations.ADD_FLOW, { name: 'addFlow' }),
  graphql(tabQueries.SELECTED_TAB_INDEX, { name: '$selectedTabIndex' }),
)(PriceDetails);
