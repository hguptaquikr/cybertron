const priceProto = {
  get introduce() {
    return `Hey, my name is ${this.name}`;
  },
};

export const makePrice = (name) => {
  const price = Object.create(priceProto);
  price.name = name;
  return price;
};

export const normalize = (prices) => ({
  byId: prices.reduce((obj, price, index) => ({ ...obj, [index]: price }), {}),
  results: prices.map((price, index) => index),
});

export const getAll = (results, byId) =>
  results.map((priceId) => byId[priceId]);
