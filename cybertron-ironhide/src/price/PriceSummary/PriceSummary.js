import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import isEmpty from 'cybertron-utils/isEmpty';
import Card from 'leaf-ui/Card/web';
import Text from 'leaf-ui/Text/web';
import Price from 'leaf-ui/Price/web';
import Spacer from 'leaf-ui/Spacer/web';
import Flex from 'leaf-ui/Flex/web';
import Divider from 'leaf-ui/Divider/web';
import Button from 'leaf-ui/Button/web';
import Form from 'leaf-ui/Form/web';
import * as tabQueries from '../../tab/tabQueries';
import * as tabMutations from '../../tab/tabMutations';

class PriceSummary extends React.Component {
  state = {
    preTax: 0,
    tax: 0,
    total: 0,
  };

  componentWillReceiveProps() {
    // console.log(nextProps);
    this.priceCalculation();
  }

  fetchRoomCount = () => {
    const { rows } = this.props;
    return Object.values(rows).reduce((a, b) => a + b, 0);
  }

  fetchRoomTypes = () => {
    const { rows } = this.props;
    const types = Object.keys(rows).join(',');
    return types;
  }

  openEditBookingOwnerInfo = () => {
    const {
      $selectedTabIndex,
      addFlow,
      values,
      totalPreTaxPrice,
      totalTax,
      totalPayable,
    } = this.props;
    addFlow({
      variables: {
        tabIndex: $selectedTabIndex.selectedTabIndex,
        flow: {
          component: 'EditBookingOwner',
          props: {
            values,
            totalPreTaxPrice,
            totalTax,
            totalPayable,
          },
        },
      },
    });
  }

  priceCalculation = () => {
    const { roomTypes, rows } = this.props;
    let totalPreTax = 0;
    let totalTax = 0;
    let totalPayable = 0;

    Object.keys(rows).map((type) => {
      const selectedPrice = !isEmpty(roomTypes) ?
        roomTypes.find((roomType) => roomType.name === type).price.ratePlans[0]
        : { preTax: 0, tax: 0 };
      totalPreTax = selectedPrice.preTax * rows[type];
      totalTax = selectedPrice.tax * rows[type];
      totalPayable = totalPreTax + totalTax;
      return type;
    });
    this.setState({
      preTax: totalPreTax,
      tax: totalTax,
      total: totalPayable,
    });
  }

  render() {
    const {
      className,
      values,
      isValid,
    } = this.props;

    return (
      <Spacer padding={[5]} className={className}>
        <Card>
          <Form.Form>
            <Flex flexDirection="column">
              <Flex justifyContent="space-between">
                <Spacer margin={[0, 0, 3, 0]}>
                  <Flex flexDirection="column" justifyContent="space-between">
                    <Text size="xs" color="greyDark">Check-in</Text>
                    <Spacer margin={[1, 0, 0, 0]}>
                      <Text size="m" color="greyDarker">{values.checkInDate}</Text>
                    </Spacer>
                  </Flex>
                </Spacer>
                <Spacer margin={[0, 0, 3, 0]}>
                  <Flex flexDirection="column" justifyContent="space-between">
                    <Text size="xs" color="greyDark">Check-out</Text>
                    <Spacer margin={[1, 0, 0, 0]}>
                      <Text size="m" color="greyDarker">{values.checkOutDate}</Text>
                    </Spacer>
                  </Flex>
                </Spacer>
              </Flex>
              <Flex justifyContent="space-between">
                <Spacer margin={[0, 0, 3, 0]}>
                  <Flex flexDirection="column" justifyContent="space-between">
                    <Text size="xs" color="greyDark">No of Rooms</Text>
                    <Spacer margin={[1, 0, 0, 0]}>
                      <Text size="m" color="greyDarker">{this.fetchRoomCount()}</Text>
                    </Spacer>
                  </Flex>
                </Spacer>
                <Spacer margin={[0, 0, 3, 0]}>
                  <Flex flexDirection="column" justifyContent="space-between">
                    <Text size="xs" color="greyDark">Room Type</Text>
                    <Spacer margin={[1, 0, 0, 0]}>
                      <Text size="m" color="greyDarker">{this.fetchRoomTypes()}</Text>
                    </Spacer>
                  </Flex>
                </Spacer>
              </Flex>
            </Flex>
            <Spacer margin={[0, 0, 3, 0]}>
              <Divider type="dashed" />
            </Spacer>
            <Flex flexDirection="column">
              <Spacer margin={[0, 0, 3, 0]}>
                <Flex justifyContent="space-between">
                  <Text color="greyDark">Total Cost for 1 Night</Text>
                  <Text color="greyDarker"><Price>{this.state.preTax}</Price></Text>
                </Flex>
              </Spacer>
              <Spacer margin={[0, 0, 3, 0]}>
                <Flex justifyContent="space-between">
                  <Text color="greyDark">Taxes</Text>
                  <Text color="greyDarker"><Price>{this.state.tax}</Price></Text>
                </Flex>
              </Spacer>
            </Flex>
            <Spacer margin={[0, 0, 3, 0]}>
              <Divider type="dashed" />
            </Spacer>
            <Flex justifyContent="space-between">
              <Spacer margin={[0, 3, 3, 0]}>
                <Flex flexDirection="column">
                  <Spacer margin={[0, 0, 1, 0]}>
                    <Text size="xl" weight="bold">Total Payable</Text>
                  </Spacer>
                  <Text size="xs" color="rock">Incl. of all taxes</Text>
                </Flex>
              </Spacer>
              <Text size="xxl" weight="bold"><Price>{this.state.total}</Price></Text>
            </Flex>
            <Button
              block
              disabled={!isValid}
              onClick={this.openEditBookingOwnerInfo}
            >
              CONTINUE TO CONFIRM
            </Button>
          </Form.Form>
        </Card>
      </Spacer>
    );
  }
}

PriceSummary.propTypes = {
  className: PropTypes.string,
  values: PropTypes.object,
  roomTypes: PropTypes.object,
  rows: PropTypes.object,
  isValid: PropTypes.bool,
  totalPreTaxPrice: PropTypes.number,
  totalTax: PropTypes.number,
  totalPayable: PropTypes.number,
  $selectedTabIndex: PropTypes.object,
  addFlow: PropTypes.func,
};

export default compose(
  graphql(tabQueries.SELECTED_TAB_INDEX, { name: '$selectedTabIndex' }),
  graphql(tabMutations.ADD_FLOW, { name: 'addFlow' }),
)(PriceSummary);
