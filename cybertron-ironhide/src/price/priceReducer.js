import { handle } from 'redux-pack';
import * as priceActionTypes from './priceActionTypes';

export const initialState = {
  byRoomType: {
    acacia: {
      EP: {
        // '10/12/2017': {
        //   basePrice: 123,
        //   sellingPrice: 123,
        //   taxPrice: 123,
        //   discountPrice: 100,
        //   discountPercentage: 23,
        // },
      },
    },
    oak: {
      EP: {},
    },
    maple: {
      EP: {},
    },
    mahogany: {
      EP: {},
    },
  },
  isLoading: false,
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case priceActionTypes.GET_ALL: return handle(state, action, {
      start: (s) => ({
        ...s,
        isLoading: true,
      }),
      failure: (s) => ({
        ...s,
        error: payload.error,
      }),
      finish: (s) => ({
        ...s,
        isLoading: false,
      }),
    });

    default:
      return state;
  }
};
