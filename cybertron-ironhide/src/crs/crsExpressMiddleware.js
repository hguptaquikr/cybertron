import express from 'express';
import proxy from 'http-proxy-middleware';
import config from '../config';
import mockBookings from './mockBookings';

const router = express.Router();

router.post('/auth', (req, res) => {
  res.send({
    user: {
      id: '007',
      hotelIds: ['H1', 'H2'],
      name: {
        first: 'James',
        last: 'Bond',
        middle: 'Herbert',
      },
      mobile: '+917007007007',
      permissions: [],
    },
    token: '108rf89018fdoa01rlfj0',
  });
});
router.get('/v1/hotel/H1/inventory/', (req, res) => {
  res.send({
    hotelId: 'H1',
    roomTypes: ['oak', 'maple'],
    rooms: [{
      id: 'R101',
      number: '101',
      type: 'oak',
    }, {
      id: 'R102',
      number: '202',
      type: 'oak',
    }, {
      id: 'R103',
      number: '103',
      type: 'oak',
    }, {
      id: 'R104',
      number: '104',
      type: 'oak',
    }, {
      id: 'R105',
      number: '105',
      type: 'oak',
    }, {
      id: 'R106',
      number: '106',
      type: 'oak',
    }, {
      id: 'R107',
      number: '107',
      type: 'oak',
    }, {
      id: 'R108',
      number: '108',
      type: 'oak',
    }, {
      id: 'R109',
      number: '109',
      type: 'oak',
    }, {
      id: 'R110',
      number: '110',
      type: 'oak',
    }, {
      id: 'R111',
      number: '111',
      type: 'oak',
    }, {
      id: 'R112',
      number: '112',
      type: 'oak',
    }, {
      id: 'R113',
      number: '113',
      type: 'oak',
    }, {
      id: 'R114',
      number: '114',
      type: 'oak',
    }, {
      id: 'R115',
      number: '115',
      type: 'oak',
    }, {
      id: 'R116',
      number: '116',
      type: 'oak',
    }, {
      id: 'R201',
      number: '201',
      type: 'maple',
    }, {
      id: 'R202',
      number: '202',
      type: 'maple',
    }, {
      id: 'R203',
      number: '203',
      type: 'maple',
    }, {
      id: 'R204',
      number: '204',
      type: 'maple',
    }, {
      id: 'R205',
      number: '205',
      type: 'maple',
    }],
  });
});

router.get('/v1/booking/:bookingId/', (req, res) => {
  res.send(mockBookings[req.params.bookingId]);
});

router.get('/v1/hotel/:hotelId/', (req, res) => {
  res.send([{
    id: req.params.hotelId,
    bookingIds: ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8'],
    roomTypeIds: ['oak', 'maple'],
    name: 'Treebo Akshaya Lalbagh Inn',
    coordinates: {
      lat: 12.943555,
      lng: 77.583549,
    },
    images: [
      {
        id: 10782,
        url: '//images.treebohotels.com/files/./Treebo_Akshaya_Lalbagh_Inn/Akshaya_Lalbagh_Oak_Queen_03.jpg',
        is_showcased: true,
        tagline: 'Treebo Akshaya Lalbagh Inn',
        main: './Treebo_Akshaya_Lalbagh_Inn/Akshaya_Lalbagh_Oak_Queen_03.jpg',
      },
      {
        id: 4670,
        url: '//images.treebohotels.com/files/./Treebo_Akshaya_Lalbagh_Inn/1Treebo_Akshaya_Lalbagh_Main.jpg',
        is_showcased: false,
        tagline: 'Treebo Akshaya Lalbagh Inn',
        main: './Treebo_Akshaya_Lalbagh_Inn/1Treebo_Akshaya_Lalbagh_Main.jpg',
      },
    ],
    area: {
      locality: 'Jayanagar',
      city: 'Bengaluru',
      country: 'India',
      pincode: 560070,
      'state:': 'Karnataka',
    },
  }]);
});

router.get('/v1/hotel/', (req, res) => {
  res.send({ name: 'Treebo Elmas' });
});

router.use(proxy({
  target: config.crsUrl,
  pathRewrite: { '^/api/crs': '/api' },
  changeOrigin: true,
}));

export default router;
