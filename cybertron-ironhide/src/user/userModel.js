export const getName = (user) => `${user.name.first} ${user.name.last}`;
export const getFullName = (user) => `${user.name.first} ${user.name.middle} ${user.name.last}`;
