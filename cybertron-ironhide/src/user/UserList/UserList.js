import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import Flex from 'leaf-ui/Flex/web';
import Text from 'leaf-ui/Text/web';
import Card from 'leaf-ui/Card/web';
import * as userActionCreators from '../../user/userActionCreators';
import * as userModel from '../../user/userModel';

class UserList extends React.Component {
  componentDidMount() {
    const { userActions } = this.props;
    userActions.getAll();
  }

  render() {
    const { users } = this.props;

    return users.length ? (
      <Flex justifyContent="space-around">
        {
          users.map((user) => (
            <Card key={user.name.first} elevation={1}>
              <Text size="xl">{user.name.first}</Text>
            </Card>
          ))
        }
      </Flex>
    ) : null;
  }
}

UserList.propTypes = {
  users: PropTypes.array.isRequired,
  userActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  users: userModel.getAll(state.$user.ids, state.$user.byId),
});

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(UserList);
