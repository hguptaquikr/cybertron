export default {
  analyticsBlacklistedIps: [],
  hostUrl: 'https://frontdesk.treebo.pr',
  crsUrl: 'https://crs.treebo.pr',
  pricingUrl: 'https://pricing.treebo.pr',
};
