export default {
  analyticsBlacklistedIps: [],
  hostUrl: 'http://localhost:8000',
  crsUrl: 'https://crs.treebo.pr',
  pricingUrl: 'https://pricing.treebo.pr',
};
