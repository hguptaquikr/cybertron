export default {
  analyticsBlacklistedIps: [],
  hostUrl: 'https://frontdesk.staging.treebo.pr',
  crsUrl: 'https://crs.staging.treebo.pr',
  pricingUrl: 'https://pricing.staging.treebo.pr',
};
