export default {
  analyticsBlacklistedIps: [],
  hostUrl: 'https://frontdesk.dev.treebo.pr',
  crsUrl: 'https://crs.dev.treebo.pr',
  pricingUrl: 'https://pricing.dev.treebo.pr',
};
