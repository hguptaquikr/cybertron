import startCase from 'cybertron-utils/startCase';

export const makeFlow = (flow) => ({
  component: flow.component,
  props: {
    ...flow.props,
    __typename: 'JSON',
  },
  __typename: 'Flow',
});

export const makeTab = (tab) => ({
  pinned: !!tab.pinned,
  flows: (tab.flows || []).map(makeFlow),
  __typename: 'Tab',
});

export const makeTabName = (tab) => {
  const [primaryFlow] = tab.flows;
  const tabName = primaryFlow ? primaryFlow.component : 'Tab';
  return startCase(tabName);
};

export const findSameTabIndex = (tabs, newTab) => {
  const [newPrimaryFlow] = newTab.flows;
  return tabs.findIndex((tab) => {
    const [primaryFlow] = tab.flows;
    if (primaryFlow.component === newPrimaryFlow.component) {
      switch (primaryFlow.component) {
        case 'HouseView':
        case 'NewBooking':
        case 'Search':
          return true;
        case 'BookingDetails':
          return primaryFlow.props.bookingId === newPrimaryFlow.props.bookingId;
        default:
          return false;
      }
    }
    return false;
  });
};

export const makeFlowId = ({ tabIndex, flowIndex }) =>
  document.getElementById(`tab-${tabIndex}-flow-${flowIndex}`);
