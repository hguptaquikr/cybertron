import gql from 'graphql-tag';
import * as tabHelpers from './tabHelpers';

export const SELECT_TAB = gql`
  mutation selectTab($tabIndex: String!) {
    selectTab(tabIndex: $tabIndex) @client
  }
`;

export const ADD_TAB = gql`
  mutation addTab($tab: Tab!) {
    addTab(tab: $tab) @client
  }
`;

export const REMOVE_TAB = gql`
  mutation removeTab($tabIndex: String!) {
    removeTab(tabIndex: $tabIndex) @client
  }
`;

export const ADD_FLOW = gql`
  mutation addFlow($tabIndex: String!, $flow: Flow!) {
    addFlow(tabIndex: $tabIndex, flow: $flow) @client
  }
`;

export const REMOVE_FLOW = gql`
  mutation removeFlow($tabIndex: String!, $flowIndex: String!) {
    removeFlow(tabIndex: $tabIndex, flowIndex: $flowIndex) @client
  }
`;

const typeDefs = `
  extend type Mutation {
    selectTab(tabIndex: String!)
    addTab(tab: Tab!)
    removeTab(tabIndex: String!)
    addFlow(tabIndex: String!, flow: Flow!)
    removeFlow(tabIndex: String!, flowIndex: String!)
  }
`;

const resolvers = {
  Mutation: {

    selectTab: (_, args, ctx) => {
      const query = gql`
        query {
          selectedTabIndex @client
        }
      `;
      const prevState = ctx.cache.readQuery({ query });
      const nextState = {
        ...prevState,
        selectedTabIndex: args.tabIndex,
      };
      ctx.cache.writeQuery({ query, data: nextState });
      return null;
    },

    addTab: (_, args, ctx) => {
      const query = gql`
        query {
          tabs @client {
            pinned
            flows {
              component
              props
            }
          }
          selectedTabIndex @client
        }
      `;
      const prevState = ctx.cache.readQuery({ query });
      const sameTabIndex = tabHelpers.findSameTabIndex(prevState.tabs, args.tab);
      let nextState = {};
      if (sameTabIndex === -1) {
        nextState = {
          ...prevState,
          tabs: [
            ...prevState.tabs,
            tabHelpers.makeTab(args.tab),
          ],
          selectedTabIndex: prevState.tabs.length,
        };
      } else {
        nextState = {
          ...prevState,
          selectedTabIndex: sameTabIndex,
        };
      }
      ctx.cache.writeQuery({ query, data: nextState });
      return null;
    },

    removeTab: (_, args, ctx) => {
      const query = gql`
        query {
          tabs @client {
            pinned
            flows {
              component
              props
            }
          }
          selectedTabIndex @client
        }
      `;
      const prevState = ctx.cache.readQuery({ query });
      const nextState = {
        ...prevState,
        tabs: prevState.tabs.filter((tab, tabIndex) => tabIndex !== args.tabIndex),
        selectedTabIndex: args.tabIndex - 1 >= 0 ? args.tabIndex - 1 : 0,
      };
      ctx.cache.writeQuery({ query, data: nextState });
      return null;
    },

    addFlow: (_, args, ctx) => {
      const query = gql`
        query {
          tabs @client {
            pinned
            flows {
              component
              props
            }
          }
        }
      `;
      const prevState = ctx.cache.readQuery({ query });
      const nextState = {
        ...prevState,
        tabs: prevState.tabs.map((tab, tabIndex) => {
          if (tabIndex === args.tabIndex) {
            return {
              ...tab,
              flows: [
                ...tab.flows,
                tabHelpers.makeFlow(args.flow),
              ],
            };
          }
          return tab;
        }),
      };
      ctx.cache.writeQuery({ query, data: nextState });
      return null;
    },

    removeFlow: (_, args, ctx) => {
      const query = gql`
        query {
          tabs @client {
            pinned
            flows {
              component
            }
          }
        }
      `;
      const prevState = ctx.cache.readQuery({ query });
      const nextState = {
        ...prevState,
        tabs: prevState.tabs.map((tab, tabIndex) => {
          if (tabIndex === args.tabIndex) {
            return {
              ...tab,
              flows: tab.flows.filter((flow, flowIndex) => flowIndex !== args.flowIndex),
            };
          }
          return tab;
        }),
      };
      ctx.cache.writeQuery({ query, data: nextState });
      return null;
    },

  },
};

export default {
  typeDefs,
  resolvers,
};
