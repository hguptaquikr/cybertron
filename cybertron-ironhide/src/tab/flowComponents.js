import AddGuest from '../guest/AddGuest/AddGuest';
import BookingDetails from '../booking/BookingDetails/BookingDetails';
import CancelBooking from '../booking/CancelBooking/CancelBooking';
import CancelCharge from '../charge/CancelCharge/CancelCharge';
import ChangeRoom from '../room/ChangeRoom/ChangeRoom';
import EditBookingOwner from '../booking/EditBookingOwner/EditBookingOwner';
import HouseView from '../houseView/HouseView';
import QuickView from '../houseView/QuickView/QuickView';
import NewBooking from '../booking/NewBooking/NewBooking';
import RecordPayment from '../payment/RecordPayment/RecordPayment';
import RemoveGuest from '../guest/RemoveGuest/RemoveGuest';
import RemoveRoom from '../room/RemoveRoom/RemoveRoom';
import Search from '../search/Search/Search';
import SelectHotel from '../hotel/SelectHotel/SelectHotel';
import TransactionDetails from '../booking/TransactionDetails/TransactionDetails';

const flowComponents = {
  AddGuest,
  BookingDetails,
  CancelBooking,
  CancelCharge,
  ChangeRoom,
  EditBookingOwner,
  HouseView,
  QuickView,
  NewBooking,
  RecordPayment,
  RemoveGuest,
  RemoveRoom,
  Search,
  SelectHotel,
  TransactionDetails,
};

export default flowComponents;
