import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { graphql, compose } from 'react-apollo';
import Flex from 'leaf-ui/Flex/web';
import flowComponents from '../flowComponents';
import * as tabQueries from '../tabQueries';
import * as tabHelpers from '../tabHelpers';

const TabContentContainer = Flex.extend`
  flex: 1;
  overflow: auto;
  position: relative;
  display: ${(props) => props.isSelected ? 'block' : 'none'};
`;

const FlowContentContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

class TabContent extends React.Component {
  state = {}

  render() {
    const { $tabs, $selectedTabIndex } = this.props;
    return (
      <React.Fragment>
        {
          $tabs.tabs.map((tab, tabIndex) => (
            <TabContentContainer
              key={tabHelpers.makeTabName(tab)}
              isSelected={tabIndex === $selectedTabIndex.selectedTabIndex}
            >
              {
                tab.flows.map((flow, flowIndex) => (
                  <FlowContentContainer id={`tab-${tabIndex}-flow-${flowIndex}`}>
                    {
                      React.createElement(
                        flowComponents[flow.component],
                        {
                          tabIndex,
                          flowIndex,
                          ...flow.props,
                        },
                      )
                    }
                  </FlowContentContainer>
                ))
              }
            </TabContentContainer>
          ))
        }
      </React.Fragment>
    );
  }
}

TabContent.propTypes = {
  $tabs: PropTypes.object.isRequired,
  $selectedTabIndex: PropTypes.object.isRequired,
};

export default compose(
  graphql(tabQueries.TABS, { name: '$tabs' }),
  graphql(tabQueries.SELECTED_TAB_INDEX, { name: '$selectedTabIndex' }),
)(TabContent);
