import gql from 'graphql-tag';

export const TABS = gql`
  query tabs {
    tabs @client {
      pinned
      flows {
        component
        props
      }
    }
  }
`;

export const SELECTED_TAB_INDEX = gql`
  query selectedTabIndex {
    selectedTabIndex @client
  }
`;

const resolvers = {
  Query: {},
};

export default {
  resolvers,
};
