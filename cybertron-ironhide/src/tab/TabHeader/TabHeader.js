import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import Flex from 'leaf-ui/Flex/web';
import Spacer from 'leaf-ui/Spacer/web';
import Card from 'leaf-ui/Card/web';
import Text from 'leaf-ui/Text/web';
import Icon from 'leaf-ui/Icon/web';
import * as tabHelpers from '../tabHelpers';
import * as tabQueries from '../tabQueries';
import * as tabMutations from '../tabMutations';

class TabHeader extends React.Component {
  onTabClick = (tabIndex) => () => {
    const { $selectedTabIndex, selectTab } = this.props;
    if (tabIndex !== $selectedTabIndex.selectedTabIndex) {
      selectTab({ variables: { tabIndex } });
    }
  }

  onTabClose = (tabIndex) => (e) => {
    const { removeTab } = this.props;
    e.stopPropagation();
    removeTab({ variables: { tabIndex } });
  }

  render() {
    const { $tabs, $selectedTabIndex } = this.props;
    return (
      <Flex>
        {
          $tabs.tabs.map((tab, tabIndex) => (
            <Spacer
              key={tabHelpers.makeTabName(tab)}
              margin={[0, 1, 0, 0]}
              padding={[1]}
              onClick={this.onTabClick(tabIndex)}
            >
              <Card color={tabIndex === $selectedTabIndex.selectedTabIndex ? 'white' : 'greyLight'} style={{ cursor: 'default' }}>
                <Flex>
                  {
                    tab.pinned ? (
                      <Text>{tabHelpers.makeTabName(tab)}</Text>
                    ) : (
                      <Spacer margin={[0, 0, 0, 1]}>
                        <Icon name="close" right onClick={this.onTabClose(tabIndex)}>
                          <Text>{tabHelpers.makeTabName(tab)}</Text>
                        </Icon>
                      </Spacer>
                    )
                  }
                </Flex>
              </Card>
            </Spacer>
          ))
        }
      </Flex>
    );
  }
}

TabHeader.propTypes = {
  $tabs: PropTypes.object.isRequired,
  $selectedTabIndex: PropTypes.object.isRequired,
  selectTab: PropTypes.func.isRequired,
  removeTab: PropTypes.func.isRequired,
};

export default compose(
  graphql(tabQueries.TABS, { name: '$tabs' }),
  graphql(tabQueries.SELECTED_TAB_INDEX, { name: '$selectedTabIndex' }),
  graphql(tabMutations.SELECT_TAB, { name: 'selectTab' }),
  graphql(tabMutations.REMOVE_TAB, { name: 'removeTab' }),
)(TabHeader);
