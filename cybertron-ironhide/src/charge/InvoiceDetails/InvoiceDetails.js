import React from 'react';
import PropTypes from 'prop-types';
import Flex from 'leaf-ui/Flex/web';
import Card from 'leaf-ui/Card/web';
import Text from 'leaf-ui/Text/web';
import Spacer from 'leaf-ui/Spacer/web';
import Table from 'leaf-ui/Table/web';

class InvoiceDetails extends React.Component {
  state = {};

  render() {
    const { className } = this.props;
    return (
      <Spacer padding={[4]} className={className}>
        <Card>
          <Flex flexDirection="column">
            <Spacer padding={[0, 0, 4, 0]}>
              <Text size="xxl" weight="bold">Invoices</Text>
            </Spacer>
            <Table>
              <Table.TBody>
                <Table.TR>
                  <Table.TH>Invoice No</Table.TH>
                  <Table.TH>Billed To</Table.TH>
                  <Table.TH>Generated Date</Table.TH>
                  <Table.TH>Invoice Type</Table.TH>
                  <Table.TH>Amount</Table.TH>
                </Table.TR>
                <Table.TR>
                  <Table.TD>1000</Table.TD>
                  <Table.TD>2</Table.TD>
                  <Table.TD>Pragya</Table.TD>
                  <Table.TD>10/11/2018</Table.TD>
                  <Table.TD>oak</Table.TD>
                </Table.TR>
              </Table.TBody>
            </Table>
          </Flex>
        </Card>
      </Spacer>
    );
  }
}

InvoiceDetails.propTypes = {
  className: PropTypes.string,
};

export default InvoiceDetails;
