const chargeProto = {
  get introduce() {
    return `Hey, my name is ${this.name}`;
  },
};

export const makeCharge = (name) => {
  const charge = Object.create(chargeProto);
  charge.name = name;
  return charge;
};

export const normalize = (charges) => ({
  byId: charges.reduce((obj, charge, index) => ({ ...obj, [index]: charge }), {}),
  results: charges.map((charge, index) => index),
});

export const getAll = (results, byId) =>
  results.map((chargeId) => byId[chargeId]);
