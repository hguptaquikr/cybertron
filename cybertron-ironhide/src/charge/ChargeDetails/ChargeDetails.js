import React from 'react';
import { graphql, compose } from 'react-apollo';
import PropTypes from 'prop-types';
import Flex from 'leaf-ui/Flex/web';
import Card from 'leaf-ui/Card/web';
import Text from 'leaf-ui/Text/web';
import Spacer from 'leaf-ui/Spacer/web';
import Price from 'leaf-ui/Price/web';
import Dropdown from 'leaf-ui/Dropdown/web';
import List from 'leaf-ui/List/web';
import Icon from 'leaf-ui/Icon/web';
import Table from 'leaf-ui/Table/web';
import * as tabQueries from '../../tab/tabQueries';
import * as tabMutations from '../../tab/tabMutations';

class ChargeDetails extends React.Component {
  openCancelCharge = () => {
    const { $selectedTabIndex, addFlow } = this.props;
    addFlow({
      variables: {
        tabIndex: $selectedTabIndex.selectedTabIndex,
        flow: {
          component: 'CancelCharge',
        },
      },
    });
  }

  render() {
    const { className } = this.props;
    return (
      <Spacer className={className} padding={[4]}>
        <Card>
          <Flex flexDirection="column">
            <Spacer padding={[0, 0, 4, 0]}>
              <Text size="xxl" weight="bold">Charges</Text>
            </Spacer>
            <Table>
              <Table.TBody>
                <Table.TR>
                  <Table.TH>Charge</Table.TH>
                  <Table.TH>Room</Table.TH>
                  <Table.TH>Charged To</Table.TH>
                  <Table.TH>Date</Table.TH>
                  <Table.TH>Type</Table.TH>
                  <Table.TH>Status</Table.TH>
                  <Table.TH>
                    <Spacer margin={[0, 0, 0, 3]}>
                      <Flex flexDirection="row">
                        <Text>Total Amount</Text>
                        <Text color="grey">(Inc. Taxes)</Text>
                      </Flex>
                    </Spacer>
                  </Table.TH>
                  <Table.TH />
                </Table.TR>
                <Table.TR>
                  <Table.TD>1000</Table.TD>
                  <Table.TD>2</Table.TD>
                  <Table.TD>Pragya</Table.TD>
                  <Table.TD>10/11/2018</Table.TD>
                  <Table.TD>oak</Table.TD>
                  <Table.TD>reserved</Table.TD>
                  <Table.TD>2000</Table.TD>
                  <Table.TD>
                    <Dropdown placement="bottom">
                      <Dropdown.Trigger>
                        <Icon name="more_horiz" />
                      </Dropdown.Trigger>
                      <Dropdown.Content>
                        <List>
                          <List.Item
                            onClick={this.openCancelCharge}
                          >
                            Cancel Charge
                          </List.Item>
                        </List>
                      </Dropdown.Content>
                    </Dropdown>
                  </Table.TD>
                </Table.TR>
              </Table.TBody>
            </Table>
            <Flex alignSelf="flex-end">
              <Card>
                <Flex flexDirection="column">
                  <Spacer margin={[4, 0, 3, 0]}>
                    <Flex justifyContent="space-between">
                      <Text color="green">Package Discount (30%)</Text>
                      <Text color="green">-<Price>200</Price></Text>
                    </Flex>
                  </Spacer>
                  <Spacer margin={[0, 0, 3, 0]}>
                    <Flex justifyContent="space-between">
                      <Text color="slate">Advance</Text>
                      <Text color="slate"><Price>300</Price></Text>
                    </Flex>
                  </Spacer>
                </Flex>
                <Flex justifyContent="space-between">
                  <Spacer margin={[0, 3, 0, 0]}>
                    <Flex flexDirection="column">
                      <Spacer margin={[0, 0, 1, 0]}>
                        <Text size="xl" weight="bold">Total Payable</Text>
                      </Spacer>
                      <Text size="xs" color="rock">Incl. of all taxes</Text>
                    </Flex>
                  </Spacer>
                  <Text size="xxl" weight="bold"><Price>500</Price></Text>
                </Flex>
              </Card>
            </Flex>
          </Flex>
        </Card>
      </Spacer>
    );
  }
}

ChargeDetails.propTypes = {
  className: PropTypes.string,
  $selectedTabIndex: PropTypes.object,
  addFlow: PropTypes.func.isRequired,
};

export default compose(
  graphql(tabQueries.SELECTED_TAB_INDEX, { name: '$selectedTabIndex' }),
  graphql(tabMutations.ADD_FLOW, { name: 'addFlow' }),
)(ChargeDetails);
