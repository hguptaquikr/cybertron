import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import Flex from 'leaf-ui/Flex/web';
import Form from 'leaf-ui/Form/web';
import Button from 'leaf-ui/Button/web';
import TextInput from 'leaf-ui/TextInput/web';
import Text from 'leaf-ui/Text/web';
import Modal from 'leaf-ui/Modal/web';
import Spacer from 'leaf-ui/Spacer/web';
import * as tabMutations from '../../tab/tabMutations';

class CancelCharge extends React.Component {
  closeCancelCharge = () => {
    const { tabIndex, flowIndex, removeFlow } = this.props;
    removeFlow({
      variables: {
        tabIndex,
        flowIndex,
      },
    });
  }

  confirmCancelCharge = () => {
    setTimeout(this.closeCancelCharge, 1000);
  }

  render() {
    return (
      <Modal
        isOpen
        onClose={this.closeCancelCharge}
      >
        <Form
          onSubmit={this.confirmCancelCharge}
        >
          <Form.Form>
            <Modal.Header>
              <Text weight="bold" size="l">
                Are you sure to cancel this charge?
              </Text>
            </Modal.Header>
            <Modal.Content>
              <Spacer margin={[2, 1, 5, 0]}>
                <TextInput
                  name="comment"
                  label="enter your comments"
                />
              </Spacer>
            </Modal.Content>
            <Modal.Footer>
              <Flex flexDirection="row-reverse">
                <Button
                  size="small"
                  type="submit"
                >
                  CANCEL CHARGE
                </Button>
              </Flex>
            </Modal.Footer>
          </Form.Form>
        </Form>
      </Modal>
    );
  }
}

CancelCharge.propTypes = {
  tabIndex: PropTypes.string,
  flowIndex: PropTypes.string,
  removeFlow: PropTypes.func.isRequired,
};

export default compose(
  graphql(tabMutations.REMOVE_FLOW, { name: 'removeFlow' }),
)(CancelCharge);
