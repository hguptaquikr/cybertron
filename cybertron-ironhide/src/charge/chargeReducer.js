import { handle } from 'redux-pack';
import * as chargeTypes from './chargeActionTypes';

export const initialState = {
  byId: {
    // c123: {
    //   id: 'c123',
    //   name: '',
    //   price: 0,
    //   bookingId: 0,
    //   roomId: 0,
    //   guestId: 0,
    // },
  },
  byBookingId: {
    // 'b123': [c123],
  },
  byRoomId: {
    // 'r123': [c234],
  },
  byGuestId: {
    // 'g123': [c345],
  },
  isLoading: false,
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case chargeTypes.GET_ALL: return handle(state, action, {
      start: (s) => ({
        ...s,
        isLoading: true,
      }),
      failure: (s) => ({
        ...s,
        error: payload.error,
      }),
      finish: (s) => ({
        ...s,
        isLoading: false,
      }),
    });

    default:
      return state;
  }
};
