import React from 'react';
import PropTypes from 'prop-types';
import { compose, graphql } from 'react-apollo';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import performance from 'cybertron-utils/performance';
import isEmpty from 'cybertron-utils/isEmpty';
import Flex from 'leaf-ui/Flex/web';
import Spacer from 'leaf-ui/Spacer/web';
import * as authActionCreators from 'cybertron-service-auth/lib/authActionCreators';
import * as hotelActionCreators from '../../hotel/hotelActionCreators';
import Sidebar from '../../navigation/Sidebar/Sidebar';
import TabHeader from '../../tab/TabHeader/TabHeader';
import TabContent from '../../tab/TabContent/TabContent';
import SelectHotel from '../../hotel/SelectHotel/SelectHotel';
import * as tabQueries from '../../tab/tabQueries';
import Login from '../../auth/Login/Login';
import theme from '../theme';

class Wrapper extends React.Component {
  componentDidMount() {
    performance.mark('first-interaction');

    // remove this when activating login flow
    // this.onLoginSuccess({
    //   user: {
    //     id: '007',
    //     hotelIds: ['H1', 'H2', 'H3'],
    //     name: {
    //       first: 'James',
    //       last: 'Bond',
    //       middle: 'Herbert',
    //     },
    //     mobile: '+917007007007',
    //     permissions: [],
    //   },
    //   token: '108rf89018fdoa01rlfj0',
    // });
  }

  onLoginSuccess = (res) => {
    const {
      authActions,
      hotelActions,
    } = this.props;

    const fetchHotelsPromise = res.user.hotelIds
      .map((hotelId) => hotelActions.fetchHotels(hotelId));
    Promise.all(fetchHotelsPromise).then(() => {
      authActions.setToken(res.token);
      authActions.setUser(res.user);
    });
  }

  renderMainContent = () => {
    const { $selectedTabIndex } = this.props;
    const { selectedTabIndex } = $selectedTabIndex;
    if (selectedTabIndex === -1) {
      return (
        <SelectHotel />
      );
    }
    return (
      <Spacer height="100vh">
        <Flex>
          <Sidebar />
          <Flex flex="1" flexDirection="column">
            <TabHeader />
            <TabContent />
          </Flex>
        </Flex>
      </Spacer>
    );
  }

  render() {
    const { $auth } = this.props;
    return (
      <ThemeProvider theme={theme}>
        {
          isEmpty($auth.token) ? (
            <Login
              onLoginSuccess={this.onLoginSuccess}
            />
          ) : (
            this.renderMainContent()
          )
        }
      </ThemeProvider>
    );
  }
}

Wrapper.propTypes = {
  $auth: PropTypes.object.isRequired,
  $selectedTabIndex: PropTypes.object.isRequired,
  authActions: PropTypes.object.isRequired,
  hotelActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  $auth: state.$auth,
});

const mapDispatchToProps = (dispatch) => ({
  authActions: bindActionCreators(authActionCreators, dispatch),
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
});

export default compose(
  graphql(tabQueries.SELECTED_TAB_INDEX, { name: '$selectedTabIndex' }),
  connect(mapStateToProps, mapDispatchToProps),
)(Wrapper);
