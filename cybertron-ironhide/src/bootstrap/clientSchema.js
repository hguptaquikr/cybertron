import deepMerge from 'cybertron-utils/deepMerge';
import tabQueries from '../tab/tabQueries';
import tabMutations from '../tab/tabMutations';

const defaults = {
  tabs: [
    // {
    //   pinned: true,
    //   flows: [{
    //     component: 'SelectHotel',
    //     props: {
    //       __typename: 'JSON',
    //     },
    //     __typename: 'Flow',
    //   }],
    //   __typename: 'Tab',
    // }
  ],
  selectedTabIndex: -1,
};

const typeDefs = `
  ${tabQueries.typeDefs}
  ${tabMutations.typeDefs}
`;

const resolvers = deepMerge(
  tabQueries.resolvers,
  tabMutations.resolvers,
);

export default {
  defaults,
  typeDefs,
  resolvers,
};
