import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink, split } from 'apollo-link';
import { onError } from 'apollo-link-error';
import { withClientState } from 'apollo-link-state';
import { createHttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import clientSchema from './clientSchema';

const cache = new InMemoryCache();

const queryAndMutationLink = ApolloLink.from([
  onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
      graphQLErrors.map(({ message, locations, path }) =>
        console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`));
    }
    if (networkError) {
      console.log(`[Network error]: ${networkError}`);
    }
  }),

  withClientState({
    cache,
    defaults: clientSchema.defaults,
    typeDefs: clientSchema.typeDefs,
    resolvers: clientSchema.resolvers,
  }),

  createHttpLink({
    uri: 'http://localhost:8000/graphql/',
    credentials: 'same-origin',
  }),
]);

const subscriptionLink = new WebSocketLink({
  uri: 'ws://localhost:8000/subscriptions/',
  options: {
    reconnect: true,
  },
});

const apolloClient = new ApolloClient({
  cache,
  link: split(
    ({ query }) => {
      const { kind, operation } = getMainDefinition(query);
      return kind === 'OperationDefinition' && operation === 'subscription';
    },
    subscriptionLink,
    queryAndMutationLink,
  ),
});

// apolloClient.onResetStore(stateLink.writeDefaults);

export default apolloClient;
