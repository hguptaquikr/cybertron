import { createHttpLink } from 'apollo-link-http';
import { introspectSchema, makeRemoteExecutableSchema, mergeSchemas } from 'graphql-tools';
import deepMerge from 'cybertron-utils/deepMerge';
import bookingCreated from '../booking/bookingCreated';
import hotelUpdated from '../hotel/hotelUpdated';

const httpLink = createHttpLink({
  uri: 'http://localhost:8888/graphql/',
  fetch,
});

const getServerSchema = async () => {
  const scavengerSchema = makeRemoteExecutableSchema({
    schema: await introspectSchema(httpLink),
    link: httpLink,
  });

  const ironhideSchema = {
    typeDefs: `
      type Subscription
      ${bookingCreated.typeDefs}
      ${hotelUpdated.typeDefs}
    `,
    resolvers: deepMerge(
      bookingCreated.resolvers,
      hotelUpdated.resolvers,
    ),
  };

  const schema = mergeSchemas({
    schemas: [
      scavengerSchema,
      ironhideSchema.typeDefs,
    ],
    resolvers: ironhideSchema.resolvers,
  });

  return schema;
};

export default getServerSchema;
