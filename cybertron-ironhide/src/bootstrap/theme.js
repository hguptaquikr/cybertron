/* eslint-disable no-unused-expressions */
import { injectGlobal } from 'styled-components';
import theme, { injectBaseStyles } from 'leaf-ui/theme';

theme.color.primaryLighter = theme.color.lagoonLighter;
theme.color.primaryLight = theme.color.lagoonLight;
theme.color.primary = theme.color.lagoon;
theme.color.primaryDark = theme.color.lagoonDark;

injectBaseStyles(theme);
injectGlobal`
  html {
    min-width: 1024px;
  }

  body {
    overflow: hidden;
  }
`;

export default theme;
