export const GET_INVENTORY = 'houseView/GET_INVENTORY';
export const CONSTRUCT_SCHEDULE = 'houseView/CONSTRUCT_SCHEDULE';
export const CHANGE_STEP = 'houseView/CHANGE_STEP';
export const CHANGE_DATE_RANGE = 'houseView/SET_START_DATE';
export const SHOW_QUICKVIEW = 'houseView/SET_QUICKVIEW';
