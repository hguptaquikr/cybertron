export const makeGuest = (guest) => ({
  id: guest.id,
});

export const normalize = (guests) => ({
  byId: guests.reduce((acc, guest) => ({
    ...acc,
    [guest.id]: makeGuest(guest),
  }), {}),
  ids: guests.map((guest, index) => index),
});

export const getAll = (ids, byId) =>
  ids.map((guestId) => byId[guestId]);
