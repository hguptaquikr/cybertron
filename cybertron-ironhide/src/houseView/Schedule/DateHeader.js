import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import format from 'date-fns/format';
import isSameDay from 'date-fns/is_same_day';
import Flex from 'leaf-ui/Flex/web';
import Spacer from 'leaf-ui/Spacer/web';
import Text from 'leaf-ui/Text/web';

const isToday = (date) => isSameDay(new Date(), date);

const Badge = styled.div`
  ${(p) => isToday(p.date) ? `background-color: ${p.theme.color.primary}` : ''};
  border-radius: ${(p) => p.theme.borderRadius};
`;

const DateHeader = ({ date }) => (
  <Spacer>
    <Flex alignItems="center" justifyContent="center">
      <Spacer padding={[1]} margin={[0, 1, 0, 0]}>
        <Badge date={date}>
          <Text
            size="xs"
            color={isToday(date) ? 'white' : 'greyDarker'}
          >
            {format(date, 'DD')}
          </Text>
        </Badge>
      </Spacer>
      {
          isToday(date) ? (
            <Text size="xs" weight="bold">Today</Text>
          ) : (
            <Text size="xs">{format(date, 'ddd')}</Text>
          )
        }
    </Flex>
  </Spacer>
);

DateHeader.propTypes = {
  date: PropTypes.object.isRequired,
};

export default DateHeader;
