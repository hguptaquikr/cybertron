import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import eachDay from 'date-fns/each_day';
import differenceInCalenderDays from 'date-fns/difference_in_calendar_days';
import capitalize from 'lodash/capitalize';
import Text from 'leaf-ui/Text/web';
import { TH, TR } from './HouseViewTable';
import DateHeader from './DateHeader';

const PastCol = styled.col`
  background-color: ${(p) => p.theme.color.greyLighter};
`;
const PresentCol = styled.col`
  background-color: ${(p) => p.theme.color.white};
`;

const ScheduleHeader = ({ range, roomType }) => {
  const rowHeaderWidth = 8; // width in % of the room number header of a grid row
  const colWidth = (100 - rowHeaderWidth) / range.steps[range.selectedStep];

  return (
    <React.Fragment>
      <colgroup>
        <col width={`${rowHeaderWidth}%`} />
        {
          eachDay(range.startDate, range.endDate).map((date) => {
            if (differenceInCalenderDays(date, new Date()) < 0) {
              return (
                <PastCol
                  key={`col-${date}`}
                  width={`${colWidth}%`}
                />
              );
            }
            return (
              <PresentCol
                key={`col-${date}`}
                width={`${colWidth}%`}
              />
            );
          })
        }
      </colgroup>
      <thead>
        <TR>
          <TH width={`${rowHeaderWidth}%`}>
            <Text size="xs">
              {capitalize(roomType)}
            </Text>
          </TH>
          {
            eachDay(range.startDate, range.endDate).map((date) => (
              <TH key={date} width={`${colWidth}%`}>
                <DateHeader date={date} />
              </TH>
            ))
          }
        </TR>
      </thead>
    </React.Fragment>
  );
};

ScheduleHeader.propTypes = {
  range: PropTypes.object.isRequired,
  roomType: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  range: state.$houseView.range,
});

export default connect(
  mapStateToProps,
)(ScheduleHeader);
