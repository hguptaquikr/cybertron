import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';
import differenceInCalenderDays from 'date-fns/difference_in_calendar_days';
import * as bookingHelpers from 'cybertron-service-booking/lib/bookingHelpers';
import Flex from 'leaf-ui/Flex/web';
import Icon from 'leaf-ui/Icon/web';
import Spacer from 'leaf-ui/Spacer/web';
import Text from 'leaf-ui/Text/web';
import { bookingConstants } from '../../core/constants';
import * as houseViewActionCreators from '../houseViewActionCreators';
import * as tabQueries from '../../tab/tabQueries';
import * as tabMutations from '../../tab/tabMutations';

const CapsuleContainer = styled.div`
  padding: 2px;
  ${(p) =>
    `background: linear-gradient(
      to right,
      ${p.theme.color.greyLighter} ${p.capsulePastPercentage},
      ${p.theme.color.white} ${p.capsulePastPercentage}
    );`}
`;

const Capsule = styled.div`
  background-color: ${(p) => p.theme.color[bookingConstants.statusToColorMap[p.status]]};
  border: 2px solid;
  border-radius: 50px;
  text-align: center;
  ${(p) => p.status === bookingConstants.status.temporary ? `
    color: ${p.theme.color.greyDarker};
  ` : `
    color: ${p.theme.color.white};
  `}
  ${(p) => p.status === bookingConstants.status.temporary ? `
    border-color: ${p.theme.color.blue};
  ` : `
    border-color: ${p.theme.color[bookingConstants.statusToColorMap[p.status]]};
  `}
  ${(p) => p.headless ? `
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
  ` : ''}
  ${(p) => p.tailless ? `
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  ` : ''}

  &:hover {
    cursor: pointer;
    box-shadow: ${(p) => p.theme.boxShadow[3]};
    ${(p) => p.status === bookingConstants.status.temporary ? `
      background-color: ${p.theme.color.translucent};
    ` : `
      background-color: ${p.theme.color[`${bookingConstants.statusToColorMap[p.status]}Dark`]};
    `}
  }
`;

class BookingCapsule extends React.Component {
  openQuickView = () => {
    const { booking, $selectedTabIndex, addFlow } = this.props;
    addFlow({
      variables: {
        tabIndex: $selectedTabIndex.selectedTabIndex,
        flow: {
          component: 'QuickView',
          props: {
            bookingId: booking.id,
          },
        },
      },
    });
    // houseViewActions.showQuickView(true, booking.id);
  }

  calculateCapsulePastPercentage = () => {
    const { booking, range } = this.props;

    if (differenceInCalenderDays(new Date(), range.endDate) > 0) return '100%';
    else if (differenceInCalenderDays(booking.checkIn, new Date()) >= 0) return '0%';

    const capsuleStart = differenceInCalenderDays(booking.checkIn, range.startDate) > 0
      ? booking.checkIn
      : range.startDate;
    const capsuleEnd = differenceInCalenderDays(booking.checkOut, range.endDate) > 0
      ? range.endDate
      : booking.checkOut;

    const capsuleSpan = differenceInCalenderDays(capsuleEnd, capsuleStart) + 1;
    const pastSpan = differenceInCalenderDays(new Date(), range.startDate);
    return `${(pastSpan / capsuleSpan) * 100}%`;
  }

  render() {
    const { booking, headless, tailless } = this.props;
    return (
      <CapsuleContainer capsulePastPercentage={this.calculateCapsulePastPercentage()}>
        <Spacer padding={[1, 1, 1, 2]}>
          <Capsule
            headless={headless}
            tailless={tailless}
            status={booking.status}
            onClick={this.openQuickView}
          >
            <Flex justifyContent="space-between" alignItems="center">
              <Text size="s">{booking.id}</Text>
              <Icon color="white" />
            </Flex>
          </Capsule>
        </Spacer>
      </CapsuleContainer>
    );
  }
}

BookingCapsule.propTypes = {
  booking: PropTypes.object.isRequired,
  range: PropTypes.object.isRequired,
  headless: PropTypes.bool,
  tailless: PropTypes.bool,
  $selectedTabIndex: PropTypes.number.isRequired,
  addFlow: PropTypes.func.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  booking: state.$booking.byId[ownProps.bookingId] || bookingHelpers.makeBooking({}),
  range: state.$houseView.range,
});

const mapDispatchToProps = (dispatch) => ({
  houseViewActions: bindActionCreators(houseViewActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  graphql(tabQueries.SELECTED_TAB_INDEX, { name: '$selectedTabIndex' }),
  graphql(tabMutations.ADD_FLOW, { name: 'addFlow' }),
)(BookingCapsule);
