import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import addDays from 'date-fns/add_days';
import eachDay from 'date-fns/each_day';
import format from 'date-fns/format';
import Card from 'leaf-ui/Card/web';
import Text from 'leaf-ui/Text/web';
import Spacer from 'leaf-ui/Spacer/web';
import ScheduleHeader from './ScheduleHeader';
import BookingCapsule from './BookingCapsule';
import { TR, TH, TD } from './HouseViewTable';
import { dateFormat } from '../../core/constants';

const Table = styled.table`
  border-collapse: collapse;
  border: 1px solid ${(p) => p.theme.color.greyDark};
  overflow: scroll;
`;

class Schedule extends React.Component {
  renderBookingCapsule = (
    bookingId,
    colSpan,
    date,
    isHeadless = false,
    isTailless = false,
  ) => (
    <TD key={`${bookingId}-${format(date, dateFormat.query)}`} colSpan={colSpan}>
      <BookingCapsule
        bookingId={bookingId}
        headless={isHeadless}
        tailless={isTailless}
      />
    </TD>
  );

  renderSchedule = (roomSchedule, roomNumber) => {
    const { $houseView: { range } } = this.props;
    const dateRange = eachDay(range.startDate, range.endDate)
      .map((date) => format(date, dateFormat.query));
    const rowElements = [];

    const lastElement = dateRange.reduce((acc, date, index) => {
      const { prevBid, colSpan, isHeadless } = acc;
      const bid = roomSchedule[date];
      const key = `${bid}-${roomNumber}-${date}`;

      if (!bid) {
        if (colSpan > 0) {
          const bookingCapsule = this.renderBookingCapsule(prevBid, colSpan, key, isHeadless);
          rowElements.push(bookingCapsule);
        }

        rowElements.push(
          <TD key={key} />,
        );

        return {
          colSpan: 0,
          prevBid: undefined,
          isHeadless: false,
        };
      } else if (bid && !prevBid) {
        if (index === 0) {
          const contBid = roomSchedule[format(addDays(range.startDate, -1), dateFormat.query)];
          return {
            colSpan: 1,
            prevBid: bid,
            isHeadless: contBid === bid,
          };
        }

        return {
          colSpan: 1,
          prevBid: bid,
          isHeadless: false,
        };
      } else if (bid && prevBid) {
        if (bid === prevBid) {
          return {
            colSpan: colSpan + 1,
            prevBid,
            isHeadless,
          };
        }
        const bookingCapsule = this.renderBookingCapsule(prevBid, colSpan, key, isHeadless);
        rowElements.push(bookingCapsule);
        return {
          colSpan: 1,
          prevBid: bid,
          isHeadless: false,
        };
      }

      return acc;
    }, {
      colSpan: 0,
      prevBid: undefined,
    });

    if (lastElement.colSpan > 0) {
      const { prevBid, colSpan, isHeadless } = lastElement;
      const contBid = roomSchedule[format(addDays(range.endDate, 1), dateFormat.query)];
      const isTailless = prevBid === contBid;
      const key = `${prevBid}-${roomNumber}`;
      const bookingCapsule = this.renderBookingCapsule(
        prevBid,
        colSpan,
        key,
        isHeadless,
        isTailless,
      );
      rowElements.push(bookingCapsule);
    }
    return rowElements;
  };

  renderRoomRow = (roomNumber) => {
    const { $houseView: { grid } } = this.props;
    const schedule = grid.schedule[roomNumber];
    return (
      <TR key={roomNumber}>
        <TH key={roomNumber}>
          <Text size="xs" weight="normal">{roomNumber}</Text>
        </TH>
        {this.renderSchedule(schedule, roomNumber)}
      </TR>
    );
  }

  render() {
    const { $roomType } = this.props;

    return (
      <Spacer maxHeight="calc(100vh - 111px)">
        <div style={{ overflow: 'scroll' }}>
          <Card>
            <Spacer padding={[1]}>
              <div>
                <Spacer width="100%">
                  <Table cellSpacing={0} cellPadding={0}>
                    <ScheduleHeader roomType="oak" />
                    <tbody>
                      {
                        $roomType.byId['H1/oak'].roomNumbers
                        .map(this.renderRoomRow)
                      }
                    </tbody>
                  </Table>
                </Spacer>
              </div>
            </Spacer>
          </Card>
          <Card>
            <Spacer padding={[1]}>
              <div>
                <Spacer width="100%">
                  <Table cellSpacing={0} cellPadding={0}>
                    <ScheduleHeader roomType="maple" />
                    <tbody>
                      {
                        $roomType.byId['H1/maple'].roomNumbers
                          .map(this.renderRoomRow)
                      }
                    </tbody>
                  </Table>
                </Spacer>
              </div>
            </Spacer>
          </Card>
        </div>
      </Spacer>
    );
  }
}

Schedule.propTypes = {
  $roomType: PropTypes.object.isRequired,
  $houseView: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  $roomType: state.$roomType,
  $houseView: state.$houseView,
});

export default connect(
  mapStateToProps,
)(Schedule);
