import styled from 'styled-components';

export const TD = styled.td`
  border: 1px solid ${(p) => p.theme.color.greyDark};
  border-top-style: dashed;
  border-bottom-style: dashed;
`;

export const TH = TD.withComponent('th').extend`
  font-weight: ${(p) => p.theme.fontWeight.normal};
`;

export const TR = styled.tr`
  height: 50px;
`;
