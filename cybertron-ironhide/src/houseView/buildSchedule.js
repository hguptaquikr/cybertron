import isEmpty from 'cybertron-utils/isEmpty';
import eachDay from 'date-fns/each_day';
import format from 'date-fns/format';
import isSameDay from 'date-fns/is_same_day';
import differenceInCalendarDays from 'date-fns/difference_in_calendar_days';
import { dateFormat } from '../core/constants';

const roomStayDuration =
  (roomStay) => differenceInCalendarDays(roomStay.checkOut, roomStay.checkIn);

const isRoomFree = (roomSchedule, dateRange) =>
  isEmpty(dateRange.map((date) => roomSchedule[date]).filter(Boolean));

const getFreeRoomNumber = (unassignedRoomStay, roomNumbers, schedule) => {
  const dateRange = eachDay(unassignedRoomStay.checkIn, unassignedRoomStay.checkOut)
    .map((date) => format(date, dateFormat.query));
  const freeRoomNumber = roomNumbers
    .reduce((accRoomNumber, roomNumber) => {
      if (accRoomNumber) return accRoomNumber;
      else if (isRoomFree(schedule[roomNumber], dateRange)) return roomNumber;
      return accRoomNumber;
    }, null);
  return freeRoomNumber;
};

const mapRoomStaysToSchedule = (assignedRoomStays, schedule) =>
  assignedRoomStays.reduce((accSchedule, roomStay) => {
    const roomStaySchedule = eachDay(roomStay.checkIn, roomStay.checkOut)
      .reduce((accRoomStaySchedule, date) => ({
        ...accRoomStaySchedule,
        [format(date, dateFormat.query)]: roomStay.bid,
      }), {});
    return {
      ...accSchedule,
      [roomStay.roomNumber]: {
        ...accSchedule[roomStay.roomNumber],
        ...roomStaySchedule,
      },
    };
  }, schedule);

const virtualAssignment = ($roomType, unassignedRoomStays, schedule) =>
  unassignedRoomStays.reduce((accSchedule, unassignedRoomStay) => {
    const roomType = $roomType.byId[unassignedRoomStay.roomType];
    const virtuallyAssignedRoomStay = {
      ...unassignedRoomStay,
      roomNumber: getFreeRoomNumber(unassignedRoomStay, roomType.roomNumbers, accSchedule),
    };
    return mapRoomStaysToSchedule([virtuallyAssignedRoomStay], accSchedule);
  }, schedule);

const buildSchedule = ($booking, $room, $roomType) => {
  const bookings = Object.values($booking.byId);

  const initialSchedule = Object.keys($room.byId).reduce((o, roomId) => ({
    ...o,
    [roomId]: {},
  }), {});

  const allRoomStays = bookings.reduce((accRoomStays, booking) => ([
    ...accRoomStays,
    ...booking.roomStays.map((roomStay) => ({
      ...roomStay,
      bid: booking.id,
    })),
  ]), []);

  const assignedRoomStays = allRoomStays.filter((roomStay) => !isEmpty(roomStay.roomNumber));
  const unassignedRoomStays = allRoomStays
    .filter((roomStay) => isEmpty(roomStay.roomNumber))
    .sort((roomStay1, roomStay2) => isSameDay(roomStay2.checkIn, roomStay1.checkIn)
      ? roomStayDuration(roomStay2) - roomStayDuration(roomStay1)
      : differenceInCalendarDays(roomStay1.checkIn, roomStay2.checkIn));

  const fixedSchedule = mapRoomStaysToSchedule(assignedRoomStays, initialSchedule);
  return virtualAssignment($roomType, unassignedRoomStays, fixedSchedule);
};

export default buildSchedule;
