import addDays from 'date-fns/add_days';
import * as houseViewActionTypes from './houseViewActionTypes';
import * as roomActionCreators from '../room/roomActionCreators';
import * as roomTypeActionCreators from '../roomType/roomTypeActionCreators';
import buildSchedule from './buildSchedule';

export const getInventory = (hotelId) => (dispatch, getState, { request }) =>
  request.get(`/api/crs/v1/hotel/${hotelId}/inventory`)
    .then((response) => {
      const roomTypes = response.roomTypes.map((roomType) => ({
        id: `${hotelId}/${roomType}`,
        type: roomType,
        roomNumbers: response.rooms.filter((room) => room.type === roomType).map((room) => room.id),
      }));
      const roomTypeIds = roomTypes.map((roomType) => roomType.id);
      dispatch(roomTypeActionCreators.setRoomTypes(roomTypes));
      dispatch(roomTypeActionCreators.setRoomTypeIds(roomTypeIds));

      const rooms = response.rooms.map((room) => ({
        id: room.id,
        number: room.number,
        type: `${hotelId}/${room.type}`,
      }));
      const roomIds = rooms.map((room) => room.id);
      dispatch(roomActionCreators.setRooms(rooms));
      dispatch(roomActionCreators.setRoomIds(roomIds));
    });

export const constructSchedule = () => (dispatch, getState) => {
  const { $booking, $room, $roomType } = getState();
  const schedule = buildSchedule($booking, $room, $roomType);

  dispatch({
    type: houseViewActionTypes.CONSTRUCT_SCHEDULE,
    payload: schedule,
  });
};

export const changeDateRange = (range) => ({
  type: houseViewActionTypes.CHANGE_DATE_RANGE,
  payload: { range },
});

export const changeStep = (step) => (dispatch, getState) => {
  const { $houseView: { range } } = getState();

  return dispatch({
    type: houseViewActionTypes.CHANGE_STEP,
    payload: {
      range: {
        startDate: range.startDate,
        endDate: addDays(range.startDate, range.steps[step] - 1),
        selectedStep: step,
      },
    },
  });
};

export const nextStep = () => (dispatch, getState) => {
  const { $houseView: { range } } = getState();

  return dispatch(
    changeDateRange({
      startDate: addDays(range.startDate, range.steps[range.selectedStep]),
      endDate: addDays(range.endDate, range.steps[range.selectedStep]),
    }),
  );
};

export const previousStep = () => (dispatch, getState) => {
  const { $houseView: { range } } = getState();

  return dispatch(
    changeDateRange({
      startDate: addDays(range.startDate, 0 - range.steps[range.selectedStep]),
      endDate: addDays(range.endDate, 0 - range.steps[range.selectedStep]),
    }),
  );
};

export const showQuickView = (isOpen, bookingId) => ({
  type: houseViewActionTypes.SHOW_QUICKVIEW,
  payload: { isOpen, bookingId },
});
