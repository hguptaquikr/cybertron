import React from 'react';
import styled from 'styled-components';
import Flex from 'leaf-ui/Flex/web';
import Text from 'leaf-ui/Text/web';
import Spacer from 'leaf-ui/Spacer/web';
import { bookingConstants } from '../../core/constants';

const legend = [
  {
    type: bookingConstants.status.checkedOut,
    label: 'Checked Out',
  }, {
    type: bookingConstants.status.checkedIn,
    label: 'Checked In',
  }, {
    type: bookingConstants.status.reserved,
    label: 'Reserved',
  }, {
    type: bookingConstants.status.temporary,
    label: 'Temp Booking',
  }, {
    type: bookingConstants.status.dnr,
    label: 'DNR',
  }, {
    type: bookingConstants.status.payAtHotel,
    label: 'Pay at Hotel',
  },
];

const LegendIcon = styled.div`
  border: 2px solid;
  border-radius: 50px;
  ${(p) => p.type === bookingConstants.status.temporary ? `
    background-color: ${p.theme.color[bookingConstants.statusToColorMap[p.type]]};
    border-color: ${p.theme.color.blue};
  ` : `
    background-color: ${p.theme.color[bookingConstants.statusToColorMap[p.type]]};
    border-color: ${p.theme.color[bookingConstants.statusToColorMap[p.type]]};
  `}
`;

const Legend = () => (
  <Flex>
    {
      legend.map(({ label, type }) => (
        <Spacer margin={[0, 3, 0, 0]} key={type}>
          <Flex alignItems="center">
            <Spacer margin={[0, 1, 0, 0]} width="24px" height="16px">
              <LegendIcon type={type} />
            </Spacer>
            <Spacer>
              <Text>{label}</Text>
            </Spacer>
          </Flex>
        </Spacer>
      ))
    }
  </Flex>
);

export default Legend;
