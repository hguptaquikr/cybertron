import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import capitalize from 'lodash/capitalize';
import Button from 'leaf-ui/Button/web';
import Card from 'leaf-ui/Card/web';
import Flex from 'leaf-ui/Flex/web';
import Icon from 'leaf-ui/Icon/web';
import Spacer from 'leaf-ui/Spacer/web';
import Text from 'leaf-ui/Text/web';
import Divider from 'leaf-ui/Divider/web';
import Legend from './Legend';
import * as houseViewActionCreators from '../houseViewActionCreators';

const HouseViewHeader = ({
  houseViewActions,
  $houseView,
}) => (
  <Card>
    <Spacer padding={[2]}>
      <Flex justifyContent="space-between">
        <Flex>
          <div>
            <Button
              size="small"
              color="grey"
              onClick={() => houseViewActions.previousStep()}
            ><Icon name="keyboard_arrow_left" />
            </Button>
            <Button
              size="small"
              color="grey"
              onClick={() => houseViewActions.nextStep()}
            ><Icon name="keyboard_arrow_right" />
            </Button>
          </div>

          <Spacer margin={[0, 0, 0, 2]}>
            <Flex>
              <Button
                size="small"
                color="grey"
                onClick={() => houseViewActions.changeRoomType('maple')}
              ><Text>Calendar</Text>
              </Button>
            </Flex>
          </Spacer>

          <Spacer margin={[0, 0, 0, 2]}>
            <Flex>
              {
                Object.keys($houseView.range.steps).map((step) => (
                  <Button
                    key={step}
                    size="small"
                    color="grey"
                    onClick={() => houseViewActions.changeStep(step)}
                  ><Text>{capitalize(step)}</Text>
                  </Button>
                ))
              }
            </Flex>
          </Spacer>
        </Flex>

        <Legend />

      </Flex>
    </Spacer>
    <Divider color="greyLight" />
  </Card>
);

HouseViewHeader.propTypes = {
  $houseView: PropTypes.object.isRequired,
  houseViewActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  $houseView: state.$houseView,
});

const mapDispatchToProps = (dispatch) => ({
  houseViewActions: bindActionCreators(houseViewActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HouseViewHeader);
