import addDays from 'date-fns/add_days';
import * as houseViewActionTypes from './houseViewActionTypes';

export const initialState = {
  range: {
    startDate: addDays(new Date(), -1),
    endDate: addDays(new Date(), 5),
    steps: {
      week: 7,
      fortnight: 14,
    },
    selectedStep: 'week',
  },
  grid: {
    schedule: {},
  },
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case houseViewActionTypes.CONSTRUCT_SCHEDULE:
      return {
        ...state,
        grid: { schedule: payload },
      };

    case houseViewActionTypes.CHANGE_STEP:
    case houseViewActionTypes.CHANGE_DATE_RANGE:
      return {
        ...state,
        range: {
          ...state.range,
          ...payload.range,
        },
      };

    default:
      return state;
  }
};
