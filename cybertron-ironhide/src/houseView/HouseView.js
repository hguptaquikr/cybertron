import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import isEmpty from 'cybertron-utils/isEmpty';
import Flex from 'leaf-ui/Flex/web';
import HouseViewHeader from './HouseViewHeader/HouseViewHeader';
import Schedule from './Schedule/Schedule';
import * as houseViewActionCreators from './houseViewActionCreators';
import * as bookingActionCreators from '../booking/bookingActionCreators';

class HouseView extends React.Component {
  async componentDidMount() {
    const { $hotel, houseViewActions } = this.props;
    await Promise.all([
      houseViewActions.getInventory($hotel.selectedHotelId),
      this.getBookings(),
    ]);
    houseViewActions.constructSchedule();
  }

  componentWillReceiveProps(nextProps) {
    const { props } = this;

    if (
      Object.values(props.$booking.byId).length
        !== Object.values(nextProps.$booking.byId).length
    ) {
      props.houseViewActions.constructSchedule();
    }
  }

  getBookings = () => {
    const { $hotel, bookingActions } = this.props;
    const bookingsPromises = $hotel.byId[$hotel.selectedHotelId].bookingIds
      .map((bookingId) => bookingActions.getBooking(bookingId));
    return Promise.all(bookingsPromises);
  }

  render() {
    const { $houseView } = this.props;

    return !isEmpty($houseView.grid.schedule) ? (
      <Flex flexDirection="column">
        <HouseViewHeader />
        <Schedule />
      </Flex>
    ) : null;
  }
}

HouseView.propTypes = {
  $hotel: PropTypes.object.isRequired,
  $booking: PropTypes.object.isRequired,
  $houseView: PropTypes.object.isRequired,
  houseViewActions: PropTypes.object.isRequired,
  bookingActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  $hotel: state.$hotel,
  $booking: state.$booking,
  $houseView: state.$houseView,
});

const mapDispatchToProps = (dispatch) => ({
  houseViewActions: bindActionCreators(houseViewActionCreators, dispatch),
  bookingActions: bindActionCreators(bookingActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HouseView);
