import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import format from 'date-fns/format';
import differenceInCalendarDays from 'date-fns/difference_in_calendar_days';
import Button from 'leaf-ui/Button/web';
import Card from 'leaf-ui/Card/web';
import Flex from 'leaf-ui/Flex/web';
import Icon from 'leaf-ui/Icon/web';
import Price from 'leaf-ui/Price/web';
import Spacer from 'leaf-ui/Spacer/web';
import Text from 'leaf-ui/Text/web';
import * as bookingHelpers from 'cybertron-service-booking/lib/bookingHelpers';
import { dateFormat } from '../../core/constants';
import * as houseViewActionCreators from '../houseViewActionCreators';
import * as tabMutations from '../../tab/tabMutations';
import BookingIndicators from '../../booking/BookingIndicators/BookingIndicators';

class QuickView extends React.Component {
  closeQuickView = () => {
    const { removeFlow, tabIndex, flowIndex } = this.props;
    removeFlow({
      variables: {
        tabIndex,
        flowIndex,
      },
    });
  }

  renderQuickViewProps = () => {
    const { booking } = this.props;
    const stayDuration = differenceInCalendarDays(booking.checkOut, booking.checkIn);
    const roomsAssigned = booking.roomStays
      .map((roomStay) => roomStay.roomNumber)
      .filter(Boolean)
      .join(', ');

    const quickViewProps = [{
      label: 'Check In',
      value: format(booking.checkin, dateFormat.view),
    }, {
      label: 'Check Out',
      value: format(booking.checkOut, dateFormat.view),
    }, {
      label: 'Stay Duration',
      value: `${stayDuration} Nights`,
    }, {
      label: 'Rooms Assigned',
      value: roomsAssigned,
    }, {
      label: 'Booking Details',
      value: `${booking.source.name} - ${booking.source.referenceNumber}`,
    }, {
      label: 'Creator Details',
      value: booking.creator.phone,
    }, {
      label: 'Special Request',
      value: booking.specialRequest,
    }];

    return (
      <Spacer margin={[3, 0, 0, 0]}>
        <Flex flexWrap="wrap">
          {
            quickViewProps.map((quickViewProp) => (
              <Spacer margin={[2, 0]} minWidth="50%">
                <Flex flexDirection="column" flex="1">
                  <Spacer margin={[0, 0, 1, 0]}>
                    <Text size="xs">
                      {quickViewProp.label}
                    </Text>
                  </Spacer>
                  <Text>
                    {quickViewProp.value}
                  </Text>
                </Flex>
              </Spacer>
            ))
          }
        </Flex>
      </Spacer>
    );
  }

  render() {
    const { booking, primaryGuest, addTab } = this.props;
    return (
      <Spacer width="30%" height="100%" margin={[0, 0, 0, 'auto']}>
        <Flex>
          <Card color="white" elevation={3}>
            <Spacer height="100%">
              <Flex flexDirection="column" justifyContent="space-between">
                <Spacer padding={[4]}>
                  <Card>
                    <Spacer margin={[0, 0, 2, 0]}>
                      <Flex justifyContent="space-between">
                        <Text size="xxl" weight="bold">
                          {primaryGuest.name}
                        </Text>
                        <Icon
                          name="close"
                          onClick={this.closeQuickView}
                        />
                      </Flex>
                    </Spacer>
                    <BookingIndicators bookingId={booking.id} />
                    {this.renderQuickViewProps()}
                  </Card>
                </Spacer>
                <Spacer padding={[4]}>
                  <Card color="greyLighter">
                    <Text size="xl" weight="bold">
                      Total Payable
                      <Spacer margin={[0, 0, 0, 1]}>
                        <Price>{booking.price.sellingPrice}</Price>
                      </Spacer>
                    </Text>
                    <Text color="blue">Record Payment {'>'}</Text>
                    <Spacer margin={[4, 0, 0, 0]}>
                      <Flex justifyContent="space-between">
                        <Button
                          kind="outlined"
                          onClick={() => {
                            addTab({
                              variables: {
                                tab: {
                                  flows: [{
                                    component: 'BookingDetails',
                                    props: { bookingId: 'B1' },
                                  }],
                                },
                              },
                            });
                          }}
                        >
                          VIEW BOOKING DETAILS
                        </Button>
                        <Button>
                          CHECK IN
                        </Button>
                      </Flex>
                    </Spacer>
                  </Card>
                </Spacer>
              </Flex>
            </Spacer>
          </Card>
        </Flex>
      </Spacer>
    );
  }
}

QuickView.propTypes = {
  booking: PropTypes.object,
  primaryGuest: PropTypes.object,
  tabIndex: PropTypes.number.isRequired,
  flowIndex: PropTypes.number.isRequired,
  addTab: PropTypes.func.isRequired,
  removeFlow: PropTypes.func.isRequired,
};

const mapStateToProps = (state, props) => {
  const booking = state.$booking.byId[props.bookingId]
    || bookingHelpers.makeBooking({});
  const primaryGuest = state.$guest.byId[booking.guestIds[0]] || state.$guest.byId.g1;

  return {
    booking,
    primaryGuest,
  };
};

const mapDispatchToProps = (dispatch) => ({
  houseViewActions: bindActionCreators(houseViewActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  graphql(tabMutations.ADD_TAB, { name: 'addTab' }),
  graphql(tabMutations.REMOVE_FLOW, { name: 'removeFlow' }),
)(QuickView);
