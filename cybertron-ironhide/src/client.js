import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux';
import createStore from './store/createStore';
import apolloClient from './bootstrap/apolloClient';
import Wrapper from './bootstrap/Wrapper/Wrapper';

const store = createStore(window.__INITIAL_STATE__);

ReactDOM.render(
  <ApolloProvider client={apolloClient}>
    <Provider store={store}>
      <Wrapper />
    </Provider>
  </ApolloProvider>,
  document.getElementById('root'),
);
