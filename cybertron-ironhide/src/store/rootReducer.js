import { combineReducers } from 'redux';
import * as authReducer from 'cybertron-service-auth/lib/authReducer';
import hotelReducer from '../hotel/hotelReducer';
import bookingReducer from '../booking/bookingReducer';
import roomTypeReducer from '../roomType/roomTypeReducer';
import roomReducer from '../room/roomReducer';
import guestReducer from '../guest/guestReducer';
import priceReducer from '../price/priceReducer';
import chargeReducer from '../charge/chargeReducer';
import houseViewReducer from '../houseView/houseViewReducer';

export default combineReducers({
  $auth: authReducer.reducer,
  $hotel: hotelReducer,
  $booking: bookingReducer,
  $roomType: roomTypeReducer,
  $room: roomReducer,
  $guest: guestReducer,
  $price: priceReducer,
  $charge: chargeReducer,
  $houseView: houseViewReducer,
});
