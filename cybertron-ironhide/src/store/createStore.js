import { createStore, compose, applyMiddleware } from 'redux';
import reduxThunkMiddleware from 'redux-thunk';
import { makeRequest } from 'cybertron-utils/request';
import { middleware as reduxPackMiddleware } from 'redux-pack';
import config from '../config';
import rootReducer from './rootReducer';

const middlewares = [
  reduxThunkMiddleware.withExtraArgument({ request: makeRequest(config.hostUrl) }),
  reduxPackMiddleware,
].filter(Boolean);

const storeEnhancers = [
  applyMiddleware(...middlewares),
  __LOCAL__ && window.devToolsExtension && window.devToolsExtension(),
].filter(Boolean);

export default (initialState) => createStore(
  rootReducer,
  initialState,
  compose(...storeEnhancers),
);
