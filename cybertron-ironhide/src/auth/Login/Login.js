import React from 'react';
import PropTypes from 'prop-types';
import Button from 'leaf-ui/Button/web';
import Card from 'leaf-ui/Card/web';
import Checkbox from 'leaf-ui/Checkbox/web';
import Flex from 'leaf-ui/Flex/web';
import Form from 'leaf-ui/Form/web';
import Link from 'leaf-ui/Link/web';
import Text from 'leaf-ui/Text/web';
import Spacer from 'leaf-ui/Spacer/web';
import TextInput from 'leaf-ui/TextInput/web';
import * as loginService from './loginService';

const Login = (props) => (
  <Spacer height="100vh">
    <Flex alignItems="center" justifyContent="center">
      <Spacer width="800px">
        <Flex>
          <Spacer padding={[4]} width="50%">
            <Card color="greyLight">&nbsp;</Card>
          </Spacer>
          <Spacer padding={[4]} width="50%">
            <Card>
              <Form
                onSubmit={(values) => {
                  loginService.loginUser(values)
                  .then((response) => {
                    props.onLoginSuccess(response);
                  });
                }}
                validationSchema={
                  Form.validation.object().shape({
                    username: Form.validation.string().required(),
                    password: Form.validation.string().required(),
                  })
                }
              >
                <Form.Form>
                  <Spacer margin={[0, 0, 3, 0]}>
                    <Text size="m" weight="semibold">
                      Hello, Login to your account
                    </Text>
                  </Spacer>
                  <Spacer margin={[0, 0, 2, 0]}>
                    <TextInput
                      name="username"
                      label="USER NAME"
                      block
                    />
                  </Spacer>
                  <Spacer margin={[0, 0, 5, 0]}>
                    <TextInput
                      name="password"
                      label="PASSWORD"
                      type="password"
                      block
                    />
                  </Spacer>
                  <Spacer margin={[0, 0, 2, 0]}>
                    <Button type="submit" block>
                      Login
                    </Button>
                  </Spacer>
                  <Flex justifyContent="space-between">
                    <Checkbox name="rememberMe" label="Remember me" />
                    <Spacer padding={[2, 0]}>
                      <Link>
                        Forgot password?
                      </Link>
                    </Spacer>
                  </Flex>
                </Form.Form>
              </Form>
            </Card>
          </Spacer>
        </Flex>
      </Spacer>
    </Flex>
  </Spacer>
);

Login.propTypes = {
  onLoginSuccess: PropTypes.func,
};

export default Login;
