import request from 'cybertron-utils/request';

export const loginUser = (credentials) =>
  request('http://localhost:8000/api/crs/auth/', credentials, { method: 'POST' });
