import flattenReducers from 'cybertron-utils/flattenReducers';
import * as roomTypeServiceReducer from 'cybertron-service-roomType/lib/roomTypeReducer';
import * as roomTypeActionTypes from './roomTypeActionTypes';

const initialState = {
  ids: [],
};

const roomTypeReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case roomTypeActionTypes.SET_ROOM_TYPE_IDS: {
      return {
        ...state,
        ids: payload.roomTypeIds,
      };
    }

    default:
      return state;
  }
};

export default flattenReducers({
  byId: roomTypeServiceReducer.reducer,
}, roomTypeReducer);
