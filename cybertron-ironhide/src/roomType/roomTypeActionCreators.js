import * as roomTypeActionTypes from './roomTypeActionTypes';

export * from 'cybertron-service-roomType/lib/roomTypeActionCreators';

export const setRoomTypeIds = (roomTypeIds) => ({
  type: roomTypeActionTypes.SET_ROOM_TYPE_IDS,
  payload: {
    roomTypeIds,
  },
});
