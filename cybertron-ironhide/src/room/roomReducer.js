import flattenReducers from 'cybertron-utils/flattenReducers';
import * as roomServiceReducer from 'cybertron-service-room/lib/roomReducer';
import * as roomActionTypes from './roomActionTypes';

const initialState = {
  ids: [],
};

const roomReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case roomActionTypes.SET_ROOM_IDS: {
      return {
        ...state,
        ids: payload.roomIds,
      };
    }

    default:
      return state;
  }
};

export default flattenReducers({
  byId: roomServiceReducer.reducer,
}, roomReducer);
