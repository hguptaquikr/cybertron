import * as roomActionTypes from './roomActionTypes';

export * from 'cybertron-service-room/lib/roomActionCreators';

export const setRoomIds = (roomIds) => ({
  type: roomActionTypes.SET_ROOM_IDS,
  payload: {
    roomIds,
  },
});
