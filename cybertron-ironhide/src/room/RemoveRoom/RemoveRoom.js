import React from 'react';
import Text from 'leaf-ui/Text/web';
import Button from 'leaf-ui/Button/web';
import Flex from 'leaf-ui/Flex/web';
import Modal from 'leaf-ui/Modal/web';
import Spacer from 'leaf-ui/Spacer/web';
import Price from 'leaf-ui/Price/web';
import * as tabHelpers from '../../tab/tabHelpers';

class RemoveRoom extends React.Component {
  state = {
    show: true,
  };

  onClose = () => {
    this.setState({ show: false });
  }

  render() {
    return (
      <Modal
        isOpen={this.state.show}
        onClose={this.onClose}
        container={() => tabHelpers.makeFlowId(this.props)}
      >
        <Modal.Header>
          <Text size="xxxl" weight="bold">Remove 201 Oak</Text>
        </Modal.Header>
        <Modal.Content>
          <Text size="l" weight="semi">Are you sure you want to remove this room?</Text>
          <Text size="m">This booking has entered the stay period. Cancellation Feee will be charged.</Text>
        </Modal.Content>
        <Modal.Footer>
          <Flex justifyContent="space-between" alignItems="center">
            <Flex>
              <Spacer margin={[0, 1, 0, 0]}>
                <Text size="l" weight="bold">Cancellation Fee</Text>
              </Spacer>
              <Text size="l" weight="bold"><Price>800</Price></Text>
            </Flex>
            <Flex justifyContent="space-between">
              <Spacer margin={[0, 2, 0, 0]}>
                <Button size="small" kind="outlined">CANCEL</Button>
              </Spacer>
              <Button size="small">REMOVE ROOM</Button>
            </Flex>
          </Flex>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default RemoveRoom;
