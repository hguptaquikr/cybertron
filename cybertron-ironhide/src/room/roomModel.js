export const makeRoomType = (roomType) => ({
  type: roomType.type,
});

export const normalize = (roomTypes) => ({
  byType: roomTypes.reduce((acc, roomType) => ({
    ...acc,
    [roomType.type]: makeRoomType(roomType),
  }), {}),
  types: roomTypes.map((roomType, index) => index),
});

export const getAll = (byType, types) =>
  types.map((roomTypeId) => byType[roomTypeId]);

export const getRoomType = (room, roomNumber) =>
  Object.values(room.byType)
    .find((roomType) => roomType.roomNumbers.includes(roomNumber))
    .type;
