import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import Form from 'leaf-ui/Form/web';
import RadioButton from 'leaf-ui/RadioButton/web';
import Select from 'leaf-ui/Select/web';
import Text from 'leaf-ui/Text/web';
import Button from 'leaf-ui/Button/web';
import Flex from 'leaf-ui/Flex/web';
import Modal from 'leaf-ui/Modal/web';
import Spacer from 'leaf-ui/Spacer/web';
import Price from 'leaf-ui/Price/web';
import * as tabMutations from '../../tab/tabMutations';

const roomNumberOptions = [
  {
    value: '201',
    label: '201',
  }, {
    value: '301',
    label: '301',
  }, {
    value: '401',
    label: '401',
  }, {
    value: '202',
    label: '202',
  },
];

const roomTypes = [
  {
    value: 'maple',
    label: 'Maple',
  },
  {
    value: 'mahogany',
    label: 'Mahogany',
  },
  {
    value: 'oak',
    label: 'Oak',
  },
];

class ChangeRoom extends React.Component {
  closeChangeRoom = () => {
    const { tabIndex, flowIndex, removeFlow } = this.props;
    removeFlow({
      variables: {
        tabIndex,
        flowIndex,
      },
    });
  }

  confirmChangeRoom = () => {
    setTimeout(this.closeChangeRoom, 1000);
  }

  render() {
    const { roomNumber, roomType } = this.props;
    const roomTypeName = roomType.split('/')[1];
    return (
      <Modal
        isOpen
        onClose={this.closeChangeRoom}
      >
        <Form
          initialValues={{
            roomNumbers: { label: roomNumber, value: roomNumber },
            roomTypes: { label: roomTypeName, value: roomTypeName },
          }}
          validationSchema={
            Form.validation.object().shape({
              upgradeType: Form.validation.string().required(),
              roomNumbers: Form.validation.object().required(),
              roomTypes: Form.validation.object().required(),
            })
          }
          onSubmit={this.confirmChangeRoom}
        >
          <Form.Form>
            <Modal.Header>
              <Text size="xxxl" weight="bold">
                Change Room for {`${roomNumber} ${roomTypeName}`}
              </Text>
            </Modal.Header>
            <Modal.Content>
              <Flex>
                <Spacer margin={[0, 0, 2, 0]}>
                  <Flex>
                    <Spacer margin={[0, 2, 0, 0]}>
                      <Select
                        name="roomTypes"
                        label="Room Type"
                        options={roomTypes}
                      />
                    </Spacer>
                    <Flex>
                      <Select
                        name="roomNumbers"
                        label="Room No"
                        options={roomNumberOptions}
                      />
                    </Flex>
                  </Flex>
                </Spacer>
              </Flex>
              <Flex>
                <Spacer margin={[0, 2, 4, 0]}>
                  <Flex flexDirection="column">
                    <Spacer margin={[0, 0, 1, 0]}>
                      <Text size="xs" color="slate">
                          Room Details
                      </Text>
                    </Spacer>
                    <Text size="m" weight="bold">
                        1 Adult for 2 Nights
                    </Text>
                  </Flex>
                </Spacer>
              </Flex>
              <Flex>
                <Text size="xs" color="slate">
                    Upgrade mode
                </Text>
              </Flex>
              <Spacer margin={[0, 0, 2, 0]}>
                <Flex>
                  <Spacer margin={[0, 1, 0, 0]}>
                    <RadioButton
                      name="upgradeType"
                      label="Paid Upgrade"
                      value="paid"
                    />
                  </Spacer>
                  <Spacer margin={[0, 1, 0, 1]}>
                    <RadioButton
                      name="upgradeType"
                      label="Free Upgrade"
                      value="free"
                    />
                  </Spacer>
                </Flex>
              </Spacer>

            </Modal.Content>
            <Modal.Footer>
              <Flex justifyContent="space-between" alignItems="center">
                <Flex>
                  <Spacer margin={[0, 1, 0, 0]}>
                    <Text size="l" weight="bold">
                      Total Payable
                    </Text>
                  </Spacer>
                  <Text size="l" weight="bold">
                    <Price>
                      1825
                    </Price>
                  </Text>
                </Flex>
                <Flex
                  justifyContent="space-between"
                >
                  <Button
                    type="submit"
                    size="small"
                  >
                    CHANGE ROOM
                  </Button>
                </Flex>
              </Flex>
            </Modal.Footer>
          </Form.Form>
        </Form>
      </Modal>
    );
  }
}

ChangeRoom.propTypes = {
  tabIndex: PropTypes.string,
  flowIndex: PropTypes.string,
  removeFlow: PropTypes.func.isRequired,
  roomNumber: PropTypes.string,
  roomType: PropTypes.string,
};

export default compose(
  graphql(tabMutations.REMOVE_FLOW, { name: 'removeFlow' }),
)(ChangeRoom);
