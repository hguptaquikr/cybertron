import React from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { graphql, compose } from 'react-apollo';
import isEmpty from 'cybertron-utils/isEmpty';
import Card from 'leaf-ui/Card/web';
import Select from 'leaf-ui/Select/web';
import TextInput from 'leaf-ui/TextInput/web';
import Flex from 'leaf-ui/Flex/web';
import Button from 'leaf-ui/Button/web';
import Spacer from 'leaf-ui/Spacer/web';
import Form from 'leaf-ui/Form/web';

class RoomItem extends React.Component {
  formatPrice = (price, round) =>
    (round ? Math.round(price) : price).toLocaleString('en-IN');

  render() {
    const {
      index,
      type,
      onRemove,
      isInclusiveofTaxes,
      values,
      $roomItem,
    } = this.props;

    const selectedRoomType = $roomItem.loading ?
      {} : $roomItem.hotelById.roomTypes.find((roomType) => roomType.name === type);

    const postTax = !isEmpty(selectedRoomType.price) ?
      selectedRoomType.price.ratePlans[0].postTax : 0;
    const preTax = !isEmpty(selectedRoomType.price) ?
      selectedRoomType.price.ratePlans[0].preTax : 0;
    const editPrice = isInclusiveofTaxes ?
      this.formatPrice(postTax, 2) : this.formatPrice(preTax, 2);

    const maxAdults = !isEmpty(selectedRoomType.maxOccupancy) ?
      Array(selectedRoomType.maxOccupancy.adults).fill(1).map((item, i) => ({
        label: i,
        value: i,
      })) : [{
        label: 1,
        value: 1,
      }];

    const maxKids = !isEmpty(selectedRoomType.maxOccupancy) ?
      Array(selectedRoomType.maxOccupancy.children).fill(1).map((item, i) => ({
        label: i,
        value: i,
      })) : [{
        label: 0,
        value: 0,
      }];

    return (
      <React.Fragment key={index}>
        <Spacer padding={[1, 0]}>
          <Card>
            <Form.Form>
              <Flex flexDirection="column">
                <Flex justifyContent="space-between" alignItems="center">
                  <Select
                    name={`roomTypes[${index}].noOfAdults`}
                    label="No. of Adults"
                    options={maxAdults}
                    defaultSelected={maxAdults[1]}
                    onChange={(noOfAdults) => $roomItem.refetch({
                      roomConfig: `${noOfAdults.value}-${(values.roomTypes && values.roomTypes[index] && values.roomTypes[index].noOfKids && values.roomTypes[index].noOfKids.value) || '0'}`,
                    })}
                  />
                  <Select
                    name={`roomTypes[${index}].noOfKids`}
                    label="No. of Kids"
                    options={maxKids}
                    defaultSelected={maxKids[0]}
                    onChange={(noOfKids) => $roomItem.refetch({
                      roomConfig: `${(values.roomTypes && values.roomTypes[index] && values.roomTypes[index].noOfAdults && values.roomTypes[index].noOfAdults.value) || '1'}-${noOfKids.value}`,
                    })}
                  />
                  <TextInput
                    name="editedPrice"
                    label="Edit Price"
                    defaultValue={editPrice}
                  />
                  <Button
                    color="red"
                    kind="outlined"
                    onClick={onRemove}
                  >
                    Remove Room
                  </Button>
                </Flex>
              </Flex>
            </Form.Form>
          </Card>
        </Spacer>
      </React.Fragment>
    );
  }
}

RoomItem.propTypes = {
  index: PropTypes.string,
  type: PropTypes.string,
  onRemove: PropTypes.func,
  isInclusiveofTaxes: PropTypes.bool,
  values: PropTypes.object,
  $roomItem: PropTypes.object,
};

export default compose(
  graphql(gql`
    query roomItem(
      $hotelId: ID!,
      $fromDate: String!,
      $toDate: String!,
      $roomConfig: String!,
    ) {
      hotelById(id: $hotelId) {
        roomTypes {
          name
          maxOccupancy {
            adults
            children
          }
          price (
            fromDate: $fromDate,
            toDate: $toDate,
            roomConfig: $roomConfig,
          ) {
            ratePlans {
              name
              preTax
              postTax
              tax
            }
          }
        }
      }
    }
  `, {
    name: '$roomItem',
    options: {
      variables: {
        hotelId: '0001661',
        fromDate: '2018-06-22',
        toDate: '2018-06-23',
        roomConfig: '1-0',
      },
    },
  }),
)(RoomItem);
