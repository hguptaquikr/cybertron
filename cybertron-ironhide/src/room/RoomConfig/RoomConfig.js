import React from 'react';
import PropTypes from 'prop-types';
import Card from 'leaf-ui/Card/web';
import Price from 'leaf-ui/Price/web';
import Link from 'leaf-ui/Link/web';
import Flex from 'leaf-ui/Flex/web';
import Text from 'leaf-ui/Text/web';
import Spacer from 'leaf-ui/Spacer/web';
import Form from 'leaf-ui/Form/web';

class RoomConfig extends React.Component {
  getRoomCount = (roomType) => {
    const { rows } = this.props;
    if (roomType.name in rows) {
      return (
        <Text size="xl" weight="bold">
          {roomType.availability.count - rows[roomType.name]} {roomType.name}
        </Text>
      );
    }
    return (
      <Text size="xl" weight="bold">
        {roomType.availability.count} {roomType.name}
      </Text>
    );
  }

  addRoom = (roomType) => {
    const { handleAddRoom } = this.props;
    handleAddRoom(roomType.name);
  }

  render() {
    const {
      className,
      roomTypes,
    } = this.props;

    return (
      <React.Fragment>
        <Spacer padding={[5]} margin={[0, 0, 1, 0]} className={className}>
          <Card>
            <Form.Form>
              <Flex>
                {
                  roomTypes.map((roomType) => (
                    <Flex flexDirection="column" flex="1">
                      <Spacer padding={[0, 0, 1, 1]}>
                        {this.getRoomCount(roomType)}
                      </Spacer>
                      <Spacer padding={[0, 0, 1, 1]}>
                        <Text color="greyDark">
                          <Price>{roomType.price.ratePlans[0].preTax}</Price>/Night
                        </Text>
                      </Spacer>
                      <Flex>
                        <Link onClick={() => this.addRoom(roomType)}>
                         ADD ROOM
                        </Link>
                      </Flex>
                    </Flex>
                  ))
                }
              </Flex>
            </Form.Form>
          </Card>
        </Spacer>
      </React.Fragment>
    );
  }
}

RoomConfig.propTypes = {
  className: PropTypes.string,
  roomTypes: PropTypes.object,
  rows: PropTypes.array,
  handleAddRoom: PropTypes.func,
};

export default RoomConfig;
