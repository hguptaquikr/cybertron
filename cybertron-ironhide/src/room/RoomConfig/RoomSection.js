import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'cybertron-utils/isEmpty';
import Card from 'leaf-ui/Card/web';
import Text from 'leaf-ui/Text/web';
import Spacer from 'leaf-ui/Spacer/web';
import Checkbox from 'leaf-ui/Checkbox/web';
import RoomItem from './RoomItem';

class RoomSection extends React.Component {
  removeRoom = (roomType) => {
    const { handleRemoveRoom } = this.props;
    handleRemoveRoom(roomType);
  }

  render() {
    const {
      className,
      rows,
      values,
    } = this.props;

    return (
      <React.Fragment>
        {
          !isEmpty(rows) && Object.values(rows).filter(Boolean).length ? (
            <Spacer padding={[3, 5, 3, 5]} className={className}>
              <Card>
                <Checkbox
                  name="showInclusiveAllTaxes"
                  label="Show inclusive of all taxes"
                />
                {
                  Object.keys(rows).map((type) => (
                    <Card>
                      {
                        rows[type] ?
                          <Text size="xl" weight="bold">{type}</Text>
                        : null
                      }
                      {
                        Array(rows[type]).fill(1).map((item, i) => (
                          <RoomItem
                            index={`${type}_${i}`}
                            type={type}
                            onRemove={() => this.removeRoom(type, i)}
                            isInclusiveofTaxes={values.showInclusiveAllTaxes}
                            values={values}
                          />
                        ))
                      }
                    </Card>
                  ))
                }
              </Card>
            </Spacer>
          ) : null
         }
      </React.Fragment>
    );
  }
}

RoomSection.propTypes = {
  className: PropTypes.string,
  values: PropTypes.object,
  rows: PropTypes.array,
  handleRemoveRoom: PropTypes.func,
};

export default RoomSection;
