import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import { connect } from 'react-redux';
import isEmpty from 'cybertron-utils/isEmpty';
import Card from 'leaf-ui/Card/web';
import Tag from 'leaf-ui/Tag/web';
import List from 'leaf-ui/List/web';
import Text from 'leaf-ui/Text/web';
import Spacer from 'leaf-ui/Spacer/web';
import Flex from 'leaf-ui/Flex/web';
import Divider from 'leaf-ui/Divider/web';
import Dropdown from 'leaf-ui/Dropdown/web';
import Icon from 'leaf-ui/Icon/web';
import * as tabQueries from '../../tab/tabQueries';
import * as tabMutations from '../../tab/tabMutations';

class RoomStayList extends React.Component {
  openFlow = (flow) => () => {
    const { $selectedTabIndex, addFlow } = this.props;
    addFlow({
      variables: {
        tabIndex: $selectedTabIndex.selectedTabIndex,
        flow,
      },
    });
  }

  render() {
    const {
      className,
      booking,
      bookingId,
      $guest,
    } = this.props;

    return (
      <Spacer className={className} padding={[5]}>
        <Card>
          <Spacer margin={[0, 0, 4, 0]}>
            <Text size="xxl" weight="bold">2 Rooms (3 Adults & 1 Kid)</Text>
          </Spacer>
          {
            !isEmpty(booking.byId[bookingId]) ?
            booking.byId[bookingId].roomStays.map((bookingRoom) => (
              <React.Fragment key={bookingRoom.roomNumber}>
                <Spacer margin={[0, 0, 2, 0]}>
                  <Flex justifyContent="space-between" alignItems="center">
                    <Text size="m" weight="bold">
                      {bookingRoom.roomNumber}{bookingRoom.roomType}
                    </Text>
                    <Dropdown placement="top">
                      <Dropdown.Trigger>
                        <Icon name="more_horiz" />
                      </Dropdown.Trigger>
                      <Dropdown.Content>
                        <List>
                          <List.Item
                            onClick={this.openFlow({
                              component: 'AddCharge',
                              props: { bookingId: 1 },
                            })}
                          >
                            Add Charges
                          </List.Item>
                          <List.Item
                            onClick={this.openFlow({
                              component: 'AddGuest',
                              props: { bookingId: 1 },
                            })}
                          >
                            Add Guest
                          </List.Item>
                          <List.Item
                            onClick={this.openFlow({
                              component: 'ChangeRoom',
                              props: {
                                bookingId: 1,
                                roomNumber: bookingRoom.roomNumber,
                                roomType: bookingRoom.roomType,
                              },
                            })}
                          >
                            Change Room
                          </List.Item>
                          <List.Item
                            onClick={this.openFlow({
                              component: 'ChangeStayDates',
                              props: { bookingId: 1 },
                            })}
                          >
                            Change Stay Dates
                          </List.Item>
                          <List.Item
                            onClick={this.openFlow({
                              component: 'EditAddOn',
                              props: { bookingId: 1 },
                            })}
                          >
                            Edit Add-On
                          </List.Item>
                          <List.Item
                            onClick={this.openFlow({
                              component: 'RemoveGuest',
                              props: { bookingId: 1 },
                            })}
                          >
                            Remove Guests
                          </List.Item>
                          <List.Item
                            onClick={this.openFlow({
                              component: 'RemoveRoom',
                              props: { bookingId: 1 },
                            })}
                          >
                            Remove Room
                          </List.Item>
                          <List.Item
                            onClick={this.openFlow({
                              component: 'CheckOut',
                              props: { bookingId: 1 },
                            })}
                          >
                            Check Out
                          </List.Item>
                        </List>
                      </Dropdown.Content>
                    </Dropdown>
                  </Flex>
                </Spacer>
                <Divider />
                <Flex flexDirection="column">
                  <List>
                    {
                      bookingRoom.guestIds.map((guestId) => (
                        <List.Item key={guestId} onClick={() => {}}>
                          <Flex justifyContent="space-between" alignItems="center">
                            <Text color="slate">{$guest.byId[guestId].name}</Text>
                            <Tag
                              color="primary"
                              kind="outlined"
                              shape="capsular"
                            >
                              {$guest.byId[guestId].status}
                            </Tag>
                          </Flex>
                        </List.Item>
                      ))
                    }
                  </List>
                </Flex>
              </React.Fragment>
            )) : null
          }
        </Card>
      </Spacer>
    );
  }
}

RoomStayList.propTypes = {
  className: PropTypes.string,
  booking: PropTypes.object,
  bookingId: PropTypes.string,
  $guest: PropTypes.object,
  $selectedTabIndex: PropTypes.object,
  addFlow: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  booking: state.$booking,
  $guest: state.$guest,
});

export default compose(
  connect(mapStateToProps),
  graphql(tabQueries.SELECTED_TAB_INDEX, { name: '$selectedTabIndex' }),
  graphql(tabMutations.ADD_FLOW, { name: 'addFlow' }),
)(RoomStayList);
