import { withFilter } from 'graphql-subscriptions';
import pubsub from '../bootstrap/pubsub';

// THIS EVENT IS JUST AN EXAMPLE
// IT DOES NOT EXIST ON THE BMS_QUEUE

// TRIGGER FAKE SUBSCRIPTION PUSHES
// setInterval(() => {
//   console.log('hotelUpdated:publish');
//   pubsub.publish('hotelUpdated', {
//     hotelId: '0001661',
//   });
// }, 10000);

const typeDefs = `
  extend type Subscription {
    hotelUpdated(id: ID!): Hotel!
  }
`;

const resolvers = {
  Subscription: {
    hotelUpdated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator('hotelUpdated'),
        (payload, args) => payload.hotelId === args.id,
      ),
      resolve: (payload, args, context, info) =>
        info.mergeInfo.delegateToSchema({
          schema: info.schema,
          operation: 'query',
          fieldName: 'hotelById',
          args: {
            id: payload.hotelId,
          },
          context,
          info,
        }),
    },
  },
};

export default {
  typeDefs,
  resolvers,
};
