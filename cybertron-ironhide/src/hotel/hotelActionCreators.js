import * as hotelActionCreators from 'cybertron-service-hotel/lib/hotelActionCreators';
import * as hotelActionTypes from './hotelActionTypes';

export * from 'cybertron-service-hotel/lib/hotelActionCreators';

export const fetchHotels = (hotelId) =>
  (dispatch, getState, { request }) =>
    dispatch({
      type: hotelActionTypes.FETCH_HOTELS,
      promise: request.get(`/api/crs/v1/hotel/${hotelId}`)
        .then((response) => {
          const hotels = response.map((hotel) => ({
            id: hotel.id,
            bookingIds: hotel.bookingIds,
            roomIds: hotel.roomIds,
            roomTypeIds: hotel.roomTypeIds,
            name: hotel.name,
            coordinates: hotel.coordinates,
            images: hotel.images,
            area: hotel.area,
          }));
          return dispatch(hotelActionCreators.setHotels(hotels));
        }),
    });

export const selectHotel = (hotelId) => ({
  type: hotelActionTypes.SELECT_HOTEL,
  payload: {
    hotelId,
  },
});
