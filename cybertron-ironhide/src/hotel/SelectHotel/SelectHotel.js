import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import isEmpty from 'cybertron-utils/isEmpty';
import Card from 'leaf-ui/Card/web';
import Flex from 'leaf-ui/Flex/web';
import List from 'leaf-ui/List/web';
import Spacer from 'leaf-ui/Spacer/web';
import Text from 'leaf-ui/Text/web';
import TextInput from 'leaf-ui/TextInput/web';
import * as bookingActionCreators from '../../booking/bookingActionCreators';
import * as hotelActionCreators from '../../hotel/hotelActionCreators';
import * as tabMutations from '../../tab/tabMutations';

class SelectHotel extends React.Component {
  state = {
    searchText: '',
  }

  onHotelSelected = (selectedHotelId) => () => {
    const { addTab, hotelActions } = this.props;
    hotelActions.selectHotel(selectedHotelId);
    addTab({
      variables: {
        tab: {
          pinned: true,
          flows: [{
            component: 'HouseView',
          }],
        },
      },
    });
  }

  getHotels = () => {
    const { searchText } = this.state;
    const { $hotel, $auth } = this.props;

    return !isEmpty($auth.user.hotelIds) ? $auth.user.hotelIds
      .map((hotelId) => $hotel.byId[hotelId])
      .filter((hotel) => hotel.name.includes(searchText))
      : [];
  }

  setSearchText = (e) => {
    this.setState({
      searchText: e.target.value,
    });
  }

  render() {
    const { searchText } = this.state;
    const hotels = this.getHotels();

    return (
      <Spacer height="100vh">
        <Flex alignItems="center" justifyContent="center">
          <Spacer padding={[5]} width="800px">
            <Card>
              <Text
                size="xxxl"
                weight="bold"
                align="center"
              >
                Select Hotel
              </Text>
              <Spacer margin={[4, 0]} width="680px">
                <div>
                  <TextInput
                    name="searchText"
                    label="Search by Hotel Name"
                    onChange={this.setSearchText}
                    value={searchText}
                    block
                  />
                  <List>
                    {
                      hotels.map((hotel) => (
                        <List.Item onClick={this.onHotelSelected(hotel.id)}>
                          {hotel.name}
                        </List.Item>
                      ))
                    }
                  </List>
                </div>
              </Spacer>
            </Card>
          </Spacer>
        </Flex>
      </Spacer>
    );
  }
}

SelectHotel.propTypes = {
  $hotel: PropTypes.object.isRequired,
  $auth: PropTypes.object.isRequired,
  hotelActions: PropTypes.object.isRequired,
  addTab: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  $hotel: state.$hotel,
  $auth: state.$auth,
});

const mapDispatchToProps = (dispatch) => ({
  bookingActions: bindActionCreators(bookingActionCreators, dispatch),
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  graphql(tabMutations.ADD_TAB, { name: 'addTab' }),
  graphql(tabMutations.REMOVE_TAB, { name: 'removeTab' }),
)(SelectHotel);
