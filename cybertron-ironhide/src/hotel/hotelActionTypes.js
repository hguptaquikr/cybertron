export const FETCH_HOTELS = 'hotel/FETCH_HOTELS';
export const DEMOLISH_HOTELS = 'hotel/DEMOLISH_HOTELS';
export const SELECT_HOTEL = 'hotel/SELECT_HOTEL';
