import * as hotelServiceReducer from 'cybertron-service-hotel/lib/hotelReducer';
import flattenReducers from 'cybertron-utils/flattenReducers';
import * as hotelActionTypes from './hotelActionTypes';

const initialState = {
  selectedHotelId: '',
};

const hotelReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case hotelActionTypes.SELECT_HOTEL:
      return {
        ...state,
        selectedHotelId: payload.hotelId,
      };

    default:
      return state;
  }
};

export default flattenReducers({
  byId: hotelServiceReducer.reducer,
}, hotelReducer);
