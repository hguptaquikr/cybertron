import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Text from 'leaf-ui/Text/web';
import Button from 'leaf-ui/Button/web';
import * as hotelActionCreators from './hotelActionCreators';

class Hotel extends React.Component {
  componentDidMount() {
    const {
      hotelActions,
    } = this.props;

    hotelActions.fetchHotels('1');
  }

 deleteHotels = (hotelId) => {
   const {
     hotelActions,
   } = this.props;
   hotelActions.deleteHotels(hotelId);
 }

 updateHotels = () => {
   const {
     hotelActions,
   } = this.props;

   const updatedHotel = [{
     id: 1,
     bookingIds: ['B1', 'B2'],
     roomIds: [1, 2, 3, 4],
     roomTypeIds: ['h12/oak', 'h12/maple'],
     name: 'Treebo Akshaya Lalbagh Inn',
     coordinates: {
       lat: 12.943555,
       lng: 77.583549,
     },
     images: [
       {
         id: 10782,
         url: '//images.treebohotels.com/files/./Treebo_Akshaya_Lalbagh_Inn/Akshaya_Lalbagh_Oak_Queen_03.jpg',
         is_showcased: true,
         tagline: 'Treebo Akshaya Lalbagh Inn',
         main: './Treebo_Akshaya_Lalbagh_Inn/Akshaya_Lalbagh_Oak_Queen_03.jpg',
       },
       {
         id: 4670,
         url: '//images.treebohotels.com/files/./Treebo_Akshaya_Lalbagh_Inn/1Treebo_Akshaya_Lalbagh_Main.jpg',
         is_showcased: false,
         tagline: 'Treebo Akshaya Lalbagh Inn',
         main: './Treebo_Akshaya_Lalbagh_Inn/1Treebo_Akshaya_Lalbagh_Main.jpg',
       },
     ],
     area: {
       locality: 'Bannerghatta',
       city: 'Bengaluru',
       country: 'India',
       pincode: 560070,
       'state:': 'Karnataka',
     },
   }];
   hotelActions.updateHotels(updatedHotel);
 }
 render() {
   const {
     $hotel,
   } = this.props;

   const name = $hotel.byId['1'] ? $hotel.byId['1'].name : '';

   return (
     <React.Fragment>
       <Text>Hotel Name : {name}</Text>
       <Button onClick={() => this.deleteHotels('1')}>Remove this hotel</Button>
       <Button onClick={() => this.updateHotels()}>Update this hotel</Button>
     </React.Fragment>
   );
 }
}

Hotel.propTypes = {
  $hotel: PropTypes.object,
  hotelActions: PropTypes.object,
};

const mapStateToProps = (state) => ({
  $hotel: state.$hotel,
});

const mapStateToDispatch = (dispatch) => ({
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
});

export default connect(mapStateToProps, mapStateToDispatch)(Hotel);
