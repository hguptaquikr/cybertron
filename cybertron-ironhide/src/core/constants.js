export const dateFormat = {
  query: 'YYYY-MM-DD',
  view: 'ddd, D MMM',
};

export const bookingConstants = {
  status: {
    checkedOut: 'CHECKED_OUT',
    checkedIn: 'CHECKED_IN',
    reserved: 'RESERVED',
    temporary: 'TEMP',
    payAtHotel: 'PAY_AT_HOTEL',
    dnr: 'DNR',
  },

  statusToColorMap: {
  },
};

bookingConstants.statusToColorMap = {
  [bookingConstants.status.checkedOut]: 'grey',
  [bookingConstants.status.checkedIn]: 'teal',
  [bookingConstants.status.reserved]: 'blue',
  [bookingConstants.status.temp]: 'white',
  [bookingConstants.status.payAtHotel]: 'blue',
  [bookingConstants.status.dnr]: 'red',
};
