import express from 'express';
import proxy from 'http-proxy-middleware';
import config from '../config';

const router = express.Router();

router.use(proxy({
  target: config.pricingUrl,
  pathRewrite: { '^/api/pricing': '/api' },
  changeOrigin: true,
}));

export default router;
