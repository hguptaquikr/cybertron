import 'babel-polyfill';
import 'isomorphic-fetch';
import http from 'http';
import express from 'express';
import helmet from 'helmet';
import compression from 'compression';
import slashes from 'connect-slashes';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { execute, subscribe } from 'graphql';
import logger from 'cybertron-utils/logger';
import expressLoggerMiddleware from 'cybertron-utils/expressLoggerMiddleware';
import renderMiddleware from './render/renderMiddleware';
import crsExpressMiddleware from './crs/crsExpressMiddleware';
import pricingExpressMiddleware from './pricing/pricingExpressMiddleware';
import getServerSchema from './bootstrap/getServerSchema';

const { PORT } = process.env;
const app = express();
const server = http.createServer(app);

app.set('trust proxy', true);
app.use(helmet({ dnsPrefetchControl: false }));
app.use(compression());
app.use(expressLoggerMiddleware);
app.use('/health', (req, res) => res.send({ 'cybertron-ironhide': true }));
app.use('/serviceWorker.js', express.static('build/client/serviceWorker.js'));
app.use('/ironhide/build/client', express.static('build/client'));
app.use(slashes(true));
app.use('/api/crs', crsExpressMiddleware);
app.use('/api/pricing', pricingExpressMiddleware);
app.use('/graphql/', express.json(), graphqlExpress(async () => ({
  schema: await getServerSchema(),
})));
app.use('/graphiql/', express.json(), graphiqlExpress({
  endpointURL: '/graphql/',
  subscriptionsEndpoint: 'ws://localhost:8000/subscriptions/',
}));
app.use(renderMiddleware);

server.listen(PORT, async () => {
  logger.info(`cybertron-ironhide is running as ${__APP_ENV__} on port ${PORT}`);
  // eslint-disable-next-line no-new
  new SubscriptionServer({
    execute,
    subscribe,
    schema: await getServerSchema(),
    // onOperation: (message, params) => ({ ...params, context: {} }),
  }, {
    server,
    path: '/subscriptions/',
  });
});
