/* eslint-disable max-len, import/no-unresolved */
import assetsManifest from '../../build/client/assetsManifest.json';
import * as scripts from './scripts';

export default (ip) => `
  <!doctype html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="preconnect" href="//static.treebohotels.com">
      <link rel="preconnect" href="//images.treebohotels.com">
      <link rel="shortcut icon" href="//images.treebohotels.com/images/rodimus/favicon.ico?v=5A5wklOX2k">
    </head>
    <body>
      <div id="root"></div>
      <script>${scripts.analytics(ip)}</script>
      <script src="${assetsManifest.webpackManifest.js}"></script>
      <script src="${assetsManifest.vendor.js}"></script>
      <script src="${assetsManifest.main.js}"></script>
      ${__LOCAL__ ? '' : `<script>${scripts.serviceWorker}</script>`}
    </body>
  </html>
`;
