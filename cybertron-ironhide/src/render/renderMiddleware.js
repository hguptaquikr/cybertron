import html from './html';

export default (req, res) => {
  res.send(html(req.ip));
};
