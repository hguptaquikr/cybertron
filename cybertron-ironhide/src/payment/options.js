export const paymentModes = [{
  label: 'Cash',
  value: 'cash',
}, {
  label: 'Debit Card',
  value: 'debitCard',
}, {
  label: 'Bank Transfer',
  value: 'bankTransfer',
}];

export const paymentTypes = [{
  label: 'Online',
  value: 'online',
}, {
  label: 'Offline',
  value: 'offline',
}];

export const paidBy = [{
  label: 'Guest',
  value: 'guest',
}];

export const paidTo = [{
  label: 'Hotel',
  value: 'hotel',
}, {
  label: 'Treebo',
  value: 'treebo',
}];
