import React from 'react';
import PropTypes from 'prop-types';
import Flex from 'leaf-ui/Flex/web';
import Card from 'leaf-ui/Card/web';
import Text from 'leaf-ui/Text/web';
import Spacer from 'leaf-ui/Spacer/web';
import Price from 'leaf-ui/Price/web';
import Table from 'leaf-ui/Table/web';

class PaymentDetails extends React.Component {
  state = {};

  render() {
    const { className } = this.props;
    return (
      <Spacer padding={[4]} className={className}>
        <Card>
          <Flex flexDirection="column">
            <Spacer padding={[0, 0, 4, 0]}>
              <Text size="xxl" weight="bold">Payments</Text>
            </Spacer>
            <Table>
              <Table.TBody>
                <Table.TR>
                  <Table.TH>Type</Table.TH>
                  <Table.TH>Date</Table.TH>
                  <Table.TH>ID</Table.TH>
                  <Table.TH>Paid By</Table.TH>
                  <Table.TH>Paid To</Table.TH>
                  <Table.TH>Mode</Table.TH>
                  <Table.TH>Via</Table.TH>
                  <Table.TH>Amount</Table.TH>
                </Table.TR>
                <Table.TR>
                  <Table.TD>1000</Table.TD>
                  <Table.TD>2</Table.TD>
                  <Table.TD>Pragya</Table.TD>
                  <Table.TD>10/11/2018</Table.TD>
                  <Table.TD>oak</Table.TD>
                  <Table.TD>reserved</Table.TD>
                  <Table.TD>2000</Table.TD>
                  <Table.TD>2000</Table.TD>
                </Table.TR>
              </Table.TBody>
            </Table>
            <Flex alignSelf="flex-end">
              <Card>
                <Flex flexDirection="column">
                  <Spacer margin={[4, 0, 3, 0]}>
                    <Flex justifyContent="space-between">
                      <Text color="slate">Total Paid</Text>
                      <Text color="slate"><Price>300</Price></Text>
                    </Flex>
                  </Spacer>
                </Flex>
                <Flex justifyContent="space-between">
                  <Spacer margin={[0, 3, 0, 0]}>
                    <Flex flexDirection="column">
                      <Spacer margin={[0, 0, 1, 0]}>
                        <Text size="xl" weight="bold">Balance to be Paid</Text>
                      </Spacer>
                      <Text size="xs" color="rock">Incl. of all taxes</Text>
                    </Flex>
                  </Spacer>
                  <Text size="xxl" weight="bold"><Price>500</Price></Text>
                </Flex>
              </Card>
            </Flex>
          </Flex>
        </Card>
      </Spacer>
    );
  }
}

PaymentDetails.propTypes = {
  className: PropTypes.string,
};

export default PaymentDetails;
