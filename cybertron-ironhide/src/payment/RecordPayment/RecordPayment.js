import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import Form from 'leaf-ui/Form/web';
import TextInput from 'leaf-ui/TextInput/web';
import Modal from 'leaf-ui/Modal/web';
import Flex from 'leaf-ui/Flex/web';
import Text from 'leaf-ui/Text/web';
import Spacer from 'leaf-ui/Spacer/web';
import RadioButton from 'leaf-ui/RadioButton/web';
import Select from 'leaf-ui/Select/web';
import Button from 'leaf-ui/Button/web';
import * as tabMutations from '../../tab/tabMutations';
import * as tabHelpers from '../../tab/tabHelpers';
import * as options from '../options';

class RecordPayment extends React.Component {
  confirmRecordPayment = () => {
    setTimeout(this.closeRecordPayment, 1000);
  }

  closeRecordPayment = () => {
    const { tabIndex, flowIndex, removeFlow } = this.props;
    removeFlow({
      variables: {
        tabIndex,
        flowIndex,
      },
    });
  }

  render() {
    return (
      <Modal
        isOpen
        onClose={this.closeRecordPayment}
        container={() => tabHelpers.makeFlowId(this.props)}
      >
        <Form
          initialValues={{
            transactionType: 'payment',
            paidBy: options.paidBy[0],
            paidTo: options.paidTo[0],
          }}
          validationSchema={
            Form.validation.object().shape({
              paymentMode: Form.validation.object().required(),
              paymentType: Form.validation.object().required(),
              amount: Form.validation.number().required().positive().integer(),
            })
          }
          onSubmit={this.confirmRecordPayment}
        >
          <Form.Form>
            <Modal.Header>
              <Text size="xxxl">
                Record Payment
              </Text>
            </Modal.Header>
            <Modal.Content>
              <Flex>
                <Spacer margin={[0, 2, 0, 0]}>
                  <RadioButton
                    name="transactionType"
                    label="Payment"
                    value="payment"
                  />
                </Spacer>
                <RadioButton
                  name="transactionType"
                  label="Refund"
                  value="refund"
                />
              </Flex>
              <Flex>
                <Spacer margin={[0, 2, 0, 0]}>
                  <Select
                    name="paymentMode"
                    label="Payment Mode"
                    options={options.paymentModes}
                  />
                </Spacer>
                <Spacer margin={[0, 2, 0, 0]}>
                  <Select
                    name="paymentType"
                    label="Type"
                    options={options.paymentTypes}
                  />
                </Spacer>
                <TextInput
                  name="amount"
                  label="Amount"
                />
              </Flex>
              <Spacer margin={[1, 1, 1, 0]}>
                <TextInput
                  name="comments"
                  label="Enter your comments"
                />
              </Spacer>
              <Flex>
                <Spacer margin={[0, 2, 0, 0]}>
                  <Select
                    name="paidBy"
                    label="Paid By"
                    options={options.paidBy}
                  />
                </Spacer>
                <Select
                  name="paidTo"
                  label="Paid To"
                  options={options.paidTo}
                />
              </Flex>
            </Modal.Content>
            <Modal.Footer>
              <Flex justifyContent="flex-end">
                <Button type="submit">
                  RECORD PAYMENT
                </Button>
              </Flex>
            </Modal.Footer>
          </Form.Form>
        </Form>
      </Modal>
    );
  }
}

RecordPayment.propTypes = {
  tabIndex: PropTypes.string,
  flowIndex: PropTypes.string,
  removeFlow: PropTypes.func.isRequired,
};

export default compose(
  graphql(tabMutations.REMOVE_FLOW, { name: 'removeFlow' }),
)(RecordPayment);
