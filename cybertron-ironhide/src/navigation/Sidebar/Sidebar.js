import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import Text from 'leaf-ui/Text/web';
import Link from 'leaf-ui/Link/web';
import Flex from 'leaf-ui/Flex/web';
import Card from 'leaf-ui/Card/web';
import Divider from 'leaf-ui/Divider/web';
import List from 'leaf-ui/List/web';
import Spacer from 'leaf-ui/Spacer/web';
import Dropdown from 'leaf-ui/Dropdown/web';
import Icon from 'leaf-ui/Icon/web';
import * as tabMutations from '../../tab/tabMutations';

class Sidebar extends React.Component {
  componentDidMount() {}

  render() {
    const { addTab } = this.props;
    return (
      <Spacer width="240px">
        <Card color="primary" style={{ display: 'flex', flexDirection: 'column' }}>
          <Spacer padding={[3]}>
            <Flex>
              <Spacer margin={[0, 1, 0, 0]}>
                <Icon color="primaryLighter" name="home" />
              </Spacer>
              <Flex flexDirection="column">
                <Text color="primaryLighter" size="l" weight="bold">Treebo</Text>
                <Text color="primaryLighter" size="l" weight="bold">Front Desk</Text>
              </Flex>
            </Flex>
          </Spacer>
          <Flex flexDirection="column" flex="auto">
            <Spacer margin={[0, 0, 3, 0]}>
              <Divider color="primaryLight" />
            </Spacer>
            <Spacer padding={[0, 3]}>
              <Text color="primaryLight" size="xs">TODAY&#39;S STATUS</Text>
            </Spacer>
            <List>
              <Link to="/checkin/">
                <Spacer padding={[2, 3]}>
                  <List.Item onClick={() => {}}>
                    <Text color="primaryLighter">
                      <Text weight="bold" component="span">12</Text> Check In
                    </Text>
                  </List.Item>
                </Spacer>
              </Link>
              <Link to="/checkout/">
                <Spacer padding={[2, 3]}>
                  <List.Item onClick={() => {}}>
                    <Text color="primaryLighter">
                      <Text weight="bold" component="span">04</Text> Check Out
                    </Text>
                  </List.Item>
                </Spacer>
              </Link>
              <Link to="/dnr/">
                <Spacer padding={[2, 3]}>
                  <List.Item onClick={() => {}}>
                    <Text color="primaryLighter">
                      <Text weight="bold" component="span">01</Text> DNR
                    </Text>
                  </List.Item>
                </Spacer>
              </Link>
            </List>
            <Spacer margin={[0, 0, 3, 0]}>
              <Divider color="primaryLight" />
            </Spacer>
            <Spacer padding={[0, 3]}>
              <Text color="primaryLight" size="xs">HOT LINKS</Text>
            </Spacer>
            <List>
              <Spacer padding={[2, 3]}>
                <List.Item
                  onClick={() => {
                    addTab({
                      variables: {
                        tab: {
                          flows: [{
                            component: 'NewBooking',
                          }],
                        },
                      },
                    });
                  }}
                >
                  <Text color="primaryLighter">New Booking</Text>
                </List.Item>
              </Spacer>
              <Link to="/search/">
                <Spacer padding={[2, 3]}>
                  <List.Item
                    onClick={() => {
                      addTab({
                        variables: {
                          tab: {
                            flows: [{
                              component: 'Search',
                            }],
                          },
                        },
                      });
                    }}
                  >
                    <Text color="primaryLighter">Search</Text>
                  </List.Item>
                </Spacer>
              </Link>
            </List>
          </Flex>
          <Divider color="primaryLight" />
          <Spacer padding={[3]}>
            <Flex justifyContent="space-between" alignItems="center">
              <Flex flexDirection="column">
                <Text color="primaryLighter">Pragya Jha</Text>
                <Text color="primaryLight" size="xs">Treebo Mayflower Hayz</Text>
              </Flex>
              <Dropdown placement="top">
                <Dropdown.Trigger>
                  <Icon color="primaryLighter" name="more_horiz" />
                </Dropdown.Trigger>
                <Dropdown.Content>
                  <List>
                    <List.Item onClick={() => {}}>Change Hotel</List.Item>
                    <List.Item onClick={() => {}}>Technical Support</List.Item>
                    <List.Item onClick={() => {}}>Logout</List.Item>
                  </List>
                </Dropdown.Content>
              </Dropdown>
            </Flex>
          </Spacer>
        </Card>
      </Spacer>
    );
  }
}

Sidebar.propTypes = {
  addTab: PropTypes.object,
};

export default compose(
  graphql(tabMutations.ADD_TAB, { name: 'addTab' }),
)(Sidebar);
