import { handle } from 'redux-pack';
import addDays from 'date-fns/add_days';
import * as guestActionTypes from './guestActionTypes';
import * as bookingActionTypes from '../booking/bookingActionTypes';

export const initialState = {
  byId: {
    g1: {
      id: 'g1',
      name: 'Pragya Jha',
      checkIn: new Date(),
      checkOut: addDays(new Date(), 2),
      status: 'checked in',
      isAdult: true,
      roomStayGuestMappingId: 26794,
    },
    g2: {
      id: 'g2',
      name: 'Lakshya Ranganath',
      checkIn: new Date(),
      checkOut: addDays(new Date(), 2),
      status: 'expected',
      isAdult: true,
      roomStayGuestMappingId: 26793,
    },
  },
  ids: ['g1', 'g2'],
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case guestActionTypes.GET_ALL: return handle(state, action, {
      start: (s) => ({
        ...s,
        isLoading: true,
      }),
      success: (s) => ({
        ...s,
        byId: payload.byId,
        ids: payload.ids,
      }),
      failure: (s) => ({
        ...s,
        error: payload.error,
      }),
      finish: (s) => ({
        ...s,
        isLoading: false,
      }),
    });

    case guestActionTypes.REMOVE_GUEST: return handle(state, action, {
      start: (s) => ({
        ...s,
        isLoading: true,
      }),
      success: (s) => ({
        ...s,
        byId: payload.byId,
        ids: payload.ids,
      }),
      failure: (s) => ({
        ...s,
        error: payload.error,
      }),
      finish: (s) => ({
        ...s,
        isLoading: false,
      }),
    });

    case bookingActionTypes.GET_BOOKING: return handle(state, action, {
      success: (s) => ({
        ...s,
      }),
    });

    default:
      return state;
  }
};
