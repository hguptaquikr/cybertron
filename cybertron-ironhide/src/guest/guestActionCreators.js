import * as guestActionTypes from './guestActionTypes';

export const getAll = () => (dispatch, getState, { request }) =>
  dispatch({
    type: guestActionTypes.GET_ALL,
    promise: request.get('/api', {
      results: 3,
      inc: 'name,location,picture',
    }),
  });

export const getOne = () => (dispatch, getState, { request }) =>
  dispatch({
    type: guestActionTypes.GET_ONE,
    promise: request.get('/api', {
      results: 1,
      inc: 'name,location,picture',
    }),
  });

export const removeGuest = (booking, roomStayId, roomGuestMappingIds) =>
  (dispatch, getState, { request }) =>
    dispatch({
      type: guestActionTypes.REMOVE_GUEST,
      promise: request.post(`/api/crs/v1/booking/${booking.id}/remove-guest`, {
        version_id: booking.versionId,
        room_stay_id: roomStayId,
        room_guest_mapping_ids: roomGuestMappingIds,
      }),
    });
