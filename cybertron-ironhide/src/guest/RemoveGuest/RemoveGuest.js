import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import isEmpty from 'cybertron-utils/isEmpty';
import Text from 'leaf-ui/Text/web';
import Button from 'leaf-ui/Button/web';
import Flex from 'leaf-ui/Flex/web';
import Modal from 'leaf-ui/Modal/web';
import Spacer from 'leaf-ui/Spacer/web';
import Price from 'leaf-ui/Price/web';
import Tag from 'leaf-ui/Tag/web';
import Checkbox from 'leaf-ui/Checkbox/web';
import Form from 'leaf-ui/Form/web';
import * as tabHelpers from '../../tab/tabHelpers';
import * as guestActionCreators from '../guestActionCreators';
import * as bookingActionCreators from '../../booking/bookingActionCreators';

class RemoveGuest extends React.Component {
  state = {
    show: true,
  };

  onClose = () => {
    // this.setState({ show: false });
    // const {
    //   tabIndex,
    //   tabActions,
    // } = this.props;
    // tabActions.removeFlow([tabIndex], 'removeguest');
  }

  onRemoveGuest = (fields) => {
    console.log(fields);
    // const {
    //   booking,
    //   guest,
    //   guestActions,
    //   roomNumber,
    // } = this.props;

    // const selectedGuestIds = Object.keys(fields).filter((key) => fields[key] ? key : null);
    // const selectedRoomStayId = booking.roomStays.find((roomStay) =>
    //   roomStay.roomNumber === roomNumber).stayId;
    // const roomGuestMappingIds = selectedGuestIds.map((guestId) =>
    //   guest.byId[guestId].roomStayGuestMappingId);

    // guestActions.removeGuest(
    //   booking,
    //   selectedRoomStayId,
    //   roomGuestMappingIds,
    // );
  }

  render() {
    const {
      booking,
      $guest,
      roomNumber,
    } = this.props;
    const selectedRoomStay = !isEmpty(booking) ? booking.roomStays.find((roomStay) =>
      roomStay.roomNumber === roomNumber) : [];
    const roomStays = !isEmpty(booking) ? booking.roomStays : [];
    return (
      <Modal
        isOpen={this.state.show}
        onClose={() => this.onClose()}
        container={() => tabHelpers.makeFlowId(this.props)}
      >
        <React.Fragment>
          <Modal.Header>
            Remove Guests from {selectedRoomStay.roomNumber} {selectedRoomStay.roomType}
          </Modal.Header>
          <Form onSubmit={this.onRemoveGuest}>
            <Form.Form>
              <Modal.Content>
                {
                  roomStays.map((roomStay) => roomStay.roomNumber === roomNumber ? (
                    roomStay.guestIds.map((guestId) => (
                      <Flex key={guestId} alignItems="center" justifyContent="space-between">
                        <Checkbox name={guestId} label={$guest.byId[guestId].name} />
                        <Tag
                          color="red"
                          kind="outlined"
                          shape="capsular"
                        >
                          {$guest.byId[guestId].status}
                        </Tag>
                      </Flex>
                    ))
                  ) : null)
                }
              </Modal.Content>
              <Modal.Footer>
                <Flex justifyContent="space-between" alignItems="center">
                  <Flex>
                    <Spacer margin={[0, 1, 0, 0]}>
                      <Text size="l" weight="bold">Cancellation Fee</Text>
                    </Spacer>
                    <Text size="l" weight="bold">
                      <Price>0</Price>
                    </Text>
                  </Flex>
                  <Flex justifyContent="space-between">
                    <Button size="small" type="submit">REMOVE GUEST</Button>
                  </Flex>
                </Flex>
              </Modal.Footer>
            </Form.Form>
          </Form>
        </React.Fragment>
      </Modal>
    );
  }
}

RemoveGuest.propTypes = {
  booking: PropTypes.object,
  roomNumber: PropTypes.string,
  $guest: PropTypes.object,
};

const mapStateToProps = (state) => ({
  booking: state.$booking.byId.B1,
  roomNumber: 'R101',
  $guest: state.$guest,
});

const mapDispatchToProps = (dispatch) => ({
  guestActions: bindActionCreators(guestActionCreators, dispatch),
  bookingActions: bindActionCreators(bookingActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RemoveGuest);
