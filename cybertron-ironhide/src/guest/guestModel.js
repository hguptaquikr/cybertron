export const makeGuest = (guest) => ({
  id: guest.room_guest_mapping_id,
  name: {
    first: 'Pragya',
    last: 'Jha',
  },
  checkIn: guest.checkin,
  checkOut: guest.checkout,
  status: guest.status,
  isAdult: guest.adult,
  roomStayGuestMappingId: guest.guest_id,
});

export const normalize = (guests) => ({
  byId: guests.reduce((acc, guest) => ({
    ...acc,
    [guest.room_guest_mapping_id]: makeGuest(guest),
  }), {}),
  ids: guests.map((guest) => guest.room_guest_mapping_id),
});

export const getAll = (byId, ids) =>
  ids.map((guestId) => byId[guestId]);

export const getFullName = (name = {}) => {
  if (name.last) {
    return `${name.first} ${name.last}`;
  }
  return name.first;
};
