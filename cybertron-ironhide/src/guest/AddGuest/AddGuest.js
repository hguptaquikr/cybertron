import React from 'react';
import Text from 'leaf-ui/Text/web';
import Button from 'leaf-ui/Button/web';
import Flex from 'leaf-ui/Flex/web';
import Modal from 'leaf-ui/Modal/web';
import Spacer from 'leaf-ui/Spacer/web';
import Price from 'leaf-ui/Price/web';
import * as tabHelpers from '../../tab/tabHelpers';

class AddGuest extends React.Component {
  state = {
    show: true,
  };

  onClose = () => {
    this.setState({ show: false });
  }

  render() {
    return (
      <Modal
        isOpen={this.state.show}
        onClose={this.onClose}
        container={() => tabHelpers.makeFlowId(this.props)}
      >
        <React.Fragment>
          <Modal.Header>
            <Text size="xxxl" weight="bold">Add Guests for 201 Oak</Text>
          </Modal.Header>
          <Modal.Content>
            <Flex>
              <Spacer margin={[0, 3, 0, 0]}>
                <Flex flexDirection="column">
                  <Spacer margin={[0, 0, 1, 0]}>
                    <Text size="xs" color="slate">NO OF ADULTS</Text>
                  </Spacer>
                  <input type="number" defaultValue="1" />
                </Flex>
              </Spacer>
              <Flex flexDirection="column">
                <Spacer margin={[0, 0, 1, 0]}>
                  <Text size="xs" color="slate">NO OF KIDS</Text>
                </Spacer>
                <input type="number" defaultValue="0" />
              </Flex>
            </Flex>
          </Modal.Content>
          <Modal.Footer>
            <Flex justifyContent="space-between" alignItems="center">
              <Flex>
                <Spacer margin={[0, 1, 0, 0]}>
                  <Text size="l" weight="bold">Total Payable</Text>
                </Spacer>
                <Text size="l" weight="bold"><Price>1825</Price></Text>
              </Flex>
              <Flex justifyContent="space-between">
                <Spacer margin={[0, 2, 0, 0]}>
                  <Button kind="outlined" size="small">CANCEL</Button>
                </Spacer>
                <Button size="small">ADD GUEST</Button>
              </Flex>
            </Flex>
          </Modal.Footer>
        </React.Fragment>
      </Modal>
    );
  }
}

export default AddGuest;
