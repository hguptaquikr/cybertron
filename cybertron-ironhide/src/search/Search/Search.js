import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';
import SearchInput from './SearchInput';
import SearchResults from './SearchResults';

const Search = (props) => {
  const { $searchBookings } = props;
  return (
    <React.Fragment>
      <SearchInput onSearch={$searchBookings.refetch} />
      <SearchResults bookings={$searchBookings.bookings} />
    </React.Fragment>
  );
};

Search.propTypes = {
  $searchBookings: PropTypes.object.isRequired,
};

export default compose(
  graphql(gql`
    query searchBookings(
      $hotelId: String,
      $fromDate: String,
      $toDate: String,
      $searchText: String,
      $searchField: String,
      $bookingStatus: String,
      $skip: Boolean!,
    ) {
      bookings (
        hotelId: $hotelId,
        fromDate: $fromDate,
        toDate: $toDate,
        searchText: $searchText,
        searchField: $searchField,
        bookingStatus: $bookingStatus,
      ) @skip(if: $skip) {
        id
        referenceNumber
        source {
          channelCode
        }
        checkIn
        checkOut
        status
      }
    }
  `, {
    name: '$searchBookings',
    options: {
      variables: {
        skip: true,
      },
    },
  }),
)(Search);
