import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'cybertron-utils/isEmpty';
import pluralize from 'cybertron-utils/pluralize';
import Card from 'leaf-ui/Card/web';
import Spacer from 'leaf-ui/Spacer/web';
import Text from 'leaf-ui/Text/web';
import Table from 'leaf-ui/Table/web';
import SearchResultItem from './SearchResultItem';

const SearchResults = (props) => {
  const { bookings } = props;
  return (
    !isEmpty(bookings) ? (
      <Spacer padding={[3, 10, 0]}>
        <Card color="greyLighter">
          <Spacer padding={[0, 0, 2, 0]}>
            <Text
              size="xs"
              weigh="semibold"
            >
              {`${bookings.length} ${pluralize(bookings.length, 'Result')} found`}
            </Text>
          </Spacer>
          <Card>
            <Table width="100%">
              <Table.THead>
                <Table.TR>
                  <Table.TH width="25%">Guest Details</Table.TH>
                  <Table.TH width="25%">Booking Id</Table.TH>
                  <Table.TH width="25%">Booking Amount</Table.TH>
                  <Table.TH width="25%">Dates</Table.TH>
                </Table.TR>
              </Table.THead>
              <Table.TBody>
                {
                  bookings.map((booking) => (
                    <SearchResultItem booking={booking} />
                  ))
                }
              </Table.TBody>
            </Table>
          </Card>
        </Card>
      </Spacer>
    ) : (
      null
      // <Text weight="bold" size="l">No results found</Text>
    )
  );
};

SearchResults.propTypes = {
  bookings: PropTypes.array.isRequired,
};

export default SearchResults;
