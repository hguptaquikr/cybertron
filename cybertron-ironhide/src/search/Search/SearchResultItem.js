import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';
import formatDate from 'cybertron-utils/formatDate';
import Flex from 'leaf-ui/Flex/web';
import Price from 'leaf-ui/Price/web';
import Spacer from 'leaf-ui/Spacer/web';
import Tag from 'leaf-ui/Tag/web';
import Text from 'leaf-ui/Text/web';
import Link from 'leaf-ui/Link/web';
import Table from 'leaf-ui/Table/web';
import * as tabMutations from '../../tab/tabMutations';

const SearchResultItem = ({
  booking,
  addTab,
}) => {
  const checkIn = formatDate(booking.checkIn, 'DD MMM');
  const checkOut = formatDate(booking.checkOut, 'DD MMM');
  return (
    <Table.TR>
      <Table.TD>
        <Flex flexDirection="column" alignItems="flex-start">
          <Spacer margin={[0, 0, 1, 0]}>
            <Text size="m" weight="medium">Peri Nikhil</Text>
          </Spacer>
          <Spacer margin={[0, 0, 0, 0]}>
            <Text>+919886077198</Text>
          </Spacer>
        </Flex>
      </Table.TD>
      <Table.TD>
        <Flex flexDirection="column" alignItems="flex-start">
          <Spacer margin={[0, 0, 1, 0]}>
            <Link
              onClick={() => addTab({
                variables: {
                  tab: {
                    flows: [{
                      component: 'BookingDetails',
                      props: { bookingId: 'B1' },
                    }],
                  },
                },
              })}
            >
              {booking.referenceNumber}
            </Link>
          </Spacer>
          <Spacer margin={[0, 0, 0, 0]}>
            <Text>{booking.source.channelCode}</Text>
          </Spacer>
        </Flex>
      </Table.TD>
      <Table.TD>
        <Flex flexDirection="column" alignItems="flex-start">
          <Spacer margin={[0, 0, 1, 0]}>
            <Text size="m" weight="medium"><Price>2000</Price></Text>
          </Spacer>
          <Spacer margin={[0, 0, 0, 0]}>
            <Tag color="blue" kind="outlined" size="small" shape="capsular">
              {booking.status}
            </Tag>
          </Spacer>
        </Flex>
      </Table.TD>
      <Table.TD>
        <Flex flexDirection="column" alignItems="flex-start">
          <Spacer margin={[0, 0, 1, 0]}>
            <Text size="m" weight="medium">
              {`${checkIn} - ${checkOut}`}
            </Text>
          </Spacer>
          <Spacer margin={[0, 0, 0, 0]}>
            <Text size="xs">{booking.status}</Text>
          </Spacer>
        </Flex>
      </Table.TD>
    </Table.TR>
  );
};

SearchResultItem.propTypes = {
  booking: PropTypes.object.isRequired,
  addTab: PropTypes.func.isRequired,
};

export default compose(
  graphql(tabMutations.ADD_TAB, { name: 'addTab' }),
)(SearchResultItem);
