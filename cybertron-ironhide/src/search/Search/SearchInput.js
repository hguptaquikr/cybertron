import React from 'react';
import PropTypes from 'prop-types';
import Button from 'leaf-ui/Button/web';
import Card from 'leaf-ui/Card/web';
import Form from 'leaf-ui/Form/web';
import Flex from 'leaf-ui/Flex/web';
import Select from 'leaf-ui/Select/web';
import Spacer from 'leaf-ui/Spacer/web';
import TextInput from 'leaf-ui/TextInput/web';

const searchFieldOptions = [{
  label: 'Booking Id',
  value: 'bookingId',
}, {
  label: 'Guest Name',
  value: 'guestName',
}];

const bookingStatusOptions = [{
  label: 'Reserved',
  value: 'RESERVED',
}, {
  label: 'Checked In',
  value: 'CHECKED-IN',
}, {
  label: 'Checked Out',
  value: 'CHECKED-OUT',
}, {
  label: 'No Show',
  value: 'NO-SHOW',
}];

class SearchInput extends React.Component {
  performSearch = (values) => {
    const { onSearch } = this.props;
    onSearch({
      searchText: values.searchText,
      searchField: values.searchField.value,
      bookingStatus: values.bookingStatus.value,
      fromDate: values.checkIn,
      toDate: values.checkOut,
      skip: false,
    });
  }

  render() {
    return (
      <Spacer padding={[3, 15, 3]}>
        <Card>
          <Form
            onSubmit={this.performSearch}
            initialValues={{
              searchField: searchFieldOptions[0],
              bookingStatus: bookingStatusOptions[0],
            }}
            validationSchema={
              Form.validation.object().shape({
                searchText: Form.validation.string().required(),
                searchField: Form.validation.object().required(),
                bookingStatus: Form.validation.object(),
                checkIn: Form.validation.string(),
                checkOut: Form.validation.string(),
              })
            }
          >
            <Form.Form>
              <Flex alignItems="center">
                <Spacer margin={[0, 2, 0, 0]} width="66%">
                  <TextInput
                    name="searchText"
                    label="Search by Hotel Name"
                    block
                  />
                </Spacer>
                <Spacer margin={[0, 2, 0, 0]}>
                  <Select
                    name="searchField"
                    label="Search on"
                    options={searchFieldOptions}
                  />
                </Spacer>
                <Button type="submit">
                  Search
                </Button>
              </Flex>
              <Flex>
                <Spacer margin={[0, 2, 0, 0]}>
                  <Select
                    name="bookingStatus"
                    label="Booking Status"
                    options={bookingStatusOptions}
                  />
                </Spacer>
                <Spacer margin={[0, 2, 0, 0]}>
                  <TextInput
                    name="checkIn"
                    label="Check-In Date"
                  />
                </Spacer>
                <TextInput
                  name="checkOut"
                  label="Check-Out Date"
                />
              </Flex>
            </Form.Form>
          </Form>
        </Card>
      </Spacer>
    );
  }
}

SearchInput.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

export default SearchInput;
