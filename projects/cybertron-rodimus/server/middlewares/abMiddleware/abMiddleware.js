import api from '../../../client/services/api/apiService';
import * as abService from '../../../client/services/ab/abService';

export default async (req, res, next) => {
  abService.init();
  const abUserId = req.cookies.ab_user ? JSON.parse(req.cookies.ab_user).id : '';
  const ab = await api.get('/api/web/v1/ab/assign_all/', { abUserId })
    .then(abService.transformAbApi);
  abService.init(ab.user, ab.experiments);
  res.cookie('ab_user', JSON.stringify(ab.user), { maxAge: 2592000000 });
  res.cookie('ab_experiments', JSON.stringify(ab.experiments), { maxAge: 2592000000 });
  next();
};
