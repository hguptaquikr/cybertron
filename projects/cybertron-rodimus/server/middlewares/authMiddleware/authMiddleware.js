import * as authService from '../../../client/services/auth/authService';

export default async (req, res, next) => {
  authService.unsetAuth();
  const authTokenCookie = req.cookies.token;
  const authToken = authTokenCookie ? authTokenCookie.split(' ')[1] : null;
  const userProfile = req.cookies.authenticated_user;
  authService.setToken(authToken);
  authService.setUser(userProfile);
  authService.setIp(req.ip);
  next();
};
