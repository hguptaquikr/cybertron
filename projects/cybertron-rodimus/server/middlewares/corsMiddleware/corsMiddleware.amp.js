import logger from 'cybertron-utils/logger';
import config from '../../../config';

const allowedOrigins = [
  config.hostUrl,
  `${config.hostUrl.replace('-', '--').replace('.', '-')}.cdn.ampproject.org`,
  `${config.hostUrl}.amp.cloudflare.com`,
  'https://cdn.ampproject.org',
];

const corsMiddlewareAmp = (req, res, next) => {
  if (__APP_ENV__ !== 'staging' && __APP_ENV__ !== 'production') {
    logger.info('[corsMiddlewareAmp] bypassing since __APP_ENV__:', __APP_ENV__);
    return next();
  }

  let origin = '';
  const ampSourceOrigin = req.query.__amp_source_origin;
  if (!ampSourceOrigin) {
    logger.info('[corsMiddlewareAmp] no ampSourceOrigin', ampSourceOrigin);
    return next();
  }

  if (req.header('Amp-Same-Origin') === 'true') {
    origin = ampSourceOrigin;
    logger.info('[corsMiddlewareAmp] Amp-Same-Origin is true');
  } else if (
    allowedOrigins.includes(req.header('Origin')) &&
    ampSourceOrigin === config.hostUrl
  ) {
    origin = req.header('Origin');
    logger.info('[corsMiddlewareAmp] Orign is not Host', origin, config.hostUrl);
  } else {
    logger.info('[corsMiddlewareAmp] cors validation failure');
    return res.sendStatus(401);
  }

  logger.info('[corsMiddlewareAmp] cors validation success');

  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Origin', origin);
  res.header('AMP-Access-Control-Allow-Source-Origin', ampSourceOrigin);
  res.header('Access-Control-Expose-Headers', 'AMP-Access-Control-Allow-Source-Origin');

  return next();
};

export default corsMiddlewareAmp;
