/* eslint-disable max-len, import/no-unresolved */

export default (app, scStyles, head, analytics) => `
  <!doctype html>
  <html ⚡ lang="en">
    <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
      <link rel="shortcut icon" href="//images.treebohotels.com/images/rodimus/favicon.ico?v=5A5wklOX2k">
      <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

      ${scStyles ? scStyles.replace(/!important/g, '').replace(/data-styled-components=".*">/g, 'amp-custom>.perfect-stay{border-left: 1px solid #e0e0e0;border-right:1px solid #e0e0e0;border-bottom:1px solid #e0e0e0;border-radius: 0 0 2px 2px;}.link{text-decoration:none;}.hide{display: none;}.refine-bar { box-shadow: 0 -2px 4px 0 rgba(0,0,0,.1);}.seperator { border-left: 1px solid #f1f1f1;width: 31px;height: 40px;}') : ''}

      <script async src="https://cdn.ampproject.org/v0.js"></script>
      <script async
        custom-element="amp-install-serviceworker"
        src="https://cdn.ampproject.org/v0/amp-install-serviceworker-0.1.js">
      </script>
      <script async
        custom-element="amp-analytics"
        src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js">
      </script>

      <meta name="msapplication-config" content="//images.treebohotels.com/images/rodimus/browserconfig.xml?v=5A5wklOX2k">
      <meta name="application-name" content="Treebo Hotels">
      <meta name="theme-color" content="#ffffff">

      ${head.title.toString()}
      ${head.meta.toString()}
      ${head.link.toString()}
      ${head.script.toString()}
    </head>
    <body>
      <div id="root">${app}</div>
      <amp-install-serviceworker
        src="/serviceWorker.js"
        data-iframe-src="https://www.treebo.com/"
        layout="nodisplay">
      </amp-install-serviceworker>
      <amp-analytics type="segment">
        <script type="application/json">
          ${analytics}
        </script>
    </amp-analytics>
    </body>
  </html>`;
