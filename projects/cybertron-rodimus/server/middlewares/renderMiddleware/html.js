/* eslint-disable max-len, import/no-unresolved */
import { assets, scripts } from './fragments';
import assetsManifest from '../../build/client/assetsManifest.json';
import * as packageJson from '../../../package.json';

export default (app, scStyles, head, initialState, route, chunks, req) => `
  <!doctype html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
      <link rel="preconnect" href="//static.treebohotels.com">
      <link rel="preconnect" href="//images.treebohotels.com">
      <link rel="preload" as="script" href="${assets.webpackManifest.js}">
      <link rel="preload" as="script" href="${assets.vendor.js}">
      <link rel="preload" as="script" href="${assets.main.js}">
      ${chunks.reduce((preloads, chunk) => `${preloads}<link rel="preload" as="script" href="${assets[chunk].js}">`, '')}
      ${__LOCAL__ ? '' : `<style>${assets.vendor.styles}</style>`}
      ${__LOCAL__ ? '' : `<style>${assets.main.styles}</style>`}
      ${__LOCAL__ ? '' : chunks.reduce((s, name) => `${s}<style id="${name}.css">${assets[name].styles}</style>`, '')}
      ${__LOCAL__ ? '' : scStyles}
      ${__LOCAL__ ? '' : '<link rel="manifest" href="/manifest.json">'}
      <meta name="mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-title" content="Treebo Hotels">
      <meta name="msapplication-config" content="//images.treebohotels.com/images/rodimus/browserconfig.xml?v=5A5wklOX2k">
      <meta name="application-name" content="Treebo Hotels">
      <meta name="theme-color" content="#ffffff">
      <link rel="apple-touch-icon" sizes="180x180" href="//images.treebohotels.com/images/rodimus/apple-touch-icon.png?v=5A5wklOX2k">
      <link rel="icon" type="image/png" href="//images.treebohotels.com/images/rodimus/favicon-32x32.png?v=5A5wklOX2k" sizes="32x32">
      <link rel="icon" type="image/png" href="//images.treebohotels.com/images/rodimus/favicon-16x16.png?v=5A5wklOX2k" sizes="16x16">
      <link rel="mask-icon" href="//images.treebohotels.com/images/rodimus/safari-pinned-tab.svg?v=5A5wklOX2k" color="#0eb550">
      <link rel="shortcut icon" href="//images.treebohotels.com/images/rodimus/favicon.ico?v=5A5wklOX2k">
      ${head.title.toString()}
      ${head.meta.toString()}
      ${head.link.toString()}
      ${head.script.toString()}
    </head>
    <body data-version="${packageJson.version}">
      <div id="root">${app}</div>
      <script>window.__INITIAL_STATE__ = ${JSON.stringify(initialState)}</script>
      <script>window.__ASSETS_MANIFEST__ = ${JSON.stringify(assetsManifest)}</script>
      <script>${scripts.analytics(req.ip)}</script>
      <script src="${assets.webpackManifest.js}"></script>
      <script src="${assets.vendor.js}"></script>
      <script src="${assets.main.js}"></script>
      ${chunks.reduce((s, chunk) => `${s}<script src="${assets[chunk].js}"></script>`, '')}
      ${__LOCAL__ ? '' : `<script>${scripts.serviceWorker}</script>`}
      <script>${scripts.a2hsListener}</script>
    </body>
  </html>`;
