import config from '../../../config';

const makeExtraUrlParams = (properties = {}) =>
  Object.keys(properties).reduce((extraUrlParams, property) => ({
    ...extraUrlParams,
    [`properties.${property}`]: properties[property],
  }), {});

const makeTriggers = (events = []) => events.reduce((triggers, event) => ({
  ...triggers,
  [event.selector]: triggers[event.name] || {
    on: event.on || 'click',
    selector: event.selector,
    request: 'track',
    vars: {
      event: event.name,
    },
    extraUrlParams: makeExtraUrlParams(event.properties),
  },
}), {});

const makeAnalytics = (analytics) => JSON.stringify({
  vars: {
    writeKey: config.segmentApiKeyWeb,
    name: analytics.pageEventName,
  },
  triggers: makeTriggers(analytics.events),
  extraUrlParams: {
    ...makeExtraUrlParams(analytics.properties),
    'properties.nonInteraction': 1,
  },
});

export const getAnalytics = (branches, ctx) => {
  let analytics = {};
  branches.find((branch) => {
    const { component } = branch.route;
    const context = {
      match: branch.match,
      ...ctx,
    };

    if (component.analytics) {
      analytics = component.analytics(context);
      return true;
    }

    return false;
  });
  return makeAnalytics(analytics);
};
