import React from 'react';
import Helmet from 'react-helmet';
import { matchRoutes } from 'react-router-config';
import { renderToString } from 'react-dom/server';
import { Router } from 'react-router-dom';
import createMemoryHistory from 'history/createMemoryHistory';
import Loadable from 'react-loadable';
import { Provider } from 'react-redux';
import { ServerStyleSheet } from 'styled-components';
import logger from 'cybertron-utils/logger';
import makeCreateStore from '../../../client/services/store/makeCreateStore';
import routes from '../../../client/services/route/routes';
import * as routeService from '../../../client/services/route/routeService';
import execComponentWillServerRender from './execComponentWillServerRender';
import { getAnalytics } from './analytics.amp';
import htmlAmp from './html.amp';

const renderMiddlewareAmp = async (req, res) => {
  logger.info('[renderMiddlewareAmp] req.originalUrl', req.originalUrl);
  const history = createMemoryHistory();
  history.replace(req.originalUrl);
  const branches = matchRoutes(routes, req.originalUrl.split('?')[0]);
  const branch = branches[branches.length - 1];
  const sheet = new ServerStyleSheet();
  const store = makeCreateStore({ history })();
  const chunks = [];

  await execComponentWillServerRender(
    branches,
    { store, route: branch.route, history, req, res },
  );

  const analytics = getAnalytics(
    branches,
    { store, route: branch.route, history, req, res },
  );

  const app = renderToString(sheet.collectStyles(
    <Loadable.Capture report={(name) => chunks.push(name.replace(/.*\//, ''))}>
      <Provider store={store}>
        <Router history={history}>
          {routeService.renderRoutes(routes)}
        </Router>
      </Provider>
    </Loadable.Capture>,
  ));

  const currentUrl = history.location.pathname + history.location.search;
  logger.info(`[renderMiddlewareAmp] currentUrl: ${currentUrl}`);
  logger.info(`[renderMiddlewareAmp] req.originalUrl: ${req.originalUrl}`);
  if (currentUrl !== req.originalUrl) {
    logger.info('[renderMiddlewareAmp] redirecting');
    return res.redirect(currentUrl);
  }

  const page = htmlAmp(
    app,
    sheet.getStyleTags(),
    Helmet.renderStatic(),
    analytics,
  );

  return res.send(page);
};

export default renderMiddlewareAmp;
