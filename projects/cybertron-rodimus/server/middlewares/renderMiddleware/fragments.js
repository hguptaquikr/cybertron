/* eslint-disable max-len, import/no-unresolved */
import fs from 'fs';
import config from '../../../config';
import assetsManifest from '../../build/client/assetsManifest.json';

export const assets = Object.keys(assetsManifest)
  .reduce((obj, entry) => ({
    ...obj,
    [entry]: {
      ...assetsManifest[entry],
      styles: assetsManifest[entry].css
        ? fs.readFileSync(`build/client/css/${assetsManifest[entry].css.split('/').pop()}`, 'utf8')
        : undefined,
    },
  }), {});

export const scripts = {
  serviceWorker: '"serviceWorker"in window.navigator&&window.addEventListener("load",function(){window.navigator.serviceWorker.register("/serviceWorker.js").then(function(r){console.log("ServiceWorker registration successful with scope: ",r.scope)}).catch(function(e){console.error("ServiceWorker registration failed: ",e)})});',

  a2hsListener: 'window.addEventListener("beforeinstallprompt",function(e){console.log("beforeinstallprompt Event fired"),e.userChoice.then(function(e){"dismissed"==e.outcome?(console.log("User cancelled home screen install"),window.analytics&&window.analytics.track("Add to Homescreen",{success:!1})):(console.log("User added to home screen"),window.analytics&&window.analytics.track("Add to Homescreen",{success:!0}))})});',

  analytics(ip) {
    const allowAnalytics = !config.analyticsBlacklistedIps.some((blackListedIp) => blackListedIp === ip);
    return allowAnalytics
      ? [
        `!function(){var k=window.__ANDROID_INTERFACE__?"${config.segmentApiKeyAndroid}":"${config.segmentApiKeyWeb}",a=window.analytics=window.analytics||[];if(!a.initialize)if(a.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{a.invoked=!0,a.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","page","once","off","on"],a.factory=function(r){return function(){var o=Array.prototype.slice.call(arguments);return o.unshift(r),a.push(o),a}};for(var r=0;r<a.methods.length;r++){var o=a.methods[r];a[o]=a.factory(o)}a.load=function(a){var r=document.createElement("script");r.type="text/javascript",r.async=!0,r.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+a+"/analytics.min.js";var o=document.getElementsByTagName("script")[0];o.parentNode.insertBefore(r,o)},a.SNIPPET_VERSION="3.1.0",window.addEventListener("load", function(){a.load(k)})}}();`,
        `window.clevertap={event:[],profile:[],account:[{id:"${config.clevertapApiKey}"}],onUserLogin:[],notifications:[]},window.clevertap.loadscript=function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src=("https:"==document.location.protocol?"https://d2r1yp2w7bby2u.cloudfront.net":"http://static.clevertap.com")+"/js/a.js";var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)},window.addEventListener("load",window.clevertap.loadscript);`,
      ].join('')
      : [
        'window.analytics={track:function(){},identify:function(){},page:function(){}};',
        'window.clevertap={event:[],profile:[],account:[]};',
      ].join('');
  },
};
