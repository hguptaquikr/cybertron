/* eslint-disable import/first */
import './newrelicAgent';
import 'babel-polyfill';
import express from 'express';
import helmet from 'helmet';
import compression from 'compression';
import cookieParser from 'cookie-parser';
import slashes from 'connect-slashes';
import Loadable from 'react-loadable';
import logger from 'cybertron-utils/logger';
import expressLoggerMiddleware from 'cybertron-utils/expressLoggerMiddleware';
import corsMiddlewareAmp from './middlewares/corsMiddleware/corsMiddleware.amp';
import webMiddleware from './middlewares/webMiddleware/webMiddleware';
import abMiddleware from './middlewares/abMiddleware/abMiddleware';
import authMiddleware from './middlewares/authMiddleware/authMiddleware';
import cacheMiddleware from './middlewares/cacheMiddleware/cacheMiddleware';
import renderMiddleware from './middlewares/renderMiddleware/renderMiddleware';
import renderMiddlewareAmp from './middlewares/renderMiddleware/renderMiddleware.amp';
import * as redisClient from '../client/services/redis/redisClient';

const app = express();
app.set('trust proxy', true);
app.use(helmet({ dnsPrefetchControl: false }));
app.use(compression());
app.use(expressLoggerMiddleware);
app.use('*/amp/', corsMiddlewareAmp);
app.use('/health', (req, res) => res.send({ ok: true }));
app.use('/serviceWorker.js', express.static('build/client/serviceWorker.js'));
app.use('/manifest.json', express.static('build/client/manifest.json'));
app.use('/rodimus/build/client', express.static('build/client'));
app.use(slashes(true));
app.use('/api/web', webMiddleware);
app.use(cookieParser());
app.use(abMiddleware);
app.use(authMiddleware);
if (__APP_ENV__ === 'staging' || __APP_ENV__ === 'production') {
  redisClient.init();
  app.use(cacheMiddleware);
}
app.use('*/amp/', renderMiddlewareAmp);
app.use('*', renderMiddleware);

const PORT = process.env.PORT || 8000;
Loadable.preloadAll().then(() => {
  app.listen(PORT, () => {
    logger.info(`rodimus is running as ${__APP_ENV__} on port ${PORT}`);
  });
});
