import config from '../../../config';

export default {
  initHandlers() {
    if (__BROWSER__) {
      const GoogleMapsLoader = require('google-maps'); // eslint-disable-line
      GoogleMapsLoader.KEY = config.googleMapsApiKey;
      GoogleMapsLoader.REGION = ['IN'];
      GoogleMapsLoader.LIBRARIES = ['places'];
      GoogleMapsLoader.load((google) => {
        this.placesHandler = new google.maps.places.PlacesService(
          document.body.appendChild(document.createElement('div')),
        );
      });
    }
  },

  getPlaceDetails(place) {
    const placeId = place.googlePlaceId;
    return new Promise((resolve, reject) => {
      this.placesHandler.getDetails({ placeId }, (result, status) => {
        if (status === 'OK') {
          resolve({ result });
        } else {
          reject({ status });
        }
      });
    });
  },

  constructPlace(googlePlace) {
    const googleAddressPieceMap = {
      city: 'locality',
      state: 'administrative_area_level_1',
      country: 'country',
    };

    const area = googlePlace.address_components.reduce((obj, addressComponent) => {
      Object.keys(googleAddressPieceMap)
        .forEach((addressPiece) => {
          obj[addressPiece] = // eslint-disable-line
            addressComponent.types.includes(googleAddressPieceMap[addressPiece])
              ? addressComponent.long_name : obj[addressPiece];
        });
      return obj;
    }, {});

    const coordinates = {
      lat: googlePlace.geometry.location.lat(),
      lng: googlePlace.geometry.location.lng(),
    };

    return { area, coordinates };
  },
};
