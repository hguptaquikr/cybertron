import { handle } from 'redux-pack';
import couponService from './couponService';
import analyticsService from '../analytics/analyticsService';
import androidService from '../android/androidService';

const SHOW_COUPON_MODAL = 'SHOW_COUPON_MODAL';
const GET_COUPONS = 'GET_COUPONS';
const APPLY_REMOVE_COUPON = 'APPLY_REMOVE_COUPON';
const GET_CHECKOUT_DETAILS = 'GET_CHECKOUT_DETAILS';

const initialState = {
  results: {
    '': {
      code: '',
      amount: '',
      tagline: '',
      description: '',
    },
  },
  sortedCouponCodes: [],
  appliedCouponCode: '',
  showCouponModal: false,
  isCouponApplyLoading: false,
  isCouponResultsLoading: false,
  error: '',
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SHOW_COUPON_MODAL:
      return {
        ...state,
        showCouponModal: payload.visibility,
        error: '',
      };

    case GET_COUPONS: return handle(state, action, {
      start: (s) => ({
        ...s,
        results: payload.results,
        sortedCouponCodes: payload.sortedCouponCodes,
        isCouponResultsLoading: true,
      }),
      success: (s) => ({
        ...s,
        results: {
          ...s.results,
          ...payload.results,
        },
        sortedCouponCodes: payload.sortedCouponCodes,
      }),
      finish: (s) => ({ ...s, isCouponResultsLoading: false }),
    });

    case APPLY_REMOVE_COUPON: return handle(state, action, {
      start: (s) => ({ ...s, isCouponApplyLoading: true }),
      success: (s) => ({
        ...s,
        results: {
          ...s.results,
          [payload.coupon.appliedCouponCode]: {
            ...s.results[payload.coupon.appliedCouponCode],
            ...payload.coupon.results[payload.coupon.appliedCouponCode],
          },
        },
        appliedCouponCode: payload.coupon.appliedCouponCode,
        showCouponModal: false,
      }),
      failure: (s) => ({ ...s, error: payload.errors[0].message }),
      finish: (s) => ({ ...s, isCouponApplyLoading: false }),
    });

    case GET_CHECKOUT_DETAILS: return handle(state, action, {
      start: (s) => ({ ...s, appliedCouponCode: '' }),
      success: (s) => ({
        ...s,
        results: {
          ...s.results,
          [payload.coupon.appliedCouponCode]: {
            ...s.results[payload.coupon.appliedCouponCode],
            ...payload.coupon.results[payload.coupon.appliedCouponCode],
          },
        },
        appliedCouponCode: payload.coupon.appliedCouponCode,
      }),
    });

    default:
      return state;
  }
};

export const showCouponModal = (visibility) => ({
  type: SHOW_COUPON_MODAL,
  payload: { visibility },
});

export const getCoupons = (bid) => (dispatch, getState, { api }) => {
  const { ui } = getState();
  const payload = {
    bid,
    channel: androidService.isAndroid ? 'app' : 'msite',
    ...ui.utmParams,
  };
  return dispatch({
    type: GET_COUPONS,
    payload: {
      results: initialState.results,
      sortedCouponCodes: initialState.sortedCouponCodes,
    },
    promise: api.get('/api/web/v2/discount/featured/', payload)
      .then(couponService.transformCouponResultsApi),
  });
};

export const applyCoupon = (couponCode, hotel) => (dispatch, getState, { api }) => {
  const { booking, wallet, ui } = getState();
  const treeboPoints = wallet.byType.TP || wallet.byType[''];
  const payload = {
    bid: booking.bid,
    couponcode: couponCode,
    channel: androidService.isAndroid ? 'app' : 'msite',
    apply_wallet: treeboPoints.isUsingTreeboPoints,
    ...ui.utmParams,
  };

  return dispatch({
    type: APPLY_REMOVE_COUPON,
    payload: { hotelId: booking.hotelId },
    promise: api.post('/api/web/v5/checkout/coupon/', payload).then(
      (res) => couponService.transformCouponApplyApi(res, booking.hotelId, booking, couponCode),
    ),
    meta: {
      onSuccess: () => analyticsService.couponApplied(hotel.id, couponCode, true),
      onFailure: () => analyticsService.couponApplied(hotel.id, couponCode, false),
    },
  });
};

export const removeCoupon = () => (dispatch, getState, { api }) => {
  const { booking, wallet, ui } = getState();
  const treeboPoints = wallet.byType.TP || wallet.byType[''];
  const payload = {
    bid: booking.bid,
    channel: androidService.isAndroid ? 'app' : 'msite',
    apply_wallet: treeboPoints.isUsingTreeboPoints,
    ...ui.utmParams,
  };

  return dispatch({
    type: APPLY_REMOVE_COUPON,
    payload: { hotelId: booking.hotelId },
    promise: api.post('/api/web/v5/checkout/coupon/remove/', payload)
      .then((res) => couponService.transformCouponApplyApi(res, booking.hotelId, booking)),
  });
};
