import * as roomTypeService from './../roomType/roomTypeService';

export default {
  transformCouponResultsApi({ data }) {
    return {
      results: data.reduce((obj, coupon) => ({
        ...obj,
        [coupon.code]: {
          code: coupon.code,
          amount: coupon.coupon_amount,
          tagline: coupon.tagline,
          description: coupon.description,
        },
      }), {}),
      sortedCouponCodes: data.map((coupon) => coupon.code),
    };
  },

  transformCouponApplyApi({ data }, hotelId, { room }, couponCode = '') {
    return {
      coupon: {
        results: {
          [couponCode]: {
            description: data.coupon_desc,
          },
        },
        appliedCouponCode: couponCode,
      },
      booking: {
        isPrepaidOnly: 'is_prepaid' in data ? data.is_prepaid : false,
      },
      wallets: {
        [data.wallet_info.wallet_type]: {
          isUsingTreeboPoints: data.wallet_info.wallet_applied,
          totalBalance: +data.wallet_info.total_wallet_balance,
          totalPoints: +data.wallet_info.total_wallet_points,
          usableBalance: +data.wallet_info.wallet_deductable_amount,
        },
      },
      price: {
        byRoomTypeId: {
          [roomTypeService.makeRoomTypeId(hotelId, room.type)]: {
            available: 1,
            id: roomTypeService.makeRoomTypeId(hotelId, room.type),
            ratePlans: data.all_rate_plans.reduce((ratePlans, ratePlan) => ({
              ...ratePlans,
              [ratePlan.rate_plan.code]: {
                code: ratePlan.rate_plan.code,
                description: ratePlan.rate_plan.code,
                type: ratePlan.rate_plan.refundable ? 'refundable' : 'non-refundable',
                tag: ratePlan.rate_plan.tag,
                basePrice: ratePlan.price.pretax_price,
                treeboPointsUsed: ratePlan.price.wallet_deduction,
                sellingPrice: ratePlan.price.net_payable_amount,
                strikedPrice: ratePlan.price.rack_rate,
                totalPrice: ratePlan.price.sell_price,
                tax: ratePlan.price.tax,
                discountPercentage: ratePlan.price.total_discount_percent,
                discountAmount: ratePlan.price.coupon_discount,
              },
            }), {}),
            memberDiscount: {
              value: data.selected_rate_plan.price.member_discount,
              isApplied: data.member_discount_applied,
              isAvailable: data.member_discount_available,
            },
          },
        },
        roomTypeIds: [roomTypeService.makeRoomTypeId(hotelId, room.type)],
      },
    };
  },
};
