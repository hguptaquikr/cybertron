import { handle } from 'redux-pack';
import groupBy from 'lodash/groupBy';
import isEmpty from 'lodash/isEmpty';
import * as hotelService from '../hotel/hotelService';
import contentService from '../content/contentService';
import priceService from '../price/priceService';
import { omitKeys } from '../utils';

const SORT_CHANGE = 'SORT_CHANGE';
const CLEAR_ALL_FILTERS = 'CLEAR_ALL_FILTERS';
const CONSTRUCT_LOCALITIES_FILTER = 'CONSTRUCT_LOCALITIES_FILTER';
const CONSTRUCT_PRICE_RANGES_FILTER = 'CONSTRUCT_PRICE_RANGES_FILTER';
const GET_AMENITIES_FILTER = 'GET_AMENITIES_FILTER';
const FILTERS_CHECKBOX_CHANGE = 'FILTERS_CHECKBOX_CHANGE';
const SET_REFINED_HOTEL_IDS = 'SET_REFINED_HOTEL_IDS';
const GET_HOTEL_RESULTS = 'GET_HOTEL_RESULTS';

const initialState = {
  filters: {
    priceRanges: [],
    localities: [],
    amenities: [],
    showCoupleFriendly: [{ checked: false }],
    showAvailableOnly: [{ checked: false }],
    showTopRatedOnly: [{ checked: false }],
    showFrequentlyBookedOnly: [{ checked: false }],
    showValueForMoneyOnly: [{ checked: false }],
    distanceCap: null,
  },
  sort: {
    by: 'price',
    list: ['price'],
    coordinates: { lat: '', lng: '' },
  },
  refinedHotelIds: [],
};

export default (state = initialState, action) => {
  const { type, payload, meta } = action;
  switch (type) {
    case SORT_CHANGE:
      return {
        ...state,
        sort: {
          ...state.sort,
          by: payload.sortBy,
        },
      };

    case CLEAR_ALL_FILTERS:
      return {
        ...state,
        filters: payload.clearedFilters,
      };

    case CONSTRUCT_LOCALITIES_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          localities: payload.localities,
        },
      };

    case CONSTRUCT_PRICE_RANGES_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          priceRanges: payload.priceRanges,
        },
      };

    case GET_AMENITIES_FILTER: return handle(state, action, {
      success: (s) => ({
        ...s,
        filters: {
          ...state.filters,
          amenities: payload.data.map((amenity) => ({ ...amenity, checked: false })),
        },
      }),
    });

    case FILTERS_CHECKBOX_CHANGE: {
      const { i: index, filterKey } = payload;
      return {
        ...state,
        filters: {
          ...state.filters,
          [filterKey]: [
            ...state.filters[filterKey].slice(0, index),
            {
              ...state.filters[filterKey][index],
              checked: payload.checked,
            },
            ...state.filters[filterKey].slice(index + 1),
          ],
        },
      };
    }

    case SET_REFINED_HOTEL_IDS:
      return {
        ...state,
        refinedHotelIds: payload.refinedHotelIds,
      };

    case GET_HOTEL_RESULTS: return handle(state, action, {
      start: (s) => ({
        ...s,
        refinedHotelIds: [],
      }),
      success: (s) => ({
        ...s,
        filters: {
          ...state.filters,
          ...payload.filters,
        },
        sort: {
          ...state.sort,
          ...(meta.startPayload.sort || payload.sort),
        },
        refinedHotelIds: payload.ids,
      }),
    });

    default:
      return state;
  }
};

export const constructPriceRangesFilter = () => (dispatch, getState) => {
  const { price } = getState();
  const sortedPrices = priceService.sortPrice(omitKeys(price.byRoomTypeId, ['0|oak']));
  const lowestRatePlan = priceService.sortRatePlan(sortedPrices[0].ratePlans);
  const highestRatePlan = priceService
    .sortRatePlan(sortedPrices[sortedPrices.length - 1].ratePlans);
  const minPrice = lowestRatePlan[0].sellingPrice;
  const maxPrice = highestRatePlan[highestRatePlan.length - 1].sellingPrice;
  const incPrice = Math.round((maxPrice - minPrice) / 3);
  let currentPrice = Math.round(minPrice);
  const priceRanges = (maxPrice > 0) ? (
    [0, 1, 2].map(() => {
      const priceRange = {
        start: currentPrice,
        end: currentPrice + incPrice,
        name: `&#x20b9;${currentPrice} - &#x20b9;${currentPrice + incPrice}`,
        checked: false,
      };
      currentPrice += incPrice;
      return priceRange;
    })
  ) : [];

  dispatch({
    type: CONSTRUCT_PRICE_RANGES_FILTER,
    payload: { priceRanges },
  });
};

export const constructLocalitiesFilter = () => (dispatch, getState) => {
  const { hotel } = getState();
  const allLocalities = hotel.ids.map((hotelId) => hotel.byId[hotelId].address.locality);
  const distinctLocalities = Array.from(new Set(allLocalities));
  const localities = distinctLocalities.sort().map((locality) => ({
    name: locality,
    checked: false,
  }));

  dispatch({
    type: CONSTRUCT_LOCALITIES_FILTER,
    payload: { localities },
  });
};

export const getAmenitiesFilter = (route, params) => (dispatch, getState, { api }) => {
  const seoQuery = contentService.makeSeoQuery(route, params);
  dispatch({
    type: GET_AMENITIES_FILTER,
    promise: api.get('/api/web/v1/facilities/', seoQuery),
  });
};

const _filterResults = (hotelIds, getState) => {
  const {
    filter: { filters },
    hotel,
    price,
  } = getState();
  const hasPrices = price.roomTypeIds.length > 0;
  const checkedPriceRanges = filters.priceRanges.filter((priceRange) => priceRange.checked);
  const checkedLocalities = filters.localities.filter((locality) => locality.checked);
  const checkedAmenities = filters.amenities.filter((amenity) => amenity.checked);
  const shouldFilterWithinDistanceCap = !!filters.distanceCap;
  const shouldFilterAvailableOnly = filters.showAvailableOnly[0].checked && hasPrices;
  const shouldFilterTopRatedOnly = filters.showTopRatedOnly[0].checked;
  const shouldFilterFrequentlyBookedOnly = filters.showFrequentlyBookedOnly[0].checked;
  const shouldFilterValueForMoneyOnly = filters.showValueForMoneyOnly[0].checked;
  const shouldFilterPriceRanges = checkedPriceRanges.length && hasPrices;
  const shouldFilterLocalities = checkedLocalities.length;
  const shouldFilterAmenities = checkedAmenities.length;
  const shouldFilterCoupleFriendlyOnly = filters.showCoupleFriendly[0].checked;

  const withinDistanceCap = (hotelDetail) => hotelDetail.distance <= filters.distanceCap;
  const isAvailable = (priceObj) => priceObj.available;
  const isTopRated = (hotelDetail) => hotelDetail.isTopRated;
  const isFrequentlyBooked = (hotelDetail) => hotelDetail.isFrequentlyBooked;
  const isValueForMoney = (hotelDetail) => hotelDetail.isValueForMoney;
  const inPriceRange = (priceRanges, ratePlan) =>
    priceRanges.some((priceRange) =>
      Math.round(ratePlan.sellingPrice) >= Math.round(priceRange.start)
      && Math.round(ratePlan.sellingPrice) <= Math.round(priceRange.end));
  const inLocalities = (localities, hotelDetail) =>
    localities.some((locality) => hotelDetail.address.locality === locality.name);
  const hasAmenities = (amenities, hotelDetail) =>
    amenities.every((amenity) => hotelDetail.amenities.includes(amenity.id));
  const isCoupleFriendly = (hotelDetail) => hotelService.isCoupleFriendly(hotelDetail.policies);

  const filteredHotelIds = hotelIds.filter((hotelId) => {
    const selectedHotel = hotel.byId[hotelId];
    const hotelPrices = priceService
      .getPricesForRoomTypeIds(selectedHotel.roomTypeIds, price);
    const cheapestPrice = priceService.sortPrice(hotelPrices)[0];
    const cheapestRatePlan = priceService.sortRatePlan(cheapestPrice.ratePlans)[0] || {};

    let keepHotel = true;
    if (shouldFilterWithinDistanceCap) {
      keepHotel = keepHotel && withinDistanceCap(selectedHotel);
    }
    if (shouldFilterAvailableOnly) {
      keepHotel = keepHotel && isAvailable(cheapestPrice);
    }
    if (shouldFilterTopRatedOnly) {
      keepHotel = keepHotel && isTopRated(cheapestRatePlan);
    }
    if (shouldFilterFrequentlyBookedOnly) {
      keepHotel = keepHotel && isFrequentlyBooked(cheapestRatePlan);
    }
    if (shouldFilterValueForMoneyOnly) {
      keepHotel = keepHotel && isValueForMoney(cheapestRatePlan);
    }
    if (shouldFilterPriceRanges) {
      keepHotel = keepHotel && inPriceRange(checkedPriceRanges, cheapestRatePlan);
    }
    if (shouldFilterLocalities) {
      keepHotel = keepHotel && inLocalities(checkedLocalities, selectedHotel);
    }
    if (shouldFilterAmenities) {
      keepHotel = keepHotel && hasAmenities(checkedAmenities, selectedHotel);
    }
    if (shouldFilterCoupleFriendlyOnly) {
      keepHotel = keepHotel && isCoupleFriendly(selectedHotel);
    }
    return keepHotel;
  });

  return filteredHotelIds;
};

const _sortResults = (hotelIds, getState) => {
  const {
    filter: { sort },
    search: { searchInput },
    hotel,
    price,
  } = getState();

  // break if no results to sort
  if (isEmpty(hotelIds)) return hotelIds;

  // sort by
  let sortedHotelIds = [...hotelIds];
  const hasPrices = price.roomTypeIds.length > 0;

  if (sort.by === 'price' && hasPrices) {
    sortedHotelIds.sort((hotelIdA, hotelIdB) => {
      const hotelAPrices = priceService
        .getPricesForRoomTypeIds(hotel.byId[hotelIdA].roomTypeIds, price);
      const cheapestPriceA = priceService.sortPrice(hotelAPrices)[0];
      const cheapestRatePlanA = priceService.sortRatePlan(cheapestPriceA.ratePlans)[0] || {};

      const hotelBPrices = priceService
        .getPricesForRoomTypeIds(hotel.byId[hotelIdB].roomTypeIds, price);
      const cheapestPriceB = priceService.sortPrice(hotelBPrices)[0];
      const cheapestRatePlanB = priceService.sortRatePlan(cheapestPriceB.ratePlans)[0] || {};
      return cheapestRatePlanA.sellingPrice - cheapestRatePlanB.sellingPrice;
    });
  } else if (sort.by === 'distance') {
    sortedHotelIds.sort((a, b) =>
      hotel.byId[a].distance - hotel.byId[b].distance);
  } else if (sort.by === 'recommended' && hasPrices) {
    sortedHotelIds.sort((a, b) => {
      if (hotel.byId[a].rank && hotel.byId[b].rank) {
        return hotel.byId[a].rank.recommended - hotel.byId[b].rank.recommended;
      }
      return 0;
    });
  }

  // group by availability
  if (hasPrices) {
    const groupedResults = groupBy(
      sortedHotelIds,
      (hotelId) => {
        const hotelPrices = priceService
          .getPricesForRoomTypeIds(hotel.byId[hotelId].roomTypeIds, price);
        const cheapestPrice = priceService.sortPrice(hotelPrices)[0];
        return !!cheapestPrice.available;
      },
    );
    sortedHotelIds = [];
    sortedHotelIds = groupedResults.true
      ? sortedHotelIds.concat(groupedResults.true) : sortedHotelIds;
    sortedHotelIds = groupedResults.false
      ? sortedHotelIds.concat(groupedResults.false) : sortedHotelIds;
  }

  // move searched soldout hotel to top
  if (searchInput.place.hotelId) {
    const placeHotelId = searchInput.place.hotelId;
    sortedHotelIds = sortedHotelIds.reduce((arr, hotelId) => {
      if (+hotelId === +placeHotelId) arr.unshift(placeHotelId);
      else arr.push(hotelId);
      return arr;
    }, []);
  }

  return sortedHotelIds;
};

export const refineResults = () => (dispatch, getState) => {
  const { hotel } = getState();

  const filteredHotelIds = _filterResults(hotel.ids, getState);
  const sortedHotelIds = _sortResults(filteredHotelIds, getState);

  dispatch({
    type: SET_REFINED_HOTEL_IDS,
    payload: { refinedHotelIds: sortedHotelIds },
  });
};

export const clearAllFilters = () => (dispatch, getState) => {
  const { filter: { filters } } = getState();
  const clearedFilters = { ...filters };
  Object.keys(clearedFilters)
    .filter((filterKey) => filterKey !== 'distanceCap')
    .forEach((filterKey) => {
      clearedFilters[filterKey].forEach((filter) => {
        filter.checked = false; // eslint-disable-line
      });
    });

  dispatch({
    type: CLEAR_ALL_FILTERS,
    payload: { clearedFilters },
  });
  dispatch(refineResults());
};

export const filtersCheckboxChange = (filterKey, i, checked) => (dispatch) => {
  const quickFilters = ['showTopRatedOnly', 'showFrequentlyBookedOnly', 'showValueForMoneyOnly'];
  if (quickFilters.includes(filterKey)) {
    dispatch(clearAllFilters());
  }
  dispatch({
    type: FILTERS_CHECKBOX_CHANGE,
    payload: {
      filterKey,
      i,
      checked,
    },
  });
  dispatch(refineResults());
};

export const sortBy = (parameter) => (dispatch) => {
  dispatch({
    type: SORT_CHANGE,
    payload: { sortBy: parameter },
  });
  dispatch(refineResults());
};
