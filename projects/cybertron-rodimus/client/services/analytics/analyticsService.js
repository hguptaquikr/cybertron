/* eslint-disable max-len */
import cookies from 'js-cookie';
import get from 'lodash/get';
import set from 'lodash/set';
import isEmpty from 'lodash/isEmpty';
import moment from 'moment';
import startCase from 'cybertron-utils/startCase';
import analyticsService from 'cybertron-service-analytics/lib/analytics';
import * as analyticsConstants from 'cybertron-service-analytics/lib/analytics/constants';
import { matchRoutes } from 'react-router-config';
import * as abService from '../ab/abService';
import searchService from '../search/searchService';
import androidService from '../android/androidService';
import routes from '../route/routes';
import { constants, performanceMark, getPerformanceDuration } from '../utils';

export default {
  routeNameMap: {
    details: 'Hotel Detail',
    search: 'Search',
    landing: 'Home',
    itinerary: 'Itinerary',
    seoSearch: 'Hotels In City',
    confirmation: 'Confirmation',
  },

  getState: () => ({}),

  history: {},

  init(getState, history) {
    this.getState = getState;
    this.history = history;
    analyticsService.init({ platform: 'mobileWeb' });
  },

  getSearchData({ search }) {
    const getEntity = (place) => {
      if (place.type) {
        return place.type;
      } else if (place.hotelId) {
        return 'hotel';
      } else if (place.area && place.area.landmark) {
        return 'landmark';
      } else if (place.area && place.area.locality) {
        return 'locality';
      } else if (place.area && place.area.city) {
        return 'city';
      } else if (place.area && place.area.state) {
        return 'state';
      }
      return 'google_api';
    };

    return {
      q: get(search, 'searchInput.place.q'),
      entity: getEntity(get(search, 'searchInput.place', {})),
      hotelId: get(search, 'searchInput.place.hotelId'),
      city: get(search, 'searchInput.place.area.city'),
      state: get(search, 'searchInput.place.area.search'),
      checkin: get(search, 'datePicker.range.start'),
      checkout: get(search, 'datePicker.range.end'),
      roomConfig: get(search, 'roomConfig.rooms', [])
        .map((room) => `${room.adults}-${room.kids || 0}`).join(','),
      roomtype: get(search, 'roomConfig.roomType'),
      abwDays: get(search, 'datePicker.range.start').startOf('day')
        .diff(moment().startOf('day'), 'days'),
    };
  },

  getHotelDetails(hotelId) {
    const { hotel } = this.getState();
    return !isEmpty(hotel.ids) && hotel.byId[hotelId] ? {
      hotelId,
      hotelName: hotel.byId[hotelId].name,
      city: hotel.byId[hotelId].address.city,
      locality: hotel.byId[hotelId].address.locality,
      pincode: hotel.byId[hotelId].address.pincode,
      state: hotel.byId[hotelId].address.state,
      country: hotel.byId[hotelId].address.country,
    } : {};
  },

  getStayDates() {
    const { search: { datePicker } } = this.getState();
    const stayDates = searchService.formattedDatePicker(datePicker.range, constants.dateFormat.query);
    return {
      checkin: stayDates.checkIn,
      checkout: stayDates.checkOut,
      lengthOfStay: stayDates.numberOfNights.count,
      abwDays: stayDates.abwDuration,
    };
  },

  getRoomConfig() {
    const { search: { roomConfig } } = this.getState();
    return searchService.formattedRoomConfig(roomConfig.rooms);
  },

  getFilterData() {
    const { filter: { filters } } = this.getState();
    const isChecked = (filterValue) => filterValue.checked;
    return {
      priceFilterValues: filters.priceRanges
        .filter(isChecked).map((priceRange) => `${priceRange.start} - ${priceRange.end}`),
      amenitiesFilterValues: filters.amenities.filter(isChecked).map((amenity) => amenity.name),
      localitesFilterValues: filters.localities.filter(isChecked).map((locality) => locality.name),
      availabilityFilterValues: filters.showAvailableOnly.filter(isChecked).map(() => true),
    };
  },

  getCurrentRoute() {
    const branches = matchRoutes(routes, this.history.location.pathname);
    const branch = branches[branches.length - 1];
    return branch.route || {};
  },

  exposeGlobally(properties = {}) {
    const { adults, kids, rooms } = this.getRoomConfig();
    const { checkin, checkout } = this.getStayDates();
    window.rt = {
      ...window.rt,
      ...properties,
      checkInDate: checkin,
      checkOutDate: checkout,
      getCheckin() { return this.checkInDate; },
      getCheckout() { return this.checkOutDate; },
      getAdults() { return adults.count; },
      getKids() { return kids.count; },
      getRooms() { return rooms.count; },
    };
  },

  exposeQueryParameters(query) {
    const expose = {};
    const allCookies = cookies.get();
    const queryExposePath = {
      utm_name: 'analytics.utm_params.name',
      utm_source: 'analytics.utm_params.source',
      utm_medium: 'analytics.utm_params.medium',
      utm_campaign: 'analytics.utm_params.campaign',
      utm_keyword: 'analytics.utm_params.keyword',
      avclk: 'analytics.referral_params.avclk',
      refid: 'analytics.refid',
    };

    Object.keys(queryExposePath).forEach((parameter) => {
      if (query[parameter]) {
        cookies.set(parameter, query[parameter], { expires: 30 });
        set(expose, queryExposePath[parameter], query[parameter]);
      } else {
        set(expose, queryExposePath[parameter], allCookies[parameter]);
      }
    });

    this.exposeGlobally(expose);
  },

  updateClevertapProfile({ name, phone, email }) {
    if (!androidService.isAndroid) {
      window.clevertap.profile.push({
        Site: {
          Name: name,
          Phone: `+91${phone}`,
          Email: email,
        },
      });
    }
  },

  recordPerformance(pathname) {
    if (window.performance && window.performance.getEntriesByType) {
      const performanceProperties = [
        ...window.performance.getEntriesByName('first-paint'),
        ...window.performance.getEntriesByName('first-contentful-paint'),
        ...window.performance.getEntriesByName('first-interaction'),
      ].reduce((properties, entry) => ({
        ...properties,
        [entry.name]: entry.startTime,
      }), {});
      let connectionMeta = {};
      if (navigator && navigator.connection) {
        connectionMeta = {
          connectionEffectiveType: navigator.connection.effectiveType,
          connectionDownlink: navigator.connection.downlink,
        };
      }
      window.analytics.track('Record Performance', {
        isMobile: true,
        nonInteraction: 1,
        ...performanceProperties,
        ...connectionMeta,
        pathname,
      });
    }
  },

  trackPerformanceMeta: {},

  trackPerformanceStart(type, name, meta = {}) {
    if (__BROWSER__) {
      performanceMark(`${type}|${name}|start`);
      this.trackPerformanceMeta[`${type}|${name}`] = meta;
    }
  },

  trackPerformanceEnd(type, name, meta = {}) {
    if (__BROWSER__) {
      performanceMark(`${type}|${name}|end`);
      const duration = getPerformanceDuration(
        `${type}|${name}`,
        `${type}|${name}|start`,
        `${type}|${name}|end`,
      );
      if (duration) {
        let connectionMeta = {};
        if (navigator && navigator.connection) {
          connectionMeta = {
            connectionEffectiveType: navigator.connection.effectiveType,
            connectionDownlink: navigator.connection.downlink,
          };
        }
        window.analytics.track('Track Performance', {
          ...this.trackPerformanceMeta[`${type}|${name}`],
          ...meta,
          ...connectionMeta,
          isMobile: true,
          nonInteraction: 1,
          type,
          name,
          duration,
        });
      }
    }
  },

  pageTransition() {
    const pageName = this.getCurrentRoute().name;
    this.pageViewed(pageName);
  },

  pageViewed(eventName, properties = {}) {
    const analyticsProperties = {
      isMobile: true,
      nonInteraction: 1,
      ...properties,
      ...(properties.hotel_id ? this.getHotelDetails(properties.hotel_id) : {}),
      pageName: startCase(this.getCurrentRoute().name),
    };
    if (androidService.isAndroid) {
      androidService.track(`${eventName} Page Viewed`, analyticsProperties);
    } else if (eventName === 'Confirmation') {
      window.clevertap.event.push(`${eventName} Page Viewed`, analyticsProperties);
    }
    analyticsService.page(`${eventName}`, analyticsProperties);
  },

  trackInteractiveEvents(eventName, properties = {}) {
    analyticsService.track(`${eventName}`, {
      ...properties,
      pageName: startCase(this.getCurrentRoute().name),
    });
  },

  trackNonInteractiveEvents(eventName, properties = {}) {
    analyticsService.track(`${eventName} Content Viewed`, {
      ...properties,
      nonInteraction: 1,
      pageName: startCase(this.getCurrentRoute().name),
    });
  },

  searchTreebo(prevSearch) {
    let prevSearchProperties = {};
    if (prevSearch) {
      const newSearch = this.getSearchData(this.getState());
      const oldSearch = this.getSearchData({ search: prevSearch });
      const oldStayDuration = oldSearch.checkout.diff(oldSearch.checkin, 'days');
      const newStayDuration = newSearch.checkout.diff(newSearch.checkin, 'days');
      const deltaStayDuration = newStayDuration - oldStayDuration;
      const searchDiff = {
        oldDestination: oldSearch.q,
        newDestination: newSearch.q,
        oldRoomConfig: oldSearch.roomConfig,
        newRoomConfig: newSearch.roomConfig,
        destinationChanged: oldSearch.q !== newSearch.q,
        deltaCheckinDate: newSearch.checkin.diff(oldSearch.checkin, 'days'),
        deltaCheckoutDate: newSearch.checkout.diff(oldSearch.checkout, 'days'),
        deltaStayDuration,
      };

      prevSearchProperties = {
        ...searchDiff,
        ...abService.actionProperties(['findPageCtaMobile']),
        pageName: startCase(this.getCurrentRoute().name),
        searchType: 'modify',
      };
    }
    const searchData = this.getSearchData(this.getState());
    const analyticsProperties = {
      searchType: 'new',
      ...searchData,
      ...this.getStayDates(),
      ...prevSearchProperties,
      ...abService.actionProperties(['findPageCtaMobile']),
    };
    if (androidService.isAndroid) {
      androidService.track(analyticsConstants.SEARCH_TREEBO, analyticsProperties);
    } else {
      window.clevertap.event.push(analyticsConstants.SEARCH_TREEBO, analyticsProperties);
    }
    this.trackInteractiveEvents(analyticsConstants.SEARCH_TREEBO, analyticsProperties);
    this.exposeGlobally();
  },

  searchBarClicked(page) {
    this.trackInteractiveEvents(analyticsConstants.SEARCH_BAR_CLICKED, {
      page,
    });
  },

  cityClicked(city, page) {
    this.trackInteractiveEvents(analyticsConstants.CITY_CLICKED, {
      city: city.toLowerCase(),
      page,
    });
  },

  similarTreeboClicked(selectedHotel, clickedHotel) {
    this.trackInteractiveEvents(analyticsConstants.SIMILAR_TREEBO_CLICKED, {
      isMobile: true,
      baseHotelName: selectedHotel.name,
      baseHotelId: selectedHotel.id,
      otherHotelName: clickedHotel.name,
      otherHotelId: clickedHotel.id,
    });
  },

  hotelViewDetailsClicked({ hotelId, cheapestPrice, cheapestRatePlan, $roomType, ...analyticsData }) {
    const { wallet, search: {
      roomConfig,
    } } = this.getState();
    const treeboPoints = wallet.byType.TP || wallet.byType[''];
    const { memberDiscount } = cheapestPrice;
    const analyticsProperties = {
      isMobile: true,
      roomType: $roomType.byId[cheapestPrice.id].type,
      ratePlan: cheapestRatePlan.code,
      price: Math.round(cheapestRatePlan.sellingPrice),
      totalPrice: Math.round(cheapestRatePlan.totalPrice),
      memberDiscountApplied: memberDiscount.isApplied,
      isSoldout: !cheapestPrice.available,
      roomConfig: roomConfig.rooms.map((room) => `${room.adults}-${room.kids || 0}`).join(','),
      isUsingTreeboPoints: treeboPoints.isUsingTreeboPoints,
      treeboPoints: treeboPoints.usableBalance,
      ...this.getHotelDetails(hotelId),
      ...this.getStayDates(),
      ...analyticsData,
    };

    this.trackInteractiveEvents(analyticsConstants.HOTEL_CLICKED, analyticsProperties);
    if (!androidService.isAndroid) {
      window.clevertap.event.push(analyticsConstants.HOTEL_CLICKED, analyticsProperties);
    }
  },

  quickBookClicked({ hotelId, cheapestPrice, cheapestRatePlan, $roomType, ...analyticsData }) {
    const { wallet } = this.getState();
    const treeboPoints = wallet.byType.TP || wallet.byType[''];
    const { memberDiscount } = cheapestPrice;
    const analyticsProperties = {
      isMobile: true,
      roomType: $roomType.byId[cheapestPrice.id].type,
      price: Math.round(cheapestRatePlan.sellingPrice),
      totalPrice: Math.round(cheapestRatePlan.totalPrice),
      memberDiscountApplied: memberDiscount.isApplied,
      isSoldout: !cheapestPrice.available,
      roomConfig: this.getRoomConfig(),
      isUsingTreeboPoints: treeboPoints.isUsingTreeboPoints,
      treeboPoints: treeboPoints.usableBalance,
      ...this.getHotelDetails(hotelId),
      ...this.getStayDates(),
      ...analyticsData,
      ...abService.actionProperties(['quickBookCTAMobile']),
    };
    if (androidService.isAndroid) {
      androidService.track(analyticsConstants.QUICK_BOOK_CLICKED, analyticsProperties);
    } else {
      window.clevertap.event.push(analyticsConstants.QUICK_BOOK_CLICKED, analyticsProperties);
    }
    window.analytics.track(analyticsConstants.QUICK_BOOK_CLICKED, analyticsProperties);
  },

  bookNowClicked({ hotelId, selectedPrice, selectedRatePlan, selectedRoomType }) {
    const { wallet } = this.getState();
    const treeboPoints = wallet.byType.TP || wallet.byType[''];
    window.analytics.track(analyticsConstants.BOOK_NOW_CLICKED, {
      isMobile: true,
      price: Math.round(get(selectedRatePlan, 'sellingPrice')),
      totalPrice: Math.round(get(selectedRatePlan, 'totalPrice')),
      isUsingTreeboPoints: treeboPoints.isUsingTreeboPoints,
      treeboPoints: treeboPoints.usableBalance,
      memberDiscountApplied: get(selectedPrice, 'memberDiscount.isApplied'),
      roomType: selectedRoomType.type,
      roomConfig: this.getRoomConfig(),
      ...this.getHotelDetails(hotelId),
      ...this.getStayDates(),
      ...abService.actionProperties(['hdCtaMobile']),
    });
  },

  hdBookNowClicked({ hotelId, selectedPrice, selectedRatePlan, selectedRoomType, coupon }) {
    const { wallet } = this.getState();
    const treeboPoints = wallet.byType.TP || wallet.byType[''];
    window.analytics.track(analyticsConstants.BOOK_NOW_CLICKED, {
      is_mobile: true,
      page: startCase(this.getCurrentRoute().name),
      sellingPrice: get(selectedRatePlan, 'sellingPrice'),
      price: get(selectedRatePlan, 'totalPrice'),
      coupon: coupon.appliedCouponCode,
      couponAmount: selectedRatePlan.discountAmount,
      is_using_treebo_points: treeboPoints.isUsingTreeboPoints,
      treebo_points: treeboPoints.usableBalance,
      member_discount_applied: get(selectedPrice, 'memberDiscount.isApplied'),
      roomType: selectedRoomType.type,
      ratePlan: selectedRatePlan.code,
      roomConfig: this.getRoomConfig(),
      ...this.getHotelDetails(hotelId),
      ...this.getStayDates(),
      ...abService.actionProperties(['hdCtaMobile']),
    });
  },

  checkoutStep(step, properties) {
    const stepNumber = parseInt(step[0], 10);
    const analyticsProperties = {
      isMobile: true,
      ...properties,
    };
    if (stepNumber === 0 && androidService.isAndroid) {
      androidService.track(`${step}`, analyticsProperties);
    }
    this.trackInteractiveEvents(`${step}`, analyticsProperties);
    if (!androidService.isAndroid && (stepNumber === 0 || stepNumber === 2)) {
      window.clevertap.event.push(`${step}`, analyticsProperties);
    }
  },

  addTravellerInfo(properties) {
    const analyticsProperties = {
      is_mobile: true,
      ...properties,
    };

    window.analytics.track(analyticsConstants.TRAVELLER_INFO_ADDED, analyticsProperties);
  },

  applyCouponLinkClicked() {
    this.trackInteractiveEvents(analyticsConstants.ADD_COUPON_LINK_CLICKED, {
      ...abService.actionProperties(['checkoutCouponMobile']),
    });
  },

  couponApplied(hotelId, coupon, success) {
    this.trackInteractiveEvents(analyticsConstants.COUPON_APPLIED, {
      coupon,
      success,
      ...this.getStayDates(),
      ...this.getHotelDetails(hotelId),
    });
  },

  ipLocationTracked(success, city) {
    if (__BROWSER__) {
      this.trackInteractiveEvents(analyticsConstants.IP_LOCATION, {
        city,
        success,
      });
    }
  },

  browserLocationTracked(allow, lat, long) {
    this.trackInteractiveEvents(analyticsConstants.BROWSER_LOCATION, {
      allow,
      lat,
      long,
    });
  },

  otpScreenDisplayed() {
    this.trackInteractiveEvents(analyticsConstants.OTP_SCREEN_DISPLAYED, {});
  },

  adwordsCompletedOrder(revenue, ratePlanType) {
    window.analytics.track('Adwords Completed Order', {
      isMobile: true,
      nonInteraction: 1,
      ...revenue,
      ratePlanType,
      ...abService.actionProperties(['pahCtaMobile', 'searchPage', 'perfectStayBanner', 'pwaSinglePageHd']),
    });
  },

  loginButtonClicked(page) {
    window.analytics.track(analyticsConstants.LOGIN_CLICKED, {
      isMobile: true,
      page,
    });
  },

  navigationIconClicked() {
    const { auth } = this.getState();
    window.analytics.track(analyticsConstants.MENU_CLICKED, {
      isMobile: true,
      isLoggedIn: auth.isAuthenticated,
    });
  },

  faqClicked(isLoggedIn, page) {
    const { auth } = this.getState();
    window.analytics.track(analyticsConstants.FAQ_CLICKED, {
      isMobile: true,
      isLoggedIn: auth.isAuthenticated,
      page,
    });
  },

  contactUsClicked(isLoggedIn, page) {
    const { auth } = this.getState();
    window.analytics.track(analyticsConstants.CONTACT_US_CLICKED, {
      isMobile: true,
      isLoggedIn: auth.isAuthenticated,
      page,
    });
  },

  mobileNumberEntered(mobile) {
    window.analytics.track(analyticsConstants.MOBILE_NUMBER_ENTERED, {
      isMobile: true,
      mobileNumber: mobile,
      pageName: startCase(this.getCurrentRoute().name),
    });
  },

  otpSent(mobile) {
    window.analytics.track(analyticsConstants.OTP_REQUESTED, {
      isMobile: true,
      mobileNumber: mobile,
      pageName: startCase(this.getCurrentRoute().name),
    });
  },

  resendOtpClicked(mobile) {
    window.analytics.track(analyticsConstants.VERIFY_OTP_STATUS, {
      isMobile: true,
      mobileNumber: mobile,
      pageName: startCase(this.getCurrentRoute().name),
    });
  },

  otpEntered(mobile) {
    window.analytics.track(analyticsConstants.OTP_ENTERED, {
      isMobile: true,
      mobileNumber: mobile,
      pageName: startCase(this.getCurrentRoute().name),
    });
  },

  changeNumberClicked(oldNumber, newNumber) {
    window.analytics.track(analyticsConstants.EDIT_MOBILE_CLICKED, {
      isMobile: true,
      oldNumber,
      newNumber,
      pageName: startCase(this.getCurrentRoute().name),
    });
  },

  invalidOtpEntered(mobile) {
    window.analytics.track(analyticsConstants.INVALID_OTP_ENTERED, {
      isMobile: true,
      nonInteraction: 1,
      mobile,
      pageName: startCase(this.getCurrentRoute().name),
    });
  },

  loginClicked(option) {
    window.analytics.track(`${option} Login Clicked`, {
      isMobile: true,
      pageName: startCase(this.getCurrentRoute().name),
    });
  },

  continueWithMobileClicked() {
    window.analytics.track(analyticsConstants.CONTINUE_WITH_MOBILE_CLICKED, {
      isMobile: true,
      pageName: startCase(this.getCurrentRoute().name),
    });
  },

  loginSuccess(type) {
    const eventData = {
      isMobile: true,
      nonInteraction: 1,
      pageName: startCase(this.getCurrentRoute().name),
      type,
    };
    if (androidService.isAndroid) androidService.track(analyticsConstants.LOGIN_SUCCESS, eventData);
    this.trackInteractiveEvents(analyticsConstants.LOGIN_SUCCESS, eventData);
  },

  loginDetails(user, type) {
    const userDetails = {
      firstName: user.firstName,
      lastName: user.lastName,
      mobileNumber: user.mobile,
      email: user.email,
      userId: user.userId,
      type,
    };
    const eventData = {
      isMobile: true,
      nonInteraction: 1,
      ...userDetails,
    };
    if (androidService.isAndroid) {
      androidService.track(analyticsConstants.LOGIN_DETAILS, eventData);
    } else {
      window.clevertap.profile.push({
        Site: {
          Name: user.name,
          Phone: `+91${user.mobile}`,
          Email: user.email,
        },
      });
    }
    window.analytics.track(analyticsConstants.LOGIN_DETAILS, eventData);
    window.analytics.identify(userDetails.user_id, userDetails);
  },

  loginModalClosed(flow) {
    window.analytics.track(analyticsConstants.LOGIN_MODAL_CLOSED, {
      isMobile: true,
      flow,
    });
  },

  logoutClicked() {
    if (!androidService.isAndroid) window.clevertap.profile.pop();
  },

  viewLocationClicked(hotelId, properties) {
    const state = this.getState();
    window.analytics.track(analyticsConstants.VIEW_LOCATION_CLICKED, {
      isMobile: true,
      entity: state.search.searchInput.place.type,
      q: state.search.searchInput.place.q,
      ...this.getHotelDetails(hotelId),
      ...properties,
    });
  },

  ratePlanChanged(properties) {
    const {
      hotelId,
      page,
      oldRatePlan,
      newRatePlan,
      oldRoomType,
      newRoomType,
      abwDuration,
    } = properties;

    const priceDiff = Math.round(oldRatePlan.sellingPrice - newRatePlan.sellingPrice);
    const analyticData = {
      page,
      entity: this.getState().search.searchInput.place.type,
      ...this.getHotelDetails(hotelId),
      oldRoomType: oldRoomType.type,
      oldRatePlan: oldRatePlan.code,
      oldRoomPrice: Math.round(oldRatePlan.sellingPrice),
      newRoomType: newRoomType.type,
      newRatePlan: newRatePlan.code,
      newRoomPrice: Math.round(newRatePlan.sellingPrice),
      priceDifference: priceDiff,
      roomTypeChanged: (oldRoomType.type !== newRoomType.type),
      ratePlanChanged: (oldRatePlan.plan !== newRatePlan.plan),
      abwDays: abwDuration,
    };
    this.trackInteractiveEvents(analyticsConstants.RATE_PLAN_SELECTED, {
      isMobile: true,
      ...analyticData,
    });
  },

  roomTypeChanged(properties) {
    window.analytics.track(analyticsConstants.ROOM_TYPE_CHANGED, {
      isMobile: true,
      entity: this.getState().search.searchInput.place.type,
      ...this.getHotelDetails(properties.hotel_id),
      baseRoomType: properties.oldRoom.type,
      baseRatePlan: properties.oldRoom.price.code,
      baseRoomPrice: Math.round(properties.oldRoom.price.sellingPrice),
      newRoomType: properties.newRoom.type,
      newRatePlan: properties.newRoom.price.code,
      newRoomPrice: Math.round(properties.newRoom.price.sellingPrice),
    });
  },

  filtersApplied(properties) {
    const state = this.getState();
    window.analytics.track(analyticsConstants.FILTER_APPLIED, {
      isMobile: true,
      entity: state.search.searchInput.place.type,
      q: state.search.searchInput.place.q,
      resultCount: state.filter.refinedHotelIds.length,
      ...properties,
      ...this.getFilterData(),
    });
  },

  sortApplied(properties) {
    const state = this.getState();
    window.analytics.track(analyticsConstants.SORT_APPLIED, {
      isMobile: true,
      entity: state.search.searchInput.place.type,
      q: state.search.searchInput.place.q,
      sortType: state.filter.sort.by,
      ...properties,
    });
  },

  memberDiscountApplied(page) {
    window.analytics.track(analyticsConstants.MEMBER_DISCOUNT_APPLIED, {
      isMobile: true,
      page,
    });
  },

  viewOtherRatePlansClicked(hotelId, price) {
    window.analytics.track(analyticsConstants.VIEW_OTHER_PLANS_CLICKED, {
      isMobile: true,
      ...this.getHotelDetails(hotelId),
      ...this.getStayDates(),
      ...this.getRoomConfig,
      ...price,
    });
  },

  showBiggerRoomsClicked() {
    window.analytics.track(analyticsConstants.VIEW_BIGGER_ROOMS_CLICKED, {
      isMobile: true,
    });
  },

  payAtHotelClicked(hotelId, selectedPrice, selectedRatePlan) {
    const { memberDiscount } = selectedPrice;
    const { coupon } = this.getState();
    this.trackInteractiveEvents(analyticsConstants.PAH_SELECTED, {
      isMobile: true,
      ...this.getHotelDetails(hotelId),
      ...this.getStayDates(),
      ...this.getRoomConfig(),
      ...selectedRatePlan,
      ratePlanType: selectedRatePlan.type,
      memberDiscount,
      coupon: coupon.appliedCouponCode,
      ...abService.impressionProperties(['pahCtaMobile']),
    });
  },

  bookingCancellationSuccess(bookingId, reason) {
    const { auth: { bookingHistory } } = this.getState();
    const bookingDetails = Object.values(bookingHistory.results).find((booking) => (booking.id === bookingId));
    const {
      dates,
      hotel,
      price,
      status,
      amountPaid,
      pendingAmount,
      coupon,
      partPayLink,
    } = bookingDetails;
    const cancelEventData = {
      orderId: bookingId,
      checkin: dates.checkIn,
      checkout: dates.checkOut,
      hotelName: hotel.name,
      hotelId: hotel.id,
      price: Math.round(price.totalPrice),
      paymentMode: (price.pendingAmount > 0) ? 'pay-at-hotel' : 'pre-paid',
      bookingStatus: status,
      amountPaid,
      amountPending: pendingAmount,
      coupon: coupon.code,
      memberDiscountAmount: price.memberDiscount,
      // member_discount_perc: ,
      partPayPending: status === 'Reserved' && partPayLink,
      // refund_amount_displayed: ,
      cancellationReason: reason,
    };
    window.analytics.track('Booking Successfully Cancelled', cancelEventData);
  },

  treeboRewardsClicked() {
    window.analytics.track(analyticsConstants.TREEBO_REWARDS_CLICKED, {
      isMobile: true,
    });
  },

  rewardsBannerClicked() {
    this.trackInteractiveEvents(analyticsConstants.REWARDS_BANNER_CLICKED, {});
  },

  loyaltyLandingPageClosed() {
    this.trackInteractiveEvents(analyticsConstants.LOYALTY_LANDING_CLOSED, {});
  },

  loyaltyLandingActionClicked(properties) {
    this.trackInteractiveEvents(analyticsConstants.LOYALTY_LANDING_ACTION_CLICKED, {
      ...properties,
    });
  },

  walletUsage(isWalletUsed) {
    this.trackInteractiveEvents(analyticsConstants.WALLET_USAGE, {
      isPointsUsed: isWalletUsed,
    });
  },

  walletPageActionClicked(properties) {
    this.trackInteractiveEvents(analyticsConstants.WALLET_PAGE_ACTION_CLICKED, {
      ...properties,
    });
  },

  feedbackDetail(analyticsStmt, analyticsProperties) {
    window.analytics.track(analyticsStmt, analyticsProperties);
  },

  perfectStayKnowMoreClicked() {
    this.trackInteractiveEvents(analyticsConstants.PERFECT_STAY_KNOW_MORE_CLICKED, {});
  },

  perfectStayExpandClicked() {
    this.trackInteractiveEvents(analyticsConstants.PERFECT_STAY_EXPANDED, {});
  },

  ctaClicked(name) {
    this.trackInteractiveEvents(analyticsConstants.PERFECT_STAY_CTA_CLICKED, {
      name,
    });
  },

  psychologicalTriggersViewed(page, hotelId, selectedRoomTypeId) {
    const {
      content: { psychologicalTriggers },
    } = this.getState();

    const psychologicalTrigger = psychologicalTriggers.byRoomTypeId[selectedRoomTypeId];

    this.trackInteractiveEvents(analyticsConstants.PSYCHOLOGICAL_TRIGGERS, {
      hotelId,
      isMobile: true,
      nonInteraction: 1,
      triggerDisplayed: !isEmpty(psychologicalTrigger),
      triggerType: !isEmpty(psychologicalTrigger) ? psychologicalTrigger.type : '',
    });
  },

  // View Events
  homeContentViewed(properties) {
    this.trackNonInteractiveEvents(analyticsConstants.HOME, properties);
  },

  checkoutContentViewed(properties) {
    this.trackNonInteractiveEvents(analyticsConstants.CHECKOUT_DETAILS, properties);
  },

  itineraryContentViewed(properties) {
    this.trackNonInteractiveEvents(analyticsConstants.ITINERARY, properties);
  },

  listContentViewed(properties) {
    this.trackNonInteractiveEvents(analyticsConstants.HOTEL_LIST, properties);
  },

  loyaltyLandingContentViewed(properties) {
    this.trackNonInteractiveEvents(analyticsConstants.REWARDS_LANDING, properties);
  },

  hotelContentViewed(properties) {
    this.trackNonInteractiveEvents(analyticsConstants.HOTEL_DETAIL, properties);
  },
};
