import isEmpty from 'lodash/isEmpty';
import moment from 'moment';
import kebabCase from 'lodash/kebabCase';
import { pluralize } from '../../services/utils';

export default {
  transformLandingContentApi({ data }) {
    return {
      offers: data.mobile_offers_list,
      banner: {
        imageUrl: data.mobile_landing_banner.image_url,
        description: data.mobile_landing_banner.description,
        title: data.mobile_landing_banner.title,
      },
      rewards: {
        landingPage: data.mobile_rewards.landingPage,
        introducingTreeboRewardsPage: data.mobile_rewards.aboutTreeboRewards,
        rewardsPage: data.mobile_rewards.rewardsPage,
        confirmationPage: data.mobile_rewards.confirmationPage,
      },
      popularCities: data.mobile_popular_cities,
      whyTreebo: data.promises_list,
    };
  },

  transformCitiesListApi({ data }) {
    return data.sort((city1, city2) => {
      if (city1.name < city2.name) return -1;
      if (city1.name > city2.name) return 1;
      return 0;
    });
  },

  transformSeoContentsApi({ data }) {
    return {
      content: {
        // common
        links: data.content.left_nav
          ? [data.content.left_nav].concat(data.content.links)
          : data.content.links,
        // searchResultsPage
        breadcrumbs: data.content.breadcrumbs,
        description: data.content.description,
        // hotelDetailsPage
        aboutHotel: data.content.about_hotel,
        policies: data.content.policies,
        facilities: data.content.facilities,
        guestFeedbacks: data.content.positive_feedback,
        nearbyPlaces: data.content.nearby,
        qna: !isEmpty(data.content.posts)
          ? {
            title: data.content.posts.title,
            content: data.content.posts.content.map((QnA) => ({
              question: {
                text: QnA.question,
                author: QnA.user,
                postedOn: QnA.posted_date,
              },
              answers: QnA.answers.map((answer) => ({
                text: answer.text,
                author: answer.user,
                postedOn: answer.posted_date,
                isVerified: answer.verified,
              })),
            })),
          } : null,
        // SeoSearchResultsPage
        cityImageUrl: data.content.city_image || null,
        minPrice: data.content.min_price || null,
        weather: !isEmpty(data.content.weather_widget) ? {
          title: data.content.weather_widget.content.header,
          averageTemp: data.content.weather_widget.content.avg_temp,
          summary: data.content.weather_widget.content.summary,
          icon: data.content.weather_widget.content.icon,
        } : null,
        blogs: !isEmpty(data.content.blogs) ? {
          title: data.content.blogs.title,
          blogsList: data.content.blogs.content.map((blog) => ({
            name: blog.title,
            blogUrl: blog.blog_url,
            imageUrl: blog.image_url,
            priority: blog.priority,
          })),
        } : null,
        reviews: !isEmpty(data.content.top_reviews) ? {
          title: data.content.top_reviews.title,
          reviewsList: data.content.top_reviews.content.map((review) => ({
            rating: review.review_ratings,
            ratingImage: review.rating_image,
            title: review.review_title,
            description: review.review_text,
            publishDate: moment(review.review_date).format('MMM YYYY'),
            user: {
              name: review.user.name,
            },
            hotel: {
              name: review.hotel.name,
              id: review.hotel.id,
            },
            isCrawlable: review.is_crawlable,
          })).sort((prevReview, nextReview) => (prevReview.rating < nextReview.rating)),
        } : null,
      },
      link: data.link,
      meta: data.meta,
      schema: data.schema,
      page: data.page,
    };
  },

  transformFaqApi({ data }) {
    return { faq: data.value };
  },

  transformRewardsContentApi({ data }) {
    return {
      rewards: {
        landingPage: data.mobile_rewards.landingPage,
        introducingTreeboRewardsPage: data.mobile_rewards.aboutTreeboRewards,
        rewardsPage: data.mobile_rewards.rewardsPage,
        confirmationPage: data.mobile_rewards.confirmationPage,
      },
    };
  },

  transformNewYearRateTagApi({ data }) {
    const response = {};
    Object.keys(data.special_rate_hotels).forEach((key) => {
      response[key] =
          data.special_rate_hotels[key].special_rate_dates.map((value) => ({
            text: value.text,
            startDate: value.start_date,
            endDate: value.end_date,
          }));
    });
    return response;
  },

  transformAdsBannerApi({ data }) {
    return { adsBanner: data.adsbanner_content };
  },

  transformTriggersApi({ data }, price) {
    return Object.keys(price.byRoomTypeId)
      .reduce((obj, roomTypeId) => ({
        ...obj,
        [roomTypeId]: this.makeTriggers(data, price, roomTypeId),
      }), {});
  },

  makeTriggers(payload, price, roomTypeId) {
    const [hotelId, roomType] = roomTypeId.split('|');
    const roomPrice = price.byRoomTypeId[roomTypeId];
    const triggers = !isEmpty(payload[hotelId])
      ? payload[hotelId]
      : [{ triggerType: 'hurry', displayText: 'Hurry! {rooms} left!' }];
    let trigger = triggers.find((t) => t.triggerType !== 'hurry');
    const isFillingFast = (roomType === 'oak') ? roomPrice.available < 5 : roomPrice.available < 2;
    if (isFillingFast) {
      trigger = { ...triggers.find((t) => t.triggerType === 'hurry') };
      trigger.displayText = trigger.displayText.replace(
        '{rooms}', `${roomPrice.available} ${pluralize(roomPrice.available, 'room')}`,
      );
    }
    return !isEmpty(trigger) && roomPrice.available ? {
      type: trigger.triggerType,
      text: trigger.displayText,
    } : {};
  },

  getSeoPathName(place) {
    const city = place.area.city_slug;
    if (place.type === 'landmark') {
      const landmark = place.area.landmark_slug || kebabCase(`${place.area.landmark}-${city}`);
      return `/hotels-near-${landmark}/`;
    } else if (place.type === 'locality') {
      const locality = kebabCase(place.area.locality);
      return `/hotels-in-${locality}-${city}/`;
    } else if (place.type === 'city') {
      return `/hotels-in-${city}/`;
    } else if (place.type === 'category') {
      const category = kebabCase(place.category);
      return `/${category}-in-${city}/`;
    } else if (place.type === 'amenity') {
      const amenity = kebabCase(place.amenity);
      return `/hotels-in-${city}-with-${amenity}/`;
    } else if (place.type === 'hotel_type') {
      const category = kebabCase(place.category);
      const isLocality = place.area.locality;
      const slug = isLocality ? kebabCase(place.area.locality) : place.area.landmark_slug;
      return isLocality ? `/${category}-in-${slug}-${city}/` : `/${category}-near-${slug}/`;
    } else if (place.type === 'hotel') {
      const hotel = kebabCase(place.area.hotel);
      const locality = kebabCase(place.area.locality);
      const hotelId = place.hotel_id;
      return `/hotels-in-${city}/${hotel}-${locality}-${hotelId}/`;
    }
    return '';
  },

  makeSeoQuery(route, params) {
    const seoQuery = {
      page: 'common',
      is_amp: false,
    };

    if (route.path === '/cities/') {
      seoQuery.q = 'cities';
    } else if (route.path === '/hotels-in-:city/' || route.path === '/hotels-in-:city/amp/') {
      seoQuery.page = 'city';
      seoQuery.q = params.city;
      seoQuery.is_amp = !route.path.includes('/amp/');
    } else if (route.path === '/:category-near-:landmark/' || route.path === '/:category-near-:landmark/amp/') {
      seoQuery.page = 'landmark-category';
      seoQuery.q = `${params.category}-near-${params.landmark}`;
      seoQuery.is_amp = !route.path.includes('/amp/');
    } else if (route.path === '/:category-in-:locality-:city(\\w+)/' || route.path === '/:category-in-:locality-:city(\\w+)/amp/') {
      seoQuery.page = 'locality-category';
      seoQuery.q = `${params.category}-in-${params.locality}-${params.city}`;
      seoQuery.is_amp = !route.path.includes('/amp/');
    } else if (route.path === '/hotels-in-:city-with-:amenity/' || route.path === '/hotels-in-:city-with-:amenity/amp/') {
      seoQuery.page = 'city-amenity';
      seoQuery.q = `${kebabCase(params.amenity)}-${params.city}`;
      seoQuery.is_amp = !route.path.includes('/amp/');
    } else if (route.path === '/hotels-in-:locality-:city(\\w+)/' || route.path === '/hotels-in-:locality-:city(\\w+)/amp/') {
      seoQuery.page = 'locality';
      seoQuery.q = `${params.locality}-${params.city}`;
      seoQuery.is_amp = !route.path.includes('/amp/');
    } else if (route.path === '/:category-in-:city/' || route.path === '/:category-in-:city/amp/') {
      seoQuery.page = 'category';
      seoQuery.q = `${params.category}-${params.city}`;
      seoQuery.is_amp = !route.path.includes('/amp/');
    } else if (route.path === '/hotels-near-:landmark/' || route.path === '/hotels-near-:landmark/amp/') {
      seoQuery.page = params.landmark === 'me' ? 'near' : 'landmark';
      seoQuery.q = params.landmark;
      seoQuery.is_amp = !route.path.includes('/amp/');
    } else if (route.path === '/hotels-in-:city/:hotelSlug-:hotelId(\\w+)/' || route.path === '/hotels-in-:city/:hotelSlug-:hotelId(\\w+)/amp/') {
      seoQuery.page = 'hd';
      seoQuery.q = params.hotelId;
      seoQuery.is_amp = !route.path.includes('/amp/');
    } else if (route.path === '/faq/') {
      seoQuery.q = 'faq';
    } else if (route.path === '/introducing-rewards/') {
      seoQuery.q = 'rewards';
    }

    return seoQuery;
  },
};
