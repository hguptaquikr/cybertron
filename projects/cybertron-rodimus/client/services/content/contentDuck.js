import { handle } from 'redux-pack';
import contentService from './contentService';
import * as routeService from '../../services/route/routeService';

const GET_CITIES_LIST = 'GET_CITIES_LIST';
const GET_FAQ_CONTENT = 'GET_FAQ_CONTENT';
const GET_HOTEL_DETAILS = 'GET_HOTEL_DETAILS';
const GET_LANDING_CONTENT = 'GET_LANDING_CONTENT';
const GET_REWARDS_CONTENT = 'GET_REWARDS_CONTENT';
const GET_SEO_CONTENT = 'GET_SEO_CONTENT';
const SUBMIT_QUESTION = 'SUBMIT_QUESTION';
const GET_NEW_YEAR_RATE_TAG = 'GET_NEW_YEAR_RATE_TAG';
const SUBMIT_FEEDBACK = 'SUBMIT_FEEDBACK';
const GET_FEEDBACK_TAGS = 'GET_FEEDBACK_TAGS';
const UPDATE_FEEDBACK_TAGS = 'UPDATE_FEEDBACK_TAGS';
const GET_PSYCHOLOGICAL_TRIGGERS = 'GET_PSYCHOLOGICAL_TRIGGERS';
const GET_ADS_BANNER_CONTENT = 'GET_ADS_BANNER_CONTENT';
const GET_PERFECT_STAY_CONTENT = 'GET_PERFECT_STAY_CONTENT';
const GET_WALLETS_PROMOTIONS_CONTENT = 'GET_WALLETS_PROMOTIONS_CONTENT';

const initialState = {
  seo: {
    content: {},
    link: [],
    meta: [],
    schema: [],
    page: [],
  },
  policies: [],
  banner: {
    imageUrl: '//images.treebohotels.com/images/homepage-mobile-banner-29-12-17.jpg?fm=pjpg&auto=compress',
    title: 'India\'s Top Rated Hotel Chain',
    description: 'Good rooms. Good service. Good prices.',
  },
  rewards: {
    landingPage: {
      imageUrl: '//images.treebohotels.com/images/book-direct-earn.svg',
    },
    introducingTreeboRewardsPage: {
      icons: {},
      imageUrl: '//images.treebohotels.com/images/introducing-treebo-rewards.svg',
    },
    rewardsPage: {
      imageUrl: '//images.treebohotels.com/images/no-treebo-points.svg',
    },
    confirmationPage: {
      imageUrl: '//images.treebohotels.com/images/why-login.svg',
    },
  },
  offers: [],
  popularCities: [],
  whyTreebo: [],
  cities: [],
  faq: [],
  newYearRateTag: [],
  feedback: {
    state: '',
    user: {
      name: '',
    },
    booking: {
      hotel_name: '',
      hotel_address: '',
      from_date: '',
      to_date: '',
      guest: '',
    },
    current: {
      sections: {
        tag_section: {
          values: [],
        },
      },
    },
  },
  psychologicalTriggers: {
    byRoomTypeId: {
      '0|oak': {
        type: '',
        text: '',
        value: '',
      },
    },
  },
  perfectStay: {
    metas: [],
  },
  walletsPromotion: {
    phonePe: '',
  },
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_SEO_CONTENT: return handle(state, action, {
      success: (s) => ({ ...s, seo: payload }),
    });

    case GET_LANDING_CONTENT: return handle(state, action, {
      success: (s) => ({ ...s, ...payload }),
    });

    case GET_CITIES_LIST: return handle(state, action, {
      success: (s) => ({ ...s, cities: payload }),
    });

    case GET_FAQ_CONTENT: return handle(state, action, {
      success: (s) => ({ ...s, faq: payload.faq }),
    });

    case GET_PERFECT_STAY_CONTENT: return handle(state, action, {
      success: (s) => ({ ...s, perfectStay: payload.data.perfect_stay }),
    });

    case GET_WALLETS_PROMOTIONS_CONTENT: return handle(state, action, {
      success: (s) => ({ ...s, walletsPromotion: payload.data.wallets_promotion }),
    });

    case GET_REWARDS_CONTENT: return handle(state, action, {
      success: (s) => ({
        ...s,
        rewards: {
          ...s.rewards,
          ...payload.rewards,
        },
      }),
    });

    case GET_HOTEL_DETAILS: return handle(state, action, {
      success: (s) => ({
        ...s,
        policies: payload.content.policies,
        whyTreebo: payload.content.whyTreebo,
      }),
    });

    case GET_NEW_YEAR_RATE_TAG: return handle(state, action, {
      success: (s) => ({ ...s, newYearRateTag: payload }),
    });

    case GET_FEEDBACK_TAGS: return handle(state, action, {
      success: (s) => ({ ...s, feedback: payload.data }),
    });

    case UPDATE_FEEDBACK_TAGS:
      return {
        ...state,
        feedback: payload.feedback,
      };

    case GET_PSYCHOLOGICAL_TRIGGERS:
      return handle(state, action, {
        success: (s) => ({
          ...s,
          psychologicalTriggers: {
            byRoomTypeId: {
              ...s.byRoomTypeId,
              ...payload,
            },
          },
        }),
      });

    case GET_ADS_BANNER_CONTENT: return handle(state, action, {
      success: (s) => ({ ...s, adsBanner: payload.adsBanner }),
    });

    default:
      return state;
  }
};

export const getSeoContent = (route, params) => (dispatch, getState, { api }) => {
  const seoQuery = contentService.makeSeoQuery(route, params);

  return dispatch({
    type: GET_SEO_CONTENT,
    promise: api.get('/api/web/v3/contents/meta/', seoQuery)
      .then(contentService.transformSeoContentsApi),
  });
};

export const getLandingContent = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_LANDING_CONTENT,
    promise: api.get('/api/web/v2/contents/', {
      keys: 'mobile_landing_banner,promises_list,mobile_offers_list,' +
        'mobile_popular_cities, mobile_rewards',
    }).then(contentService.transformLandingContentApi),
  });

export const getCitiesList = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_CITIES_LIST,
    promise: api.get('/api/web/v2/cities/')
      .then(contentService.transformCitiesListApi),
  });

export const getFaqContent = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_FAQ_CONTENT,
    promise: api.get('/api/web/v1/contents/', { key: 'faq_content' })
      .then(contentService.transformFaqApi),
  });

export const getAdsBannerContent = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_ADS_BANNER_CONTENT,
    promise: api.get('/api/web/v2/contents/', { keys: 'adsbanner_content' })
      .then(contentService.transformAdsBannerApi),
  });

export const getRewardsContent = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_REWARDS_CONTENT,
    promise: api.get('/api/web/v2/contents/', {
      keys: 'mobile_rewards',
    }).then(contentService.transformRewardsContentApi),
  });

export const submitQuestion = (question, pathname) => (dispatch, getState, { api }) => {
  const submitQuestionQuery = {
    question,
    page: pathname,
  };
  return dispatch({
    type: SUBMIT_QUESTION,
    promise: api.post('/api/web/v1/posts/add/question/', submitQuestionQuery),
  });
};

export const getNewYearRateTag = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_NEW_YEAR_RATE_TAG,
    promise: api.get('/api/web/v2/contents/', { keys: 'special_rate_hotels' })
      .then(contentService.transformNewYearRateTagApi),
  });

export const getFeedbackTags = (feedbackId) => (dispatch, getState, { api }) => dispatch({
  type: GET_FEEDBACK_TAGS,
  promise: api.get(`/api/web/feedback/v1/feedbacks/${feedbackId}/`),
});

export const updateFeedbackTags = (tagName, tagValue) => (dispatch, getState) => {
  const { content: { feedback } } = getState();
  const updatedFeedback = {
    ...feedback,
    current: {
      ...feedback.current,
      sections: {
        ...feedback.current.sections,
        tag_section: {
          ...feedback.current.sections.tag_section,
          values: feedback.current.sections.tag_section.values.map((value) => ({
            ...value,
            body: {
              name: value.name,
              value: value.name === tagName ? tagValue : value.body.value,
            },
          })),
        },
      },
    },
  };
  return dispatch({
    type: UPDATE_FEEDBACK_TAGS,
    payload: { feedback: updatedFeedback },
  });
};

export const submitFeedback = (query) => (dispatch, getState, { api }) =>
  dispatch({
    type: SUBMIT_FEEDBACK,
    payload: query,
    promise: api.put('/api/web/feedback/v1/feedbacks/', query),
  });

export const getPsychologicalTriggers = (hotelIds) => (dispatch, getState, { api }) => {
  const { search, price } = getState();
  const psychologicalTriggersQuery = {
    ...routeService.makeHotelIdQuery(hotelIds.filter(Number).join(',')),
    ...routeService.makeDateRangeQuery(search.datePicker.range),
    ...routeService.makeRoomConfigQuery(search.roomConfig.rooms),
  };

  return dispatch({
    type: GET_PSYCHOLOGICAL_TRIGGERS,
    promise: api.get('/api/web/v1/psychological-triggers/', psychologicalTriggersQuery)
      .then((res) => contentService.transformTriggersApi(res, price)),
  });
};

export const getPerfectStayContent = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_PERFECT_STAY_CONTENT,
    promise: api.get('/api/web/v2/contents/', { keys: 'perfect_stay' }),
  });

export const getWalletsPromotion = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_WALLETS_PROMOTIONS_CONTENT,
    promise: api.get('/api/web/v2/contents/', { keys: 'wallets_promotion' }),
  });
