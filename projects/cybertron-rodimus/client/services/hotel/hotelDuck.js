import { handle } from 'redux-pack';
import startCase from 'lodash/startCase';
import merge from 'lodash/merge';
import pick from 'lodash/pick';
import * as hotelService from './hotelService';
import searchService from '../../services/search/searchService';
import { omitKeys } from '../utils';

const ADD_DISTANCE_TO_RESULTS = 'ADD_DISTANCE_TO_RESULTS';
const GET_HOTEL_RESULTS = 'GET_HOTEL_RESULTS';
const GET_NEARBY_HOTEL_RESULTS = 'GET_NEARBY_HOTEL_RESULTS';
const GET_HOTEL_DETAILS = 'GET_HOTEL_DETAILS';
const GET_CONFIRMATION_DETAILS = 'GET_CONFIRMATION_DETAILS';
const GET_CHECKOUT_DETAILS = 'GET_CHECKOUT_DETAILS';
const GET_SEARCH_PRICES = 'GET_SEARCH_PRICES';
const GET_NEARBY_SEARCH_PRICES = 'GET_NEARBY_SEARCH_PRICES';
const GET_ROOM_PRICES = 'GET_ROOM_PRICES';

const initialState = {
  byId: {
    0: {
      id: '',
      name: '',
      isActive: true,
      coordinates: {},
      address: {},
      description: '',
      amenities: [],
      accessibilities: [],
      trilights: [],
      images: [],
      reviews: {},
      roomTypeIds: [],
      policies: [],
    },
  },
  ids: [],
  isHotelResultsLoading: false,
  isHotelDetailsLoading: false,
};

export default (state = initialState, action) => {
  const { type, payload, meta } = action;
  switch (type) {
    case ADD_DISTANCE_TO_RESULTS:
      return {
        ...state,
        byId: merge({}, state.byId, payload.byId),
      };
    case GET_HOTEL_RESULTS: return handle(state, action, {
      start: (s) => ({
        ...s,
        byId: payload.byId,
        ids: payload.ids,
        isHotelResultsLoading: true,
      }),
      success: (s) => ({
        ...s,
        byId: merge({}, s.byId, payload.byId),
        ids: Array.from(
          new Set([
            ...s.ids,
            ...payload.ids,
          ]),
        ),
      }),
      finish: (s) => ({ ...s, isHotelResultsLoading: false }),
    });

    case GET_SEARCH_PRICES: return handle(state, action, {
      success: (s) => ({
        ...s,
        byId: merge({}, s.byId, pick(payload.hotel.byId, Object.keys(s.byId))),
      }),
    });

    case GET_NEARBY_SEARCH_PRICES: return handle(state, action, {
      start: (s) => ({
        ...s,
      }),
      success: (s) => ({
        ...s,
        byId: merge({}, s.byId, payload.hotel.byId),
      }),
    });

    case GET_NEARBY_HOTEL_RESULTS: return handle(state, action, {
      start: (s) => ({
        ...s,
      }),
      success: (s) => ({
        ...s,
        byId: merge({}, s.byId, payload.byId),
        ids: Array.from(
          new Set([
            ...s.ids,
            ...payload.ids,
          ]),
        ),
      }),
    });

    case GET_HOTEL_DETAILS: return handle(state, action, {
      start: (s) => ({ ...s, isHotelDetailsLoading: true }),
      success: (s) => ({
        ...s,
        byId: {
          ...s.byId,
          [payload.hotel.id]: merge({}, s.byId[payload.hotel.id], payload.hotel),
        },
      }),
      finish: (s) => ({ ...s, isHotelDetailsLoading: false }),
    });

    case GET_ROOM_PRICES: return handle(state, action, {
      success: (s) => ({
        ...s,
        byId: {
          ...s.byId,
          [meta.startPayload.hotelId]: {
            ...s.byId[meta.startPayload.hotelId],
            roomTypeIds: payload.price.roomTypeIds,
          },
        },
      }),
    });

    case GET_CONFIRMATION_DETAILS: return handle(state, action, {
      success: (s) => ({
        ...s,
        byId: {
          ...s.byId,
          [payload.booking.hotelId]: {
            ...s.byId[payload.booking.hotelId],
            ...payload.hotel.byId[payload.booking.hotelId],
          },
        },
      }),
    });

    case GET_CHECKOUT_DETAILS: return handle(state, action, {
      success: (s) => ({
        ...s,
        byId: {
          ...s.byId,
          [payload.checkout.hotelId]: merge(
            {},
            s.byId[payload.checkout.hotelId],
            payload.hotel.byId[payload.checkout.hotelId],
          ),
        },
      }),
    });

    default:
      return state;
  }
};

export const getHotelResults = (query, params) => async (dispatch, getState, { api }) => {
  const searchResultsQuery = {
    hotel_id: query.hotel_id || params.hotel_id,
    nearby: !!(query.hotel_id || params.hotel_id),
    state: startCase(query.state || params.state),
    city: startCase(query.city || params.city),
    locality: startCase(query.locality || params.locality),
    landmark: startCase(query.landmark || params.landmark),
    category: startCase(query.category || params.category),
    distance_cap: !!(query.locality || query.landmark || params.locality || params.landmark),
    amenity: startCase(query.amenity || params.amenity),
  };

  // talk to backend about better fallbacks
  if (searchResultsQuery.landmark === 'Me') {
    if (!searchResultsQuery.city) {
      const res = await searchService.getIpLocation();
      searchResultsQuery.city = res.city;
    }
    searchResultsQuery.landmark = '';
  }

  return dispatch({
    type: GET_HOTEL_RESULTS,
    payload: {
      byId: initialState.byId,
      ids: initialState.ids,
      sort: query.lat && query.lng ? {
        by: 'distance',
        list: ['price', 'distance'],
        coordinates: {
          lat: query.lat,
          lng: query.lng,
        },
      } : null,
    },
    promise: api.get('/api/web/v3/search/', searchResultsQuery)
      .then(hotelService.transformHotelResultsApi),
  });
};

export const getNearbyHotelResults = (hotelId) => (dispatch, getState, { api }) => {
  const nearbyHotelResultsQuery = {
    hotel_id: hotelId,
    nearby: true,
  };
  return dispatch({
    type: GET_NEARBY_HOTEL_RESULTS,
    promise: api.get('/api/web/v3/search/', nearbyHotelResultsQuery)
      .then(hotelService.transformHotelResultsApi)
      .then((payload) => ({
        ...payload,
        byId: omitKeys(payload.byId, [hotelId]),
      })),
  });
};

export const getHotelDetails = (hotelId) => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_HOTEL_DETAILS,
    payload: { hotelId },
    meta: { hotelId },
    promise: api.get(`/api/web/v3/hotels/${hotelId}/details/`)
      .then(hotelService.transformHotelDetailsApi),
  });

export const addDistanceToResults = () => (dispatch, getState) => {
  const {
    filter: { sort },
    hotel,
  } = getState();

  return dispatch({
    type: ADD_DISTANCE_TO_RESULTS,
    payload: {
      byId: hotel.ids.reduce((obj, hotelId) => ({
        ...obj,
        [hotelId]: {
          ...hotel.byId[hotelId],
          distance:
            hotelService.getDistanceBetween(sort.coordinates, hotel.byId[hotelId].coordinates),
        },
      }), {}),
    },
  });
};
