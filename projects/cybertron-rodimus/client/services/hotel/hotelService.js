import Hashids from 'hashids';
import kebabCase from 'lodash/kebabCase';
import qs from 'query-string';
import * as roomTypeService from './../roomType/roomTypeService';

const coupleFriendlyPolicies = ['is_couple_friendly', 'is_local_id_allowed'];

const sortRoomType = (data) => {
  const roomTypePriority = ['common', 'acacia', 'oak', 'maple', 'mahogany'];
  const byRoomTypePriority = (a, b) =>
    roomTypePriority.indexOf(a.type || a) - roomTypePriority.indexOf(b.type || b);
  return data.sort(byRoomTypePriority);
};

export const transformPolicies = (policies) => {
  const sortedPolicies = [];
  const otherPolicies = [];
  policies.forEach((policy) => {
    const formattedPolicy = {
      isPolicy: policy.display_in_policy,
      isNeedToKnow: policy.display_in_need_to_know,
      description: policy.description,
      title: policy.title,
      type: policy.policy_type,
    };
    if (formattedPolicy.type === coupleFriendlyPolicies[0]) {
      sortedPolicies[0] = formattedPolicy;
    } else if (formattedPolicy.type === coupleFriendlyPolicies[1]) {
      sortedPolicies[1] = formattedPolicy;
    } else otherPolicies.push(formattedPolicy);
  });
  return [
    ...sortedPolicies.filter(Boolean),
    ...otherPolicies,
  ];
};

export const transformHotelResultsApi = ({ data }) => ({
  byId: data.result.reduce((obj, result) => ({
    ...obj,
    [result.id]: {
      id: result.id,
      name: result.hotelName,
      coordinates: result.coordinates || {},
      address: result.area || {},
      description: '',
      amenities: result.amenities || [],
      accessibilities: [],
      trilights: [],
      images: result.images,
      isTopRated: result.isFourPlusRated,
      isFrequentlyBooked: result.isFrequentlyBooked,
      isValueForMoney: result.isValueForMoney,
      isActive: result.is_active,
      policies: transformPolicies(result.hotel_policies),
      reviews: {
        isTaEnabled: result.ta_review ? result.ta_review.ta_enabled : false,
        overallRating: {
          rating: result.ta_review && result.ta_review.ta_enabled ? result.ta_review.rating : '',
          ratingCount: result.ta_review && result.ta_review.ta_enabled ? result.ta_review.count : '',
        },
        amenitiesRatings: [],
        userReviews: [],
      },
      roomTypeIds: [],
    },
  }), {}),
  ids: data.result.map((hotel) => hotel.id),
  filters: {
    distanceCap: data.filters.distance_cap,
  },
  sort: {
    by: data.sort.by,
    list: ['price', data.sort.by],
    coordinates: data.sort.coordinates,
  },
});

export const transformHotelDetailsApi = ({ data }) => ({
  hotel: {
    id: data.id,
    name: data.name,
    coordinates: data.coordinates || {},
    address: data.address || {},
    description: data.description,
    amenities: data.facilities || [],
    accessibilities: data.accessibilities,
    trilights: data.trilights,
    images: data.images && sortRoomType(Object.keys(data.images))
      .reduce((arr, roomType) => [
        ...arr,
        ...data.images[roomType].map((image) => ({ ...image, roomType })),
      ], []),
    isActive: data.is_active,
    coupleFriendly: data.coupleFriendly,
    reviews: {},
    policies: transformPolicies(data.hotel_policies),
    roomTypeIds: data.rooms.map((room) =>
      roomTypeService.makeRoomTypeId(data.id, room.type)),
  },
  roomType: {
    byId: data.rooms.reduce((roomType, room) => ({
      ...roomType,
      [roomTypeService.makeRoomTypeId(data.id, room.type)]: {
        type: room.type,
        amenities: room.facilities,
        maxOccupancy: {
          adults: room.max_adults,
          children: room.max_children,
          guests: room.max_guests,
          occupancy: room.occupancy,
        },
        area: room.size,
      },
    }), {}),
    ids: data.rooms.map((room) => roomTypeService.makeRoomTypeId(data.id, room.type)),
  },
  content: {
    policies: data.policies,
    whyTreebo: data.promises,
  },
});

// make inline in SearchHOC
export const transformHotelAvailabilityApi = ({ data }) => {
  const availableRooms = Object.keys(data.availability)
    .map((room) => ({
      type: room.toLowerCase(),
      count: data.availability[room],
    }));

  return {
    rooms: sortRoomType(availableRooms),
    available: data.available,
  };
};

export const constructAddress = ({ street, locality = '', city, pincode = '' }) => {
  const address = street ? `${street.replace(locality, '')} ${locality}, ${city} ${pincode}` : null;
  return address;
};

const hashids = new Hashids('bqoksIlEnWtsL9lmaiZD', 8);
export const decodeHotelId = (hotelId) =>
  !Number.isFinite(+hotelId) ? hashids.decode(hotelId)[0] : false;

export const isCoupleFriendly = (policies) =>
  policies && policies.some((policy) => policy && (policy.type === 'is_couple_friendly'));

export const getNeedToKnowPolicies = (policies) => policies.filter((policy) => policy.isNeedToKnow);

export const makeHotelDetailsLocation = (hotel, roomType, location, ratePlan) => {
  const city = kebabCase(hotel.address.city);
  const hotelSlug = kebabCase(`${hotel.name} ${hotel.address.locality}`);
  return {
    pathname: `/hotels-in-${city}/${hotelSlug}-${hotel.id}/`,
    search: qs.stringify({
      ...qs.parse(location.search),
      q: `${hotel.name}, ${hotel.address.locality}`,
      hotel_id: hotel.id,
      roomtype: roomType.type,
      rateplan: ratePlan.code,
    }),
  };
};

export const getDistanceBetween = (start, end, accuracy = 1, precision = 0) => {
  /* eslint-disable no-param-reassign, no-mixed-operators, no-plusplus, max-len */
  accuracy = Math.floor(accuracy);
  precision = Math.floor(precision);
  const toRad = Math.PI / 180;
  const a = 6378137;
  const b = 6356752.314245;
  const f = 1 / 298.257223563; // WGS-84 ellipsoid params
  const L = (end.lng - start.lng) * toRad;
  let cosSigma;
  let sigma;
  let sinAlpha;
  let cosSqAlpha;
  let cos2SigmaM;
  let sinSigma;
  const U1 = Math.atan((1 - f) * Math.tan(parseFloat(start.lat) * toRad));
  const U2 = Math.atan((1 - f) * Math.tan(parseFloat(end.lat) * toRad));
  const sinU1 = Math.sin(U1);
  const cosU1 = Math.cos(U1);
  const sinU2 = Math.sin(U2);
  const cosU2 = Math.cos(U2);
  let lambda = L;
  let lambdaP;
  let iterLimit = 100;
  do {
    const sinLambda = Math.sin(lambda);
    const cosLambda = Math.cos(lambda);
    sinSigma = Math.sqrt(cosU2 * sinLambda * (cosU2 * sinLambda) + (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
    if (sinSigma === 0) return 0; // co-incident points
    cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
    sigma = Math.atan2(sinSigma, cosSigma);
    sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
    cosSqAlpha = 1 - sinAlpha * sinAlpha;
    cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
    if (Number.isNaN(cos2SigmaM)) cos2SigmaM = 0; // equatorial line: cosSqAlpha=0 (§6)
    const C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
    lambdaP = lambda;
    lambda = L + (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));
  } while (Math.abs(lambda - lambdaP) > 1e-12 && --iterLimit > 0);
  if (iterLimit === 0) return NaN; // formula failed to converge
  const uSq = cosSqAlpha * (a * a - b * b) / (b * b);
  const A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
  const B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
  const deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
  const distance = (b * A * (sigma - deltaSigma)).toFixed(precision); // round to 1mm precision
  return Math.round(distance * (10 ** precision) / accuracy) * accuracy / (10 ** precision);
  /* eslint-enable no-param-reassign, no-mixed-operators, no-plusplus, max-len */
};
