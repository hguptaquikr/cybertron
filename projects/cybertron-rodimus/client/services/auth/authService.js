import androidService from '../android/androidService';
import storageService from '../storage/storageService';
import * as routeService from '../route/routeService';
import config from '../../../config';

let ip = '';

export const setIp = (ipAddress) => {
  ip = ipAddress;
};

export const getIp = () => ip;

export const setToken = (token) => {
  storageService.setItem('auth_token', token);
};

export const getToken = () =>
  storageService.getItem('auth_token');

export const refreshAuthToken = (newAuthToken) => {
  if (newAuthToken) {
    storageService.setItem('auth_token', newAuthToken.split(' ')[1]);
  }
};

export const setUser = (user) => {
  storageService.setItem('user', JSON.stringify(user));
};

export const getUser = () => {
  const user = storageService.getItem('user');
  return user ? JSON.parse(user) : false;
};

export const unsetAuth = () => {
  storageService.removeItem('auth_token');
  storageService.removeItem('user');
};

export const isLoggedIn = () =>
  getToken() && getUser();

export const authenticateRoute = () =>
  (nextState, replace) => {
    if (!isLoggedIn()) {
      replace('/login/');
    }
  };

export const isAlreadyAuthenticated = (altPathname) =>
  (nextState, replace) => {
    if (isLoggedIn()) {
      if (androidService.isAndroid) {
        const { userId } = getUser();
        routeService.followRedirectUrl({ user: userId });
        replace('/');
      } else {
        replace(altPathname);
      }
    }
  };

export const verifyDomain = () => {
  if (
    __BROWSER__
      && window.location.origin
      && !config.hostUrl === window.location.origin
  ) {
    throw new Error('Invalid Host');
  }
};

export const transformBookingHistoryApi = ({ data }) => {
  const transformBookingHistoryDetails = (booking) => ({
    id: booking.order_id,
    status: booking.status,
    amountPaid: booking.payment_detail.paid_amount,
    partPayAmount: booking.partial_payment_amount,
    partPayLink: booking.partial_payment_link,
    isPayAtHotel: booking.paid_at_hotel,
    hotel: {
      id: booking.hotel_detail.hotel_id,
      name: booking.hotel_name,
      city: booking.hotel_detail.city,
      address: booking.hotel_detail.address,
      locality: booking.hotel_detail.locality,
      state: booking.hotel_detail.state,
      imageUrl: booking.hotel_detail.showcased_image_url,
      contact: booking.hotel_detail.contact_number,
      coordinates: {
        lat: booking.hotel_detail.latitude,
        lng: booking.hotel_detail.longitude,
      },
    },
    dates: {
      checkIn: booking.checkin_date,
      checkOut: booking.checkout_date,
      durationOfStay: booking.total_number_of_nights,
    },
    roomConfig: {
      adults: booking.room_config.no_of_adults,
      kids: booking.room_config.no_of_childs,
      roomCount: booking.room_config.room_count,
      roomType: booking.room_config.room_type,
    },
    price: {
      basePrice: booking.payment_detail.room_price,
      tax: booking.payment_detail.tax,
      totalPrice: booking.payment_detail.total,
      memberDiscount: booking.payment_detail.member_discount,
      treeboPointsUsed: booking.payment_detail.wallet_deduction,
      voucherPrice: booking.payment_detail.voucher_amount,
      pendingAmount: booking.payment_detail.pending_amount,
      amountPaidViaGateway: booking.payment_detail.external_pg_share,
      ratePlan: {
        code: booking.rate_plan,
        tag: booking.rate_plan_meta && booking.rate_plan_meta.tag,
        type: booking.rate_plan_meta && booking.rate_plan_meta.refundable ? 'refundable' : 'non-refundable',
        description: booking.rate_plan_meta && booking.rate_plan_meta.description,
      },
    },
    coupon: {
      code: booking.payment_detail.coupon_code,
      amount: booking.payment_detail.discount,
    },
    guest: {
      contact: booking.user_detail.contact_number,
      name: booking.user_detail.name,
      email: booking.user_detail.email,
    },
    cancellation: {
      hash: booking.cancel_hash,
    },
  });

  const transformedData = {
    results: {},
    upcomingSortOrder: [],
    pastSortOrder: [],
  };

  transformedData.upcomingSortOrder = data.context.upcoming.map((bh) => {
    transformedData.results[bh.order_id] = {
      ...transformBookingHistoryDetails(bh),
      isPastBooking: false,
    };
    return bh.order_id;
  });

  transformedData.pastSortOrder = data.context.previous.map((bh) => {
    transformedData.results[bh.order_id] = {
      ...transformBookingHistoryDetails(bh),
      isPastBooking: true,
    };
    return bh.order_id;
  });

  return transformedData;
};

export const transformInitiateCancellationApi = ({ data }) => ({
  bookingId: data.booking_details.booking_id,
  isPayAtHotel: data.booking_details.paid_at_hotel,
  hotel: {
    name: data.booking_details.hotel_name,
    address: data.booking_details.hotel_address,
  },
  dates: {
    checkIn: data.booking_details.checkin_date,
    checkOut: data.booking_details.checkout_date,
  },
  guest: {
    name: data.booking_details.guest_name,
  },
  cancellation: {
    hash: data.booking_details.booking_hash,
    reasons: data.booking_details.reasons.map((reason) => ({
      label: reason.label,
      value: reason.value.toString(),
    })),
    status: data.booking_status.status.toUpperCase(),
    message: data.booking_status.message,
    date: data.cancelled_dateTime,
  },
});

export const transformConfirmCancellationApi = ({ data }) => ({
  bookingId: data.bookingId,
  cancellation: {
    hash: data.cancellationHash,
    status: data.booking_status.status,
    message: data.booking_status.message,
  },
});

export const transformLoginApi = ({ data }) => ({
  profile: {
    email: data.email,
    mobile: data.phone_number,
    firstName: data.first_name,
    lastName: data.last_name,
    name: `${data.first_name}${data.last_name ? ` ${data.last_name}` : ''}`,
    userId: data.id,
  },
  token: data.token,
});
