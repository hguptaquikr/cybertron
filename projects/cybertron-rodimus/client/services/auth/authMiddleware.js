import * as userActionCreators from '../auth/authDuck';
import * as authService from '../auth/authService';

export default ({ history }) => ({ dispatch }) => (next) => (action) => {
  if (action.error && action.payload.status === 401) {
    authService.unsetAuth();
    const unsetUserProfileDispatch = dispatch(userActionCreators.unsetUser());
    history.push('/login/');
    return unsetUserProfileDispatch;
  }
  return next(action);
};
