import { handle } from 'redux-pack';
import merge from 'lodash/merge';
import * as authService from './authService';

const SEND_LOGIN_OTP = 'SEND_LOGIN_OTP';
const UPDATE_LOGIN_MOBILE = 'UPDATE_LOGIN_MOBILE';
const VERIFY_LOGIN_OTP = 'VERIFY_LOGIN_OTP';
const EMAIL_LOGIN = 'EMAIL_LOGIN';
const GOOGLE_LOGIN = 'GOOGLE_LOGIN';
const FACEBOOK_LOGIN = 'FACEBOOK_LOGIN';
const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
const RESET_PASSWORD = 'RESET_PASSWORD';
const SET_USER = 'SET_USER';
const UNSET_USER = 'UNSET_USER';
const USER_LOGOUT = 'USER_LOGOUT';
const GET_BOOKING_HISTORY = 'GET_BOOKING_HISTORY';
const INITIATE_CANCELLATION = 'INITIATE_CANCELLATION';
const CONFIRM_CANCELLATION = 'CONFIRM_CANCELLATION';

const initialState = {
  isVerifyingOtp: false,
  isAuthenticated: false,
  profile: {
    email: '',
    firstName: '',
    lastName: '',
    mobile: '',
    name: '',
    userId: '',
  },
  bookingHistory: {
    isBookingHistoryLoading: false,
    upcomingSortOrder: [],
    pastSortOrder: [],
    results: {
      0: {
        id: '',
        status: '',
        amountPaid: '',
        partPayLink: '',
        partPayAmount: '',
        isPayAtHotel: false,
        isPastBooking: false,
        booking: { status: '', type: '', statusInfo: '' },
        hotel: {
          id: '',
          name: '',
          address: '',
          imageUrl: '',
          coordinates: { lat: '', lng: '' },
          contact: '',
        },
        dates: { checkIn: '', checkOut: '', durationOfStay: '' },
        price: {
          basePrice: '',
          tax: '',
          totalPrice: '',
          voucherPrice: '',
          memberDiscountPrice: '',
          pendingAmount: '',
        },
        coupon: { code: '', amount: '' },
        roomConfig: {},
        guest: { contact: '', name: '', email: '' },
        cancellation: {
          hash: '',
          hashUrl: '',
          status: '',
          message: '',
          charges: '',
          reasons: [],
        },
      },
    },
  },
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SEND_LOGIN_OTP: return handle(state, action, {
      start: (s) => ({
        ...s,
        profile: {
          ...s.profile,
          mobile: payload.mobile,
        },
      }),
    });

    case UPDATE_LOGIN_MOBILE:
      return {
        ...state,
        profile: {
          ...state.profile,
          mobile: payload.mobile,
        },
      };

    case VERIFY_LOGIN_OTP: return handle(state, action, {
      start: (s) => ({
        ...s,
        isVerifyingOtp: true,
      }),
      success: (s) => ({
        ...s,
        isAuthenticated: true,
        isVerifyingOtp: false,
        profile: {
          ...s.profile,
          ...payload.profile,
        },
      }),
      failure: (s) => ({
        ...s,
        isVerifyingOtp: false,
      }),
    });

    case FORGOT_PASSWORD: return handle(state, action, {
      start: (s) => ({
        ...s,
        profile: {
          ...s.profile,
          email: payload.email,
        },
      }),
    });

    case EMAIL_LOGIN:
    case GOOGLE_LOGIN:
    case FACEBOOK_LOGIN:
    case RESET_PASSWORD: return handle(state, action, {
      success: (s) => ({
        ...s,
        isAuthenticated: true,
        profile: {
          ...s.profile,
          ...payload.profile,
        },
      }),
      failure: (s) => ({
        ...s,
        error: payload.error,
      }),
    });

    case SET_USER:
      return {
        ...state,
        isAuthenticated: true,
        profile: {
          ...state.profile,
          ...payload.profile,
        },
      };

    case UNSET_USER:
      return {
        ...state,
        profile: {},
        isAuthenticated: false,
        wallet: {
          introducingTreeboRewardsPageViewed: false,
        },
      };

    case USER_LOGOUT: return handle(state, action, {
      success: (s) => ({
        ...s,
        isAuthenticated: false,
        profile: {},
        bookingHistory: {
          results: {
            0: s.bookingHistory.results[0],
          },
          upcomingSortOrder: [],
          pastSortOrder: [],
        },
      }),
      failure: (s) => ({
        ...s,
        error: payload.error,
      }),
    });

    case GET_BOOKING_HISTORY: return handle(state, action, {
      start: (s) => ({
        ...s,
        bookingHistory: {
          ...s.bookingHistory,
          isBookingHistoryLoading: true,
        },
      }),
      success: (s) => ({
        ...s,
        bookingHistory: merge({}, s.bookingHistory, payload),
      }),
      finish: (s) => ({
        ...s,
        bookingHistory: {
          ...s.bookingHistory,
          isBookingHistoryLoading: false,
        },
      }),
    });

    case INITIATE_CANCELLATION: return handle(state, action, {
      success: (s) => ({
        ...s,
        bookingHistory: {
          ...s.bookingHistory,
          results: {
            ...s.bookingHistory.results,
            [payload.bookingId]: {
              ...merge(
                {},
                payload,
                s.bookingHistory.results[payload.bookingId],
              ),
            },
          },
        },
      }),
    });

    case CONFIRM_CANCELLATION: return handle(state, action, {
      success: (s) => ({
        ...s,
        bookingHistory: {
          ...s.bookingHistory,
          results: {
            ...s.bookingHistory.results,
            [payload.bookingId]: {
              ...s.bookingHistory.results[payload.bookingId],
              cancellation: {
                ...payload.cancellation,
              },
            },
          },
        },
      }),
    });

    default:
      return state;
  }
};

export const sendLoginOtp = (mobile) => (dispatch, getState, { api }) =>
  dispatch({
    type: SEND_LOGIN_OTP,
    payload: { mobile },
    promise: api.post('/api/web/v2/auth/login/otp/', { phone_number: mobile }),
  });

export const updateLoginMobile = (mobile) => (dispatch) => {
  dispatch({
    type: UPDATE_LOGIN_MOBILE,
    payload: { mobile },
  });
  return dispatch(sendLoginOtp(mobile));
};

export const verifyLoginOtp = (mobile, otp) => (dispatch, getState, { api }) => {
  const verifyOtpData = {
    phone_number: mobile,
    verification_code: otp,
  };
  return dispatch({
    type: VERIFY_LOGIN_OTP,
    promise: api.post('/api/web/v2/auth/login/otp/verify/', verifyOtpData)
      .then(authService.transformLoginApi),
    meta: {
      onSuccess: (res) => {
        authService.setUser(res.profile);
        authService.setToken(res.token);
      },
    },
  });
};

export const emailLogin = (userQuery) => (dispatch, getState, { api }) =>
  dispatch({
    type: EMAIL_LOGIN,
    promise: api.post('/api/web/v2/auth/login/email/', userQuery)
      .then(authService.transformLoginApi),
    meta: {
      onSuccess: (res) => {
        authService.setUser(res.profile);
        authService.setToken(res.token);
      },
    },
  });

export const facebookLogin = (facebookResponse) => (dispatch, getState, { api }) => {
  const userQuery = {
    token: facebookResponse.accessToken,
  };

  return dispatch({
    type: FACEBOOK_LOGIN,
    promise: api.post('/api/web/v2/auth/login/facebook/', userQuery)
      .then(authService.transformLoginApi),
    meta: {
      onSuccess: (res) => {
        authService.setUser(res.profile);
        authService.setToken(res.token);
      },
    },
  });
};

export const googleLogin = (googleResponse) => (dispatch, getState, { api }) => {
  const userQuery = {
    token: googleResponse.tokenId,
  };

  return dispatch({
    type: GOOGLE_LOGIN,
    promise: api.post('/api/web/v2/auth/login/google/', userQuery)
      .then(authService.transformLoginApi),
    meta: {
      onSuccess: (res) => {
        authService.setUser(res.profile);
        authService.setToken(res.token);
      },
    },
  });
};

export const forgotPassword = (email) => (dispatch, getState, { api }) =>
  dispatch({
    type: FORGOT_PASSWORD,
    payload: { email },
    promise: api.post('/api/web/v2/auth/forgot-password/', { email }),
  });

export const resetPassword = (password, token) => (dispatch, getState, { api }) => {
  const resetPasswordData = {
    confirm: password,
    password,
    verification_code: token,
  };
  return dispatch({
    type: RESET_PASSWORD,
    promise: api.post('/api/web/v2/auth/reset-password/', resetPasswordData)
      .then(authService.transformLoginApi),
    meta: {
      onSuccess: (res) => {
        authService.setUser(res.profile);
        authService.setToken(res.token);
      },
    },
  });
};

export const setUser = (profile) => ({
  type: SET_USER,
  payload: { profile },
});

export const unsetUser = () => ({
  type: UNSET_USER,
});

export const userLogout = () => (dispatch, getState, { api }) =>
  dispatch({
    type: USER_LOGOUT,
    promise: api.get('/api/web/v2/auth/logout/'),
    meta: {
      onSuccess: () => {
        authService.unsetAuth();
      },
    },
  });

export const hydrateUser = () => (dispatch) => {
  if (authService.isLoggedIn()) {
    const user = authService.getUser();
    dispatch(setUser(user));
  }
};

export const getBookingHistory = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_BOOKING_HISTORY,
    promise: api.post('/api/web/v1/booking/booking-history/')
      .then(authService.transformBookingHistoryApi),
  });

export const initiateCancellation = (cancellationHash) =>
  (dispatch, getState, { api }) => {
    const cancellationData = {
      booking_hash: cancellationHash,
    };
    return dispatch({
      type: INITIATE_CANCELLATION,
      promise: api.get('/api/web/v1/growth/bookingdata/', cancellationData)
        .then(authService.transformInitiateCancellationApi),
    });
  };

export const confirmCancellation = (cancellationHash, bookingId, reason, message) =>
  (dispatch, getState, { api }) => {
    const cancellationData = {
      order_id: bookingId,
      booking_hash: cancellationHash,
      reason,
      message,
    };
    return dispatch({
      type: CONFIRM_CANCELLATION,
      promise: api.post('/api/web/v1/growth/submitcancel', cancellationData)
        .then((response) => ({
          data: {
            ...response.data,
            bookingId,
            cancellationHash,
          },
        }))
        .then(authService.transformConfirmCancellationApi),
    });
  };
