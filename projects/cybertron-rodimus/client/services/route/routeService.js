import React from 'react';
import qs from 'query-string';
import { Route, Switch } from 'react-router-dom';
import moment from 'moment';
import { constants } from '../utils';
import userService from '../user/userService';
import androidService from '../android/androidService';

export const renderRoutes = (routes, extraProps = {}, switchProps = {}) => (
  routes ? (
    <Switch {...switchProps}>
      {
        routes.map((route, i) => (
          <Route
            key={route.key || i}
            path={route.path}
            exact={route.exact}
            strict={route.strict}
            render={(props) => {
              const location = route.check && route.check(props);
              if (location) {
                props.history.push(location);
                return null;
              }
              return (
                <route.component
                  {...props}
                  {...extraProps}
                  route={route}
                />
              );
            }}
          />
        ))
      }
    </Switch>
  ) : null
);

// use history instead of window
export const followRedirectUrl = (query) => {
  const { redirectUrl } = qs.parse(window.location.search);
  if (redirectUrl) {
    window.open(`${redirectUrl}/?${qs.stringify(query)}`);
  }
};

export const makeHotelIdQuery = (hotelId) => ({
  hotel_id: hotelId,
});

export const makeDateRangeQuery = (range = {}) => {
  const checkin = range.start && moment(range.start);
  const checkout = range.end && moment(range.end);
  return {
    checkin: checkin && checkin.format(constants.dateFormat.query),
    checkout: checkout && checkout.format(constants.dateFormat.query),
  };
};

export const makeRoomConfigQuery = (rooms = []) => ({
  roomconfig: rooms
    .map((room) => `${room.adults}-${room.kids || 0}`)
    .join(','),
});

export const makeRoomTypeQuery = (roomType) => ({
  roomtype: roomType,
});

export const makeRatePlanQuery = (ratePlan) => ({
  rateplan: ratePlan,
});

export const makeCoordinatesQuery = (coordinates = {}) => ({
  lat: coordinates.lat,
  lng: coordinates.lng,
});

export const check = {
  member: (location = '/login/') => () => {
    if (!userService.isLoggedIn()) {
      return location;
    }
    return null;
  },
  guest: (location) => () => {
    if (userService.isLoggedIn()) {
      if (androidService.isAndroid) {
        const { userId } = userService.getUserProfile();
        followRedirectUrl({ user: userId });
        return '/';
      }
      return location;
    }
    return null;
  },
};
