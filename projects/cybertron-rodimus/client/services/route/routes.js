import Wrapper from '../../bootstrap/Wrapper/Wrapper';
import LandingPage from '../../views/LandingPage';
import SearchResultsPage from '../../views/SearchResultsPage';
import SeoSearchResultsPage from '../../views/SeoSearchResultsPage';
import AmpSeoSearchResultsPage from '../../views/SeoSearchResultsPage/SeoSearchResultsPage.amp';
import HotelDetailsPage from '../../views/HotelDetailsPage';
import AmpHotelDetailsPage from '../../views/HotelDetailsPage/HotelDetailsPage.amp';
import CheckoutPage from '../../views/CheckoutPage';
import ConfirmationPage from '../../views/ConfirmationPage';
import BookingHistoryListPage from '../../views/BookingHistory/BookingHistoryListPage';
import BookingHistoryDetailsPage from '../../views/BookingHistory/BookingHistoryDetailsPage';
import BookingHistoryCancellationPage from '../../views/BookingHistory/BookingHistoryCancellationPage';
import CitiesPage from '../../views/CitiesPage';
import IntroducingTreeboRewards from '../../views/IntroducingTreeboRewards';
import FaqPage from '../../views/FaqPage';
import LoginPage from '../../views/Authentication';
import ResetPasswordPage from '../../views/ResetPasswordPage';
import ReviewCollectPage from '../../views/ReviewsAndRatings/ReviewCollectPage';
import ReviewSubmittedPage from '../../views/ReviewsAndRatings/ReviewSubmittedPage';
import RewardsPage from '../../views/RewardsPage';
import Feedback from '../../views/Feedback/Feedback';
import FeedbackChange from '../../views/Feedback/FeedbackChange';
import FeedbackThanks from '../../views/Feedback/FeedbackThanks';
import PerfectStayPage from '../../views/PerfectStayPage/PerfectStayPage';
import PaymentProcess from '../../views/PaymentProcess/PaymentProcess';
import NotFoundPage from '../../views/NotFoundPage';
import * as routeService from './routeService';

const defaultCacheDuration = 3600000;
const routes = [{
  component: Wrapper,
  routes: [{
    path: '/',
    exact: true,
    strict: true,
    name: 'Home',
    component: LandingPage,
    cache: defaultCacheDuration,
  }, {
    path: '/cities/',
    exact: true,
    strict: true,
    name: 'Cities',
    component: CitiesPage,
    cache: defaultCacheDuration,
  }, {
    path: '/search/',
    exact: true,
    strict: true,
    name: 'Search',
    component: SearchResultsPage,
    cache: 0,
  }, {
    path: '/hotels-in-:city-with-:amenity/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoSearchResultsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/hotels-in-:city-with-:amenity/amp/',
    exact: true,
    strict: true,
    name: 'Amp Hotel Listing',
    component: AmpSeoSearchResultsPage,
    cache: 0,
  }, {
    path: '/hotels-in-:locality-:city(\\w+)/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoSearchResultsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/hotels-in-:locality-:city(\\w+)/amp/',
    exact: true,
    strict: true,
    name: 'Amp Hotel Listing',
    component: AmpSeoSearchResultsPage,
    cache: 0,
  }, {
    path: '/hotels-in-:city/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoSearchResultsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/hotels-in-:city/amp/',
    exact: true,
    strict: true,
    name: 'Amp Hotel Listing',
    component: AmpSeoSearchResultsPage,
    cache: 0,
  }, {
    path: '/:category-in-:locality-:city(\\w+)/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoSearchResultsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/:category-in-:locality-:city(\\w+)/amp/',
    exact: true,
    strict: true,
    name: 'Amp Hotel Listing',
    component: AmpSeoSearchResultsPage,
    cache: 0,
  }, {
    path: '/:category-in-:city/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoSearchResultsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/:category-in-:city/amp/',
    exact: true,
    strict: true,
    name: 'Amp Hotel Listing',
    component: AmpSeoSearchResultsPage,
    cache: 0,
  }, {
    path: '/hotels-near-:landmark/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoSearchResultsPage,
    cache: [{
      path: '*',
      duration: defaultCacheDuration,
    }, {
      path: '/hotels-near-me/',
      duration: 0,
    }],
  }, {
    path: '/hotels-near-:landmark/amp/',
    exact: true,
    strict: true,
    name: 'Amp Hotel Listing',
    component: AmpSeoSearchResultsPage,
    cache: 0,
  }, {
    path: '/:category-near-:landmark/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoSearchResultsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/:category-near-:landmark/amp/',
    exact: true,
    strict: true,
    name: 'Amp Hotel Listing',
    component: AmpSeoSearchResultsPage,
    cache: 0,
  }, {
    path: '/hotels-in-:city/:hotelSlug-:hotelId(\\w+)/',
    exact: true,
    strict: true,
    name: 'Hotel Details',
    component: HotelDetailsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/hotels-in-:city/:hotelSlug-:hotelId(\\w+)/amp/',
    exact: true,
    strict: true,
    name: 'Amp Hotel Details',
    component: AmpHotelDetailsPage,
    cache: 0,
  }, {
    path: '/itinerary/',
    exact: true,
    strict: true,
    name: 'Itinerary',
    component: CheckoutPage,
    cache: 0,
  }, {
    path: '/confirmation/:bookingId/',
    exact: true,
    strict: true,
    name: 'Confirmation',
    component: ConfirmationPage,
    cache: 0,
  }, {
    path: '/login/',
    exact: true,
    strict: true,
    check: routeService.check.guest('/'),
    name: 'Login',
    component: LoginPage,
    cache: defaultCacheDuration,
  }, {
    path: '/user/reset-password/:resetToken/',
    exact: true,
    strict: true,
    name: 'Reset Password',
    component: ResetPasswordPage,
    cache: defaultCacheDuration,
  }, {
    path: '/account/booking-history/',
    exact: true,
    strict: true,
    check: routeService.check.member(),
    name: 'Booking History List',
    component: BookingHistoryListPage,
    cache: 0,
  }, {
    path: '/account/booking-history/:bookingId/',
    exact: true,
    strict: true,
    check: routeService.check.member(),
    name: 'Booking History Details',
    component: BookingHistoryDetailsPage,
    cache: 0,
  }, {
    path: '/c-:cancellationHash/',
    exact: true,
    strict: true,
    name: 'Booking History Cancellation',
    component: BookingHistoryCancellationPage,
    cache: 0,
  }, {
    path: '/tripadvisor/reviews-:hotelId/',
    exact: true,
    strict: true,
    name: 'Reviews',
    component: ReviewCollectPage,
    cache: 0,
  }, {
    path: '/tripadvisor/thank-you/',
    exact: true,
    strict: true,
    name: 'Review Submitted',
    component: ReviewSubmittedPage,
    cache: defaultCacheDuration,
  }, {
    path: '/faq/',
    exact: true,
    strict: true,
    name: 'Faq',
    component: FaqPage,
    cache: defaultCacheDuration,
  }, {
    path: '/introducing-rewards/',
    exact: true,
    strict: true,
    name: 'Introducing Rewards',
    component: IntroducingTreeboRewards,
    cache: defaultCacheDuration,
  }, {
    path: '/rewards/',
    exact: true,
    strict: true,
    check: routeService.check.member(),
    name: 'Rewards',
    component: RewardsPage,
    cache: 0,
  }, {
    path: '/feedback/:feedbackId/',
    exact: true,
    strict: true,
    name: 'Feedback',
    component: Feedback,
    cache: 0,
  }, {
    path: '/feedback/:feedbackId/change/',
    exact: true,
    strict: true,
    name: 'Feedback Change',
    component: FeedbackChange,
    cache: 0,
  }, {
    path: '/feedback/:feedbackId/thanks/',
    exact: true,
    strict: true,
    name: 'Feedback Submit',
    component: FeedbackThanks,
    cache: 0,
  }, {
    path: '/perfect-stay/',
    exact: true,
    strict: true,
    name: 'Perfect Stay',
    component: PerfectStayPage,
    cache: defaultCacheDuration,
  }, {
    path: '/payment-process/:bookingId/',
    exact: true,
    strict: true,
    name: 'paymentProcess',
    component: PaymentProcess,
    cache: 0,
  }, {
    name: 'Not Found',
    component: NotFoundPage,
  }],
}];

export default routes;
