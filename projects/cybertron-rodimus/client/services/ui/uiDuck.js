import qs from 'query-string';

const IS_SEARCH_WIDGET_OPEN = 'IS_SEARCH_WIDGET_OPEN';
const SET_UTM_PARAMS = 'SET_UTM_PARAMS';
const IS_PERFECT_STAY_BANNER_OPEN = 'IS_PERFECT_STAY_BANNER_OPEN';

const initialState = {
  isSearchWidgetOpen: false,
  isProccedToBook: false,
  isPerfectStayBannerOpen: false,
  utmParams: {
    utm_source: '',
    utm_medium: '',
    utm_campaign: '',
  },
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case IS_SEARCH_WIDGET_OPEN:
      return {
        ...state,
        isSearchWidgetOpen: payload.show,
      };

    case SET_UTM_PARAMS:
      return {
        ...state,
        utmParams: payload.utmParams,
      };

    case IS_PERFECT_STAY_BANNER_OPEN:
      return {
        ...state,
        isPerfectStayBannerOpen: payload.show,
      };

    default:
      return state;
  }
};

export const showSearchWidget = (show) => ({
  type: IS_SEARCH_WIDGET_OPEN,
  payload: { show },
});

export const showPerfectStayBanner = (show) => ({
  type: IS_PERFECT_STAY_BANNER_OPEN,
  payload: { show },
});

export const setSearchParameters = (search) => (dispatch) => {
  const query = qs.parse(search);
  query.utm_source = query.utm_source || 'direct';
  dispatch({
    type: SET_UTM_PARAMS,
    payload: {
      utmParams: {
        utm_source: query.utm_source,
        utm_medium: query.utm_medium,
        utm_campaign: query.utm_campaign,
      },
    },
  });
};
