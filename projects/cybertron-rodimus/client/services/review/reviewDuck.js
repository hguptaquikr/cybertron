import union from 'lodash/union';
import { handle } from 'redux-pack';
import reviewService from './reviewService';

const GET_HOTEL_REVIEWS = 'GET_HOTEL_REVIEWS';
const REVIEW_SUBMITTED = 'REVIEW_SUBMITTED';
const GET_NON_JCR = 'GET_NON_JCR';

const initialState = {
  results: {
    0: {
      noOfReviews: '',
      overallRating: {
        ratingImage: '',
        rating: '',
        subRatings: [],
      },
      amenitiesRatings: [],
      userReviews: [],
      readReviewsUrl: '',
      writeReviewsUrl: '',
      isTaEnabled: false,
    },
  },
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_HOTEL_REVIEWS: return handle(state, action, {
      success: (s) => ({
        ...s,
        results: {
          ...s.results,
          [payload.hotelId]: {
            ...s.results[payload.hotelId],
            ...payload.reviews,
          },
        },
      }),
    });

    case GET_NON_JCR: return handle(state, action, {
      success: (s) => ({
        ...s,
        results: {
          ...s.results,
          [payload.hotelId]: {
            ...s.results[payload.hotelId],
            ...payload.reviews,
            userReviews: union(
              [],
              s.results[payload.hotelId].userReviews,
              payload.reviews.userReviews,
            ),
          },
        },
      }),
    });

    default:
      return state;
  }
};

export const getHotelReviews = (hotelId) => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_HOTEL_REVIEWS,
    promise: api.get('/api/web/v1/reviews/tripadvisor/hotel_review_data/', { hotel_id: hotelId })
      .then(reviewService.transformHotelReviewsApi),
  });

export const getNonJCR = (hotelId) => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_NON_JCR,
    promise: api.get('/api/web/v1/reviews/tripadvisor/hotel_non_jcr_review_data/', { hotel_id: hotelId })
      .then(reviewService.transformNonJCRApi),
  });

export const reviewSubmitted = (bookingId) => (dispatch, getState, { api }) =>
  dispatch({
    type: REVIEW_SUBMITTED,
    promise: api.get('/api/web/v1/growth/communication/update_review_status/', { booking_id: bookingId }),
  });
