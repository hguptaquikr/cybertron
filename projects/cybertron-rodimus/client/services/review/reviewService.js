import moment from 'moment';

export default {
  transformHotelReviewsApi({ request_data: requestData, data: { ta_reviews: reviews } }) {
    const httpUrlFallback = (url = '') => url.replace('http:', 'https:');
    const ratingMap = {
      5: 'Excellent',
      4: 'Very Good',
      3: 'Good',
      2: 'Average',
      1: 'Poor',
    };

    const overallRating = reviews.overall_rating || { rating_percent: [] };
    const amenitiesRatings = reviews.subratings || [];
    const topReviews = reviews.top_reviews || [];

    return {
      hotelId: requestData.hotel_id,
      reviews: {
        noOfReviews: reviews.num_reviews,
        overallRating: {
          ratingImage: httpUrlFallback(overallRating.image),
          rating: overallRating.rating,
          subRatings: overallRating.rating_percent.map((percent) => ({
            percent: percent.percent,
            level: ratingMap[percent.rating],
            noOfReviews: percent.review_str,
          })),
        },
        amenitiesRatings: amenitiesRatings.map((amenitiesRating) => ({
          name: amenitiesRating.name,
          rating: amenitiesRating.rating,
          image: httpUrlFallback(amenitiesRating.image),
        })),
        userReviews: topReviews.map((topReview) => ({
          rating: +topReview.review_detail.user_rating,
          ratingImage: httpUrlFallback(topReview.review_detail.image),
          title: topReview.review_detail.title,
          description: topReview.review_detail.text,
          publishDate: moment(topReview.review_date).format('MMM YYYY'),
          user: {
            image: httpUrlFallback(topReview.user.image_url),
            name: topReview.user.user_name,
          },
        })).sort((prevReview, nextReview) => (prevReview.rating < nextReview.rating)),
        readReviewsUrl: reviews.read_reviews_url,
        writeReviewsUrl: reviews.write_review_url,
        isTaEnabled: reviews.ta_enabled,
      },
    };
  },

  transformNonJCRApi({ request_data: requestData, data: { ta_non_jcr_reviews: reviews } }) {
    const httpUrlFallback = (url = '') => url.replace('http:', 'https:');
    const topReviews = reviews.top_reviews || [];
    return {
      hotelId: requestData.hotel_id,
      reviews: {
        userReviews: topReviews.map((topReview) => ({
          rating: +topReview.review_detail.user_rating,
          ratingImage: httpUrlFallback(topReview.review_detail.image),
          title: topReview.review_detail.title,
          description: topReview.review_detail.text,
          publishDate: moment(topReview.review_date).format('MMM YYYY'),
          user: {
            image: httpUrlFallback(topReview.user.image_url),
            name: topReview.user.user_name,
          },
        })).sort((prevReview, nextReview) => (prevReview.rating < nextReview.rating)),
      },
    };
  },

  getJCR(reviews) {
    return !__BROWSER__ ? reviews.filter((review) => review.isCrawlable) : reviews;
  },
};
