import { combineReducers } from 'redux';
import uiReducer from '../ui/uiDuck';
import authReducer from '../auth/authDuck';
import contentReducer from '../content/contentDuck';
import hotelReducer from '../hotel/hotelDuck';
import filterReducer from '../filter/filterDuck';
import priceReducer from '../price/priceDuck';
import loaderReducer from '../loader/loaderDuck';
import searchReducer from '../search/searchDuck';
import checkoutReducer from '../checkout/checkoutDuck';
import bookingReducer from '../booking/bookingDuck';
import couponReducer from '../coupon/couponDuck';
import toastReducer from '../toast/toastDuck';
import reviewReducer from '../review/reviewDuck';
import walletReducer from '../wallet/walletDuck';
import roomTypeReducer from '../roomType/roomTypeDuck';

export default combineReducers({
  ui: uiReducer,
  auth: authReducer,
  content: contentReducer,
  hotel: hotelReducer,
  filter: filterReducer,
  loader: loaderReducer,
  price: priceReducer,
  search: searchReducer,
  checkout: checkoutReducer,
  booking: bookingReducer,
  coupon: couponReducer,
  toast: toastReducer,
  review: reviewReducer,
  wallet: walletReducer,
  $roomType: roomTypeReducer,
});
