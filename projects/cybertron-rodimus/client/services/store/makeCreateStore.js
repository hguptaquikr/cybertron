import { createStore, compose, applyMiddleware } from 'redux';
import { default as reduxThunk } from 'redux-thunk';
import { middleware as reduxPack } from 'redux-pack';
import authMiddleware from '../auth/authMiddleware';
import rootReducer from './rootReducer';
import apiService from '../api/apiService';

const makeCreateStore = ({
  history,
}) => {
  const middlewares = [
    reduxThunk.withExtraArgument({ api: apiService, history }),
    reduxPack,
    authMiddleware({ history }),
  ].filter(Boolean);

  const storeEnhancers = [
    applyMiddleware(...middlewares),
    __BROWSER__ && __LOCAL__ && window.devToolsExtension && window.devToolsExtension(),
  ].filter(Boolean);

  return (initialState) => createStore(
    rootReducer,
    initialState,
    compose(...storeEnhancers),
  );
};

export default makeCreateStore;
