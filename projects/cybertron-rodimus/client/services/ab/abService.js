let user = {
  id: '',
};

let experiments = {};

const setExperiments = (exps = {}) => {
  experiments = exps;
};

export const getExperiments = () => experiments;

const setUser = (usr = {}) => {
  user = usr;
};

export const getUser = () => user;

export const init = (usr = {}, exps = {}) => {
  setUser(usr);
  setExperiments(exps);
};

export const transformAbApi = ({ data }) => ({
  user: {
    id: data.abUserId,
  },
  experiments: Object.keys(data.abExperiments)
    .reduce((acc, experiment) => {
      let payload;
      try {
        payload = JSON.parse(data.payloads[experiment]);
      } catch (e) {
        payload = data.payloads[experiment];
      }
      return {
        ...acc,
        [experiment]: {
          variant: data.abExperiments[experiment],
          payload,
        },
      };
    }, {}),
});

const abAnalyticsProperties = (abType, abExperiments) => ({
  abType,
  abApplication: 'website',
  abUserId: user.id,
  abExperiment: abExperiments.filter((exp) => experiments[exp]).join(','),
  abBucket: abExperiments
    .map((exp) => (experiments[exp] && `${exp}:${experiments[exp].variant}`))
    .filter(Boolean)
    .join(','),
});

export const actionProperties = (exps) =>
  abAnalyticsProperties('action', exps);

export const impressionProperties = (exps) =>
  abAnalyticsProperties('impression', exps);
