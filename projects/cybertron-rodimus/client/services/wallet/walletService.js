import moment from 'moment';
import storageService from '../storage/storageService';
import * as authService from '../auth/authService';
import analyticsService from '../analytics/analyticsService';

export default {
  updateWalletData(data) {
    const wallet = storageService.getItem('wallet');
    const walletData = {
      ...JSON.parse(wallet || '{}'),
      ...data,
    };
    storageService.setItem('wallet', JSON.stringify(walletData));
  },

  getWalletData() {
    const wallet = storageService.getItem('wallet');
    return JSON.parse(wallet || 'false');
  },

  hasViewedIntroducingTreeboRewardsPage() {
    return (nextState, replace) => {
      const alwaysShowFlows = ['Rewards Page'];
      const { flow } = nextState.location.query;
      if (__BROWSER__) {
        const wallet = this.getWalletData() || {};
        if (authService.isLoggedIn()) {
          if (!wallet.introducingTreeboRewardsPageViewed) {
            return true;
          }
          if (alwaysShowFlows.includes(flow)) {
            return true;
          }
        } else {
          return true;
        }
        replace(`/rewards/?flow=${flow}`);
        analyticsService.loyaltyLandingContentViewed({
          source: flow,
        });
      }
      return true;
    };
  },

  transformWalletBalanceApi({ data }) {
    return {
      wallets: data.wallets.reduce((obj, wallet) => ({
        [wallet.type]: {
          type: wallet.type,
          usableBalance: +wallet.spendable_amount,
          totalBalance: +wallet.total_balance,
          totalPoints: +wallet.total_points,
          isUsingTreeboPoints: !!wallet.spendable_amount,
        },
      }), {}),
      types: data.wallets.map((wallet) => wallet.type),
    };
  },

  transformWalletStatementApi({ data }) {
    const getTransactionType = (type) => {
      if (type === 'Rf') return 'refund';
      else if (type === 'Cr') return 'credit';
      return 'debit';
    };
    return {
      type: 'TP',
      statements: data.wallet_statement.map((transaction) => ({
        amount: +transaction.amount,
        type: getTransactionType(transaction.type),
        desc: transaction.particular,
        isPerishable: transaction.is_perishable,
        status: transaction.status,
        isExpired: transaction.status.toLowerCase() === 'expired',
        expiresOn: transaction.expires_on ?
          moment(transaction.expires_on).format('DD MMM ’YY') : '',
        createdOn: transaction.created_on ?
          moment(transaction.created_on).format('DD MMM ’YY, h:mmA') : null,
        customField: transaction.custom_field_one,
        credits: transaction.related_credits ? transaction.related_credits.map((credit) => ({
          type: 'credit',
          amount: +credit.amount,
          isPerishable: credit.is_perishable,
          desc: credit.particular,
          status: credit.status.toLowerCase(),
          isExpired: credit.status.toLowerCase() === 'expired',
          expiresOn: moment(credit.expires_on).format('DD MMM ’YY, h:mmA'),
          createdOn: moment(credit.created_on).format('DD MMM ’YY'),
        })) : null,
      })),
    };
  },

  transformWalletApplyApi({ data }, { roomTypeId }) {
    return {
      wallets: {
        [data.wallet_info.wallet_type]: {
          isUsingTreeboPoints: data.wallet_info.wallet_applied,
          totalBalance: data.wallet_info.total_wallet_balance,
          totalPoints: data.wallet_info.total_wallet_points,
          usableBalance: data.wallet_info.wallet_deductable_amount,
        },
      },
      price: {
        byRoomTypeId: {
          [roomTypeId]: {
            available: 1,
            id: roomTypeId,
            ratePlans: data.all_rate_plans.reduce((ratePlans, ratePlan) => ({
              ...ratePlans,
              [ratePlan.rate_plan.code]: {
                code: ratePlan.rate_plan.code,
                description: ratePlan.rate_plan.code,
                type: ratePlan.rate_plan.refundable ? 'refundable' : 'non-refundable',
                tag: ratePlan.rate_plan.tag,
                basePrice: ratePlan.price.pretax_price,
                treeboPointsUsed: ratePlan.price.wallet_discount,
                sellingPrice: ratePlan.price.net_payable_amount,
                strikedPrice: ratePlan.price.rack_rate,
                totalPrice: ratePlan.price.sell_price,
                tax: ratePlan.price.tax,
                discountPercentage: ratePlan.price.total_discount_percent,
                discountAmount: ratePlan.price.coupon_discount,
              },
            }), {}),
            memberDiscount: {
              value: data.selected_rate_plan.price.member_discount,
              isApplied: data.member_discount_applied,
              isAvailable: data.member_discount_available,
            },
          },
        },
        roomTypeIds: [roomTypeId],
      },
    };
  },
};
