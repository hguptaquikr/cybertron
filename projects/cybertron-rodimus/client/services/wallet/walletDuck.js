import { handle } from 'redux-pack';
import recycleState from 'redux-recycle';
import merge from 'lodash/merge';
import androidService from '../android/androidService';
import walletService from './walletService';

const APPLY_REMOVE_COUPON = 'APPLY_REMOVE_COUPON';
const APPLY_REMOVE_WALLET = 'APPLY_REMOVE_WALLET';
const GET_CHECKOUT_DETAILS = 'GET_CHECKOUT_DETAILS';
const GET_ROOM_PRICES = 'GET_ROOM_PRICES';
const GET_WALLET_BALANCE = 'GET_WALLET_BALANCE';
const GET_WALLET_STATEMENT = 'GET_WALLET_STATEMENT';
const USER_LOGOUT = 'USER_LOGOUT';

const initialState = {
  byType: {
    '': {
      usableBalance: undefined,
      totalBalance: undefined,
      totalPoints: undefined,
      statements: [],
      isUsingTreeboPoints: false,
    },
  },
  types: [],
};

export default recycleState(
  (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
      case GET_WALLET_BALANCE: return handle(state, action, {
        success: (s) => ({
          ...s,
          byType: merge({}, s.byType, payload.wallets),
          types: payload.types,
        }),
      });

      case GET_WALLET_STATEMENT: return handle(state, action, {
        success: (s) => ({
          ...s,
          byType: {
            ...s.byType,
            [payload.type]: {
              ...s.byType[payload.type],
              statements: payload.statements,
            },
          },
        }),
      });

      case GET_CHECKOUT_DETAILS: return handle(state, action, {
        success: (s) => ({
          ...s,
          byType: merge({}, s.byType, payload.wallets),
        }),
      });

      case APPLY_REMOVE_COUPON:
      case APPLY_REMOVE_WALLET: return handle(state, action, {
        start: (s) => ({
          ...s,
          byType: merge({}, s.byType, payload.wallets),
        }),
        success: (s) => ({
          ...s,
          byType: merge({}, s.byType, payload.wallets),
        }),
      });

      case GET_ROOM_PRICES: return handle(state, action, {
        success: (s) => ({
          ...s,
          byType: merge({}, s.byType, payload.wallets),
        }),
      });

      default:
        return state;
    }
  }, [
    USER_LOGOUT,
  ],
);

export const getWalletBalance = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_WALLET_BALANCE,
    promise: api.get('/api/web/v1/reward/balance/')
      .then(walletService.transformWalletBalanceApi),
  });

export const getWalletStatement = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_WALLET_STATEMENT,
    promise: api.get('/api/web/v1/reward/statement/')
      .then(walletService.transformWalletStatementApi),
  });

export const applyWallet = () => (dispatch, getState, { api }) => {
  const { booking, ui } = getState();
  const payload = {
    bid: booking.bid,
    channel: androidService.isAndroid ? 'app' : 'msite',
    ...ui.utmParams,
  };
  return dispatch({
    type: APPLY_REMOVE_WALLET,
    payload: { hotelId: booking.hotelId, wallets: { TP: { isUsingTreeboPoints: true } } },
    promise: api.post('/api/web/v5/checkout/wallet/apply/', payload)
      .then((res) => walletService.transformWalletApplyApi(res, booking)),
  });
};

export const removeWallet = () => (dispatch, getState, { api }) => {
  const { booking, ui } = getState();
  const payload = {
    bid: booking.bid,
    channel: androidService.isAndroid ? 'app' : 'msite',
    ...ui.utmParams,
  };
  return dispatch({
    type: APPLY_REMOVE_WALLET,
    payload: { hotelId: booking.hotelId, wallets: { TP: { isUsingTreeboPoints: false } } },
    promise: api.post('/api/web/v5/checkout/wallet/remove/', payload)
      .then((res) => walletService.transformWalletApplyApi(res, booking)),
  });
};
