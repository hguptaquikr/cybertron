import { handle } from 'redux-pack';
import isEmpty from 'lodash/isEmpty';
import searchService from './searchService';
import * as routeService from '../route/routeService';
import * as hotelService from '../hotel/hotelService';

const SET_SEARCH_MODAL = 'SET_SEARCH_MODAL';
const GET_SEARCH_SUGGESTIONS = 'GET_SEARCH_SUGGESTIONS';
const SEARCH_LOCATION_CHANGE = 'SEARCH_LOCATION_CHANGE';
const DATE_RANGE_CHANGE = 'DATE_RANGE_CHANGE';
const ADD_ROOM_CONFIG = 'ADD_ROOM_CONFIG';
const ADD_GUEST_TYPE = 'ADD_GUEST_TYPE';
const SUBTRACT_GUEST_TYPE = 'SUBTRACT_GUEST_TYPE';
const REMOVE_ROOM_CONFIG = 'REMOVE_ROOM_CONFIG';
const CHECK_HOTEL_AVAILABILITY = 'CHECK_HOTEL_AVAILABILITY';
const SET_SEARCH_STATE = 'SET_SEARCH_STATE';

const initialState = {
  searchModal: 'intermediate',
  searchInput: {
    isLoading: false,
    suggestions: [],
    place: {
      coordinates: {},
      area: {},
    },
  },
  datePicker: {
    range: { start: null, end: null },
    highlight: '',
  },
  roomConfig: {
    rooms: [
      { adults: 1, kids: 0 },
    ],
    roomType: '',
    ratePlan: '',
  },
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_SEARCH_MODAL:
      return {
        ...state,
        searchModal: payload.searchModal,
        datePicker: {
          ...state.datePicker,
          highlight: payload.type,
        },
      };

    case GET_SEARCH_SUGGESTIONS: return handle(state, action, {
      start: (s) => ({
        ...s,
        searchInput: {
          ...s.searchInput,
          isLoading: true,
        },
      }),
      success: (s) => ({
        ...s,
        searchInput: {
          ...s.searchInput,
          isLoading: false,
          suggestions: payload,
        },
      }),
      failure: (s) => ({
        ...s,
        searchInput: {
          ...s.searchInput,
          isLoading: false,
          error: payload.error,
        },
      }),
    });

    case SEARCH_LOCATION_CHANGE:
      return {
        ...state,
        searchInput: {
          ...state.searchInput,
          place: payload.place,
        },
      };

    case DATE_RANGE_CHANGE:
      return {
        ...state,
        datePicker: {
          ...state.datePicker,
          range: payload.range,
        },
      };

    case ADD_ROOM_CONFIG:
      return {
        ...state,
        roomConfig: {
          ...state.roomConfig,
          rooms: [
            ...state.roomConfig.rooms,
            { adults: 1, kids: 0 },
          ],
        },
      };

    case ADD_GUEST_TYPE:
      return {
        ...state,
        roomConfig: {
          ...state.roomConfig,
          rooms: state.roomConfig.rooms.map((room, roomNumber) => (
            roomNumber === payload.roomNumber ? {
              ...room,
              [payload.guestType]: +room[payload.guestType] + 1 > 5
                ? 5 : +room[payload.guestType] + 1,
            } : room
          )),
        },
      };

    case SUBTRACT_GUEST_TYPE:
      return {
        ...state,
        roomConfig: {
          ...state.roomConfig,
          rooms: state.roomConfig.rooms.map((room, roomNumber) => {
            if (roomNumber === payload.roomNumber) {
              let newGuestCount = +room[payload.guestType] - 1;
              if (payload.guestType === 'adults' && newGuestCount < 1) newGuestCount = 1;
              return {
                ...room,
                [payload.guestType]: newGuestCount < 0
                  ? 0 : newGuestCount,
              };
            }
            return room;
          }),
        },
      };

    case REMOVE_ROOM_CONFIG:
      return {
        ...state,
        roomConfig: {
          ...state.roomConfig,
          rooms: state.roomConfig.rooms.slice(0, state.roomConfig.rooms.length - 1),
        },
      };

    case SET_SEARCH_STATE:
      return {
        ...state,
        searchInput: {
          ...state.searchInput,
          place: payload.place || state.searchInput.place,
        },
        datePicker: {
          ...state.datePicker,
          range: payload.range || state.datePicker.range,
        },
        roomConfig: {
          ...state.roomConfig,
          rooms: payload.rooms || state.roomConfig.rooms,
          roomType: payload.roomType || state.roomConfig.roomType,
          ratePlan: payload.ratePlan || state.roomConfig.ratePlan,
        },
      };

    default:
      return state;
  }
};

export const setSearchModal = (searchModal, type) => ({
  type: SET_SEARCH_MODAL,
  payload: {
    searchModal,
    type,
  },
});

export const getSearchSuggestions = (input) => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_SEARCH_SUGGESTIONS,
    promise: api.get('/api/web/v1/search/autocomplete/', { search_string: input })
      .then((res) => {
        if ('near me'.includes(input.toLowerCase())) {
          res.data.unshift({
            label: 'Near Me',
            type: 'near_me',
            icon: 'icon-current-location',
            area: { country: 'India' },
          });
        }
        return res;
      })
      .then(searchService.transformAutocompleteApi),
  });

export const searchLocationChange = (place) => (dispatch, getState) => {
  const { search: { datePicker } } = getState();
  dispatch({
    type: SEARCH_LOCATION_CHANGE,
    payload: { place },
  });
  if (place.q && (!datePicker.range.start || !datePicker.range.end)) {
    dispatch(setSearchModal('datePicker'));
  } else if (place.q) {
    dispatch(setSearchModal('intermediate'));
  }
};

export const dateRangeChange = (range) => (dispatch) => {
  dispatch({
    type: DATE_RANGE_CHANGE,
    payload: { range },
  });
};

export const addRoomConfig = () => ({
  type: ADD_ROOM_CONFIG,
});

export const addGuestType = (roomNumber, guestType) => ({
  type: ADD_GUEST_TYPE,
  payload: {
    roomNumber,
    guestType,
  },
});

export const subtractGuestType = (roomNumber, guestType) => ({
  type: SUBTRACT_GUEST_TYPE,
  payload: {
    roomNumber,
    guestType,
  },
});

export const removeRoomConfig = () => ({
  type: REMOVE_ROOM_CONFIG,
});

export const isSearchWidgetValid = () => (dispatch, getState) => {
  const { search: { searchInput, datePicker, roomConfig } } = getState();
  if (!searchInput.place.q || isEmpty(searchInput.place.area)) {
    dispatch(setSearchModal('searchInput'));
    return false;
  } else if (!datePicker.range.start || !datePicker.range.end) {
    dispatch(setSearchModal('datePicker'));
    return false;
  } else if (isEmpty(roomConfig.rooms)) {
    dispatch(setSearchModal('roomConfig'));
    return false;
  }
  return true;
};

export const checkHotelAvailability = () => (dispatch, getState, { api }) => {
  const { search: { searchInput, datePicker, roomConfig } } = getState();
  const { hotelId } = searchInput.place;
  const availabilityQuery = {
    ...routeService.makeDateRangeQuery(datePicker.range),
    ...routeService.makeRoomConfigQuery(roomConfig.rooms),
  };

  return dispatch({
    type: CHECK_HOTEL_AVAILABILITY,
    promise: api.get(`/api/web/v2/hotels/${hotelId}/availability/`, availabilityQuery)
      .then(hotelService.transformHotelAvailabilityApi),
  });
};

export const setSearchState = (urlQuery) => ({
  type: SET_SEARCH_STATE,
  payload: searchService.getSearchState(urlQuery),
});
