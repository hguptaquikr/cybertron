import moment from 'moment';
import startCase from 'lodash/startCase';
import analyticsService from '../analytics/analyticsService';
import * as authService from '../auth/authService';
import { constants, pluralize } from '../utils';
import config from '../../../config';

export default {
  transformAutocompleteApi({ data }) {
    return data.map((result) => ({
      q: result.label,
      type: result.type,
      area: result.area,
      hotelId: result.hotel_id,
      hotelCount: result.hotel_count,
      coordinates: result.coordinates,
      googlePlaceId: result.place_id,
      icon: result.icon || '',
    }));
  },

  formattedRoomConfig(rooms) {
    const { adults, kids } = rooms.reduce((obj, room) => ({
      adults: obj.adults + +room.adults,
      kids: obj.kids + (+room.kids ? +room.kids : 0),
    }), { adults: 0, kids: 0 });
    const adultsText = `${adults} ${pluralize(adults, 'Adult')}`;
    const kidsText = `${kids} ${pluralize(kids, 'Kid')}`;
    const roomsText = `${rooms.length} ${pluralize(rooms.length, 'Room')}`;

    return {
      adults: {
        count: adults,
        text: adultsText,
      },
      kids: {
        count: kids,
        text: kidsText,
      },
      rooms: {
        count: rooms.length,
        text: roomsText,
      },
      text: [
        adultsText,
        kids ? kidsText : null,
        roomsText,
      ].filter(Boolean).join(', '),
    };
  },

  formattedDatePicker({ start, end }, dateFormat = constants.dateFormat.view) {
    const checkIn = start ? moment(start).format(dateFormat) : '';
    const checkOut = end ? moment(end).format(dateFormat) : '';
    const abwDuration = moment(start).startOf('day').diff(moment().startOf('day'), 'days');
    const numberOfNights = start && end ? moment(end).diff(start, 'days') : '';
    return {
      checkIn,
      checkOut,
      abwDuration,
      numberOfNights: {
        count: numberOfNights,
        text: `${numberOfNights} ${pluralize(numberOfNights, 'night')}`,
      },
    };
  },

  getGeoLocation() {
    return new Promise(((resolve, reject) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(({ coords: { latitude, longitude } }) => {
          analyticsService.browserLocationTracked(true, latitude, longitude);
          resolve({
            lat: latitude,
            lng: longitude,
          });
        }, () => {
          analyticsService.browserLocationTracked(false);
          reject(false);
        });
      } else {
        reject(false);
      }
    }));
  },

  async getIpLocation() {
    const response = await fetch(`https://pro.ip-api.com/json/${authService.getIp()}?key=${config.ipApiKey}`);
    const json = await response.json();
    if (response.ok) {
      analyticsService.ipLocationTracked(true, json.city);
      return {
        city: json.city,
      };
    }
    analyticsService.ipLocationTracked(false);
    return {
      city: '',
    };
  },

  getSearchState(combinedQuery) {
    const search = {};

    search.place = {
      q: combinedQuery.q
      || startCase(combinedQuery.hotelSlug)
      || startCase(combinedQuery.landmark)
      || startCase(combinedQuery.locality)
      || startCase(combinedQuery.city),
      area: {
        landmark: startCase(combinedQuery.landmark),
        locality: startCase(combinedQuery.locality),
        city: startCase(combinedQuery.city),
        state: startCase(combinedQuery.state),
        country: startCase(combinedQuery.country || 'India'),
      },
      coordinates: {
        lat: combinedQuery.lat,
        lng: combinedQuery.lng,
      },
      hotelId: combinedQuery.hotelId || combinedQuery.hotel_id,
    };

    const today = moment();
    let checkin = moment(combinedQuery.checkin);
    let checkout = moment(combinedQuery.checkout);
    checkin = checkin.diff(today, 'days') >= 0 ? checkin : today;
    checkout = checkout.diff(checkin, 'days') >= 1 ? checkout : moment(checkin).add(1, 'days');
    search.range = {
      start: checkin,
      end: checkout,
    };

    if (combinedQuery.roomconfig) {
      search.rooms = combinedQuery.roomconfig
        .split(',')
        .map((room) => {
          const [adults, kids] = room.split('-');
          return { adults, kids };
        });
    }

    search.roomType = combinedQuery.roomtype;
    search.ratePlan = combinedQuery.rateplan;

    return search;
  },
};
