import { handle } from 'redux-pack';
import merge from 'lodash/merge';
import * as routeService from '../../services/route/routeService';
import priceService from '../../services/price/priceService';
import analyticsService from '../../services/analytics/analyticsService';

const GET_SEARCH_PRICES = 'GET_SEARCH_PRICES';
const GET_NEARBY_SEARCH_PRICES = 'GET_NEARBY_SEARCH_PRICES';
const GET_ROOM_PRICES = 'GET_ROOM_PRICES';
const GET_CONFIRMATION_DETAILS = 'GET_CONFIRMATION_DETAILS';
const GET_CHECKOUT_DETAILS = 'GET_CHECKOUT_DETAILS';
const APPLY_REMOVE_COUPON = 'APPLY_REMOVE_COUPON';
const APPLY_REMOVE_WALLET = 'APPLY_REMOVE_WALLET';

const initialState = {
  byRoomTypeId: {
    '0|oak': {
      id: '0|oak',
      available: 1,
      ratePlans: {},
      memberDiscount: {},
    },
  },
  roomTypeIds: [],
  isPricesLoading: false,
  error: '',
};

// Todo: Make sure actions don't override the existing values. (Extend the state)
export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_SEARCH_PRICES: return handle(state, action, {
      start: (s) => ({
        ...s,
        byRoomTypeId: payload.byRoomTypeId,
        roomTypeIds: payload.byRoomTypeId,
        isPricesLoading: true,
      }),
      success: (s) => ({
        ...s,
        byRoomTypeId: {
          ...s.byRoomTypeId,
          ...payload.price.byRoomTypeId,
        },
        roomTypeIds: payload.price.roomTypeIds,
      }),
      failure: (s) => ({
        ...s,
        error: payload.price,
      }),
      finish: (s) => ({ ...s, isPricesLoading: false }),
    });

    case GET_ROOM_PRICES: return handle(state, action, {
      start: (s) => ({
        ...s,
        isPricesLoading: true,
      }),
      success: (s) => ({
        ...s,
        byRoomTypeId: merge({}, s.byRoomTypeId, payload.price.byRoomTypeId),
        roomTypeIds: payload.price.roomTypeIds,
      }),
      failure: (s) => ({
        ...s,
        error: payload.price,
      }),
      finish: (s) => ({ ...s, isPricesLoading: false }),
    });

    case GET_NEARBY_SEARCH_PRICES:
    case GET_CONFIRMATION_DETAILS:
    case GET_CHECKOUT_DETAILS:
    case APPLY_REMOVE_COUPON:
    case APPLY_REMOVE_WALLET: return handle(state, action, {
      success: (s) => ({
        ...s,
        byRoomTypeId: {
          ...s.byRoomTypeId,
          ...payload.price.byRoomTypeId,
        },
        roomTypeIds: Array.from(
          new Set([
            ...s.roomTypeIds,
            ...payload.price.roomTypeIds,
          ]),
        ),
      }),
    });

    default:
      return state;
  }
};

export const getSearchPrices = (hotelIds) => (dispatch, getState, { api }) => {
  const { search, ui, filter } = getState();

  const searchPricesQuery = {
    ...routeService.makeHotelIdQuery(hotelIds.filter(Number).join(',')),
    ...routeService.makeDateRangeQuery(search.datePicker.range),
    ...routeService.makeRoomConfigQuery(search.roomConfig.rooms),
    ...ui.utmParams,
    sort: filter.sort.by,
    channel: 'msite',
  };
  return dispatch({
    type: GET_SEARCH_PRICES,
    promise: api.get('/api/web/v5/pricing/hotels/', searchPricesQuery)
      .then(priceService.transformSearchPricesApi),
    payload: initialState,
    meta: {
      onSuccess: (res) => {
        const isMemberDiscountApplied = Object.values(res.price.byRoomTypeId)
          .some((price) => price.isMemberDiscountApplied);
        if (isMemberDiscountApplied) {
          analyticsService.memberDiscountApplied('Search Results Page');
        }
      },
    },
  });
};

export const getNearbySearchPrices = (hotelId, hotelIds) => (dispatch, getState, { api }) => {
  const { search, ui } = getState();
  const nearbySearchPricesQuery = {
    ...routeService.makeHotelIdQuery(hotelIds.filter((hId) => hId !== parseInt(hotelId, 0)).join(',')),
    ...routeService.makeDateRangeQuery(search.datePicker.range),
    ...routeService.makeRoomConfigQuery(search.roomConfig.rooms),
    ...ui.utmParams,
    channel: 'msite',
  };
  return dispatch({
    type: GET_NEARBY_SEARCH_PRICES,
    promise: api.get('/api/web/v5/pricing/hotels/', nearbySearchPricesQuery)
      .then(priceService.transformSearchPricesApi),
    meta: { hotelId },
  });
};

export const getRoomPrices = (hotelId) => (dispatch, getState, { api }) => {
  const { search, ui } = getState();
  const roomPricesQuery = {
    ...routeService.makeDateRangeQuery(search.datePicker.range),
    ...routeService.makeRoomConfigQuery(search.roomConfig.rooms),
    ...ui.utmParams,
    channel: 'msite',
  };
  return dispatch({
    type: GET_ROOM_PRICES,
    promise: api.get(`/api/web/v5/pricing/hotels/${hotelId}/room-prices/`, roomPricesQuery)
      .then(priceService.transformRoomPricesApi),
    payload: { hotelId },
  });
};
