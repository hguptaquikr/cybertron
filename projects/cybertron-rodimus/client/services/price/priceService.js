import isEmpty from 'lodash/isEmpty';
import * as roomTypeService from './../roomType/roomTypeService';

export default {
  transformSearchPricesApi({ data: hotels }) {
    return {
      price: Object.keys(hotels)
        .reduce((obj, hotelId) => ({
          byRoomTypeId: {
            ...obj.byRoomTypeId,
            [roomTypeService
              .makeRoomTypeId(hotelId, hotels[hotelId].cheapest_room.room_type)]: {
              couponCode: hotels[hotelId].coupon_code,
              available: hotels[hotelId].cheapest_room.availability,
              id: roomTypeService
                .makeRoomTypeId(hotelId, hotels[hotelId].cheapest_room.room_type),
              memberDiscount: {
                isApplied: hotels[hotelId].member_discount_applied,
                isAvailable: hotels[hotelId].member_discount_available,
              },
              wallets: {
                [hotels[hotelId].wallet_type]: {
                  isUsingTreeboPoints: hotels[hotelId].wallet_applied,
                  totalPoints: +hotels[hotelId].total_wallet_points,
                },
              },
              ratePlans: [hotels[hotelId].rate_plan].reduce((ratePlans, ratePlan) => ({
                ...ratePlans,
                [ratePlan.code]: {
                  code: ratePlan.code,
                  tag: ratePlan.tag,
                  type: ratePlan.refundable ? 'refundable' : 'non-refundable',
                  description: ratePlan.description,
                  basePrice: hotels[hotelId].pretax_price,
                  treeboPointsUsed: hotels[hotelId].wallet_deduction,
                  sellingPrice: hotels[hotelId].net_payable_amount,
                  strikedPrice: hotels[hotelId].rack_rate,
                  totalPrice: hotels[hotelId].sell_price,
                  tax: hotels[hotelId].tax,
                  discountPercentage: hotels[hotelId].total_discount_percent,
                },
              }), {}),
            },
          },
          roomTypeIds: [
            ...obj.roomTypeIds,
            roomTypeService.makeRoomTypeId(hotelId, hotels[hotelId].cheapest_room.room_type),
          ],
        }), {
          byRoomTypeId: {},
          roomTypeIds: [],
        }),
      hotel: Object.keys(hotels)
        .reduce((obj, hotelId) => ({
          byId: {
            ...obj.byId,
            [hotelId]: {
              id: hotelId,
              address: {},
              rank: {
                recommended: hotels[hotelId].sort_index,
              },
              roomTypeIds: [roomTypeService
                .makeRoomTypeId(hotelId, hotels[hotelId].cheapest_room.room_type)],
            },
          },
          ids: [...obj.ids, hotelId],
        }), {
          byId: {},
          ids: [],
        }),
      roomType: Object.keys(hotels)
        .reduce((obj, hotelId) => ({
          byId: {
            ...obj.byId,
            [roomTypeService
              .makeRoomTypeId(hotelId, hotels[hotelId].cheapest_room.room_type)]: {
              type: hotels[hotelId].cheapest_room.room_type,
            },
          },
          ids: [...obj.ids, roomTypeService
            .makeRoomTypeId(hotelId, hotels[hotelId].cheapest_room.room_type)],
        }), {
          byId: {},
          ids: [],
        }),
    };
  },

  transformRoomPricesApi({
    data: { total_price: prices, wallet_info: walletInfo },
    request_data: { hotel_id: hotelId },
  }) {
    return {
      wallets: {
        [walletInfo.wallet_type]: {
          isUsingTreeboPoints: walletInfo.wallet_applied,
          totalPoints: +walletInfo.total_wallet_points,
          totalBalance: +walletInfo.total_wallet_balance,
          usableBalance: +walletInfo.wallet_deductable_amount,
        },
      },
      price: Object.keys(prices)
        .reduce((obj, roomType) => ({
          byRoomTypeId: {
            ...obj.byRoomTypeId,
            [roomTypeService.makeRoomTypeId(hotelId, roomType)]: {
              available: prices[roomType].available,
              id: roomTypeService.makeRoomTypeId(hotelId, roomType),
              couponCode: prices[roomType].coupon_code,
              memberDiscount: {
                isApplied: prices[roomType].member_discount_applied,
                isAvailable: prices[roomType].member_discount_available,
              },
              ratePlans: prices[roomType].rate_plans.reduce((ratePlans, ratePlan) => ({
                ...ratePlans,
                [ratePlan.rate_plan.code]: {
                  code: ratePlan.rate_plan.code,
                  tag: ratePlan.rate_plan.tag,
                  type: ratePlan.rate_plan.refundable ? 'refundable' : 'non-refundable',
                  description: ratePlan.rate_plan.description,
                  basePrice: ratePlan.price.pretax_price,
                  treeboPointsUsed: ratePlan.price.wallet_deduction,
                  sellingPrice: ratePlan.price.net_payable_amount,
                  strikedPrice: ratePlan.price.rack_rate,
                  totalPrice: ratePlan.price.sell_price,
                  tax: ratePlan.price.tax,
                  discountPercentage: ratePlan.price.total_discount_percent,
                },
              }), {}),
            },
          },
          roomTypeIds: [...obj.roomTypeIds, roomTypeService.makeRoomTypeId(hotelId, roomType)],
        }), {
          byRoomTypeId: {},
          roomTypeIds: [],
        }),
    };
  },

  sortPrice(price) {
    return Object.values(price).sort((priceA, priceB) =>
      this.sortRatePlan(priceA.ratePlans)[0].sellingPrice -
      this.sortRatePlan(priceB.ratePlans)[0].sellingPrice);
  },

  sortRatePlan(ratePlans) {
    const ratePlanKeys = Object.values(ratePlans);
    const filteredRatePlans = ratePlanKeys.length > 1 ? (
      ratePlanKeys.filter((value) => value.basePrice)
    ) : ratePlanKeys;
    return filteredRatePlans
      .sort((ratePlanA, ratePlanB) =>
        ratePlanA.sellingPrice - ratePlanB.sellingPrice);
  },

  getPricesForRoomTypeIds(roomTypeIds, price) {
    const prices = roomTypeIds.map((roomTypeId) => price.byRoomTypeId[roomTypeId]).filter(Boolean);
    return isEmpty(prices) ? [price.byRoomTypeId['0|oak']] : prices;
  },

  getAutoAppliedCoupon(prices) {
    const priceWithCouponCode = Object.values(prices)
      .find((price) => price.couponCode);
    return priceWithCouponCode && priceWithCouponCode.couponCode;
  },

};
