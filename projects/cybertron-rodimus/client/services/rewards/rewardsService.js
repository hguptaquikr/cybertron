import qs from 'query-string';
import analyticsService from '../../services/analytics/analyticsService';
import userService from '../../services/user/userService';
import walletService from '../../services/wallet/walletService';

const alwaysShowFlows = ['Rewards Page'];

export default {
  hasViewedIntroducingTreeboRewardsPage(props) {
    if (__BROWSER__) {
      const { location, history } = props;
      const query = qs.parse(location.search);
      const currentFlow = query.flow;
      const hasViewedIntroducingTreeboRewardsPage =
          walletService.hasViewedIntroducingTreeboRewardsPage();
      if (userService.isLoggedIn()) {
        if (!hasViewedIntroducingTreeboRewardsPage) {
          return true;
        }
        if (alwaysShowFlows.includes(currentFlow)) {
          return true;
        }
      } else {
        return true;
      }
      history.replace(`/rewards/?flow=${currentFlow}`);
      analyticsService.loyaltyLandingContentViewed({
        source: currentFlow,
      });
    }
    return true;
  },
};
