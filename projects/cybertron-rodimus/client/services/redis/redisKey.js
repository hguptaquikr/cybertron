import * as abService from '../ab/abService';

export const getRenderKey = (path) => `${path}|${JSON.stringify(abService.getExperiments())}`;
