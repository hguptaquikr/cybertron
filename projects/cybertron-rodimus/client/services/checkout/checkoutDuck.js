import { handle } from 'redux-pack';
import get from 'lodash/get';
import * as abService from '../ab/abService';
import analyticsService from '../analytics/analyticsService';
import androidService from '../android/androidService';
import checkoutService from './checkoutService';
import * as routeService from '../route/routeService';

const APPLY_REMOVE_WALLET = 'APPLY_REMOVE_WALLET';
const CHECKOUT_PAY_AT_HOTEL = 'CHECKOUT_PAY_AT_HOTEL';
const CHECKOUT_PAY_NOW = 'CHECKOUT_PAY_NOW';
const CHECKOUT_CONFIRM_BOOKING = 'CHECKOUT_CONFIRM_BOOKING';
const CONTINUE_DETAILS = 'CONTINUE_DETAILS';
const GET_CHECKOUT_DETAILS = 'GET_CHECKOUT_DETAILS';
const GOTO_CHECKOUT_STEP = 'GOTO_CHECKOUT_STEP';
const IS_VERIFIED_NUMBER = 'IS_VERIFIED_NUMBER';
const SEND_CHECKOUT_OTP = 'SEND_CHECKOUT_OTP';
const UPDATE_CHECKOUT_MOBILE = 'UPDATE_CHECKOUT_MOBILE';
const VERIFY_CHECKOUT_OTP = 'VERIFY_CHECKOUT_OTP';
const GET_CONFIRMATION_DETAILS = 'GET_CONFIRMATION_DETAILS';

const initialState = {
  step: 1,
  hotelId: 0,
  isPayAtHotelEnabled: true,
  payNowEnabled: true,
  isOtpEnabled: true,
  isOtpVerified: false,
  isVerifyingOtp: false,
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case APPLY_REMOVE_WALLET: return handle(state, action, {
      start: (s) => ({ ...s, isLoading: true }),
      success: (s) => ({ ...s, isLoading: false }),
    });

    case GET_CHECKOUT_DETAILS: return handle(state, action, {
      start: (s) => ({ ...s, isLoading: true }),
      success: (s) => ({ ...s, ...payload.checkout, isLoading: false }),
    });

    case GOTO_CHECKOUT_STEP:
      return {
        ...state,
        ...payload,
      };

    case IS_VERIFIED_NUMBER: return handle(state, action, {
      success: (s) => ({
        ...s,
        ...payload,
      }),
    });

    case VERIFY_CHECKOUT_OTP: return handle(state, action, {
      start: (s) => ({
        ...s,
        isVerifyingOtp: true,
      }),
      success: (s) => ({
        ...s,
        isVerifyingOtp: false,
      }),
      failure: (s) => ({
        ...s,
        isVerifyingOtp: false,
      }),
    });

    case CONTINUE_DETAILS:
      return {
        ...state,
        step: payload.step,
      };

    case GET_CONFIRMATION_DETAILS: return handle(state, action, {
      start: ((s) => ({
        ...s,
        step: 1,
      })),
    });

    default:
      return state;
  }
};

export const sendCheckoutOtp = (mobile) => (dispatch, getState, { api }) =>
  dispatch({
    type: SEND_CHECKOUT_OTP,
    promise: api.post('/api/web/v2/otp/', { phone_number: mobile }),
  });

export const isVerifiedNumber = (mobile) => (dispatch, getState, { api }) =>
  dispatch({
    type: IS_VERIFIED_NUMBER,
    promise: api.post('/api/web/v2/otp/is-verified/', { phone_number: mobile })
      .then(checkoutService.transformIsNumberVerifiedApi),
  });

export const updateCheckoutMobile = (mobile) => (dispatch) => {
  dispatch({
    type: UPDATE_CHECKOUT_MOBILE,
    payload: { mobile },
  });
  return dispatch(sendCheckoutOtp(mobile));
};

export const verifyCheckoutOtp = (mobile, otp) => (dispatch, getState, { api }) => {
  const verificationData = {
    phone_number: mobile,
    verification_code: otp,
  };
  return dispatch({
    type: VERIFY_CHECKOUT_OTP,
    promise: api.post('/api/web/v2/otp/verify/', verificationData),
  });
};

export const getCheckoutDetails = (bid) => (dispatch, getState, { api }) => {
  const {
    search: { searchInput, datePicker, roomConfig },
    coupon: { appliedCouponCode },
    ui,
  } = getState();
  const itineraryQuery = {
    bid,
    ...routeService.makeHotelIdQuery(searchInput.place.hotelId),
    ...routeService.makeDateRangeQuery(datePicker.range),
    ...routeService.makeRoomConfigQuery(roomConfig.rooms),
    ...routeService.makeRoomTypeQuery(roomConfig.roomType),
    ...routeService.makeRatePlanQuery(roomConfig.ratePlan),
    ...ui.utmParams,
    channel: androidService.isAndroid ? 'app' : 'msite',
    couponcode: appliedCouponCode,
    apply_wallet: true,
  };

  return dispatch({
    type: GET_CHECKOUT_DETAILS,
    promise: api.get('/api/web/v6/checkout/itinerary/', itineraryQuery)
    // FIXME: get the api to return the number of nights
      .then(checkoutService.transformCheckoutApi),
  });
};

export const goToStep = (step) => ({
  type: GOTO_CHECKOUT_STEP,
  payload: {
    step,
  },
});

export const continueReview = (couponcode, roomType, price, hotel) => (dispatch, getState) => {
  const { wallet } = getState();
  const { isUsingTreeboPoints, usableBalance } = wallet.byType.TP || wallet.byType[''];
  dispatch(goToStep(2));
  analyticsService.checkoutStep('Review itinerary', {
    couponcode,
    room_type: roomType.type,
    base_price: price.basePrice,
    discount_amount: price.discountAmount,
    tax: Math.round(price.tax),
    total_price: Math.round(price.totalPrice),
    is_using_treebo_points: isUsingTreeboPoints,
    price: Math.round(price.sellingPrice),
    treebo_points: usableBalance,
    ...hotel,
    ...abService.actionProperties(['checkoutCtaMobile']),
  });
};

export const continueDetails = ({
  details,
  isGSTFieldsVisible,
  bypassPaymentStep,
}) => (dispatch) => {
  dispatch({
    type: CONTINUE_DETAILS,
    payload: {
      step: bypassPaymentStep ? 2 : 3,
      details: { ...details },
      isGSTFieldsVisible,
    },
  });
  if (androidService.isAndroid) {
    // send checkout details to webengage and appflyers
    androidService.track('Traveller Info Added', details);
  }
  analyticsService.checkoutStep('Traveller Info Added', {
    ...details,
    ...abService.actionProperties(['checkoutDetailsCtaMobile']),
  });
};

export const buildAnalyticsData = ({ booking, hotel, ratePlan }) => ({
  hotelId: booking.hotelId,
  hotelName: hotel.byId[booking.hotelId].name,
  ratePlan: ratePlan.code,
  price: Math.round(ratePlan.sellingPrice),
  totalPrice: Math.round(ratePlan.totalPrice),
});

export const addTravellerInfo = ({
  details,
  isGSTFieldsVisible,
  bypassPaymentStep,
  hotelId,
  hotelName,
}) => (dispatch) => {
  dispatch({
    type: CONTINUE_DETAILS,
    payload: {
      details: { ...details },
      bypassPaymentStep,
      isGSTFieldsVisible,
    },
  });

  analyticsService.addTravellerInfo({
    ...details,
    ...{ hotelId },
    ...{ hotelName },
    ...abService.actionProperties(['checkoutDetailsCtaMobile']),
  });
};

export const initiateBooking = (ratePlan, gateway) => (dispatch, getState, { api }) => {
  const { booking, hotel } = getState();
  const payData = checkoutService.buildPayData({ booking, ratePlan, gateway });
  const analyticsProperties = {
    ...buildAnalyticsData({ booking, hotel, ratePlan, gateway }),
    mode: 'pre-paid',
  };
  return dispatch({
    type: CHECKOUT_PAY_NOW,
    promise: api.post('/api/web/v5/checkout/paynow/', payData)
      .then((res) => {
        if (gateway === 'razorpay') {
          return checkoutService.transformPayApi(res, false);
        } return res;
      }),
    meta: {
      onSuccess: () => analyticsService.checkoutStep('Payment Initiated', analyticsProperties),
    },
  });
};

export const payAtHotel = (ratePlan) => (dispatch, getState, { api }) => {
  const { booking, wallet, hotel } = getState();
  const payData = checkoutService.buildPayData({ booking, ratePlan, wallet });
  const mode = 'pay-at-hotel';
  const bookingData = checkoutService.buildConfirmData({ booking, ratePlan, wallet, mode });
  const analyticsData = {
    ...buildAnalyticsData({ booking, hotel, ratePlan }),
    mode,
  };
  analyticsService.checkoutStep('Payment Initiated', analyticsData);

  return dispatch({
    type: CHECKOUT_PAY_AT_HOTEL,
    promise: api.post('/api/web/v5/checkout/payathotel/', payData)
      .then((res) => checkoutService.transformPayApi(res, true)),
    meta: {
      onSuccess: (res) => {
        bookingData.orderId = bookingData.order_id || res.orderEventArgs.orderId;
        const analyticsProperties = {
          orderId: bookingData.orderId,
          pgOrderId: bookingData.pg_order_id,
          psOrderId: bookingData.ps_order_id,
          mode: bookingData.mode,
          price: bookingData.amount_paid,
          pgPaymentId: get(bookingData, 'pg_meta.pg_payment_id'),
          gateway: bookingData.gateway,
        };
        if (androidService.isAndroid) {
          // send checkout details to webengage and appflyers
          androidService.track('Payment Confirmed', {
            ...analyticsProperties,
            ratePlanType: 'refundable',
          });
        }
        analyticsService.checkoutStep('Payment Confirmed', {
          ...analyticsProperties,
          ratePlanType: 'refundable',
        });
        analyticsService.exposeGlobally({ confirm_order_analytics: res.orderEventArgs });
        analyticsService.adwordsCompletedOrder(res.orderEventArgs, 'refundable');
      },
    },
  });
};

export const confirmBooking = (razorpayId, razorpaySignature, selectedRatePlan, gateway) =>
  (dispatch, getState, { api }) => {
    const { booking, price } = getState();
    const { type: ratePlanType, code: ratePlan } = selectedRatePlan;
    const mode = 'pre-paid';
    const bookingData = checkoutService.buildConfirmData({
      booking,
      price,
      ratePlan,
      razorpayId,
      razorpaySignature,
      gateway,
      mode,
    });
    const analyticsProperties = {
      orderId: bookingData.orderId,
      pgOrderId: bookingData.pg_order_id,
      psOrderId: bookingData.ps_order_id,
      mode: bookingData.mode,
      price: bookingData.amount_paid,
      pgPaymentId: get(bookingData, 'pg_meta.pg_payment_id'),
      gateway: bookingData.gateway,
    };
    analyticsService.checkoutStep('Payment Confirmed', {
      ...analyticsProperties,
      ratePlanType,
    });

    return dispatch({
      type: CHECKOUT_CONFIRM_BOOKING,
      promise: api.post('/api/web/v5/checkout/confirmbooking/', bookingData)
        .then(checkoutService.transformPayApi),
      meta: {
        onSuccess: (res) => {
          analyticsService.exposeGlobally({ confirm_order_analytics: res.orderEventArgs });
          analyticsService.adwordsCompletedOrder(res.orderEventArgs, selectedRatePlan.type);
        },
      },
    });
  };
