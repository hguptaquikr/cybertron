import moment from 'moment';
import get from 'lodash/get';
import qs from 'query-string';
import searchService from '../search/searchService';
import * as roomTypeService from './../roomType/roomTypeService';
import { transformPolicies } from './../hotel/hotelService';
import * as routeService from '../../services/route/routeService';

export default {
  transformCheckoutApi({ data }) {
    const { hotel, room, price, date } = data;
    // Fixme: Need to remove all the defaults from the hotel object
    return {
      hotel: {
        byId: {
          [hotel.id]: {
            id: hotel.id,
            isActive: true,
            name: hotel.name,
            address: {
              city: hotel.city,
              cityId: hotel.city_id,
              locality: hotel.locality,
              pincode: hotel.pincode,
              state: hotel.state,
              street: hotel.street,
            },
            policies: transformPolicies(hotel.hotel_policies),
            images: [{ url: hotel.image_url }],
            coordinates: hotel.coordinates || {},
            amenities: hotel.amenities || [],
          },
        },
        ids: [hotel.id],
      },

      booking: {
        bid: data.bid,
        room: { ...room, type: room.type.toLowerCase() },
        roomTypeId: roomTypeService.makeRoomTypeId(hotel.id, room.type.toLowerCase()),
        hotelId: hotel.id,
        dates: searchService.formattedDatePicker({
          start: moment(date.checkin),
          end: moment(date.checkout),
        }),
        isPrepaidOnly: 'is_prepaid' in data ? data.is_prepaid : false,
        payment: { orderId: data.order_id },
      },
      wallets: {
        [data.price.wallet_info.wallet_type]: {
          isUsingTreeboPoints: data.price.wallet_info.wallet_applied,
          totalBalance: +data.price.wallet_info.total_wallet_balance,
          totalPoints: +data.price.wallet_info.total_wallet_points,
          usableBalance: +data.price.wallet_info.wallet_deductable_amount,
        },
      },
      roomType: {
        byId: {
          [roomTypeService.makeRoomTypeId(hotel.id, room.type.toLowerCase())]: {
            type: room.type,
          },
        },
        ids: [roomTypeService.makeRoomTypeId(hotel.id, room.type.toLowerCase())],
      },
      price: {
        byRoomTypeId: {
          [roomTypeService.makeRoomTypeId(hotel.id, room.type.toLowerCase())]: {
            available: 1,
            id: roomTypeService.makeRoomTypeId(hotel.id, room.type.toLowerCase()),
            ratePlans: price.all_rate_plans.reduce((roomObj, ratePlan) => ({
              ...roomObj,
              [ratePlan.rate_plan.code]: {
                code: ratePlan.rate_plan.code,
                description: ratePlan.rate_plan.code,
                type: ratePlan.rate_plan.refundable ? 'refundable' : 'non-refundable',
                tag: ratePlan.rate_plan.tag,
                totalPrice: ratePlan.price.sell_price,
                treeboPointsUsed: ratePlan.price.wallet_deduction,
                sellingPrice: ratePlan.price.net_payable_amount,
                strikedPrice: ratePlan.price.rack_rate,
                basePrice: ratePlan.price.pretax_price,
                tax: ratePlan.price.tax,
                discountPercentage: ratePlan.price.total_discount_percent,
                discountAmount: ratePlan.price.coupon_discount,
              },
            }), {}),
            memberDiscount: {
              value: price.selected_rate_plan.price.member_discount,
              isApplied: price.member_discount_applied,
              isAvailable: price.member_discount_available,
            },
          },
        },
        roomTypeIds: [roomTypeService.makeRoomTypeId(hotel.id, room.type.toLowerCase())],
      },
      checkout: {
        hotelId: hotel.id,
        isPayAtHotelEnabled: 'pah_enabled' in data ? data.pah_enabled : true,
        payNowEnabled: 'paynow_enabled' in data ? data.paynow_enabled : true,
        isOtpEnabled: data.isOtpEnabled,
      },
      coupon: {
        results: {
          [data.coupon_code]: {
            description: data.coupon_desc,
          },
        },
        appliedCouponCode: data.coupon_code || '',
      },
    };
  },

  transformPayApi({ data }, isPayAtHotel) {
    return {
      orderId: data.order_id,
      orderEventArgs: data.order_event_args,
      gatewayOrderId: data.pg_order_id,
      serviceOrderId: data.ps_order_id,
      bid: data.bid,
      isPayAtHotel,
    };
  },

  buildPayData({
    booking: { bid, guest },
    ratePlan,
    gateway,
  }) {
    // TODO: Check for the guest obj returned in the confirmation page api
    return {
      gateway,
      name: guest.name,
      mobile: guest.mobile,
      email: guest.email,
      organization_name: guest.organizationName,
      organization_address: guest.organizationAddress,
      organization_taxcode: guest.organizationTaxcode,
      bid,
      check_in_time: '12:00',
      check_out_time: '11:00',
      message: '',
      actual_total_cost: ratePlan.sellingPrice,
    };
  },

  buildConfirmData({
    ratePlan,
    razorpayId,
    razorpaySignature,
    booking: { payment },
    gateway,
    mode,
  }) {
    return {
      order_id: payment.orderId,
      ps_order_id: payment.serviceOrderId,
      pg_order_id: payment.gatewayOrderId,
      pg_meta: {
        pg_payment_id: razorpayId,
        pg_signature: razorpaySignature,
      },
      amount_paid: ratePlan.sellingPrice,
      mode,
      gateway,
    };
  },

  makeCheckoutLocation(search, roomType, hotel, ratePlan, singlePageHd = 0, bid = '') {
    const { searchInput, datePicker, roomConfig } = search;
    return {
      pathname: '/itinerary/',
      search: qs.stringify({
        q: hotel && hotel.address ? `${hotel.name}, ${hotel.address.locality}` : searchInput.place.q,
        landmark: get(searchInput, 'place.area.landmark'),
        locality: get(searchInput, 'place.area.locality'),
        city: get(searchInput, 'place.area.city'),
        state: get(searchInput, 'place.area.state'),
        step: 3,
        singlePageHd,
        bid,
        ...routeService.makeCoordinatesQuery(searchInput.place.coordinates),
        ...routeService.makeHotelIdQuery((hotel && hotel.id) || searchInput.place.hotelId),
        ...routeService.makeDateRangeQuery(datePicker.range),
        ...routeService.makeRoomConfigQuery(roomConfig.rooms),
        ...routeService.makeRoomTypeQuery(roomType.type.toLowerCase()),
        ...routeService.makeRatePlanQuery(ratePlan.code),
      }),
    };
  },

  transformIsNumberVerifiedApi({ data }) {
    return {
      isOtpVerified: data.is_verified,
    };
  },
};
