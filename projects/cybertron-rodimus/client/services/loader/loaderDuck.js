const SHOW_LOADER = 'SHOW_LOADER';
const HIDE_LOADER = 'HIDE_LOADER';

const initialState = {
  visible: false,
};

export default (state = initialState, action) => {
  const { type } = action;

  switch (type) {
    case SHOW_LOADER:
      return {
        ...state,
        visible: true,
      };

    case HIDE_LOADER:
      return {
        ...state,
        visible: false,
      };

    default:
      return state;
  }
};

export const showLoader = () => ({
  type: SHOW_LOADER,
});

export const hideLoader = () => ({
  type: HIDE_LOADER,
});
