import * as authService from '../auth/authService';

export default {
  isAndroid: !!(__BROWSER__ && window.__ANDROID_INTERFACE__),

  init() {
    this.addDOMContentLoadedListener();
  },

  getVersion() {
    return this.isAndroid && window.__ANDROID_INTERFACE__.getAppVersion
      ? window.__ANDROID_INTERFACE__.getAppVersion() : undefined;
  },

  addDOMContentLoadedListener() {
    if (this.isAndroid) {
      document.addEventListener(
        'DOMContentLoaded',
        () => window.__ANDROID_INTERFACE__.DOMContentLoaded(),
      );
    }
  },

  addOtpListener(otpComponent) {
    if (this.isAndroid) {
      window.__ANDROID_INTERFACE__.listenForOtp();
      window.__ANDROID_INTERFACE__.passOtp = (androidPassedOtp) => {
        otpComponent.setState({ androidPassedOtp });
      };
    }
  },

  makePayment(paymentData, bookingPromise) {
    window.__ANDROID_INTERFACE__.initiatePayment(JSON.stringify(paymentData));
    const authToken = authService.getToken();
    if (authToken && window.__ANDROID_INTERFACE__.sendAuthToken) {
      window.__ANDROID_INTERFACE__.sendAuthToken(authToken);
    }
    bookingPromise.then((bookingData) => {
      window.__ANDROID_INTERFACE__.makePayment(JSON.stringify(bookingData));
    }, () => {
      window.__ANDROID_INTERFACE__.initiatePaymentFailed();
    });
  },

  track(eventName, eventData) {
    if (this.isAndroid && window.__ANDROID_INTERFACE__.logEvent) {
      window.__ANDROID_INTERFACE__.logEvent(eventName, JSON.stringify(eventData));
    }
  },
};
