const SHOW_TOAST = 'SHOW_TOAST';

const initialState = {
  label: '',
  timeout: 2000,
  shape: 'capsular',
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SHOW_TOAST:
      return {
        ...state,
        ...payload,
      };

    default:
      return state;
  }
};

export const showToast = (label, shape) => ({
  type: SHOW_TOAST,
  payload: { label, shape },
});
