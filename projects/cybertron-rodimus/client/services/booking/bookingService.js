import moment from 'moment';
import searchService from '../search/searchService';
import * as roomTypeService from './../roomType/roomTypeService';
import { transformPolicies } from './../hotel/hotelService';

export default {
  transformConfirmationApi({ data }) {
    // Fixme: Need to remove all the defaults from the hotel object
    return {
      booking: {
        guest: {
          name: data.user.name,
          mobile: data.user.contact_number,
          email: data.user.email,
        },
        room: {
          ...data.room,
          room: data.room.type.toLowerCase(),
        },
        dates: {
          ...searchService.formattedDatePicker({
            start: moment(data.date.checkin),
            end: moment(data.date.checkout),
          }),
          checkInDate: data.date.checkin,
          checkOutDate: data.date.checkout,
        },
        payment: {
          orderId: data.order_id,
          isPayAtHotel: data.pay_at_hotel,
          partialPaymentAmount: data.partial_payment_amount,
          partialPaymentLink: data.partial_payment_link,
        },
        roomTypeId: roomTypeService
          .makeRoomTypeId(data.hotel.id, data.room.type.toLowerCase()),
        ratePlanCode: data.price.selected_rate_plan.rate_plan.code,
        hotelId: data.hotel.id,
        isGSTFieldsVisible: false,
      },
      hotel: {
        byId: {
          [data.hotel.id]: {
            id: data.hotel.id,
            name: data.hotel.name,
            coordinates: data.hotel.coordinates,
            address: {
              locality: data.hotel.locality,
              street: data.hotel.street,
              city: data.hotel.city,
              pincode: data.hotel.pincode,
            },
            policies: transformPolicies(data.hotel.hotel_policies),
          },
        },
        ids: [data.hotel.id],
      },
      price: {
        byRoomTypeId: {
          [roomTypeService.makeRoomTypeId(data.hotel.id, data.room.type.toLowerCase())]: {
            id: roomTypeService.makeRoomTypeId(data.hotel.id, data.room.type.toLowerCase()),
            available: 1,
            ratePlans: {
              [data.price.selected_rate_plan.rate_plan.code]: {
                code: data.price.selected_rate_plan.rate_plan.code,
                description: data.price.selected_rate_plan.rate_plan.description,
                tag: data.price.selected_rate_plan.code === 'NRP' ? 'Non Refundable' : 'Refundable',
                type: data.price.selected_rate_plan.rate_plan.code === 'NRP' ? 'non-refundable' : 'refundable',
                walletDiscount: data.price.selected_rate_plan.price.wallet_discount,
                walletDiscountApplied: data.price.selected_rate_plan.price.wallet_applied,
                sellingPrice: data.price.selected_rate_plan.price.net_payable_amount,
                basePrice: data.price.selected_rate_plan.price.pretax_price,
                totalPrice: data.price.selected_rate_plan.price.sell_price,
                tax: data.price.selected_rate_plan.price.tax,
                discountAmount: data.price.selected_rate_plan.price.coupon_discount,
              },
            },
            memberDiscount: {
              value: data.price.selected_rate_plan.price.member_discount,
              isApplied: data.price.member_discount_applied,
              isAvailable: data.price.member_discount_available,
            },
            selectedRatePlan: data.price.selected_rate_plan.rate_plan.code,
          },
        },
        roomTypeIds: [roomTypeService
          .makeRoomTypeId(data.hotel.id, data.room.type.toLowerCase()),
        ],
      },
    };
  },
};
