import { handle } from 'redux-pack';
import bookingService from './bookingService';

const APPLY_REMOVE_COUPON = 'APPLY_REMOVE_COUPON';
const CHECKOUT_PAY_AT_HOTEL = 'CHECKOUT_PAY_AT_HOTEL';
const CHECKOUT_PAY_NOW = 'CHECKOUT_PAY_NOW';
const CONTINUE_DETAILS = 'CONTINUE_DETAILS';
const GET_CHECKOUT_DETAILS = 'GET_CHECKOUT_DETAILS';
const GET_CONFIRMATION_DETAILS = 'GET_CONFIRMATION_DETAILS';
const GET_HOTEL_DETAILS = 'GET_HOTEL_DETAILS';
const UPDATE_CHECKOUT_MOBILE = 'UPDATE_CHECKOUT_MOBILE';
const GET_PAYMENT_CONFIRMATION_DETAILS = 'GET_PAYMENT_CONFIRMATION_DETAILS';

const initialState = {
  guest: {
    name: '',
    mobile: '',
    email: '',
    organizationName: '',
    organizationAddress: '',
    organizationTaxcode: '',
  },
  room: {
    type: 'oak',
  },
  isPrepaidOnly: false,
  isGSTFieldsVisible: false,
  dates: {
    numberOfNights: {},
  },
  roomTypeId: '0|oak',
  payment: {
    orderId: '',
    isPayAtHotel: '',
    partialPaymentAmount: '',
    partialPaymentLink: '',
    serviceOrderId: '',
    gatewayOrderId: '',
    gateway: '',
  },
  hotelId: 0,
  attempts: 0,
  maxAttempts: 10,
  isError: false,
  paymentDetails: {
    status: '',
  },
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_HOTEL_DETAILS: return handle(state, action, {
      start: (s) => ({ ...s, ...payload }),
    });

    case GET_CONFIRMATION_DETAILS: return handle(state, action, {
      success: (s) => ({
        ...s,
        ...payload.booking,
      }),
    });

    case GET_PAYMENT_CONFIRMATION_DETAILS: return handle(state, action, {
      success: (s) => ({
        ...s,
        ...payload,
        paymentDetails: payload.data,
        attempts: state.attempts + 1,
      }),
      failure: (s) => ({
        ...s,
        isError: true,
      }),
    });

    case GET_CHECKOUT_DETAILS: return handle(state, action, {
      success: (s) => ({
        ...s,
        ...payload.booking,
      }),
    });

    case CONTINUE_DETAILS:
      return {
        ...state,
        guest: {
          ...state.guest,
          ...payload.details,
        },
        isGSTFieldsVisible: payload.isGSTFieldsVisible,
      };

    case CHECKOUT_PAY_AT_HOTEL: return handle(state, action, {
      success: (s) => ({
        ...s,
        payment: {
          ...s.payment,
          ...payload,
        },
      }),
    });

    case CHECKOUT_PAY_NOW: return handle(state, action, {
      success: (s) => ({
        ...s,
        payment: {
          ...s.payment,
          ...payload,
        },
      }),
    });

    case APPLY_REMOVE_COUPON: return handle(state, action, {
      success: (s) => ({ ...s, ...payload.booking }),
    });

    case UPDATE_CHECKOUT_MOBILE:
      return {
        ...state,
        guest: {
          ...state.guest,
          ...payload,
        },
      };

    default:
      return state;
  }
};

export const getBookingConfirmationDetails = (bookingId) =>
  (dispatch, getState, { api }) => dispatch({
    type: GET_CONFIRMATION_DETAILS,
    promise: api.get(`/api/web/v5/checkout/confirmed-booking/${bookingId}/`)
      .then(bookingService.transformConfirmationApi),
  });

export const getPaymentConfirmationDetails = (bookingId) => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_PAYMENT_CONFIRMATION_DETAILS,
    promise: api.get(`/api/web/v5/checkout/payment/status/${bookingId}/`),
  });
