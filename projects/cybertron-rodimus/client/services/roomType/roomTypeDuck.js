import { handle } from 'redux-pack';
import merge from 'lodash/merge';

const GET_HOTEL_DETAILS = 'GET_HOTEL_DETAILS';
const GET_SEARCH_PRICES = 'GET_SEARCH_PRICES';
const GET_NEARBY_SEARCH_PRICES = 'GET_NEARBY_SEARCH_PRICES';
const GET_CHECKOUT_DETAILS = 'GET_CHECKOUT_DETAILS';

const initialState = {
  byId: {
    '0|oak': {
      type: 'oak',
      maxOccupancy: {},
    },
  },
  ids: [],
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_SEARCH_PRICES:
    case GET_NEARBY_SEARCH_PRICES:
    case GET_HOTEL_DETAILS:
    case GET_CHECKOUT_DETAILS: return handle(state, action, {
      success: (s) => ({
        byId: merge(
          {},
          s.byId,
          payload.roomType.byId,
        ),
        ids: Array.from(
          new Set([
            ...s.ids,
            ...payload.roomType.ids,
          ]),
        ),
      }),
    });

    default:
      return state;
  }
};
