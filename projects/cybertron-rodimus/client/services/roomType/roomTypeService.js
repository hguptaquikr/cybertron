export const makeRoomTypeId = (hotelId, roomType) => `${hotelId}|${roomType}`;

export const makeRoomTypeImages = (images) => (
  images.reduce((RoomTypeImages, image) => ({
    ...RoomTypeImages,
    [image.roomType]: [
      { url: image.url, tagline: image.tagline },
      ...(RoomTypeImages[image.roomType] || []),
    ],
  }))
);
