export const pluralize = (value = 0, text = '', suffix = 's') => (
  +value > 1 ? `${text}${suffix}` : text
);

export const performanceMark = (name) => {
  if (window.performance && window.performance.mark) {
    window.performance.mark(name);
  }
};

export const getPerformanceDuration = (name, start, end) => {
  try {
    if (
      window.performance
        && window.performance.mark
        && window.performance.measure
    ) {
      window.performance.measure(name, start, end);
      const perf = window.performance.getEntriesByName(name);
      window.performance.clearMarks(start);
      window.performance.clearMarks(end);
      window.performance.clearMeasures(name);
      return perf[0].duration;
    }
    return false;
  } catch (e) {
    return false;
  }
};

// returns an array of keys, removing the keys '0', ''
export const safeKeys = (ob = {}) =>
  Object.keys(ob).filter((v) => v.length && v !== '0');

// returns a new object, removing the keysToOmit
export const omitKeys = (ob = {}, keysToOmit = []) =>
  Object.keys(ob).reduce((obj, key) => (
    keysToOmit.includes(key) ? obj : {
      ...obj,
      [key]: ob[key],
    }
  ), {});

export const browserDimensions = () => ({
  width: __BROWSER__ && (
    window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth
  ),
  height: __BROWSER__ && (
    window.innerHeight
    || document.documentElement.clientHeight
    || document.body.clientHeight
  ),
});

export const getMapsUrl = (lat, lng) =>
  `https://www.google.com/maps/search/?api=1&query=${lat},${lng}`;

export const constants = {
  customerCareNumber: '9322800100',
  dateFormat: { query: 'YYYY-MM-DD', view: 'ddd, D MMM' },
  treeboTalkiesUrl: 'http://talkies.treebo.com',
};

export const scrollIntoView = (DOMNode, offset = 0) => {
  const viewPortYPos = DOMNode.getBoundingClientRect().top;
  window.scrollTo(0, viewPortYPos + window.scrollY + offset);
};

export const makeRoomTypeId = (hotelId, roomType) => `${hotelId}|${roomType}`;

export const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const mobileRegex = /^[0]?[789]\d{9}$/;

export const otpRegex = /\d{6}/;

export const gstnRegex = /^[\d]{2}[A-Z]{5}[\d]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/;

export const isIos = __BROWSER__ && !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
