import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import moment from 'moment';
import * as contentActionCreators from '../../services/content/contentDuck';
import { Text } from '../Typography/Typography';
import './newYearRateTag.css';

class NewYearRateTag extends Component {
  componentDidMount() {
    const { contentActions } = this.props;
    contentActions.getNewYearRateTag();
  }
  render() {
    const { newYearRateTag = [], range } = this.props;
    const texts = newYearRateTag.filter((value) => {
      const startingDate = moment(value.startDate, 'DD/MM/YYYY');
      const endingDate = moment(value.endDate, 'DD/MM/YYYY');
      const isBetween = startingDate.isBetween(range.start, range.end, 'days', [])
        && endingDate.isBetween(range.start, range.end, 'days', []);
      return isBetween;
    }).map((value) => value.text);
    return (
      !isEmpty(texts) ? (
        <div className="nyrt">
          <div>
            {
              texts.map((text) => (
                <Text type="2" className="nyrt__txt">{text}</Text>
              ))
            }
          </div>
        </div>
      ) : null
    );
  }
}

NewYearRateTag.propTypes = {
  contentActions: PropTypes.object.isRequired,
  newYearRateTag: PropTypes.array,
  range: PropTypes.object.isRequired,
};

const mapStateToProps = (state, { hotelId, range }) => ({
  newYearRateTag: state.content.newYearRateTag[hotelId],
  range: range || state.search.datePicker.range,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewYearRateTag);
