import React, { Component, Children } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import cx from 'classnames';
import './ripple.css';

class Ripple extends Component {
  componentDidMount() {
    this.shouldDelay = !navigator.userAgent.includes('UCBrowser');
  }

  getAugmentedProps = (childElem) => {
    const { to, onClick, className } = childElem.props;
    return {
      onClick: (e) => this.handleClick(e, to, onClick),
      className: cx('ripple', className),
    };
  };

  handleClick = (e, to, onClick) => {
    const { eagerLoad, history } = this.props;
    e.preventDefault();
    if (this.shouldDelay) {
      if (eagerLoad) eagerLoad(e);
      setTimeout(() => {
        if (to) history.push(to);
        if (onClick) onClick(e);
      }, 400);
    } else {
      if (to) history.push(to);
      if (onClick) onClick(e);
    }
  }

  render() {
    const { children } = this.props;
    const childElem = Children.only(children);
    return React.cloneElement(childElem, this.getAugmentedProps(childElem));
  }
}

Ripple.propTypes = {
  children: PropTypes.node.isRequired,
  eagerLoad: PropTypes.func,
  history: PropTypes.object,
};

export default withRouter(Ripple);
