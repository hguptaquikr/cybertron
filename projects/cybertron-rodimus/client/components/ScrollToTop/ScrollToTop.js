import { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import analyticsService from '../../services/analytics/analyticsService';

class ScrollToTop extends Component {
  componentDidMount() {
    analyticsService.pageTransition();
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      window.scrollTo(0, 0);
      analyticsService.pageTransition();
    }
  }

  render() {
    return this.props.children;
  }
}

ScrollToTop.propTypes = {
  location: PropTypes.object.isRequired,
  children: PropTypes.element,
};

export default withRouter(ScrollToTop);
