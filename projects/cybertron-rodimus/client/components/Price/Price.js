import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './price.css';

const formatPrice = (price, round) =>
  (round ? Math.round(price) : price).toLocaleString('en-IN');

const Price = ({ price, type, round, size, bold, previewStyle, className }) => {
  const isValidPrice = price !== undefined || price;

  return (
    <div
      className={cx(`price price__size--${size} price__type--${type}`, {
        'price--bold': bold,
        'price--preview': !isValidPrice,
      }, className)}
      style={!isValidPrice ? previewStyle : {}}
    >
      {type === 'debit' ? '-' : ''}
      {type === 'credit' ? '+' : ''}
      <span className="price__rs" id="t-price-rs">&#x20b9;</span>
      <span className="price__val" id="t-price-val">
        {isValidPrice ? formatPrice(price, round) : ''}
      </span>
    </div>
  );
};

Price.propTypes = {
  price: PropTypes.number,
  round: PropTypes.bool,
  bold: PropTypes.bool,
  size: PropTypes.oneOf(['small', 'regular', 'medium', 'large', 'x-large']),
  type: PropTypes.oneOf(['default', 'regular', 'striked', 'discount', 'debit', 'credit']),
  previewStyle: PropTypes.object,
  className: PropTypes.string,
};

Price.defaultProps = {
  size: 'regular',
  type: 'regular',
  round: true,
};

export default Price;
