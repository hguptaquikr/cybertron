import React from 'react';
import PropTypes from 'prop-types';
import Space from 'leaf-ui/cjs/Space/web';
import Text from 'leaf-ui/cjs/Text/web';

const RatePlanLabel = ({ type, tag }) => (
  <Space padding={[0.5, 1, 0.25, 0]} margin={[0, 0, 1, 0]}>
    <Text
      tag="span"
      color={type === 'refundable' ? 'green' : 'yellow'}
    >
      {tag}
    </Text>
  </Space>
);

RatePlanLabel.propTypes = {
  tag: PropTypes.string,
  type: PropTypes.string,
};

export default RatePlanLabel;
