import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Heading = ({ className, tag, type, preview, previewStyle, children, ...props }) =>
  React.createElement(tag, {
    style: preview ? previewStyle : {},
    className: cx({
      [`heading-${type}`]: type,
      [`heading-${type}--preview`]: preview,
    }, className),
    ...props,
  }, children);

Heading.propTypes = {
  className: PropTypes.string,
  tag: PropTypes.string,
  type: PropTypes.string,
  preview: PropTypes.bool,
  previewStyle: PropTypes.object,
  children: PropTypes.node,
};

Heading.defaultProps = {
  tag: 'h1',
  type: '1',
  preview: false,
};

export default Heading;
