import './typography.css';

export Heading from './Heading';
export Subheading from './Subheading';
export Text from './Text';
