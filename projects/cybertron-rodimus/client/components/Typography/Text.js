import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Text = ({ className, tag, type, modifier, preview, previewStyle, children, ...props }) =>
  React.createElement(tag, {
    style: preview ? previewStyle : {},
    className: cx({
      [`text-${type}`]: type,
      [`text--${modifier}`]: modifier,
      [`text-${type}--preview`]: preview,
    }, className),
    ...props,
  }, children);

Text.propTypes = {
  className: PropTypes.string,
  tag: PropTypes.string,
  type: PropTypes.string,
  modifier: PropTypes.string,
  preview: PropTypes.bool,
  previewStyle: PropTypes.object,
  children: PropTypes.node,
};

Text.defaultProps = {
  tag: 'p',
  type: '1',
  preview: false,
};

export default Text;
