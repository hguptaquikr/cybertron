import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Subheading = ({ className, tag, type, preview, previewStyle, children, ...props }) =>
  React.createElement(tag, {
    style: preview ? previewStyle : {},
    className: cx({
      [`subheading-${type}`]: type,
      [`subheading-${type}--preview`]: preview,
    }, className),
    ...props,
  }, children);

Subheading.propTypes = {
  className: PropTypes.string,
  tag: PropTypes.string,
  type: PropTypes.string,
  preview: PropTypes.bool,
  previewStyle: PropTypes.object,
  children: PropTypes.node,
};

Subheading.defaultProps = {
  tag: 'h2',
  type: '1',
  preview: false,
};

export default Subheading;
