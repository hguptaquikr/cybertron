import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import Button from 'leaf-ui/cjs/Button/web';
import * as abService from '../../services/ab/abService';

const ABButton = ({ experiment, render, ...props }) => {
  const text = get(abService.getExperiments(), `${experiment}.payload.text`);
  const abProps = get(abService.getExperiments(), `${experiment}.payload.style`);
  return (
    <Button
      {...props}
      {...abProps}
    >
      {render(text)}
    </Button>
  );
};

ABButton.propTypes = {
  experiment: PropTypes.string.isRequired,
  render: PropTypes.func.isRequired,
};

export default ABButton;
