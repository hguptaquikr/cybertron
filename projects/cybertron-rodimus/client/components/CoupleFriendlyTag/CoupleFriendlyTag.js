import React from 'react';
import PropTypes from 'prop-types';
import Chip from '../Chip/Chip';

const CoupleFriendlyTag = ({ isCoupleFriendly }) => isCoupleFriendly ? (
  <Chip
    tag="div"
    type="primary"
  >
    Couple Friendly
  </Chip>
) : null;

CoupleFriendlyTag.propTypes = {
  isCoupleFriendly: PropTypes.bool.isRequired,
};

export default CoupleFriendlyTag;
