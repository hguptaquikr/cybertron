import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './loadMore.css';

class LoadMore extends Component {
  state = {
    stepCount: 0,
  }

  onLoadMore = () => {
    const { onLoadMore } = this.props;
    const { stepCount } = this.state;
    if (!onLoadMore) {
      this.setState({
        stepCount: stepCount + 1,
      });
    } else {
      onLoadMore();
    }
  }

  onLoadLess = () => {
    const { onLoadLess } = this.props;
    if (!onLoadLess) {
      this.setState({
        stepCount: 0,
      });
    } else {
      onLoadLess();
    }
  }

  render() {
    const { stepCount } = this.state;
    const {
      children,
      stepper,
      containerClassName,
      actionClassName,
      loadMoreText,
      loadLessText,
      onLoadMore,
      onLoadLess,
    } = this.props;
    const childrenToShow = stepper.initial + (stepper.step * stepCount);
    const isLoadMoreEnabled = !!onLoadMore || React.Children.count(children) > childrenToShow;
    const isLoadLessEnabled = !!onLoadLess || (
      React.Children.count(children) > stepper.initial
        && React.Children.count(children) === childrenToShow
    );

    return (
      <div className={containerClassName}>
        {
          !onLoadMore ? (
            <div>
              {
                React.Children.toArray(children).slice(0, childrenToShow)
              }
              {
                isLoadMoreEnabled ? (
                  <div className="hide">
                    {
                      React.Children.toArray(children).slice(childrenToShow)
                    }
                  </div>
                ) : null
              }
            </div>
          ) : children
        }
        {
          isLoadMoreEnabled ? (
            <div
              className={cx('load-more__action', actionClassName)}
              onClick={this.onLoadMore}
            >
              {loadMoreText}
            </div>
          ) : null
        }
        {
          isLoadLessEnabled ? (
            <div
              className={cx('load-more__action', actionClassName)}
              onClick={this.onLoadLess}
            >
              {loadLessText}
            </div>
          ) : null
        }
      </div>
    );
  }
}

LoadMore.propTypes = {
  children: PropTypes.node.isRequired,
  stepper: PropTypes.shape({
    initial: PropTypes.number,
    step: PropTypes.number,
  }),
  containerClassName: PropTypes.string,
  actionClassName: PropTypes.string,
  loadMoreText: PropTypes.string,
  loadLessText: PropTypes.string,
  onLoadMore: PropTypes.func,
  onLoadLess: PropTypes.func,
};

LoadMore.defaultProps = {
  stepper: { initial: 1, step: 1 },
  containerClassName: '',
  actionClassName: '',
  loadMoreText: 'Load more',
  loadLessText: 'Load less',
};

export default LoadMore;
