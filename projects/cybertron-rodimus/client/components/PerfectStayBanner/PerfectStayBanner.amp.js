import React from 'react';
import Accordion from 'leaf-ui/cjs/Accordion/amp';
import Flex from 'leaf-ui/cjs/Flex/amp';
import Space from 'leaf-ui/cjs/Space/amp';
import Text from 'leaf-ui/cjs/Text/amp';
import Card from 'leaf-ui/cjs/Card/amp';
import Icon from 'leaf-ui/cjs/Icon/amp';
import List from 'leaf-ui/cjs/List/amp';
import Link from 'leaf-ui/cjs/Link/amp';
import View from 'leaf-ui/cjs/View/amp';
import Image from 'leaf-ui/cjs/Image/amp';
import config from '../../../config';

const PerfectStayItems = [
  {
    icon: 'king-bed',
    text: 'Comfortable Rooms',
  },
  {
    icon: 'staff',
    text: 'Best-in-class Service',
  },
  {
    icon: 'breakfast',
    text: 'Wholesome Breakfast',
  },
  {
    icon: 'ac',
    text: 'Assured Room Amenities',
  },
];

const PerfectStay = () => (
  <Accordion>
    <Accordion.Section>
      <Accordion.Section.Trigger id="t-ampPerfectStayBanner">
        <Space padding={[2]}>
          <Card backgroundColor="lagoon">
            <Flex
              flexDirection="row"
              alignItems="center"
              justifyContent="space-between"
            >
              <View>
                <View>
                  <Text
                    size="m"
                    weight="medium"
                    color="white"
                  >
                  Perfect Stay or Don’t Pay
                  </Text>
                  <Space padding={[1, 0, 0, 0]}>
                    <Text size="xs" color="white">
                    100% money-back promise
                    </Text>
                  </Space>
                </View>
                <Icon name="keyboard_arrow_down" color="white" right />
              </View>
            </Flex>
          </Card>
        </Space>
      </Accordion.Section.Trigger>
      <Accordion.Section.Content>
        <Space padding={[2, 2, 0, 0]} className="perfect-stay">
          <List>
            {
              PerfectStayItems.map((item) => (
                <List.Item key={item.text}>
                  <Flex
                    flexDirection="row"
                    alignItems="center"
                  >
                    <View>
                      <Image
                        src={`https://s3-ap-southeast-1.amazonaws.com/treebo/static/images/Icons/${item.icon}.svg`}
                        alt="ac"
                        width="20"
                        height="20"
                      />
                      <Space padding={[0, 0, 0, 1]}>
                        <Text size="s" color="greyDarker">{item.text}</Text>
                      </Space>
                    </View>
                  </Flex>
                </List.Item>
              ))
            }
            <List.Item>
              <Link
                id="t-ampPerfectStayKnowMore"
                href={`${config.hostUrl}/perfect-stay/`}
              >
              Know more
              </Link>
            </List.Item>
          </List>
        </Space>
      </Accordion.Section.Content>
    </Accordion.Section>
  </Accordion>
);

export default PerfectStay;
