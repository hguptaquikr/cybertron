import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Text from 'leaf-ui/cjs/Text/web';
import { Subheading, Heading } from '../../components/Typography/Typography';
import Accordion from '../../components/Accordion/Accordion';
import PerfectStayBannerConstant from './PerfectStayBannerConstant';
import analyticsService from '../../services/analytics/analyticsService';
import './perfectStayBanner.css';

class PerfectStayBanner extends Component {
  state = {
    isOpen: this.props.isOpen,
  }

  knowMoreClick = () => {
    analyticsService.perfectStayKnowMoreClicked();
  }

  accordionTitleClicked = () => {
    analyticsService.perfectStayExpandClicked();
    this.setState((prev) => ({ isOpen: !prev.isOpen }));
  }

  render() {
    const { isOpen } = this.state;
    return (
      <Accordion.Container className="psb" >
        <Accordion.Section className="psb__section" isOpen={isOpen}>
          <Accordion.Title className="psb__section__title" onClick={this.accordionTitleClicked}>
            <Heading className="psb__section__title__heading">
              {'Perfect Stay or Don’t Pay'}
            </Heading>
            <Subheading type="2" className="psb__section__title__subheading">
              <Text type="2" className="psb__text">100% money-back promise</Text>
            </Subheading>
          </Accordion.Title>
          <Accordion.Content className="psb__section__content">
            {
              PerfectStayBannerConstant ? (
                PerfectStayBannerConstant.map((image) => (
                  <div className="psb__section__content__item" key={image.icon}>
                    <i className={`psb__section__content__icon ${image.icon}`} />
                    <Text color="greyDarker" weight="medium">
                      {image.text}
                    </Text>
                  </div>
                ))) : null
            }
            <Text weight="medium">
              <Link to="/perfect-stay/" className="psb__section__content__link" onClick={this.knowMoreClick}>
                Know more
              </Link>
            </Text>
          </Accordion.Content>
        </Accordion.Section>
      </Accordion.Container>
    );
  }
}

PerfectStayBanner.propTypes = {
  isOpen: PropTypes.bool,
};

PerfectStayBanner.defaultProps = {
  isOpen: false,
};

export default PerfectStayBanner;
