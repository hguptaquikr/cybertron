import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withFormsy as formsyHOC } from 'formsy-react';
import '../Input/input.css';

class RadioGroup extends Component {
  state = { value: '' }

  onChange = (e) => {
    const { setValue } = this.props;
    this.setState({ value: e.currentTarget.value });
    setValue(e.currentTarget.value);
  }

  render() {
    const { options, name, containerClassName } = this.props;

    return (
      <div>
        {
          options.map(({ value, label }) => (
            <div key={value} className={`input ${containerClassName}`}>
              <input
                name={name}
                value={value}
                id={value}
                type="radio"
                className="input__field"
                checked={value === this.state.value}
                onChange={this.onChange}
              />
              <label
                className="input__label"
                htmlFor={value}
              >
                {label}
              </label>
            </div>
          ))
        }
      </div>
    );
  }
}

RadioGroup.propTypes = {
  options: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  setValue: PropTypes.func.isRequired,
  containerClassName: PropTypes.string,
};

export default formsyHOC(RadioGroup);
