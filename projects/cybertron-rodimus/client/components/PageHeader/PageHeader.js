import React from 'react';
import PropTypes from 'prop-types';
import BackButton from '../BackButton/BackButton';
import './pageHeader.css';

const PageHeader = ({ children, onBack }) => (
  <div className="ph">
    <BackButton eagerLoad={onBack} />
    <div className="ph__content">
      {children}
    </div>
  </div>
);

PageHeader.propTypes = {
  children: PropTypes.node,
  onBack: PropTypes.func,
};

export default PageHeader;
