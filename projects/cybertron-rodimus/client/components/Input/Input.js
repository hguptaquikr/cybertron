import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { withFormsy as formsyHoc } from 'formsy-react';
import './input.css';

class Input extends Component {
  state = {
    isDirty: false,
  }

  componentDidMount() {
    if (this.props.autoFocus) {
      this.inputRef.focus();
    }
  }

  onChange = (e) => {
    const { setValue, getValue, onChange, formatValue } = this.props;
    const currentValue = getValue() || '';
    setValue(formatValue(e.currentTarget.value, currentValue));
    onChange(e);
  }

  onBlur = () => {
    const { getValue, onBlur } = this.props;
    this.setState({ isDirty: true });
    onBlur(getValue());
  };

  render() {
    const {
      containerClassName,
      inputClassName,
      errorClassName,
      type,
      name,
      label,
      placeholder,
      getValue,
      getErrorMessage,
      isValid,
      isFormSubmitted,
      pattern,
      autoComplete,
    } = this.props;
    const { isDirty } = this.state;
    const isError = (isDirty || isFormSubmitted()) && !isValid();
    const containerClassNames = cx('input', containerClassName);
    const inputClassNames = cx('input__field', inputClassName, {
      'input__field--error': isError,
      'input__field--valid': isValid(),
    });
    const labelClassNames = cx('input__label', {
      'input__label--has-value': getValue() || placeholder,
      'input__label--valid': isValid(),
      'input__label--error': isError,
    });
    const errorClassNames = cx('input__error-message', errorClassName);

    return (
      <div className={containerClassNames}>
        <input
          ref={(ref) => { this.inputRef = ref; }}
          id={name}
          type={type}
          name={name}
          placeholder={placeholder}
          className={inputClassNames}
          onChange={this.onChange}
          onBlur={this.onBlur}
          value={getValue()}
          pattern={pattern}
          autoComplete={autoComplete}
        />
        <label className={labelClassNames} htmlFor={name}>{label}</label>
        {
          isError ? (
            <div className={errorClassNames}>
              {getErrorMessage() || 'Please enter a value'}
            </div>
          ) : null
        }
      </div>
    );
  }
}

Input.propTypes = {
  containerClassName: PropTypes.string,
  inputClassName: PropTypes.string,
  errorClassName: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string,
  pattern: PropTypes.string,
  placeholder: PropTypes.string,
  label: PropTypes.string.isRequired,
  getValue: PropTypes.func.isRequired,
  setValue: PropTypes.func.isRequired,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  formatValue: PropTypes.func,
  getErrorMessage: PropTypes.func.isRequired,
  isValid: PropTypes.func.isRequired,
  isFormSubmitted: PropTypes.func.isRequired,
  autoFocus: PropTypes.bool,
  autoComplete: PropTypes.string,
};

Input.defaultProps = {
  type: 'text',
  onChange: () => {},
  onBlur: () => {},
  formatValue: (v) => v,
};

const FormsyInput = formsyHoc(Input);

const updateValidations = (validations) => {
  if (typeof validations === 'string') {
    return `${validations}${validations ? ',' : ''}isExisty`;
  }
  return {
    ...validations,
    isExisty: true,
  };
};

const RequiredInput = (props) => {
  const { required, validations = '', ...restProps } = props;
  if (required) {
    restProps.required = true;
    restProps.validations = updateValidations(validations);
  }

  return <FormsyInput {...restProps} />;
};

RequiredInput.propTypes = {
  required: PropTypes.bool,
  validations: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

export default RequiredInput;
