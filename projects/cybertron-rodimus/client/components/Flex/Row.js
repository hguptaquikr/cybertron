import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Row = ({
  className,
  tag,
  start,
  center,
  end,
  top,
  baseline,
  middle,
  bottom,
  around,
  between,
  reverse,
  children,
  ...props
}) =>
  React.createElement(tag, {
    className: cx('row', {
      'row--start': start,
      'row--center': center,
      'row--end': end,
      'row--top': top,
      'row--baseline': baseline,
      'row--middle': middle,
      'row--bottom': bottom,
      'row--around': around,
      'row--between': between,
      'row--reverse': reverse,
    }, className),
    ...props,
  }, children);

Row.propTypes = {
  className: PropTypes.string,
  tag: PropTypes.string,
  start: PropTypes.bool,
  center: PropTypes.bool,
  end: PropTypes.bool,
  top: PropTypes.bool,
  baseline: PropTypes.bool,
  middle: PropTypes.bool,
  bottom: PropTypes.bool,
  around: PropTypes.bool,
  between: PropTypes.bool,
  reverse: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

Row.defaultProps = {
  tag: 'div',
};

export default Row;
