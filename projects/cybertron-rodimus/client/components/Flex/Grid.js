import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Grid = ({
  className,
  tag,
  fluid,
  children,
  ...props
}) =>
  React.createElement(tag, {
    className: cx({
      container: !fluid,
      'container-fluid': fluid,
    }, className),
    ...props,
  }, children);

Grid.propTypes = {
  className: PropTypes.string,
  tag: PropTypes.string,
  fluid: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

Grid.defaultProps = {
  tag: 'div',
  fluid: false,
};

export default Grid;
