import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Col = ({
  className,
  tag,
  size,
  offset,
  first,
  last,
  reverse,
  children,
  ...props
}) =>
  React.createElement(tag, {
    className: cx('col', {
      [`col--${size}`]: size,
      [`col--offset-${offset}`]: offset,
      'col--first': first,
      'col--last': last,
      'col--reverse': reverse,
    }, className),
    ...props,
  }, children);

Col.propTypes = {
  className: PropTypes.string,
  tag: PropTypes.string,
  size: PropTypes.string,
  offset: PropTypes.string,
  first: PropTypes.bool,
  last: PropTypes.bool,
  reverse: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

Col.defaultProps = {
  tag: 'div',
};

export default Col;
