import { Component } from 'react';
import PropTypes from 'prop-types';

class ErrorBoundary extends Component {
  state = {
    error: '',
    info: '',
  };

  componentDidCatch(error, info) {
    console.error(`errorBoundary] error: ${error}`);
    console.info(`[errorBoundary] info: ${info}`);
    this.setState({
      error,
      info,
    });
  }

  render() {
    const { render, children } = this.props;

    if (this.state.error) {
      return render(this.state);
    }
    return children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node,
  render: PropTypes.func,
};

export default ErrorBoundary;
