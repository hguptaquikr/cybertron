import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import get from 'lodash/get';
import Flex from 'leaf-ui/cjs/Flex/web';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import View from 'leaf-ui/cjs/View/web';
import Tag from 'leaf-ui/cjs/Tag/web';
import PhonePe from './PhonePe';
import AmazonPay from './AmazonPay';
import TapSection from '../TapSection/TapSection';
import Modal from '../Modal/Modal';
import { Heading } from '../Typography/Typography';
import * as contentActionCreators from '../../services/content/contentDuck';

class Wallet extends Component {
  state = {
    selectedWallet: 'PhonePe',
    isOpen: false,
    isPaymentBtnDisabled: false,
  };

  componentDidMount() {
    const { contentActions } = this.props;
    contentActions.getWalletsPromotion();
  }

  onPaymentChange = (gateWay) => () => {
    const { initiateBooking } = this.props;
    this.setState({ isPaymentBtnDisabled: true },
      () =>
        initiateBooking(gateWay).then((value) => {
          window.location.replace(value.data.redirect_url);
        }));
  }

  toggleWallet = (value) => () => {
    this.setState({ selectedWallet: value });
  }

  mobikwikPay = () => {
    this.props.makePayment({
      method: 'wallet',
      wallet: this.state.selectedWallet,
    });
  }

  shouldWalletVisibile = (isOpen) => {
    this.setState({ isOpen, isPaymentBtnDisabled: false });
  }

  render() {
    const {
      selectedWallet,
      isOpen,
      isPaymentBtnDisabled,
    } = this.state;
    const {
      selectedPrice,
      selectedRatePlan,
      content: { walletsPromotion },
    } = this.props;
    const ratePlan = get(selectedPrice.ratePlans, selectedRatePlan.code);

    return (
      <TapSection
        headerClass="pay__title"
        border="bottom"
        onClick={() => this.shouldWalletVisibile(true)}
        ripple
      >
        <Flex flexDirection="row">
          <View>
            <Text
              weight="medium"
              color="black"
              size="s"
            >
              WALLET/ UPI
            </Text>
            <Space margin={[0, 1, 0]}>
              <Tag
                color="yellow"
                shape="capsular"
                kind="filled"
                size="small"
              >
                NEW
              </Tag>
            </Space>
          </View>
        </Flex>
        <Modal
          isOpen={isOpen}
          onClose={() => this.shouldWalletVisibile(false)}
          closeIcon="back"
          header={<Heading>Select Wallet</Heading>}
          label="Wallet"
        >
          <PhonePe
            onPaymentChange={this.onPaymentChange('phonepe')}
            ratePlan={ratePlan}
            toggleWallet={this.toggleWallet('PhonePe')}
            selectedWallet={selectedWallet}
            isPaymentBtnDisabled={isPaymentBtnDisabled}
            promotionText={walletsPromotion.phonePe}
          />
          <AmazonPay
            onPaymentChange={this.onPaymentChange('amazonpay')}
            ratePlan={ratePlan}
            toggleWallet={this.toggleWallet('AmazonPay')}
            selectedWallet={selectedWallet}
            isPaymentBtnDisabled={isPaymentBtnDisabled}
          />
        </Modal>
      </TapSection>
    );
  }
}

Wallet.propTypes = {
  makePayment: PropTypes.func.isRequired,
  initiateBooking: PropTypes.func.isRequired,
  selectedPrice: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Wallet);
