import React, { Component } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { connect } from 'react-redux';
import ABButton from '../ABButton/ABButton';
import Modal from '../Modal/Modal';
import { Row } from '../Flex/Flex';
import { Heading, Subheading, Text } from '../Typography/Typography';
import Price from '../Price/Price';
import RatePlanLabel from '../RatePlanLabel/RatePlanLabel';
import TapSection from '../TapSection/TapSection';
import * as abService from '../../services/ab/abService';
import searchService from '../../services/search/searchService';

class PayAtHotel extends Component {
  state = {
    showPayAtHotel: false,
  }

  getSellingPriceDifference = (price) => {
    const refundableRatePlan = Object.values(price.ratePlans).find((ratePlan) => ratePlan.type === 'refundable');
    const nonRefundableRatePlan = Object.values(price.ratePlans).find((ratePlan) => ratePlan.type === 'non-refundable');
    const refundableSellingPrice = get(refundableRatePlan, 'sellingPrice');
    const nonRefundableSellingPlanPrice = get(nonRefundableRatePlan, 'sellingPrice');
    return Math.round(refundableSellingPrice - nonRefundableSellingPlanPrice);
  }

  toggleVisibility = (showPayAtHotel) => {
    const { onPayAtHotelClick } = this.props;
    if (showPayAtHotel) onPayAtHotelClick();
    this.setState({ showPayAtHotel });
  }

  render() {
    const { showPayAtHotel } = this.state;
    const {
      payAtHotel,
      selectedRatePlan,
      selectedPrice,
      ui,
      search: { datePicker },
    } = this.props;
    const { numberOfNights } = searchService.formattedDatePicker(datePicker.range, 'D MMM');
    const isNonRefundableRatePlan = selectedRatePlan.type === 'non-refundable';
    const ratePlanDiff = (
      isNonRefundableRatePlan
        ? this.getSellingPriceDifference(selectedPrice) : 0
    );
    const ratePlan = get(selectedPrice.ratePlans, 'EP');

    return (
      <div>
        <TapSection
          className="pay pay__hotel"
          headerClass="pay__title"
          border="bottom"
          title={get(abService.getExperiments(), 'pahCtaMobile.payload.title', 'Pay at Hotel')}
          onClick={() => this.toggleVisibility(true)}
          ripple
        />
        {
          showPayAtHotel ? (
            <Modal
              isOpen
              bottom
              onClose={() => this.toggleVisibility(false)}
              closeIcon="close"
              label="Pay at Hotel"
              header={
                <Heading type="3" tag="span" className="pay__pah__header text-center">
                  {get(abService.getExperiments(), 'pahCtaMobile.payload.title') || 'Pay at Hotel'}
                </Heading>
              }
              headerClass="pay__pah__header"
            >
              <div className="pay__pah__container">
                {
                  isNonRefundableRatePlan ? (
                    <div className="pay__pah__section">
                      <Subheading className="pay__pah__section-title">Additional Charge of ₹{ratePlanDiff}</Subheading>
                      <Text type="2">
                        If you&apos;re unsure of your travel dates,
                        you can choose to pay at hotel for an additional
                        ₹{ratePlanDiff}. Zero cancellation fee within 24hours of checkin.
                      </Text>
                    </div>
                  ) : null
                }
                <div className="pay__pah__section">
                  <Subheading className="pay__pah__section-title">Secure your Rooms</Subheading>
                  <Text type="2">
                    You can secure your rooms by paying
                    a part payment 2 days prior your check-in dates.
                  </Text>
                </div>
                <div className="pay__pah__section">
                  <Subheading className="pay__pah__section-title">Total Payable Amount</Subheading>
                  <Row middle className="pay__pah__price" >
                    <Price
                      size="large"
                      type="regular"
                      price={ratePlan.sellingPrice}
                      preview={!ratePlan.sellingPrice}
                      previewStyle={{ width: '80px' }}
                    />
                    <RatePlanLabel
                      className="pay__pah__rate-plan-tag"
                      type="refundable"
                      tag={ratePlan.tag}
                    />
                  </Row>
                  <Text
                    tag="span"
                    type="3"
                    className="pay__pah__price__tax"
                  >
                    Tax incl. price for {numberOfNights.text}
                  </Text>
                </div>
                <ABButton
                  id="t-itineraryPahCTA"
                  disabled={ui.isProccedToBook}
                  flat
                  large
                  block
                  onClick={() => payAtHotel(ratePlan)}
                  experiment="pahCtaMobile"
                  render={(text) => text || 'CONFIRM'}
                />
              </div>
            </Modal>
          ) : null
        }
      </div>
    );
  }
}

PayAtHotel.propTypes = {
  payAtHotel: PropTypes.func.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
  onPayAtHotelClick: PropTypes.func.isRequired,
  selectedPrice: PropTypes.object.isRequired,
  ui: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  ui: state.ui,
  search: state.search,
});

export default connect(
  mapStateToProps,
)(PayAtHotel);
