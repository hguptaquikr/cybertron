import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import cx from 'classnames';
import Payment, { fns as PayService } from 'payment';
import Modal from '../Modal/Modal';
import TapSection from '../TapSection/TapSection';
import { Row, Col } from '../Flex/Flex';
import { Heading } from '../Typography/Typography';
import Input from '../Input/Input';
import Button from '../Button/Button';
import Price from '../Price/Price';
import CardInput from './CardInput';

Payment.addToCardArray({
  type: 'rupay',
  pattern: /^(508|606|607|608|652|653)/,
  format: /(\d{1,4})/g,
  length: [16],
  cvcLength: [3],
  luhn: true,
});

// Add this to the end of the array to get default values
Payment.addToCardArray({
  type: 'default',
  pattern: /^\d*/,
  format: /(\d{1,4})/g,
  length: [16],
  cvcLength: [3],
  luhn: false,
});

class CardPay extends Component {
  state = {
    isOpen: false,
    cardType: {
      type: 'default',
      cvcLength: [3],
      length: [16],
    },
  };

  setCardType = (value) => {
    this.setState({ cardType: value });
  };

  handleCardVisibility = (isOpen) => {
    this.setState({ isOpen });
  }

  // TODO: Format expiry
  formatExpiryMonth = (val, oldVal) => {
    const expMonthChecks = (
      !/^\d+$/.test(val) ||
      val.length > 2
    );
    if (val && expMonthChecks) {
      return oldVal;
    }
    return val;
  }

  formatExpiryYear = (val, oldVal) => {
    const expYearChecks = (
      !/^\d+$/.test(val) ||
        val.length > 2
    );
    if (val && expYearChecks) {
      return oldVal;
    }
    return val;
  }

  formatCvv = (val, oldVal) => {
    const maxLength = this.state.cardType.cvcLength.slice(-1)[0];
    const cvvChecks = (
      !/^\d+$/.test(val) ||
       val.length > maxLength
    );

    if (val && cvvChecks) {
      return oldVal;
    }
    return val;
  }

  pay = ({ card, name, expiryMonth, expiryYear, cvv }) => {
    const data = {
      method: 'card',
      'card[name]': name,
      'card[number]': card.replace(/ /g, ''),
      'card[cvv]': cvv,
      'card[expiry_month]': expiryMonth,
      'card[expiry_year]': expiryYear,
    };

    this.setState({ isOpen: false });
    this.props.makePayment(data);
  };

  // TODO: Disable button during pay
  render() {
    const { cardType, isOpen } = this.state;
    const { price } = this.props;
    return (
      <TapSection
        className="pay pay__card"
        headerClass="pay__title"
        title="DEBIT / CREDIT CARD"
        onClick={() => this.handleCardVisibility(true)}
        ripple
      >
        <Modal
          isOpen={isOpen}
          onClose={() => this.handleCardVisibility(false)}
          closeIcon="back"
          header={<Heading>Debit/Credit Card</Heading>}
          label="DEBIT / CREDIT CARD"
          bodyClassName="card-pay"
        >
          <Formsy className="card-pay__container" onValidSubmit={this.pay} noValidate>
            <CardInput
              getCardArray={Payment.getCardArray}
              formatCardNumber={PayService.formatCardNumber}
              name="card"
              cardType={cardType}
              validations={{
                validCard: (vs, v) => !!PayService.validateCardNumber(v),
              }}
              validatationError="Please Enter the Card Number"
              validationErrors={{
                validCard: 'Card Number Invalid',
                isDefaultRequiredValue: 'Please Enter the Card Number',
              }}
              onChange={this.setCardType}
              label="Card Number"
              required
            />
            <Input
              type="text"
              name="name"
              label="Card Holder's Name"
              errorClassName="card-pay__error"
              inputClassName="card-pay__input"
              containerClassName="card-pay__input-container"
              required
            />
            <Row>
              <Col size="3">
                <Input
                  type="number"
                  name="expiryMonth"
                  label="Expiry"
                  placeholder="MM"
                  errorClassName="card-pay__error"
                  inputClassName="card-pay__input"
                  containerClassName="card-pay__input-container"
                  pattern="\d*"
                  formatValue={this.formatExpiryMonth}
                  validationErrors={{
                    isDefaultRequiredValue: 'Required',
                  }}
                  required
                />
              </Col>
              <Col size="3">
                <Input
                  type="number"
                  name="expiryYear"
                  label=""
                  placeholder="YY"
                  errorClassName="card-pay__error"
                  inputClassName="card-pay__input"
                  containerClassName="card-pay__input-container"
                  pattern="\d*"
                  formatValue={this.formatExpiryYear}
                  validationErrors={{
                    isDefaultRequiredValue: 'Required',
                  }}
                  required
                />
              </Col>
              <Col size="6">
                <Input
                  label="CVV"
                  name="cvv"
                  type="password"
                  formatValue={this.formatCvv}
                  validations={{
                    validCvv: (vs, v) => !!PayService.validateCardCVC(v, cardType.type),
                  }}
                  validationErrors={{
                    validCvv: 'Invalid CVV',
                    isDefaultRequiredValue: 'Please Enter the CVV',
                  }}
                  pattern="\d*"
                  autoComplete="false"
                  required
                />
              </Col>
            </Row>
            <div className="cop__footer">
              <Button
                block
                large
                flat
                type="submit"
                name="button"
                className={cx('checkout__action button')}
              >
                CONFIRM PAY&nbsp;
                <Price
                  price={price}
                  bold
                  type="default"
                  size="medium"
                  previewStyle={{ display: 'none' }}
                />
              </Button>
            </div>
          </Formsy>
        </Modal>
      </TapSection>
    );
  }
}

CardPay.propTypes = {
  makePayment: PropTypes.func.isRequired,
  price: PropTypes.number,
};

export default CardPay;
