import './pay.css';

export CardPay from './CardPay';
export Netbank from './Netbank';
export Wallet from './Wallet';
export PayAtHotel from './PayAtHotel';
