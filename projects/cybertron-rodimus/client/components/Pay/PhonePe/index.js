import Loadable from 'react-loadable';

const PhonePe = Loadable({
  loader: () => import('./PhonePe' /* webpackChunkName: 'PhonePe' */),
  loading: () => null,
});

export default PhonePe;
