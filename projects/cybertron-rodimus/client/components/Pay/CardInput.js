import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { withFormsy as formsyHoc } from 'formsy-react';

// Todo: Refactor to use the new Input
class CardInput extends Component {
  state = {
    isDirty: false,
  };

  componentDidMount() {
    this.inputRef.focus();
  }

  getCardTypeDetails = (num) => this.props.getCardArray().find(
    (v) => v.pattern.test(num),
  );

  handleFieldChange = (e) => {
    const current = e.target.value.replace(/ /g, '');
    const { formatCardNumber, setValue, getValue, onChange } = this.props;

    if (current && !/^\d+$/.test(current)) {
      setValue(getValue() || '');
    }

    const cardType = this.getCardTypeDetails(current);
    const formattedValue = (formatCardNumber(current) || '').trim();
    setValue(formattedValue);
    onChange(cardType);
  };

  render() {
    const {
      name,
      label,
      isFormSubmitted,
      isValid,
      getErrorMessage,
      getValue,
      cardType: { type },
    } = this.props;

    const { isDirty } = this.state;
    const isError = (isDirty || isFormSubmitted()) && !isValid();

    const inputClassNames = cx('input__field card-pay__input card-pay__number', {
      'input__field--error': isError,
    });
    const labelClassNames = cx('input__label', {
      'input__label--has-value': getValue(),
      'input__label--valid': isValid(),
      'input__label--error': isError,
    });

    return (
      <div className="input card-pay__input-container">
        {
          type !== 'default' ? (
            <img
              className="old-image card-pay__card-logo"
              src={`//images.treebohotels.com/images/payment/${type}.svg`}
              alt={type}
            />
          ) : null
        }
        <input
          ref={(ref) => { this.inputRef = ref; }}
          id={name}
          className={inputClassNames}
          type="text"
          value={getValue()}
          onChange={this.handleFieldChange}
          onBlur={() => this.setState({ isDirty: true })}
          pattern="\d*"
        />
        <label htmlFor={name} className={labelClassNames}>
          {label}
        </label>
        {
          isError ? (
            <div className="card-pay__error input__error-message">
              {getErrorMessage()}
            </div>
          ) : (null)
        }
      </div>
    );
  }
}

CardInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  cardType: PropTypes.object.isRequired,
  isValid: PropTypes.func.isRequired,
  isFormSubmitted: PropTypes.func.isRequired,
  getErrorMessage: PropTypes.func.isRequired,
  getValue: PropTypes.func.isRequired,
  setValue: PropTypes.func.isRequired,
  formatCardNumber: PropTypes.func.isRequired,
  getCardArray: PropTypes.func.isRequired,
  onChange: PropTypes.func,
};

export default formsyHoc(CardInput);
