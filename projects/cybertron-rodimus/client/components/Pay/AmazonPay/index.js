import Loadable from 'react-loadable';
import get from 'lodash/get';
import * as abService from '../../../services/ab/abService';

export default {
  visible: Loadable({
    loader: () => import('./AmazonPay' /* webpackChunkName: 'AmazonPay' */),
    loading: () => null,
  }),
  hidden: Loadable({
    loader: () => Promise.resolve(() => null),
    loading: () => null,
  }),
}[get(abService.getExperiments(), 'AmazonPayWallet.variant') || 'hidden'];
