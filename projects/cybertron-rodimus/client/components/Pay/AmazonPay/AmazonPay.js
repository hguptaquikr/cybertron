import React from 'react';
import PropTypes from 'prop-types';
import Flex from 'leaf-ui/cjs/Flex/web';
import Text from 'leaf-ui/cjs/Text/web';
import Image from 'leaf-ui/cjs/Image/web';
import Button from 'leaf-ui/cjs/Button/web';
import RadioButton from 'leaf-ui/cjs/RadioButton/web';
import Space from 'leaf-ui/cjs/Space/web';
import View from 'leaf-ui/cjs/View/web';
import Price from 'leaf-ui/cjs/Price/web';

const AmazonPay = ({
  onPaymentChange,
  ratePlan,
  toggleWallet,
  isPaymentBtnDisabled,
  selectedWallet,
}) => (
  <Space margin={[2, 2, 4]}>
    <Flex justifyContent="space-between">
      <View>
        <Flex
          flexDirection="row"
          alignItems="center"
        >
          <View>
            <RadioButton
              onClick={toggleWallet}
              checked={selectedWallet === 'AmazonPay'}
              value="AmazonPay"
              label={(<Space margin={[0, 1, 0, 0]}><Image src="https://images.treebohotels.com/images/amazonPay.png" /></Space>)}
            />
            <Text color="greyDarker" size="m">
              Amazon Pay
            </Text>
          </View>
        </Flex>
        {
          selectedWallet === 'AmazonPay' ? (
            <Space margin={[3, 0, 0, 3]}>
              <Button
                onClick={onPaymentChange}
                disabled={isPaymentBtnDisabled}
              >
                PROCEED TO PAY <Price>{ratePlan.sellingPrice}</Price>
              </Button>
            </Space>
          ) : null
        }
      </View>
    </Flex>
  </Space>

);

AmazonPay.propTypes = {
  onPaymentChange: PropTypes.func,
  toggleWallet: PropTypes.func,

  isPaymentBtnDisabled: PropTypes.bool,
  ratePlan: PropTypes.object,
  selectedWallet: PropTypes.string,
};

export default AmazonPay;
