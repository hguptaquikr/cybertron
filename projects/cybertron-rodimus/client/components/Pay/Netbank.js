import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Heading } from '../Typography/Typography';
import { Row, Col } from '../Flex/Flex';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';
import TapSection from '../TapSection/TapSection';

class Netbank extends Component {
  state = {
    isOpen: false,
  };

  defaultBanks = [
    { key: 'SBIN', value: 'sbi' },
    { key: 'HDFC', value: 'hdfc' },
    { key: 'ICIC', value: 'icici' },
    { key: 'UTIB', value: 'axis' },
    { key: 'KKBK', value: 'kotak' },
  ];

  handleBankVisibility = (isOpen) => {
    this.setState({ isOpen });
  }

  pay = (bank) => {
    this.setState({ isOpen: false });
    this.props.makePayment({
      method: 'netbanking',
      bank,
    });
  };

  render() {
    const { isOpen } = this.state;
    const { banks } = this.props;
    return (
      <div className="netbank">
        <Row className="pay__title" tag="h3" type="3" middle>
          <Col size="6">NETBANKING</Col>
          <Col size="6">
            <Button
              className="text-right netbank__other"
              modifier="link"
              block
              onClick={() => this.handleBankVisibility(true)}
            >
              Other Banks <i className="icon-angle-right" />
            </Button>
          </Col>
        </Row>
        <Row tag="ul" className="netbank__list">
          {
            this.defaultBanks.map(({ key, value }) => (
              <Col tag="li" className="text-center" key={value} onClick={() => this.pay(key)}>
                <img
                  className="old-image netbank__img"
                  src={`//images.treebohotels.com/images/payment/${value}.svg?v=1`}
                  alt={value}
                />
                <p className="netbank__title">{value}</p>
              </Col>
            ))
          }
        </Row>
        <Modal
          isOpen={isOpen}
          onClose={() => this.handleBankVisibility(false)}
          closeIcon="back"
          header={<Heading>Netbanking</Heading>}
          label="NETBANKING"
        >
          {
            banks.map(({ key, value }) => (
              <TapSection
                key={key}
                title={value}
                border="bottom"
                onClick={() => this.pay(key)}
                ripple
              />
            ))
          }
        </Modal>
      </div>
    );
  }
}

Netbank.propTypes = {
  makePayment: PropTypes.func.isRequired,
  banks: PropTypes.array.isRequired,
};

export default Netbank;
