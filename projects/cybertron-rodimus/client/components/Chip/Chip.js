import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Text } from '../Typography/Typography';
import './chip.css';

const Chip = ({ className, type, tag, children }) => (
  <Text
    tag={tag}
    className={cx(`chip text-center chip--${type}`, className)}
  >
    {children}
  </Text>
);

Chip.propTypes = {
  tag: PropTypes.string,
  children: PropTypes.string,
  className: PropTypes.string,
  type: PropTypes.string,
};

Chip.defaultProps = {
  type: 'primary',
};

export default Chip;
