import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './button.css';

const Button = ({
  children,
  className,
  modifier,
  block,
  flat,
  large,
  bold,
  round,
  disabled,
  preview,
  ...props
}) => {
  const buttonClass = cx(`button button--${modifier}`, {
    'button--block': block,
    'button--flat': flat,
    'button--large': large,
    'button--disabled': disabled,
    'button--bold': bold,
    'button--round': round,
    'button--preview': preview,
  }, className);
  return (
    <button
      className={buttonClass}
      disabled={disabled || preview}
      {...props}
    >
      {children}
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  block: PropTypes.bool,
  flat: PropTypes.bool,
  large: PropTypes.bool,
  bold: PropTypes.bool,
  round: PropTypes.bool,
  preview: PropTypes.bool,
  disabled: PropTypes.bool,
  modifier: PropTypes.oneOf([
    'primary',
    'secondary',
    'tertiary',
    'link',
  ]),
};

Button.defaultProps = {
  modifier: 'primary',
};

export default Button;
