import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import { Text } from '../../Typography/Typography';
import './earnRewards.css';

const EarnRewards = ({ className, isUserAuthenticated, border, wallet }) => {
  const treeboPoints = wallet.byType.TP || wallet.byType[''];
  return (
    isUserAuthenticated && !treeboPoints.usableBalance ? (
      <Text
        className={cx('er', { 'er--border': border }, className)}
        tag="span"
      >
        <i className="icon-earn-rewards er__icon" />
        <span>To earn Treebo Points on your stay - Book Now!</span>
      </Text>
    ) : null
  );
};

EarnRewards.propTypes = {
  className: PropTypes.string,
  isUserAuthenticated: PropTypes.bool.isRequired,
  border: PropTypes.bool,
  wallet: PropTypes.object.isRequired,
};

EarnRewards.defaultProps = {
  border: false,
};

const mapStateToProps = (state) => ({
  isUserAuthenticated: state.auth.isAuthenticated,
  wallet: state.wallet,
});

export default connect(
  mapStateToProps,
)(EarnRewards);
