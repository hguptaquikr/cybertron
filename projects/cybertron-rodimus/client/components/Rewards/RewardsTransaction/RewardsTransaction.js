import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Price from '../../Price/Price';
import { Row, Col } from '../../Flex/Flex';
import { Subheading, Text } from '../../Typography/Typography';
import './rewardsTransaction.css';

const RewardsTransaction = ({ transaction, priceType, transactionIcon }) => (
  <div
    className="rtx"
    data-txn-type={transaction.isExpired ? 'expired' : transaction.type}
    data-txn-amount={`${priceType} ${transaction.amount}`}
    data-booking-id={transaction.customField}
  >
    {transactionIcon}
    <div className="rtx__details">
      <div className="rtx__header">
        <Row between className="rtx__title">
          <Subheading
            className={cx({ 'rtx__title--striked': transaction.isExpired })}
            preview={!transaction.desc}
            previewStyle={{ width: '100px' }}
          >
            {transaction.desc}
          </Subheading>
          <Price
            type={priceType}
            price={transaction.amount}
            preview={!transaction.amount}
            round={false}
          />
        </Row>
        {
          transaction.type !== 'debit' && transaction.expiresOn ? (
            <Text className="rtx__subtitle">
              <span className={cx({ 'rtx__credit--expired': transaction.isExpired })}>
                { transaction.isExpired ? 'Expired ' : 'Expires ' }
              </span>
              on {transaction.expiresOn}
            </Text>
          ) : null
        }
      </div>
      <Text type="2">
        { transaction.customField ? `Booking ID: ${transaction.customField}` : null }
      </Text>
      <Text
        type="2"
        preview={!transaction.createdOn}
        previewStyle={{ width: '60px' }}
      >
        {transaction.createdOn}
      </Text>
      {
        transaction.type === 'refund' ? transaction.credits.map((credit) => (
          <div
            className="rtx__credit"
            data-credit-expired={credit.isExpired ? 'true' : 'false'}
            data-credit-amount={credit.amount}
          >
            <Row type="2">
              <Col size="9">
                <Text
                  className={cx('rtx__credit__title', {
                    'rtx__title--striked': credit.isExpired,
                  })}
                  type="2"
                >
                  {credit.desc}
                </Text>
              </Col>
              <Col className="text-right" size="3">
                <Price
                  className="rtx__credit__amount"
                  type={credit.isExpired ? 'striked' : 'credit'}
                  price={+credit.amount}
                  round={false}
                />
              </Col>
            </Row>
            <Text type="3" tag="span" className="rtx__credit__expires-on">
              <span className={cx({ 'rtx__credit--expired': transaction.isExpired })}>
                { credit.isExpired ? 'Expired' : 'Expires ' }
              </span>
              on {credit.expiresOn}
            </Text>
          </div>
        )) : null
      }
    </div>
  </div>
);

RewardsTransaction.propTypes = {
  transaction: PropTypes.object.isRequired,
  priceType: PropTypes.string,
  transactionIcon: PropTypes.node,
};

export default RewardsTransaction;
