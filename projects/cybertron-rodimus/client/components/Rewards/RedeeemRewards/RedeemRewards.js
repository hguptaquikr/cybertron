import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Price from '../../Price/Price';
import { Text } from '../../Typography/Typography';
import './redeemRewards.css';

const RedeemRewards = ({ isUserAuthenticated, wallet }) => {
  const treeboPoints = wallet.byType.TP || wallet.byType[''];
  return (
    isUserAuthenticated && treeboPoints.usableBalance ? (
      <Text className="rr" tag="div">
        <i className="icon-redeem-rewards rr__icon" />
        <div>
          <Price price={treeboPoints.usableBalance} type="regular" size="small" />
          <span> worth of Treebo Points preapplied to price.</span>
        </div>
      </Text>
    ) : null
  );
};

RedeemRewards.propTypes = {
  isUserAuthenticated: PropTypes.bool.isRequired,
  wallet: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  isUserAuthenticated: state.auth.isAuthenticated,
  wallet: state.wallet,
});

export default connect(
  mapStateToProps,
)(RedeemRewards);
