import React, { Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import cx from 'classnames';
import qs from 'query-string';
import Price from '../../Price/Price';
import Ripple from '../../Ripple/Ripple';
import TapSection from '../../TapSection/TapSection';
import RewardsPage from '../../../views/RewardsPage';
import analyticsService from '../../../services/analytics/analyticsService';
import './rewardsTapSection.css';

class RewardsTapSection extends Component {
  redirectToIntroducingTreeboRewardsPage = () => {
    const { history } = this.props;
    analyticsService.treeboRewardsClicked();
    history.push({
      pathname: '/introducing-rewards/',
      search: qs.stringify({
        flow: 'menu',
      }),
    });
  }

  render() {
    const { wallet } = this.props;
    const treeboPoints = wallet.byType.TP || wallet.byType[''];
    const rewardsTitle = (
      <div className="rts__ts__title">
        <i className="icon-redeem-rewards rts__ts__icon" />
        Treebo Rewards
        <div
          className={cx('rts__balance', {
            'rts__balance--empty': !treeboPoints.totalBalance,
            'rts__balance--has-balance': treeboPoints.totalBalance,
          })}
        >
          {
            treeboPoints.totalBalance ? (
              <Price
                price={+treeboPoints.totalBalance}
                size="small"
                className="rts__balance__value"
              />
            ) : 'NEW'
          }
        </div>
      </div>
    );
    return (
      <div className="rts">
        <Ripple eagerLoad={RewardsPage.preload}>
          <TapSection
            className="rts__ts"
            title={rewardsTitle}
            border="bottom"
            onClick={this.redirectToIntroducingTreeboRewardsPage}
          />
        </Ripple>
      </div>
    );
  }
}

RewardsTapSection.propTypes = {
  wallet: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  wallet: state.wallet,
});

export default compose(
  connect(mapStateToProps),
  withRouter,
)(RewardsTapSection);
