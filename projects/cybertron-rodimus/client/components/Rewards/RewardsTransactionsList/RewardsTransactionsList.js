import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import { bindActionCreators } from 'redux';
import Button from '../../Button/Button';
import Ripple from '../../Ripple/Ripple';
import { Heading, Text } from '../../Typography/Typography';
import RewardsTransaction from '../RewardsTransaction/RewardsTransaction';
import analyticsService from '../../../services/analytics/analyticsService';
import * as searchActionCreators from '../../../services/search/searchDuck';
import * as uiActionCreators from '../../../services/ui/uiDuck';
import './rewardsTransactionsList.css';

class RewardsTransactionsList extends Component {
  onSearchClick = () => {
    const { uiActions } = this.props;
    uiActions.showSearchWidget(true);
    analyticsService.walletPageActionClicked();
  }

  getPriceType = (transaction) => {
    const transactionType = this.getTransactionType(transaction.type);
    const isCreditExpired = transactionType === 'credit' && transaction.isExpired;
    if (isCreditExpired) return 'striked';
    else if (transaction.type === 'debit') return 'debit';
    return 'credit';
  }

  getTransactionType = (type) => (type === 'debit' ? 'debit' : 'credit')

  icons = {
    credit: (
      <span className="icon-credit">
        <span className="path1" />
        <span className="path2" />
        <span className="path3" />
      </span>
    ),
    debit: (
      <span className="icon-debit">
        <span className="path1" />
        <span className="path2" />
        <span className="path3" />
      </span>
    ),
    striked: (
      <span className="icon-credit-expired" />
    ),
  }

  renderEmptyWallet = () => {
    const { content } = this.props;
    return (
      <div className="text-center">
        <img className="old-image rtxl__image" src={content.rewards.rewardsPage.imageUrl} alt="no-rewards" />
        <Heading type="2" className="rtxl__title">Earn Treebo Points</Heading>
        <Text type="2">Book and stay with us to start earning Treebo Points!</Text>
        <Ripple>
          <div className="floaty" onClick={this.onSearchClick}>
            <Button
              large
              flat
              block
              className="floaty"
            >Start Booking
            </Button>
          </div>
        </Ripple>
      </div>
    );
  }

  renderWallet = (treeboPoints) => {
    if (isEmpty(treeboPoints.statements)) {
      return [0, 1, 2, 3].map((number, index) => (
        <RewardsTransaction
          key={`${number}${index + 1}`}
          transaction={{}}
          priceType=""
          transactionIcon=""
        />
      ));
    }
    return treeboPoints.statements.map((transaction, index) => {
      const transactionType = this.getPriceType(transaction);
      const icon = this.icons[transactionType];
      return (
        <RewardsTransaction
          key={`${transaction}${index + 1}`}
          transaction={transaction}
          priceType={this.getPriceType(transaction)}
          transactionIcon={icon}
        />
      );
    });
  }

  render() {
    const { wallet } = this.props;
    const treeboPoints = wallet.byType.TP || wallet.byType[''];
    return (
      <div className="rtxl">
        {
          !treeboPoints.usableBalance && isEmpty(treeboPoints.statements) ? (
            this.renderEmptyWallet()
          ) : (
            this.renderWallet(treeboPoints)
          )
        }
      </div>
    );
  }
}

RewardsTransactionsList.propTypes = {
  wallet: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  uiActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
  wallet: state.wallet,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  uiActions: bindActionCreators(uiActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RewardsTransactionsList);
