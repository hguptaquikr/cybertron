import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './showMore.css';

class ShowMore extends Component {
  state = {
    isShowingMore: false,
  }

  componentDidMount() {
    setTimeout(this.checkContainerHeight);
  }

  componentDidUpdate() {
    setTimeout(this.checkContainerHeight);
  }

  getMaxHeight = () => {
    const { isShowingMore } = this.state;
    const { initialHeight } = this.props;
    if (!this.showMoreEnabled) return 'none';
    return isShowingMore ? 'none' : `${initialHeight}px`;
  }

  checkContainerHeight = () => {
    const { initialHeight } = this.props;
    if (this.containerRef.clientHeight) {
      const prevShowMoreEnabled = this.showMoreEnabled;
      this.showMoreEnabled = this.containerRef.clientHeight >= initialHeight;
      if (prevShowMoreEnabled !== this.showMoreEnabled) this.forceUpdate();
    }
  }

  toggleShowMore = (e) => {
    const { onShowMore, onShowLess } = this.props;
    e.stopPropagation();
    this.setState((prevState) => {
      if (!prevState.isShowingMore) onShowMore();
      else onShowLess();
      return {
        isShowingMore: !prevState.isShowingMore,
      };
    });
  }

  storeContainerRef = (node) => {
    if (node) this.containerRef = node;
  }

  render() {
    const { isShowingMore } = this.state;
    const { children, className, actionClassName, showMoreText, showLessText } = this.props;

    return (
      <div className={className}>
        <div
          className="show-more"
          style={{ maxHeight: this.getMaxHeight() }}
          ref={this.storeContainerRef}
          id="t-show-more"
        >
          {children}
        </div>
        <div
          className={cx('show-more__action', actionClassName, {
            hide: !this.showMoreEnabled,
          })}
          onClick={this.toggleShowMore}
        >
          {
            isShowingMore ? showLessText : showMoreText
          }
        </div>
      </div>
    );
  }
}

ShowMore.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  actionClassName: PropTypes.string,
  initialHeight: PropTypes.number.isRequired,
  showMoreText: PropTypes.string,
  showLessText: PropTypes.string,
  onShowMore: PropTypes.func,
  onShowLess: PropTypes.func,
};

ShowMore.defaultProps = {
  onShowMore: () => {},
  onShowLess: () => {},
  showMoreText: 'SHOW MORE',
  showLessText: 'SHOW LESS',
};

export default ShowMore;
