import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { Subheading } from '../Typography/Typography';
import './loader.css';

const Loader = ({ loader }) => (
  <div className={cx('loader-container', {
    hide: !loader.visible,
  })}
  >
    <div className="loader text-center">
      <span className="loader__circle" />
      <Subheading className="loader__text">Processing</Subheading>
    </div>
  </div>
);

Loader.propTypes = {
  loader: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  loader: state.loader,
});

export default connect(
  mapStateToProps,
)(Loader);
