import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Text } from '../Typography/Typography';
import './specialDealsTag.css';

const SpecialDealsTag = ({ className }) => (
  <Text className={cx('special-deals-tag', className)} tag="span">
    <span className="icon-member-rates special-deal-tag__icon">
      <span className="path1" />
      <span className="path2" />
      <span className="path3" />
      <span className="path4" />
      <span className="path5" />
      <span className="path6" />
    </span>
    Member Rate
  </Text>
);

SpecialDealsTag.propTypes = {
  className: PropTypes.string,
};

export default SpecialDealsTag;
