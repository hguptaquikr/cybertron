import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Text from 'leaf-ui/cjs/Text/web';
import { Subheading } from '../../components/Typography/Typography';
import './needToKnow.css';

class NeedToKnow extends Component {
  state = {
    showMore: true,
  }

  toogle = () => {
    const { scrollToSection } = this.props;
    this.setState({ showMore: !this.state.showMore }, () => {
      if (scrollToSection && this.state.showMore) scrollToSection();
    });
  }

  render() {
    const { showMore } = this.state;
    const { needToKnowPolicies, className } = this.props;

    return (
      <div className={cx('ntk', className)}>
        <ul>
          {
            needToKnowPolicies.map((content, index) => (
              <li
                id={index}
                key={content.type}
                className={cx('ntk__content', {
                  hide: index >= 2 && showMore,
                })}
              >
                {content.description}
              </li>
            ))
          }
        </ul>
        {
          needToKnowPolicies.length > 2 ? (
            <Text weight="medium">
              <Subheading tag="div" className="ntk__view-more" onClick={this.toogle}>
                {showMore ? 'View More' : 'View Less'}
              </Subheading>
            </Text>
          ) : null
        }
      </div>
    );
  }
}

NeedToKnow.propTypes = {
  needToKnowPolicies: PropTypes.array.isRequired,
  className: PropTypes.string,
  scrollToSection: PropTypes.func,
};

export default NeedToKnow;
