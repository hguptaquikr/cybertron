import PropTypes from 'prop-types';
import React from 'react';
import Flex from 'leaf-ui/cjs/Flex/web';
import List from 'leaf-ui/cjs/List/web';
import MoreOrLess from 'leaf-ui/cjs/MoreOrLess/web';
import Space from 'leaf-ui/cjs/Space/web';
import Text from 'leaf-ui/cjs/Text/web';
import View from 'leaf-ui/cjs/View/web';

const NeedToKnow = ({ needToKnowPolicies }) => (
  <Space padding={[1, 2, 3, 2]}>
    <MoreOrLess initialHeight="135px">
      <Flex>
        <View>
          <Space margin={[2, 0]}>
            <Text size="m" color="black" weight="medium">
              Need To Know
            </Text>
          </Space>
          <List type="unordered">
            {
              needToKnowPolicies.map(({ description, type }) => (
                <List.Item key={type}>
                  <Text
                    size="s"
                    color="greyDarker"
                    weight="normal"
                    component="p"
                  >
                    {description}
                  </Text>
                </List.Item>
              ))
            }
          </List>
        </View>
      </Flex>
    </MoreOrLess>
  </Space>
);

NeedToKnow.propTypes = {
  needToKnowPolicies: PropTypes.array.isRequired,
};

export default NeedToKnow;
