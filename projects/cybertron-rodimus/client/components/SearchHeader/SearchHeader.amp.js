import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { withRouter } from 'react-router-dom';
import Flex from 'leaf-ui/cjs/Flex/amp';
import Text from 'leaf-ui/cjs/Text/amp';
import Divider from 'leaf-ui/cjs/Divider/amp';
import Icon from 'leaf-ui/cjs/Icon/amp';
import Space from 'leaf-ui/cjs/Space/amp';
import Size from 'leaf-ui/cjs/Size/amp';
import Image from 'leaf-ui/cjs/Image/amp';
import View from 'leaf-ui/cjs/View/amp';
import config from '../../../config';
import searchService from '../../services/search/searchService';
import * as uiActionCreators from '../../services/ui/uiDuck';
import * as searchActionCreators from '../../services/search/searchDuck';

const SearchHeader = ({
  search: {
    searchInput: { place },
    datePicker: { range },
    roomConfig: { rooms },
  },
  location,
}) => {
  const { checkIn, checkOut } = searchService.formattedDatePicker(range, 'D MMM');
  const formattedRoomConfig = searchService.formattedRoomConfig(rooms);
  const hdUrl = `${config.hostUrl}${location.pathname.replace('amp/', '')}?modal=search`;

  return (
    <Fragment>
      <Space padding={[1, 2]}>
        <Flex
          flexDirection="row"
          alignItems="center"
          justifyContent="space-between"
        >
          <View>
            <Space padding={[0.25]}>
              <Size width="24px" height="24px">
                <a id="t-ampHome" href={config.hostUrl} className="link">
                  <Image
                    src="https://s3-ap-southeast-1.amazonaws.com/treebo/static/images/Icons/treebo_logo.svg"
                    alt="logo"
                    width="20"
                    height="20"
                  />
                </a>
              </Size>
            </Space>
            <a href={hdUrl} id="t-ampSearchHeader" className="link">
              <Space padding={[0, 2]} margin={[0, 0, 0.5, 0]} >
                <Size width="245px">
                  <Text size="s" color="black" truncate>
                    {place.q}
                  </Text>
                </Size>
              </Space>
              <Space padding={[0, 2]} margin={[0]}>
                <Text size="s" color="grey" truncate>
                  {checkIn && checkOut ? `${checkIn} - ${checkOut} / ` : null}
                  {formattedRoomConfig.text}
                </Text>
              </Space>
            </a>
            <a href={hdUrl} id="t-ampSearchHeaderIcon" className="link">
              <Icon name="search" color="greyDarker" />
            </a>
          </View>
        </Flex>
      </Space>
      <Divider />
    </Fragment>
  );
};

SearchHeader.propTypes = {
  search: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  uiActions: bindActionCreators(uiActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(SearchHeader);
