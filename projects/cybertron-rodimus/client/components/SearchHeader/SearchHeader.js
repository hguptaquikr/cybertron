import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Text } from '../Typography/Typography';
import Ripple from '../Ripple/Ripple';
import BackButton from '../BackButton/BackButton';
import searchService from '../../services/search/searchService';
import * as uiActionCreators from '../../services/ui/uiDuck';
import * as searchActionCreators from '../../services/search/searchDuck';
import './searchHeader.css';

const SearchHeader = ({
  search: {
    searchInput: { place },
    datePicker: { range },
    roomConfig: { rooms },
  },
  uiActions,
}) => {
  const { checkIn, checkOut } = searchService.formattedDatePicker(range, 'D MMM');
  const formattedRoomConfig = searchService.formattedRoomConfig(rooms);

  return (
    <div className="sh" id="t-sh">
      <Ripple>
        <BackButton className="sh__back" id="t-sh-back" />
      </Ripple>
      <Ripple>
        <div className="sh__search" onClick={() => uiActions.showSearchWidget(true)} id="t-sh-search">
          <div className="sh__search__content" id="t-sh-search-content">
            <Text className="sh__q text-truncate">{place.q}</Text>
            <Text type="2" className="sh__info text-truncate">
              {checkIn && checkOut ? `${checkIn} - ${checkOut} / ` : null}
              {formattedRoomConfig.text}
            </Text>
          </div>
          <i className="icon-search sh__search__icon" />
        </div>
      </Ripple>
    </div>
  );
};

SearchHeader.propTypes = {
  search: PropTypes.object.isRequired,
  uiActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  uiActions: bindActionCreators(uiActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchHeader);
