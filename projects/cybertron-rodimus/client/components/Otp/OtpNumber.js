import React from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import Button from '../Button/Button';
import Input from '../Input/Input';
import { Heading } from '../Typography/Typography';

const NumScreen = ({ closeButton, onMobileChanged, onClose }) => (
  <div className="otp">
    <Heading>Enter Mobile Number</Heading>
    <p className="otp__subtitle">
      A One Time Password (OTP) will be  sent via SMS to verify your number.
    </p>
    <Formsy className="checkout__form" onValidSubmit={onMobileChanged}>
      <Input
        type="text"
        name="mobile"
        validations="isNumeric,isLength:10"
        validationError="Please enter a valid mobile number"
        label="Mobile Number"
        required
      />
      <Button
        type="submit"
        block
        className="otp__verify otp__btn"
      >Verify
      </Button>
    </Formsy>
    {
      closeButton ? (
        <Button
          type="button"
          block
          modifier="secondary"
          className="otp__skip otp__btn"
          onClick={onClose}
        >
          {closeButton}
        </Button>
      ) : null
    }
  </div>
);

NumScreen.propTypes = {
  closeButton: PropTypes.string.isRequired,
  onMobileChanged: PropTypes.func.isRequired,
  onClose: PropTypes.func,
};

export default NumScreen;
