import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import Input from '../Input/Input';
import Button from '../Button/Button';
import analyticsService from '../../services/analytics/analyticsService';
import androidService from '../../services/android/androidService';

class OtpScreen extends Component {
  state = {
    androidPassedOtp: '',
  }

  componentDidMount() {
    analyticsService.otpScreenDisplayed();
    androidService.addOtpListener(this);
  }

  render() {
    const {
      mobile,
      onEditMobile,
      onClose,
      onResendOtp,
      closeButton,
      onVerifyOtp,
      isVerifyingOtp,
    } = this.props;

    const { androidPassedOtp } = this.state;

    return (
      <div className="otp">
        <p className="otp__subtitle" onClick={onEditMobile}>
          Please enter the verification code (OTP) sent to +91 <strong>{mobile}</strong>
          <i className="otp__edit icon-edit" />
        </p>
        <Formsy className="checkout__form" onValidSubmit={onVerifyOtp}>
          <Input
            type="tel"
            name="otp"
            value={androidPassedOtp}
            validations="isNumeric"
            validationError="Please enter a valid OTP"
            label="OTP"
            required
          />
          <p type="2" className="otp__text">
            Not received your code?
            <span className="otp__action text--primary" onClick={onResendOtp}>
              Resend Code
            </span>
          </p>
          <Button
            block
            type="submit"
            className="otp__btn otp__verify"
            disabled={isVerifyingOtp}
          >
            { isVerifyingOtp ? 'Verifying ...' : 'Verify' }
          </Button>
          {
            closeButton ? (
              <Button
                type="button"
                block
                modifier="secondary"
                className="otp__btn otp__skip"
                onClick={onClose}
              >
                {closeButton}
              </Button>
            ) : null
          }
        </Formsy>
      </div>
    );
  }
}

OtpScreen.propTypes = {
  onResendOtp: PropTypes.func.isRequired,
  onVerifyOtp: PropTypes.func.isRequired,
  onClose: PropTypes.func,
  onEditMobile: PropTypes.func.isRequired,
  mobile: PropTypes.string,
  closeButton: PropTypes.string.isRequired,
  isVerifyingOtp: PropTypes.bool.isRequired,
};

export default OtpScreen;
