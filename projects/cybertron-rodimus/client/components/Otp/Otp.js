import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from '../Modal/Modal';
import OtpVerify from './OtpVerify';
import OtpNumber from './OtpNumber';
import analyticsService from '../../services/analytics/analyticsService';
import './otp.css';

class Otp extends Component {
  state = {
    flow: 'enterOtp',
  }

  onClose = () => {
    this.setState({ flow: 'enterOtp' });
    this.props.onClose();
  }

  onEditMobile = () => {
    this.setState({ flow: 'changeMobile' });
  }

  onMobileChanged = (form, reset, invalidate) => {
    const {
      mobile,
      onMobileChanged,
    } = this.props;

    onMobileChanged(form.mobile)
      .then((res) => {
        if (res.error) invalidate({ mobile: res.payload.errors[0].message });
        else {
          analyticsService.otpSent(mobile);
          this.setState({ flow: 'enterOtp' });
        }
      });
    analyticsService.changeNumberClicked(mobile, form.mobile);
  }

  render() {
    const { flow } = this.state;
    const {
      isOpen,
      onClose,
      closeButton,
      mobile,
      onResendOtp,
      onVerifyOtp,
      isVerifyingOtp,
    } = this.props;

    return (
      <Modal
        isOpen={isOpen}
        center
        label="OTP"
        onClose={this.onClose}
      >
        {
          flow === 'enterOtp' ? (
            <OtpVerify
              onVerifyOtp={onVerifyOtp}
              onResendOtp={onResendOtp}
              onEditMobile={this.onEditMobile}
              onClose={onClose}
              mobile={mobile}
              closeButton={closeButton}
              isVerifyingOtp={isVerifyingOtp}
            />
          ) : null
        }
        {
          flow === 'changeMobile' ? (
            <OtpNumber
              onMobileChanged={this.onMobileChanged}
              onClose={onClose}
              closeButton={closeButton}
            />
          ) : null
        }
      </Modal>
    );
  }
}

Otp.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  closeButton: PropTypes.string,
  mobile: PropTypes.string,
  onClose: PropTypes.func,
  onResendOtp: PropTypes.func.isRequired,
  onMobileChanged: PropTypes.func.isRequired,
  onVerifyOtp: PropTypes.func.isRequired,
  isVerifyingOtp: PropTypes.bool.isRequired,
};

Otp.defaultProps = {
  onClose: () => {},
  closeButton: '',
};

export default Otp;
