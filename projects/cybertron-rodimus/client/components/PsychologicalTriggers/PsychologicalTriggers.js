import React from 'react';
import PropTypes from 'prop-types';
import Flex from 'leaf-ui/cjs/Flex/web';
import Space from 'leaf-ui/cjs/Space/web';
import Text from 'leaf-ui/cjs/Text/web';
import View from 'leaf-ui/cjs/View/web';

const HURRY = 'hurry';
const PRICE_DROP = 'priceDrop';
const DEMAND = 'demand';

class PsychologicalTriggers extends React.Component {
  renderIcon = (type) => {
    switch (type) {
      case HURRY: return (
        <svg width="16px" height="16px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
          <defs />
          <g id="high-demand" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <polygon id="Shape" fill="#FAA31A" fillRule="nonzero" points="19 9 12.7 9 17 2 10 2 5 12 10 12 6 22" />
          </g>
        </svg>
      );

      case PRICE_DROP: return (
        <svg width="16px" height="16px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
          <defs />
          <g id="price-drop" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <path d="M22.0421436,8.6 C21.7421436,7.6 19.7421436,7.4 19.1421436,6.5 C18.5421436,5.6 18.9421436,3.7 18.0421436,3.1 C17.1421436,2.5 15.4421436,3.4 14.4421436,3.1 C13.4421436,2.8 12.6421436,1 11.5421436,1 C10.4421436,1 9.6421436,2.8 8.6421436,3.1 C7.6421436,3.4 5.9421436,2.5 5.0421436,3.1 C4.1421436,3.7 4.5421436,5.6 3.9421436,6.5 C3.3421436,7.4 1.4421436,7.6 1.0421436,8.6 C0.7421436,9.6 2.1421436,10.9 2.1421436,12 C2.1421436,13.1 0.7421436,14.4 1.0421436,15.4 C1.3421436,16.4 3.3421436,16.6 3.9421436,17.5 C4.5421436,18.4 4.1421436,20.3 5.0421436,20.9 C5.9421436,21.5 7.5421436,20.6 8.6421436,20.9 C9.6421436,21.2 10.4421436,23 11.5421436,23 C12.6421436,23 13.4421436,21.2 14.4421436,20.9 C15.4421436,20.6 17.1421436,21.5 18.0421436,20.9 C18.9421436,20.3 18.5421436,18.4 19.1421436,17.5 C19.7421436,16.6 21.6421436,16.4 22.0421436,15.4 C22.3421436,14.4 20.9421436,13.1 20.9421436,12 C20.9421436,10.9 22.4421436,9.6 22.0421436,8.6 Z M8.1421436,6.3 C9.2421436,6.3 10.1421436,7.2 10.1421436,8.3 C10.1421436,9.4 9.2421436,10.3 8.1421436,10.3 C7.0421436,10.3 6.1421436,9.4 6.1421436,8.3 C6.1421436,7.2 7.0421436,6.3 8.1421436,6.3 Z M8.3421436,16.7 C8.1421436,16.9 7.8421436,17 7.6421436,17 C7.4421436,17 7.1421436,16.9 6.9421436,16.7 C6.5421436,16.3 6.5421436,15.7 6.9421436,15.3 L14.9421436,7.3 C15.3421436,6.9 15.9421436,6.9 16.3421436,7.3 C16.7421436,7.7 16.7421436,8.3 16.3421436,8.7 L8.3421436,16.7 Z M15.1421436,17.5 C14.0421436,17.5 13.1421436,16.6 13.1421436,15.5 C13.1421436,14.4 14.0421436,13.5 15.1421436,13.5 C16.2421436,13.5 17.1421436,14.4 17.1421436,15.5 C17.1421436,16.6 16.2421436,17.5 15.1421436,17.5 Z" id="Shape" fill="#FAA31A" fillRule="nonzero" />
          </g>
        </svg>
      );

      case DEMAND: return (
        <svg width="16px" height="16px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
          <defs />
          <g id="people-are-viewing" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <path d="M17,10.5 C19.8,10.5 22.1,12.4 22.1,14.7 L22.1,18.5 L16.1,18.5 L16.1,14.7 C16.1,13.1 15.3,11.6 14,10.5 L17,10.5 Z M14.1,14.7 L14.1,18.5 L2.1,18.5 L2.1,14.7 C2.1,12.4 4,10.5 6.4,10.5 L9.8,10.5 C10.4,10.5 11,10.6 11.6,10.9 C13.1,11.6 14.1,13 14.1,14.7 Z M8.1,8.5 C6.5,8.5 5.1,7.2 5.1,5.5 C5.1,3.8 6.4,2.5 8.1,2.5 C9.8,2.5 11.1,3.8 11.1,5.5 C11.1,7.2 9.8,8.5 8.1,8.5 Z M16.1,8.5 C14.5,8.5 13.1,7.2 13.1,5.5 C13.1,3.8 14.4,2.5 16.1,2.5 C17.8,2.5 19.1,3.8 19.1,5.5 C19.1,7.2 17.8,8.5 16.1,8.5 Z M21.1,20.5 C21.7,20.5 22.1,20.9 22.1,21.5 C22.1,22.1 21.7,22.5 21.1,22.5 L3.1,22.5 C2.5,22.5 2.1,22.1 2.1,21.5 C2.1,20.9 2.5,20.5 3.1,20.5 L21.1,20.5 Z" id="Combined-Shape" fill="#FAA31A" fillRule="nonzero" />
          </g>
        </svg>
      );
      default:
        return '';
    }
  }
  render() {
    const { trigger, className } = this.props;
    return (
      <Flex
        className={className}
        flexDirection="row"
        alignItems="center"
      >
        <View>
          {this.renderIcon(trigger.type)}
          <Space margin={[0]} padding={[0, 0, 0, 1]}>
            <Text size="xs" color="greyDarker">
              {trigger.text}
            </Text>
          </Space>
        </View>
      </Flex>
    );
  }
}

PsychologicalTriggers.propTypes = {
  trigger: PropTypes.object.isRequired,
  className: PropTypes.string,
};

PsychologicalTriggers.defaultProps = {
  className: '',
};

export default PsychologicalTriggers;
