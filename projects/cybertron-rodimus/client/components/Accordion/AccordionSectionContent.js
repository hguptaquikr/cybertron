import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

class AccordionSectionContent extends Component {
  state = {}

  render() {
    const { children, isOpen, className } = this.props;
    return (
      <div
        className={cx('accordion__content', {
          hide: !isOpen,
          [className]: className,
        })}
      >
        {children}
      </div>
    );
  }
}

AccordionSectionContent.propTypes = {
  children: PropTypes.node.isRequired,
  isOpen: PropTypes.bool,
  className: PropTypes.string,
};

AccordionSectionContent.defaultProps = {
  className: '',
  isOpen: false,
};

export default AccordionSectionContent;
