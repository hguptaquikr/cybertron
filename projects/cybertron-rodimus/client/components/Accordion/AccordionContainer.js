import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import AccordionSection from './AccordionSection';

/*
  == TODO ==
  -> add disabled attribute to Section
  -> allow Accordion to render other tags as well
  -> classNames on Accordion, Section, Title, Content
  -> enhance title to have two states - one when section is open and one when closed
  -> animations?
*/

const getSelectedSection = (props) =>
  React.Children.map(props.children, (child, index) => (
    child && child.props.isOpen ? index : null
  )).find((index) => index !== null);

class AccordionContainer extends Component {
  state = {
    selectedSection: this.props.children ? getSelectedSection(this.props) : null,
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedSection: nextProps.children ? getSelectedSection(nextProps) : null,
    });
  }

  handleClick = (i) => (cb) => {
    const { selectedSection } = this.state;
    this.setState({
      selectedSection: selectedSection !== i ? i : null,
    }, cb);
  }

  render() {
    const { selectedSection } = this.state;
    const { children, className } = this.props;
    return (
      <div className={cx('accordion', {
        [className]: className,
      })}
      >
        {
          React.Children.map(children, (child, i) => (
            child && child.type === AccordionSection ? (
              React.cloneElement(child, {
                isOpen: selectedSection === i,
                handleClick: this.handleClick(i),
              })
            ) : null
          ))
        }
      </div>
    );
  }
}

AccordionContainer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.object,
  ]),
  className: PropTypes.string,
};

export default AccordionContainer;
