import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

class AccordionSectionTitle extends Component {
  state = {}
  titleClicked = (sectionTitleClicked, onClick) => () => {
    sectionTitleClicked();
    if (onClick) onClick();
  }

  render() {
    const { children, isOpen, sectionTitleClicked, onClick, className } = this.props;
    return (
      <div
        onClick={this.titleClicked(sectionTitleClicked, onClick)}
        className={cx('accordion__title', {
          [className]: className,
        })}
      >
        <div className="truncate">
          {children}
        </div>
        <i
          className={cx('accordion__icon icon-angle-down', {
            'animation-rotate': isOpen,
          })}
        />
      </div>
    );
  }
}

AccordionSectionTitle.propTypes = {
  children: PropTypes.node.isRequired,
  isOpen: PropTypes.bool,
  sectionTitleClicked: PropTypes.func,
  className: PropTypes.string,
  onClick: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func,
  ]),
};

AccordionSectionTitle.defaultProps = {
  className: '',
  onClick: '',
  isOpen: false,
};

export default AccordionSectionTitle;
