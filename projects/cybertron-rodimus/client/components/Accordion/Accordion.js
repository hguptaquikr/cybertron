import AccordionContainer from './AccordionContainer';
import AccordionSection from './AccordionSection';
import AccordionSectionTitle from './AccordionSectionTitle';
import AccordionSectionContent from './AccordionSectionContent';
import './accordion.css';

export default {
  Container: AccordionContainer,
  Section: AccordionSection,
  Title: AccordionSectionTitle,
  Content: AccordionSectionContent,
};
