/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import AccordionSectionTitle from './AccordionSectionTitle';
import AccordionSectionContent from './AccordionSectionContent';
import { browserDimensions, scrollIntoView } from '../../services/utils';

class AccordionSection extends Component {
  storeAccordionSectionRef = (ref) => {
    if (ref) this.accordionSectionRef = ref;
  }

  sectionTitleClicked = () => {
    const { handleClick } = this.props;
    const halfScreenHeight = browserDimensions().height / 2;
    const originalViewPosition = this.accordionSectionRef.getBoundingClientRect().top;
    handleClick(() => {
      const newViewPosition = this.accordionSectionRef.getBoundingClientRect().top;
      if (originalViewPosition > halfScreenHeight) {
        scrollIntoView(this.accordionSectionRef, 100 - halfScreenHeight);
      } else if (newViewPosition < 0) {
        scrollIntoView(this.accordionSectionRef, -originalViewPosition);
      }
    });
  }

  render() {
    const {
      isOpen,
      children,
      className,
    } = this.props;
    return (
      <div
        ref={this.storeAccordionSectionRef}
        className={cx('accordion__section', {
          [className]: className,
        })}
        id="t-accordion-section"
      >
        {
          React.Children.map(children, (child) => (
            child
              && (child.type === AccordionSectionTitle
                || child.type === AccordionSectionContent) ? (
                React.cloneElement(child, {
                  isOpen,
                  sectionTitleClicked: this.sectionTitleClicked,
                })
              ) : null
          ))
        }
      </div>
    );
  }
}

AccordionSection.propTypes = {
  handleClick: PropTypes.func,
  isOpen: PropTypes.bool,
  children: PropTypes.arrayOf(PropTypes.node),
  className: PropTypes.string,
};

AccordionSection.defaultProps = {
  className: '',
  isOpen: false,
};

export default AccordionSection;
