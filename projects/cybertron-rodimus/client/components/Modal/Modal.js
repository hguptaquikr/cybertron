import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Portal from 'react-portal';
import qs from 'query-string';
import { withRouter } from 'react-router-dom';
import cx from 'classnames';
import { browserDimensions } from '../../services/utils';
import './modal.css';

const applicationElement = __BROWSER__ && document.getElementById('root');

// FixMe move me to modalcontent class
class ModalStack {
  constructor() {
    this.stack = [];
    this.listener = '';
  }

  getModal = (modalId) =>
    this.stack.find((modal) => (modal.modalId === modalId));

  hideModal = () => {
    const topModal = this.stack.pop();
    if (topModal) {
      if (!topModal.backButtonPressed) topModal.onClose();
      this.unsubscribeListener();
    }
  }

  pushModal = (modalId, onClose) =>
    this.stack.push({ modalId, onClose, backButtonPressed: false });

  unsubscribeListener = () => {
    if (this.stack.length === 0) {
      this.listener();
      this.listener = '';
      document.body.classList.remove('modal--open');
    }
  }
}

const CloseIcon = ({ closeIcon, history }) => {
  const iconClassMap = {
    back: 'arrow-left',
    close: 'close',
  };

  return closeIcon ? (
    <i className={`modal__icon icon-${iconClassMap[closeIcon]}`} onClick={history.goBack} />
  ) : null;
};

CloseIcon.propTypes = {
  closeIcon: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
};

class ModalContent extends Component {
  static modalStack = new ModalStack();

  componentDidMount() {
    const {
      history,
      location,
      onClose,
    } = this.props;
    const { modalStack } = this.constructor;
    let historyLocationUpdateKey = '';
    this.modalId = +(new Date());
    applicationElement.setAttribute('aria-hidden', 'true');
    document.body.classList.add('modal--open');
    history.push({
      pathname: location.pathname,
      search: qs.stringify({
        ...qs.parse(location.search),
        modal: this.modalId,
      }),
    });
    modalStack.pushModal(this.modalId, onClose);
    if (!modalStack.listener) {
      modalStack.listener = history.listen((currentLocation, action) => {
        if (action === 'POP' && currentLocation.key !== historyLocationUpdateKey) {
          modalStack.hideModal();
          historyLocationUpdateKey = currentLocation.key;
        } else if (action === 'PUSH' && !currentLocation.search.includes('modal')) {
          modalStack.hideModal();
        }
      });
    }
  }

  componentWillUnmount() {
    const {
      modalId,
    } = this;
    const { modalStack } = this.constructor;
    applicationElement.removeAttribute('aria-hidden');
    const modal = modalStack.getModal(modalId);
    if (modal) {
      modal.backButtonPressed = true;
    }
  }

  preventClickPropogation = (e) => {
    e.stopPropagation();
  }

  render() {
    const {
      children,
      backdropClassName,
      modalClassName,
      bodyClassName,
      headerClassName,
      header,
      footer,
      closeIcon,
      onClose,
      role,
      label,
      center,
      bottom,
      scroll,
      history,
    } = this.props;

    return (
      <div
        className={cx('modal', {
          'modal--center': center,
        }, backdropClassName)}
        onClick={() => {
          history.goBack();
          onClose();
        }}
      >
        <div
          className={cx('modal__dialog', {
            'modal__dialog--center': center,
            'modal__dialog--bottom': bottom,
          }, modalClassName)}
          tabIndex="-1"
          role={role}
          aria-label={label}
          onClick={this.preventClickPropogation}
        >
          {
            header || closeIcon ? (
              <div className="modal__header">
                <CloseIcon closeIcon={closeIcon} onClose={onClose} history={history} />
                <div className={cx('modal__header-cont', headerClassName)}>
                  {header}
                </div>
              </div>
            ) : null
          }

          <div
            className="modal__body"
            style={{
              overflowY: scroll ? 'scroll' : 'hidden',
              height: center || bottom ? 'auto' : browserDimensions().height,
            }}
          >
            <div className={cx('modal__ios-hack-cont', bodyClassName)}>
              {children}
            </div>
          </div>
          <div className="modal__footer">{footer}</div>
        </div>
      </div>
    );
  }
}

ModalContent.propTypes = {
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func.isRequired,
  backdropClassName: PropTypes.string,
  modalClassName: PropTypes.string,
  bodyClassName: PropTypes.string,
  headerClassName: PropTypes.string,
  center: PropTypes.bool,
  bottom: PropTypes.bool,
  role: PropTypes.string,
  label: PropTypes.string,
  header: PropTypes.node,
  footer: PropTypes.node,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  closeIcon: PropTypes.oneOf(['', 'back', 'close']),
  scroll: PropTypes.bool.isRequired,
};

ModalContent.defaultProps = {
  backdropClassName: '',
  modalClassName: '',
  role: 'dialog',
  closeIcon: '',
  scroll: true,
};

const Modal = ({ isOpen, ...props }) => (
  <Portal isOpened={isOpen}>
    <ModalContent {...props} />
  </Portal>
);

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
};

export default withRouter(Modal);
