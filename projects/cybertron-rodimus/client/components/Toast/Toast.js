import React, { Component } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './toast.css';

class Toast extends Component {
  state = {
    label: '',
    visible: false,
  };

  componentWillReceiveProps(nextProps) {
    this.showToast(nextProps.toast);
  }

  showToast = (toast) => {
    const { label, timeout } = toast;
    this.hideToast();
    this.setState({ label, visible: true });
    this.toastTimer = setTimeout(() => this.hideToast(), timeout);
  }

  hideToast = () => {
    const { label } = this.state;
    if (label) {
      clearTimeout(this.toastTimer);
      this.toastTimer = null;
      this.setState({ visible: false });
    }
  }

  render() {
    const { label, visible } = this.state;
    const { toast } = this.props;
    return (
      <div
        className={cx({
          toast__capsular: toast.shape === 'capsular',
          toast__sharpEdged: toast.shape === 'sharpEdged',
          'toast--capsular-visible': toast.shape === 'capsular' && visible,
          'toast--sharpEdged-visible': toast.shape === 'sharpEdged' && visible,
        })}
      >
        {label}
      </div>
    );
  }
}

Toast.propTypes = {
  toast: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  toast: state.toast,
});

export default connect(
  mapStateToProps,
)(Toast);
