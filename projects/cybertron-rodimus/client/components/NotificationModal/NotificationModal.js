import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import Modal from '../Modal/Modal';
import Button from '../Button/Button';
import { Heading, Subheading } from '../Typography/Typography';
import './notificationModal.css';

class NotificationModal extends Component {
  errorTypes = {
    soldOut: {
      title: 'Sorry!',
      imageUrl: '//images.treebohotels.com/images/payment/soldout.svg',
    },
    error: {
      title: 'Payment Failed',
      imageUrl: '//images.treebohotels.com/images/payment/payment-error.svg',
    },
  };

  render() {
    const { show, message, onRetry, onClose, type, redirectUrl } = this.props;
    const {
      title = this.props.title,
      imageUrl = this.props.imageUrl,
    } = this.errorTypes[type] || {};

    return (
      <div className="notification-modal">
        <Modal
          isOpen={show}
          label="Payment error"
          onClose={onClose}
          closeIcon="close"
          bodyClassName="notification-modal__cont"
          scroll={false}
        >
          <div className="notification-modal__info text-center">
            <img
              className="old-image notification-modal__img"
              src={imageUrl || this.errorTypes.error.imageUrl}
              alt="Payment error"
            />
            <Heading className="notification-modal__title">
              {title || this.errorTypes.error.title}
            </Heading>
            <Subheading tag="p" className="text--error">{message}</Subheading>
          </div>
          {
            isEmpty(redirectUrl) ? (
              <Button
                block
                large
                modifier="secondary"
                className="notification-modal__action"
                onClick={onRetry || onClose}
              >
                Retry
              </Button>
            ) : (
              <Link to={redirectUrl}>
                <Button
                  block
                  large
                  modifier="secondary"
                  className="notification-modal__action"
                >
                  SEARCH AGAIN
                </Button>
              </Link>
            )
          }
        </Modal>
      </div>
    );
  }
}

NotificationModal.propTypes = {
  title: PropTypes.string,
  imageUrl: PropTypes.string,
  show: PropTypes.bool,
  message: PropTypes.string,
  redirectUrl: PropTypes.object,
  onClose: PropTypes.func.isRequired,
  onRetry: PropTypes.func,
  type: PropTypes.oneOf(['error', 'soldOut']),
};

NotificationModal.defaultProps = {
  show: false,
};

export default NotificationModal;
