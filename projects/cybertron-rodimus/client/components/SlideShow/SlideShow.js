import React, { Component } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import Image from '../Image/Image';
import { Text } from '../Typography/Typography';
import './slideShow.css';

class SlideShow extends Component {
  state = {
    current: 0,
  }

  componentDidUpdate(prevProps) {
    const { props } = this;
    if (get(prevProps, 'images[1].url') !== get(props, 'images[1].url')) {
      this.prefetchImage(get(props, 'images[1]', {}));
    }
  }

  prefetchImage = (image) => {
    const { aspectRatio } = this.props;
    (new window.Image()).src = Image.getCdnUrl(image.url, aspectRatio);
  }

  showNext = () => {
    this.setState(({ current }, { images }) => {
      const totalImages = images.length;
      const newCurrent = current + 1 >= totalImages ? totalImages - 1 : current + 1;
      this.prefetchImage(images[(newCurrent + 1) % totalImages]);
      return { current: newCurrent };
    });
  };

  showPrev = () => {
    this.setState(({ current }) => ({
      current: current - 1 < 0 ? 0 : current - 1,
    }));
  }

  render() {
    const { images, grayscale, aspectRatio, tagField } = this.props;
    const { current } = this.state;
    const currentImage = images[current] || {};

    return (
      <div className="ss">
        {
          tagField ? (
            <Text className="ss__tag">
              {currentImage[tagField]}
            </Text>
          ) : null
        }
        <Image
          src={currentImage.url}
          alt={currentImage.tagline}
          aspectRatio={aspectRatio}
          grayscale={grayscale}
        />
        <div className="ss__count">{current + 1} / {images.length}</div>
        {
          current !== 0 ? (
            <div className="ss__navigation ss__navigation--left" onClick={this.showPrev}>
              <i className="icon-angle-left ss__arrow" />
            </div>
          ) : null
        }
        {
          current !== images.length - 1 ? (
            <div className="ss__navigation ss__navigation--right" onClick={this.showNext}>
              <i className="icon-angle-right ss__arrow" />
            </div>
          ) : null
        }
      </div>
    );
  }
}

SlideShow.propTypes = {
  images: PropTypes.array,
  aspectRatio: PropTypes.object,
  grayscale: PropTypes.bool,
  tagField: PropTypes.string,
};

SlideShow.defaultProps = {
  images: [],
};

export default SlideShow;
