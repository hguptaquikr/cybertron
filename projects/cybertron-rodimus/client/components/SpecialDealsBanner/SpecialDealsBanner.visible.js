import React, { Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import cx from 'classnames';
import LoginModal from '../../views/Authentication/LoginModal/LoginModal';
import Button from '../Button/Button';
import { Row, Col } from '../Flex/Flex';
import { Text } from '../Typography/Typography';
import './specialDealsBanner.css';

class SpecialDealsBanner extends Component {
  state = {
    isLoginModalOpen: false,
  }

  onLoginSuccess = () => {
    this.showLoginModal(false);
    this.props.onLoginSuccess();
  }

  onLoginModalClose = () => {
    this.showLoginModal(false);
  }

  showLoginModal = (show) => {
    this.setState({ isLoginModalOpen: show });
  }

  render() {
    const { isLoginModalOpen } = this.state;
    const { className, border, auth } = this.props;

    return !auth.isAuthenticated ? (
      <div className={cx('sdb', { 'sdb--border': border }, className)}>
        <Row middle between className="sdb__cont">
          <Col size="9" className="sdb__title">
            <span className="sdb__title__text">Login to get our best rates!</span>
          </Col>
          <Col className="text-right" size="3">
            <Button
              className="sdb__btn"
              modifier="link"
              onClick={() => this.showLoginModal(true)}
            >
              <Text className="sdb__btn__text" tag="span">Login</Text>
            </Button>
          </Col>
        </Row>
        <LoginModal
          isOpen={isLoginModalOpen}
          onLoginSuccess={this.onLoginSuccess}
          onClose={this.onLoginModalClose}
        />
      </div>
    ) : null;
  }
}

SpecialDealsBanner.propTypes = {
  className: PropTypes.string,
  onLoginSuccess: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  border: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

SpecialDealsBanner.defaultProps = {
  border: false,
};

export default compose(
  connect(mapStateToProps),
  withRouter,
)(SpecialDealsBanner);
