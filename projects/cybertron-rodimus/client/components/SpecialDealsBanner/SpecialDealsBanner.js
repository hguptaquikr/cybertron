import Loadable from 'react-loadable';
import importCss from '../../services/importCss';
import * as abService from '../../services/ab/abService';

export default {
  visible: Loadable({
    loader: () => {
      importCss('SpecialDealsBanner.visible');
      return import('./SpecialDealsBanner.visible' /* webpackChunkName: 'SpecialDealsBanner.visible' */);
    },
    loading: () => null,
  }),
  hidden: Loadable({
    loader: () => Promise.resolve(() => null),
    loading: () => null,
  }),
}[abService.getExperiments().memberPricing || 'visible'];
