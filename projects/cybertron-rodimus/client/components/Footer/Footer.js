import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import kebabCase from 'lodash/kebabCase';
import { Text } from '../Typography/Typography';
import Accordion from '../Accordion/Accordion';
import './footer.css';

const footerLinks = [{
  title: 'Company',
  list: [{
    text: 'About',
    href: '/about/',
  }, {
    text: 'Contact',
    href: '/contact/',
  }, {
    text: 'Careers',
    href: '//careers.treebohotels.com',
    target: '_blank',
  }, {
    text: 'Faq',
    href: '/faq/',
  }, {
    text: 'Terms of Service',
    href: '/terms/',
  }, {
    text: 'Privacy Policy',
    href: '/policy/',
  }, {
    text: 'Feedback',
    href: 'mailto:feedback@treebohotels.com',
  }, {
    text: 'Blog',
    href: 'https://blog.treebohotels.com/',
    target: '_blank',
  }],
}, {
  title: 'Business',
  list: [{
    text: 'Join our Network',
    href: '/joinus/',
  }, {
    text: 'Corporate Enquiry',
    href: 'mailto:corporate@treebohotels.com',
  }, {
    text: 'Travel Agents',
    href: 'mailto:ta@treebohotels.com',
  }],
}, {
  title: 'Discover',
  list: [{
    text: 'Alpha',
    href: '//alpha.treebohotels.com/',
    target: '_blank',
    rel: 'noopener noreferrer',
  }, {
    text: 'Friends of Treebo',
    href: '/fot/',
    target: '_blank',
    rel: 'noopener noreferrer',
  }],
}, {
  title: 'Social',
  list: [{
    text: 'Facebook',
    href: 'https://www.facebook.com/TreeboHotels/',
    target: '_blank',
    rel: 'noopener noreferrer',
  }, {
    text: 'Twitter',
    href: 'https://twitter.com/TreeboHotels',
    target: '_blank',
    rel: 'noopener noreferrer',
  }, {
    text: 'linkedin',
    href: 'https://www.linkedin.com/company/treebo-hotels/',
    target: '_blank',
    rel: 'noopener noreferrer',
  }, {
    text: 'Instagram',
    href: 'https://www.instagram.com/treebo_hotels/',
    target: '_blank',
    rel: 'noopener noreferrer',
  }, {
    text: 'Google Plus',
    href: 'https://plus.google.com/101173360836250828959',
    target: '_blank',
    rel: 'noopener noreferrer',
  }],
}];

class Footer extends Component {
  createSections = () => footerLinks.map((link) => ({
    title: link.title,
    content: (
      <ul className="footer-link__list">
        {
          link.list.map(({ href, text, target, rel }) => (
            <li key={text}>
              <Link
                className={`footer-link__item footer-link__${kebabCase(text)}`}
                to={href}
                target={target}
                rel={rel}
              >
                <Text type="2" className="footer-link__text">{text}</Text>
              </Link>
            </li>
          ))
        }
      </ul>
    ),
  }))

  render() {
    const sections = this.createSections();
    return (
      <div className="footer">
        <div className="footer__container">
          <Accordion.Container>
            {
              sections.map((section) => (
                <Accordion.Section>
                  <Accordion.Title>
                    {section.title}
                  </Accordion.Title>
                  <Accordion.Content>
                    {section.content}
                  </Accordion.Content>
                </Accordion.Section>
              ))
            }
          </Accordion.Container>
        </div>
      </div>
    );
  }
}

export default Footer;
