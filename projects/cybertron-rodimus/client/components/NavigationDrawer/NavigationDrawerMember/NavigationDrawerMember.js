import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from '../../Button/Button';
import Ripple from '../../Ripple/Ripple';
import TapSection from '../../TapSection/TapSection';
import { Heading } from '../../Typography/Typography';
import BookingHistoryListPage from '../../../views/BookingHistory/BookingHistoryListPage';
import RewardsTapSection from '../../Rewards/RewardsTapSection/RewardsTapSection';
import * as authActionCreators from '../../../services/auth/authDuck';
import analyticsService from '../../../services/analytics/analyticsService';
import androidService from '../../../services/android/androidService';
import { constants } from '../../../services/utils';
import './navigationDrawerMember.css';

class MemberNavigationDrawer extends Component {
  logoutUser = () => {
    const { onLogout, authActions } = this.props;
    authActions.userLogout()
      .then((res) => {
        if (!res.error) onLogout();
      });
  }

  render() {
    const { auth: { profile } } = this.props;
    return (
      <div className="ndm">
        <Heading className="ndm__header" tag="p">
          {`Hello ${profile.firstName || 'User'}!`}
        </Heading>
        <Ripple eagerLoad={BookingHistoryListPage.preload}>
          <TapSection
            headerClass="nd__ts__title"
            title="Manage Booking"
            titleIcon="icon-calendar"
            border="bottom"
            to="/account/booking-history/"
          />
        </Ripple>
        <RewardsTapSection />
        <TapSection
          headerClass="nd__ts__title"
          title="Contact Us"
          titleIcon="icon-phone"
          border="bottom"
          onClick={() => analyticsService.contactUsClicked('Landing')}
          href={`tel:${constants.customerCareNumber}`}
        />
        {
          androidService.isAndroid ? (
            <TapSection
              headerClass="nd__ts__title"
              title="Treebo Talkies"
              titleIcon="icon-entertainment"
              href={constants.treeboTalkiesUrl}
              target="_blank"
              rel="noreferrer noopener"
            />
          ) : null
        }
        <div className="ndm__footer">
          <Ripple>
            <Button
              block
              modifier="secondary"
              onClick={this.logoutUser}
            >
              Logout
            </Button>
          </Ripple>
        </div>
      </div>
    );
  }
}

MemberNavigationDrawer.propTypes = {
  onLogout: PropTypes.func.isRequired,
  authActions: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  authActions: bindActionCreators(authActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MemberNavigationDrawer);
