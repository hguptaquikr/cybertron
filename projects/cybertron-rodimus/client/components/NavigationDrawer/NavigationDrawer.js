import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import NavigationDrawerGuest from './NavigationDrawerGuest/NavigationDrawerGuest';
import NavigationDrawerMember from './NavigationDrawerMember/NavigationDrawerMember';
import SideDrawer from '../SideDrawer/SideDrawer';
import analyticsService from '../../services/analytics/analyticsService';
import * as packageJson from '../../../package.json';
import './navigationDrawer.css';

class NavigationDrawer extends Component {
  state = {
    isOpen: false,
  }

  onClose = () => {
    this.setState({ isOpen: false });
  }

  onLogout = () => {
    this.onClose();
    analyticsService.logoutClicked();
  }

  openNavigationDrawer = () => {
    this.setState({ isOpen: true });
    analyticsService.navigationIconClicked();
  }

  render() {
    const { auth } = this.props;
    const { isOpen } = this.state;
    return (
      <div className="nd">
        <span className="nd__icon" onClick={this.openNavigationDrawer}>&#9776;</span>
        <SideDrawer isOpen={isOpen} onOverlayClick={this.onClose}>
          {
            auth.isAuthenticated ? (
              <NavigationDrawerMember onLogout={this.onLogout} />
            ) : (
              <NavigationDrawerGuest />
            )
          }
          <div className="nd__version">Version {packageJson.version}</div>
        </SideDrawer>
      </div>
    );
  }
}

NavigationDrawer.propTypes = {
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
)(NavigationDrawer);
