import React from 'react';
import { Link } from 'react-router-dom';
import Button from '../../Button/Button';
import TapSection from '../../TapSection/TapSection';
import Ripple from '../../Ripple/Ripple';
import { Subheading } from '../../Typography/Typography';
import RewardsTapSection from '../../Rewards/RewardsTapSection/RewardsTapSection';
import analyticsService from '../../../services/analytics/analyticsService';
import androidService from '../../../services/android/androidService';
import { constants } from '../../../services/utils';
import LoginPage from '../../../views/Authentication';
import './navigationDrawerGuest.css';

const NavigationDrawerGuest = () => (
  <div className="ndg">
    <div className="ndg__header">
      <div className="nd__container">
        <Subheading className="ndg__title" tag="p">Manage your bookings</Subheading>
        <Subheading className="ndg__subtitle" tag="p">anytime anywhere</Subheading>
        <img className="old-image ndg__img" src="//images.treebohotels.com/images/login-illustration.svg" alt="login" />
        <Ripple eagerLoad={LoginPage.preload}>
          <Link
            to="/login/"
            onClick={() => analyticsService.loginButtonClicked('Landing')}
          >
            <Button block>Login</Button>
          </Link>
        </Ripple>
      </div>
    </div>
    <RewardsTapSection />
    <TapSection
      title="Contact Us"
      titleIcon="icon-phone"
      onClick={() => analyticsService.contactUsClicked('Landing')}
      href={`tel:${constants.customerCareNumber}`}
    />
    {
      androidService.isAndroid ? (
        <TapSection
          title="Treebo Talkies"
          titleIcon="icon-entertainment"
          href={constants.treeboTalkiesUrl}
          target="_blank"
          rel="noreferrer noopener"
        />
      ) : null
    }
  </div>
);

export default NavigationDrawerGuest;
