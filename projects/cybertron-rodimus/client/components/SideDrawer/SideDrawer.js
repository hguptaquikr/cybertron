import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './sideDrawer.css';

const SideDrawer = ({ isOpen, onOverlayClick, children }) => (
  <div>
    <div
      className={cx('sd-overlay', { 'sd-overlay--visible': isOpen })}
      onClick={onOverlayClick}
    />
    <div
      className={cx('sd-drawer', { 'sd-drawer--open': isOpen })}
      onClick={(e) => e.stopPropagation()}
    >
      {children}
    </div>
  </div>
);

SideDrawer.propTypes = {
  children: PropTypes.element.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onOverlayClick: PropTypes.func.isRequired,
};

export default SideDrawer;
