import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { browserDimensions } from '../../services/utils';
import './image.css';

class Image extends Component {
  static getCdnUrl(src = '', aspectRatio = { width: 4, height: 3 }) {
    const width = aspectRatio.width * 100;
    const height = aspectRatio.height * 100;
    return src ? `${src}?w=${width}&h=${height}&fm=pjpg&fit=crop&auto=compress` : src;
  }

  state = {
    src: __BROWSER__ ? '' : this.props.src,
    shouldFetch: !__BROWSER__,
    isLoaded: !__BROWSER__,
  };

  componentDidMount() {
    window.setTimeout(this.handleScroll);
    window.addEventListener('scroll', this.handleScroll, { passive: true });
  }

  componentWillReceiveProps(nextProps) {
    const { shouldFetch } = this.state;
    this.setState({ src: shouldFetch ? nextProps.src : '' });
  }

  componentDidUpdate() {
    const { isLoaded } = this.state;
    if (!isLoaded) {
      window.setTimeout(this.handleScroll);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll, { passive: true });
  }

  onImageLoaded = () => {
    this.setState({ isLoaded: true });
  }

  handleScroll = () => {
    const { isLoaded } = this.state;
    const { src } = this.props;
    const boundingClientRect = this.containerRef.getBoundingClientRect();
    const isInViewport =
      (boundingClientRect.top <= browserDimensions().height && boundingClientRect.bottom >= 0);
    if (!isLoaded && isInViewport) {
      this.setState({ src, shouldFetch: true });
      window.removeEventListener('scroll', this.handleScroll, { passive: true });
    }
  }

  storeContainerRef = (ref) => {
    if (ref) this.containerRef = ref;
  }

  render() {
    const { src, isLoaded } = this.state;
    const {
      children,
      className,
      alt,
      aspectRatio,
      grayscale,
      shouldFetchFromCdn,
    } = this.props;

    const imageContainerStyles = {
      paddingTop: !isLoaded ? `${100 / (aspectRatio.width / aspectRatio.height)}%` : '0',
    };
    const imageContainerClassName = cx('image-container', {
      'image-container--loaded': isLoaded,
    });
    const imageClassName = cx(className, 'image-container__img fadeIn', {
      'image-container__img--grayscale': grayscale,
      'image-container__img--loaded': isLoaded,
    });

    const imageSrc = shouldFetchFromCdn ? Image.getCdnUrl(src, aspectRatio) : src;

    return (
      <div
        style={imageContainerStyles}
        className={imageContainerClassName}
        ref={this.storeContainerRef}
      >
        <img
          className={`old-image ${imageClassName}`}
          src={imageSrc}
          alt={alt}
          onLoad={this.onImageLoaded}
        />
        {children}
      </div>
    );
  }
}

Image.propTypes = {
  children: PropTypes.element,
  className: PropTypes.string,
  src: PropTypes.string,
  aspectRatio: PropTypes.object,
  grayscale: PropTypes.bool,
  alt: PropTypes.string,
  shouldFetchFromCdn: PropTypes.bool,
};

Image.defaultProps = {
  src: '',
  aspectRatio: { width: 4, height: 3 },
  shouldFetchFromCdn: true,
};

export default Image;
