import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { withRouter } from 'react-router-dom';
import Ripple from '../Ripple/Ripple';
import './backButton.css';

class BackButton extends Component {
  onBack = () => {
    const { onBack, history } = this.props;
    if (onBack) {
      return onBack();
    }
    return history.goBack();
  }

  render() {
    const { eagerLoad } = this.props;
    return (
      <Ripple eagerLoad={eagerLoad}>
        <i
          onClick={this.onBack}
          className={cx('back__btn icon-arrow-left', this.props.className)}
        />
      </Ripple>
    );
  }
}

BackButton.propTypes = {
  eagerLoad: PropTypes.func,
  onBack: PropTypes.func,
  history: PropTypes.object,
  className: PropTypes.string,
};

export default withRouter(BackButton);
