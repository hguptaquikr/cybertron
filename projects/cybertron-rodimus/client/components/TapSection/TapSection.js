import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'react-router-dom';
import { Heading, Text } from '../Typography/Typography';
import Ripple from '../Ripple/Ripple';
import './tapSection.css';

const TapSection = ({
  title,
  titleIcon,
  icon,
  subtitle,
  border,
  className,
  headerClass,
  to,
  children,
  ripple,
  href,
  ...props
}) => {
  const newProps = {
    href,
    to,
    className: cx('ts', `ts--border-${border}`, className),
    ...props,
  };
  let newChildren = (
    <div>
      <Heading
        tag="h3"
        type="3"
        className={cx('ts__title', headerClass)}
      >
        {
          titleIcon ? (
            <i className={cx('ts__title__icon', titleIcon)} />
          ) : null
        }
        {title}
        <i className={cx('ts__icon', `icon-angle-${icon}`)} />
      </Heading>
      {
        subtitle ? (
          <Text className="ts__subtitle" type="2">{subtitle}</Text>
        ) : null
      }
      {children}
    </div>
  );
  if (href) {
    newChildren = <a {...newProps}>{newChildren}</a>;
  } else if (to) {
    newChildren = <Link {...newProps}>{newChildren}</Link>;
  } else {
    newChildren = <div {...newProps}>{newChildren}</div>;
  }
  return (
    ripple ? <Ripple>{newChildren}</Ripple> : newChildren
  );
};

TapSection.propTypes = {
  title: PropTypes.node.isRequired,
  titleIcon: PropTypes.node,
  icon: PropTypes.oneOf(['left', 'right', 'up', 'down']),
  subtitle: PropTypes.node,
  border: PropTypes.oneOf(['top', 'bottom', 'both', 'none']),
  className: PropTypes.string,
  headerClass: PropTypes.string,
  onClick: PropTypes.func,
  to: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  children: PropTypes.node,
  ripple: PropTypes.bool,
  href: PropTypes.string,
};

TapSection.defaultProps = {
  border: 'both',
  ripple: false,
  icon: 'right',
};

export default TapSection;
