import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import moment from 'moment';
import { Text } from '../../components/Typography/Typography';
import Button from '../../components/Button/Button';
import Ripple from '../../components/Ripple/Ripple';
import LoadMore from '../../components/LoadMore/LoadMore';
import LoginModal from '../../views/Authentication/LoginModal/LoginModal';
import QnASearch from './QnASearch';
import Question from './Question';
import Answer from './Answer';
import * as authService from '../../services/auth/authService';
import * as toastActionCreators from '../../services/toast/toastDuck';
import * as contentActionCreators from '../../services/content/contentDuck';
import './questionAndAnswer.css';

class QuestionAndAnswer extends Component {
  state = {
    searchText: '',
    isLoginModalOpen: false,

  }

  onChange = (e) => {
    this.setState({
      searchText: e.target.value,
    });
  }

  onLoginModalClose = () => {
    this.setState({
      isLoginModalOpen: false,
    });
  }

  onLoginSuccess = () => {
    const { searchText: question } = this.state;
    this.submitQuestion(question);
    this.setState({
      isLoginModalOpen: false,
      searchText: '',
    });
  }

  onSubmitQuestion = () => {
    const { searchText: question } = this.state;
    if (authService.isLoggedIn()) {
      this.setState({
        searchText: '',
      });
      this.submitQuestion(question);
    } else {
      this.setState({
        isLoginModalOpen: true,
      });
    }
  }

  filterQnAs = () => {
    const { searchText } = this.state;
    const { qna } = this.props;
    return qna.filter(({ question }) => (
      question.text.toLowerCase().includes(searchText)
    )).sort((a, b) => moment(b.question.postedOn, 'DD-MM-YYYY').toDate()
      - moment(a.question.postedOn, 'DD-MM-YYYY').toDate());
  }

  storeQnARef = (ref) => {
    if (ref) {
      this.qnaRef = ref;
    }
  }

  submitQuestion = (question) => {
    const { toastActions, contentActions, location } = this.props;
    contentActions.submitQuestion(question, location.pathname)
      .then(() => toastActions.showToast('Submitted Question', 'capsular'))
      .catch(() => toastActions.showToast('Failed to submit quesiton! Try again.', 'capsular'));
  }

  render() {
    const { searchText, isLoginModalOpen } = this.state;
    const { qna } = this.props;
    const filteredQna = this.filterQnAs();

    return (
      <div className="qna" ref={this.storeQnARef} id="t-qna">
        <QnASearch searchText={searchText} onChange={this.onChange} />
        <LoadMore
          stepper={{ initial: 5, step: 3 }}
          loadMoreText="Load more questions"
          actionClassName="qna__load-more-action"
        >
          {
            filteredQna.map((fqna, index) => (
              <div className="qna__qna-container" key={`fqna${index + 1}`}>
                <Question question={fqna.question} />
                {fqna.answers.map((answer) => (<Answer answer={answer} key={`answer${index + 1}`} />))}
              </div>
            ))
          }
        </LoadMore>
        {
          isEmpty(filteredQna) || isEmpty(qna) ? (
            <div>
              <Text className="qna__no-answers" id="t-qna-no-answers">
                {
                  isEmpty(qna)
                    ? 'Be the first one to ask a question'
                    : 'We were unable to find any existing answers to your question'
                }
              </Text>
              <Ripple>
                <Button
                  onClick={this.onSubmitQuestion}
                  disabled={searchText.split(' ').filter(Boolean).length < 2}
                >
                  Submit Question
                </Button>
              </Ripple>
            </div>
          ) : null
        }
        <LoginModal
          isOpen={isLoginModalOpen}
          onLoginSuccess={this.onLoginSuccess}
          onClose={this.onLoginModalClose}
        />
      </div>
    );
  }
}

QuestionAndAnswer.propTypes = {
  qna: PropTypes.array.isRequired,
  toastActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  toastActions: bindActionCreators(toastActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  connect(null, mapDispatchToProps),
  withRouter,
)(QuestionAndAnswer);
