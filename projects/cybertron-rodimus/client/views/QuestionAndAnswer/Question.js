import React from 'react';
import PropTypes from 'prop-types';
import { Text } from '../../components/Typography/Typography';

export default function Question({ question }) {
  return (
    <div className="qna__question" id="t-qna-question">
      <img
        src="https://images.treebohotels.com/images/question_answer/question-icon.svg"
        alt="Q"
        className="old-image qna__icon"
      />
      <div>
        <Text className="qna__question__text" id="t-qna-question-text">{question.text}</Text>
        <Text type="2">Asked by {question.author}, {question.postedOn}</Text>
      </div>
    </div>
  );
}

Question.propTypes = {
  question: PropTypes.object.isRequired,
};
