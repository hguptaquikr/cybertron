import React from 'react';
import PropTypes from 'prop-types';
import { Text } from '../../components/Typography/Typography';

export default function Answer({ answer }) {
  return (
    <div className="qna__answer">
      <img
        src="https://images.treebohotels.com/images/question_answer/answer-icon.svg"
        alt="A"
        className="qna__icon"
      />
      <div>
        <Text
          className="qna__answer__text"
          dangerouslySetInnerHTML={{ __html: answer.text }}
        />
        <Text type="2">
          {
            answer.isVerified ? (
              <img
                src="https://images.treebohotels.com/images/question_answer/verified-icon.svg"
                alt="v"
                className="old-image qna__icon qna__icon--verified"
              />
            ) : null
          }
          Response by {answer.author}, {answer.postedOn}
        </Text>
      </div>
    </div>
  );
}

Answer.propTypes = {
  answer: PropTypes.object.isRequired,
};
