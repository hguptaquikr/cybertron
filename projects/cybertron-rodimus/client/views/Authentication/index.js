import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('LoginPage');
    return import('./LoginPage/LoginPage' /* webpackChunkName: 'LoginPage' */);
  },
  loading: () => null,
});
