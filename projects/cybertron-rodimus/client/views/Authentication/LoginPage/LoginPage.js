import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import qs from 'query-string';
import PageHeader from '../../../components/PageHeader/PageHeader';
import LandingPage from '../../LandingPage';
import LoginFlow from '../LoginFlow/LoginFlow';
import * as contentActionCreators from '../../../services/content/contentDuck';
import * as toastActionCreators from '../../../services/toast/toastDuck';
import analyticsService from '../../../services/analytics/analyticsService';
import androidService from '../../../services/android/androidService';
import * as routeService from '../../../services/route/routeService';

class LoginPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    return Promise.all(promises);
  }

  state = {
    flow: 'Mobile',
  }

  componentDidMount() {
    const { contentActions, route, match } = this.props;
    contentActions.getSeoContent(route, match.params);
    LandingPage.preload();
  }

  onLoginRedirectMap = {
    menu: '/rewards/',
  }

  onBackButtonClick = () => {
    analyticsService.loginModalClosed(this.state.flow);
  }

  onFlowChange = (flow) => {
    this.setState({ flow });
  }

  onLoginSuccess = (profile) => {
    const { history, toastActions } = this.props;
    const { flow } = qs.parse(history.location.search);
    setTimeout(() => {
      const { firstName, email, phone } = profile;
      const toastLabel = firstName || email || phone;
      toastActions.showToast(`Logged in as ${toastLabel}`, 'capsular');
    }, 700);
    if (androidService.isAndroid) {
      routeService.followRedirectUrl({ user: profile.userId });
    }
    if (this.onLoginRedirectMap[flow]) {
      history.push(this.onLoginRedirectMap[flow]);
    } else {
      history.push('/');
    }
  }

  render() {
    return (
      <div>
        <PageHeader onBack={this.onBackButtonClick} />
        <LoginFlow
          onFlowChange={this.onFlowChange}
          onLoginSuccess={this.onLoginSuccess}
        />
      </div>
    );
  }
}

LoginPage.propTypes = {
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  toastActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,

};

const mapDisptachToProps = (dispatch) => ({
  toastActions: bindActionCreators(toastActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  null,
  mapDisptachToProps,
)(LoginPage);
