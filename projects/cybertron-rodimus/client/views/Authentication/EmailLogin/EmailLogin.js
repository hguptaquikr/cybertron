import React, { Component } from 'react';
import Formsy from 'formsy-react';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Button from '../../../components/Button/Button';
import Input from '../../../components/Input/Input';
import Ripple from '../../../components/Ripple/Ripple';
import * as authActionCreators from '../../../services/auth/authDuck';
import analyticsService from '../../../services/analytics/analyticsService';
import './emailLogin.css';

class EmailScreen extends Component {
  state = {
    isCredentialsValid: true,
    errorMsg: '',
    showPassword: false,
  }

  onUserLogin = (credentials) => {
    const {
      onLoginSuccess,
      authActions: { emailLogin },
    } = this.props;

    emailLogin(credentials)
      .then((res) => {
        if (res.error) {
          this.setState({
            isCredentialsValid: false,
            errorMsg: res.payload.errors[0].message,
          });
        } else onLoginSuccess(res.payload.profile);
      })
      .catch(() => this.setState({ isCredentialsValid: false }));
  }

  onShowMobileNumberLogin = () => {
    const { onShowMobileNumberLogin } = this.props;
    onShowMobileNumberLogin();
    analyticsService.continueWithMobileClicked();
  }

  togglePasswordVisibility = () => {
    const { showPassword } = this.state;
    this.setState({ showPassword: !showPassword });
  }

  render() {
    const { errorMsg, isCredentialsValid, showPassword } = this.state;
    const { onShowForgotPasswordScreen } = this.props;
    return (
      <div className="el">
        <Formsy className="el__email-login" onValidSubmit={this.onUserLogin}>
          <Input
            type="text"
            name="email"
            validations="isEmail"
            validationError="Please enter a valid email"
            label="Email"
            required
          />
          <Input
            type={showPassword ? 'text' : 'password'}
            name="password"
            label="Password"
            required
          />
          <i
            className={cx('el__sh-password', {
              'icon-visibility-on': !showPassword,
              'icon-visibility-off': showPassword,
            })}
            onClick={this.togglePasswordVisibility}
          />
          <div className="floaty">
            {
              !isCredentialsValid ? (
                <div
                  className="lf__error text-center"
                  onClick={this.onShowMobileNumberLogin}
                >
                  <p className="lf__error__text">{errorMsg}.
                    <span className="text--link"> Continue with mobile no.</span>
                  </p>
                </div>
              ) : (null)
            }
            <Button
              block
              flat
              large
              type="submit"
            >Get Started
            </Button>
          </div>
        </Formsy>
        <Ripple>
          <div
            className="el__forgot-password"
            onClick={onShowForgotPasswordScreen}
          >
            Forgot your password?
          </div>
        </Ripple>

      </div>
    );
  }
}

EmailScreen.propTypes = {
  authActions: PropTypes.object.isRequired,
  onLoginSuccess: PropTypes.func.isRequired,
  onShowForgotPasswordScreen: PropTypes.func.isRequired,
  onShowMobileNumberLogin: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  authActions: bindActionCreators(authActionCreators, dispatch),
});

export default compose(
  connect(null, mapDispatchToProps),
)(EmailScreen);
