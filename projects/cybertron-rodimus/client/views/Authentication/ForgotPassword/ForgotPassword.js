import React, { Component } from 'react';
import Formsy from 'formsy-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Button from '../../../components/Button/Button';
import Input from '../../../components/Input/Input';
import { Subheading } from '../../../components/Typography/Typography';
import * as authActionCreators from '../../../services/auth/authDuck';
import * as toastActionCreators from '../../../services/toast/toastDuck';
import './forgotPassword.css';

class ForgotPassword extends Component {
  state = {
    showSuccessMsg: false,
    showErrorMsg: false,
  }

  onForgotPassword = ({ email }) => {
    const { authActions } = this.props;
    authActions.forgotPassword(email)
      .then((res) => {
        if (res.error) this.showErrorMsg();
        else this.showSuccessMsg();
      });
  }

  onEditEmail = () => {
    this.setState({
      showSuccessMsg: false,
      showErrorMsg: false,
    });
  }

  resendEmail = () => {
    const {
      auth: { profile },
      authActions,
      toastActions,
    } = this.props;
    authActions.forgotPassword(profile.email)
      .then((res) => {
        if (res.error) this.showErrorMsg();
        else toastActions.showToast('Email Sent', 'capsular');
      });
  }

  showErrorMsg = () => {
    this.setState({
      showSuccessMsg: false,
      showErrorMsg: true,
    });
  }

  showSuccessMsg = () => {
    this.setState({
      showSuccessMsg: true,
      showErrorMsg: false,
    });
  }

  render() {
    const { auth: { profile } } = this.props;
    const { showSuccessMsg, showErrorMsg } = this.state;
    return (
      <div className="forgot-pass">
        {
          showSuccessMsg ? (
            <div className="forgot-pass__reset-link">
              <Subheading className="forgot-pass__reset-link__title forgot-pass__text text-center">
                An email with a link to reset your password
                  has been sent to <strong>{profile.email}</strong>.
                <span onClick={this.onEditEmail} className="text--link"> Edit.</span>
              </Subheading>
              <Subheading
                className="forgot-pass__text text-center"
                onClick={this.resendEmail}
              >
                Didn’t receive the email?<span className="text--link"> Resend.</span>
              </Subheading>
            </div>
          ) : (
            <div>
              <Subheading className="forgot-pass__text">
                Please enter your registered email to reset your password
              </Subheading>
              <Formsy onValidSubmit={this.onForgotPassword}>
                <Input
                  type="text"
                  name="email"
                  validations="isEmail"
                  validationError="Please enter a valid email"
                  label="Email"
                  required
                />
                <Button
                  block
                  flat
                  large
                  type="submit"
                >SEND EMAIL
                </Button>
              </Formsy>
              {
                showErrorMsg ? (
                  <div className="floaty">
                    <div className="lf__error text-center">
                      <span className="lf__error__text">
                        Error sending email. Try again
                      </span>
                    </div>
                  </div>
                ) : null
              }
            </div>
          )
        }
      </div>
    );
  }
}

ForgotPassword.propTypes = {
  auth: PropTypes.object.isRequired,
  authActions: PropTypes.object.isRequired,
  toastActions: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  authActions: bindActionCreators(authActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgotPassword);
