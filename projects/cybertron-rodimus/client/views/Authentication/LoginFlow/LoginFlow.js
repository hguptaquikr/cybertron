import React, { Component } from 'react';
import PropTypes from 'prop-types';
import EmailLogin from '../EmailLogin/EmailLogin';
import ForgotPassword from '../ForgotPassword/ForgotPassword';
import MobileLogin from '../MobileLogin/MobileLogin';
import analyticsService from '../../../services/analytics/analyticsService';
import './loginFlow.css';

class LoginFlow extends Component {
  state = {
    flow: 'Mobile',
  }

  onFlowChange = (flow) => () => {
    this.setState({ flow });
    this.props.onFlowChange(flow);
  }

  onLoginSuccess = (profile, flow = this.state.flow) => {
    const { onLoginSuccess } = this.props;
    analyticsService.loginSuccess(flow);
    analyticsService.loginDetails(profile, flow);
    onLoginSuccess(profile);
  }

  render() {
    const { flow } = this.state;
    return (
      <div className="lf">
        {{
          Mobile: (
            <MobileLogin
              onLoginSuccess={this.onLoginSuccess}
              onShowEmailScreen={this.onFlowChange('Email')}
            />
          ),
          Email: (
            <EmailLogin
              onLoginSuccess={this.onLoginSuccess}
              onShowMobileNumberLogin={this.onFlowChange('Mobile')}
              onShowForgotPasswordScreen={this.onFlowChange('Forgot-Password')}
            />
          ),
          'Forgot-Password': (
            <ForgotPassword />
          ),
        }[flow]}
      </div>
    );
  }
}

LoginFlow.propTypes = {
  onFlowChange: PropTypes.func.isRequired,
  onLoginSuccess: PropTypes.func.isRequired,
};

export default LoginFlow;
