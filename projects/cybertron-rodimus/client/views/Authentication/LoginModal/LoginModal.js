import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from '../../../components/Modal/Modal';
import LoginFlow from '../LoginFlow/LoginFlow';
import analyticsService from '../../../services/analytics/analyticsService';

class LoginModal extends Component {
  state = {
    flow: 'Mobile',
  }

  onClose = () => {
    analyticsService.loginModalClosed(this.state.flow);
    this.props.onClose();
  }

  onFlowChange = (flow) => {
    this.setState({ flow });
  }

  onLoginSuccess = () => {
    this.props.onLoginSuccess();
  }

  render() {
    const { isOpen } = this.props;
    return (
      <Modal
        isOpen={isOpen}
        onClose={this.onClose}
        closeIcon="close"
      >
        <LoginFlow
          onFlowChange={this.onFlowChange}
          onLoginSuccess={this.onLoginSuccess}
        />
      </Modal>
    );
  }
}

LoginModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onLoginSuccess: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default LoginModal;
