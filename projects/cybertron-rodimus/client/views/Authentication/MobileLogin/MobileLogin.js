import React, { Component } from 'react';
import Formsy from 'formsy-react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import PropTypes from 'prop-types';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import Button from '../../../components/Button/Button';
import { Row, Col } from '../../../components/Flex/Flex';
import Input from '../../../components/Input/Input';
import Otp from '../../../components/Otp/Otp';
import { Heading, Subheading, Text } from '../../../components/Typography/Typography';
import * as authActionCreators from '../../../services/auth/authDuck';
import * as toastActionCreators from '../../../services/toast/toastDuck';
import analyticsService from '../../../services/analytics/analyticsService';
import androidService from '../../../services/android/androidService';
import config from '../../../../config';
import './mobileLogin.css';

class MobileLogin extends Component {
  state = {
    showOtpModal: false,
    showErrorMsg: false,
  }

  onBlur = (mobile) => {
    analyticsService.mobileNumberEntered(mobile);
  }

  onSocialLoginClick = (type) => {
    analyticsService.loginClicked(type);
  }

  onFacebookLogin = (fbLoginData) => {
    const {
      onLoginSuccess,
      authActions,
    } = this.props;
    if (fbLoginData.accessToken) {
      authActions.facebookLogin(fbLoginData)
        .then((res) => {
          if (res.error) this.showError();
          else onLoginSuccess(res.payload.profile, 'Facebook');
        });
    } else {
      this.showError();
    }
  }

  onGoogleLogin = (googleLoginData) => {
    const {
      onLoginSuccess,
      authActions,
    } = this.props;
    authActions.googleLogin(googleLoginData)
      .then((res) => {
        if (res.error) this.showError();
        else onLoginSuccess(res.payload.profile, 'Google');
      });
  }

  onShowEmailScreen = () => {
    this.props.onShowEmailScreen();
    analyticsService.loginClicked('Email');
  }

  showError = () => {
    this.setState({
      showErrorMsg: true,
    });
  }

  sendOtp = ({ mobile }) => {
    const {
      toastActions,
      authActions: { sendLoginOtp },
    } = this.props;
    sendLoginOtp(mobile)
      .then((res) => {
        if (res.error) toastActions.showToast('Error in sending Otp, Try again.', 'capsular');
        else {
          analyticsService.otpSent(mobile);
          this.showOtpModal(true);
        }
      });
  }

  showOtpModal = (visibility) => {
    this.setState({ showOtpModal: visibility });
  }

  resendOtp = () => {
    const {
      auth: { profile },
      authActions,
    } = this.props;
    analyticsService.resendOtpClicked(profile.mobile);
    authActions.sendLoginOtp(profile.mobile);
  }

  verifyOtp = ({ otp }, reset, invalidate) => {
    const {
      onLoginSuccess,
      auth: { profile },
      authActions,
    } = this.props;

    analyticsService.otpEntered(profile.mobile);
    authActions.verifyLoginOtp(profile.mobile, otp)
      .then((res) => {
        if (res.error) {
          analyticsService.invalidOtpEntered(profile.mobile);
          invalidate({ otp: res.payload.errors[0].message });
        } else onLoginSuccess(res.payload.profile);
      });
  }

  render() {
    const {
      auth: {
        profile,
        isVerifyingOtp,
      },
      authActions,
    } = this.props;
    const { showErrorMsg, showOtpModal } = this.state;
    return (
      <div className="ml">
        <Heading
          className="ml__title"
          type="3"
        >
          Continue with your mobile number
        </Heading>
        <Formsy onValidSubmit={this.sendOtp}>
          <Input
            type="tel"
            name="mobile"
            onBlur={this.onBlur}
            validations="isNumeric,isLength:10"
            validationError="Please enter a valid mobile number"
            label="Enter 10 digit Mobile Number"
            required
          />
          <Button
            block
            className="ml__requestotp"
            type="submit"
          >Request OTP
          </Button>
        </Formsy>
        <div className="ml__social-login text-center">
          <Subheading className="ml__social-login__title">Or Sign in With</Subheading>
          <Row between>
            {
              !androidService.isAndroid ? (
                <Col
                  className="ml__social-login__option"
                  onClick={() => this.onSocialLoginClick('Facebook')}
                >
                  <FacebookLogin
                    appId={config.facebookAppId}
                    autoload
                    isMobile={false}
                    xfbml
                    cookie
                    scope="email"
                    fields="name, email"
                    version="2.5"
                    cssClass="button button--primary button--round ml__social-login__btn icon-facebook"
                    callback={this.onFacebookLogin}
                    textButton=""
                  />
                  <Text className="text-center" style={{ color: '#3b5998' }}>Facebook</Text>
                </Col>
              ) : null
            }
            {
              !androidService.isAndroid ? (
                <Col
                  className="ml__social-login__option"
                  onClick={() => this.onSocialLoginClick('Google')}
                >
                  <GoogleLogin
                    className="button button--primary button--round ml__social-login__btn icon-google"
                    clientId={config.googleAuthId}
                    onSuccess={this.onGoogleLogin}
                    onFailure={this.showError}
                    buttonText=""
                    tag="button"
                  />
                  <Text className="text-center" style={{ color: '#4285f4' }}>Google</Text>
                </Col>
              ) : null
            }
            <Col className="ml__social-login__option">
              <Button
                round
                className="ml__social-login__btn"
                onClick={this.onShowEmailScreen}
              >
                <i className="icon-email" />
              </Button>
              <Text className="text-center">Email</Text>
            </Col>
          </Row>
        </div>
        {
          showErrorMsg ? (
            <div className="floaty">
              <div className="lf__error text-center">
                <span className="lf__error__text">
                  Login failed. Try again
                </span>
              </div>
            </div>
          ) : (null)
        }
        <Otp
          isOpen={showOtpModal}
          onClose={() => this.showOtpModal(false)}
          mobile={profile.mobile}
          onResendOtp={this.resendOtp}
          onMobileChanged={authActions.updateLoginMobile}
          onVerifyOtp={this.verifyOtp}
          isVerifyingOtp={isVerifyingOtp}
        />
      </div>
    );
  }
}

MobileLogin.propTypes = {
  auth: PropTypes.object.isRequired,
  authActions: PropTypes.object.isRequired,
  onLoginSuccess: PropTypes.func.isRequired,
  onShowEmailScreen: PropTypes.func.isRequired,
  toastActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  authActions: bindActionCreators(authActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(MobileLogin);
