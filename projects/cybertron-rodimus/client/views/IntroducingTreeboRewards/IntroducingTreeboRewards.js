import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import qs from 'query-string';
import Image from 'leaf-ui/cjs/Image/web';
import Space from 'leaf-ui/cjs/Space/web';
import View from 'leaf-ui/cjs/View/web';
import Button from '../../components/Button/Button';
import { Col } from '../../components/Flex/Flex';
import { Heading, Subheading, Text } from '../../components/Typography/Typography';
import * as contentActionCreators from '../../services/content/contentDuck';
import walletService from '../../services/wallet/walletService';
import analyticsService from '../../services/analytics/analyticsService';
import rewardsService from '../../services/rewards/rewardsService';
import './introducingTreeboRewards.css';

class IntroducingTreeboRewards extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(contentActionCreators.getRewardsContent()));
    return Promise.all(promises);
  }

  componentWillMount() {
    rewardsService.hasViewedIntroducingTreeboRewardsPage(this.props);
  }

  componentDidMount() {
    const { contentActions, route, match } = this.props;
    contentActions.getSeoContent(route, match.params);
    contentActions.getRewardsContent();
  }

  redirectToLogin = () => {
    const { history } = this.props;
    walletService.updateWalletData({ introducingTreeboRewardsPageViewed: true });

    history.push({
      pathname: '/login/',
      search: qs.stringify({
        flow: 'menu',
      }),
    });
  }

  redirectToRewardsPage = () => {
    const { history } = this.props;
    walletService.updateWalletData({ introducingTreeboRewardsPageViewed: true });
    history.push('/rewards/');
  }

  redirectToFaq = () => {
    const { history } = this.props;
    walletService.updateWalletData({ introducingTreeboRewardsPageViewed: true });
    history.push('/faq/');
  }

  renderCta = () => {
    const { auth } = this.props;
    let handleCTAClick = () => {};
    let ctaText = '';
    if (auth.isAuthenticated) {
      handleCTAClick = () => {
        analyticsService.loyaltyLandingActionClicked({
          type: 'logged in',
        });
        this.redirectToRewardsPage();
      };
      ctaText = 'CHECK YOUR TREEBO POINTS';
    } else {
      handleCTAClick = () => {
        analyticsService.loyaltyLandingActionClicked({
          type: 'logged out',
        });
        this.redirectToLogin();
      };
      ctaText = 'SIGN UP FOR REWARD POINTS';
    }
    return (
      <Button
        flat
        large
        block
        className="floaty"
        onClick={handleCTAClick}
      >{ctaText}
      </Button>
    );
  }

  render() {
    const { rewardsContent: { introducingTreeboRewardsPage } } = this.props;
    return (
      <View className="itr">
        <View className="itr__header">
          <Image
            src={introducingTreeboRewardsPage.imageUrl}
            alt={introducingTreeboRewardsPage.tagLine}
            width="100%"
            height="100%"
          />
        </View>
        <View className="itr__container">
          <View className="itr__direct-booking">
            <Heading className="itr__title">BOOKING DIRECTLY ON TREEBO PAYS!</Heading>
            <View className="itr__direct-booking__benefits">
              <Space margin={[0, 2, 0, 0]}>
                <Image
                  src={introducingTreeboRewardsPage.icons.bestRates}
                  alt=""
                  width="56px"
                  height="56px"
                />
              </Space>
              <Col>
                <Heading type="3" className="itr__sub-title">Our Best Rates</Heading>
                <Subheading className="itr__text">Members of the program get our best rates.</Subheading>
              </Col>
            </View>
            <View className="itr__direct-booking__benefits">
              <Space margin={[0, 2, 0, 0]}>
                <Image
                  src={introducingTreeboRewardsPage.icons.earnPoints}
                  alt=""
                  width="56px"
                  height="56px"
                />
              </Space>
              <Col>
                <Heading type="3" className="itr__sub-title">Earn Treebo Points</Heading>
                <Subheading className="itr__text">Earn for every stay booked directly with us.</Subheading>
              </Col>
            </View>
          </View>
          <View className="itr__htpw">
            <Heading type="3" className="itr__htpw__title">How do Treebo Points Work?</Heading>
            <View className="itr__treebo-points">
              <Heading type="3" className="itr__sub-title">Book via Treebo</Heading>
              <Subheading className="itr__text">Book a Treebo Hotel, directly on treebo.com or the treebo apps.</Subheading>
            </View>
            <View className="itr__treebo-points">
              <Heading type="3" className="itr__sub-title">Stay & Earn</Heading>
              <Subheading className="itr__text">
                Treebo points get credited to your account on checkout.
                <Subheading className="itr__points-value">1 Treebo Point = ₹ 1</Subheading>
              </Subheading>
            </View>
            <View className="itr__treebo-points">
              <Heading type="3" className="itr__sub-title">Use Treebo Points</Heading>
              <Subheading className="itr__text">
                Treebo Points earned can be used on your future stays with us,
                  booked directly on Treebo.
              </Subheading>
            </View>
            <View className="itr__view-details" onClick={this.redirectToFaq}>
              <Text tag="span" type="2">Want more details?</Text>
              <Text tag="span" className="text--link"> View detailed T&Cs.</Text>
            </View>
          </View>
        </View>
        {this.renderCta()}
      </View>
    );
  }
}

IntroducingTreeboRewards.propTypes = {
  route: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  rewardsContent: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  rewardsContent: state.content.rewards,
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(IntroducingTreeboRewards);
