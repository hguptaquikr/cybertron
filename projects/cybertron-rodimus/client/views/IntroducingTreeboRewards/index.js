import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('IntroducingTreeboRewards');
    return import('./IntroducingTreeboRewards' /* webpackChunkName: 'IntroducingTreeboRewards' */);
  },
  loading: () => null,
});
