import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import get from 'lodash/get';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { Subheading, Text } from '../../../components/Typography/Typography';
import { browserDimensions, pluralize } from '../../../services/utils';
import * as searchActionCreators from '../../../services/search/searchDuck';
import googleService from '../../../services/google/googleService';
import './searchInput.css';

class SearchInput extends Component {
  componentDidMount() {
    this.getSuggestions();
    this.resizeSuggestions();
    this.onFocus();
  }

  onFocus = () => {
    if (get(this.inputRef, 'value')) {
      setTimeout(() => {
        this.inputRef.focus();
        this.inputRef.select();
      }, 50);
    }
  }

  onSuggestionClicked = (suggestion) => async () => {
    const { searchActions, history } = this.props;
    let place = suggestion;
    if (suggestion.type === 'google_api') {
      const { result } = await googleService.getPlaceDetails(suggestion);
      place = {
        ...place,
        ...googleService.constructPlace(result),
      };
    }
    searchActions.searchLocationChange(place);
    history.goBack();
  }

  getSuggestions = debounce(() => {
    const { searchActions } = this.props;
    searchActions.getSearchSuggestions(this.inputRef.value.trim());
  }, 300);

  resizeSuggestions = () => {
    this.suggestionsRef.style.height = `${browserDimensions().height - 144}px`;
  }

  storeRef = (variable) => (node) => {
    this[variable] = node;
  }

  renderSuggestion = (suggestion, index) => {
    let { type } = suggestion;
    if (suggestion.type === 'google_api') {
      type = '';
    } else if (suggestion.hotelCount) {
      type = `${suggestion.hotelCount} ${pluralize(suggestion.hotelCount, 'hotel')}`;
    } else if (suggestion.icon) {
      type = <i className={suggestion.icon} />;
    }

    return (
      <div className="si__suggestion" onClick={this.onSuggestionClicked(suggestion)} key={`suggestion${index}`}>
        <Subheading tag="span" type="1" className="si__suggestion__label text-truncate">
          {suggestion.q}
        </Subheading>
        {
          type ? (
            <Text tag="span" type="2" className="si__suggestion__type">
              {type}
            </Text>
          ) : null
        }
      </div>
    );
  }

  render() {
    const {
      search: {
        searchInput: {
          place,
          suggestions,
          isLoading,
        },
      },
    } = this.props;

    const value = get(this.inputRef, 'value', place.q);

    return (
      <div className="si">
        <input
          type="text"
          className="si__input"
          defaultValue={value}
          ref={this.storeRef('inputRef')}
          onChange={this.getSuggestions}
          onFocus={this.onFocus}
          placeholder="City, Locality or Hotel name"
        />
        <div
          className="si__suggestions"
          ref={this.storeRef('suggestionsRef')}
        >
          {suggestions.map(this.renderSuggestion)}
        </div>
        {
          isLoading ? (
            <span className="si__loader" />
          ) : null
        }
      </div>
    );
  }
}

SearchInput.propTypes = {
  search: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
)(SearchInput);
