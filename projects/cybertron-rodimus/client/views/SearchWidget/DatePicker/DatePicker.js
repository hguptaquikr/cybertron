import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { DateRangePicker } from 'react-dates';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { START_DATE, END_DATE, VERTICAL_SCROLLABLE } from 'react-dates/constants';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment';
import { constants, browserDimensions } from '../../../services/utils';
import * as searchActionCreators from '../../../services/search/searchDuck';
import './datePicker.css';

class DatePicker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      focusedInput: this.getInputToFocus(this.props),
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ focusedInput: this.getInputToFocus(nextProps) });
  }

  componentDidUpdate() {
    this.resizeCalendarHeight();
  }

  onDatesChange = ({ startDate, endDate }) => {
    const { searchActions, history } = this.props;
    searchActions.dateRangeChange({
      start: startDate,
      end: endDate,
    });
    if (startDate && endDate) history.goBack();
  }

  onFocusChange = (focusedInput) => {
    if (focusedInput) {
      this.setState({ focusedInput });
    }
  }

  getDaySize = () => {
    if (__BROWSER__) {
      const { width } = browserDimensions();
      if (width >= 400) return 48;
      else if (width >= 380) return 46;
      else if (width >= 360) return 43;
      else if (width >= 340) return 40;
    }
    return 37;
  };

  getInputToFocus = ({ search: { datePicker: { range, highlight } } }) => {
    if (!range.start) return START_DATE;
    else if (!range.end) return END_DATE;
    else if (highlight) return highlight === 'checkIn' ? START_DATE : END_DATE;
    return START_DATE;
  }

  storePickerRef = (dateRangePickerRef) => {
    if (dateRangePickerRef) {
      this.pickerRef = dateRangePickerRef.dayPickerContainer;
    }
  }

  resizeCalendarHeight = () => {
    this.pickerRef.style.height = `${browserDimensions().height - 153}px`;
  }

  initialVisibleMonth = () => moment()

  render() {
    const { search: { datePicker: { range } } } = this.props;
    const { focusedInput } = this.state;

    return (
      <DateRangePicker
        ref={this.storePickerRef}
        displayFormat={constants.dateFormat.view}
        startDatePlaceholderText="Check In"
        endDatePlaceholderText="Check Out"
        startDate={range.start}
        endDate={range.end}
        numberOfMonths={4}
        orientation={VERTICAL_SCROLLABLE}
        daySize={this.getDaySize()}
        focusedInput={focusedInput}
        onFocusChange={this.onFocusChange}
        onDatesChange={this.onDatesChange}
        initialVisibleMonth={this.initialVisibleMonth}
        customArrowIcon={<span />}
        navNext={<i className="icon-angle-down" />}
      />
    );
  }
}

DatePicker.propTypes = {
  search: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
)(DatePicker);
