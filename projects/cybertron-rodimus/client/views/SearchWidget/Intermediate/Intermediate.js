import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import cx from 'classnames';
import qs from 'query-string';
import { withRouter } from 'react-router-dom';
import { Heading, Text } from '../../../components/Typography/Typography';
import Ripple from '../../../components/Ripple/Ripple';
import { Row, Col } from '../../../components/Flex/Flex';
import ABButton from '../../../components/ABButton/ABButton';
import searchService from '../../../services/search/searchService';
import * as searchActionCreators from '../../../services/search/searchDuck';
import * as priceActionCreators from '../../../services/price/priceDuck';
import * as uiActionCreators from '../../../services/ui/uiDuck';
import * as routeService from '../../../services/route/routeService';
import './intermediate.css';

class Intermediate extends Component {
  onSearch = () => {
    const {
      search,
      searchActions,
      performSearch,
      priceActions,
      location,
      history,
      uiActions,
    } = this.props;

    const query = qs.parse(location.search);
    if (
      search.searchInput.place.q !== query.q
        || routeService.makeDateRangeQuery(search.datePicker.range).checkin
          !== query.checkin
        || routeService.makeDateRangeQuery(search.datePicker.range).checkout
          !== query.checkout
        || routeService.makeRoomConfigQuery(search.roomConfig.rooms).roomconfig
          !== query.roomconfig
    ) {
      if (searchActions.isSearchWidgetValid()) {
        performSearch(search, searchActions, null, priceActions);
      }
    } else {
      history.goBack();
      uiActions.showSearchWidget(false);
    }
  }

  renderSearchInputTrigger = () => {
    const {
      search: {
        searchInput,
      },
      searchActions,
    } = this.props;

    return (
      <Heading
        type="2"
        tag="div"
        onClick={() => searchActions.setSearchModal('searchInput')}
        className={cx('st__input st__search-input text-truncate', {
          'st__input--placeholder': !searchInput.place.q,
        })}
      >
        {searchInput.place.q || 'City, Locality or Hotel name'}
      </Heading>
    );
  }

  renderDatePickerTrigger = (type, placeholder) => {
    const {
      search: {
        datePicker,
      },
      searchActions,
    } = this.props;

    const datePickerValue = searchService.formattedDatePicker(datePicker.range)[type];

    return (
      <Heading
        type="2"
        tag="div"
        className={cx('st__input text-truncate', `st__${type}`, {
          'st__input--placeholder': !datePickerValue,
        })}
        onClick={() => searchActions.setSearchModal('datePicker', type)}
      >
        {datePickerValue || placeholder}
      </Heading>
    );
  }

  renderRoomConfigTrigger = () => {
    const {
      search: {
        roomConfig,
      },
      searchActions,
    } = this.props;

    const { text: roomConfigText } = searchService.formattedRoomConfig(roomConfig.rooms);

    return (
      <Heading
        type="2"
        tag="div"
        className={cx('st__input st_room text-truncate', { 'st__input--placeholder': !roomConfigText })}
        onClick={() => searchActions.setSearchModal('roomConfig')}
      >
        {roomConfigText || '1 Adult, 1 Room'}
      </Heading>
    );
  }

  renderSearchButtonTrigger = () => (
    <Ripple>
      <ABButton
        id="t-searchCTA"
        block
        size="large"
        onClick={this.onSearch}
        experiment="findPageCtaMobile"
        render={(text) => text || 'SEARCH'}
      />
    </Ripple>
  )

  render() {
    return (
      <div>
        <div>
          <div className="st sw__section">
            <Text className="st__text">Destination</Text>
            {this.renderSearchInputTrigger()}
          </div>
          <Row className="sw__section">
            <Col>
              <div className="st sw__section">
                <Text className="st__text">Dates</Text>
                {this.renderDatePickerTrigger('checkIn', 'Check In')}
              </div>
            </Col>
            <Col>
              <div className="st sw__section">
                <Text className="st__text"><span>&nbsp;</span></Text>
                {this.renderDatePickerTrigger('checkOut', 'Check Out')}
              </div>

            </Col>
          </Row>
          <div className="st sw__section">
            <Text className="st__text">Guests</Text>
            {this.renderRoomConfigTrigger()}
          </div>
        </div>
        <div className="floaty">
          {this.renderSearchButtonTrigger()}
        </div>
      </div>
    );
  }
}

Intermediate.propTypes = {
  search: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  priceActions: PropTypes.object.isRequired,
  uiActions: PropTypes.object.isRequired,
  performSearch: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
  price: state.price,
  history: state.history,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  priceActions: bindActionCreators(priceActionCreators, dispatch),
  uiActions: bindActionCreators(uiActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(Intermediate));
