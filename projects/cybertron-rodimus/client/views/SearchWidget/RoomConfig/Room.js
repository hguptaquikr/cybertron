import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Row, Col } from '../../../components/Flex/Flex';
import { Heading } from '../../../components/Typography/Typography';

const Room = ({
  roomNumber,
  adults,
  kids,
  addGuestType,
  subtractGuestType,
}) => (
  <div className="room">
    <Heading type="3" className="room__number">Room {roomNumber + 1}</Heading>
    <Row middle className="room__guest">
      <Col size="6">
        <Heading tag="p" type="3" className="room__guest-type">
          Adults
        </Heading>
      </Col>
      <Col size="6" className="room__modify">
        <i
          className={cx('icon-circle-minus room__sign', { 'room__sign--disabled': +adults === 1 })}
          onClick={() => subtractGuestType(roomNumber, 'adults')}
        />
        <Heading tag="span" className="room__adults-count">
          {adults}
        </Heading>
        <i
          className={cx('icon-circle-plus room__sign', { 'room__sign--disabled': +adults === 5 })}
          onClick={() => addGuestType(roomNumber, 'adults')}
        />
      </Col>
    </Row>
    <Row middle className="room__guest">
      <Col size="6">
        <Heading tag="p" type="3" className="room__guest-type">
          Kids
          <span className="room__guest-type--info"> (2-8 years)</span>
        </Heading>
      </Col>
      <Col size="6" className="room__modify">
        <i
          className={cx('icon-circle-minus room__sign', { 'room__sign--disabled': +kids === 0 })}
          onClick={() => subtractGuestType(roomNumber, 'kids')}
        />
        <Heading tag="span" className={cx('room__kids-count', { 'room__kids-count--disabled': +kids === 0 })}>
          {kids}
        </Heading>
        <i
          className={cx('icon-circle-plus room__sign', { 'room__sign--disabled': +kids === 5 })}
          onClick={() => addGuestType(roomNumber, 'kids')}
        />
      </Col>
    </Row>
  </div>
);

Room.propTypes = {
  roomNumber: PropTypes.number.isRequired,
  adults: PropTypes.number.isRequired,
  kids: PropTypes.number.isRequired,
  addGuestType: PropTypes.func.isRequired,
  subtractGuestType: PropTypes.func.isRequired,
};

export default Room;
