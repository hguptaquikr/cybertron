/* eslint-disable react/no-array-index-key */
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { Row, Col } from '../../../components/Flex/Flex';
import { Heading } from '../../../components/Typography/Typography';
import Button from '../../../components/Button/Button';
import Room from './Room';
import * as searchActionCreators from '../../../services/search/searchDuck';
import './roomConfig.css';

class RoomConfig extends Component {
  onSave = () => {
    const {
      history,
      onSave,
    } = this.props;

    if (onSave) onSave();
    history.goBack();
  }

  render() {
    const {
      search: { roomConfig: { rooms } },
      searchActions,
    } = this.props;

    return (
      <div className="rc">
        {
          rooms.map((room, roomNumber) => (
            <Room
              key={roomNumber}
              roomNumber={roomNumber}
              adults={+room.adults}
              kids={+room.kids}
              addGuestType={searchActions.addGuestType}
              subtractGuestType={searchActions.subtractGuestType}
            />
          ))
        }
        <Row bottom>
          <Col size="6">
            <Heading
              tag="div"
              type="3"
              className="rc__add-room"
              onClick={searchActions.addRoomConfig}
            >
              <i className="icon-circle-plus rc__add-room__icon" />
              Add Room
            </Heading>
          </Col>
          {
            rooms.length > 1 ? (
              <Col size="6" className="text-right">
                <Heading
                  tag="div"
                  type="3"
                  className="rc__remove-room"
                  onClick={searchActions.removeRoomConfig}
                >
                  Remove Room
                </Heading>
              </Col>
            ) : null
          }
        </Row>
        <Button
          flat
          large
          block
          className="floaty"
          onClick={this.onSave}
        >
          Confirm
        </Button>
      </div>
    );
  }
}

RoomConfig.propTypes = {
  history: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
)(RoomConfig);
