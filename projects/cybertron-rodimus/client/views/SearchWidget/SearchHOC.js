import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import kebabCase from 'lodash/kebabCase';
import get from 'lodash/get';
import qs from 'query-string';
import * as routeService from '../../services/route/routeService';
import analyticsService from '../../services/analytics/analyticsService';
import * as roomTypeService from '../../services/roomType/roomTypeService';
import filterService from '../../services/filter/filterService';
import priceService from '../../services/price/priceService';
import checkoutService from '../../services/checkout/checkoutService';
import searchService from '../../services/search/searchService';

const SearchHOC = (WrappedComponent) => withRouter(
  class extends Component {
    static propTypes = {
      search: PropTypes.object.isRequired,
      location: PropTypes.object.isRequired,
      history: PropTypes.object.isRequired,
      uiActions: PropTypes.object.isRequired,
    }

    componentDidMount() {
      this.initialSearchState = this.props.search;
    }

    performSearch = (search, searchActions, place, priceActions) => {
      const { location } = this.props;
      this.search = search;
      this.searchActions = searchActions;
      this.place = place || search.searchInput.place;
      this.priceActions = priceActions;
      if (
        location.pathname === '/itinerary/'
          && this.initialSearchState.searchInput.place.q === search.searchInput.place.q
      ) {
        this.handleCheckoutSearch();
      } else if (search.searchInput.place.hotelId) {
        this.handleHotelSearch();
      } else if (this.place.type === 'near_me') {
        this.handleNearMeSearch();
      } else {
        this.handleSearch();
      }
      this.captureAnalytics();
    }

    captureAnalytics = () => {
      const { location } = this.props;
      if (location.pathname === '/') analyticsService.searchTreebo();
      else analyticsService.searchTreebo(this.initialSearchState);
    };

    handleNearMeSearch = async () => {
      const res = await searchService.getIpLocation();
      this.place.area.city = res.city;
      this.place.q = res.city;
      this.handleSearch();
      filterService.sortByNearMe(this.props, res.city);
    }

    handleCheckoutSearch = () => {
      this.searchActions.checkHotelAvailability()
        .then((res) => {
          if (res.payload.available) {
            this.priceActions.getRoomPrices(this.search.searchInput.place.hotelId)
              .then((price) => this.showCheckoutDetails(res.payload.rooms, price.payload.price));
          } else {
            this.showNearbyHotels();
          }
        });
    }

    handleHotelSearch = () => {
      this.searchActions.checkHotelAvailability()
        .then((res) => {
          if (res.payload.available) {
            this.showHotelDetails();
          } else {
            this.showNearbyHotels();
          }
        });
    }

    showCheckoutDetails = (availableRooms, price) => {
      const {
        props: { history, uiActions },
        search,
      } = this;
      const room = availableRooms.find(({ type }) => type === search.roomConfig.roomType)
        || availableRooms[0];
      const roomTypeId = roomTypeService
        .makeRoomTypeId(search.searchInput.place.hotelId, room.type);
      const selectedRatePlan = priceService
        .sortRatePlan(price.byRoomTypeId[roomTypeId].ratePlans)[0] || {};

      uiActions.showSearchWidget(false);
      history.push(checkoutService.makeCheckoutLocation(search, room, null, selectedRatePlan));
    }

    showHotelDetails = () => {
      const {
        search: { datePicker, roomConfig },
        place,
        props: { history, uiActions },
      } = this;
      const city = kebabCase(place.area.city);
      const [hotelName, hotelLocality] = place.q.split(',');
      const hotelSlug = kebabCase(`${hotelName} ${hotelLocality}`);
      uiActions.showSearchWidget(false);
      history.push({
        pathname: `/hotels-in-${city}/${hotelSlug}-${place.hotelId}/`,
        search: qs.stringify({
          q: get(place, 'q'),
          landmark: get(place, 'area.landmark'),
          locality: get(place, 'area.locality'),
          city: get(place, 'area.city'),
          state: get(place, 'area.state'),
          ...routeService.makeCoordinatesQuery(place.coordinates),
          ...routeService.makeDateRangeQuery(datePicker.range),
          ...routeService.makeRoomConfigQuery(roomConfig.rooms),
        }),
      });
    }

    showNearbyHotels = () => this.handleSearch();

    handleSearch = () => {
      const {
        place,
        props: { history, uiActions },
        search: { datePicker, roomConfig },
      } = this;
      uiActions.showSearchWidget(false);
      history.push({
        pathname: '/search/',
        search: qs.stringify({
          q: get(place, 'q'),
          landmark: get(place, 'area.landmark'),
          locality: get(place, 'area.locality'),
          city: get(place, 'area.city'),
          state: get(place, 'area.state'),
          ...routeService.makeCoordinatesQuery(place.coordinates),
          ...routeService.makeDateRangeQuery(datePicker.range),
          ...routeService.makeRoomConfigQuery(roomConfig.rooms),
        }),
      });
    }

    render() {
      const additionalProps = {
        performSearch: this.performSearch,
      };

      return (
        <WrappedComponent {...this.props} {...additionalProps} />
      );
    }
  },
);

export default SearchHOC;
