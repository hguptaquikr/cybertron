import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('SearchWidget');
    return import('./SearchWidget' /* webpackChunkName: 'SearchWidget' */);
  },
  loading: () => null,
});
