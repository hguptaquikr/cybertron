import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import qs from 'query-string';
import SearchWidgetAsync from './SearchWidgetAsync';
import * as uiActionCreators from '../../services/ui/uiDuck';

class SearchWidgetWrapper extends React.Component {
  componentDidMount() {
    const { uiActions, location } = this.props;
    if (qs.parse(location.search).modal === 'search') {
      uiActions.showSearchWidget(true);
    }
  }
  render() {
    const { ui } = this.props;
    return (
      ui.isSearchWidgetOpen ? (
        <SearchWidgetAsync />
      ) : null
    );
  }
}

SearchWidgetWrapper.propTypes = {
  ui: PropTypes.object.isRequired,
  uiActions: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  ui: state.ui,
});

const mapDispatchToProps = (dispatch) => ({
  uiActions: bindActionCreators(uiActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(SearchWidgetWrapper);
