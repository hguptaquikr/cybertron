import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import Modal from '../../components/Modal/Modal';
import { Text } from '../../components/Typography/Typography';
import SearchInput from './SearchInput/SearchInput';
import DatePicker from './DatePicker/DatePicker';
import RoomConfig from './RoomConfig/RoomConfig';
import Intermediate from './Intermediate/Intermediate';
import SearchHOC from './SearchHOC';
import SearchResultsPage from '../SearchResultsPage';
import * as priceActionCreators from '../../services/price/priceDuck';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as uiActionCreators from '../../services/ui/uiDuck';
import analyticsService from '../../services/analytics/analyticsService';
import './searchWidget.css';
import * as abService from '../../services/ab/abService';

class SearchWidget extends Component {
  componentDidMount() {
    const {
      search: { searchInput },
      searchActions,
    } = this.props;

    if (!searchInput.place.q) searchActions.setSearchModal('searchInput');
    SearchResultsPage.preload();
    analyticsService.pageViewed('Find', {
      ...abService.impressionProperties(['findPageCtaMobile']),
    });
  }

  onClose = () => {
    const {
      search: { searchModal },
      searchActions,
      uiActions,
    } = this.props;
    if (searchModal !== 'intermediate') {
      searchActions.setSearchModal('intermediate');
    } else {
      uiActions.showSearchWidget(false);
    }
  }

  render() {
    const {
      search: { searchModal },
      performSearch,
    } = this.props;

    return (
      <div className="sw">
        <Modal
          label="searchWidget"
          bodyClassName="sw__body"
          isOpen
          onClose={this.onClose}
          closeIcon="close"
          scroll={searchModal === 'roomConfig'}
        >
          {{
            searchInput: (
              <Modal
                label="searchInput"
                bodyClassName="sw__body"
                isOpen
                onClose={this.onClose}
                closeIcon="close"
              >
                <div>
                  <Text>Destination</Text>
                  <SearchInput />
                </div>
              </Modal>
            ),
            datePicker: (
              <Modal
                label="datePicker"
                bodyClassName="sw__body"
                isOpen
                onClose={this.onClose}
                closeIcon="close"
              >
                <div>
                  <Text>Dates</Text>
                  <DatePicker />
                </div>
              </Modal>
            ),
            roomConfig: (
              <Modal
                label="roomConfig"
                bodyClassName="sw__body"
                isOpen
                onClose={this.onClose}
                closeIcon="close"
              >
                <div>
                  <Text>How many guests?</Text>
                  <RoomConfig />
                </div>
              </Modal>
            ),
          }[searchModal]}
          <Intermediate performSearch={performSearch} />
        </Modal>
      </div>
    );
  }
}

SearchWidget.propTypes = {
  search: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  uiActions: PropTypes.object.isRequired,
  performSearch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
  price: state.price,
  performSearch: state.performSearch,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  priceActions: bindActionCreators(priceActionCreators, dispatch),
  uiActions: bindActionCreators(uiActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  SearchHOC,
)(SearchWidget);
