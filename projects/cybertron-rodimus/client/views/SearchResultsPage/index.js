import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('SearchResultsPage');
    return import('./SearchResultsPage' /* webpackChunkName: 'SearchResultsPage' */);
  },
  loading: () => null,
});
