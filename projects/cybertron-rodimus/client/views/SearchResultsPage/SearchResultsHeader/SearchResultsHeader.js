import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import startCase from 'lodash/startCase';
import { Text, Subheading } from '../../../components/Typography/Typography';
import './searchResultsHeader.css';

class SearchResultsHeader extends Component {
  renderHeading = () => {
    const { history: { location } } = this.props;
    return (
      <Subheading tag="h1" className="srh__heading text-truncate">
        {startCase(location.pathname)}
      </Subheading>
    );
  }

  renderBreadcrumbs = () => {
    const { content: { breadcrumbs = [] } } = this.props;
    return breadcrumbs.length ? (
      <ul
        className="srh__breadcrumbs text-truncate"
        itemScope
        itemType="http://schema.org/BreadcrumbList"
      >
        {
          breadcrumbs.map((breadcrumb, i) => (
            <li
              key={breadcrumb.label}
              className="srh__breadcrumbs__item"
              itemScope
              itemProp="itemListElement"
              itemType="http://schema.org/ListItem"
            >
              <meta itemProp="position" content={i + 1} />
              <meta itemProp="name" content={breadcrumb.schema} />
              <Link
                to={breadcrumb.destination}
                className="srh__breadcrumbs__link"
                itemProp="item"
              >
                <Text tag="span" type="2">{startCase(breadcrumb.label)}</Text>
              </Link>
            </li>
          ))
        }
      </ul>
    ) : (
      <div className="srh__breadcrumbs" />
    );
  }

  renderShowingResults = () => {
    const {
      history: { location },
      isSearchPage,
      filter,
      search,
    } = this.props;

    const noOfTotalResults = filter.refinedHotelIds.length;

    return isSearchPage ? (
      <Text tag="div" type="2" className="srh__showing text-truncate">
        {`Showing ${noOfTotalResults} best hotels in ${startCase(search.searchInput.place.q)}`}
      </Text>
    ) : (
      <Text tag="div" type="2" className="srh__showing text-truncate">
        {`Showing ${noOfTotalResults} best ${startCase(location.pathname)}`}
      </Text>
    );
  }

  render() {
    const { isSearchPage } = this.props;
    return (
      <div className="srh">
        {isSearchPage ? null : this.renderBreadcrumbs()}
        {isSearchPage ? null : this.renderHeading()}
        {this.renderShowingResults()}
      </div>
    );
  }
}

SearchResultsHeader.propTypes = {
  history: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  content: PropTypes.object,
  isSearchPage: PropTypes.bool.isRequired,
  search: PropTypes.object.isRequired,
};

SearchResultsHeader.defaultProps = {
  content: {},
};

export default withRouter(SearchResultsHeader);
