/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import isEmpty from 'lodash/isEmpty';
import { Text, Heading } from '../../../components/Typography/Typography';
import contentService from '../../../services/content/contentService';
import './searchResultsFooter.css';

class SearchResultsFooter extends Component {
  state = {
    showMoreDescription: false,
  }

  toggleShowMoreDescription = () => {
    const { showMoreDescription } = this.state;
    this.setState({ showMoreDescription: !showMoreDescription });
  }

  renderLinks = (group, index) => (
    !isEmpty(group.content) ? (
      <div className="srf__links" key={`${group.title}${index + 1}`}>
        <Heading tag="h3" type="3" className="srf__links__title">
          {group.title}
        </Heading>
        <div className="srf__links__list">
          {group.content.map(this.renderLink)}
        </div>
      </div>
    ) : null
  )

  renderLink = (place) => {
    const url = contentService.getSeoPathName(place);
    const label = place.label.split(',')[0];
    return (
      <Link className="srf__links__item" to={url} key={url}>
        {label}
      </Link>
    );
  }

  render() {
    const { showMoreDescription } = this.state;
    const { content: { description, links } } = this.props;
    return (
      <div className="srf">
        {
          !isEmpty(description) ? (
            <div className="srf__description">
              <div
                className={cx('srf__description__text', {
                  'srf__description__text--more': showMoreDescription,
                })}
                dangerouslySetInnerHTML={{ __html: description }}
              />
              <Text
                tag="div"
                className="angle-link"
                onClick={this.toggleShowMoreDescription}
              >
                {showMoreDescription ? 'VIEW LESS' : 'VIEW MORE'}
              </Text>
            </div>
          ) : null
        }
        {!isEmpty(links) ? links.map(this.renderLinks) : null}
      </div>
    );
  }
}

SearchResultsFooter.propTypes = {
  content: PropTypes.object,
};

SearchResultsFooter.defaultProps = {
  content: {},
};

export default SearchResultsFooter;
