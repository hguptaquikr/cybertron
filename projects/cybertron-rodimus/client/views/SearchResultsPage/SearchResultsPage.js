import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import qs from 'query-string';
import Size from 'leaf-ui/cjs/Size/web';
import Text from 'leaf-ui/cjs/Text/web';
import Image from 'leaf-ui/cjs/Image/web';
import Gallery from 'leaf-ui/cjs/Gallery/web';
import View from 'leaf-ui/cjs/View/web';
import Space from 'leaf-ui/cjs/Space/web';
import Tag from 'leaf-ui/cjs/Tag/web';
import Ripple from '../../components/Ripple/Ripple';
import { Row, Col } from '../../components/Flex/Flex';
import { Heading } from '../../components/Typography/Typography';
import SearchHeader from '../../components/SearchHeader/SearchHeader';
import SearchWidgetAsync from '../SearchWidget/SearchWidgetAsync';
import HotelDetailsPage from './../HotelDetailsPage';
import PerfectStayBanner from '../../components/PerfectStayBanner/PerfectStayBanner';
import SearchResultsHeader from './SearchResultsHeader/SearchResultsHeader';
import SearchResultsRefine from './SearchResultsRefine/SearchResultsRefine';
import SearchResultsItem from './SearchResultsItem/SearchResultsItem';
import SearchResultsFooter from './SearchResultsFooter/SearchResultsFooter';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as hotelActionCreators from '../../services/hotel/hotelDuck';
import * as filterActionCreators from '../../services/filter/filterDuck';
import * as priceActionCreators from '../../services/price/priceDuck';
import * as toastActionCreators from '../../services/toast/toastDuck';
import * as walletActionCreators from '../../services/wallet/walletDuck';
import * as hotelService from '../../services/hotel/hotelService';
import * as abService from '../../services/ab/abService';
import priceService from '../../services/price/priceService';
import searchService from '../../services/search/searchService';
import analyticsService from '../../services/analytics/analyticsService';
import { constants } from '../../services/utils';
import './searchResultsPage.css';

class SearchResultsPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match, req }) => {
    const promises = [];
    dispatch(searchActionCreators.setSearchState({ ...req.query, ...match.params }));
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(hotelActionCreators.getHotelResults(req.query, match.params))
      .then(() => dispatch(hotelActionCreators.addDistanceToResults()))
      .then(() => dispatch(filterActionCreators.refineResults())));
    return Promise.all(promises);
  }

  state = {
    showRefineResultsModal: false,
  };

  componentDidMount() {
    const {
      contentActions,
      searchActions,
      walletActions,
      location,
      match,
      auth,
      route,
    } = this.props;

    searchActions.setSearchState({ ...qs.parse(location.search), ...match.params });
    contentActions.getSeoContent(route, match.params);
    if (auth.isAuthenticated) walletActions.getWalletBalance();
    this.getHotelResults(this.props);
    contentActions.getAdsBannerContent();
    contentActions.getAdsBannerContent();
    SearchWidgetAsync.preload();
    HotelDetailsPage.preload();
  }

  componentWillReceiveProps(nextProps) {
    const { props } = this;
    const { searchActions } = this.props;
    const query = qs.parse(props.location.search);
    const nextQuery = qs.parse(nextProps.location.search);

    if (
      query.checkin !== nextQuery.checkin
        || query.checkout !== nextQuery.checkout
        || query.city !== nextQuery.city
        || query.locality !== nextQuery.locality
        || query.roomconfig !== nextQuery.roomconfig
        || query.lat !== nextQuery.lat
        || query.lng !== nextQuery.lng
        || props.match.params.city !== nextProps.match.params.city
        || props.match.params.locality !== nextProps.match.params.locality
        || props.match.params.landmark !== nextProps.match.params.landmark
        || props.match.params.category !== nextProps.match.params.category
    ) {
      searchActions.setSearchState({ ...nextQuery, ...nextProps.match.params });
      nextProps.contentActions.getSeoContent(nextProps.route, nextProps.match.params);
      this.getHotelResults(nextProps);
    }
  }

  componentWillUnmount() {
    const { filterActions } = this.props;
    filterActions.clearAllFilters();
  }

  onSearchResultsItemClicked =
    (index, hotelId, noOfRefinedResults) =>
      (cheapestPrice, cheapestRatePlan, isQuickBook) => {
        const {
          location,
          search: {
            searchInput,
          },
          $roomType,
          content: { psychologicalTriggers },
        } = this.props;
        const psychologicalTrigger = psychologicalTriggers.byRoomTypeId[cheapestPrice.id];
        const analyticsProperties = {
          hotelId,
          cheapestPrice,
          cheapestRatePlan,
          $roomType,
          search_rank: index + 1,
          destination: searchInput.place.q,
          num_results: noOfRefinedResults,
          page_url: `${location.pathname}${location.search}`,
          trigger_displayed: !isEmpty(psychologicalTrigger),
          trigger_type: !isEmpty(psychologicalTrigger) ? psychologicalTrigger.type : '',
          flow: 'search',
        };
        if (isQuickBook) analyticsService.quickBookClicked(analyticsProperties);
        else analyticsService.hotelViewDetailsClicked(analyticsProperties);
      };

  getSearchPrices = (hotelIds) => {
    const {
      searchActions,
      priceActions,
      filterActions,
      contentActions,
    } = this.props;
    if (searchActions.isSearchWidgetValid()) {
      priceActions.getSearchPrices(hotelIds)
        .then((res) => {
          this.showAutoAppliedCouponToast(res);
          filterActions.refineResults();
          filterActions.constructPriceRangesFilter();
          contentActions.getPsychologicalTriggers(res.payload.hotel.ids);
          this.captureAnalytics();
        });
    }
  }

  getHotelResults = (props = this.props) => {
    const { hotelActions, filterActions, location, toastActions, match } = props;
    const query = qs.parse(location.search);
    filterActions.clearAllFilters();
    hotelActions.getHotelResults(qs.parse(location.search), match.params)
      .then((res) => {
        if (query.hotel_id) {
          toastActions.showToast(
            <Space padding={[1]}>
              <View>
                <Text
                  size="s"
                  color="greyDarker"
                >
                    Uh Oh!
                </Text>
                <Space margin={[0.5, 0, 0, 0]}>
                  <Text
                    size="xs"
                    color="greyDark"
                    component="p"
                  >
                      The Treebo you were looking for is sold out.
                      Here are other Treebos in the same locality
                  </Text>
                </Space>
              </View>
            </Space>,
            'sharpEdged',
          );
        }
        hotelActions.addDistanceToResults();
        this.getSearchPrices(res.payload.ids);
        filterActions.constructLocalitiesFilter();
      });
  }

  showAutoAppliedCouponToast = ({ payload = { price: {} } }) => {
    const { toastActions } = this.props;
    const couponCode = priceService.getAutoAppliedCoupon(payload.price.byRoomTypeId);
    if (couponCode) toastActions.showToast(`${couponCode} pre applied`, 'capsular');
  }

  isMemberDiscountAvailable = () => {
    const { price } = this.props;
    return Object.values(price.byRoomTypeId).some((obj) => obj.memberDiscount.isAvailable);
  }

  isCoupleFriendlyHotelAvailable = (hotels) =>
    hotels.ids.some((hotelId) => {
      const hotel = hotels.byId[hotelId];
      return hotelService.isCoupleFriendly(hotel.policies);
    });

  showRefineResultsModal = (status) => {
    this.setState({ showRefineResultsModal: status });
  };

  captureAnalytics = () => {
    const {
      hotel,
      search: {
        roomConfig,
        datePicker: {
          range,
        },
      },
      location,
      match,
    } = this.props;

    const query = qs.parse(location.search);
    const { adults, kids, rooms } =
      searchService.formattedRoomConfig(roomConfig.rooms);
    const { checkIn, checkOut, numberOfNights, abwDuration } =
      searchService.formattedDatePicker(range, constants.dateFormat.query);

    analyticsService.listContentViewed({
      flow: 'search',
      resultCount: hotel.ids.length,
      destination: match.params.city || query.q,
      checkin: checkIn,
      checkout: checkOut,
      lengthOfStay: numberOfNights.count,
      adults: adults.count,
      kids: kids.count,
      rooms: rooms.count,
      abwDays: abwDuration,
      ...abService.impressionProperties(['quickBookCTAMobile', 'perfectStayBanner']),
    });
  }

  renderNoResultsText = (noOfTotalResults) => (
    <div className="srp__no-results">
      <Heading className="srp__no-results__head">Sorry</Heading>
      <Heading className="srp__no-results__title" type="3">
        No results found matching your {noOfTotalResults ? 'filters' : 'search'}!
      </Heading>
    </div>
  );

  renderSingleResultText = () => (
    <Text className="srp__single-result">
      That&apos;s all folks<br />
      There&apos;s only one and it&apos;s the best option available in this location
    </Text>
  )

  renderAdsBanner = () => {
    const { content } = this.props;
    return !isEmpty(content.adsBanner) ? (
      <View className="sri" itemScope>
        <View className="sri__link">
          <Size width="100%" height="150px">
            <Gallery>
              {
                content.adsBanner.map((image, index) => (
                  <Link to={image.urlRedirect} key={`${image}${index + 1}`}>
                    <Image
                      src={image.url}
                      alt={image.alt}
                      width="200px"
                      height="150px"
                    />
                  </Link>
                ))
              }
            </Gallery>
          </Size>
        </View>
      </View>
    ) : null;
  }

  renderResults = () => {
    const {
      hotel,
      price,
      filter: { refinedHotelIds },
      $roomType,
      location,
      search,
    } = this.props;
    const noOfTotalResults = hotel.ids.length;
    const noOfRefinedResults = refinedHotelIds.length;

    if (hotel.isHotelResultsLoading) {
      return [0, 1, 2, 3].map((key) => (
        <SearchResultsItem
          key={key}
          hotel={hotel.byId[0]}
          price={price}
          $roomType={$roomType}
          search={search}
          location={location}
        />
      ));
    } else if (noOfRefinedResults || price.isPricesLoading) {
      return refinedHotelIds.map((hotelId, index) => (
        <SearchResultsItem
          key={hotelId}
          hotel={hotel.byId[hotelId]}
          $roomType={$roomType}
          price={price}
          search={search}
          location={location}
          isSimilarTreebo={false}
          onClick={this.onSearchResultsItemClicked(index, hotelId, noOfRefinedResults)}
        />
      ));
    }
    return this.renderNoResultsText(noOfTotalResults);
  }

  renderResultsWithAds = () => {
    const { filter: { refinedHotelIds } } = this.props;
    const noOfRefinedResults = refinedHotelIds.length;

    return noOfRefinedResults <= 3 ? (
      <div>
        {this.renderResults()}
        {this.renderAdsBanner()}
      </div>
    ) : (
      React.Children.map(this.renderResults(), (result, index) => (
        index === 3 ? (
          <div>
            {result}
            {this.renderAdsBanner()}
          </div>
        ) : result
      ))
    );
  }

  render() {
    const { showRefineResultsModal } = this.state;
    const {
      hotel,
      filter,
      content: { seo },
      location,
      search,
      route,
    } = this.props;

    const noOfTotalResults = hotel.ids.length;
    const isSearchPage = location.pathname === '/search/';
    const isCoupleFriendlyHotelAvailable = this.isCoupleFriendlyHotelAvailable(hotel);
    const isPerfectStayBannerOpen = get(abService.getExperiments(), 'perfectStayBanner.variant') === 'open';
    return (
      <View className="srp">
        <SearchHeader />
        <View className="srp__body">
          <SearchResultsHeader
            search={search}
            hotels={hotel.byId}
            content={seo.content}
            isSearchPage={isSearchPage}
            filter={filter}
          />
          <Space padding={[2]}>
            <PerfectStayBanner isOpen={isPerfectStayBannerOpen} />
          </Space>
          <View style={{ borderTop: '8px solid #e0e0e0' }}>
            {this.renderResultsWithAds()}
          </View>
          {noOfTotalResults === 1 ? this.renderSingleResultText() : null}
          <SearchResultsFooter
            content={seo.content}
            isSearchPage={isSearchPage}
          />
        </View>
        {
          noOfTotalResults >= 3 ? (
            <View>
              <Ripple>
                <Row center className="floaty srp__filter" onClick={() => this.showRefineResultsModal(true)}>
                  <Col size="6" className="srp__filter__item"><Text className="srp__filter__item__sort">SORTED BY {filter.sort.by}</Text></Col>
                  <Col size="6" className="srp__filter__item">
                    <Text className="srp__filter__item__filter">
                      FILTERS
                      {
                        isCoupleFriendlyHotelAvailable ? (
                          <Space margin={[0, 0, 0, 0.5]}>
                            <Tag
                              color="yellowLight"
                              shape="capsular"
                              kind="filled"
                              size="small"
                            >
                              New
                            </Tag>
                          </Space>
                        ) : null
                      }
                    </Text>
                  </Col>
                </Row>
              </Ripple>
              {
                showRefineResultsModal ? (
                  <SearchResultsRefine
                    route={route}
                    isCoupleFriendlyHotelAvailable={isCoupleFriendlyHotelAvailable}
                    onClose={() => this.showRefineResultsModal(false)}
                  />
                ) : null
              }
            </View>
          ) : null
        }
      </View>
    );
  }
}

SearchResultsPage.propTypes = {
  route: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  filterActions: PropTypes.object.isRequired,
  priceActions: PropTypes.object.isRequired,
  toastActions: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  hotel: state.hotel,
  price: state.price,
  filter: state.filter,
  search: state.search,
  content: state.content,
  auth: state.auth,
  $roomType: state.$roomType,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
  filterActions: bindActionCreators(filterActionCreators, dispatch),
  priceActions: bindActionCreators(priceActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
)(SearchResultsPage);
