import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import Image from 'leaf-ui/cjs/Image/amp';
import Gallery from 'leaf-ui/cjs/Gallery/amp';
import Text from 'leaf-ui/cjs/Text/amp';
import Flex from 'leaf-ui/cjs/Flex/amp';
import Space from 'leaf-ui/cjs/Space/amp';
import Divider from 'leaf-ui/cjs/Divider/amp';
import View from 'leaf-ui/cjs/View/amp';
import Size from 'leaf-ui/cjs/Size/amp';
import Tag from 'leaf-ui/cjs/Tag/amp';
import config from '../../../../config';
import priceService from '../../../services/price/priceService';
import * as hotelService from '../../../services/hotel/hotelService';
import { constants } from '../../../services/utils';

const SearchResultsItem = ({
  hotel,
  price,
  location,
  $roomType,
}) => {
  const images = !isEmpty(hotel.images) ? hotel.images : [{ url: '', tagline: '' }, { url: '', tagline: '' }];
  const hotelPrices = priceService.getPricesForRoomTypeIds(hotel.roomTypeIds, price);
  const cheapestPrice = priceService.sortPrice(hotelPrices)[0];
  const cheapestRatePlan = priceService.sortRatePlan(cheapestPrice.ratePlans)[0] || {};
  const cheapestRoomType = $roomType.byId[cheapestPrice.id];
  const isAvailable = !!cheapestPrice.available;
  const isCoupleFriendly = hotelService.isCoupleFriendly(hotel.policies);
  const hotelDetailsLocation = !isEmpty(cheapestRatePlan) ?
    hotelService.makeHotelDetailsLocation(hotel, cheapestRoomType, location, cheapestRatePlan) : '';
  const hotelDetailsUrl = `${config.hostUrl}${hotelDetailsLocation.pathname}`;
  const formatedPrice = cheapestRatePlan.sellingPrice && Math.round(cheapestRatePlan.sellingPrice).toLocaleString('en-IN');
  return (
    <View>
      <a
        className="link"
        id={`t-resultItem-${hotel.id}`}
        href={hotelDetailsUrl}
      >
        <Space margin={[2, 0]}>
          <View>
            <Gallery
              width="auto"
              height="150px"
              type="carousel"
            >
              <Space padding={[0, 1, 0, 0]}><View /></Space>
              {
                images.map((image, index) => (
                  <Image
                    key={`${image}${index + 1}`}
                    src={image.url}
                    alt={image.tagline}
                    width="200px"
                    height="150px"
                    shape="bluntEdged"
                    grayscale={!isAvailable}
                  />
                ))
              }
            </Gallery>
          </View>
        </Space>
        {
          isCoupleFriendly ? (
            <Space padding={[2]}>
              <Flex flexDirection="row">
                <View>
                  <Tag
                    color="primary"
                    shape="capsular"
                    kind="outlined"
                  >
                    Couple Friendly
                  </Tag>
                </View>
              </Flex>
            </Space>
          ) : null
        }
        <Space padding={[0, 2]} margin={[2, 0]}>
          <View>
            <Flex
              flexDirection="row"
              justifyContent="space-between"
            >
              <View>
                <View>
                  <meta itemProp="telephone" content={`+91-${constants.customerCareNumber}`} />
                  <meta itemProp="email" content="hello@treebohotels.com" />
                  <Size width="200px">
                    <Text size="m" color="greyDarker" weight="semibold" truncate>
                      {hotel.name}
                    </Text>
                  </Size>
                  <Text size="m" color="greyDark" weight="semibold">
                    <meta itemProp="addressLocality" content={hotel.address.locality} />
                    <meta itemProp="addressRegion" content={hotel.address.city} />
                    <Space margin={[1, 0, 1, 0]}>
                      <div>
                        <Text size="s" color="greyDark" weight="normal">
                          {hotel.address.locality}
                        </Text>
                      </div>
                    </Space>
                  </Text>
                  {
                      hotel.reviews.isTaEnabled && hotel.reviews.overallRating.rating ? (
                        <Flex flexDirection="row" alignItems="center">
                          <View>
                            <meta itemProp="ratingValue" content={hotel.reviews.overallRating.rating} />
                            <meta itemProp="ratingCount" content={hotel.reviews.overallRating.ratingCount} />
                            <meta itemProp="bestRating" content="5" />
                            <Image
                              src="https://images.treebohotels.com/images/ta_reviews.svg"
                              alt="TA"
                              width="24"
                              height="24"
                            />
                            <Space padding={[0, 0, 0, 1]}>
                              <View>
                                <Text size="s" color="greyDarker">
                                  <span>
                                    {hotel.reviews.overallRating.rating}
                                  </span>
                                /5
                                </Text>
                              </View>
                            </Space>
                            <Text size="s" color="greyDark">
                              <span>
                                &nbsp;
                                {+hotel.reviews.overallRating.rating >= 4.5 && +hotel.reviews.overallRating.rating <= 5.0 && 'Excellent '}
                                {+hotel.reviews.overallRating.rating >= 4.0 && +hotel.reviews.overallRating.rating < 4.5 && 'Good'}
                              </span>
                            </Text>
                          </View>
                        </Flex>
                      ) : null
                    }
                </View>
                <View>
                  {
                      isAvailable ? (
                        <div>
                          <Text size="xxxl" color="greyDarker" weight="semibold">
                            &#x20b9;{formatedPrice}
                          </Text>
                        </div>
                      ) : (
                        <Text size="m" color="red">
                          SOLD OUT
                        </Text>
                      )
                    }
                </View>
              </View>
            </Flex>
          </View>
        </Space>
      </a>
      <Divider />
    </View>
  );
};

SearchResultsItem.propTypes = {
  hotel: PropTypes.object.isRequired,
  price: PropTypes.object,
  location: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

export default connect(
  mapStateToProps,
)(SearchResultsItem);
