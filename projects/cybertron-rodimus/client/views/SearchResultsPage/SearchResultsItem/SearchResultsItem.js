import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import Image from 'leaf-ui/cjs/Image/web';
import Gallery from 'leaf-ui/cjs/Gallery/web';
import Space from 'leaf-ui/cjs/Space/web';
import Divider from 'leaf-ui/cjs/Divider/web';
import View from 'leaf-ui/cjs/View/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import { Row, Col } from '../../../components/Flex/Flex';
import { Subheading, Text } from '../../../components/Typography/Typography';
import Price from '../../../components/Price/Price';
import Ripple from '../../../components/Ripple/Ripple';
import CoupleFriendlyTag from '../../../components/CoupleFriendlyTag/CoupleFriendlyTag';
import PsychologicalTriggers from '../../../components/PsychologicalTriggers/PsychologicalTriggers';
import priceService from '../../../services/price/priceService';
import * as hotelService from '../../../services/hotel/hotelService';
import { constants } from '../../../services/utils';
import './searchResultsItem.css';

class SearchResultsItem extends Component {
  onClick = (isQuickBook, cheapestPrice, cheapestRatePlan) => () => {
    const { onClick, isSimilarTreebo } = this.props;
    if (isSimilarTreebo) onClick(cheapestPrice, cheapestRatePlan);
    else onClick(cheapestPrice, cheapestRatePlan, isQuickBook);
  }

  render() {
    const {
      hotel,
      price,
      location,
      $roomType,
      content,
    } = this.props;
    const images = !isEmpty(hotel.images) ? hotel.images : [{ url: '', tagline: '' }, { url: '', tagline: '' }];
    const hotelPrices = priceService.getPricesForRoomTypeIds(hotel.roomTypeIds, price);
    const cheapestPrice = priceService.sortPrice(hotelPrices)[0];
    const cheapestRatePlan = priceService.sortRatePlan(cheapestPrice.ratePlans)[0] || {};
    const cheapestRoomType = $roomType.byId[cheapestPrice.id];
    const isAvailable = !!cheapestPrice.available;
    const isCoupleFriendly = hotelService.isCoupleFriendly(hotel.policies);
    const hotelDetailsUrl = !isEmpty(cheapestRatePlan) ?
      hotelService.makeHotelDetailsLocation(hotel, cheapestRoomType, location, cheapestRatePlan) : '';

    const trigger = content.psychologicalTriggers.byRoomTypeId[cheapestPrice.id];

    return (
      <View
        itemScope
        itemType="http://schema.org/Hotel"
      >
        <Ripple>
          <Link
            id="t-available-hotel-count"
            to={hotelDetailsUrl}
            className={cx('srig__link', { 'srig--soldout': !isAvailable })}
            onClick={this.onClick(false, cheapestPrice, cheapestRatePlan)}
            itemProp="url"
          >
            <View>
              <Gallery width="100%" height="150px">
                <Space padding={[0, 1.5, 0, 0]}><View /></Space>
                {
                  images.map((image, index) => (
                    <Image
                      key={`${image}${index + 1}`}
                      src={image.url}
                      alt={image.tagline}
                      width="200px"
                      height="150px"
                      shape="bluntEdged"
                      grayscale={!isAvailable}
                    />
                  ))
                }
              </Gallery>
            </View>
            {
              isCoupleFriendly ? (
                <Space padding={[0, 2]} margin={[2, 0]}>
                  <Flex flexDirection="row">
                    <View>
                      <CoupleFriendlyTag isCoupleFriendly={isCoupleFriendly} />
                    </View>
                  </Flex>
                </Space>
              ) : null
            }
            <Space padding={[0, 2]} margin={[2, 0]}>
              <Row>
                <Col size="7">
                  <View>
                    <meta itemProp="telephone" content={`+91-${constants.customerCareNumber}`} />
                    <meta itemProp="email" content="hello@treebohotels.com" />
                    <Subheading
                      type="1"
                      tag="div"
                      itemProp="name"
                      className="srig__name text-truncate"
                      preview={!hotel.name}
                      previewStyle={{ width: '60%' }}
                    >
                      {hotel.name}
                    </Subheading>
                    <Subheading
                      type="2"
                      tag="div"
                      className="srig__address text-truncate"
                      preview={!hotel.address.locality}
                      previewStyle={{ width: '60%' }}
                      itemProp="address"
                      itemScope
                      itemType="http://schema.org/PostalAddress"
                    >
                      <meta itemProp="addressLocality" content={hotel.address.locality} />
                      <meta itemProp="addressRegion" content={hotel.address.city} />
                      <span>{hotel.address.locality}</span>
                    </Subheading>
                    {
                      hotel.reviews.isTaEnabled && hotel.reviews.overallRating.rating ? (
                        <View
                          className="text-right srig__rating-container"
                          itemProp="aggregateRating"
                          itemScope
                          itemType="http://schema.org/AggregateRating"
                        >
                          <meta itemProp="ratingValue" content={hotel.reviews.overallRating.rating} />
                          <meta itemProp="ratingCount" content={hotel.reviews.overallRating.ratingCount} />
                          <meta itemProp="bestRating" content="5" />
                          <img
                            src="https://images.treebohotels.com/images/ta_reviews.svg"
                            className="old-image srig__rating-image"
                            alt="TA"
                          />
                          <Text tag="div">
                            <span className="srig__rating">
                              {hotel.reviews.overallRating.rating}
                            </span>
                            <span className="srig__rating--light">
                                  /5
                            </span>
                          </Text>

                          <Text tag="div">
                            <span className="srig__rating--light srig__rating__comment">
                              {+hotel.reviews.overallRating.rating >= 4.5 && +hotel.reviews.overallRating.rating <= 5.0 && ' "Excellent" '}
                              {+hotel.reviews.overallRating.rating >= 4.0 && +hotel.reviews.overallRating.rating < 4.5 && ' "Good" '}
                            </span>
                          </Text>
                        </View>
                      ) : null
                    }
                  </View>
                </Col>
                <Col size="5">
                  {
                  isAvailable ? (
                    <View className="srig__price">
                      <View>
                        <Price
                          size="small"
                          type="striked"
                          className="srig__striked-price hide"
                          preview={cheapestRatePlan.discountPercentage <= 0}
                          previewStyle={{ visibility: 'hidden' }}
                          price={cheapestRatePlan.strikedPrice}
                        />
                        <Price
                          bold
                          className="srig__selling-price"
                          preview
                          price={cheapestRatePlan.sellingPrice}
                          previewStyle={{ height: '4rem', width: '12rem' }}
                        />
                        {
                          cheapestRatePlan.discountPercentage > 0 ? (
                            <Text
                              type="2"
                              tag="span"
                              className="srig__discount-percentage hide"
                            >
                              {cheapestRatePlan.discountPercentage}% off
                            </Text>
                          ) : null
                        }
                      </View>
                    </View>
                  ) : (
                    <View className="text--error">SOLD OUT</View>
                  )
                }
                </Col>
              </Row>
            </Space>
            {
              !isEmpty(hotel.roomTypeIds) && !isEmpty(trigger) ? (
                <View>
                  <Space margin={[0, 2, 0, 2]}>
                    <Divider color="greyLight" />
                  </Space>
                  <Space padding={[2, 2, 0, 2]} margin={[0, 0, 2, 0]}>
                    <PsychologicalTriggers trigger={trigger} />
                  </Space>
                </View>
              ) : null
            }
          </Link>
        </Ripple>
      </View>
    );
  }
}

SearchResultsItem.propTypes = {
  hotel: PropTypes.object.isRequired,
  content: PropTypes.object,
  price: PropTypes.object,
  location: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  isSimilarTreebo: PropTypes.bool,
  $roomType: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

export default connect(
  mapStateToProps,
)(SearchResultsItem);
