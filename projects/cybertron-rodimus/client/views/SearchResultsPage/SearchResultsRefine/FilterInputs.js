/* eslint-disable react/no-danger */
import React from 'react';
import PropTypes from 'prop-types';
import { Subheading } from '../../../components/Typography/Typography';

const FilterInputs = ({
  filterItems,
  filterKey,
  filtersCheckboxChange,
  showMore,
}) => {
  let filterItemsToMap = filterItems;
  if (filterItems.length > 3 && !showMore) filterItemsToMap = filterItems.slice(0, 3);

  return (
    <div>
      {
        filterItemsToMap.map((filterItem, i) => {
          const key = `${filterKey}-${i}`;
          return (
            <label key={key} htmlFor={key} className="filter__item">
              <input
                id={key}
                type="checkbox"
                className="filter__item__input"
                onChange={(e) => filtersCheckboxChange(filterKey, i, e.target.checked)}
                checked={filterItem.checked}
              />
              <Subheading
                tag="span"
                dangerouslySetInnerHTML={{ __html: filterItem.name.split('-').join(' to ') }}
              />
            </label>
          );
        })
      }
    </div>
  );
};

FilterInputs.propTypes = {
  filterItems: PropTypes.array.isRequired,
  filterKey: PropTypes.string.isRequired,
  filtersCheckboxChange: PropTypes.func.isRequired,
  showMore: PropTypes.bool,
};

export default FilterInputs;
