import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import Chip from '../../../components/Chip/Chip';
import Modal from '../../../components/Modal/Modal';
import Button from '../../../components/Button/Button';
import { Row, Col } from '../../../components/Flex/Flex';
import Ripple from '../../../components/Ripple/Ripple';
import { Heading, Subheading } from '../../../components/Typography/Typography';
import FilterInputs from './FilterInputs';
import * as filterActionCreators from '../../../services/filter/filterDuck';
import analyticsService from '../../../services/analytics/analyticsService';
import { safeKeys } from '../../../services/utils';
import './filters.css';

class SearchResultsRefine extends Component {
  state = {
    showMore: {
      localities: false,
      amenities: false,
    },
  };

  componentDidMount() {
    const { filter: { filters }, filterActions, hotel, route, match } = this.props;
    if (isEmpty(filters.amenities)) {
      filterActions.getAmenitiesFilter(route, match.params);
    }
    analyticsService.pageViewed('Filters', {
      num_results: safeKeys(hotel.byId).length,
    });
  }

  toggleShowMore = (filterKey) => {
    this.setState((prevState) => ({
      showMore: {
        ...prevState.showMore,
        [filterKey]: !prevState.showMore[filterKey],
      },
    }));
  };

  clearAllFilters = () => {
    const { filterActions } = this.props;
    filterActions.clearAllFilters();
  };

  filtersCheckboxChange = (filterKey, i, checked) => {
    const { filterActions } = this.props;
    filterActions.filtersCheckboxChange(filterKey, i, checked);
    analyticsService.filtersApplied();
  };

  applySort = (sortItem) => () => {
    const { filterActions } = this.props;
    filterActions.sortBy(sortItem);
    analyticsService.sortApplied();
  }

  renderSort = () => {
    const { filter: { sort } } = this.props;
    return (
      <div className="filter__section">
        <Heading className="filter__section__title" type="3">Sort</Heading>
        {
          sort.list.map((sortItem) => (
            <label htmlFor={sortItem} className="filter__item" key={sortItem}>
              <input
                id={sortItem}
                className="filter__item__input"
                type="radio"
                onChange={this.applySort(sortItem)}
                checked={sortItem === sort.by}
              />
              <span className="text-capitalize">{sortItem}</span>
            </label>
          ))
        }
      </div>
    );
  }

  renderSectionHeader = (title, filterKey, isNewComponent) => {
    const { showMore } = this.state;
    const { filter: { filters } } = this.props;
    return (
      <Row between>
        <Col>
          <Heading className="filter__section__title" type="3">
            {title}
            {
              isNewComponent ? (<Chip tag="span" type="secondary">NEW</Chip>) : null
            }
          </Heading>
        </Col>
        {
          filters[filterKey].length > 3 ? (
            <Col>
              <Subheading
                tag="div"
                className="text--link text-right"
                onClick={() => this.toggleShowMore(filterKey)}
              >
                {showMore[filterKey] ? 'Show Less' : 'Show More'}
              </Subheading>
            </Col>
          ) : null
        }
      </Row>
    );
  }

  renderModalHeader = () => (
    <div className="filter__modal-header">
      <Heading>Filters</Heading>
      <Subheading tag="div" className="text--link filter__clear" onClick={this.clearAllFilters}>
        CLEAR ALL
      </Subheading>
    </div>
  )

  renderModalFooter = () => {
    const { filter: { refinedHotelIds }, onClose } = this.props;
    const noOfRefinedResults = refinedHotelIds.length;
    return (
      <div className="filter__modal-footer">
        <Row middle>
          <Col size="6">
            <Heading tag="div" type="3" className="filter__modal-footer__results">
              {noOfRefinedResults} results
            </Heading>
          </Col>
          <Col size="6">
            <Ripple>
              <Button block bold onClick={onClose}>APPLY</Button>
            </Ripple>
          </Col>
        </Row>
      </div>
    );
  }

  render() {
    const {
      onClose,
      hotel,
      filter: { filters },
      isCoupleFriendlyHotelAvailable,
    } = this.props;
    const { showMore } = this.state;
    const noOfTotalResults = hotel.ids.length;

    return (
      <Modal
        isOpen
        label="filters"
        header={this.renderModalHeader()}
        footer={this.renderModalFooter()}
        closeIcon="close"
        onClose={onClose}
      >
        <div className="filter">
          {this.renderSort()}
          {
            isCoupleFriendlyHotelAvailable ? (
              <div className="filter__section">
                {this.renderSectionHeader('Couple Friendly Treebos', 'showCoupleFriendly', true)}
                <label htmlFor="coupleFriendly" className="filter__item">
                  <input
                    id="coupleFriendly"
                    type="checkbox"
                    className="filter__item__input"
                    onChange={(e) => this.filtersCheckboxChange('showCoupleFriendly', 0, e.target.checked)}
                    checked={filters.showCoupleFriendly[0].checked}
                  />
                  <Subheading tag="span">View ALL</Subheading>
                </label>
              </div>
            ) : null
          }
          {
            noOfTotalResults >= 3 && !isEmpty(filters.priceRanges) ? (
              <div className="filter__section">
                {this.renderSectionHeader('Price', 'priceRanges')}
                <FilterInputs
                  filterItems={filters.priceRanges}
                  filterKey="priceRanges"
                  filtersCheckboxChange={this.filtersCheckboxChange}
                />
              </div>
            ) : null
          }

          <div className="filter__section">
            {this.renderSectionHeader('Locality', 'localities')}
            <FilterInputs
              filterItems={filters.localities}
              filterKey="localities"
              filtersCheckboxChange={this.filtersCheckboxChange}
              showMore={showMore.localities}
            />
          </div>

          <div className="filter__section">
            {this.renderSectionHeader('Amenities', 'amenities')}
            <FilterInputs
              filterItems={filters.amenities}
              filterKey="amenities"
              filtersCheckboxChange={this.filtersCheckboxChange}
              showMore={showMore.amenities}
              toggleShowMore={this.toggleShowMore}
            />
          </div>

          <div className="filter__section">
            <label htmlFor="availability" className="filter__item">
              <input
                id="availability"
                type="checkbox"
                className="filter__item__input"
                onChange={(e) => this.filtersCheckboxChange('showAvailableOnly', 0, e.target.checked)}
                checked={filters.showAvailableOnly[0].checked}
              />
              <Subheading tag="span">Show only available hotels</Subheading>
            </label>
          </div>
        </div>
      </Modal>
    );
  }
}

SearchResultsRefine.propTypes = {
  onClose: PropTypes.func.isRequired,
  hotel: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  filterActions: PropTypes.object.isRequired,
  isCoupleFriendlyHotelAvailable: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  hotel: state.hotel,
  filter: state.filter,
});

const mapDispatchToProps = (dispatch) => ({
  filterActions: bindActionCreators(filterActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
)(SearchResultsRefine);
