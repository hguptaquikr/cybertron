import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'leaf-ui/cjs/Button/web';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import View from 'leaf-ui/cjs/View/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import Size from 'leaf-ui/cjs/Size/web';
import TextInput from 'leaf-ui/cjs/TextInput/web';
import Modal from '../../../components/Modal/Modal';
import CouponList from './CouponList';
import './coupons.css';

class Coupons extends Component {
  state = {
    couponCode: this.props.coupon.appliedCouponCode,
  }

  componentWillReceiveProps(nextProps) {
    const { props } = this;
    if (props.coupon.appliedCouponCode !== nextProps.coupon.appliedCouponCode) {
      this.setState({ couponCode: nextProps.coupon.appliedCouponCode });
    }
  }

  onApplyCoupon = (couponCode = this.state.couponCode) => {
    const { hotel, couponActions } = this.props;
    this.setState({ couponCode });
    couponActions.applyCoupon(couponCode, hotel);
  }

  onChange = (e) => {
    this.setState({ couponCode: e.target.value });
  }

  render() {
    const { couponCode } = this.state;
    const {
      coupon: {
        results,
        sortedCouponCodes,
        showCouponModal,
        isCouponApplyLoading,
        error,
      },
      couponActions,
    } = this.props;
    const isApplyDisabled = isCouponApplyLoading || !couponCode.length;
    const coupons = sortedCouponCodes.map((sortedCouponCode) => results[sortedCouponCode]);

    return (
      <Modal
        isOpen={showCouponModal}
        closeIcon="close"
        onClose={() => couponActions.showCouponModal(false)}
        label="Apply coupon"
      >
        <Space padding={['0', '16px']}>
          <View>
            <Space margin={[0, 0, '20px', 0]}>
              <Flex flex="1 1 auto" flexDirection="row" alignItems="baseline">
                <View>
                  <TextInput
                    innerRef={this.storeInputRef}
                    type="text"
                    placeholder="Enter Coupon Code"
                    value={couponCode}
                    onChange={this.onChange}
                    name="coupon"
                    shape="sharpEdged"
                  />
                  <Size width="100%">
                    <Button
                      onClick={() => this.onApplyCoupon()}
                      disabled={isApplyDisabled}
                      shape="sharpEdged"
                    >
                      {isCouponApplyLoading ? 'Applying...' : 'Apply'}
                    </Button>
                  </Size>
                </View>
              </Flex>
            </Space>
            {
              error ? (
                <Space margin={['4px', 0, 0, 0]}>
                  <Text modifier="error" color="red">{error}</Text>
                </Space>
              ) : null
            }
            <CouponList
              coupons={coupons}
              applyCoupon={this.onApplyCoupon}
              appliedCouponCode={couponCode}
            />
          </View>
        </Space>
      </Modal>
    );
  }
}

Coupons.propTypes = {
  coupon: PropTypes.object.isRequired,
  couponActions: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
};

export default Coupons;
