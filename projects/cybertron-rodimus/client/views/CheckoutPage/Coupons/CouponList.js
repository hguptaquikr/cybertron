import React from 'react';
import PropTypes from 'prop-types';
import Button from 'leaf-ui/cjs/Button/web';
import Price from 'leaf-ui/cjs/Price/web';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';
import Size from 'leaf-ui/cjs/Size/web';

const CouponList = ({ coupons, applyCoupon }) => (
  <View>
    {
      coupons.map((coupon, index) => (
        <Space padding={[2, 0]} key={`${coupon}${index + 1}`}>
          <View>
            <Flex flexDirection="row">
              <View>
                <Flex>
                  <Space margin={[0, 2, 0, 0]}>
                    <Size width="200px">
                      <Button
                        block
                        modifier="secondary"
                        onClick={() => applyCoupon(coupon.code)}
                        size="small"
                      >
                        {coupon.code}
                      </Button>
                    </Size>
                  </Space>
                </Flex>
                {
                  coupon.amount ? (
                    <Text className="coupon-item__save" size="m">
                      Max. discount upto <Price>{coupon.amount}</Price>
                    </Text>
                  ) : null
                }
              </View>
            </Flex>
            <Space margin={['12px', 0, '4px', 0]}>
              <Text color="grey">
                {coupon.tagline}
              </Text>
            </Space>
            <Text color="grey">
              {coupon.description}
            </Text>
          </View>
        </Space>
      ))
    }
  </View>
);

CouponList.propTypes = {
  coupons: PropTypes.array.isRequired,
  applyCoupon: PropTypes.func.isRequired,
};

export default CouponList;
