import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { CardPay, PayAtHotel, Wallet, Netbank } from '../../components/Pay/Pay';
import { Row, Col } from '../../components/Flex/Flex';
import PageHeader from '../../components/PageHeader/PageHeader';
import Price from '../../components/Price/Price';
import { Text } from '../../components/Typography/Typography';
import * as loaderActionCreators from '../../services/loader/loaderDuck';
import androidService from '../../services/android/androidService';
import searchService from '../../services/search/searchService';
import analyticsService from '../../services/analytics/analyticsService';
import config from '../../../config';

class Payment extends Component {
  state = {
    banks: [],
  };

  componentDidMount() {
    const s = document.createElement('script');
    s.setAttribute('src', 'https://checkout.razorpay.com/v1/razorpay.js');
    s.onload = () => {
      window.Razorpay.configure({
        key: config.razorpayApiKey,
        image: '//images.treebohotels.com/images/payment/payment-logo.png',
      });
      window.Razorpay.payment.getMethods(({ netbanking }) => {
        this.setState({
          banks: Object.keys(netbanking).map((key) => ({ key, value: netbanking[key] })),
        });
      });
    };
    document.body.appendChild(s);
    analyticsService.pageViewed('Payment');
  }

  invokeRazorpay = (paymentData, bookingPromise, gateway) => {
    const { confirmBooking, loaderActions, showPaymentError } = this.props;
    const razorpay = new window.Razorpay({ });
    loaderActions.showLoader();
    razorpay.createPayment({}, {
      paused: true,
      message: 'Confirming order...',
    });

    razorpay.on('payment.success', async (res) => {
      await confirmBooking(res.razorpay_payment_id, res.razorpay_signature, gateway);
      loaderActions.hideLoader();
    });

    razorpay.on('payment.error', (res) => {
      loaderActions.hideLoader();
      if (this.cancelEmitted) {
        this.cancelEmitted = false;
      } else {
        showPaymentError(res.error.description);
      }
    });

    bookingPromise.then((bookingData) => {
      razorpay.emit('payment.resume', bookingData);
    }, () => {
      // Razorpay invokes error on its own.
      // They are in plans to fix this behaviour.
      // Will have to update once razorpay contacts us with the update
      this.cancelEmitted = true;
      razorpay.emit('payment.cancel');
    });
  };

  makePayment = (gateway) => async (data) => {
    const {
      booking: {
        guest: { email, mobile },
      },
      initiateBooking,
      selectedRatePlan,
    } = this.props;

    const bookingPromise = initiateBooking(gateway)
      .then((bookingOrderDetails) => {
        const { sellingPrice } = selectedRatePlan;
        const amount = +(sellingPrice * 100).toFixed(2);
        const paymentData = {
          amount,
          currency: 'INR',
          ...data,
        };
        return {
          ...paymentData,
          email,
          contact: mobile,
          notes: {
            order_id: bookingOrderDetails.orderId,
            service_order_id: bookingOrderDetails.serviceOrderId,
          },
          // This order id razorpay order id
          // payment signature from razorpay
          order_id: bookingOrderDetails.gatewayOrderId,
        };
      });
    if (androidService.isAndroid) {
      androidService.makePayment(null, bookingPromise, gateway);
    } else {
      this.invokeRazorpay(null, bookingPromise, gateway);
    }
  };

  render() {
    const { banks } = this.state;
    const {
      selectedPrice,
      selectedRatePlan,
      onPayAtHotelClick,
      isPayAtHotelEnabled,
      booking: { isPrepaidOnly },
      payAtHotel,
      initiateBooking,
      search: { datePicker },
    } = this.props;
    const { numberOfNights } = searchService.formattedDatePicker(datePicker.range, 'D MMM');
    const showPayAtHotel = !isPrepaidOnly && isPayAtHotelEnabled;

    return (
      <div className="cop__pay">
        <PageHeader>
          <Row middle tag="h1" className="heading-1">
            <Col size="3">Pay</Col>
            <Col size="9" className="text-right cop__price-text">
              <Text tag="div">
                <div className="cop__amt cop__amt--total">
                  <Price price={selectedRatePlan.sellingPrice} size="large" />
                </div>
                <Text tag="div" type="2">
                  Tax incl. price for {numberOfNights.text}
                </Text>
              </Text>
            </Col>
          </Row>
        </PageHeader>
        <CardPay
          price={selectedRatePlan.sellingPrice}
          makePayment={this.makePayment('razorpay')}
        />
        <div className="cop__pay-section">
          <Netbank
            banks={banks}
            makePayment={this.makePayment('razorpay')}
          />
        </div>
        <Wallet
          makePayment={this.makePayment('razorpay')}
          initiateBooking={initiateBooking}
          selectedPrice={selectedPrice}
          selectedRatePlan={selectedRatePlan}
        />
        {
          showPayAtHotel ? (
            <PayAtHotel
              selectedPrice={selectedPrice}
              selectedRatePlan={selectedRatePlan}
              payAtHotel={payAtHotel}
              onPayAtHotelClick={onPayAtHotelClick}
            />
          ) : null
        }
      </div>
    );
  }
}

Payment.propTypes = {
  isPayAtHotelEnabled: PropTypes.bool.isRequired,
  booking: PropTypes.object.isRequired,
  initiateBooking: PropTypes.func.isRequired,
  showPaymentError: PropTypes.func.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
  payAtHotel: PropTypes.func.isRequired,
  confirmBooking: PropTypes.func.isRequired,
  onPayAtHotelClick: PropTypes.func.isRequired,
  loaderActions: PropTypes.object.isRequired,
  selectedPrice: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

const mapDispatchToProps = (dispatch) => ({
  loaderActions: bindActionCreators(loaderActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Payment);
