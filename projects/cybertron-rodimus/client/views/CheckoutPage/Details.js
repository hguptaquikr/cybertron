import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Formsy from 'formsy-react';
import Input from '../../components/Input/Input';
import { Subheading, Heading } from '../../components/Typography/Typography';
import ABButton from '../../components/ABButton/ABButton';
import PageHeader from '../../components/PageHeader/PageHeader';
import * as abService from '../../services/ab/abService';
import analyticsService from '../../services/analytics/analyticsService';
import androidService from '../../services/android/androidService';

class Details extends Component {
  state = {
    isGSTFieldsVisible: this.props.isGSTFieldsVisible,
  }

  componentDidMount() {
    analyticsService.checkoutContentViewed(
      abService.impressionProperties(['checkoutDetailsCtaMobile']),
    );
  }

  onSubmit = (fields) => {
    const { isGSTFieldsVisible } = this.state;
    const { continueDetails, selectedRatePlan } = this.props;
    const bypassPaymentStep = selectedRatePlan.sellingPrice === 0;
    continueDetails({ details: fields, isGSTFieldsVisible, bypassPaymentStep, selectedRatePlan });
    analyticsService.updateClevertapProfile({
      name: fields.name,
      email: fields.email,
      phone: fields.mobile,
      organization_name: fields.organizationName,
      organization_taxcode: fields.organizationTaxcode,
      organization_address: fields.organizationAddress,
    });
  }

  toggleGSTFieldsVisibility = () => {
    this.setState((prevState) => ({
      isGSTFieldsVisible: !prevState.isGSTFieldsVisible,
    }), () => window.scrollTo(0, document.body.scrollHeight));
  }

  render() {
    const { isGSTFieldsVisible } = this.state;
    const {
      details: {
        name,
        email,
        mobile,
        organizationName,
        organizationAddress,
        organizationTaxcode,
      },
      selectedRatePlan,
    } = this.props;
    return (
      <div className="cop__details">
        <PageHeader>
          <Heading>Guest Details</Heading>
        </PageHeader>
        <Formsy className="cop__details-form" onValidSubmit={this.onSubmit} noValidate>
          <Input
            name="name"
            value={name}
            validationErrors={{
              isDefaultRequiredValue: 'Please enter the name',
            }}
            label="Name"
            required
            autoFocus
          />
          <Input
            type="number"
            pattern="\d*"
            name="mobile"
            value={mobile}
            validations="isNumeric,isLength:10"
            validationError="Please enter a valid mobile number"
            validationErrors={{
              isDefaultRequiredValue: 'Please enter the mobile number',
            }}
            label="Mobile Number"
            required
          />
          <Input
            type="email"
            name="email"
            value={email}
            validations="isEmail"
            validationError="Please enter a valid email"
            validationErrors={{
              isDefaultRequiredValue: 'Please enter the email',
            }}
            label="Email ID"
            required
          />
          {
            !androidService.isAndroid || androidService.getVersion() > 22 ? (
              <div>
                <label htmlFor="toggle-gst-fields">
                  <input
                    id="toggle-gst-fields"
                    type="checkbox"
                    onChange={() => this.toggleGSTFieldsVisibility()}
                    checked={isGSTFieldsVisible}
                  />
                  <Subheading tag="span" className="cop__details__toggle-gst-fields">
                    Use GSTIN for this booking (Optional)
                  </Subheading>
                </label>
                <Input
                  containerClassName={cx('cop__details-form__organization-name', { hide: !isGSTFieldsVisible })}
                  name="organizationName"
                  value={organizationName}
                  validationError="Please enter the company name"
                  label="Company Name"
                  required={isGSTFieldsVisible}
                  autoFocus
                />
                <Input
                  containerClassName={cx({ hide: !isGSTFieldsVisible })}
                  name="organizationTaxcode"
                  validationError="Please enter a valid GST Identification No."
                  label="GST Identification No."
                  value={organizationTaxcode}
                  validations={{
                    matchRegexp: /^[\d]{2}[A-Z]{5}[\d]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/,
                  }}
                  required={isGSTFieldsVisible}
                  autoFocus
                />
                <Input
                  containerClassName={cx({ hide: !isGSTFieldsVisible })}
                  name="organizationAddress"
                  value={organizationAddress}
                  validationError="Please enter Company Address"
                  label="Company Address"
                  required={isGSTFieldsVisible}
                  autoFocus
                />
              </div>
            ) : null
          }
          <div className="cop__footer">
            <ABButton
              id="t-itineraryDetailsCTA"
              block
              size="large"
              experiment="checkoutDetailsCtaMobile"
              render={(text) => (
                  selectedRatePlan.sellingPrice === 0 ?
                    'CONTINUE TO CONFIRM BOOKING' : (text || 'PAYMENT')
                )}
            />
          </div>
        </Formsy>
      </div>
    );
  }
}

Details.propTypes = {
  details: PropTypes.object.isRequired,
  continueDetails: PropTypes.func.isRequired,
  isGSTFieldsVisible: PropTypes.bool.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
};

export default Details;
