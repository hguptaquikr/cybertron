import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import ABButton from '../../components/ABButton/ABButton';
import Button from '../../components/Button/Button';
import { Row, Col } from '../../components/Flex/Flex';
import Image from '../../components/Image/Image';
import PageHeader from '../../components/PageHeader/PageHeader';
import Price from '../../components/Price/Price';
import Ripple from '../../components/Ripple/Ripple';
import RatePlanLabel from '../../components/RatePlanLabel/RatePlanLabel';
import NewYearRateTag from '../../components/NewYearRateTag/NewYearRateTag';
import RoomTypePrice from '../HotelDetailsPage/RoomType/RoomTypePrice/RoomTypePrice';
import NeedToKnow from '../../components/NeedToKnow/NeedToKnow';
import { Heading, Subheading, Text } from '../../components/Typography/Typography';
import Coupons from './Coupons/Coupons';
import * as abService from '../../services/ab/abService';
import analyticsService from '../../services/analytics/analyticsService';
import * as hotelService from '../../services/hotel/hotelService';
import searchService from '../../services/search/searchService';

class Review extends Component {
  onSearchClick = () => {
    const { uiActions } = this.props;
    uiActions.showSearchWidget(true);
  };

  applyCouponLinkClicked = async () => {
    const { couponActions, refreshBookingId } = this.props;
    analyticsService.applyCouponLinkClicked();
    await refreshBookingId();
    couponActions.showCouponModal(true);
  }

  removeCoupon = async () => {
    const { couponActions, refreshBookingId } = this.props;
    await refreshBookingId();
    couponActions.removeCoupon();
  }

  toggleWalletUsage = async (useTreeboPoints) => {
    const { refreshBookingId, walletActions } = this.props;
    await refreshBookingId();
    if (useTreeboPoints) {
      await walletActions.applyWallet();
      analyticsService.walletUsage(true);
    } else {
      await walletActions.removeWallet();
      analyticsService.walletUsage(false);
    }
  }

  render() {
    const {
      search: { datePicker, roomConfig },
      hotel,
      hotel: {
        id,
        name,
        address,
        images,
      },
      selectedPrice,
      selectedRatePlan,
      continueReview,
      coupon,
      checkout,
      couponActions,
      onRatePlanChange,
      treeboPoints,
    } = this.props;
    const addressJsx = address.locality ? `${address.locality}, ${address.city}` : address.city;
    const image = images[0] || {};
    const { checkIn, checkOut } = searchService.formattedDatePicker(datePicker.range);
    const formattedRoomConfig = searchService.formattedRoomConfig(roomConfig.rooms);
    const { numberOfNights } = searchService.formattedDatePicker(datePicker.range, 'D MMM');
    const isSingleRatePlan = Object.keys(selectedPrice.ratePlans).length === 1;
    const needToKnowPolicies = hotelService.getNeedToKnowPolicies(hotel.policies);

    return (
      <div className="cop">
        <PageHeader>
          <Heading>Review Booking</Heading>
        </PageHeader>
        <div className="cop__hdr">
          <div className="cop__hdr-sec">
            <Row>
              <Col size="4">
                <Image
                  src={`${Image.getCdnUrl(image.url)}`}
                  alt={image.tagline}
                />
              </Col>
              <Col size="8" className="cop__hdr-info">
                <div>
                  <Subheading
                    tag="h3"
                    className="cop__hdr-name text-truncate"
                    preview={!name}
                    previewStyle={{ width: '75%' }}
                  >
                    {name}
                  </Subheading>
                  <Text
                    type="2"
                    className="cop__hdr-address text-truncate"
                    preview={!addressJsx}
                    previewStyle={{ width: '60%' }}
                  >
                    {addressJsx}
                  </Text>
                </div>
                <Row middle className="cop__hdr__price">
                  <Price price={selectedRatePlan.sellingPrice} size="large" className="cop__price-final" />
                  <RatePlanLabel
                    className="cop__hdr__rate-plan-tag"
                    type={selectedRatePlan.type}
                    tag={selectedRatePlan.tag}
                  />
                </Row>
                <Text
                  type="2"
                  tag="div"
                  previewStyle={{ width: '100%' }}
                >
                 Tax incl. price for {numberOfNights.text}
                </Text>
              </Col>
            </Row>
          </div>
        </div>
        <div className="cop__review">
          <NewYearRateTag hotelId={id} />
          <div className="cop__bd">
            <div className="cop__bd-title">
              <Subheading className="cop__bd-title-text">Booking Details</Subheading>
              <div
                className="cop__bd-edit text--link"
                onClick={this.onSearchClick}
              >
                Edit Details
              </div>
            </div>
            <hr className="cop__strike" />
            <div>
              <Row middle className="cop__bd-details">
                <Col size="4" >
                  <Subheading className="cop__bd-label">Check In</Subheading>
                </Col>
                <Col size="8" >
                  <Subheading className="text-right cop__bd-text">{checkIn}</Subheading>
                </Col>
              </Row>
              <Row middle className="cop__bd-details">
                <Col size="4" >
                  <Subheading className="cop__bd-label">Check Out</Subheading>
                </Col>
                <Col size="8" >
                  <Subheading className="text-right cop__bd-text">{checkOut}</Subheading>
                </Col>
              </Row>
              <Row middle className="cop__bd-details">
                <Col size="4" >
                  <Subheading className="cop__bd-label">Guests</Subheading>
                </Col>
                <Col size="8" >
                  <Subheading className="text-right cop__bd-text">{formattedRoomConfig.text}</Subheading>
                </Col>
              </Row>
            </div>
          </div>
          {
            !coupon.appliedCouponCode ? (
              <Row
                middle
                className={cx('cop__apply-coupon', {
                  'cop__apply-coupon--disabled': checkout.isLoading,
                })}
                onClick={this.applyCouponLinkClicked}
              >
                <Col size="6" >
                  <Subheading className="cop__apply-coupon-text" tag="p">
                    {get(abService.getExperiments(), 'checkoutCouponMobile.payload.applyCouponText', 'Apply Coupon')}
                  </Subheading>
                </Col>
                <Col size="4" className="text-right">
                  <Button
                    className={cx('col-4 cop__coupon-btn', {
                      'cop__coupon-btn--disabled': checkout.isLoading,
                    })}
                    modifier="link"
                  >
                    {get(abService.getExperiments(), 'checkoutCouponMobile.payload.text', 'Apply')}
                  </Button>
                </Col>
              </Row>
            ) : (
              <Row
                middle
                className={cx('cop__remove-coupon', {
                  'cop__remove-coupon--disabled': checkout.isLoading,
                })}
                onClick={this.removeCoupon}
              >
                <Col size="6">
                  <Subheading className="cop__apply-coupon-text" tag="p">{`"${coupon.appliedCouponCode}" applied`}</Subheading>
                </Col>
                <Col size="4" className="text-right">
                  <Button
                    className="col-4 cop__coupon-btn"
                    modifier="link"
                  >
                    Remove
                  </Button>
                </Col>
              </Row>
            )
          }
          <div className="cop__cp">
            <Subheading className="cop__cp__title">
              { isSingleRatePlan ? 'View Plan' : 'Change Plan' }
            </Subheading>
            <RoomTypePrice
              onChange={onRatePlanChange}
              selectedPrice={selectedPrice}
              isRoomSelected
              selectedRatePlanCode={selectedRatePlan.code}
            />
          </div>
          {
            !isEmpty(needToKnowPolicies) ? (
              <div className="cop__ntk">
                <Subheading className="cop__ntk__title">Need to Know</Subheading>
                <NeedToKnow className="cop__ntk__body" needToKnowPolicies={needToKnowPolicies} />
              </div>
            ) : null
          }
          <div className="cop__bd">
            <div className="cop__bd-title">
              <Subheading className="cop__bd-header-text">Payment Details</Subheading>
            </div>
            <hr className="cop__strike" />
            {
              treeboPoints.usableBalance ? (
                <div className={cx('cop__bd__use-wallet', {
                    'cop__bd__use-wallet--disabled': checkout.isLoading,
                  })}
                >
                  <label htmlFor="cop-use-wallet-points">
                    <input
                      id="cop-use-wallet-points"
                      type="checkbox"
                      onChange={() => this.toggleWalletUsage(!treeboPoints.isUsingTreeboPoints)}
                      checked={treeboPoints.isUsingTreeboPoints}
                      className="cop__bd__use-wallet__checkbox"
                    />
                    <Subheading tag="span" className="cop__details__toggle-gst-fields">
                      Pay <Price price={treeboPoints.usableBalance} round /> using Treebo Points
                    </Subheading>
                  </label>
                </div>
              ) : null
            }
            <div>
              <Row middle className="cop__bd-details">
                <Col size="4" >
                  <Subheading className="cop__bd-label">Room Tariff</Subheading>
                </Col>
                <Col size="8" className="text-right cop__bd-text">
                  <Price className="cop__price-base" price={selectedRatePlan.basePrice} round={false} />
                </Col>
              </Row>
              <Row middle className="cop__bd-details">
                <Col size="4">
                  <Subheading className="cop__bd-label">Taxes</Subheading>
                </Col>
                <Col size="8" className="text-right cop__bd-text">
                  <Price price={selectedRatePlan.tax} round={false} className="cop__price-tax" />
                </Col>
              </Row>
              {
                selectedPrice.memberDiscount
                  && selectedPrice.memberDiscount.isApplied ? (
                    <Row className="cop__price-row" between >
                      <Col size="8">
                        <Subheading className="cop__bd-label cop__bd__sd" tag="span">
                          Member Discount
                        </Subheading>
                      </Col>
                      <Col size="4" className="text-right">
                      - <Price
                        price={selectedPrice.memberDiscount.value}
                        round={false}
                        className="cop__price-tax"
                        type="discount"
                      />
                      </Col>
                    </Row>
                ) : null
              }
              {
                coupon.appliedCouponCode ? (
                  <Row className="cop__price-row" between>
                    <Col size="8" >
                      <Subheading className="cop__bd-label" tag="span">
                        {`"${coupon.appliedCouponCode}" Applied`}
                      </Subheading>
                    </Col>
                    <Col size="4" className="text-right">
                      - <Price
                        price={selectedRatePlan.discountAmount}
                        round={false}
                        type="discount"
                        className="cop__price-discount"
                      />
                    </Col>
                  </Row>
                ) : null
              }
              {
                treeboPoints.isUsingTreeboPoints ? (
                  <Row middle className="cop__bd-details cop__bd-border-top">
                    <Col size="6" >
                      <Subheading>Total Tariff</Subheading>
                    </Col>
                    <Col size="6" className="text-right cop__bd-text">
                      <Price
                        price={selectedRatePlan.totalPrice}
                        size="regular"
                        className="cop__price-final"
                        round={false}
                      />
                    </Col>
                  </Row>
                ) : null
              }
              {
                treeboPoints.isUsingTreeboPoints ? (
                  <Row middle className="cop__bd-details cop__bd-border-top">
                    <Col size="8" >
                      <Subheading>Treebo Points Applied</Subheading>
                    </Col>
                    <Col size="4" className="text-right cop__bd-text">
                      - <Price
                        price={treeboPoints.usableBalance}
                        size="regular"
                        className="cop__price-final"
                        type="discount"
                        round={false}
                      />
                    </Col>
                  </Row>
                ) : null
              }
              <Row middle className="cop__bd-details cop__bd-border-top">
                <Col size="6" >
                  <Subheading>Total Payable</Subheading>
                </Col>
                <Col size="6" className="text-right cop__bd-text">
                  <Price price={selectedRatePlan.sellingPrice} size="large" className="cop__price-final" />
                </Col>
              </Row>
              <Row end className="cop__bd__rate-plan-tag">
                <RatePlanLabel type={selectedRatePlan.type} tag={selectedRatePlan.tag} />
              </Row>
            </div>
          </div>
          <Coupons
            coupon={coupon}
            couponActions={couponActions}
            hotel={hotel}
          />
        </div>
        <Row middle className="cop__footer cop__footer--review">
          <Col size="6" className="text-right">
            <Ripple>
              <ABButton
                id="t-itineraryCTA"
                block
                size="large"
                onClick={continueReview}
                experiment="checkoutCtaMobile"
                render={(text) => text || 'ADD DETAILS'}
              />
            </Ripple>
          </Col>
        </Row>
      </div>
    );
  }
}

Review.propTypes = {
  continueReview: PropTypes.func.isRequired,
  coupon: PropTypes.object.isRequired,
  checkout: PropTypes.object.isRequired,
  couponActions: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  onRatePlanChange: PropTypes.func.isRequired,
  selectedPrice: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
  refreshBookingId: PropTypes.func.isRequired,
  treeboPoints: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
  uiActions: PropTypes.object.isRequired,
};

export default Review;
