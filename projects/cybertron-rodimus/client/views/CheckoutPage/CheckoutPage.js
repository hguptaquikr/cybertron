import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import kebabCase from 'lodash/kebabCase';
import qs from 'query-string';
import moment from 'moment';
import isEmpty from 'lodash/isEmpty';
import NotificationModal from '../../components/NotificationModal/NotificationModal';
import Otp from '../../components/Otp/Otp';
import SearchWidgetAsync from '../SearchWidget/SearchWidgetAsync';
import Review from './Review';
import Details from './Details';
import Payment from './Payment';
import ConfirmationPage from '../ConfirmationPage';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as checkoutActionCreators from '../../services/checkout/checkoutDuck';
import * as loaderActionCreators from '../../services/loader/loaderDuck';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as couponActionCreators from '../../services/coupon/couponDuck';
import * as walletActionCreators from '../../services/wallet/walletDuck';
import * as uiActionCreators from '../../services/ui/uiDuck';
import * as abService from '../../services/ab/abService';
import analyticsService from '../../services/analytics/analyticsService';
import priceService from '../../services/price/priceService';
import searchService from '../../services/search/searchService';
import { constants } from '../../services/utils';
import * as roomTypeService from '../../services/roomType/roomTypeService';
import './checkoutPage.css';

const CheckoutSection = ({ step, currentStep, children }) => (
  step === currentStep ? children : null
);

CheckoutSection.propTypes = {
  step: PropTypes.number.isRequired,
  currentStep: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired,
};

class CheckoutPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match, req }) => {
    const promises = [];
    dispatch(searchActionCreators.setSearchState({ ...req.query, ...match.params }));
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    return Promise.all(promises);
  }

  state = {
    showError: false,
    errorMessage: '',
    errorType: 'soldOut',
    redirectUrl: {},
    showOtpModal: false,
    onRetry: () => {},
  };

  componentDidMount() {
    const {
      checkout: { step },
      searchActions,
      contentActions,
      checkoutActions,
      history,
      location,
      match,
      route,
      price,
    } = this.props;

    const query = qs.parse(location.search);

    if (+query.singlePageHd && isEmpty(price.roomTypeIds)) {
      history.go(-1);
    } else {
      let changeStep = step;
      if (+query.singlePageHd) {
        changeStep = 3;
        checkoutActions.goToStep(changeStep);
      }

      if ((query.step || 1) !== step) {
        history.replace({
          pathname: location.pathname,
          search: qs.stringify({
            ...query,
            step: changeStep,
          }),
        });
      }

      searchActions.setSearchState({ ...qs.parse(location.search), ...match.params });
      contentActions.getSeoContent(route, match.params);
      this.getCheckoutDetails(true);
      this.captureAnalytics();
      SearchWidgetAsync.preload();
      ConfirmationPage.preload();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { props } = this;
    const { searchActions, checkoutActions } = this.props;
    const query = qs.parse(props.location.search);
    const nextQuery = qs.parse(nextProps.location.search);

    const nextStep = +nextQuery.step || 1;

    if (props.checkout.step > nextStep) {
      checkoutActions.goToStep(nextStep);
    }

    if (
      query.hotel_id === nextQuery.hotel_id
        && (query.checkin !== nextQuery.checkin
            || query.checkout !== nextQuery.checkout
            || query.roomconfig !== nextQuery.roomconfig)
    ) {
      searchActions.setSearchState({ ...nextQuery, ...nextProps.match.params });
      nextProps.contentActions.getSeoContent(nextProps.route, nextProps.match.params);
      this.getCheckoutDetails(true);
    }
  }

  onPayAtHotelClick = (hotelId, selectedPrice, selectedRatePlan) => () => {
    analyticsService.payAtHotelClicked(hotelId, selectedPrice, selectedRatePlan);
  }

  getCheckoutDetails = async (useBookingId) => {
    const {
      location,
      checkoutActions,
      couponActions,
      history,
    } = this.props;
    try {
      const query = qs.parse(location.search);
      const bookingId = useBookingId ? query.bid : null;
      const bookingDetails = await checkoutActions.getCheckoutDetails(bookingId);
      if (bookingDetails.payload.errors && bookingDetails.payload.errors[0].code === 'WEB_4017') {
        history.push({
          pathname: '/search/',
          search: location.search,
        });
      }
      const { bid } = bookingDetails.payload.booking;
      await couponActions.getCoupons(bid);
      this.replaceItineraryUrl(bid);
    } catch (error) {
      this.showPaymentError(error.msg);
    }
  }

  getSelectedRoomTypeId = (prices) => {
    const { location } = this.props;
    const query = qs.parse(location.search);
    return query.roomtype
      && prices[roomTypeService.makeRoomTypeId(query.hotel_id, query.roomtype)]
      && prices[roomTypeService.makeRoomTypeId(query.hotel_id, query.roomtype)].available
      ? roomTypeService.makeRoomTypeId(query.hotel_id, query.roomtype) : '0|oak';
  }

  getSelectedRatePlan = (selectedPrice) => {
    const { location } = this.props;
    const query = qs.parse(location.search);
    return query.rateplan && selectedPrice.ratePlans[query.rateplan]
      ? selectedPrice.ratePlans[query.rateplan]
      : priceService.sortRatePlan(selectedPrice.ratePlans)[0];
  }

  captureAnalytics() {
    const {
      hotel,
      search: {
        roomConfig,
        datePicker: {
          range,
        },
      },
      match,
      location,
    } = this.props;

    const { checkIn, checkOut, numberOfNights, abwDuration } =
      searchService.formattedDatePicker(range, constants.dateFormat.query);

    const analyticsProperties = {
      hotelId: match.params.hotelId,
      hotelName: hotel.name,
      pageUrl: location.pathname,
      checkin: checkIn,
      checkout: checkOut,
      lengthOfStay: numberOfNights.count,
      abwDays: abwDuration,
      roomConfig: roomConfig.rooms.map((room) => `${room.adults}-${room.kids || 0}`).join(','),
      ...abService.impressionProperties(['checkoutCtaMobile', 'checkoutCouponMobile']),
    };
    analyticsService.itineraryContentViewed(analyticsProperties);
  }

  replaceItineraryUrl = (bid) => {
    const { history, location } = this.props;
    if (history.location.pathname === '/itinerary/') {
      history.replace({
        pathname: location.pathname,
        search: qs.stringify({
          ...qs.parse(location.search),
          bid,
        }),
      });
    }
  };

  pushStep = () => {
    const {
      checkout: { step },
      history,
      location,
    } = this.props;
    const nextStep = step + 1;

    history.push({
      pathname: location.pathname,
      search: qs.stringify({
        ...qs.parse(location.search),
        step: nextStep,
      }),
    });
  };

  redirectToBooking = ({ payload, error }) => {
    const { history, loaderActions, location } = this.props;
    loaderActions.hideLoader();
    if (error) {
      this.showSoldOutError(payload.error.msg);
    } else {
      const query = qs.parse(location.search);
      history.push({
        pathname: `/confirmation/${payload.orderId}/`,
        search: qs.stringify({
          rateplan: query.rateplan,
        }),
      });
    }
  };

  initiateBooking = (selectedRatePlan) => async (gateway) => {
    const {
      checkoutActions: { initiateBooking },
    } = this.props;
    await this.refreshBookingId();
    return initiateBooking(selectedRatePlan, gateway)
      .then((bookingOrderDetails) => {
        if (bookingOrderDetails.error) {
          const { code: errorCode, msg: errorMsg } = bookingOrderDetails.payload.error;
          if (errorCode === '5006') {
            this.showInsufficientWalletBalanceError(errorMsg, 'error', {});
          } else {
            this.showSoldOutError(errorMsg, 'error', {});
          }
          return Promise.reject();
        }
        return bookingOrderDetails.payload;
      });
  };

  payAtHotel = async (selectedRatePlan) => {
    const {
      booking: { guest },
      checkout: { isOtpEnabled },
      checkoutActions,
      checkoutActions: { isVerifiedNumber, sendCheckoutOtp },
      loaderActions,
      searchActions,
      location,
      match,
      history,
    } = this.props;
    loaderActions.showLoader();
    const query = qs.parse(location.search);
    const isRatePlanChanged = query.rateplan !== selectedRatePlan.code;
    if (isRatePlanChanged) {
      searchActions.setSearchState({
        ...query,
        rateplan: selectedRatePlan.code,
        ...match.params,
      });
      await checkoutActions.getCheckoutDetails(query.bid);
      history.replace({
        pathname: location.pathname,
        search: qs.stringify({ ...query, rateplan: selectedRatePlan.code }),
      });
    }
    if (!isOtpEnabled) {
      await this.otpSuccess(selectedRatePlan);
    } else {
      const otpData = await isVerifiedNumber(guest.mobile);
      if (otpData.payload.isOtpVerified) {
        await this.otpSuccess(selectedRatePlan);
      } else {
        loaderActions.hideLoader();
        this.showOtpModal(true);
        await sendCheckoutOtp(guest.mobile);
      }
    }
  };

  verifyOtp = ({ otp }, reset, invalidate) => {
    const {
      booking: { guest },
      checkoutActions: { verifyCheckoutOtp },
    } = this.props;

    verifyCheckoutOtp(guest.mobile, otp)
      .then((res) => {
        if (res.error) {
          invalidate({ otp: res.payload.errors[0].message });
        } else if (!res.payload.data.is_valid) {
          invalidate({ otp: 'Otp Expired, Resend OTP' });
        } else {
          this.otpSuccess(res);
        }
      });
  }

  resendOtp = () => {
    const {
      booking: { guest },
      checkoutActions: { sendCheckoutOtp },
    } = this.props;
    sendCheckoutOtp(guest.mobile);
  }

  showOtpModal = (visibility) => {
    this.setState({ showOtpModal: visibility });
  }

  otpSuccess = async (selectedRatePlan) => {
    const { checkoutActions: { payAtHotel }, loaderActions } = this.props;
    loaderActions.showLoader();
    this.showOtpModal(false);
    await this.refreshBookingId();
    return payAtHotel(selectedRatePlan).then(this.redirectToBooking);
  };

  confirmBooking = (selectedRatePlan) => async (id, signature, gateway) => {
    const {
      checkoutActions: { confirmBooking },
    } = this.props;
    try {
      const bookingDetails = await confirmBooking(id, signature, selectedRatePlan, gateway);
      this.redirectToBooking(bookingDetails);
    } catch (error) {
      this.showPaymentError();
    }
  };

  continueReview = (selectedPrice, selectedRatePlan) => () => {
    const {
      hotel,
      coupon: { appliedCouponCode },
      checkoutActions: { continueReview },
      $roomType,
    } = this.props;

    const hotelDetails = {
      hotel_id: hotel.id,
      hotel_name: hotel.name,
      ...hotel.address,
    };

    const roomType = $roomType.byId[selectedPrice.id];
    this.pushStep();
    continueReview(appliedCouponCode, roomType, selectedRatePlan, hotelDetails);
  };

  continueDetails = ({
    details,
    isGSTFieldsVisible,
    bypassPaymentStep,
    selectedRatePlan,
  }) => {
    this.props.checkoutActions.continueDetails({
      details,
      isGSTFieldsVisible,
      bypassPaymentStep,
    });
    if (bypassPaymentStep) this.bypassPaymentStep(selectedRatePlan);
    else this.pushStep();
  };

  showPayError = (errorMessage, errorType, redirectUrl, onRetry) => {
    this.setState({
      showError: true,
      errorMessage,
      errorType,
      redirectUrl,
      onRetry,
    });
  };

  showSoldOutError = (errorMessage) => {
    const { location } = this.props;
    const query = qs.parse(location.search);
    const redirectUrl = {
      pathname: `/hotels-in-${kebabCase(query.city)}/`,
      search: qs.stringify({
        checkin: query.checkin,
        checkout: query.checkout,
        roomconfig: query.roomconfig,
      }),
    };
    this.showPayError(errorMessage, 'soldOut', redirectUrl);
  };

  showInsufficientWalletBalanceError = (errorMessage) => {
    const { checkoutActions } = this.props;
    const onRetry = () => {
      this.hidePaymentError();
      setTimeout(() => {
        checkoutActions.goToStep(1);
        this.getCheckoutDetails(false);
      }, 200);
    };
    this.showPayError(errorMessage, 'insufficientWalletBalance', null, onRetry);
  }

  showPaymentError = (errorMessage) => {
    this.showPayError(errorMessage, 'error', {});
  };

  hidePaymentError = () => {
    this.setState({ showError: false });
  };

  changeRatePlan = (oldPrice, oldRatePlan) => (newRatePlan, newPrice) => {
    const {
      searchActions,
      search: {
        datePicker,
      },
      history,
      location,
      match,
      $roomType,
    } = this.props;
    const newRoomType = $roomType.byId[newPrice.id];
    const oldRoomType = $roomType.byId[oldPrice.id];
    const query = qs.parse(location.search);
    const abwDuration = typeof datePicker.range.start === 'object'
      && datePicker.range.start.startOf('day').diff(moment().startOf('day'), 'days');

    history.replace({
      pathname: location.pathname,
      search: qs.stringify({
        ...query,
        rateplan: newRatePlan.code,
      }),
    });

    analyticsService.ratePlanChanged({
      hotelId: query.hotel_id,
      oldRatePlan,
      newRatePlan,
      oldRoomType,
      newRoomType,
      abwDuration,
      page: 'Itinerary Page',
    });

    searchActions.setSearchState({
      ...query,
      rateplan: newRatePlan.code,
      ...match.params,
    });
    this.getCheckoutDetails(true);
  }

  bypassPaymentStep = (selectedRatePlan) => {
    const { loaderActions } = this.props;
    loaderActions.showLoader();
    this.initiateBooking(selectedRatePlan)('razorpay')
      .then((bookingDetails) => {
        if (bookingDetails.error) this.showPaymentError(bookingDetails.error.description);
        else this.confirmBooking(selectedRatePlan)(null, null, 'razorpay');
      })
      .catch(loaderActions.hideLoader);
  }

  refreshBookingId = async () => {
    const { booking } = this.props;
    const isOrderIdFetched = booking.payment.orderId;
    if (isOrderIdFetched) await this.getCheckoutDetails();
  }

  render() {
    const {
      checkout,
      checkout: {
        step,
        isPayAtHotelEnabled,
        isVerifyingOtp,
      },
      checkoutActions: { updateCheckoutMobile },
      price,
      hotel,
      booking,
      search,
      searchActions,
      coupon,
      couponActions,
      auth,
      treeboPoints,
      loaderActions,
      walletActions,
      uiActions,
    } = this.props;
    const {
      showError,
      errorMessage,
      errorType,
      redirectUrl,
      showOtpModal,
      onRetry,
    } = this.state;
    const roomTypeIds = [this.getSelectedRoomTypeId(price.byRoomTypeId)] || hotel.roomTypeIds;
    const hotelPrices = priceService.getPricesForRoomTypeIds(roomTypeIds, price);
    const selectedPrice = priceService.sortPrice(hotelPrices)[0];
    const selectedRatePlan = this.getSelectedRatePlan(selectedPrice) || {};
    const prefillDetails = booking.guest.name ? booking.guest : auth.profile;

    return (
      <div className="cop">
        <CheckoutSection step={1} currentStep={step}>
          <Review
            search={search}
            searchActions={searchActions}
            uiActions={uiActions}
            hotel={hotel}
            selectedPrice={selectedPrice}
            selectedRatePlan={selectedRatePlan}
            coupon={coupon}
            checkout={checkout}
            treeboPoints={treeboPoints}
            couponActions={couponActions}
            loaderActions={loaderActions}
            walletActions={walletActions}
            continueReview={this.continueReview(selectedPrice, selectedRatePlan)}
            onRatePlanChange={this.changeRatePlan(selectedPrice, selectedRatePlan)}
            refreshBookingId={this.refreshBookingId}
          />
        </CheckoutSection>
        <CheckoutSection step={2} currentStep={step}>
          <Details
            details={prefillDetails}
            continueDetails={this.continueDetails}
            isGSTFieldsVisible={booking.isGSTFieldsVisible}
            selectedRatePlan={selectedRatePlan}
          />
        </CheckoutSection>
        <CheckoutSection step={3} currentStep={step}>
          <Payment
            initiateBooking={this.initiateBooking(selectedRatePlan)}
            payAtHotel={this.payAtHotel}
            fetchCheckoutDetailsOnPayAtHotel={this.fetchCheckoutDetailsOnPayAtHotel}
            isPayAtHotelEnabled={isPayAtHotelEnabled}
            confirmBooking={this.confirmBooking(selectedRatePlan)}
            selectedPrice={selectedPrice}
            selectedRatePlan={selectedRatePlan}
            showPaymentError={this.showPaymentError}
            booking={booking}
            onPayAtHotelClick={
                  this.onPayAtHotelClick(hotel.id, selectedPrice, selectedRatePlan)
                }
          />
        </CheckoutSection>
        <Otp
          isOpen={showOtpModal}
          onClose={() => this.showOtpModal(false)}
          closeButton="SKIP, TO PAY NOW"
          mobile={booking.guest.mobile}
          onMobileChanged={updateCheckoutMobile}
          onResendOtp={this.resendOtp}
          onVerifyOtp={this.verifyOtp}
          isVerifyingOtp={isVerifyingOtp}
        />
        <NotificationModal
          show={showError}
          message={errorMessage}
          onClose={this.hidePaymentError}
          onRetry={onRetry}
          redirectUrl={redirectUrl}
          type={errorType}
        />
      </div>
    );
  }
}

CheckoutPage.propTypes = {
  route: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  checkout: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  booking: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  coupon: PropTypes.object.isRequired,
  couponActions: PropTypes.object.isRequired,
  loaderActions: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  treeboPoints: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  checkoutActions: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
  uiActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state, { location }) => ({
  checkout: state.checkout,
  search: state.search,
  booking: state.booking,
  coupon: state.coupon,
  hotel: state.hotel.byId[qs.parse(location.search).hotel_id] || state.hotel.byId[0],
  price: state.price,
  auth: state.auth,
  treeboPoints: state.wallet.byType.TP || state.wallet.byType[''],
  $roomType: state.$roomType,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  checkoutActions: bindActionCreators(checkoutActionCreators, dispatch),
  loaderActions: bindActionCreators(loaderActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  couponActions: bindActionCreators(couponActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
  uiActions: bindActionCreators(uiActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CheckoutPage);
