import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('CheckoutPage');
    return import('./CheckoutPage' /* webpackChunkName: 'CheckoutPage' */);
  },
  loading: () => null,
});
