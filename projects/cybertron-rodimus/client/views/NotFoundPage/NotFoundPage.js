import React from 'react';
import image404 from './404.png';

const NotFoundPage = () => (
  <img src={image404} alt="404" style={{ width: '100%' }} className="old-image" />
);

export default NotFoundPage;
