import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('NotFoundPage');
    return import('./NotFoundPage' /* webpackChunkName: 'NotFoundPage' */);
  },
  loading: () => null,
});
