import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import qs from 'query-string';
import Text from 'leaf-ui/cjs/Text/web';
import Image from 'leaf-ui/cjs/Image/web';
import Space from 'leaf-ui/cjs/Space/web';
import View from 'leaf-ui/cjs/Card/web';
import { Col, Row } from '../../components/Flex/Flex';
import Button from '../../components/Button/Button';
import Price from '../../components/Price/Price';
import Ripple from '../../components/Ripple/Ripple';
import NeedToKnow from '../../components/NeedToKnow/NeedToKnow';
import NewYearRateTag from '../../components/NewYearRateTag/NewYearRateTag';
import LandingPage from '../LandingPage';
import { Heading, Subheading } from '../../components/Typography/Typography';
import PerfectStayBanner from '../../components/PerfectStayBanner/PerfectStayBanner';
import * as bookingActionCreators from '../../services/booking/bookingDuck';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as hotelService from '../../services/hotel/hotelService';
import androidService from '../../services/android/androidService';
import RatePlanLabel from '../../components/RatePlanLabel/RatePlanLabel';
import PaymentStatusFailure from './PaymentStatus/PaymentStatusFailure';
import PaymentStatusPending from './PaymentStatus/PaymentStatusPending';
import PaymentStatusSuccess from './PaymentStatus/PaymentStatusSuccess';
import './confirmationPage.css';

class ConfirmationPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(contentActionCreators.getRewardsContent()));
    promises.push(dispatch(
      bookingActionCreators.getBookingConfirmationDetails(match.params.bookingId),
    ));
    return Promise.all(promises);
  }

  componentDidMount() {
    const {
      match,
      contentActions,
      bookingActions,
      route,
      location,
    } = this.props;
    bookingActions.getBookingConfirmationDetails(match.params.bookingId)
      .then((res) => {
        const { booking } = res.payload;
        const query = qs.parse(location.search);
        const isPayAtHotel = get(booking, 'payment.isPayAtHotel', false);
        const isPending = query.status === 'pending';
        if (!isPayAtHotel && isPending) {
          this.interval = setInterval(() => {
            bookingActions.getPaymentConfirmationDetails(match.params.bookingId);
          }, 5000);
        }
      });
    contentActions.getSeoContent(route, match.params);
    contentActions.getRewardsContent();
    LandingPage.preload();
  }

  getPaymentStatusLabel =() => {
    const {
      booking: {
        payment: {
          isPayAtHotel,
        },
        status,
      },
      location,
    } = this.props;
    const query = qs.parse(location.search);
    const paymentStatus = status || query.status;
    let label = '';
    switch (true) {
      case !isPayAtHotel && paymentStatus === 'failure':
        label = 'Payable at Hotel';
        break;
      case !isPayAtHotel && paymentStatus === 'success':
        label = 'Paid';
        break;
      case !isPayAtHotel && paymentStatus === 'pending':
        label = 'Payment Status Pending';
        break;
      case isPayAtHotel:
        label = 'Payable at Hotel';
        break;
      case !paymentStatus && !isPayAtHotel:
        label = 'Paid';
        break;
      default:
        label = '';
    }
    return label;
  }

  shouldStopFetchingPaymentStatus = () => {
    const { booking: { status, attempts, maxAttempts } } = this.props;
    if (status !== 'pending' || attempts >= maxAttempts) {
      clearInterval(this.interval);
    }
  }

  redirectToFaq = () => {
    const { history } = this.props;
    history.push('/faq/');
  }

  render() {
    const {
      booking: {
        roomTypeId,
        guest,
        room,
        dates,
        payment: {
          orderId,
          partialPaymentAmount,
          partialPaymentLink,
        },
        ratePlanCode,
        status,
        isError,
      },
      content,
      hotel,
      hotel: {
        id,
        address,
        coordinates = {},
      },
      prices,
      location,
      history,
      auth,
    } = this.props;
    const query = qs.parse(location.search);
    const selectedRatePlanCode = query.rateplan || ratePlanCode;
    const paymentStatus = status || query.status;
    const ratePlan = !isEmpty(prices[roomTypeId].ratePlans) ? (
      prices[roomTypeId].ratePlans[selectedRatePlanCode]
    ) : {};
    let shouldCollectPartialPayment = !!partialPaymentLink;
    if (androidService.isAndroid) {
      // only version >= 9 has partial payment handling enabled
      shouldCollectPartialPayment =
        shouldCollectPartialPayment && androidService.getVersion() >= 9;
    }
    const needToKnowPolicies = hotelService.getNeedToKnowPolicies(hotel.policies);
    const label = this.getPaymentStatusLabel();
    this.shouldStopFetchingPaymentStatus();
    if (isError) clearInterval(this.interval);
    return (
      <div className="cp">
        <div className="cp__header">
          <Heading className=" cp__heading" tag="h2">
            {
              shouldCollectPartialPayment ? (
                'Your Booking is Temporarily Confirmed'
              ) : (
                'Your Booking is Confirmed!'
              )
            }
          </Heading>
          <Subheading
            className="cp__hotel-name"
            preview={!hotel.name}
            previewStyle={{ width: '80px' }}
            tag="p"
          >
            {hotel.name}
          </Subheading>
          <Text
            type="2"
            className="cp__hotel-address"
            preview={!address.street}
            previewStyle={{ width: '100px' }}
          >
            {hotelService.constructAddress(address)}
          </Text>
          <a
            className="cp__hotel-directions"
            href={`https://www.google.com/maps/search/?api=1&query=${coordinates.lat},${coordinates.lng}`}
            rel={androidService.isAndroid ? '' : 'noopener noreferrer'}
            target="_blank"
          >
            <i className="icon-directions cp__directions-icon" />
            <span className="cp__directions-text">Get Directions</span>
          </a>
          <div className="cp__treebo-rewards" onClick={this.redirectToFaq}>
            <Subheading className="cp__treebo-rewards__text" type="2" tag="span">
              Treebo Points will be credited to {guest.mobile} on checkout.
              <span className="text--link">View T&C</span>
            </Subheading>
          </div>
        </div>
        <Space padding={[2]}>
          <View>
            <PerfectStayBanner />
          </View>
        </Space>
        <NeedToKnow className="cp__ntk" needToKnowPolicies={needToKnowPolicies} />
        {{
          failure: (
            <PaymentStatusFailure />
          ),
          pending: (
            <PaymentStatusPending />
          ),
          success: (
            <PaymentStatusSuccess />
          ),
        }[paymentStatus]}
        {
          !auth.isAuthenticated ? (
            <div
              className="cp__container cp__login-img"
              onClick={() => history.push({ pathname: '/login/' })}
            >
              <Image
                src={content.rewards.confirmationPage.imageUrl}
                alt={content.rewards.confirmationPage.tagLine}
                width="100%"
                height="100%"
              />
            </div>
          ) : null
        }
        <div className="bd">
          <div className="bd__container--top bd__container">
            <Text type="2">Primary Guest</Text>
            <Subheading className="cp__section cp__guest-name" preview={!guest.name} tag="p">{guest.name}</Subheading>
            <Row className="cp__section">
              <Col size="6">
                <Text type="2">Check In</Text>
                <Subheading className="cp__section__text cp__check-in" preview={!dates.checkIn} tag="p">{dates.checkIn}</Subheading>
              </Col>
              <Col size="6">
                <Text type="2">Check Out</Text>
                <Subheading className="cp__section__text cp__check-out" preview={!dates.checkOut} tag="p">{dates.checkOut}</Subheading>
              </Col>
            </Row>
            <Row className="cp__section">
              <Col size="6">
                <Text type="2">Nights</Text>
                <Subheading className="cp__section__text cp__number-of-nights" preview={!dates.numberOfNights.count} tag="p">{dates.numberOfNights.count}</Subheading>
              </Col>
              <Col size="6">
                <Text type="2">Rooms</Text>
                <Subheading className="cp__section__text cp__room-count" preview={!room.count} tag="p">{room.count}</Subheading>
              </Col>
            </Row>
            <Row>
              <Col>
                <Text type="2">Booking ID</Text>
                <Subheading className="cp__section__text cp__booking-id" preview={!orderId} tag="p">{orderId}</Subheading>
              </Col>
            </Row>
          </div>
          <div className="bd__container">
            <Row>
              <Col>
                <Text type="2">
                  {label}
                </Text>
                <Price
                  className="cp__section__text cp__selling-price"
                  size="large"
                  preview={ratePlan ? !ratePlan.sellingPrice : ''}
                  price={ratePlan ? ratePlan.sellingPrice : ''}
                />
                <RatePlanLabel
                  type={ratePlan ? ratePlan.type : ''}
                  tag={ratePlan ? ratePlan.tag : ''}
                />
              </Col>
            </Row>
          </div>
        </div>
        <NewYearRateTag
          hotelId={id}
          range={{
            start: dates.checkInDate,
            end: dates.checkOutDate,
          }}
        />
        <Ripple>
          <Link className="cp__container" to="/">
            <Button flat block large modifier="secondary">BACK TO HOME</Button>
          </Link>
        </Ripple>
        {
          shouldCollectPartialPayment ? (
            <Button flat block large className="floaty">
              <a
                href={partialPaymentLink}
                rel={androidService.isAndroid ? '' : 'noopener noreferrer'}
                target="_blank"
              >
                Pay &#x20b9;{partialPaymentAmount} To Confirm
              </a>
            </Button>
          ) : null
        }
      </div>
    );
  }
}

ConfirmationPage.propTypes = {
  route: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  booking: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  prices: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  bookingActions: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  booking: state.booking,
  content: state.content,
  hotel: state.hotel.byId[state.booking.hotelId],
  prices: state.price.byRoomTypeId,
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  bookingActions: bindActionCreators(bookingActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ConfirmationPage);
