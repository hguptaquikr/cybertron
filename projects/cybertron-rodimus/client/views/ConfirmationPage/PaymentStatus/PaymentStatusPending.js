import React from 'react';
import Text from 'leaf-ui/cjs/Text/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import Space from 'leaf-ui/cjs/Space/web';
import Image from 'leaf-ui/cjs/Image/web';
import View from 'leaf-ui/cjs/View/web';
import Card from 'leaf-ui/cjs/Card/web';

const PaymentStatusPending = () => (
  <Space
    padding={[3]}
    style={{ borderBottom: '24px solid #f1f1f1' }}
  >
    <Card backgroundColor="white">
      <Flex
        flexDirection="row"
        justifyContent="space-around"
        alignItems="center"
      >
        <View>
          <Image
            src="http://images.treebohotels.com/images/computer.svg"
            alt="Treebo"
            height="16px"
            width="16px"
          />
          <Flex
            justifyContent="center"
            alignItems="center"
          >
            <View>
              <Text
                color="greyDark"
                size="s"
                className="payment-process"
              >
              Your payment is being processed
                <Text component="span">.</Text>
                <Text component="span">.</Text>
                <Text component="span">.</Text>
              </Text>
              <Text
                color="greyDarker"
                size="s"
              >
              Booking is confirmed. Rest Assured
              </Text>
            </View>
          </Flex>
        </View>
      </Flex>
    </Card>
  </Space>
);

export default PaymentStatusPending;
