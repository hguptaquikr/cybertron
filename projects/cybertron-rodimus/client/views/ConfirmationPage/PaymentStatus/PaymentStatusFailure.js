import React from 'react';
import Text from 'leaf-ui/cjs/Text/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import Space from 'leaf-ui/cjs/Space/web';
import Card from 'leaf-ui/cjs/Card/web';
import View from 'leaf-ui/cjs/View/web';

const PaymentStatusFailure = () => (
  <Space style={{ border: '24px solid #f1f1f1' }}>
    <Text component="div">
      <Card backgroundColor="redLighter">
        <Space
          padding={[2]}
          style={{ border: 'solid 1px #ffa3ab' }}
        >
          <Flex>
            <View>
              <Space margin={[0, 0, 1]}>
                <Text
                  color="greyDarker"
                  size="m"
                >
                Payment Failed
                </Text>
              </Space>
              <Text
                color="greyDark"
                size="s"
              >
                Your payment failed, but your booking is CONFIRMED.
                You can pay at the hotel. Please call our helpline 932280010 for any help.
              </Text>
            </View>
          </Flex>
        </Space>
      </Card>
    </Text>
  </Space>
);

export default PaymentStatusFailure;
