import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('ConfirmationPage');
    return import('./ConfirmationPage' /* webpackChunkName: 'ConfirmationPage' */);
  },
  loading: () => null,
});
