import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import Button from 'leaf-ui/cjs/Button/web';
import Space from 'leaf-ui/cjs/Space/web';
import Text from 'leaf-ui/cjs/Text/web';
import View from 'leaf-ui/cjs/View/web';
import { Heading, Subheading } from '../../components/Typography/Typography';
import { constants } from '../../services/utils';
import Accordion from '../../components/Accordion/Accordion';
import * as contentActionCreators from '../../services/content/contentDuck';
import analyticsService from '../../services/analytics/analyticsService';
import './perfectStayPage.css';

class PerfectStayPage extends Component {
  static componentWillServerRender = ({ store: { dispatch } }) =>
    dispatch(contentActionCreators.getPerfectStayContent());

  componentDidMount() {
    const { contentActions } = this.props;
    contentActions.getPerfectStayContent();
  }

  ctaClicked = (name) => {
    analyticsService.ctaClicked(name);
  }

  render() {
    const { perfectStay } = this.props;
    return (
      <View className="psp">
        <Helmet meta={perfectStay.metas} />
        <View className="psp__header">
          <Heading className="psp__header__heading">
            {'Perfect Stay or Don’t Pay'}
          </Heading>
          <Subheading type="2" className="psp__header__subheading">
            {'100% money-back promise'}
          </Subheading>
        </View>
        <View className="psp__container">
          <View className="psp__direct-booking">
            <Text type="3" className="psp__title" size="m" weight="medium" color="greyDarker">Need help with your stay?</Text>
            <View className="psp__direct-booking__benefits">
              <img
                src="https://images.treebohotels.com/images/hotrod/contact-us.svg"
                className="old-image psp__direct-booking__icon"
                alt="Let us know"
              />
              <View>
                <Text type="3" className="psp__sub-title" color="greyDarker">Let us know</Text>
                <Text type="2" className="psp__text psp__call-us">
                  Call&nbsp;
                  <Text className="text--link">
                    <a href={`tel:${constants.customerCareNumber}`} onClick={() => this.ctaClicked('Phone')}>{`+91-${constants.customerCareNumber}`}</a>
                  </Text>
                </Text>
              </View>
            </View>
            <View className="psp__direct-booking__benefits">
              <img
                src="https://images.treebohotels.com/images/hotrod/give-us-30-mins.svg"
                className="old-image psp__direct-booking__icon"
                alt="Give us 30 minutes"
              />
              <View>
                <Text type="3" className="psp__sub-title" color="greyDarker">Give us 30 minutes
                </Text>
                <Text type="2" className="psp__text">We will resolve the issue</Text>
              </View>
            </View>
            <View className="psp__direct-booking__benefits">
              <img
                src="https://images.treebohotels.com/images/hotrod/refund.svg"
                className="old-image psp__direct-booking__icon"
                alt="And if we can’t"
              />
              <View>
                <Text type="3" className="psp__sub-title" color="greyDarker">And if we can’t</Text>
                <Text type="2" className="psp__text">100% money-back upto &#x20b9;1200</Text>
              </View>
            </View>
            <a href={`tel:${constants.customerCareNumber}`}>
              <Button
                kind="outlined"
                shape="bluntEdged"
                size="medium"
                block
                onClick={() => this.ctaClicked('Call Now')}
              >
                CALL NOW
              </Button>
            </a>
          </View>
          <View className="psp__htpw">
            <Text size="m" type="1" className="psp__htpw__title" color="greyDarker">What does this cover?</Text>
            <View className="psp__treebo-points">
              <i className="icon-comfortable-room" />
              <Space margin={[0, 0, 0, 2]}>
                <View>
                  <Text type="3" className="psp__sub-title" color="greyDarker">Comfortable Rooms</Text>
                  <Text type="2" className="psp__text">Fresh linen & clean washroom</Text>
                </View>
              </Space>
            </View>
            <View className="psp__treebo-points">
              <i className="icon-best-service" />
              <Space margin={[0, 0, 0, 2]}>
                <View>
                  <Text type="3" className="psp__sub-title" color="greyDarker">Best-in-class Service</Text>
                  <Text type="2" className="psp__text">
                  Well trained staff and 24x7 support
                  </Text>
                </View>
              </Space>
            </View>
            <View className="psp__treebo-points">
              <i className="icon-breakfast" />
              <Space margin={[0, 0, 0, 2]}>
                <View>
                  <Text type="3" className="psp__sub-title" color="greyDarker">Wholesome Breakfast</Text>
                  <Text type="2" className="psp__text">
                  A minimum of 1 main dish, tea/coffee, juice/cut fruits
                  </Text>
                </View>
              </Space>
            </View>
            <View className="psp__treebo-points">
              <i className="icon-room-amenities" />
              <Space margin={[0, 0, 0, 2]}>
                <View>
                  <Text type="3" className="psp__sub-title" color="greyDarker">Assured Room Amenities</Text>
                  <Text type="2" className="psp__text">
                  DTH or Cable TV
                  </Text>
                  <Text type="2" className="psp__text">
                  WiFi with min speed of 1 mbps (upto 500 MB) *
                  </Text>
                  <Text type="2" className="psp__text">
                  AC *
                  </Text>
                </View>
              </Space>
            </View>
            <Space margin={[2, 0, 0, 0]}>
              <Text type="2">
                * Not applicable in hill stations
              </Text>
            </Space>
          </View>
          <View className="psp__htpw">
            <Text type="2" size="m" className="psp__htpw__title" color="greyDarker">Other issues?</Text>
            <View>
              <Text className="psp__text">We are here to to resolve any issue you face while staying at a Treebo.</Text>
            </View>
            <View style={{ margin: '16px 0 0 0' }}>
              <Space margin={[0, 0, 1.5, 0]}>
                <Text type="1" className="psp__sub-title" color="greyDarker">Let us know</Text>
              </Space>
              <Space margin={[0, 0, 1.5, 0]}>
                <Text color="blue">
                  <a href={`tel:${constants.customerCareNumber}`} onClick={() => this.ctaClicked('Phone')}>{`+91-${constants.customerCareNumber}`}</a>
                </Text>
              </Space>
              <Space margin={[0, 0, 1.5, 0]}>
                <Text color="blue">
                  <a href="mailto:hello@treebohotels.com" onClick={() => this.ctaClicked('Email')}>hello@treebohotels.com</a>
                </Text>
              </Space>
            </View>
          </View>
        </View>
        <Accordion.Container className="psp__accordion">
          <Accordion.Section className="psp__accordion__section">
            <Accordion.Title className="psp__accordion__section__title">
              <Subheading type="2" className="psp__accordion__section__title__subheading">
                  Terms and Conditions
              </Subheading>
            </Accordion.Title>
            <Accordion.Content className="psp__accordion__section__content">
              <ul style={{ 'list-style': 'unset' }}>
                <Space margin={[3]} style={{ lineHeight: '21px' }}>
                  <li>
                    {'Upon receipt of a complaint from the customer against the guaranteed services as stipulated in the offer page, Treebo will pay a maximum refund amount of ₹1200/- (Rupees One Thousand and Two Hundred Only) per booking.'}
                  </li>
                </Space>
                <Space margin={[3]} style={{ lineHeight: '21px' }}>
                  <li>
                    {'Complaints shall be raised during customer’s stay at Treebo Hotels only. Any complaints raised post check-out from Treebo Hotels shall be entertained at Treebo’s discretion.'}
                  </li>
                </Space>
                <Space margin={[3]} style={{ lineHeight: '21px' }}>
                  <li>
                    {'Complaints can be raised either by calling Treebo customer care or through an email at hello@treebohotels.com.'}
                  </li>
                </Space>
                <Space margin={[3]} style={{ lineHeight: '21px' }}>
                  <li>
                    {'This offer shall not be applicable in the event Treebo rectifies any complaint received by the customer within 30 minutes from the receipt of such complaint.'}
                  </li>
                </Space>
                <Space margin={[3]} style={{ lineHeight: '21px' }}>
                  <li>
                    {'Treebo reserves the right to rescind this offer at its sole discretion.'}
                  </li>
                </Space>
                <Space margin={[3]} style={{ lineHeight: '21px' }}>
                  <li>
                    {'Any complaint raised against the hotel policies shall not be covered under this offer.'}
                  </li>
                </Space>
                <Space margin={[3]} style={{ lineHeight: '21px' }}>
                  <li>
                      Terms of Service and Privacy Policy as mentioned at&nbsp;
                    <Text color="blue" style={{ display: 'inline-block' }}>
                      <Link to="/" onClick={() => this.ctaClicked('Email')}> www.treebo.com</Link>
                    </Text> shall be applicable.
                  </li>
                </Space>
              </ul>
            </Accordion.Content>
          </Accordion.Section>
        </Accordion.Container>
        <Link to="/" onClick={() => this.ctaClicked('Book Now')}>
          <Button
            kind="filled"
            shape="bluntEdged"
            size="medium"
            block
          >
            BOOK NOW
          </Button>
        </Link>
      </View>
    );
  }
}

PerfectStayPage.propTypes = {
  perfectStay: PropTypes.object,
  contentActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  perfectStay: state.content.perfectStay,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PerfectStayPage);
