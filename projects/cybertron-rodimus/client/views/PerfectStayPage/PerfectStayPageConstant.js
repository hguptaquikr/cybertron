const PerfectStayPageConstant = {
  termAndConditions: [
    'Upon receipt of a complaint from the customer against the guaranteed services as stipulated in the offer page, Treebo will pay a maximum refund amount of ₹1200/- (Rupees One Thousand and Two Hundred Only) per booking.',
    'Complaints shall be raised during customer’s stay at Treebo Hotels only. Any complaints raised post check-out from Treebo Hotels shall be entertained at Treebo’s discretion.',
    'Complaints can be raised either by calling Treebo customer care or through an email at hello@treebohotels.com.',
    'This offer shall not be applicable in the event Treebo rectifies any complaint received by the customer within 30 minutes from the receipt of such complaint.',
    'Treebo reserves the right to rescind this offer at its sole discretion.',
    'Any complaint raised against the hotel policies shall not be covered under this offer.',
    'Terms of Service and Privacy Policy as mentioned at https://www.treebo.com/ shall be applicable.',
  ],
};

export default PerfectStayPageConstant;
