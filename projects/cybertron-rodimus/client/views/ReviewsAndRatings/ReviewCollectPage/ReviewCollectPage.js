import React, { Component } from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Text from 'leaf-ui/cjs/Text/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import Space from 'leaf-ui/cjs/Space/web';
import View from 'leaf-ui/cjs/View/web';
import PageHeader from '../../../components/PageHeader/PageHeader';
import { Heading } from '../../../components/Typography/Typography';
import ReviewSubmittedPage from '../../ReviewsAndRatings/ReviewCollectPage';
import * as contentActionCreators from '../../../services/content/contentDuck';

class ReviewCollectPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    return Promise.all(promises);
  }

  componentDidMount() {
    const {
      content: { feedback },
      contentActions,
      location,
      route,
      match,
    } = this.props;
    ReviewSubmittedPage.preload();
    const query = queryString.parse(location.search);
    contentActions.getSeoContent(route, match.params);

    const overallFeedback = {
      ...feedback,
      source: query.source,
      feedback_flow: {
        ...feedback.feedback_flow,
        value: query.flow,
      },
    };
    if (query.source && query.flow) {
      contentActions.submitFeedback(overallFeedback);
    }
  }

  render() {
    const { location, content } = this.props;
    const query = queryString.parse(location.search);

    let taWidgetUrl;
    if (query.flow === 'avg' || query.flow === 'good') {
      taWidgetUrl = decodeURIComponent(content.feedback.ta_url);
    } else if (query.flow === 'amazing') {
      taWidgetUrl = decodeURIComponent(query.taWidgetUrl);
    } else {
      const [taWidgetPathname, taWidgetQueryString] = query.taWidgetUrl.split('?');
      const taWidgetQuery = queryString.parse(taWidgetQueryString);
      taWidgetQuery.overallRating = query.overallRating || 5;
      taWidgetUrl = `${taWidgetPathname}?${queryString.stringify(taWidgetQuery)}`;
    }
    return (
      <Flex>
        <View>
          <PageHeader>
            {
            query.flow ? (
              <Space
                padding={[2, 0]}
              >
                <Flex alignItems="center">
                  <View>
                    <Text
                      size="l"
                      color="greyDarker"
                      weight="bold"
                    >
                    How about writing a review, then ?
                    </Text>
                    <Text
                      size="xs"
                      color="greyDark"
                    >
                    Please take a minute to review us on TripAdvisor.
                    </Text>
                  </View>
                </Flex>
              </Space>
            ) : (
              <Heading>Write a Review</Heading>
            )
          }
          </PageHeader>
          <iframe
            title="ta-widget"
            src={taWidgetUrl}
            width="100%"
            height="500px"
            frameBorder="0"
          />
        </View>
      </Flex>
    );
  }
}

ReviewCollectPage.propTypes = {
  route: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  content: PropTypes.object,
  contentActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ReviewCollectPage);
