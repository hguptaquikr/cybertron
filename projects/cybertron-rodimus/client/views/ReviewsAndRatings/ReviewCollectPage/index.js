import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('ReviewCollectPage');
    return import('./ReviewCollectPage' /* webpackChunkName: 'ReviewCollectPage' */);
  },
  loading: () => null,
});
