import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from '../../components/Flex/Flex';
import { Heading, Text } from '../../components/Typography/Typography';

export const OverallRatingsHeader = ({ rating, ratingImage }) => (
  <div className="rar-or__header">
    <Heading
      tag="h3"
      type="1"
      preview={!rating}
      previewStyle={{ width: '10%' }}
      className="rar-or__header__rating"
    >
      <span>{rating}</span>
      <span className="rar-or__header__rating--light">/5</span>
    </Heading>
    <img
      className="old-image"
      src={ratingImage}
      alt="Overall Rating"
    />
  </div>
);

OverallRatingsHeader.propTypes = {
  rating: PropTypes.string.isRequired,
  ratingImage: PropTypes.string.isRequired,
};

const OverallRatings = ({ overallRating }) => {
  const { subRatings } = overallRating;
  return (
    <div className="rar-or">
      <OverallRatingsHeader rating={overallRating.rating} ratingImage={overallRating.ratingImage} />
      <div className="rar-or__subratings">
        {
          subRatings.map((subRating, index) => (
            <Row key={`${subRating.level}${index + 1}`} middle className="rar-or__subrating-item">
              <Col size="4">
                <Text tag="div">{subRating.level}</Text>
              </Col>
              <Col size="4">
                <div className="rar-or__bar">
                  <div
                    className="rar-or__bar__fill"
                    style={{ width: subRating.percent }}
                  />
                </div>
              </Col>
              <Col size="4" className="text-right">
                <Text type="2" tag="div">{subRating.noOfReviews}</Text>
              </Col>
            </Row>
          ))
        }
      </div>
    </div>
  );
};

OverallRatings.propTypes = {
  overallRating: PropTypes.object.isRequired,
};

export default OverallRatings;
