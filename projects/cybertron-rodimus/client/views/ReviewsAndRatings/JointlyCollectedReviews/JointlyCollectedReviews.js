import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'react-router-dom';
import LoadMore from '../../../components/LoadMore/LoadMore';
import UserReview from '../UserReview/UserReview';
import priceService from '../../../services/price/priceService';
import reviewService from '../../../services/review/reviewService';
import * as hotelService from '../../../services/hotel/hotelService';

class JointlyCollectedReviews extends Component {
  makeHotelDetailsLocation = (hotel) => {
    const { price, location } = this.props;
    const hotelPrices = priceService.getPricesForRoomTypeIds(hotel.roomTypeIds, price);
    const cheapestRoomType = priceService.sortPrice(hotelPrices)[0];
    const cheapestRatePlan = priceService.sortRatePlan(cheapestRoomType.ratePlans)[0] || {};
    return hotelService
      .makeHotelDetailsLocation(hotel, cheapestRoomType, location, cheapestRatePlan);
  }

  render() {
    const { className, previewNumber, reviews, hotels } = this.props;
    const reviewsToDisplay = reviewService.getJCR(reviews);
    return (
      <LoadMore
        stepper={{ initial: previewNumber, step: reviewsToDisplay.length - previewNumber }}
        loadMoreText="Show more reviews"
        loadLessText="Show less reviews"
        actionClassName="jcr__toggle-reviews"
        containerClassName={cx(className, 'jcr')}
      >
        {
          reviewsToDisplay
            .map((review, index) => hotels && hotels[review.hotel.id] && review.hotel ? (
              <Link
                to={this.makeHotelDetailsLocation(hotels[review.hotel.id])}
                key={`${index + 1}`}
              >
                <UserReview review={review} />
              </Link>
            ) : <UserReview review={review} key={`${index + 1}`} />)
        }
      </LoadMore>
    );
  }
}

JointlyCollectedReviews.propTypes = {
  previewNumber: PropTypes.number,
  reviews: PropTypes.array.isRequired,
  className: PropTypes.string,
  location: PropTypes.object,
  hotels: PropTypes.object,
  price: PropTypes.object,
};

JointlyCollectedReviews.defaultProps = {
  previewNumber: 2,
};

export default JointlyCollectedReviews;
