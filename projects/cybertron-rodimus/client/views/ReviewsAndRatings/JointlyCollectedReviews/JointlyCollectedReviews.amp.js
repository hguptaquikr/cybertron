import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MoreOrLess from 'leaf-ui/cjs/MoreOrLess/web';
import UserReview from '../UserReview/UserReview.amp';
import config from '../../../../config';
import priceService from '../../../services/price/priceService';
import * as hotelService from '../../../services/hotel/hotelService';

class JointlyCollectedReviews extends Component {
  makeHotelDetailsLocation = (hotel) => {
    const { price, location } = this.props;
    const hotelPrices = priceService.getPricesForRoomTypeIds(hotel.roomTypeIds, price);
    const cheapestRoomType = priceService.sortPrice(hotelPrices)[0];
    const cheapestRatePlan = priceService.sortRatePlan(cheapestRoomType.ratePlans)[0] || {};
    const hotelDetailsLocation = hotelService
      .makeHotelDetailsLocation(hotel, cheapestRoomType, location, cheapestRatePlan);
    return `${config.hostUrl}${hotelDetailsLocation.pathname}`;
  }

  render() {
    const { reviews, hotels } = this.props;
    return (
      <MoreOrLess
        initialHeight="150px"
        labelForMore="Show More Reviews"
        labelForLess="Show Less Reviews"
      >
        {
          reviews
            .map((review, index) => hotels && hotels[review.hotel.id] && review.hotel ? (
              <a
                id={`t-reviewHDLink-${hotels[review.hotel.id].id}`}
                className="link"
                href={this.makeHotelDetailsLocation(hotels[review.hotel.id])}
                key={`${index + 1}`}
              >
                <UserReview review={review} />
              </a>
            ) : <UserReview review={review} key={`${index + 1}`} />)
        }
      </MoreOrLess>
    );
  }
}

JointlyCollectedReviews.propTypes = {
  reviews: PropTypes.array.isRequired,
  location: PropTypes.object,
  hotels: PropTypes.object,
  price: PropTypes.object,
};

export default JointlyCollectedReviews;
