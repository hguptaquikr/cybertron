import React from 'react';
import PropTypes from 'prop-types';
import Text from 'leaf-ui/cjs/Text/amp';
import Flex from 'leaf-ui/cjs/Flex/amp';
import Space from 'leaf-ui/cjs/Space/amp';
import Image from 'leaf-ui/cjs/Image/amp';
import View from 'leaf-ui/cjs/View/amp';

const UserReview = ({ review }) => (
  <Space margin={[2, 0, 0, 0]}>
    <View>
      {
        review.hotel ? (
          <Text
            size="m"
            weight="medium"
            color="blue"
            component="p"
          >
            {review.hotel.name}
          </Text>
        ) : null
      }
      <Space margin={[1, 0, 0, 0]}>
        <View>
          <Text
            size="s"
            weight="medium"
            color="greyDark"
            component="p"
          >
            {review.title}
          </Text>
        </View>
      </Space>
      <Space margin={[0, 0, 1, 0]}>
        <Flex flexDirection="row">
          <View>
            <Space padding={[0, 1, 0, 0]}>
              <View>
                <Image
                  src={review.ratingImage}
                  alt="rating"
                  width="60"
                  height="24"
                />
              </View>
            </Space>
            <Text
              size="s"
              weight="normal"
              color="greyDark"
              component="p"
            >
              {review.user.name}, {review.publishDate}
            </Text>
          </View>
        </Flex>
      </Space>
      <Text
        size="s"
        weight="normal"
        color="greyDark"
        component="p"
      >
        {review.description}
      </Text>
    </View>
  </Space>
);

UserReview.propTypes = {
  review: PropTypes.object.isRequired,
};

export default UserReview;
