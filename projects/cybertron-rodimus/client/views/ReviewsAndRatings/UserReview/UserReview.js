import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Subheading, Text } from '../../../components/Typography/Typography';
import './UserReview.css';

const UserReview = ({ className, review }) => (
  <div className={cx(className, 'ur')} key={review.title} id="t-ur">
    {
      review.hotel ? (
        <Subheading className="ur__hotel-name" id="t-ur-hotel-name">{review.hotel.name}</Subheading>
      ) : null
    }
    <Subheading tag="p" className="ur__title" id="t-ur-title">{review.title}</Subheading>
    <div className="ur__image-container">
      <img className="old-image ur__image" src={review.ratingImage} alt="rating" />
      <Text type="2" tag="span">{review.user.name}, {review.publishDate}</Text>
    </div>
    <Text type="2" className="ur__description">{review.description}</Text>

  </div>
);

UserReview.propTypes = {
  className: PropTypes.string,
  review: PropTypes.object.isRequired,
};

export default UserReview;
