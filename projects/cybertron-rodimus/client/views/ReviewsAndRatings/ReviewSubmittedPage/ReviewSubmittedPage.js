import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import queryString from 'query-string';
import { Heading, Text } from '../../../components/Typography/Typography';
import Button from '../../../components/Button/Button';
import * as reviewActionCreators from '../../../services/review/reviewDuck';
import * as contentActionCreators from '../../../services/content/contentDuck';
import './reviewSubmittedPage.css';

class ReviewSubmittedPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    return Promise.all(promises);
  }

  componentDidMount() {
    const {
      route,
      match,
      reviewActions,
      contentActions,
    } = this.props;
    contentActions.getSeoContent(route, match.params);
    if (window.parent !== window) {
      const { bookingId } = queryString.parse(window.parent.location.search);
      if (bookingId) {
        reviewActions.reviewSubmitted(bookingId);
      }
      const { feedbackId } = queryString.parse(window.parent.location.search);
      if (feedbackId) {
        const {
          content: { feedback },
        } = this.props;
        const completeFeedback = {
          ...feedback,
          state: 'COMPLETED',
        };
        contentActions.submitFeedback(completeFeedback);
      }
    }
  }

  render() {
    return (
      <div className="rsp text-center">
        <img
          src="http://images.treebohotels.com/images/thankyou-2x.png?w=230&h=125"
          alt="review submission"
          className="old-image rsp__image"
        />
        <Heading className="rsp__heading">
          Thanks for your review
        </Heading>
        <Text type="2" className="rsp__text text--secondary">
          Thank you for sharing your experience! It was our pleasure hosting you and
            we look forward to the same opportunity again.
        </Text>
        <Link to="/" className="rsp__action">
          <Button
            block
            large
            modifier="secondary"
          >
            SEARCH HOTELS
          </Button>
        </Link>
      </div>
    );
  }
}

ReviewSubmittedPage.propTypes = {
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  reviewActions: PropTypes.arrayOf().isRequired,
  content: PropTypes.object,
  contentActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  reviewActions: bindActionCreators(reviewActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ReviewSubmittedPage);
