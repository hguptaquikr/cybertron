import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('ReviewSubmittedPage');
    return import('./ReviewSubmittedPage' /* webpackChunkName: 'ReviewSubmittedPage' */);
  },
  loading: () => null,
});
