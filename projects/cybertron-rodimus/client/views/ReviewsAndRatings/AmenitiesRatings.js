import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from '../../components/Flex/Flex';
import { Text } from '../../components/Typography/Typography';

const AmenitiesRatings = ({ amenitiesRatings }) => (
  <div className="rar-ar">
    {
      amenitiesRatings.map((amenityRating, index) => (
        <Row key={`${amenityRating.name}${index + 1}`} between middle className="rar-ar__rating" >
          <Col size="5">
            <Text tag="div">{amenityRating.name}</Text>
          </Col>
          <Col size="5" className="rar-ar__rating-image">
            <img className="old-image" src={amenityRating.image} alt={amenityRating.rating} />
          </Col>
        </Row>
      ))
    }
  </div>
);

AmenitiesRatings.propTypes = {
  amenitiesRatings: PropTypes.array.isRequired,
};

export default AmenitiesRatings;
