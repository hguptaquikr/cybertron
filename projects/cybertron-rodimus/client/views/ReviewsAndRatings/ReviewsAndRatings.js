import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import qs from 'query-string';
import * as reviewActionCreators from '../../services/review/reviewDuck';
import { Heading, Text } from '../../components/Typography/Typography';
import Modal from '../../components/Modal/Modal';
import Button from '../../components/Button/Button';
import Ripple from '../../components/Ripple/Ripple';
import OverallRatings, { OverallRatingsHeader } from './OverallRatings';
import AmenitiesRatings from './AmenitiesRatings';
import JointlyCollectedReviews from './JointlyCollectedReviews/JointlyCollectedReviews';
import UserReview from './UserReview/UserReview';
import androidService from '../../services/android/androidService';
import ReviewCollectPage from '../ReviewsAndRatings/ReviewCollectPage';
import './reviewsAndRatings.css';

class ReviewsAndRatings extends Component {
  static componentWillServerRender = ({ store: { dispatch, getState }, match }) => {
    const promise = [];
    const reviews = getState().review.results[match.params.hotelId];
    if (isEmpty(reviews)) {
      promise.push(dispatch(reviewActionCreators.getHotelReviews(match.params.hotelId)));
    }
    return Promise.all(promise);
  }

  state = {
    showAllReviews: false,
  }

  componentDidMount() {
    const { reviewActions, match } = this.props;
    reviewActions.getHotelReviews(match.params.hotelId)
      .then(() => reviewActions.getNonJCR(match.params.hotelId));
  }

  toggleReviewsPageVisibility = (visibility) => () =>
    this.setState({
      showAllReviews: visibility,
    });

  render() {
    const { showAllReviews } = this.state;
    const {
      reviews,
      reviews: {
        overallRating,
        userReviews,
        amenitiesRatings,
        writeReviewsUrl,
      },
      match,
    } = this.props;

    return reviews.isTaEnabled && reviews.overallRating.rating ? (
      <div className="rar">
        <Heading type="3" className="rar__header__title">TripAdvisor Reviews</Heading>
        <div>
          <OverallRatingsHeader
            rating={overallRating.rating}
            ratingImage={overallRating.ratingImage}
          />
        </div>
        {
          userReviews[0] ? (
            <UserReview className="rar__user-review" review={userReviews[0]} />
          ) : null
        }
        <Modal
          closeIcon="back"
          isOpen={showAllReviews}
          label="TripAdvisor Reviews"
          header={<Heading>TripAdvisor Reviews</Heading>}
          onClose={this.toggleReviewsPageVisibility(false)}
        >
          <div className="rar__container">
            <div className="rar__section">
              <Heading
                type="3"
                tag="h3"
                className="rar__section__heading"
              >
                Overall Ratings
              </Heading>
              <OverallRatings overallRating={overallRating} />
            </div>
            <div className="rar__section">
              <Heading
                type="3"
                tag="h3"
                className="rar__section__heading"
              >
                Amenities Ratings
              </Heading>
              <AmenitiesRatings amenitiesRatings={amenitiesRatings} />
            </div>
            <div className="rar__section">
              <Heading
                type="3"
                tag="h3"
                className="rar__section__heading"
              >
                Top User Reviews
              </Heading>
              <Text>
                In partnership with
                <img
                  className="old-image rar__ta-logo"
                  src="https://images.treebohotels.com/images/ta_logo.svg"
                  alt="TripAdvisor"
                />
              </Text>
              <JointlyCollectedReviews reviews={userReviews} previewNumber={5} />
            </div>
            {
              !androidService.isAndroid ? (
                <Ripple eagerLoad={ReviewCollectPage.preload}>
                  <Link
                    className="floaty"
                    to={{
                      pathname: `/tripadvisor/reviews-${match.params.hotelId}/`,
                      search: qs.stringify({ taWidgetUrl: writeReviewsUrl }),
                    }}
                  >
                    <Button block large flat>Write a Review</Button>
                  </Link>
                </Ripple>
              ) : null
            }
          </div>
        </Modal>
        <Text
          className="rar__header text-left angle-link"
          tag="div"
          onClick={this.toggleReviewsPageVisibility(true)}
        >
          View all reviews
        </Text>
      </div>
    ) : null;
  }
}

ReviewsAndRatings.propTypes = {
  reviews: PropTypes.object.isRequired,
  reviewActions: PropTypes.object.isRequired,
  match: PropTypes.object,
};

const mapStateToProps = (state, { match }) => ({
  reviews: state.review.results[match.params.hotelId] || state.review.results[0],
});

const mapDispatchToProps = (dispatch) => ({
  reviewActions: bindActionCreators(reviewActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(ReviewsAndRatings);
