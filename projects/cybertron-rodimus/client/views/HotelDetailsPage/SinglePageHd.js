import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import qs from 'query-string';
import moment from 'moment';
import Space from 'leaf-ui/cjs/Space/web';
import Price from 'leaf-ui/cjs/Price/web';
import Card from 'leaf-ui/cjs/Card/web';
import View from 'leaf-ui/cjs/View/web';
import Position from 'leaf-ui/cjs/Position/web';
import Text from 'leaf-ui/cjs/Text/web';
import Button from 'leaf-ui/cjs/Button/web';
import Size from 'leaf-ui/cjs/Size/web';
import Image from 'leaf-ui/cjs/Image/web';
import Gallery from 'leaf-ui/cjs/Gallery/web';
import HotelInfo from './SinglePageHd/HotelInfo';
import SearchInfo from './SinglePageHd/SearchInfo';
import PriceInfo from './SinglePageHd/Price/PriceInfo';
import RoomType from './SinglePageHd/RoomType/RoomType';
import CouponInfo from './SinglePageHd/CouponInfo';
import TravellerInfo from './SinglePageHd/TravellerInfo';
import SoldOutInfo from './SinglePageHd/SoldOutInfo';
import { Animation } from './SinglePageHd/Animation';
import { GalleryComponent } from './SinglePageHd/GalleryComponent';
import NeedToKnow from '../../components/NeedToKnow/NeedToKnow';
import PerfectStayBanner from '../../components/PerfectStayBanner/PerfectStayBanner';
import PsychologicalTriggers from '../../components/PsychologicalTriggers/PsychologicalTriggers';
import Modal from '../../components/Modal/Modal';
import SearchHeader from '../../components/SearchHeader/SearchHeader';
import searchService from '../../services/search/searchService';
import * as checkoutActionCreators from '../../services/checkout/checkoutDuck';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as hotelActionCreators from '../../services/hotel/hotelDuck';
import * as priceActionCreators from '../../services/price/priceDuck';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as toastActionCreators from '../../services/toast/toastDuck';
import * as uiActionCreators from '../../services/ui/uiDuck';
import * as reviewActionCreators from '../../services/review/reviewDuck';
import * as couponActionCreators from '../../services/coupon/couponDuck';
import * as loaderActionCreators from '../../services/loader/loaderDuck';
import * as walletActionCreators from '../../services/wallet/walletDuck';
import * as hotelService from '../../services/hotel/hotelService';
import * as abService from '../../services/ab/abService';
import * as routeService from '../../services/route/routeService';
import analyticsService from '../../services/analytics/analyticsService';
import checkoutService from '../../services/checkout/checkoutService';
import priceService from '../../services/price/priceService';
import { omitKeys, makeRoomTypeId, browserDimensions, constants } from '../../services/utils';
import OtherResults from './OtherResults/OtherResults';
import MoreInfo from './MoreInfo/MoreInfo';
import SearchWidgetAsync from '../SearchWidget/SearchWidgetAsync';
import CheckoutPage from '../CheckoutPage';

class SinglePageHd extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match, req, history }) => {
    const decodedHotelId = hotelService.decodeHotelId(match.params.hotelId);
    if (decodedHotelId) {
      return history.replace({
        pathname: history.location.pathname.replace(match.params.hotelId, decodedHotelId),
        search: history.location.search,
      });
    }
    const promises = [];
    dispatch(searchActionCreators.setSearchState({ ...req.query, ...match.params }));
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(hotelActionCreators.getHotelDetails(match.params.hotelId)));
    return Promise.all(promises);
  }

  state = {
    isGalleryOpen: false,
    animateFrom: 0,
    animateTo: 0,
    isRoomPricesError: false,
    travelllerInfo: false,
    isDatePickerOpen: false,
    isRoomConfigOpen: false,
    shouldWindowResize: false,
  }

  componentDidMount() {
    const {
      match,
      location,
      searchActions,
      contentActions,
      reviewActions,
      route,
    } = this.props;

    const query = qs.parse(location.search);
    searchActions.setSearchState({ ...query, ...match.params });
    contentActions.getSeoContent(route, match.params);
    this.fetchHotelDetails(match.params.hotelId);
    this.fetchHotelPrices(match.params.hotelId);
    this.getCheckoutDetails(true, query);
    reviewActions.getHotelReviews(match.params.hotelId);
    SearchWidgetAsync.preload();
    CheckoutPage.preload();
    document.getElementById('singlePageHd').addEventListener('scroll', () => {
      this.handleBodyScroll();
    }, { passive: true });
    window.onresize = this.handleWindowResize;
  }

  componentWillReceiveProps(nextProps) {
    const { props } = this;
    const { searchActions } = this.props;
    const query = qs.parse(props.location.search);
    const nextQuery = qs.parse(nextProps.location.search);
    const dateRange = routeService.makeDateRangeQuery(props.search.datePicker.range);
    const nextDateRange = routeService.makeDateRangeQuery(nextProps.search.datePicker.range);

    if (
      props.match.params.hotelId !== nextProps.match.params.hotelId
    ) {
      searchActions.setSearchState({ ...nextQuery, ...nextProps.match.params });
      nextProps.contentActions.getSeoContent(nextProps.route, nextProps.match.params);
      this.fetchHotelDetails(nextProps.match.params.hotelId);
      this.fetchHotelPrices(nextProps.match.params.hotelId);
    } else if (
      nextDateRange.checkin
        && nextDateRange.checkout
        && (dateRange.checkin !== nextDateRange.checkin
          || dateRange.checkout !== nextDateRange.checkout
        )
    ) {
      nextProps.contentActions.getSeoContent(nextProps.route, nextProps.match.params);
      this.onChangeSearchConfig();
    } else if (
      (!query.rateplan && query.rateplan !== nextQuery.rateplan)
      || (!query.roomtype && query.roomtype !== nextQuery.roomtype)
    ) {
      searchActions.setSearchState({ ...nextQuery, ...nextProps.match.params });
      nextProps.contentActions.getSeoContent(nextProps.route, nextProps.match.params);
      this.getCheckoutDetails(true, nextQuery);
    }
  }

  onChangeSearchConfig = () => {
    const {
      match,
      location,
      searchActions,
    } = this.props;

    const query = qs.parse(location.search);

    this.fetchHotelPrices(match.params.hotelId);
    searchActions.checkHotelAvailability()
      .then((res) => {
        if (res.payload.available) {
          this.getCheckoutDetails(false, query);
        }
      });
  }

  getCheckoutDetails = async (useBookingId, query) => {
    const {
      checkoutActions,
      couponActions,
    } = this.props;

    if (query.rateplan && query.roomtype) {
      const bookingId = useBookingId ? query.bid : null;
      const bookingDetails = await checkoutActions.getCheckoutDetails(bookingId);
      const { bid } = bookingDetails.payload.booking;
      await couponActions.getCoupons(bid);
      this.addBidToUrl(bid);
    }
  }

  getRoomPrices = (hotelId) => {
    const {
      contentActions,
      priceActions,
      searchActions,
    } = this.props;
    if (searchActions.isSearchWidgetValid()) {
      priceActions.getRoomPrices(hotelId)
        .then((res) => {
          this.updateRoomQuery();
          if (res.error) {
            this.setState({ isRoomPricesError: true });
            return false;
          }
          this.setState({ isRoomPricesError: false });
          return contentActions.getPsychologicalTriggers([hotelId]);
        })
        .then(() => {
          this.trackPsychologicalTriggerEvent(hotelId);
          this.captureAnalytics();
        });
    }
  }

  getAvailableRoomTypeIds = (roomTypeIds, prices) =>
    roomTypeIds.filter((roomTypeId) => (prices[roomTypeId] && prices[roomTypeId].available));

  getSelectedRoomTypeId = (prices, chepeastRoomTypeId) => {
    const { location, match: { params } } = this.props;
    const query = qs.parse(location.search);

    return query.roomtype
      && prices[makeRoomTypeId(params.hotelId, query.roomtype)]
      && prices[makeRoomTypeId(params.hotelId, query.roomtype)].available
      ? makeRoomTypeId(params.hotelId, query.roomtype) : chepeastRoomTypeId;
  }

  getSelectedRatePlan = (selectedPrice) => {
    const { location } = this.props;
    const query = qs.parse(location.search);

    return query.rateplan && selectedPrice.ratePlans[query.rateplan]
      ? selectedPrice.ratePlans[query.rateplan]
      : priceService.sortRatePlan(selectedPrice.ratePlans)[0];
  }

  updateRoomQuery = () => {
    const {
      price,
      location,
      history,
      $roomType,
      hotel,
      search,
    } = this.props;

    const query = qs.parse(location.search);
    const availableRoomTypeIds =
    this.getAvailableRoomTypeIds(hotel.roomTypeIds, price.byRoomTypeId);
    const hotelPrices = priceService.getPricesForRoomTypeIds(availableRoomTypeIds, price);
    const sortedAvailablePrices = priceService.sortPrice(hotelPrices);
    const selectedRoomTypeId = this.getSelectedRoomTypeId(price.byRoomTypeId, sortedAvailablePrices[0].id) || '0|oak';
    const selectedPrice = price.byRoomTypeId[selectedRoomTypeId];
    const selectedRatePlan = this.getSelectedRatePlan(selectedPrice) || {};
    const selectedRoomType = $roomType.byId[selectedRoomTypeId] || $roomType.byId['0|oak'];
    history.replace({
      pathname: location.pathname,
      search: qs.stringify({
        ...query,
        ...routeService.makeDateRangeQuery(search.datePicker.range),
        ...routeService.makeRoomConfigQuery(search.roomConfig.rooms),
        ...routeService.makeRoomTypeQuery(selectedRoomType.type),
        ...routeService.makeRatePlanQuery(selectedRatePlan.code),
      }),
    });
  };

  scrollToSection = (id) => {
    if (document.getElementById('singlePageHd') && document.getElementById(id)) {
      document.getElementById('singlePageHd').scrollTo({
        top: document.getElementById(id).parentElement.offsetTop - 56,
        behavior: 'smooth',
      });
    }
  }

  handleBodyScroll = () => {
    const singlePageHdBody = document.querySelector('#singlePageHdBody');
    const shouldHeaderAnimate = singlePageHdBody
      && singlePageHdBody.getBoundingClientRect().top < 0;
    if (this.state.shouldHeaderAnimate !== shouldHeaderAnimate) {
      this.setState({
        shouldHeaderAnimate,
      });
    }
  }

  travellerInfoSubmit = ({ details, isGSTFieldsVisible, bypassPaymentStep, checkoutUrl }) => {
    const { checkoutActions, history, hotel } = this.props;
    checkoutActions.addTravellerInfo({
      details,
      hotelId: hotel.id,
      hotelName: hotel.name,
      isGSTFieldsVisible,
      bypassPaymentStep,
    });
    if (bypassPaymentStep) this.bypassPaymentStep();
    else history.push(checkoutUrl);
  };

  bypassPaymentStep = () => {
    const { loaderActions } = this.props;
    loaderActions.showLoader();
    this.initiateBooking()
      .then((bookingDetails) => {
        if (bookingDetails.error) this.showPaymentError(bookingDetails.error.description);
        else this.confirmBooking();
      })
      .catch(loaderActions.hideLoader);
  }

  addBidToUrl = (bid) => {
    const { history, location } = this.props;
    const route = analyticsService.getCurrentRoute();
    if (route.path === '/hotels-in-:city/:hotelSlug-:hotelId(\\w+)/') {
      history.replace({
        pathname: location.pathname,
        search: qs.stringify({
          ...qs.parse(location.search),
          bid,
        }),
      });
    }
  };

  showPaymentError = (errorMessage) => {
    this.showPayError(errorMessage, 'error', {});
  };

  trackPsychologicalTriggerEvent = (hotelId) => {
    const {
      price,
    } = this.props;
    const selectedRoomTypeId = this.getSelectedRoomTypeId(price.byRoomTypeId);
    analyticsService.psychologicalTriggersViewed(
      'Hotel Detail',
      hotelId,
      selectedRoomTypeId,
    );
  }

  fetchHotelDetails = (hotelId) => {
    const {
      isHotelDetailsLoading,
      hotelActions,
    } = this.props;

    if (!isHotelDetailsLoading) {
      hotelActions.getHotelDetails(hotelId);
    }
  }

  fetchHotelPrices = (hotelId) => {
    const {
      isPricesLoading,
    } = this.props;

    if (!isPricesLoading) {
      this.getRoomPrices(hotelId);
    }
  }

  changeRatePlan = (oldPrice, oldRatePlan) => (selectedRoomTypeId, newRatePlanCode) => {
    const {
      search,
      searchActions,
      history,
      location,
      match,
      $roomType,
    } = this.props;
    const newRoomType = $roomType.byId[selectedRoomTypeId];
    const oldRoomType = $roomType.byId[oldPrice.id];
    const query = qs.parse(location.search);
    const abwDuration = typeof search.datePicker.range.start === 'object'
      && search.datePicker.range.start.startOf('day').diff(moment().startOf('day'), 'days');

    if (oldRoomType.type !== newRoomType.type || oldRatePlan.code !== newRatePlanCode) {
      history.replace({
        pathname: location.pathname,
        search: qs.stringify({
          ...query,
          roomtype: $roomType.byId && newRoomType.type,
          rateplan: newRatePlanCode,
        }),
      });
      searchActions.setSearchState({
        ...query,
        rateplan: newRatePlanCode,
        roomtype: $roomType.byId && newRoomType.type,
        ...match.params,
      });
      analyticsService.ratePlanChanged({
        hotelId: match.params.hotelId,
        oldRatePlan,
        newRatePlan: newRatePlanCode,
        oldRoomType,
        newRoomType,
        abwDuration,
        page: 'Hotel Details Page',
      });
    } else {
      history.goBack();
    }
  }

  captureAnalytics() {
    const {
      hotel,
      search,
      match,
      location,
      price,
      content,
    } = this.props;

    try {
      const { checkIn, checkOut, numberOfNights, abwDuration } =
        searchService.formattedDatePicker(search.datePicker.range, constants.dateFormat.query);
      const availableRoomTypeIds =
        this.getAvailableRoomTypeIds(hotel.roomTypeIds, price.byRoomTypeId);
      const hotelPrices = priceService.getPricesForRoomTypeIds(availableRoomTypeIds, price);
      const sortedAvailablePrices = priceService.sortPrice(hotelPrices);
      const selectedRoomTypeId = this.getSelectedRoomTypeId(price.byRoomTypeId, sortedAvailablePrices[0].id) || '0|oak';
      const selectedPrice = price.byRoomTypeId[selectedRoomTypeId];
      const psychologicalTrigger = content.psychologicalTriggers.byRoomTypeId[selectedRoomTypeId];
      const analyticsProperties = {
        hotelId: match.params.hotelId,
        hotelName: hotel.name,
        pageUrl: location.pathname,
        checkin: checkIn,
        checkout: checkOut,
        lengthOfStay: numberOfNights.count,
        abwDays: abwDuration,
        roomConfig: search.roomConfig.rooms.map((room) => `${room.adults}-${room.kids || 0}`).join(','),
        price: Math.round(Object.values(selectedPrice.ratePlans)[0].sellingPrice),
        triggerDisplayed: !isEmpty(psychologicalTrigger),
        triggerType: !isEmpty(psychologicalTrigger) ? psychologicalTrigger.type : '',
        ...abService.impressionProperties(['hdCtaMobile', 'pwaSinglePageHd']),
      };
      analyticsService.hotelContentViewed(analyticsProperties);
    } catch (err) {
      // console.log(err);
    }
  }

  refreshBookingId = async () => {
    const { booking } = this.props;
    const isOrderIdFetched = booking.payment.orderId;
    if (isOrderIdFetched) await this.getCheckoutDetails();
  }

  galleryOpen = () => {
    const animateFrom = 0;
    const animateTo = `${browserDimensions().height}px`;

    this.setState({
      isGalleryOpen: true,
      animateFrom,
      animateTo,
    });
  }

  galleryClose = () => {
    const animateFrom = 0;
    const animateTo = 0;
    this.setState({
      isGalleryOpen: false,
      animateFrom,
      animateTo,
    });
  }

  showTravellerInfo = (travelllerInfo) => () => {
    this.setState({
      travelllerInfo,
    });
  }

  gallerySection = (
    checkoutUrl,
    selectedRatePlan,
    prefillDetails,
    isAvailable,
    selectedPrice,
    selectedRoomType,
    coupon,
  ) => {
    const { travelllerInfo, isGalleryOpen, shouldHeaderAnimate } = this.state;
    const { width: screenWidth, height: screenHeight } = browserDimensions();
    const {
      hotel,
      checkoutActions,
    } = this.props;
    return (
      <GalleryComponent opacity={shouldHeaderAnimate ? 0 : 1}>
        <View>
          <Modal
            closeIcon="close"
            isOpen={travelllerInfo}
            onClose={this.showTravellerInfo(false)}
          >
            <React.Fragment>
              <Space padding={[2.5, 2, 8, 2]}>
                <Card backgroundColor="white" borderStyle="hidden">
                  <TravellerInfo
                    checkoutUrl={checkoutUrl}
                    travellerInfoSubmit={this.travellerInfoSubmit}
                    selectedRatePlan={selectedRatePlan}
                    id="travellerInfoSinglePageHdModal"
                    userDetail={prefillDetails}
                    checkoutActions={checkoutActions}
                    hotel={hotel}
                    selectedPrice={selectedPrice}
                    selectedRoomType={selectedRoomType}
                    coupon={coupon}
                  />
                </Card>
              </Space>
              <Position position="fixed" bottom="0">
                <Size width="100%">
                  <Space padding={[0, 1]}>
                    <View>
                      <Space padding={[0, 0, 1, 0]}>
                        <Card backgroundColor="white" borderStyle="hidden">
                          <Button
                            block
                            name="GSTINBOOK"
                            form="travellerInfoSinglePageHdModal"
                          >
                            BOOK NOW FOR <Price>{selectedRatePlan.sellingPrice}</Price>
                          </Button>
                        </Card>
                      </Space>
                    </View>
                  </Space>
                </Size>
              </Position>
            </React.Fragment>
          </Modal>
          <Position>
            <Size height={`${screenHeight}px`} width="100%">
              <Card borderStyle="hidden" backgroundColor="black">
                {
                  isGalleryOpen ? (
                    <Space padding={[2, 2]}>
                      <Position position="fixed" zIndex="1">
                        <Card backgroundColor="transparent">
                          <Text color="white">
                            <i className="icon-close" onClick={this.galleryClose} id="t-sh-close" />
                          </Text>
                        </Card>
                      </Position>
                    </Space>
                  ) : null
                }
                <Space padding={[0, 0, 7, 0]}>
                  <Gallery scrollDirection="vertical" height={`${screenHeight}px`}>
                    {
                      hotel.images.map((image, index) => (
                        <Image
                          key={`${image}${index + 1}`}
                          src={image.url}
                          alt={image.tagline}
                          width={`${screenWidth}px`}
                          height="200px"
                          shape="sharpEdged"
                          grayscale={!isAvailable}
                        />
                      ))
                    }
                  </Gallery>
                </Space>
                {
                  isGalleryOpen ? (
                    <Space padding={[1]}>
                      <Size width="100%">
                        <Position position="fixed" bottom="0">
                          <Card backgroundColor="white">
                            {
                              isAvailable ? (
                                <Button block onClick={this.showTravellerInfo(true)}>
                                  BOOK NOW FOR <Price>{selectedRatePlan.sellingPrice}</Price>
                                </Button>
                              ) : (
                                <Button
                                  block
                                  name="GSTIN"
                                  onClick={this.toggleDatePickerModal}
                                >
                                  CHECK DIFFERENT DATES
                                </Button>
                              )
                            }
                          </Card>
                        </Position>
                      </Size>
                    </Space>
                  ) : null
                }
              </Card>
            </Size>
          </Position>
        </View>
      </GalleryComponent>
    );
  }

  headerSection = () => {
    const { isGalleryOpen, shouldHeaderAnimate } = this.state;

    return (
      <Animation animateFrom={shouldHeaderAnimate ? 0 : 0} animateTo={shouldHeaderAnimate ? 0 : '-150px'}>
        <Position position={isGalleryOpen ? 'fixed' : 'absolute'} top="0" zIndex="1">
          <Size width="100%">
            <Card backgroundColor="white" borderStyle="hidden">
              <SearchHeader />
            </Card>
          </Size>
        </Position>
      </Animation>
    );
  }

  goBackSection = () => {
    const { isGalleryOpen, shouldHeaderAnimate } = this.state;
    const { history } = this.props;

    return !isGalleryOpen ? (
      <Space padding={[2, 2]}>
        <Position position="fixed" top="0" zIndex={shouldHeaderAnimate ? 0 : 1}>
          <Card backgroundColor="transparent">
            <Text color="white">
              <i onClick={history.goBack} className="icon-arrow-left" />
            </Text>
          </Card>
        </Position>
      </Space>
    ) : null;
  }

  isRoomTypePriceAvailable = (roomTypeIds, prices) =>
    roomTypeIds && roomTypeIds.some((roomTypeId) => (prices[roomTypeId]))

  handleWindowResize = () => {
    this.setState((prev) => ({ shouldWindowResize: !prev.shouldWindowResize }));
  }

  toggleDatePickerModal = () => {
    this.setState((prev) => ({ isDatePickerOpen: !prev.isDatePickerOpen }));
  }

  toggleRoomConfigModal = () => {
    this.setState((prev) => ({ isRoomConfigOpen: !prev.isRoomConfigOpen }));
  }

  render() {
    const {
      animateFrom,
      animateTo,
      isGalleryOpen,
      isRoomPricesError,
      isDatePickerOpen,
      isRoomConfigOpen,
      shouldWindowResize,
    } = this.state;
    const {
      hotel,
      price,
      search,
      booking,
      auth,
      content: { seo, psychologicalTriggers },
      coupon,
      couponActions,
      $roomType,
      reviews,
      location,
      searchActions,
      treeboPoints,
      walletActions,
      checkoutActions,
      isPricesLoading,
      isHotelDetailsLoading,
    } = this.props;

    const query = qs.parse(location.search);
    const availableRoomTypeIds =
      this.getAvailableRoomTypeIds(hotel.roomTypeIds, price.byRoomTypeId);
    const hotelPrices = priceService.getPricesForRoomTypeIds(availableRoomTypeIds, price);
    const sortedAvailablePrices = priceService.sortPrice(hotelPrices);
    const selectedRoomTypeId = this.getSelectedRoomTypeId(price.byRoomTypeId, sortedAvailablePrices[0].id) || '0|oak';
    const selectedPrice = price.byRoomTypeId[selectedRoomTypeId];
    const selectedRatePlan = this.getSelectedRatePlan(selectedPrice) || {};
    const selectedRoomType = $roomType.byId[selectedRoomTypeId] || $roomType.byId['0|oak'];

    const isAvailable = !__BROWSER__ || (
      !isRoomPricesError
        && (
          isPricesLoading
            || isHotelDetailsLoading
            || !isEmpty(availableRoomTypeIds)
        )
    );

    const checkoutUrl = isAvailable ? (
      checkoutService.makeCheckoutLocation(
        search,
        selectedRoomType,
        hotel,
        selectedRatePlan,
        1,
        query.bid,
      )
    ) : {};
    const needToKnowPolicies = hotel.policies && hotelService.getNeedToKnowPolicies(hotel.policies);
    const prefillDetails = booking.guest.name ? booking.guest : auth.profile;
    const isCoupleFriendly = !isEmpty(hotel.policies)
      && hotelService.isCoupleFriendly(hotel.policies);
    const trigger = !isEmpty(availableRoomTypeIds) ?
      psychologicalTriggers.byRoomTypeId[selectedPrice.id] : {};
    const { numberOfNights } = searchService.formattedDatePicker(search.datePicker.range, 'D MMM');

    return (
      <React.Fragment>
        {
          !hotel.isActive ? (
            <Helmet meta={[{ name: 'robots', content: 'noindex, follow' }]} />
          ) : null
        }
        {this.headerSection()}
        {
          this.gallerySection(
            checkoutUrl,
            selectedRatePlan,
            prefillDetails,
            isAvailable,
            selectedPrice,
            selectedRoomType,
            coupon,
          )
        }
        {this.goBackSection()}
        <Animation animateFrom={animateFrom} animateTo={animateTo}>
          <Position position={isGalleryOpen ? 'fixed' : 'absolute'} top="0">
            <Size height={`${browserDimensions().height}px`} width="100%">
              <Card borderStyle="hidden" id="singlePageHd" backgroundColor="transparent">
                <Size height="270px" onClick={this.galleryOpen}>
                  <View />
                </Size>
                <Card backgroundColor="greyLighter" borderStyle="hidden" id="singlePageHdBody">
                  {
                    !isAvailable ? (
                      <Space padding={[2.5, 2]} margin={[0, 0, 1, 0]}>
                        <Card backgroundColor="white">
                          <SoldOutInfo onDateChange={this.toggleDatePickerModal} />
                        </Card>
                      </Space>
                    ) : null
                  }
                  {
                    !isEmpty(hotel.name) ? (
                      <Space padding={[0, 2, 2.5, 2]} margin={[0, 0, 1, 0]}>
                        <Card backgroundColor="white">
                          <HotelInfo
                            hotel={hotel}
                            reviews={reviews}
                            isCoupleFriendly={isCoupleFriendly}
                            checkoutUrl={checkoutUrl}
                            selectedRatePlan={selectedRatePlan}
                            selectedPrice={selectedPrice}
                            selectedRoomType={selectedRoomType}
                            travellerInfoSubmit={this.travellerInfoSubmit}
                            userDetail={prefillDetails}
                            checkoutActions={checkoutActions}
                            coupon={coupon}
                          />
                        </Card>
                      </Space>
                    ) : null
                  }
                  {
                    !isEmpty(trigger) ? (
                      <Space padding={[2.5, 2]} margin={[0, 0, 1, 0]}>
                        <Card backgroundColor="white">
                          <PsychologicalTriggers trigger={trigger} />
                        </Card>
                      </Space>
                    ) : null
                  }
                  <Space padding={[2]} margin={[0, 0, 1, 0]}>
                    <Card backgroundColor="white">
                      <PerfectStayBanner isOpen />
                    </Card>
                  </Space>
                  <Space padding={[2.5, 2]} margin={[0, 0, 1, 0]}>
                    <Card backgroundColor="white">
                      <SearchInfo
                        toggleDatePickerModal={this.toggleDatePickerModal}
                        toggleRoomConfigModal={this.toggleRoomConfigModal}
                        isDatePickerOpen={isDatePickerOpen}
                        isRoomConfigOpen={isRoomConfigOpen}
                        onChangeSearchConfig={this.onChangeSearchConfig}
                      />
                    </Card>
                  </Space>
                  {
                    isAvailable ? (
                      <React.Fragment>
                        <Space padding={[2.5, 2]} margin={[0, 0, 1, 0]}>
                          <Card backgroundColor="white">
                            <PriceInfo
                              hotel={hotel}
                              selectedRatePlan={selectedRatePlan}
                              selectedPrice={selectedPrice}
                              selectedRoomType={selectedRoomType}
                              isAvailable={isAvailable}
                              changeRatePlan={
                                this.changeRatePlan(selectedPrice, selectedRatePlan)
                              }
                              treeboPoints={treeboPoints}
                              refreshBookingId={this.refreshBookingId}
                              walletActions={walletActions}
                              coupon={coupon}
                              checkoutUrl={checkoutUrl}
                              travellerInfoSubmit={this.travellerInfoSubmit}
                              userDetail={prefillDetails}
                              checkoutActions={checkoutActions}
                              numberOfNights={numberOfNights}
                            />
                          </Card>
                        </Space>
                        <Space padding={[0, 2]} margin={[0, 1, 1, 1]}>
                          <Card backgroundColor="white">
                            <CouponInfo
                              coupon={coupon}
                              couponActions={couponActions}
                              hotel={hotel}
                              refreshBookingId={this.refreshBookingId}
                              selectedRatePlan={selectedRatePlan}
                            />
                          </Card>
                        </Space>
                        <RoomType
                          prices={price.byRoomTypeId}
                          sortedAvailablePrices={sortedAvailablePrices}
                          selectedPrice={selectedPrice}
                          selectedRatePlan={selectedRatePlan}
                          $roomType={$roomType}
                          selectedRoomType={selectedRoomType}
                          hotel={hotel}
                          isCoupleFriendly={isCoupleFriendly}
                          searchActions={searchActions}
                          changeRatePlan={
                            this.changeRatePlan(selectedPrice, selectedRatePlan)
                          }
                        />
                      </React.Fragment>
                    ) : null
                  }
                  {
                    !isEmpty(needToKnowPolicies) ? (
                      <Space padding={[2.5, 2]} margin={[0, 0, 1, 0]}>
                        <Card backgroundColor="white">
                          <div id="hdp__ntk">
                            <Space padding={[0, 0, 2.5, 0]}>
                              <Text size="m" weight="bold">Need to Know</Text>
                            </Space>
                            <Space padding={[0, 0, 0, 2.5]} margin={[0, 0, 1, 0]}>
                              <NeedToKnow needToKnowPolicies={needToKnowPolicies} scrollToSection={() => this.scrollToSection('hdp__ntk')} />
                            </Space>
                          </div>
                        </Card>
                      </Space>
                    ) : null
                  }
                  {
                    isAvailable ? (
                      <Space padding={[2.5, 2, 0, 2]} margin={[0, 0, 1, 0]}>
                        <Card backgroundColor="white">
                          <TravellerInfo
                            checkoutUrl={checkoutUrl}
                            travellerInfoSubmit={this.travellerInfoSubmit}
                            selectedRatePlan={selectedRatePlan}
                            id="travellerInfoSinglePageHd"
                            setAnimationFromTo={this.setAnimationFromTo}
                            userDetail={prefillDetails}
                            checkoutActions={checkoutActions}
                            hotel={hotel}
                            selectedPrice={selectedPrice}
                            selectedRoomType={selectedRoomType}
                            coupon={coupon}
                            shouldWindowResize={shouldWindowResize}
                          />
                        </Card>
                      </Space>
                    ) : null
                  }
                  <Space margin={[0, 0, 8, 0]}>
                    <Card backgroundColor="white">
                      <MoreInfo
                        hotel={hotel}
                        aboutHotel={seo.content.aboutHotel}
                        seoLinks={seo.content.links}
                        facilities={seo.content.facilities}
                        nearbyPlaces={seo.content.nearbyPlaces}
                        policies={hotel.policies}
                        qna={seo.content.qna}
                      />
                      <OtherResults links={seo.content.links} />
                    </Card>
                  </Space>
                </Card>
              </Card>
            </Size>
          </Position>
        </Animation>
        {
          !isGalleryOpen ? (
            <Size width="100%" height="60px">
              <Space padding={[1]}>
                <Position position="fixed" bottom="0">
                  <Card backgroundColor="white">
                    {
                      isAvailable ? (
                        <Button
                          block
                          name="GSTIN"
                          form="travellerInfoSinglePageHd"
                          onClick={() => this.scrollToSection('travellerInfoSinglePageHd')}
                        >
                          BOOK NOW FOR&nbsp;
                          {
                            selectedRatePlan.totalPrice
                            ? <Price>{selectedRatePlan.sellingPrice}</Price> : '---'
                          }
                        </Button>
                      ) : (
                        <Button
                          block
                          name="GSTIN"
                          onClick={this.toggleDatePickerModal}
                        >
                          CHECK DIFFERENT DATES
                        </Button>
                      )
                    }
                  </Card>
                </Position>
              </Space>
            </Size>
          ) : null
        }
      </React.Fragment>
    );
  }
}

SinglePageHd.propTypes = {
  route: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  isHotelDetailsLoading: PropTypes.bool.isRequired,
  price: PropTypes.object.isRequired,
  isPricesLoading: PropTypes.bool.isRequired,
  search: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  reviews: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
  hotelActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  priceActions: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  reviewActions: PropTypes.object.isRequired,
  coupon: PropTypes.object.isRequired,
  couponActions: PropTypes.object.isRequired,
  checkoutActions: PropTypes.object.isRequired,
  loaderActions: PropTypes.object.isRequired,
  booking: PropTypes.object.isRequired,
  treeboPoints: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state, { match }) => ({
  hotel: state.hotel.byId[match.params.hotelId] || state.hotel.byId[0],
  isHotelDetailsLoading: state.hotel.isHotelDetailsLoading,
  price: state.price,
  isPricesLoading: state.price.isPricesLoading,
  $roomType: state.$roomType,
  search: state.search,
  content: state.content,
  nearbyHotels: omitKeys(state.hotel.byId, ['0', match.params.hotelId]),
  reviews: state.review.results[match.params.hotelId] || state.review.results[0],
  coupon: state.coupon,
  booking: state.booking,
  treeboPoints: state.wallet.byType.TP || state.wallet.byType[''],
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  priceActions: bindActionCreators(priceActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
  uiActions: bindActionCreators(uiActionCreators, dispatch),
  reviewActions: bindActionCreators(reviewActionCreators, dispatch),
  couponActions: bindActionCreators(couponActionCreators, dispatch),
  checkoutActions: bindActionCreators(checkoutActionCreators, dispatch),
  loaderActions: bindActionCreators(loaderActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SinglePageHd);
