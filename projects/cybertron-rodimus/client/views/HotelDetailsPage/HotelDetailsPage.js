import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import qs from 'query-string';
import View from 'leaf-ui/cjs/View/web';
import Space from 'leaf-ui/cjs/Space/web';
import Card from 'leaf-ui/cjs/Card/web';
import ABButton from '../../components/ABButton/ABButton';
import Button from '../../components/Button/Button';
import Ripple from '../../components/Ripple/Ripple';
import { Heading } from '../../components/Typography/Typography';
import SearchHeader from '../../components/SearchHeader/SearchHeader';
import SearchWidgetAsync from '../SearchWidget/SearchWidgetAsync';
import CheckoutPage from '../CheckoutPage';
import PerfectStayBanner from '../../components/PerfectStayBanner/PerfectStayBanner';
import Hotel from './Hotel/Hotel';
import StaticMap from './Address/StaticMap';
import RoomType from './RoomType/RoomType';
import ReviewsAndRatings from '../ReviewsAndRatings/ReviewsAndRatings';
import OtherResults from './OtherResults/OtherResults';
import MoreInfo from './MoreInfo/MoreInfo';
import ThingsGuestLove from './ThingsGuestLove/ThingsGuestLove';
import NeedToKnow from '../../components/NeedToKnow/NeedToKnow';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as hotelActionCreators from '../../services/hotel/hotelDuck';
import * as priceActionCreators from '../../services/price/priceDuck';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as toastActionCreators from '../../services/toast/toastDuck';
import * as uiActionCreators from '../../services/ui/uiDuck';
import * as hotelService from '../../services/hotel/hotelService';
import analyticsService from '../../services/analytics/analyticsService';
import * as abService from '../../services/ab/abService';
import checkoutService from '../../services/checkout/checkoutService';
import searchService from '../../services/search/searchService';
import priceService from '../../services/price/priceService';
import { makeRoomTypeId, constants } from '../../services/utils';
import './hotelDetailsPage.css';
import PsychologicalTriggers from '../../components/PsychologicalTriggers/PsychologicalTriggers';

class HotelDetailsPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match, history, req }) => {
    const decodedHotelId = hotelService.decodeHotelId(match.params.hotelId);
    if (decodedHotelId) {
      return history.replace({
        pathname: history.location.pathname.replace(match.params.hotelId, decodedHotelId),
        search: history.location.search,
      });
    }
    const promises = [];
    dispatch(searchActionCreators.setSearchState({ ...req.query, ...match.params }));
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(hotelActionCreators.getHotelDetails(match.params.hotelId)));
    return Promise.all(promises);
  }

  state = {
    isRoomPricesError: false,
  }

  componentDidMount() {
    const {
      match,
      location,
      searchActions,
      contentActions,
      route,
    } = this.props;
    searchActions.setSearchState({ ...qs.parse(location.search), ...match.params });
    contentActions.getSeoContent(route, match.params);
    this.fetchDetailsAndPrices(match.params.hotelId);
    SearchWidgetAsync.preload();
    CheckoutPage.preload();
  }

  componentWillReceiveProps(nextProps) {
    const { props } = this;
    const { searchActions } = this.props;
    const query = qs.parse(props.location.search);
    const nextQuery = qs.parse(nextProps.location.search);

    if (
      props.match.params.hotelId !== nextProps.match.params.hotelId
      || query.hotel_id !== nextQuery.hotel_id
      || query.checkin !== nextQuery.checkin
      || query.checkout !== nextQuery.checkout
      || query.roomconfig !== nextQuery.roomconfig
    ) {
      searchActions.setSearchState({ ...nextQuery, ...nextProps.match.params });
      nextProps.contentActions.getSeoContent(nextProps.route, nextProps.match.params);
      this.fetchDetailsAndPrices(nextProps.match.params.hotelId);
    }
  }

  getRoomPrices = (hotelId) => {
    const {
      contentActions,
      priceActions,
      searchActions,
    } = this.props;
    if (searchActions.isSearchWidgetValid()) {
      priceActions.getRoomPrices(hotelId)
        .then((res) => {
          if (res.error) this.setState({ isRoomPricesError: true });
          return contentActions.getPsychologicalTriggers([hotelId]);
        })
        .then(() => {
          this.trackPsychologicalTriggerEvent(hotelId);
          this.captureAnalytics();
        });
    }
  }

  getAssuredAmenities = () => {
    const { hotel } = this.props;
    return hotel && hotel.amenities && hotel.amenities.some((amenity) => amenity.css_class === 'ac')
      ? this.assuredAmenities : this.assuredAmenities.slice(1);
  }

  getAvailableRoomTypeIds = (roomTypeIds, prices) =>
    roomTypeIds.filter((roomTypeId) => (prices[roomTypeId] && prices[roomTypeId].available));

  getSelectedRoomTypeId = (prices, chepeastRoomTypeId) => {
    const { location, match: { params } } = this.props;
    const query = qs.parse(location.search);

    return query.roomtype
      && prices[makeRoomTypeId(params.hotelId, query.roomtype)]
      && prices[makeRoomTypeId(params.hotelId, query.roomtype)].available
      ? makeRoomTypeId(params.hotelId, query.roomtype) : chepeastRoomTypeId;
  }

  getSelectedRatePlan = (selectedPrice) => {
    const { location } = this.props;
    const query = qs.parse(location.search);

    return query.rateplan && selectedPrice.ratePlans[query.rateplan]
      ? selectedPrice.ratePlans[query.rateplan]
      : priceService.sortRatePlan(selectedPrice.ratePlans)[0];
  }

  captureAnalytics() {
    const {
      hotel,
      search,
      match,
      location,
      price,
      content: { psychologicalTriggers },
    } = this.props;

    const { checkIn, checkOut, numberOfNights, abwDuration } =
      searchService.formattedDatePicker(search.datePicker.range, constants.dateFormat.query);
    const availableRoomTypeIds =
      this.getAvailableRoomTypeIds(hotel.roomTypeIds, price.byRoomTypeId);
    const hotelPrices = priceService.getPricesForRoomTypeIds(availableRoomTypeIds, price);
    const sortedAvailablePrices = priceService.sortPrice(hotelPrices);
    const selectedRoomTypeId = this.getSelectedRoomTypeId(price.byRoomTypeId, sortedAvailablePrices[0].id) || '0|oak';
    const selectedPrice = price.byRoomTypeId[selectedRoomTypeId];
    const psychologicalTrigger = psychologicalTriggers.byRoomTypeId[selectedRoomTypeId];
    const analyticsProperties = {
      hotelId: match.params.hotelId,
      hotelName: hotel.name,
      pageUrl: location.pathname,
      checkin: checkIn,
      checkout: checkOut,
      lengthOfStay: numberOfNights.count,
      abwDays: abwDuration,
      roomConfig: search.roomConfig.rooms.map((room) => `${room.adults}-${room.kids || 0}`).join(','),
      price: Math.round(Object.values(selectedPrice.ratePlans)[0].sellingPrice),
      triggerDisplayed: !isEmpty(psychologicalTrigger),
      triggerType: !isEmpty(psychologicalTrigger) ? psychologicalTrigger.type : '',
      ...abService.impressionProperties(['hdCtaMobile', 'pwaSinglePageHd']),
    };
    analyticsService.hotelContentViewed(analyticsProperties);
  }

  isRoomTypePriceAvailable = (roomTypeIds, prices) =>
    roomTypeIds && roomTypeIds.some((roomTypeId) => (prices[roomTypeId]))

  trackPsychologicalTriggerEvent = (hotelId) => {
    const {
      price,
    } = this.props;
    const selectedRoomTypeId = this.getSelectedRoomTypeId(price.byRoomTypeId);
    analyticsService.psychologicalTriggersViewed(
      'Hotel Detail',
      hotelId,
      selectedRoomTypeId,
    );
  }

  changeDates = () => {
    const { searchActions, uiActions } = this.props;
    searchActions.setSearchModal('datePicker');
    uiActions.showSearchWidget(true);
  }

  fetchDetailsAndPrices = (hotelId) => {
    const {
      isHotelDetailsLoading,
      isPricesLoading,
      hotelActions,
    } = this.props;
    if (!isHotelDetailsLoading) {
      hotelActions.getHotelDetails(hotelId);
    }
    if (!isPricesLoading) {
      this.getRoomPrices(hotelId);
    }
  }

  renderChurnedOutHotel = () => {
    const {
      hotel,
      content: { seo },
    } = this.props;
    return (
      <div>
        <Helmet meta={[{ name: 'robots', content: 'noindex, follow' }]} />
        <Hotel
          hotel={hotel}
          room={{}}
        />
        <StaticMap
          coordinates={{}}
          address={{}}
          width={400}
          height={300}
        />
        <ReviewsAndRatings />
        <MoreInfo
          hotel={hotel}
          aboutHotel={seo.content.aboutHotel}
          seoLinks={seo.content.links}
          facilities={seo.content.facilities}
          nearbyPlaces={seo.content.nearbyPlaces}
          policies={hotel.policies}
          qna={seo.content.qna}
        />
        <OtherResults links={seo.content.links} />
        <Button flat block large disabled className="floaty">
          CHECK DIFFERENT DATES
        </Button>
      </div>
    );
  }

  renderHotel = () => {
    const { isRoomPricesError } = this.state;
    const {
      hotel,
      price,
      search,
      content: { seo, psychologicalTriggers },
      location,
      $roomType,
    } = this.props;
    const availableRoomTypeIds =
      this.getAvailableRoomTypeIds(hotel.roomTypeIds, price.byRoomTypeId);
    const hotelPrices = priceService.getPricesForRoomTypeIds(availableRoomTypeIds, price);
    const sortedAvailablePrices = priceService.sortPrice(hotelPrices);
    const selectedRoomTypeId = this.getSelectedRoomTypeId(price.byRoomTypeId, sortedAvailablePrices[0].id) || '0|oak';
    const selectedPrice = price.byRoomTypeId[selectedRoomTypeId];
    const selectedRatePlan = this.getSelectedRatePlan(selectedPrice) || {};
    const selectedRoomType = $roomType.byId[selectedRoomTypeId];

    const isAvailable =
      (!isRoomPricesError && !this.isRoomTypePriceAvailable(hotel.roomTypeIds, price.byRoomTypeId))
      || !isEmpty(availableRoomTypeIds);
    const checkoutUrl = isAvailable &&
      checkoutService.makeCheckoutLocation(search, selectedRoomType, hotel, selectedRatePlan);
    const needToKnowPolicies = hotelService.getNeedToKnowPolicies(hotel.policies);
    const query = qs.parse(location.search);

    const trigger = !isEmpty(availableRoomTypeIds) ?
      psychologicalTriggers.byRoomTypeId[selectedPrice.id] : {};

    return (
      <div>
        <Hotel
          hotel={hotel}
          price={selectedPrice}
          ratePlan={selectedRatePlan}
          isPricesLoading={price.isPricesLoading}
          isAvailable={isAvailable}
        />
        {
          !isEmpty(trigger) ? (
            <Space padding={[0, 2, 2]}>
              <View>
                <PsychologicalTriggers trigger={trigger} />
              </View>
            </Space>
          ) : null
        }
        {
          isAvailable ? (
            <RoomType
              prices={price.byRoomTypeId}
              sortedAvailablePrices={sortedAvailablePrices}
              selectedPrice={selectedPrice}
              selectedRatePlan={selectedRatePlan}
              checkoutUrl={checkoutUrl}
              $roomType={$roomType}
              selectedRoomType={selectedRoomType}
              search={search}
            />
          ) : null
        }

        <Space padding={[2]} margin={[0, 0, 1, 0]}>
          <Card backgroundColor="white">
            <PerfectStayBanner isOpen />
          </Card>
        </Space>
        {
          !isEmpty(needToKnowPolicies) ? (
            <div className="hdp__ntk">
              <Heading tag="h3" type="3" className="hdp__ntk__title">Need to Know</Heading>
              <NeedToKnow className="hdp__ntk__body" needToKnowPolicies={needToKnowPolicies} />
            </div>
          ) : null
        }
        <StaticMap
          coordinates={hotel.coordinates}
          address={hotel.address}
          width={400}
          height={300}
        />
        {
          !isEmpty(seo.content.guestFeedbacks) ? (
            <ThingsGuestLove feedbacks={seo.content.guestFeedbacks} />
          ) : null
        }
        <ReviewsAndRatings />
        <MoreInfo
          hotel={hotel}
          aboutHotel={seo.content.aboutHotel}
          seoLinks={seo.content.links}
          facilities={seo.content.facilities}
          nearbyPlaces={seo.content.nearbyPlaces}
          policies={hotel.policies}
          qna={seo.content.qna}
        />
        <OtherResults links={seo.content.links} />
        {
          isAvailable ? (
            <Ripple>
              <Link
                className="floaty"
                to={checkoutUrl}
                onClick={() => analyticsService.bookNowClicked({
                  hotelId: hotel.id,
                  selectedRatePlan,
                  selectedPrice,
                  selectedRoomType,
                  ...abService.actionProperties(['hdCtaMobile']),
                })}
              >
                <ABButton
                  id="t-hdBookCTA"
                  block
                  size="large"
                  experiment="hdCtaMobile"
                  render={(text) => (
                      !query.checkin || !query.checkout ?
                        'REVIEW DATES AND BOOK' : (text || 'BOOK NOW')
                    )}
                />
              </Link>
            </Ripple>
          ) : (
            <Ripple>
              <div className="floaty" onClick={this.changeDates}>
                <Button flat block large>
                  CHECK DIFFERENT DATES
                </Button>
              </div>
            </Ripple>
          )
        }
      </div>
    );
  }

  render() {
    const { hotel } = this.props;

    return (
      <div className="hdp">
        <SearchHeader />
        { hotel.isActive ? this.renderHotel() : this.renderChurnedOutHotel() }
      </div>
    );
  }
}

HotelDetailsPage.propTypes = {
  route: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  isHotelDetailsLoading: PropTypes.bool.isRequired,
  price: PropTypes.object.isRequired,
  isPricesLoading: PropTypes.bool.isRequired,
  search: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  uiActions: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
  hotelActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  priceActions: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state, { match }) => ({
  hotel: state.hotel.byId[match.params.hotelId] || state.hotel.byId[0],
  isHotelDetailsLoading: state.hotel.isHotelDetailsLoading,
  price: state.price,
  isPricesLoading: state.price.isPricesLoading,
  $roomType: state.$roomType,
  search: state.search,
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  priceActions: bindActionCreators(priceActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
  uiActions: bindActionCreators(uiActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HotelDetailsPage);
