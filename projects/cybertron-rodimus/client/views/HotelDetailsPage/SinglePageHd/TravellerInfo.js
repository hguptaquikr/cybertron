import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Space from 'leaf-ui/cjs/Space/web';
import Text from 'leaf-ui/cjs/Text/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';
import TextInput from 'leaf-ui/cjs/TextInput/web';
import Checkbox from 'leaf-ui/cjs/Checkbox/web';
import Form from 'leaf-ui/cjs/Form/web';
import * as utils from '../../../services/utils';
import analyticsService from '../../../services/analytics/analyticsService';

class TravellerInfo extends Component {
  state = {
    isGSTFieldsVisible: false,
  }

  onSubmit = (fields) => {
    const { isGSTFieldsVisible } = this.state;
    const {
      travellerInfoSubmit,
      selectedRatePlan,
      checkoutUrl,
      hotel: { hotelId },
      selectedPrice,
      selectedRoomType,
      coupon,
    } = this.props;
    const bypassPaymentStep = selectedRatePlan.sellingPrice === 0;
    travellerInfoSubmit({ details: fields, isGSTFieldsVisible, bypassPaymentStep, checkoutUrl });
    analyticsService
      .hdBookNowClicked({ hotelId, selectedPrice, selectedRatePlan, selectedRoomType, coupon });
    analyticsService.updateClevertapProfile({
      name: fields.name,
      email: fields.email,
      phone: fields.mobile,
      organization_name: fields.organizationName,
      organization_taxcode: fields.organizationTaxcode,
      organization_address: fields.organizationAddress,
    });
  }

  onfocus = (inputId) => () => {
    if (!utils.isIos) {
      const element = document.getElementById(inputId);
      document.getElementById('singlePageHd').scrollTo({
        top: element.offsetTop - 56,
        behavior: 'smooth',
      });
    }
  }

  toggleGSTFieldsVisibility = () => {
    this.inputGstinRef.blur();
    this.setState((prevState) => ({
      isGSTFieldsVisible: !prevState.isGSTFieldsVisible,
    }));
  }

  render() {
    const { isGSTFieldsVisible } = this.state;
    const {
      id,
      userDetail: {
        name,
        email,
        mobile,
        organizationName,
        organizationAddress,
        organizationTaxcode,
      },
    } = this.props;

    return (
      <React.Fragment>
        <Form
          enableReinitialize
          initialValues={{
            [`${id}-name`]: name || '',
            [`${id}-mobile`]: mobile || '',
            [`${id}-email`]: email || '',
            [`${id}-organizationTaxcode`]: organizationTaxcode || '',
            [`${id}-organizationName`]: organizationName || '',
            [`${id}-organizationAddress`]: organizationAddress || '',
          }}
          onSubmit={(values) => {
            const travellerValues = {};
            Object.keys(values).forEach((key) => {
              if (!!values[key] === true) {
                travellerValues[key.replace(`${id}-`, '')] = String(values[key]);
              } else {
                travellerValues[key.replace(`${id}-`, '')] = undefined;
              }
            });
            this.onSubmit(travellerValues);
          }}
          validationSchema={
            Form.validation.object().shape({
              [`${id}-name`]: Form.validation.string()
                .required('Name is required'),
              [`${id}-mobile`]: Form.validation.string()
                .max(10, 'Mobile Number can not be less than 10 digits')
                .min(10, 'Mobile Number can not be more than 10 digits')
                .required('Mobile Number is required'),
              [`${id}-email`]: Form.validation.string()
                .required('Email is required')
                .matches(utils.emailRegex, { message: 'Invalid email address' }),
              [`${id}-organizationTaxcode`]: !isGSTFieldsVisible ? Form.validation.string() :
                Form.validation.string()
                .required('GST Identification No. is required')
                .matches(utils.gstnRegex, { message: 'GST Identification No. is not valid' }),
              [`${id}-organizationName`]: !isGSTFieldsVisible ? Form.validation.string() :
                Form.validation.string()
                .required('Organization Name is required'),
              [`${id}-organizationAddress`]: !isGSTFieldsVisible ? Form.validation.string() :
                Form.validation.string()
                .required('Organization Address is reqiured'),

            })
          }
        >
          <Form.Form id={id} autoComplete="off">
            <Flex flexDirection="row">
              <View>
                <Text size="m" weight="bold">Primary Traveller</Text>
              </View>
            </Flex>
            <Space padding={[1.5, 0, 0, 0]}>
              <TextInput
                name={`${id}-name`}
                placeholder="Name"
                block
                id="t-name"
                onFocus={this.onfocus('t-name')}
                defaultValue={name}
              />
            </Space>
            <Space padding={[1, 0, 0, 0]}>
              <TextInput
                name={`${id}-mobile`}
                placeholder="Mobile No."
                block
                id="t-number"
                onFocus={this.onfocus('t-number')}
                defaultValue={mobile}
              />
            </Space>
            <Space padding={[1, 0, 0, 0]}>
              <TextInput
                name={`${id}-email`}
                placeholder="Email"
                block
                id="t-email"
                onFocus={this.onfocus('t-email')}
                defaultValue={email}
              />
            </Space>
            <Space padding={[3.5, 0, 2.5, 0]}>
              <Checkbox
                innerRef={(ref) => { this.inputGstinRef = ref; }}
                name={`${id}-gstin`}
                label="Use GSTIN for this booking (optional)"
                onChange={this.toggleGSTFieldsVisibility}
                defaultValue={name}
              />
            </Space>
            {
              isGSTFieldsVisible ? (
                <div>
                  <Space padding={[0, 0, 0, 0]}>
                    <TextInput
                      placeholder="GST Identification No."
                      name={`${id}-organizationTaxcode`}
                      block
                      id="t-gstn"
                      onFocus={this.onfocus('t-gstn')}
                      defaultValue={organizationTaxcode}
                    />
                  </Space>
                  <Space padding={[1, 0, 0, 0]}>
                    <TextInput
                      placeholder="Company Name"
                      name={`${id}-organizationName`}
                      block
                      id="t-companyName"
                      onFocus={this.onfocus('t-companyName')}
                      defaultValue={organizationName}
                    />
                  </Space>
                  <Space padding={[1, 0, 2.5, 0]}>
                    <TextInput
                      placeholder="Company Address"
                      name={`${id}-organizationAddress`}
                      block
                      id="t-companyAddress"
                      onFocus={this.onfocus('t-companyAddress')}
                      defaultValue={organizationAddress}
                    />
                  </Space>
                </div>
              ) : null
            }
          </Form.Form>
        </Form>
      </React.Fragment>
    );
  }
}

TravellerInfo.propTypes = {
  checkoutUrl: PropTypes.object.isRequired,
  travellerInfoSubmit: PropTypes.func.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
  userDetail: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  hotel: PropTypes.object.isRequired,
  selectedPrice: PropTypes.object.isRequired,
  selectedRoomType: PropTypes.object.isRequired,
  coupon: PropTypes.object.isRequired,
};

export default TravellerInfo;
