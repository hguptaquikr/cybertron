import React from 'react';
import PropTypes from 'prop-types';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';

const UserReview = ({ review }) => (
  <Space margin={[0, 0, 4.5, 0]}>
    <View key={review.title}>
      <Text size="m" weight="medium">{review.title}</Text>
      <Space margin={[1, 0, 0, 0]}>
        <Flex flexDirection="column">
          <View>
            <Flex flexDirection="row">
              <View>
                <img className="old-image" src={review.ratingImage} alt="rating" />
                <Space margin={[0, 0, 0, 1]}>
                  <Text color="grey">{review.user.name}, {review.publishDate}</Text>
                </Space>
              </View>
            </Flex>
            <Space margin={[1, 0, 0, 0]}>
              <Text component="p">{review.description}</Text>
            </Space>
          </View>
        </Flex>
      </Space>
    </View>
  </Space>
);

UserReview.propTypes = {
  review: PropTypes.object.isRequired,
};

export default UserReview;
