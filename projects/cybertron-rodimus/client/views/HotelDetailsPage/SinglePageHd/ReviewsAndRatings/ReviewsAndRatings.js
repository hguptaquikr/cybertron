import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import qs from 'query-string';
import Space from 'leaf-ui/cjs/Space/web';
import Text from 'leaf-ui/cjs/Text/web';
import Button from 'leaf-ui/cjs/Button/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import Image from 'leaf-ui/cjs/Image/web';
import Price from 'leaf-ui/cjs/Price/web';
import Card from 'leaf-ui/cjs/Card/web';
import View from 'leaf-ui/cjs/View/web';
import Divider from 'leaf-ui/cjs/Divider/web';
import Position from 'leaf-ui/cjs/Position/web';
import Size from 'leaf-ui/cjs/Size/web';
import TravellerInfo from '../TravellerInfo';
import * as reviewActionCreators from '../../../../services/review/reviewDuck';
import OverallRatings from './OverallRatings';
import JointlyCollectedReviews from './JointlyCollectedReviews/JointlyCollectedReviews';
import Modal from '../../../../components/Modal/Modal';

class ReviewsAndRatings extends Component {
  state = {
    showAllReviews: false,
    showTravellerInfo: false,
  }

  componentDidMount() {
    const { reviewActions, match } = this.props;
    reviewActions.getHotelReviews(match.params.hotelId);
  }

  toggleReviewsPageVisibility = () =>
    this.setState((prevState) => ({
      showAllReviews: !prevState.showAllReviews,
    }));

  toggleTravellerInfo = () => {
    this.setState((prevState) => ({
      showTravellerInfo: !prevState.showTravellerInfo,
    }));
  }

  render() {
    const { showAllReviews, showTravellerInfo } = this.state;
    const {
      hotel,
      selectedPrice,
      selectedRoomType,
      reviews,
      reviews: {
        overallRating,
        userReviews,
        writeReviewsUrl,
      },
      match,
      checkoutUrl,
      travellerInfoSubmit,
      selectedRatePlan,
      userDetail,
      checkoutActions,
      coupon,
      isAvailable,
      toggleDatePickerModal,
    } = this.props;

    return reviews.isTaEnabled && reviews.overallRating.rating ? (
      <React.Fragment>
        <Flex>
          <Image
            src={reviews.overallRating.ratingImage}
            alt="Overall Rating"
            width="78px"
            height="100%"
          />
        </Flex>&nbsp;
        <Text
          color="blue"
          component="span"
          onClick={this.toggleReviewsPageVisibility}
          weight="medium"
        >
          {reviews.noOfReviews} Reviews
        </Text>
        <Modal
          closeIcon="close"
          isOpen={showTravellerInfo}
          label="TripAdvisor Reviews"
          onClose={this.toggleTravellerInfo}
        >
          <React.Fragment>
            <Space padding={[2.5, 2, 8, 2]}>
              <Card backgroundColor="white">
                <TravellerInfo
                  checkoutUrl={checkoutUrl}
                  travellerInfoSubmit={travellerInfoSubmit}
                  selectedRatePlan={selectedRatePlan}
                  id="travellerInfoSinglePageHdModal"
                  userDetail={userDetail}
                  checkoutActions={checkoutActions}
                  hotel={hotel}
                  selectedPrice={selectedPrice}
                  selectedRoomType={selectedRoomType}
                  coupon={coupon}
                />
              </Card>
            </Space>
            <Position position="fixed" bottom="0">
              <Size width="100%">
                <Space padding={[0, 1]}>
                  <div>
                    <Space padding={[0, 0, 1, 0]}>
                      <Card backgroundColor="white">
                        <Button
                          block
                          name="GSTINBOOK"
                          form="travellerInfoSinglePageHdModal"
                        >
                          BOOK NOW FOR&nbsp;
                          {
                            selectedRatePlan.totalPrice
                            ? <Price>{selectedRatePlan.sellingPrice}</Price> : '---'
                          }
                        </Button>
                      </Card>
                    </Space>
                  </div>
                </Space>
              </Size>
            </Position>
          </React.Fragment>
        </Modal>
        <Modal
          closeIcon="close"
          isOpen={showAllReviews}
          label="TripAdvisor Reviews"
          onClose={this.toggleReviewsPageVisibility}
        >
          <React.Fragment>
            <Space padding={[0, 0, 1, 0]} margin={[0, 0, 7.5, 0]}>
              <div>
                <Space padding={[2]}>
                  <div>
                    <Text size="l" weight="bold">
                      Overall Ratings
                    </Text>
                    <OverallRatings overallRating={overallRating} />
                  </div>
                </Space>
                <Space padding={[2]}>
                  <div>
                    <Space margin={[0, 0, 1, 0]}>
                      <Text size="l" weight="bold">
                        User Reviews
                      </Text>
                    </Space>
                    <Flex flexDirection="row">
                      <View>
                        <Text>
                          In partnership with
                        </Text>
                        <Image
                          width="80px"
                          height="14px"
                          src="https://images.treebohotels.com/images/ta_logo.svg"
                          alt="TripAdvisor"
                        />
                      </View>
                    </Flex>
                    <JointlyCollectedReviews reviews={userReviews} previewNumber={5} />
                  </div>
                </Space>
                <Space padding={[0, 1, 0, 1]}>
                  <Text weight="medium">
                    <Link
                      to={{
                        pathname: `/tripadvisor/reviews-${match.params.hotelId}/`,
                        search: qs.stringify({ taWidgetUrl: writeReviewsUrl }),
                      }}
                    >
                      <Button block kind="outlined">WRITE A REVIEW</Button>
                    </Link>
                  </Text>
                </Space>
              </div>
            </Space>
            <Position position="fixed" bottom="0">
              <Size width="100%">
                <Space padding={[0, 1]}>
                  <div>
                    <Space padding={[0, 0, 1, 0]}>
                      <Card backgroundColor="white">
                        <Divider />
                        <Space margin={[1, 0, 0, 0]}>
                          {
                            isAvailable ? (
                              <Button
                                block
                                name="BookNow"
                                onClick={this.toggleTravellerInfo}
                              >
                                BOOK NOW FOR&nbsp;
                                {
                                  selectedRatePlan.totalPrice
                                  ? <Price>{selectedRatePlan.sellingPrice}</Price> : '---'
                                }
                              </Button>
                            ) : (
                              <Button block name="GSTIN" onClick={toggleDatePickerModal}>CHECK DIFFERENT DATES</Button>
                            )
                          }
                        </Space>
                      </Card>
                    </Space>
                  </div>
                </Space>
              </Size>
            </Position>
          </React.Fragment>
        </Modal>
      </React.Fragment>
    ) : null;
  }
}

ReviewsAndRatings.propTypes = {
  hotel: PropTypes.object.isRequired,
  selectedPrice: PropTypes.object.isRequired,
  selectedRoomType: PropTypes.object.isRequired,
  reviews: PropTypes.object.isRequired,
  reviewActions: PropTypes.object.isRequired,
  match: PropTypes.object,
  checkoutUrl: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
  userDetail: PropTypes.object.isRequired,
  checkoutActions: PropTypes.object.isRequired,
  travellerInfoSubmit: PropTypes.func.isRequired,
  coupon: PropTypes.object.isRequired,
  isAvailable: PropTypes.bool.isRequired,
  toggleDatePickerModal: PropTypes.func.isRequired,
};

const mapStateToProps = (state, { match }) => ({
  reviews: state.review.results[match.params.hotelId] || state.review.results[0],
});

const mapDispatchToProps = (dispatch) => ({
  reviewActions: bindActionCreators(reviewActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(ReviewsAndRatings);
