import React from 'react';
import PropTypes from 'prop-types';
import Space from 'leaf-ui/cjs/Space/web';
import Text from 'leaf-ui/cjs/Text/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import Card from 'leaf-ui/cjs/Card/web';
import View from 'leaf-ui/cjs/View/web';
import Size from 'leaf-ui/cjs/Size/web';
import Image from 'leaf-ui/cjs/Image/web';
import { Heading } from '../../../../components/Typography/Typography';

export const OverallRatingsHeader = ({ rating, ratingImage }) => (
  <View>
    <Heading
      tag="h3"
      type="1"
      preview={!rating}
      previewStyle={{ width: '10%' }}
    >
      <span>{rating}</span>
      <span className="rar-or__header__rating--light">/5</span>
    </Heading>
    <Image
      src={ratingImage}
      alt="Overall Rating"
    />
  </View>
);

OverallRatingsHeader.propTypes = {
  rating: PropTypes.string.isRequired,
  ratingImage: PropTypes.string.isRequired,
};

const OverallRatings = ({ overallRating }) => {
  const { subRatings } = overallRating;
  return (
    <View>
      {
        subRatings.map((subRating, index) => (
          <Space margin={[2, 0, 0, 0]} key={`${subRating.level}${index + 1}`}>
            <Flex
              flexDirection="row"
              justifyContent="space-between"
              alignItems="center"
            >
              <View>
                <Size width="33%">
                  <Text>{subRating.level}</Text>
                </Size>
                <Size height="4px" width="30%">
                  <Card backgroundColor="grey" borderStyle="hidden">
                    <Size height="4px" width={`${subRating.percent}%`}>
                      <Card backgroundColor="green" borderStyle="hidden" />
                    </Size>
                  </Card>
                </Size>
                <Size width="33%">
                  <Text color="greyDark" align="right">{subRating.noOfReviews}</Text>
                </Size>
              </View>
            </Flex>
          </Space>
        ))
      }
    </View>
  );
};

OverallRatings.propTypes = {
  overallRating: PropTypes.object.isRequired,
};

export default OverallRatings;
