import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Space from 'leaf-ui/cjs/Space/web';
import Text from 'leaf-ui/cjs/Text/web';
import { Link } from 'react-router-dom';
import UserReview from '../UserReview/UserReview';
import priceService from '../../../../../services/price/priceService';
import * as hotelService from '../../../../../services/hotel/hotelService';

class JointlyCollectedReviews extends Component {
  makeHotelDetailsLocation = (hotel) => {
    const { price, location } = this.props;
    const hotelPrices = priceService.getPricesForRoomTypeIds(hotel.roomTypeIds, price);
    const cheapestRoomType = priceService.sortPrice(hotelPrices)[0];
    const cheapestRatePlan = priceService.sortRatePlan(cheapestRoomType.ratePlans)[0] || {};
    return hotelService
      .makeHotelDetailsLocation(hotel, cheapestRoomType, location, cheapestRatePlan);
  }

  render() {
    const { reviews, hotels } = this.props;
    return (
      <Space margin={[3, 0, 0, 0]}>
        <div>
          {
            reviews
              .map((review, index) => hotels && hotels[review.hotel.id] && review.hotel ? (
                <Text weight="medium" key={`${index + 1}`}>
                  <Link
                    to={this.makeHotelDetailsLocation(hotels[review.hotel.id])}
                  >
                    <UserReview review={review} />
                  </Link>
                </Text>
              ) : <UserReview review={review} key={`${index + 1}`} />)
          }
        </div>
      </Space>
    );
  }
}

JointlyCollectedReviews.propTypes = {
  reviews: PropTypes.array.isRequired,
  location: PropTypes.object,
  hotels: PropTypes.object,
  price: PropTypes.object,
};

export default JointlyCollectedReviews;
