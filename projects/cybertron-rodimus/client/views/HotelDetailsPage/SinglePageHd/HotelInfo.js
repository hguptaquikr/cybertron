import React from 'react';
import PropTypes from 'prop-types';
import Space from 'leaf-ui/cjs/Space/web';
import Gallery from 'leaf-ui/cjs/Gallery/web';
import Tag from 'leaf-ui/cjs/Tag/web';
import Text from 'leaf-ui/cjs/Text/web';
import Link from 'leaf-ui/cjs/Link/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';
import Size from 'leaf-ui/cjs/Size/web';
import ReviewsAndRatings from './ReviewsAndRatings/ReviewsAndRatings';
import { getMapsUrl } from '../../../services/utils';

const HotelInfo = ({
  hotel,
  isCoupleFriendly,
  checkoutUrl,
  selectedRatePlan,
  userDetail,
  checkoutActions,
  travellerInfoSubmit,
  selectedPrice,
  selectedRoomType,
  coupon,
  isAvailable,
  toggleDatePickerModal,
}) => (
  <React.Fragment>
    <Gallery>
      {
        isCoupleFriendly ? (
          <Size width="120px">
            <Space margin={[2, 0, 0, 0]}>
              <Tag
                color="lagoon"
                kind="outlined"
                size="medium"
              >
                Couple Friendly
              </Tag>
            </Space>
          </Size>
        ) : null
      }
    </Gallery>
    <Space padding={[2, 0, 0, 0]}>
      <Text size="l" weight="medium">
        {hotel.name}
      </Text>
    </Space>
    <Space padding={[1, 0, 0, 0]}>
      <Flex alignItems="center" flexDirection="row">
        <View>
          <ReviewsAndRatings
            checkoutUrl={checkoutUrl}
            selectedRatePlan={selectedRatePlan}
            userDetail={userDetail}
            checkoutActions={checkoutActions}
            travellerInfoSubmit={travellerInfoSubmit}
            hotel={hotel}
            selectedPrice={selectedPrice}
            selectedRoomType={selectedRoomType}
            coupon={coupon}
            isAvailable={isAvailable}
            toggleDatePickerModal={toggleDatePickerModal}
          />
        </View>
      </Flex>
    </Space>
    <Space padding={[1, 0, 0, 0]}>
      <Flex alignItems="baseline" flexDirection="row">
        <View>
          <Text color="greyDark">
            {hotel.address.locality}, {hotel.address.city} &nbsp;
          </Text>
          <Size minWidth="90px">
            <Text weight="medium">
              <Link
                href={getMapsUrl(hotel.coordinates.lat, hotel.coordinates.lng)}
              >
                View in Maps
              </Link>
            </Text>
          </Size>
        </View>
      </Flex>
    </Space>
  </React.Fragment>
);

HotelInfo.propTypes = {
  hotel: PropTypes.object.isRequired,
  isCoupleFriendly: PropTypes.bool.isRequired,
  checkoutUrl: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
  userDetail: PropTypes.object.isRequired,
  checkoutActions: PropTypes.object.isRequired,
  travellerInfoSubmit: PropTypes.func.isRequired,
  selectedPrice: PropTypes.object.isRequired,
  selectedRoomType: PropTypes.object.isRequired,
  coupon: PropTypes.object.isRequired,
  isAvailable: PropTypes.bool.isRequired,
  toggleDatePickerModal: PropTypes.func.isRequired,
};

export default HotelInfo;
