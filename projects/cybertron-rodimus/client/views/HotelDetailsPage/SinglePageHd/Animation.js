import React from 'react';
import styled from 'styled-components';

export const Animation = styled(
  ({
    component,
    children,
    animateFrom,
    animateTo,
    type,
    isAnimate,
    ...props
  }) => React.cloneElement(children, props),
)`
  transition-duration: 0.5s;
  transition-timing-function: ease-in-out;
  transform: ${(props) => `translate(${props.animateFrom}, ${props.animateTo})`};
  overflow-y: scroll;
  -webkit-overflow-scrolling: touch;
`;
