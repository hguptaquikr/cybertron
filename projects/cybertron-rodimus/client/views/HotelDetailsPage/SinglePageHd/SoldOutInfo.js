import React from 'react';
import PropTypes from 'prop-types';
import Text from 'leaf-ui/cjs/Text/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';

const SoldOut = ({ onDateChange }) => (
  <Flex
    flexDirection="row"
    alignItems="center"
    justifyContent="space-between"
  >
    <View>
      <Text color="red" size="xl" weight="semibold">Sold Out</Text>
      <Text
        color="blue"
        weight="medium"
        component="span"
        onClick={onDateChange}
      >
       Try Different Dates
      </Text>
    </View>
  </Flex>
);

SoldOut.propTypes = {
  onDateChange: PropTypes.func.isRequired,
};

export default SoldOut;
