import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import qs from 'query-string';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import Card from 'leaf-ui/cjs/Card/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';
import Button from 'leaf-ui/cjs/Button/web';
import Position from 'leaf-ui/cjs/Position/web';
import Modal from '../../../../components/Modal/Modal';
import RedeemTreeboRewards from '../../../../components/Rewards/RedeeemRewards/RedeemRewards';
import TapSection from '../../../../components/TapSection/TapSection';
import RatePlan from './RatePlan/RatePlan';
import analyticsService from '../../../../services/analytics/analyticsService';

class RoomType extends Component {
  state = {
    showBiggerRooms: (this.props.sortedAvailablePrices.findIndex((price) =>
      (price.id === this.props.selectedPrice.id)) > 0),
    ratePlanCode: this.props.selectedRatePlan.code,
    selectedRoomTypeId: this.props.selectedPrice.id,
    isModalOpen: qs.parse(this.props.location.search).modal === 'bigger-rooms',
  }

  componentWillReceiveProps(nextProps) {
    const { sortedAvailablePrices, selectedPrice, selectedRatePlan } = this.props;
    if (sortedAvailablePrices.length !== nextProps.sortedAvailablePrices.length
      || (selectedPrice.id !== nextProps.selectedPrice.id)
      || (selectedRatePlan.code !== nextProps.selectedRatePlan.code)
    ) {
      this.setState({
        showBiggerRooms: this.isSelectedRoomBig(nextProps.sortedAvailablePrices,
          nextProps.selectedPrice.id, nextProps.prices),
        ratePlanCode: nextProps.selectedRatePlan.code,
        selectedRoomTypeId: nextProps.selectedPrice.id,
      });
    }
  }

  onRatePlanChange = (selectedRoomTypeId) => (ratePlan) => {
    this.setState({
      ratePlanCode: ratePlan.code,
      selectedRoomTypeId,
    });
  }

  onSave = () => {
    const { ratePlanCode, selectedRoomTypeId } = this.state;
    const { changeRatePlan } = this.props;
    this.setState({
      isModalOpen: false,
    }, () => {
      changeRatePlan(selectedRoomTypeId, ratePlanCode);
    });
  }

  getAvailableRooms =
    (roomTypeIds, prices) =>
      roomTypeIds.filter((roomTypeId) =>
        (prices[roomTypeId].available));

  getRoomByType =
    (roomTypeIds, selectedRoomType) =>
      roomTypeIds.find((roomTypeId) =>
        (roomTypeId === selectedRoomType));

  getRatePlanDetails = (sortedAvailablePrices) => {
    const {
      ratePlanDetails,
      props: { selectedRatePlan },
    } = this;
    const isSingleRoomType = sortedAvailablePrices.length === 1;
    const isSingleRatePlan = isSingleRoomType
      && (Object.keys(sortedAvailablePrices[0].ratePlans).length === 1);
    if (!sortedAvailablePrices.length) {
      return ratePlanDetails.singleRefundableRatePlan;
    } else if (isSingleRatePlan) {
      const ratePlan = Object.keys(sortedAvailablePrices[0].ratePlans)[0];
      const roomPrice = sortedAvailablePrices[0].ratePlans[ratePlan];
      return roomPrice.type === 'non-refundable' ?
        ratePlanDetails.singleNonRefundableRatePlan : ratePlanDetails.singleRefundableRatePlan;
    }
    return selectedRatePlan.code === 'NRP'
      ? ratePlanDetails.nonRefundable : ratePlanDetails.refundable;
  }

  ratePlanDetails = {
    refundable: {
      seeOtherRooms: true,
      tag: 'Refundable',
      type: 'refundable',
      text: 'Free cancellation, 24 hours before Check-in',
      cta: 'View other rooms',
    },
    nonRefundable: {
      seeOtherRooms: true,
      tag: 'Non Refundable',
      type: 'non-refundable',
      text: 'Prepaid only. Pay at Hotel not available.',
      cta: 'View Refundable Plan',
    },
    singleNonRefundableRatePlan: {
      seeOtherRooms: false,
      tag: 'Non Refundable',
      type: 'non-refundable',
      text: 'Prepaid only. Pay at Hotel not available.',
      cta: 'No other plans available',
    },
    singleRefundableRatePlan: {
      seeOtherRooms: false,
      tag: 'Refundable',
      type: 'refundable',
      text: 'Free cancellation, 24 hours before Check-in',
      cta: 'No other plans available',
    },
  }

  isSelectedRoomBig =
    (sortedAvailablePrices, selectedRoomTypeId) =>
      (sortedAvailablePrices.findIndex((price) =>
        (price.id === selectedRoomTypeId)) > 0)

  toggleBiggerRoomsVisibility = () => {
    const { showBiggerRooms } = this.state;
    if (!showBiggerRooms) analyticsService.showBiggerRoomsClicked();
    this.setState({
      showBiggerRooms: !showBiggerRooms,
    });
  }

  showOtherRatePlans = (seeOtherRatePlans) => {
    const {
      match,
      selectedRatePlan,
    } = this.props;
    if (seeOtherRatePlans) {
      analyticsService.viewOtherRatePlansClicked(match.params.hotelId, selectedRatePlan);
    }
    this.setState({ isModalOpen: seeOtherRatePlans });
  }

  render() {
    const {
      isModalOpen,
      showBiggerRooms,
      ratePlanCode,
      selectedRoomTypeId,
    } = this.state;

    const {
      sortedAvailablePrices,
      selectedPrice,
      selectedRatePlan,
      prices,
      $roomType,
      hotel,
      isCoupleFriendly,
    } = this.props;

    const showBiggerRoomsPrompt = sortedAvailablePrices.length > 1;
    const ratePlanDetails = this.getRatePlanDetails(sortedAvailablePrices);

    return (
      ratePlanDetails.seeOtherRooms ? (
        <Space padding={[1.5, 2, 2.5, 2]} margin={[0, 0, 1, 0]}>
          <Card backgroundColor="white">
            <Flex flexDirection="row" flexWrap="wrap">
              <View>
                <Space margin={[1, 0, 0, 0]}>
                  <Text>Bigger Rooms Available. &nbsp;</Text>
                </Space>
                <Space margin={[1, 0, 0, 0]}>
                  <Text
                    color="blue"
                    weight="medium"
                    onClick={() => this.showOtherRatePlans(true)}
                  >
                    View Other Room Plans
                  </Text>
                </Space>
              </View>
            </Flex>
            <Modal
              isOpen={isModalOpen}
              closeIcon="close"
              label=""
              onClose={() => this.showOtherRatePlans(false)}
              header={<Text size="xl" weight="bold">Select Room</Text>}
            >
              <RedeemTreeboRewards />
              <Space padding={[2, 2, 13]}>
                <View>
                  {
                    sortedAvailablePrices.map((price, index) => (
                      <RatePlan
                        key={price.id}
                        title
                        roomType={$roomType.byId[price.id] || $roomType.byId['0|oak']}
                        selectedPrice={prices[price.id]}
                        onRatePlanChange={this.onRatePlanChange(price.id)}
                        isVisible={showBiggerRooms || (index === 0)}
                        isRoomSelected={
                            (selectedRoomTypeId
                              || selectedPrice.id) === price.id
                          }
                        selectedRatePlanCode={ratePlanCode || selectedRatePlan.code}
                        hotel={hotel}
                        $roomType={$roomType}
                        isCoupleFriendly={isCoupleFriendly}
                      />
                    ))
                  }
                  {
                    showBiggerRoomsPrompt ? (
                      <TapSection
                        title={`${showBiggerRooms ? 'Hide' : 'Show'} Bigger Rooms`}
                        onClick={this.toggleBiggerRoomsVisibility}
                        icon={showBiggerRooms ? 'up' : 'down'}
                      />
                    ) : null
                  }
                </View>
              </Space>
              <Position position="fixed" bottom="0">
                <Button
                  block
                  onClick={this.onSave}
                >Save
                </Button>
              </Position>
            </Modal>
          </Card>
        </Space>
      ) : null
    );
  }
}

RoomType.propTypes = {
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  selectedPrice: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
  sortedAvailablePrices: PropTypes.array.isRequired,
  prices: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  isCoupleFriendly: PropTypes.bool.isRequired,
  changeRatePlan: PropTypes.func.isRequired,
};

export default withRouter(RoomType);
