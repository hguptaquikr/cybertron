import React from 'react';
import PropTypes from 'prop-types';
import Image from 'leaf-ui/cjs/Image/web';
import Gallery from 'leaf-ui/cjs/Gallery/web';
import Space from 'leaf-ui/cjs/Space/web';
import Tag from 'leaf-ui/cjs/Tag/web';
import Size from 'leaf-ui/cjs/Size/web';
import Text from 'leaf-ui/cjs/Text/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';
import Divider from 'leaf-ui/cjs/Divider/web';
import isEmpty from 'lodash/isEmpty';
import RoomTypePrice from '../RoomTypePrice/RoomTypePrice';
import * as roomTypeService from '../../../../../services/roomType/roomTypeService';

const RatePlan = ({
  title,
  roomType,
  selectedPrice,
  onRatePlanChange,
  isVisible,
  isRoomSelected,
  selectedRatePlanCode,
  $roomType,
  hotel,
  isCoupleFriendly,
}) => {
  const roomTypeImage = roomTypeService.makeRoomTypeImages(hotel.images);
  const selectedRoomType = $roomType.byId[selectedPrice.id].type.toLowerCase();
  return isVisible ? (
    <React.Fragment>
      <Space padding={[2, 0]}>
        <View key={roomType.type}>
          {
            title ? (
              <Space margin={[0, 0, 1]}>
                <Text weight="bold" size="xl">{roomType.type.toUpperCase()}</Text>
              </Space>
            ) : null
          }
          <Flex flexDirection="row">
            <View>
              <Space margin={[0, 1, 0, 0]}>
                <Text tag="span" color="black">{roomType.area} Sqft</Text>
              </Space>
              |
              <Space margin={[0, 0, 0, 1]}>
                <Text tag="span" color="black">{roomType.maxOccupancy && roomType.maxOccupancy.occupancy}</Text>
              </Space>
            </View>
          </Flex>
          <Gallery>
            {
              isCoupleFriendly ? (
                <Size width="120px">
                  <Space margin={[2.5, 0, 0, 0]}>
                    <Tag
                      color="lagoon"
                      kind="outlined"
                      size="medium"
                    >
                      Couple Friendly
                    </Tag>
                  </Space>
                </Size>
              ) : null
            }
          </Gallery>
          {
            !isEmpty(roomTypeImage[selectedRoomType]) ? (
              <Space margin={[2.5, 0, 0, 0]}>
                <Gallery>
                  {
                    roomTypeImage[selectedRoomType].map((image, index) => (
                      <Image
                        key={`${image}${index + 1}`}
                        src={image.url}
                        alt={image.tagline}
                        width="128px"
                        height="80px"
                        shape="bluntEdged"
                      />
                    ))
                  }
                </Gallery>
              </Space>
            ) : null
          }
          <RoomTypePrice
            onRatePlanChange={onRatePlanChange}
            selectedPrice={selectedPrice}
            isRoomSelected={isRoomSelected}
            selectedRatePlanCode={selectedRatePlanCode}
          />
        </View>
      </Space>
      <Divider />
    </React.Fragment>
  ) : null;
};

RatePlan.propTypes = {
  title: PropTypes.bool.isRequired,
  onRatePlanChange: PropTypes.func.isRequired,
  roomType: PropTypes.object.isRequired,
  isVisible: PropTypes.bool.isRequired,
  isRoomSelected: PropTypes.bool.isRequired,
  selectedRatePlanCode: PropTypes.string,
  selectedPrice: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  isCoupleFriendly: PropTypes.bool.isRequired,
};

export default RatePlan;
