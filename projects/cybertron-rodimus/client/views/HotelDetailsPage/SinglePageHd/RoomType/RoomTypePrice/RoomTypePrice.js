import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Space from 'leaf-ui/cjs/Space/web';
import View from 'leaf-ui/cjs/View/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import Text from 'leaf-ui/cjs/Text/web';
import Price from '../../../../../components/Price/Price';
import RatePlanLabel from '../../../../../components/RatePlanLabel/RatePlanLabel';

const ratePlanText = {
  refundable: 'Zero cancellation fee within 24 hours of checkin.',
  'non-refundable': 'Prepaid only. This booking is not refundable.',
};

const RoomTypePrice = ({
  selectedPrice,
  onRatePlanChange,
  isRoomSelected,
  selectedRatePlanCode,
}) => (
  <View>
    {
      Object.values(selectedPrice.ratePlans).map((ratePlan, index) => (
        <Space margin={[3, 0, 0]} key={`roomTypePrice__${index + 1}`}>
          <Flex flexDirection="row">
            <View onClick={() => onRatePlanChange(ratePlan, selectedPrice)}>
              <Flex flex="1">
                <Text color="green">
                  <i className={cx('icon-radiobutton-outline', {
                    'icon-radiobutton': isRoomSelected && selectedRatePlanCode === ratePlan.code,
                  })}
                  />
                </Text>
              </Flex>
              <Flex flex="11">
                <View>
                  <Flex flexDirection="row">
                    <View>
                      <Space margin={[0, 1, 0, 0]}>
                        <Price
                          size="medium"
                          type="regular"
                          price={ratePlan.sellingPrice}
                          preview={!ratePlan.sellingPrice}
                          previewStyle={{ width: '80px' }}
                        />
                      </Space>
                      <RatePlanLabel
                        type={ratePlan.type}
                        tag={ratePlan.tag}
                      />
                    </View>
                  </Flex>
                  <Text component="p">{ratePlanText[ratePlan.type]}</Text>
                </View>
              </Flex>
            </View>
          </Flex>
        </Space>
      ))
    }
  </View>
);

RoomTypePrice.propTypes = {
  isRoomSelected: PropTypes.bool.isRequired,
  onRatePlanChange: PropTypes.func.isRequired,
  selectedPrice: PropTypes.object.isRequired,
  selectedRatePlanCode: PropTypes.string,
};

export default RoomTypePrice;
