import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Space from 'leaf-ui/cjs/Space/web';
import Text from 'leaf-ui/cjs/Text/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';
import Price from 'leaf-ui/cjs/Price/web';
import Divider from 'leaf-ui/cjs/Divider/web';
import Checkbox from 'leaf-ui/cjs/Checkbox/web';
import PriceBreakup from './PriceBreakup';
import RatePlanLabel from '../../../../components/RatePlanLabel/RatePlanLabel';

class PriceInfo extends Component {
  changeRatePlan= () => {
    const { selectedRatePlan, selectedPrice, changeRatePlan } = this.props;
    const ratePlanCode = selectedRatePlan.code === 'NRP' ? 'EP' : 'NRP';
    changeRatePlan(selectedPrice.id, ratePlanCode);
  }

  render() {
    const {
      hotel,
      selectedRoomType,
      selectedRatePlan,
      selectedPrice,
      coupon,
      treeboPoints,
      refreshBookingId,
      walletActions,
      checkoutUrl,
      userDetail,
      checkoutActions,
      numberOfNights,
      travellerInfoSubmit,
    } = this.props;

    return (
      <React.Fragment>
        <PriceBreakup
          hotel={hotel}
          treeboPoints={treeboPoints}
          refreshBookingId={refreshBookingId}
          walletActions={walletActions}
          selectedRatePlan={selectedRatePlan}
          selectedPrice={selectedPrice}
          selectedRoomType={selectedRoomType}
          coupon={coupon}
          checkoutUrl={checkoutUrl}
          userDetail={userDetail}
          checkoutActions={checkoutActions}
          numberOfNights={numberOfNights}
          travellerInfoSubmit={travellerInfoSubmit}
        />
        <Space padding={[1.5, 0, 0, 0]}>
          <Flex alignItems="baseline" flexDirection="row">
            <View>
              <Text size="xxl" weight="medium">
                {
                  selectedRatePlan.totalPrice
                  ? <Price>{selectedRatePlan.sellingPrice}</Price> : '---'
                }
              </Text>
              &nbsp;
              <RatePlanLabel
                type={selectedRatePlan.type}
                tag={selectedRatePlan.tag}
              />
            </View>
          </Flex>
        </Space>
        <Space padding={[0.5, 0, 0, 0]}>
          <Flex flexDirection="row">
            <View>
              <Text color="grey">Price inclusive of all taxes</Text>
            </View>
          </Flex>
        </Space>
        {
          selectedPrice.ratePlans.EP && selectedPrice.ratePlans.NRP ? (
            <React.Fragment>
              <Space margin={[2, 0, 0, 0]}>
                <Divider />
              </Space>
              <Flex flexDirection="row">
                <View>
                  <Space padding={[2, 0, 0, 0]}>
                    <Checkbox
                      label={`Refundable plan for an additional ₹ ${Math.round(selectedPrice.ratePlans.EP.sellingPrice) - Math.round(selectedPrice.ratePlans.NRP.sellingPrice)}`}
                      name="refundable"
                      checked={selectedRatePlan.code === 'EP'}
                      onChange={this.changeRatePlan}
                    />
                  </Space>
                </View>
              </Flex>
            </React.Fragment>
          ) : null
        }
      </React.Fragment>
    );
  }
}

PriceInfo.propTypes = {
  hotel: PropTypes.object.isRequired,
  selectedPrice: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
  selectedRoomType: PropTypes.object.isRequired,
  coupon: PropTypes.object.isRequired,
  checkoutUrl: PropTypes.object.isRequired,
  treeboPoints: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
  userDetail: PropTypes.object.isRequired,
  checkoutActions: PropTypes.object.isRequired,
  numberOfNights: PropTypes.object.isRequired,
  changeRatePlan: PropTypes.func.isRequired,
  refreshBookingId: PropTypes.func.isRequired,
  travellerInfoSubmit: PropTypes.func.isRequired,
};

export default withRouter(PriceInfo);
