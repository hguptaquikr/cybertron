import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Space from 'leaf-ui/cjs/Space/web';
import Position from 'leaf-ui/cjs/Position/web';
import Text from 'leaf-ui/cjs/Text/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';
import Card from 'leaf-ui/cjs/Card/web';
import Checkbox from 'leaf-ui/cjs/Checkbox/web';
import Price from 'leaf-ui/cjs/Price/web';
import Divider from 'leaf-ui/cjs/Divider/web';
import Button from 'leaf-ui/cjs/Button/web';
import Size from 'leaf-ui/cjs/Size/web';
import Modal from '../../../../components/Modal/Modal';
import TravellerInfo from '../TravellerInfo';
import RatePlanLabel from '../../../../components/RatePlanLabel/RatePlanLabel';
import analyticsService from '../../../../services/analytics/analyticsService';

class PriceBreakup extends Component {
  state = {
    showPriceBreakupModal: false,
    showTravellerInfo: false,
  }

  tooglePriceBreakUp = () => {
    this.setState((prevState) => ({
      showPriceBreakupModal: !prevState.showPriceBreakupModal,
    }));
  }

  toogleTravellerInfo = () => {
    this.setState((prevState) => ({
      showTravellerInfo: !prevState.showTravellerInfo,
    }));
  }

  toggleWalletUsage = (useTreeboPoints) => async () => {
    const { refreshBookingId, walletActions } = this.props;
    await refreshBookingId();
    if (useTreeboPoints) {
      await walletActions.applyWallet();
      analyticsService.walletUsage(true);
    } else {
      await walletActions.removeWallet();
      analyticsService.walletUsage(false);
    }
  }

  bookNow = () => {
    this.setState({
      showTravellerInfo: true,
    });
  }

  render() {
    const { showPriceBreakupModal, showTravellerInfo } = this.state;
    const {
      hotel,
      treeboPoints,
      selectedPrice,
      selectedRatePlan,
      selectedRoomType,
      coupon,
      checkoutUrl,
      userDetail,
      checkoutActions,
      numberOfNights,
      travellerInfoSubmit,
    } = this.props;

    return (
      <React.Fragment>
        <Flex flexDirection="row">
          <View>
            <Text>Total Payable for {numberOfNights.text}. &nbsp;</Text>
            <Text
              color="blue"
              component="span"
              onClick={this.tooglePriceBreakUp}
              weight="medium"
            >
              View Breakup
            </Text>
          </View>
        </Flex>
        <Modal
          closeIcon="close"
          isOpen={showTravellerInfo}
          onClose={this.toogleTravellerInfo}
        >
          <React.Fragment>
            <Space padding={[2.5, 2, 8, 2]}>
              <Card backgroundColor="white">
                <TravellerInfo
                  checkoutUrl={checkoutUrl}
                  travellerInfoSubmit={travellerInfoSubmit}
                  selectedRatePlan={selectedRatePlan}
                  id="travellerInfoSinglePageHdModal"
                  userDetail={userDetail}
                  checkoutActions={checkoutActions}
                  hotel={hotel}
                  selectedPrice={selectedPrice}
                  selectedRoomType={selectedRoomType}
                  coupon={coupon}
                />
              </Card>
            </Space>
            <Position position="fixed" bottom="0">
              <Size width="100%">
                <Space padding={[0, 1]}>
                  <View>
                    <Space padding={[0, 0, 1, 0]}>
                      <Card backgroundColor="white">
                        <Button
                          block
                          name="GSTINBOOK"
                          form="travellerInfoSinglePageHdModal"
                        >
                          BOOK NOW FOR&nbsp;
                          {
                            selectedRatePlan.totalPrice
                            ? <Price>{selectedRatePlan.sellingPrice}</Price> : '---'
                          }
                        </Button>
                      </Card>
                    </Space>
                  </View>
                </Space>
              </Size>
            </Position>
          </React.Fragment>
        </Modal>
        <Modal
          closeIcon="close"
          isOpen={showPriceBreakupModal}
          onClose={this.tooglePriceBreakUp}
        >
          <React.Fragment>
            <Space padding={[2.5, 2]} margin={[3, 0]}>
              <View>
                <Text size="l" weight="bold">Payment Breakup</Text>
                {
                  treeboPoints.usableBalance ? (
                    <Card backgroundColor="greyLighter">
                      <Space padding={[2.5]}>
                        <Checkbox
                          name="useTreeboPoints"
                          onChange={this.toggleWalletUsage(!treeboPoints.isUsingTreeboPoints)}
                          checked={treeboPoints.isUsingTreeboPoints}
                          label={`Pay ${treeboPoints.usableBalance} using Treebo Points`}
                        />
                      </Space>
                    </Card>
                  ) : null
                }
                <Space margin={[3, 0, 0, 0]}>
                  <Flex
                    justifyContent="space-between"
                    flexDirection="row"
                  >
                    <View>
                      <Text>
                        Room Tariff
                      </Text>
                      <Text>
                        <Price rounded={false}>
                          {selectedRatePlan.basePrice}
                        </Price>
                      </Text>
                    </View>
                  </Flex>
                </Space>
                {
                  selectedPrice.memberDiscount.value ? (
                    <Space margin={[3, 0, 0, 0]}>
                      <Flex
                        justifyContent="space-between"
                        flexDirection="row"
                      >
                        <View>
                          <Text>
                            Room Discount
                          </Text>
                          <Text color="green">-
                            <Price rounded={false}>
                              {selectedPrice.memberDiscount.value}
                            </Price>
                          </Text>
                        </View>
                      </Flex>
                    </Space>
                  ) : null
                }
                {
                  coupon.appliedCouponCode && selectedRatePlan.discountAmount ? (
                    <Space margin={[3, 0, 0, 0]}>
                      <Flex
                        justifyContent="space-between"
                        flexDirection="row"
                      >
                        <View>
                          <Text>
                            {`"${coupon.appliedCouponCode}" Applied`}
                          </Text>
                          <Text color="green">-
                            <Price rounded={false}>
                              {selectedRatePlan.discountAmount}
                            </Price>
                          </Text>
                        </View>
                      </Flex>
                    </Space>
                  ) : null
                }
                <Space margin={[3, 0, 3, 0]}>
                  <Flex
                    justifyContent="space-between"
                    flexDirection="row"
                  >
                    <View>
                      <Text>
                        Taxes
                      </Text>
                      <Text>
                        <Price rounded={false}>
                          {selectedRatePlan.tax}
                        </Price>
                      </Text>
                    </View>
                  </Flex>
                </Space>
                <Divider />
                <Space margin={[2, 0, 2, 0]}>
                  <Flex
                    justifyContent="space-between"
                    flexDirection="row"
                  >
                    <View>
                      <Text>
                        Total Tariff
                      </Text>
                      <Text>
                        <Price rounded={false}>
                          {selectedRatePlan.totalPrice}
                        </Price>
                      </Text>
                    </View>
                  </Flex>
                </Space>
                <Divider />
                {
                  treeboPoints.isUsingTreeboPoints ? (
                    <React.Fragment>
                      <Space margin={[2, 0, 2, 0]}>
                        <Flex
                          justifyContent="space-between"
                          flexDirection="row"
                        >
                          <View>
                            <Text>
                              Treebo Points Applied
                            </Text>
                            <Text color="green">-
                              <Price rounded={false}>
                                {treeboPoints.usableBalance}
                              </Price>
                            </Text>
                          </View>
                        </Flex>
                      </Space>
                      <Divider />
                    </React.Fragment>
                  ) : null
                }
                <Space margin={[2, 0, 0, 0]}>
                  <Flex
                    justifyContent="space-between"
                    alignItems="baseline"
                    flexDirection="row"
                  >
                    <View>
                      <Text>
                        Total Payable for {numberOfNights.text}
                      </Text>
                      <Text size="xxl" weight="medium">
                        <Price>
                          {selectedRatePlan.sellingPrice}
                        </Price>
                      </Text>
                    </View>
                  </Flex>
                </Space>
                <Space margin={[1, 0, 8, 0]}>
                  <Flex
                    justifyContent="space-between"
                    flexDirection="row"
                  >
                    <View>
                      <Text color="grey">
                        Tax Inclusive
                      </Text>
                      <RatePlanLabel
                        type={selectedRatePlan.type}
                        tag={selectedRatePlan.tag}
                      />
                    </View>
                  </Flex>
                </Space>
              </View>
            </Space>
            <Position position="fixed" bottom="0">
              <Size width="100%">
                <Space padding={[0, 1]}>
                  <View>
                    <Space padding={[0, 0, 1, 0]}>
                      <Card backgroundColor="white">
                        <Button block onClick={this.bookNow}>
                          BOOK NOW FOR&nbsp;
                          {
                            selectedRatePlan.totalPrice
                            ? <Price>{selectedRatePlan.sellingPrice}</Price> : '---'
                          }
                        </Button>
                      </Card>
                    </Space>
                  </View>
                </Space>
              </Size>
            </Position>
          </React.Fragment>
        </Modal>
      </React.Fragment>
    );
  }
}

PriceBreakup.propTypes = {
  hotel: PropTypes.object.isRequired,
  treeboPoints: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
  selectedPrice: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
  selectedRoomType: PropTypes.object.isRequired,
  coupon: PropTypes.object.isRequired,
  checkoutUrl: PropTypes.object.isRequired,
  refreshBookingId: PropTypes.func.isRequired,
  userDetail: PropTypes.object.isRequired,
  checkoutActions: PropTypes.object.isRequired,
  numberOfNights: PropTypes.object.isRequired,
  travellerInfoSubmit: PropTypes.func.isRequired,
};

export default PriceBreakup;
