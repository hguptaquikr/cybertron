import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Space from 'leaf-ui/cjs/Space/web';
import Text from 'leaf-ui/cjs/Text/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';
import Price from 'leaf-ui/cjs/Price/web';
import Coupons from '../../CheckoutPage/Coupons/Coupons';
import analyticsService from '../../../services/analytics/analyticsService';

class CouponInfo extends Component {
  applyCouponLinkClicked = async () => {
    const { couponActions, refreshBookingId } = this.props;
    analyticsService.applyCouponLinkClicked();
    await refreshBookingId();
    couponActions.showCouponModal(true);
  }

  removeCoupon = async () => {
    const { couponActions, refreshBookingId } = this.props;
    await refreshBookingId();
    couponActions.removeCoupon();
  }

  render() {
    const {
      coupon,
      couponActions,
      hotel,
      selectedRatePlan,
    } = this.props;
    return (
      <React.Fragment>
        {
          coupon.appliedCouponCode ? (
            <Flex flexDirection="row" alignItems="center">
              <View>
                <i className="icon-coupon_code" />
                <Space padding={[2, 0, 2, 2]}>
                  <Flex flexDirection="column">
                    <View>
                      <Flex flexDirection="row">
                        <View>
                          <Text color="greyDark" weight="medium" component="span">
                            {coupon.appliedCouponCode} Applied.&nbsp;
                          </Text>
                          <Text color="blue" weight="medium" onClick={this.removeCoupon}>Change Coupon</Text>
                        </View>
                      </Flex>
                      {
                        selectedRatePlan.discountAmount ? (
                          <Flex flexDirection="row">
                            <View>
                              <Space padding={[0.5, 0, 0, 0]}>
                                <Text color="grey" size="xs" component="span">
                                  Best coupon applied. You saved&nbsp;
                                  <Price>
                                    {selectedRatePlan.discountAmount}
                                  </Price>
                                </Text>
                              </Space>
                            </View>
                          </Flex>
                        ) : null
                      }
                    </View>
                  </Flex>
                </Space>
              </View>
            </Flex>
            ) : (
              <Flex flexDirection="row" alignItems="center">
                <View>
                  <i className="icon-coupon_code" />
                  <Space padding={[2, 0, 2, 2]}>
                    <Flex flexDirection="row">
                      <View>
                        <Text color="greyDark" weight="medium" component="span">
                          Have a coupon code? &nbsp;
                        </Text>
                        <Text color="blue" weight="medium" onClick={this.applyCouponLinkClicked}>Apply Coupon</Text>
                      </View>
                    </Flex>
                  </Space>
                </View>
              </Flex>
          )
        }
        <Coupons
          coupon={coupon}
          couponActions={couponActions}
          hotel={hotel}
        />
      </React.Fragment>
    );
  }
}

CouponInfo.propTypes = {
  hotel: PropTypes.object.isRequired,
  coupon: PropTypes.object.isRequired,
  couponActions: PropTypes.object.isRequired,
  refreshBookingId: PropTypes.func.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
};

export default CouponInfo;
