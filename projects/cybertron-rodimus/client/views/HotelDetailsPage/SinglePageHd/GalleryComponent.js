import React from 'react';
import styled from 'styled-components';

export const GalleryComponent = styled(
  ({
    component,
    children,
    opacity,
    ...props
  }) => React.cloneElement(children, props),
)`
  opacity: ${(props) => props.opacity};
`;
