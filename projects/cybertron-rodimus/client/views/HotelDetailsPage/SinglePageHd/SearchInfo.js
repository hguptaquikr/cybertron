import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Space from 'leaf-ui/cjs/Space/web';
import Text from 'leaf-ui/cjs/Text/web';
import Link from 'leaf-ui/cjs/Link/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';
import Icon from 'leaf-ui/cjs/Icon/web';
import Divider from 'leaf-ui/cjs/Divider/web';
import Modal from '../../../components/Modal/Modal';
import DatePicker from './../../SearchWidget/DatePicker/DatePicker';
import RoomConfig from './../../SearchWidget/RoomConfig/RoomConfig';
import searchService from '../../../services/search/searchService';

const SearchInfo = ({
  search,
  isDatePickerOpen,
  isRoomConfigOpen,
  toggleDatePickerModal,
  toggleRoomConfigModal,
  onChangeSearchConfig,
}) => {
  const { checkIn, checkOut } = searchService.formattedDatePicker(search.datePicker.range);
  const formattedRoomConfig = searchService.formattedRoomConfig(search.roomConfig.rooms);

  return (
    <React.Fragment>
      <Modal
        label="datePicker"
        isOpen={isDatePickerOpen}
        bodyClassName="sw__body"
        onClose={toggleDatePickerModal}
        closeIcon="close"
      >
        <div>
          <Text>Dates</Text>
          <DatePicker />
        </div>
      </Modal>
      <Modal
        label="datePicker"
        isOpen={isRoomConfigOpen}
        bodyClassName="sw__body"
        onClose={toggleRoomConfigModal}
        closeIcon="close"
      >
        <div>
          <Text>Dates</Text>
          <RoomConfig onSave={onChangeSearchConfig} />
        </div>
      </Modal>
      <Flex
        alignItems="center"
        flexDirection="row"
        onClick={toggleDatePickerModal}
      >
        <View>
          <Flex flex="1" flexDirection="row">
            <View>
              <Text>Dates</Text>
            </View>
          </Flex>
          <Flex flex="3" flexDirection="row">
            <Text weight="medium">
              <Link>{checkIn} - {checkOut}</Link>
            </Text>
          </Flex>
          <Flex flexDirection="row">
            <View>
              <Icon name="chevron_right" />
            </View>
          </Flex>
        </View>
      </Flex>
      <Space margin={[2.5, 0, 0, 0]}>
        <Divider />
      </Space>
      <Space padding={[2.5, 0, 0, 0]}>
        <Flex
          alignItems="center"
          onClick={toggleRoomConfigModal}
          flexDirection="row"
        >
          <View>
            <Flex flex="1" flexDirection="row">
              <View>
                <Text>Guests</Text>
              </View>
            </Flex>
            <Flex flex="3" flexDirection="row">
              <Text weight="medium">
                <Link>{formattedRoomConfig.text}</Link>
              </Text>
            </Flex>
            <Flex flexDirection="row">
              <View>
                <Icon name="chevron_right" />
              </View>
            </Flex>
          </View>
        </Flex>
      </Space>
    </React.Fragment>
  );
};

SearchInfo.propTypes = {
  search: PropTypes.object.isRequired,
  isDatePickerOpen: PropTypes.bool.isRequired,
  isRoomConfigOpen: PropTypes.bool.isRequired,
  toggleDatePickerModal: PropTypes.func.isRequired,
  toggleRoomConfigModal: PropTypes.func.isRequired,
  onChangeSearchConfig: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

export default connect(
  mapStateToProps,
)(SearchInfo);
