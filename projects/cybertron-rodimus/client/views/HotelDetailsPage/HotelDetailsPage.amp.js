import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import qs from 'query-string';
import isEmpty from 'lodash/isEmpty';
import Divider from 'leaf-ui/cjs/Divider/amp';
import Link from 'leaf-ui/cjs/Link/amp';
import Space from 'leaf-ui/cjs/Space/amp';
import Button from 'leaf-ui/cjs/Button/amp';
import Position from 'leaf-ui/cjs/Position/amp';
import View from 'leaf-ui/cjs/View/amp';
import Size from 'leaf-ui/cjs/Size/amp';
import NeedToKnow from '../../components/NeedToKnow/NeedToKnow.amp';
import PerfectStayBanner from '../../components/PerfectStayBanner/PerfectStayBanner.amp';
import SearchHeader from '../../components/SearchHeader/SearchHeader.amp';
import checkoutService from '../../services/checkout/checkoutService';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as hotelActionCreators from '../../services/hotel/hotelDuck';
import * as hotelService from '../../services/hotel/hotelService';
import * as priceActionCreators from '../../services/price/priceDuck';
import priceService from '../../services/price/priceService';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as toastActionCreators from '../../services/toast/toastDuck';
import * as uiActionCreators from '../../services/ui/uiDuck';
import * as reviewActionCreators from '../../services/review/reviewDuck';
import { makeRoomTypeId, omitKeys } from '../../services/utils';
import config from '../../../config';
import Hotel from './Hotel/Hotel.amp';
import MoreInfo from './MoreInfo/MoreInfo.amp';
import RoomType from './RoomType/RoomType.amp';
import ThingsGuestLove from './ThingsGuestLove/ThingsGuestLove.amp';
import Address from './Address/Address.amp';
import analyticsData from './HotelDetailsAnalytics.amp';

class HotelDetailsPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match, history, req }) => {
    const decodedHotelId = hotelService.decodeHotelId(match.params.hotelId);
    if (decodedHotelId) {
      return history.replace({
        pathname: history.location.pathname.replace(match.params.hotelId, decodedHotelId),
        search: history.location.search,
      });
    }
    const promises = [];
    dispatch(searchActionCreators.setSearchState({ ...req.query, ...match.params }));
    dispatch(uiActionCreators.setSearchParameters(qs.stringify(req.query)));
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(hotelActionCreators.getHotelDetails(match.params.hotelId)));
    promises.push(dispatch(priceActionCreators.getRoomPrices(match.params.hotelId)));
    promises.push(dispatch(reviewActionCreators.getHotelReviews(match.params.hotelId)));
    return Promise.all(promises);
  }

  static analytics = (match) => analyticsData(match);

  getAvailableRoomTypeIds = (roomTypeIds, prices) =>
    roomTypeIds.filter((roomTypeId) => (prices[roomTypeId] && prices[roomTypeId].available));

  getSelectedRoomTypeId = (prices, chepeastRoomTypeId) => {
    const { location, match: { params } } = this.props;
    const query = qs.parse(location.search);

    return query.roomtype
      && prices[makeRoomTypeId(params.hotelId, query.roomtype)]
      && prices[makeRoomTypeId(params.hotelId, query.roomtype)].available
      ? makeRoomTypeId(params.hotelId, query.roomtype) : chepeastRoomTypeId;
  }

  getSelectedRatePlan = (selectedPrice) => {
    const { location } = this.props;
    const query = qs.parse(location.search);

    return query.rateplan && selectedPrice.ratePlans[query.rateplan]
      ? selectedPrice.ratePlans[query.rateplan]
      : priceService.sortRatePlan(selectedPrice.ratePlans)[0];
  }

  renderHotel = () => {
    const {
      hotel,
      price,
      search,
      content: { seo },
      $roomType,
      location,
    } = this.props;
    const availableRoomTypeIds =
    this.getAvailableRoomTypeIds(hotel.roomTypeIds, price.byRoomTypeId);
    const hotelPrices = priceService.getPricesForRoomTypeIds(availableRoomTypeIds, price);
    const sortedAvailablePrices = priceService.sortPrice(hotelPrices);
    const selectedRoomTypeId = this.getSelectedRoomTypeId(price.byRoomTypeId, sortedAvailablePrices[0].id) || '0|oak';
    const selectedPrice = price.byRoomTypeId[selectedRoomTypeId];
    const selectedRatePlan = this.getSelectedRatePlan(selectedPrice) || {};
    const selectedRoomType = $roomType.byId[selectedRoomTypeId];

    const isAvailable = price.isPricesLoading || !isEmpty(availableRoomTypeIds);
    const checkoutLocation = isAvailable &&
    checkoutService.makeCheckoutLocation(search, selectedRoomType, hotel, selectedRatePlan);
    const checkoutUrl = `${config.hostUrl}${checkoutLocation.pathname}?${checkoutLocation.search}`;
    const searchModalUrl = `${config.hostUrl}${location.pathname.replace('amp/', '')}?modal=search`;
    const needToKnowPolicies = hotelService.getNeedToKnowPolicies(hotel.policies);

    return (
      <View>
        <Hotel
          hotel={hotel}
          price={selectedPrice}
          ratePlan={selectedRatePlan}
          isPricesLoading={price.isPricesLoading}
          isAvailable={isAvailable}
        />
        {
          isAvailable ? (
            <RoomType
              prices={price.byRoomTypeId}
              sortedAvailablePrices={sortedAvailablePrices}
              selectedPrice={selectedPrice}
              selectedRatePlan={selectedRatePlan}
              checkoutUrl={checkoutUrl}
              $roomType={$roomType}
              selectedRoomType={selectedRoomType}
              search={search}
            />
          ) : null
        }
        <Divider />
        {
          !isEmpty(needToKnowPolicies) ? (
            <NeedToKnow needToKnowPolicies={needToKnowPolicies} />
          ) : null
        }
        <Divider />
        <Address
          coordinates={hotel.coordinates}
          address={hotel.address}
        />
        <Divider />
        {
          !isEmpty(seo.content.guestFeedbacks) ? (
            <ThingsGuestLove feedbacks={seo.content.guestFeedbacks} />
          ) : null
        }
        <Divider />
        <MoreInfo
          hotel={hotel}
          aboutHotel={seo.content.aboutHotel}
          seoLinks={seo.content.links}
          facilities={seo.content.facilities}
          policies={hotel.policies}
        />
        {
          isAvailable ? (
            <Position position="fixed" bottom="0" zIndex="20">
              <Size width="100%">
                <Link href={checkoutUrl} id="t-ampBook">
                  <Button
                    block
                    size="large"
                    shape="sharpEdged"
                  >
                    REVIEW DATES AND BOOK
                  </Button>
                </Link>
              </Size>
            </Position>
          ) : (
            <Position position="fixed" bottom="0" zIndex="20">
              <Size width="100%">
                <Link href={searchModalUrl} id="t-ampBook">
                  <Button
                    block
                    size="large"
                    shape="sharpEdged"
                  >
                    CHECK DIFFERENT DATES
                  </Button>
                </Link>
              </Size>
            </Position>
          )
        }
      </View>
    );
  }

  render() {
    return (
      <Fragment>
        <SearchHeader />
        <Space margin={[2]}>
          <View>
            <PerfectStayBanner />
          </View>
        </Space>
        {this.renderHotel()}
      </Fragment>
    );
  }
}

HotelDetailsPage.propTypes = {
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
};

const mapStateToProps = (state, { match }) => ({
  hotel: state.hotel.byId[match.params.hotelId] || state.hotel.byId[0],
  isHotelDetailsLoading: state.hotel.isHotelDetailsLoading,
  price: state.price,
  isPricesLoading: state.price.isPricesLoading,
  $roomType: state.$roomType,
  search: state.search,
  content: state.content,
  nearbyHotels: omitKeys(state.hotel.byId, ['0', match.params.hotelId]),
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  priceActions: bindActionCreators(priceActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
  uiActions: bindActionCreators(uiActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HotelDetailsPage);
