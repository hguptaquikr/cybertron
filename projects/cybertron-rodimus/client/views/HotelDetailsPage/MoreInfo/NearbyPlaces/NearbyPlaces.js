import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ShowMore from '../../../../components/ShowMore/ShowMore';
import { Text } from '../../../../components/Typography/Typography';
import { Row, Col } from '../../../../components/Flex/Flex';
import { pluralize, scrollIntoView } from '../../../../services/utils';
import './nearbyPlaces.css';

class NearbyPlaces extends Component {
  onShowLess = () => {
    if (this.nearbyPlacesRef) {
      scrollIntoView(this.nearbyPlacesRef, -52);
    }
  }

  storeNearbyPlacesRef = (ref) => {
    if (ref) this.nearbyPlacesRef = ref;
  }

  render() {
    const { nearbyPlaces } = this.props;
    return (
      <div ref={this.storeNearbyPlacesRef}>
        <ShowMore
          initialHeight={250}
          showMoreText="Read more"
          showLessText="Read less"
          onShowLess={this.onShowLess}
        >
          <div className="nearby-places">
            {
              nearbyPlaces.content.map(({ title, items }, index) => (
                <div key={`${title}${index + 1}`}>
                  <Text type="2" className="nearby-places__title">
                    {title}
                  </Text>
                  {
                    items.map(({ name, distance }) => (
                      <Row className="nearby-places__description" middle key={`${name}${distance}${index + 1}`}>
                        <Col size="8"><Text type="2">{name}</Text></Col>
                        <Col size="4"><Text type="2">{`${distance} ${pluralize(distance, 'Km')}`}</Text></Col>
                      </Row>
                    ))
                  }
                </div>
              ))
            }
          </div>
        </ShowMore>
      </div>
    );
  }
}

NearbyPlaces.propTypes = {
  nearbyPlaces: PropTypes.object.isRequired,
};

export default NearbyPlaces;
