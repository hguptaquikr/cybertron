import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import Accordion from '../../../components/Accordion/Accordion';
import { Text } from '../../../components/Typography/Typography';
import QuestionAndAnswer from '../../../views/QuestionAndAnswer/QuestionAndAnswer';
import AboutHotel from './AboutHotel/AboutHotel';
import Policies from './Policies/Policies';
import Facilities from './Facilities/Facilities';
import NearbyPlaces from './NearbyPlaces/NearbyPlaces';
import './moreInfo.css';

const MoreInfo = ({
  hotel,
  aboutHotel,
  facilities,
  policies,
  nearbyPlaces,
  qna,
  seoLinks,
}) => (
  <Accordion.Container>
    {
      !isEmpty(aboutHotel) ? (
        <Accordion.Section>
          <Accordion.Title>
            <Text className="more-info__section-title truncate">
              {aboutHotel.title}
            </Text>
          </Accordion.Title>
          <Accordion.Content>
            <AboutHotel
              hotelName={hotel.name}
              aboutHotel={aboutHotel}
              seoLinks={seoLinks}
            />
          </Accordion.Content>
        </Accordion.Section>
      ) : null
    }
    {
      !isEmpty(facilities) ? (
        <Accordion.Section>
          <Accordion.Title>
            <Text className="more-info__section-title truncate">
              {facilities.title}
            </Text>
          </Accordion.Title>
          <Accordion.Content>
            <Facilities facilities={facilities.content} />
          </Accordion.Content>
        </Accordion.Section>
      ) : null
    }
    {
      !isEmpty(policies) ? (
        <Accordion.Section>
          <Accordion.Title>
            <Text className="more-info__section-title truncate">
              POLICIES
            </Text>
          </Accordion.Title>
          <Accordion.Content>
            <Policies policies={policies} />
          </Accordion.Content>
        </Accordion.Section>
      ) : null
    }
    {
      !isEmpty(nearbyPlaces) ? (
        <Accordion.Section>
          <Accordion.Title>
            <Text className="more-info__section-title truncate">
              {nearbyPlaces.title}
            </Text>
          </Accordion.Title>
          <Accordion.Content>
            <NearbyPlaces nearbyPlaces={nearbyPlaces} />
          </Accordion.Content>
        </Accordion.Section>
      ) : null
    }
    {
      !isEmpty(qna) ? (
        <Accordion.Section>
          <Accordion.Title>
            <Text className="more-info__section-title truncate">
              {qna.title}
            </Text>
          </Accordion.Title>
          <Accordion.Content>
            <QuestionAndAnswer qna={qna.content} />
          </Accordion.Content>
        </Accordion.Section>
      ) : null
    }
  </Accordion.Container>
);

MoreInfo.propTypes = {
  aboutHotel: PropTypes.object,
  facilities: PropTypes.object,
  policies: PropTypes.array,
  nearbyPlaces: PropTypes.object,
  qna: PropTypes.object,
  hotel: PropTypes.object.isRequired,
  seoLinks: PropTypes.array,
};

export default MoreInfo;
