import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import Accordion from 'leaf-ui/cjs/Accordion/amp';
import Flex from 'leaf-ui/cjs/Flex/amp';
import Space from 'leaf-ui/cjs/Space/amp';
import Text from 'leaf-ui/cjs/Text/amp';
import View from 'leaf-ui/cjs/View/amp';
import Icon from 'leaf-ui/cjs/Icon/amp';
import Divider from 'leaf-ui/cjs/Divider/amp';
import AboutHotel from './AboutHotel/AboutHotel.amp';
import Policies from './Policies/Policies.amp';
import Facilities from './Facilities/Facilities.amp';

const MoreInfo = ({ hotel, aboutHotel, facilities, policies }) => (
  <Space margin={[0, 0, 6, 0]}>
    <View>
      <Accordion>
        {
          !isEmpty(aboutHotel) ? (
            <Accordion.Section>
              <Accordion.Section.Trigger id="t-ampAboutAccordion">
                <Space padding={[2]}>
                  <Flex
                    flexDirection="row"
                    alignItems="center"
                    justifyContent="space-between"
                  >
                    <View>
                      <Text
                        size="s"
                        weight="medium"
                        color="greyDarker"
                      >
                        {aboutHotel.title.toUpperCase()}
                      </Text>
                      <Icon name="keyboard_arrow_down" right />
                    </View>
                  </Flex>
                </Space>
              </Accordion.Section.Trigger>
              <Accordion.Section.Content>
                <Space padding={[2]}>
                  <View>
                    <AboutHotel
                      hotelName={hotel.name}
                      aboutHotel={aboutHotel}
                    />
                  </View>
                </Space>
              </Accordion.Section.Content>
              <Divider />
            </Accordion.Section>
          ) : null
        }
        {
          !isEmpty(facilities) ? (
            <Accordion.Section>
              <Accordion.Section.Trigger id="t-ampFacilitiesAccordion">
                <Space padding={[2]}>
                  <View>
                    <Flex
                      flexDirection="row"
                      alignItems="center"
                      justifyContent="space-between"
                    >
                      <View>
                        <Text
                          size="s"
                          weight="medium"
                          color="greyDarker"
                        >
                          {facilities.title.toUpperCase()}
                        </Text>
                        <Icon name="keyboard_arrow_down" right />
                      </View>
                    </Flex>
                  </View>
                </Space>
              </Accordion.Section.Trigger>
              <Accordion.Section.Content>
                <Space padding={[0, 2, 2, 2]}>
                  <View>
                    <Facilities facilities={facilities.content} />
                  </View>
                </Space>
              </Accordion.Section.Content>
              <Divider />
            </Accordion.Section>
          ) : null
        }
        {
          !isEmpty(policies) ? (
            <Accordion.Section>
              <Accordion.Section.Trigger id="t-ampPoliciesAccordion">
                <Space padding={[2]}>
                  <View>
                    <Flex
                      flexDirection="row"
                      alignItems="center"
                      justifyContent="space-between"
                    >
                      <View>
                        <Text size="s" weight="medium" color="greyDarker">
                          POLICIES
                        </Text>
                        <Icon name="keyboard_arrow_down" right />
                      </View>
                    </Flex>
                  </View>
                </Space>
              </Accordion.Section.Trigger>
              <Accordion.Section.Content>
                <Space padding={[2]}>
                  <View>
                    <Policies policies={policies} />
                  </View>
                </Space>
              </Accordion.Section.Content>
              <Divider />
            </Accordion.Section>
          ) : null
        }
      </Accordion>
    </View>
  </Space>
);

MoreInfo.propTypes = {
  aboutHotel: PropTypes.object,
  facilities: PropTypes.object,
  policies: PropTypes.array,
  hotel: PropTypes.object.isRequired,
};

export default MoreInfo;
