import React, { Component } from 'react';
import PropTypes from 'prop-types';
import kebabCase from 'lodash/kebabCase';
import ShowMore from '../../../../components/ShowMore/ShowMore';
import { Text } from '../../../../components/Typography/Typography';
import { scrollIntoView } from '../../../../services/utils';
import './facilities.css';

class Facilities extends Component {
  onShowLess = () => {
    if (this.facilitiesRef) {
      scrollIntoView(this.facilitiesRef, -52);
    }
  }

  storeFacilitiesRef = (ref) => {
    if (ref) this.facilitiesRef = ref;
  }

  render() {
    const { facilities } = this.props;
    return (
      <div ref={this.storeFacilitiesRef}>
        <ShowMore
          initialHeight={300}
          showMoreText="Read more"
          showLessText="Read less"
          onShowLess={this.onShowLess}
        >
          <div className="facilities">
            {
              facilities.map((facility, index) => (
                <div key={`${facility.title}${index + 1}`}>
                  <Text type="3" className="facilities__title">
                    {facility.title}
                  </Text>
                  {facility.items.map((item) => (
                    <Text type="3" className="facilities__description" key={`${item.name}${index + 1}`}>
                      <img
                        className="old-image facilities__default-icon"
                        src={`https://images.treebohotels.com/images/facilities/${kebabCase(item.name)}.svg`}
                        alt=""
                      />
                      <span>
                        {item.name}
                      </span>
                    </Text>
                  ))}
                </div>
              ))
            }
          </div>
        </ShowMore>
      </div>
    );
  }
}

Facilities.propTypes = {
  facilities: PropTypes.array.isRequired,
};

export default Facilities;
