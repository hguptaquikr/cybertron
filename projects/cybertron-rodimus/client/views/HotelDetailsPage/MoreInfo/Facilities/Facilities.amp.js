import React from 'react';
import PropTypes from 'prop-types';
import kebabCase from 'lodash/kebabCase';
import Flex from 'leaf-ui/cjs/Flex/amp';
import Space from 'leaf-ui/cjs/Space/amp';
import View from 'leaf-ui/cjs/View/amp';
import Text from 'leaf-ui/cjs/Text/amp';
import Image from 'leaf-ui/cjs/Image/amp';
import MoreOrLess from 'leaf-ui/cjs/MoreOrLess/web';

const Facilities = (props) => {
  const { facilities } = props;
  return (
    <MoreOrLess
      initialHeight="206px"
      labelForMore="Read More"
      labelForLess="Read Less"
    >
      {
        facilities.map((facility) => (
          <View key={facility.title}>
            <Space
              margin={[2, 0]}
              padding={[1, 0, 0, 0]}
            >
              <View>
                <Text
                  size="s"
                  weight="medium"
                  color="greyDarker"
                >
                  {facility.title}
                </Text>
              </View>
            </Space>
            <Space margin={[0, 0, 2, 0]}>
              <View>
                {
                  facility.items.map((item) => (
                    <Space
                      key={facility.title}
                      margin={[2, 0, 1, 0]}
                    >
                      <Flex
                        flexDirection="row"
                        alignItems="center"
                      >
                        <View>
                          <Space
                            width="24px"
                            height="24px"
                            padding={[0.5]}
                            margin={[0, 1, 0, 0]}
                          >
                            <Image
                              src={`https://images.treebohotels.com/images/facilities/${kebabCase(item.name)}.svg`}
                              alt={kebabCase(item.name)}
                              width="20"
                              height="20"
                            />
                          </Space>
                          <Space margin={[0, 0, 0, 2]}>
                            <Text
                              size="s"
                              color="greyDark"
                              weight="normal"
                            >
                              {item.name}
                            </Text>
                          </Space>
                        </View>
                      </Flex>
                    </Space>
                  ))
                }
              </View>
            </Space>
          </View>
        ))
      }
    </MoreOrLess>
  );
};

Facilities.propTypes = {
  facilities: PropTypes.array.isRequired,
};

export default Facilities;
