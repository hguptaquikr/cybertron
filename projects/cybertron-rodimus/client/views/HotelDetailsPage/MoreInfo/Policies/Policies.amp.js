import React from 'react';
import PropTypes from 'prop-types';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import View from 'leaf-ui/cjs/View/web';
import MoreOrLess from 'leaf-ui/cjs/MoreOrLess/web';

const Policies = (props) => {
  const { policies } = props;
  return (
    <MoreOrLess
      initialHeight="180px"
      labelForMore="Read More"
      labelForLess="Read Less"
    >
      {
        policies.map((policy) => (
          <Space
            key={policy.title}
            margin={[0, 0, 3, 0]}
          >
            <View>
              <Space margin={[0, 0, 1, 0]}>
                <Text
                  size="s"
                  weight="medium"
                  color="greyDarker"
                >
                  {policy.title}
                </Text>
              </Space>
              <Text
                size="s"
                weight="normal"
                color="greyDark"
                component="p"
              >
                {policy.description}
              </Text>
            </View>
          </Space>
          ))
        }
    </MoreOrLess>
  );
};

Policies.propTypes = {
  policies: PropTypes.array.isRequired,
};

export default Policies;
