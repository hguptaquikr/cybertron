import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ShowMore from '../../../../components/ShowMore/ShowMore';
import { scrollIntoView } from '../../../../services/utils';
import { Text } from '../../../../components/Typography/Typography';

import './policies.css';

class Policies extends Component {
  onShowLess = () => {
    if (this.policiesRef) {
      scrollIntoView(this.policiesRef, -52);
    }
  }

  storePoliciesRef = (ref) => {
    if (ref) this.policiesRef = ref;
  }

  render() {
    const { policies } = this.props;
    return (
      <div ref={this.storePoliciesRef}>
        <ShowMore
          initialHeight={300}
          showMoreText="Read more"
          showLessText="Read less"
          onShowLess={this.onShowLess}
        >
          <div className="policies">
            {
              policies.map((policy) => (
                <div className="policies__list-item" key={policy.title}>
                  <Text type="2" className="policies__title">{policy.title}</Text>
                  <Text type="2" className="policies__description">{policy.description}</Text>
                </div>
              ))
            }
          </div>
        </ShowMore>
      </div>
    );
  }
}

Policies.propTypes = {
  policies: PropTypes.array.isRequired,
};

export default Policies;
