import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import MoreOrLess from 'leaf-ui/cjs/MoreOrLess/web';

const AboutHotel = (props) => {
  const { hotelName, aboutHotel } = props;

  return (
    <Fragment>
      <Text
        size="s"
        weight="medium"
        color="greyDarker"
      >
        About {hotelName}
      </Text>
      <Space margin={[2, 0, 1, 0]}>
        <div>
          <MoreOrLess
            initialHeight="130px"
            labelForMore="Read More"
            labelForLess="Read Less"
          >
            <Text
              size="s"
              weight="normal"
              color="greyDark"
              component="p"
            >
              {aboutHotel.content.description.about}
            </Text>
          </MoreOrLess>
        </div>
      </Space>
    </Fragment>
  );
};

AboutHotel.propTypes = {
  aboutHotel: PropTypes.object.isRequired,
  hotelName: PropTypes.string.isRequired,
};

export default AboutHotel;
