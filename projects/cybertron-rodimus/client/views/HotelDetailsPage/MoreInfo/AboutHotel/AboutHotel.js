import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { Link } from 'react-router-dom';
import { Heading, Text, Subheading } from '../../../../components/Typography/Typography';
import ShowMore from '../../../../components/ShowMore/ShowMore';
import { Row, Col } from '../../../../components/Flex/Flex';
import { scrollIntoView } from '../../../../services/utils';
import contentService from '../../../../services/content/contentService';
import './aboutHotel.css';

const TopReasons = ({ reasons }) => (
  <div className="ah__section">
    <Heading type="4" tag="p" className="ah__title">{reasons.title}</Heading>
    {
      reasons.items.map((item, index) => (
        <Row key={`${item}${index + 1}`} className="text-2 positive-feedback__title" start>
          <Col size="1"><i className="icon-angle-right positive-feedback__icon" /></Col>
          <Col size="11" className="ah__text">{item}</Col>
        </Row>
      ))
    }
  </div>
);

TopReasons.propTypes = {
  reasons: PropTypes.object.isRequired,
};

class AboutHotel extends Component {
  onShowLess = () => {
    if (this.aboutHotelRef) {
      scrollIntoView(this.aboutHotelRef, -52);
    }
  }

  getSeoLinksContent = (category) => {
    const { seoLinks } = this.props;
    return seoLinks.find((link) => link.key === category).content;
  }

  getNearbyData = () => ({
    localities: this.getSeoLinksContent('near_by_localities').slice(0, 2),
    landmarks: this.getSeoLinksContent('near_by_landmarks').slice(0, 2),
    budgetHotel: this.getSeoLinksContent('category')
      .find((categoryData) => categoryData.category === 'Budget Hotels'),
    serviceApartment: this.getSeoLinksContent('category')
      .find((categoryData) => categoryData.category === 'Service Apartments'),
    coupleFriendly: this.getSeoLinksContent('category')
      .find((categoryData) => categoryData.category === 'Couple Friendly Hotels'),
  });

  storeAboutHotelRef = (ref) => {
    if (ref) this.aboutHotelRef = ref;
  }

  render() {
    const { hotelName, aboutHotel } = this.props;
    const nearbyData = this.getNearbyData();

    return (
      <div ref={this.storeAboutHotelRef}>
        <ShowMore
          initialHeight={180}
          showMoreText="Read more"
          showLessText="Read less"
          onShowLess={this.onShowLess}
        >
          <div className="ah">
            {
              !isEmpty(aboutHotel.content.reasons) ? (
                <TopReasons reasons={aboutHotel.content.reasons} />
              ) : null
            }
            <div className="ah__section">
              <Heading type="4" tag="p" className="ah__title">About {hotelName}</Heading>
              <Text className="ah__text" dangerouslySetInnerHTML={{ __html: aboutHotel.content.description.about }} />
              {
                nearbyData.localities.length ? (
                  <Subheading type="2" className="ah__nearby-title">Nearby Localities</Subheading>
                ) : null
              }
              {
                nearbyData.localities.map((locality) => (
                  <Link
                    key={locality.area.locality}
                    className="text--link ah__nearby-link"
                    to={contentService.getSeoPathName(locality)}
                  >
                    Hotels in {locality.area.locality}
                  </Link>
                ))
              }
              {
                nearbyData.landmarks.length ? (
                  <Subheading type="2" className="ah__nearby-title">Nearby Landmarks</Subheading>
                ) : null
              }
              {
                nearbyData.landmarks.map((landMark) => (
                  <Link
                    key={landMark.area.landmark}
                    className="text--link ah__nearby-link"
                    to={contentService.getSeoPathName(landMark)}
                  >
                    Hotels near {landMark.area.landmark}
                  </Link>
                ))
              }
              {
                nearbyData.budgetHotel || nearbyData.serviceApartment
                  || nearbyData.coupleFriendly ? (
                    <Subheading type="2" className="ah__nearby-title">Hotel Types</Subheading>
                ) : null
              }
              {
                nearbyData.budgetHotel ? (
                  <Link
                    className="text--link ah__nearby-link"
                    to={contentService.getSeoPathName(nearbyData.budgetHotel)}
                  >
                    Budget Hotels in {nearbyData.budgetHotel.area.city}
                  </Link>
                ) : null
              }
              {
                nearbyData.serviceApartment ? (
                  <Link
                    className="text--link ah__nearby-link"
                    to={contentService.getSeoPathName(nearbyData.serviceApartment)}
                  >
                    Service Apartments in {nearbyData.serviceApartment.area.city}
                  </Link>
                ) : null
              }
              {
                nearbyData.coupleFriendly ? (
                  <Link
                    className="text--link ah__nearby-link"
                    to={contentService.getSeoPathName(nearbyData.coupleFriendly)}
                  >
                    Couple Friendly Hotels in {nearbyData.coupleFriendly.area.city}
                  </Link>
                ) : null
              }
            </div>
          </div>
        </ShowMore>
      </div>
    );
  }
}

AboutHotel.propTypes = {
  aboutHotel: PropTypes.object.isRequired,
  hotelName: PropTypes.string.isRequired,
  seoLinks: PropTypes.array,
};

export default AboutHotel;
