import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./SinglePageHd' /* webpackChunkName: 'SinglePageHd' */),
  loading: () => null,
});
