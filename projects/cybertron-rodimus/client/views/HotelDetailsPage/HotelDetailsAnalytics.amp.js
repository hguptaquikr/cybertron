const analyticsData = ({ match }) => ({
  pageEventName: 'Amp Hotel Details Page Viewed',
  properties: {
    hotelId: match.params.hotelId,
    hotelName: match.params.hotelSlug,
    pageType: 'AMP',
    page: 'HD',
    nonInteraction: 1,
  },
  events: [
    {
      selector: '#t-ampHome',
      name: 'Home Button Clicked',
      properties: {
        nonInteraction: 0,
      },
    },
    {
      selector: '#t-ampSearchHeader',
      name: 'Search Header Date Clicked',
      properties: {
        nonInteraction: 0,
      },
    },
    {
      selector: '#t-ampSearchHeaderIcon',
      name: 'Search Header Icon Clicked',
      properties: {
        nonInteraction: 0,
      },
    },
    {
      selector: '#t-ampPerfectStayBanner',
      name: 'Perfect Stay Banner Expanded',
      properties: {
        nonInteraction: 0,
      },
    },
    {
      selector: '#t-ampPerfectStayKnowMore',
      name: 'Perfect Stay Know More Clicked',
      properties: {
        nonInteraction: 0,
      },
    },
    {
      selector: '#t-ampViewOtherRooms',
      name: 'View Other Rooms Clicked',
      properties: {
        nonInteraction: 0,
      },
    },
    {
      selector: '#t-ampNeedToKnowShowMore',
      name: 'NeedToKnow Show More Clicked',
      properties: {
        type: 'needToKnow',
        nonInteraction: 0,
      },
    },
    {
      selector: '#t-ampViewMap',
      name: 'View In Maps Clicks',
    },
    {
      selector: '#t-ampAboutAccordion',
      name: 'About Accordion Expanded',
      properties: {
        type: 'about',
        nonInteraction: 0,
      },
    },
    {
      selector: '#t-ampFacilitiesAccordion',
      name: 'Facilities Accordion Expanded',
      properties: {
        type: 'facilities',
        nonInteraction: 0,
      },
    },
    {
      selector: '#t-ampPoliciesAccordion',
      name: 'Policies Accordion Expanded',
      properties: {
        type: 'policies',
        nonInteraction: 0,
      },
    },
    {
      selector: '#t-ampAboutReadMore',
      name: 'About Read More Clicked',
      properties: {
        type: 'about',
        nonInteraction: 0,
      },
    },
    {
      selector: '#t-ampFacilitiesReadMore',
      name: 'Facilities Read More Clicked',
      properties: {
        type: 'facilities',
        nonInteraction: 0,
      },
    },
    {
      selector: '#t-ampPoliciesReadMore',
      name: 'Policies Read More Clicked',
      properties: {
        type: 'policies',
        nonInteraction: 0,
      },
    },
    {
      selector: '#t-ampBook',
      name: 'Book Now Clicked',
      properties: {
        nonInteraction: 0,
      },
    },
  ],
});

export default analyticsData;
