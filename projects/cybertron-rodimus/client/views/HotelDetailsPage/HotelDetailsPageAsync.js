import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('HotelDetailsPage');
    return import('./HotelDetailsPage' /* webpackChunkName: 'HotelDetailsPage' */);
  },
  loading: () => null,
});
