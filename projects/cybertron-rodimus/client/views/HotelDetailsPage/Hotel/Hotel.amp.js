import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import Gallery from 'leaf-ui/cjs/Gallery/amp';
import Space from 'leaf-ui/cjs/Space/amp';
import Text from 'leaf-ui/cjs/Text/amp';
import Flex from 'leaf-ui/cjs/Flex/amp';
import Image from 'leaf-ui/cjs/Image/amp';
import View from 'leaf-ui/cjs/View/amp';
import Tag from 'leaf-ui/cjs/Tag/amp';
import * as hotelService from '../../../services/hotel/hotelService';
import searchService from '../../../services/search/searchService';

const formatPrice = (price) => Math.round(price).toLocaleString('en-IN');

const Hotel = ({
  hotel,
  ratePlan,
  isAvailable,
  review,
  search: { datePicker },
}) => {
  const { numberOfNights } = searchService.formattedDatePicker(datePicker.range, 'D MMM');
  const isCoupleFriendly = hotelService.isCoupleFriendly(hotel.policies);
  return (
    <View>
      <Space padding={[0, 2, 2, 2]}>
        <View>
          <Gallery
            width="400"
            height="300"
            type="slides"
          >
            {
              hotel.images.map((image) => (
                <Image
                  key={image.url}
                  src={image.url}
                  alt={image.tagline}
                  width="400"
                  height="300"
                  grayscale={!isAvailable}
                />
              ))
            }
          </Gallery>
        </View>
      </Space>
      <Flex
        flexDirection="row"
        justifyContent="space-between"
      >
        <View>
          <View>
            {
              isAvailable ? (
                <Space padding={[0, 2]}>
                  <Flex
                    flexDirection="row"
                    alignItems="flex-end"
                  >
                    <View>
                      <Text size="xxl" color="black" weight="semibold">
                        ₹{formatPrice(ratePlan.sellingPrice)}
                      </Text>
                      <Space padding={[0.3]}>
                        <Text size="xs" color="greyDark">
                          &nbsp;Tax incl. price for {numberOfNights.text}
                        </Text>
                      </Space>
                    </View>
                  </Flex>
                </Space>
              ) : (
                <Space padding={[2]}>
                  <Text color="red" weight="medium">
                    SOLD OUT
                  </Text>
                </Space>
              )
            }
          </View>
          <View>
            {
              !isEmpty(review.results[hotel.id])
                && review.results[hotel.id].isTaEnabled
                && review.results[hotel.id].overallRating.rating ? (
                  <Space padding={[1, 2]}>
                    <Flex
                      flexDirection="row"
                      alignItems="center"
                      itemScope
                      itemType="http://schema.org/AggregateRating"
                    >
                      <View>
                        <meta itemProp="ratingValue" content={review.results[hotel.id].overallRating.rating} />
                        <meta itemProp="ratingCount" content={review.results[hotel.id].overallRating.ratingCount} />
                        <meta itemProp="bestRating" content="5" />
                        <Image
                          src="https://images.treebohotels.com/images/ta_reviews.svg"
                          alt="TA"
                          width="20"
                          height="20"
                        />
                        <Space margin={[0, 0, 0, 0.5]}>
                          <Text component="span">{review.results[hotel.id].overallRating.rating}/5</Text>
                        </Space>
                      </View>
                    </Flex>
                  </Space>
              ) : null
            }
          </View>
        </View>
      </Flex>
      {
        isCoupleFriendly ? (
          <Space padding={[2]}>
            <Flex
              className="hotel__cf"
              flexDirection="row"
            >
              <View>
                <Tag
                  color="primary"
                  shape="capsular"
                  kind="outlined"
                >
                  Couple Friendly
                </Tag>
              </View>
            </Flex>
          </Space>
        ) : null
      }
    </View>
  );
};

Hotel.propTypes = {
  hotel: PropTypes.object.isRequired,
  ratePlan: PropTypes.object.isRequired,
  isAvailable: PropTypes.bool.isRequired,
  search: PropTypes.object.isRequired,
  review: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
  review: state.review,
});

export default connect(
  mapStateToProps,
)(Hotel);
