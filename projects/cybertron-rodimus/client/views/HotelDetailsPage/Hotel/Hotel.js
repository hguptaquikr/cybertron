import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import { Subheading, Text } from '../../../components/Typography/Typography';
import { Row, Col } from '../../../components/Flex/Flex';
import SlideShow from '../../../components/SlideShow/SlideShow';
import Price from '../../../components/Price/Price';
import CoupleFriendlyTag from '../../../components/CoupleFriendlyTag/CoupleFriendlyTag';
import * as hotelService from '../../../services/hotel/hotelService';
import searchService from '../../../services/search/searchService';
import './hotel.css';

const Hotel = ({
  hotel,
  ratePlan,
  isAvailable,
  review,
  search: { datePicker },
}) => {
  const { numberOfNights } = searchService.formattedDatePicker(datePicker.range, 'D MMM');
  const isCoupleFriendly = hotel.policies && hotelService.isCoupleFriendly(hotel.policies);
  return (
    <div className="hotel">
      <SlideShow
        images={hotel.images}
        grayscale={!isAvailable}
        tagField="roomType"
      />
      <Row>
        <Col size="9">
          {
            isAvailable ? (
              <div>
                <Row baseline className="hotel__price">
                  <Price
                    bold
                    size="x-large"
                    price={ratePlan.sellingPrice}
                    className="hotel__selling-price"
                    preview={!ratePlan.sellingPrice}
                    previewStyle={{ width: '72px' }}
                  />
                  <Text
                    type="2"
                    className="hotel__incl-taxes"
                    preview={!ratePlan.totalPrice}
                    previewStyle={{ width: '42%' }}
                  >
                    Tax incl. price for {numberOfNights.text}
                  </Text>
                </Row>
                {
                  ratePlan.discountPercentage > 0 ? (
                    <div>
                      <Price
                        size="regular"
                        type="striked"
                        className="hotel__striked-price hide"
                        price={ratePlan.strikedPrice}
                        preview={!ratePlan.strikedPrice}
                        previewStyle={{ width: '80px' }}
                      />
                      <Text tag="span" className="hotel__discount-percentage hide" >
                        {Math.round(ratePlan.discountPercentage)}% off
                      </Text>
                    </div>
                  ) : null
                }
              </div>
            ) : (
              <Subheading tag="p" className="hotel__soldout text--error">
                SOLD OUT
              </Subheading>
            )
          }
        </Col>
        {
          !isEmpty(review.results[hotel.id])
            && review.results[hotel.id].isTaEnabled
            && review.results[hotel.id].overallRating.rating ? (
              <Col size="3">
                <Row
                  middle
                  end
                  className="hotel__review"
                  itemProp="aggregateRating"
                  itemScope
                  itemType="http://schema.org/AggregateRating"
                >
                  <meta itemProp="ratingValue" content={review.results[hotel.id].overallRating.rating} />
                  <meta itemProp="ratingCount" content={review.results[hotel.id].overallRating.ratingCount} />
                  <meta itemProp="bestRating" content="5" />
                  <img
                    src="https://images.treebohotels.com/images/ta_reviews.svg"
                    className="old-image hotel__review__image"
                    alt="TA"
                  />
                  <Subheading tag="div">
                    <span>{review.results[hotel.id].overallRating.rating}/5</span>
                  </Subheading>
                </Row>
              </Col>
            ) : null
        }
      </Row>
      {
        isCoupleFriendly ? (
          <Row className="hotel__cf">
            <CoupleFriendlyTag isCoupleFriendly={isCoupleFriendly} />
          </Row>
        ) : null
      }

    </div>
  );
};

Hotel.propTypes = {
  hotel: PropTypes.object.isRequired,
  ratePlan: PropTypes.object.isRequired,
  isAvailable: PropTypes.bool.isRequired,
  search: PropTypes.object.isRequired,
  review: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
  review: state.review,
});

export default connect(
  mapStateToProps,
)(Hotel);
