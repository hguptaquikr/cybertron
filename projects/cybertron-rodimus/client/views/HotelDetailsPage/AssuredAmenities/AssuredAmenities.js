import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Row, Col } from '../../../components/Flex/Flex';
import { Heading, Text } from '../../../components/Typography/Typography';
import './assuredAmenities.css';

const AssuredAmenities = ({ amenities }) => (
  <div className="aa">
    <Heading tag="h3" type="3" className="aa__title">Free Assured Amenities</Heading>
    <Row middle>
      {
        amenities.map((amenity) => (
          <Col key={amenity.name} className="text-center">
            <i className={cx('aa__icon', `icon-${amenity.iconClass}`)} />
            <Text className="aa__name">{amenity.name}</Text>
          </Col>
        ))
      }
    </Row>
  </div>
);

AssuredAmenities.propTypes = {
  amenities: PropTypes.array.isRequired,
};

export default AssuredAmenities;
