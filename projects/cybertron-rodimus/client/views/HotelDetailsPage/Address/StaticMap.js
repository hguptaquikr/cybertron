import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Text } from '../../../components/Typography/Typography';
import config from '../../../../config';
import * as hotelService from '../../../services/hotel/hotelService';
import androidService from '../../../services/android/androidService';
import analyticsService from '../../../services/analytics/analyticsService';
import { getMapsUrl } from '../../../services/utils';
import './staticMap.css';

const StaticMap = ({
  coordinates,
  address,
  width,
  height,
  zoom,
  scale,
  match,
}) => {
  const lat = coordinates ? coordinates.lat : '';
  const lng = coordinates ? coordinates.lng : '';
  const imgSrc = [
    `https://maps.googleapis.com/maps/api/staticmap?zoom=${zoom}`,
    `key=${config.googleMapsApiKey}`,
    `size=${width}x${height}`,
    `scale=${scale}`,
    `markers=size:small|${lat},${lng}`,
    'format=png',
  ].join('&');

  return (lat && lng) ? (
    <div className="sm" onClick={() => analyticsService.viewLocationClicked(match.params.hotelId)}>
      <a
        className="sm__link"
        href={getMapsUrl(lat, lng)}
        rel={androidService.isAndroid ? '' : 'noopener noreferrer'}
        target="_blank"
      >
        <img className="old-image sm__image" src={imgSrc} alt="Hotel Address Map" />
        <Text className="sm__address">{hotelService.constructAddress(address)}</Text>
      </a>
    </div>
  ) : null;
};

StaticMap.propTypes = {
  coordinates: PropTypes.object.isRequired,
  address: PropTypes.object.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  zoom: PropTypes.number,
  scale: PropTypes.number,
  match: PropTypes.object.isRequired,
};

StaticMap.defaultProps = {
  zoom: 13,
  scale: 1,
};

export default withRouter(StaticMap);
