import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Link from 'leaf-ui/cjs/Link/amp';
import Space from 'leaf-ui/cjs/Space/amp';
import Text from 'leaf-ui/cjs/Text/amp';
import Flex from 'leaf-ui/cjs/Flex/amp';
import Icon from 'leaf-ui/cjs/Icon/amp';
import View from 'leaf-ui/cjs/View/amp';
import * as hotelService from '../../../services/hotel/hotelService';
import androidService from '../../../services/android/androidService';
import { getMapsUrl } from '../../../services/utils';

const Address = ({
  coordinates: { lat, lng },
  address,
}) => (
  <Space padding={[1, 2, 3, 2]}>
    <Flex>
      <View>
        <Space margin={[2, 0]}>
          <Text size="m" color="black" weight="medium">
          Address
          </Text>
        </Space>
        <Text
          size="s"
          color="greyDarker"
          weight="normal"
          component="p"
        >
          {hotelService.constructAddress(address)}
        </Text>
        {
          lat && lng ? (
            <Space margin={[2, 0, 1, 0]}>
              <View>
                <Text size="s" weight="medium">
                  <Flex
                    flexDirection="row"
                    alignItems="center"
                  >
                    <View>
                      <Link
                        id="t-ampViewMap"
                        href={getMapsUrl(lat, lng)}
                        rel={androidService.isAndroid ? '' : 'noopener noreferrer'}
                        target="_blank"
                      >
                        View in Maps
                      </Link>
                      <Icon name="chevron_right" color="blue" />
                    </View>
                  </Flex>
                </Text>
              </View>
            </Space>
          ) : null
        }
      </View>
    </Flex>
  </Space>
);

Address.propTypes = {
  coordinates: PropTypes.object.isRequired,
  address: PropTypes.object.isRequired,
};

export default withRouter(Address);
