import React from 'react';
import get from 'lodash/get';
import * as abService from '../../services/ab/abService';
import HotelDetailsPageAsync from './HotelDetailsPageAsync';
import SinglePageHdAsync from './SinglePageHdAsync';

const withAB = ({
  experiment,
  variants,
  dafaultVariant,
}) =>
  class Variant extends React.Component {
    static preload = () => {
      const chosenVariant = variants.find((variant) =>
        variant.name === (get(abService.getExperiments(), `${experiment}.variant`) || dafaultVariant));

      if (chosenVariant.Component.preload) {
        return chosenVariant.Component.preload();
      }
      return Promise.resolve({
        default: chosenVariant.Component,
      });
    }

    render() {
      const { props } = this;
      const chosenVariant = variants.find((variant) =>
        variant.name === (get(abService.getExperiments(), `${experiment}.variant`) || dafaultVariant));
      return (
        <chosenVariant.Component {...props} />
      );
    }
  };

export default withAB({
  experiment: 'pwaSinglePageHd',
  dafaultVariant: 'old',
  variants: [{
    name: 'new',
    Component: SinglePageHdAsync,
  }, {
    name: 'old',
    Component: HotelDetailsPageAsync,
  }],
});
