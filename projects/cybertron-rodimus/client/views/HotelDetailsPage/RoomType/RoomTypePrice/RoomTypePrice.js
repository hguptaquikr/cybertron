import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Text } from '../../../../components/Typography/Typography';
import { Row, Col } from '../../../../components/Flex/Flex';
import Price from '../../../../components/Price/Price';
import RatePlanLabel from '../../../../components/RatePlanLabel/RatePlanLabel';
import './roomTypePrice.css';

const ratePlanText = {
  refundable: 'Zero cancellation fee within 24 hours of checkin.',
  'non-refundable': 'Prepaid only. This booking is not refundable.',
};

const RoomTypePrice = ({
  selectedPrice,
  onChange,
  isRoomSelected,
  selectedRatePlanCode,
}) => (
  <div>
    {
      Object.values(selectedPrice.ratePlans).map((ratePlan, index) => (
        <Row className="rp" onClick={() => onChange(ratePlan, selectedPrice)} key={`roomTypePrice__${index + 1}`}>
          <Col size="1" className="rp__sp">
            <i className={cx('icon-radiobutton-outline', {
              'icon-radiobutton': isRoomSelected && selectedRatePlanCode === ratePlan.code,
            })}
            />
          </Col>
          <Col size="11">
            <Row className="rp__rate-plan">
              <Price
                size="medium"
                type="regular"
                className="rp__rate-plan__price"
                price={ratePlan.sellingPrice}
                preview={!ratePlan.sellingPrice}
                previewStyle={{ width: '80px' }}
              />
              {
                ratePlan.discountPercentage ? (
                  <div>
                    <Price
                      size="medium"
                      type="striked"
                      className="rp__rate-plan__price hide"
                      price={ratePlan.strikedPrice}
                      preview={!ratePlan.strikedPrice}
                      previewStyle={{ width: '80px' }}
                    />
                    <Text
                      className="hide"
                      modifier="primary"
                      tag="span"
                    >
                      {Math.round(ratePlan.discountPercentage)}% off
                    </Text>
                  </div>
                ) : null
              }
              <RatePlanLabel
                className="rp__rate-plan__tag"
                type={ratePlan.type}
                tag={ratePlan.tag}
              />
            </Row>
            <Text className="rp__rate-plan__text">{ratePlanText[ratePlan.type]}</Text>
          </Col>
        </Row>
      ))
    }
  </div>
);

RoomTypePrice.propTypes = {
  isRoomSelected: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  selectedPrice: PropTypes.object.isRequired,
  selectedRatePlanCode: PropTypes.string,
};

export default RoomTypePrice;
