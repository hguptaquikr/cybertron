import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Row } from '../../../../components/Flex/Flex';
import { Heading, Text } from '../../../../components/Typography/Typography';
// import SpecialDealsTag from '../../../../components/SpecialDealsTag/SpecialDealsTag';
import RoomTypePrice from '../RoomTypePrice/RoomTypePrice';

const RatePlan = ({
  title,
  roomType,
  selectedPrice,
  onRatePlanChange,
  isVisible,
  isRoomSelected,
  selectedRatePlanCode,
}) => (
  <div
    className={cx('rt__room', {
      'rt__room--hide': !isVisible,
    })}
    key={roomType.type}
  >
    <Row middle className={cx('rt__room__title-container', { hide: !title })}>
      <Heading type="2" className="rt__room__title">{roomType.type}</Heading>
      {/*
        room.memberDiscount && room.memberDiscount.isApplied ? (
          <SpecialDealsTag />
        ) : null
      */}
    </Row>
    <div className="rt__room__details">
      <Text tag="span" className="rt__room__details__size">{roomType.area} Sqft</Text>
      |
      <Text tag="span" className="rt__room__details__occupancy">{roomType.maxOccupancy && roomType.maxOccupancy.occupancy}</Text>
    </div>
    <RoomTypePrice
      onChange={onRatePlanChange}
      selectedPrice={selectedPrice}
      isRoomSelected={isRoomSelected}
      selectedRatePlanCode={selectedRatePlanCode}
    />
  </div>
);

RatePlan.propTypes = {
  title: PropTypes.bool.isRequired,
  onRatePlanChange: PropTypes.func.isRequired,
  roomType: PropTypes.object.isRequired,
  isVisible: PropTypes.bool.isRequired,
  isRoomSelected: PropTypes.bool.isRequired,
  selectedRatePlanCode: PropTypes.string.isRequired,
  selectedPrice: PropTypes.object.isRequired,
};

export default RatePlan;
