import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import qs from 'query-string';
import moment from 'moment';
import Button from '../../../components/Button/Button';
import Modal from '../../../components/Modal/Modal';
import RedeemTreeboRewards from '../../../components/Rewards/RedeeemRewards/RedeemRewards';
import { Subheading, Heading } from '../../../components/Typography/Typography';
import TapSection from '../../../components/TapSection/TapSection';
import RatePlan from './RatePlan/RatePlan';
import analyticsService from '../../../services/analytics/analyticsService';
import './roomType.css';

class RoomType extends Component {
  state = {
    showBiggerRooms: (this.props.sortedAvailablePrices.findIndex((price) =>
      (price.id === this.props.selectedPrice.id)) > 0),
    ratePlanCode: this.props.selectedRatePlan.code,
    selectedRoomTypeId: this.props.selectedPrice.id,
    isModalOpen: false || qs.parse(this.props.location.search).modal === 'bigger-rooms',
  }

  componentWillReceiveProps(nextProps) {
    const { sortedAvailablePrices, selectedPrice, selectedRatePlan } = this.props;
    if (sortedAvailablePrices.length !== nextProps.sortedAvailablePrices.length
      || (selectedPrice.id !== nextProps.selectedPrice.id)
      || (selectedRatePlan.code !== nextProps.selectedRatePlan.code)
    ) {
      this.setState({
        showBiggerRooms: this.isSelectedRoomBig(nextProps.sortedAvailablePrices,
          nextProps.selectedPrice.id, nextProps.prices),
        ratePlanCode: nextProps.selectedRatePlan.code,
        selectedRoomTypeId: nextProps.selectedPrice.id,
      });
    }
  }

  onRatePlanChange = (selectedRoomTypeId) => (ratePlan) => {
    this.setState({
      ratePlanCode: ratePlan.code,
      selectedRoomTypeId,
    });
  }

  onSave = () => {
    const { ratePlanCode, selectedRoomTypeId } = this.state;
    this.changeRatePlan(selectedRoomTypeId, ratePlanCode);
  }

  getAvailableRooms =
    (roomTypeIds, prices) =>
      roomTypeIds.filter((roomTypeId) =>
        (prices[roomTypeId].available));

  getRoomByType =
    (roomTypeIds, selectedRoomType) =>
      roomTypeIds.find((roomTypeId) =>
        (roomTypeId === selectedRoomType));

  getRatePlanDetails = (sortedAvailablePrices) => {
    const {
      ratePlanDetails,
      props: { selectedRatePlan },
    } = this;
    const isSingleRoomType = sortedAvailablePrices.length === 1;
    const isSingleRatePlan = isSingleRoomType
      && (Object.keys(sortedAvailablePrices[0].ratePlans).length === 1);
    if (!sortedAvailablePrices.length) {
      return ratePlanDetails.singleRefundableRatePlan;
    } else if (isSingleRatePlan) {
      const ratePlan = Object.keys(sortedAvailablePrices[0].ratePlans)[0];
      const roomPrice = sortedAvailablePrices[0].ratePlans[ratePlan];
      return roomPrice.type === 'non-refundable' ?
        ratePlanDetails.singleNonRefundableRatePlan : ratePlanDetails.singleRefundableRatePlan;
    }
    return selectedRatePlan.code === 'NRP'
      ? ratePlanDetails.nonRefundable : ratePlanDetails.refundable;
  }

  ratePlanDetails = {
    refundable: {
      seeOtherRooms: true,
      tag: 'Refundable',
      type: 'refundable',
      text: 'Free cancellation, 24 hours before Check-in',
      cta: 'View other rooms',
    },
    nonRefundable: {
      seeOtherRooms: true,
      tag: 'Non Refundable',
      type: 'non-refundable',
      text: 'Prepaid only. Pay at Hotel not available.',
      cta: 'View Refundable Plan',
    },
    singleNonRefundableRatePlan: {
      seeOtherRooms: false,
      tag: 'Non Refundable',
      type: 'non-refundable',
      text: 'Prepaid only. Pay at Hotel not available.',
      cta: 'No other plans available',
    },
    singleRefundableRatePlan: {
      seeOtherRooms: false,
      tag: 'Refundable',
      type: 'refundable',
      text: 'Free cancellation, 24 hours before Check-in',
      cta: 'No other plans available',
    },
  }

  changeRatePlan = (selectedRoomTypeId, ratePlanCode) => {
    const {
      search: {
        datePicker: {
          range,
        },
      },
      selectedRatePlan,
      selectedRoomType,
      prices,
      match,
      $roomType,
    } = this.props;
    const oldRatePlan = selectedRatePlan;
    const newRatePlan = prices[selectedRoomTypeId].ratePlans[ratePlanCode];

    const oldRoomType = selectedRoomType;
    const newRoomType = $roomType.byId && $roomType.byId[selectedRoomTypeId];
    const abwDuration = typeof range.start === 'object'
      && range.start.startOf('day').diff(moment().startOf('day'), 'days');
    this.setState({
      isModalOpen: false,
    }, () => {
      setTimeout(() => {
        const { history, location } = this.props;
        history.push({
          pathname: location.pathname,
          search: qs.stringify({
            ...qs.parse(location.search),
            roomtype: $roomType.byId && newRoomType.type,
            rateplan: ratePlanCode,
          }),
        });
      }, 50);
    });
    if (oldRoomType.type !== newRoomType || oldRatePlan.code !== newRatePlan.code) {
      analyticsService.ratePlanChanged({
        hotelId: match.params.hotelId,
        oldRatePlan,
        newRatePlan,
        oldRoomType,
        newRoomType,
        abwDuration,
        page: 'Hotel Details Page',
      });
    }
  }

  isSelectedRoomBig =
    (sortedAvailablePrices, selectedRoomTypeId) =>
      (sortedAvailablePrices.findIndex((price) =>
        (price.id === selectedRoomTypeId)) > 0)

  toggleBiggerRoomsVisibility = () => {
    const { showBiggerRooms } = this.state;
    if (!showBiggerRooms) analyticsService.showBiggerRoomsClicked();
    this.setState({
      showBiggerRooms: !showBiggerRooms,
    });
  }

  toggleVisibility = (seeOtherRatePlans) => {
    const {
      match,
      selectedRatePlan,
    } = this.props;
    if (seeOtherRatePlans) {
      analyticsService.viewOtherRatePlansClicked(match.params.hotelId, selectedRatePlan);
    }
    this.setState({ isModalOpen: seeOtherRatePlans });
  }

  render() {
    const {
      isModalOpen,
      showBiggerRooms,
      ratePlanCode,
      selectedRoomTypeId,
    } = this.state;

    const {
      sortedAvailablePrices,
      selectedPrice,
      selectedRatePlan,
      prices,
      $roomType,
    } = this.props;

    const showBiggerRoomsPrompt = sortedAvailablePrices.length > 1;
    const ratePlanDetails = this.getRatePlanDetails(sortedAvailablePrices);

    return (
      <div className="rt">
        <div
          className="rt__sop"
          onClick={() => ratePlanDetails.seeOtherRooms && this.toggleVisibility(true)}
        >
          <Subheading tag="div" className="rt__sop__tag">{ratePlanDetails.tag}</Subheading>
          <Subheading tag="span">{ratePlanDetails.text}</Subheading>
          {
            ratePlanDetails.seeOtherRooms ? (
              <Subheading tag="div" className="rt__sop__cta">
                { ratePlanDetails.cta } <i className="icon-angle-right" />
              </Subheading>
            ) : null
          }
        </div>
        {
          isModalOpen ? (
            <Modal
              isOpen
              closeIcon="close"
              label=""
              onClose={() => this.toggleVisibility(false)}
              header={<Heading>Select Room</Heading>}
            >
              <RedeemTreeboRewards />
              <div className="rt__cr">
                {
                  sortedAvailablePrices.map((price, index) => (
                    <RatePlan
                      key={price.id}
                      title
                      roomType={$roomType.byId[price.id]}
                      selectedPrice={prices[price.id]}
                      onRatePlanChange={this.onRatePlanChange(price.id)}
                      isVisible={showBiggerRooms || (index === 0)}
                      isRoomSelected={
                        (selectedRoomTypeId
                          || selectedPrice.id) === price.id
                      }
                      selectedRatePlanCode={ratePlanCode || selectedRatePlan.code}
                    />
                  ))
                }
                {
                  showBiggerRoomsPrompt ? (
                    <TapSection
                      className="rt__sh-rooms"
                      title={`${showBiggerRooms ? 'Hide' : 'Show'} Bigger Rooms`}
                      onClick={this.toggleBiggerRoomsVisibility}
                      icon={showBiggerRooms ? 'up' : 'down'}
                    />
                  ) : null
                }
              </div>
              <Button
                flat
                large
                block
                className="floaty"
                onClick={() => this.onSave()}
              >Save
              </Button>
            </Modal>
          ) : null
        }
      </div>
    );
  }
}

RoomType.propTypes = {
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  selectedPrice: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
  sortedAvailablePrices: PropTypes.array.isRequired,
  prices: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  selectedRoomType: PropTypes.object.isRequired,
};

export default withRouter(RoomType);
