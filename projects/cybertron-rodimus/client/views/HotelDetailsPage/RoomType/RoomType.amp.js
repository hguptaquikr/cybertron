import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Space from 'leaf-ui/cjs/Space/amp';
import Text from 'leaf-ui/cjs/Text/amp';
import Flex from 'leaf-ui/cjs/Flex/amp';
import Link from 'leaf-ui/cjs/Link/amp';
import Icon from 'leaf-ui/cjs/Icon/amp';
import Card from 'leaf-ui/cjs/Card/amp';
import View from 'leaf-ui/cjs/View/amp';
import analyticsService from '../../../services/analytics/analyticsService';
import config from '../../../../config';

class RoomType extends Component {
  state = {
    showBiggerRooms: (this.props.sortedAvailablePrices.findIndex((price) =>
      (price.id === this.props.selectedPrice.id)) > 0),
  }

  componentWillReceiveProps(nextProps) {
    const { sortedAvailablePrices, selectedPrice, selectedRatePlan } = this.props;
    if (sortedAvailablePrices.length !== nextProps.sortedAvailablePrices.length
      || (selectedPrice.id !== nextProps.selectedPrice.id)
      || (selectedRatePlan.code !== nextProps.selectedRatePlan.code)
    ) {
      this.setState({
        showBiggerRooms: this.isSelectedRoomBig(nextProps.sortedAvailablePrices,
          nextProps.selectedPrice.id, nextProps.prices),
      });
    }
  }

  getAvailableRooms =
    (roomTypeIds, prices) =>
      roomTypeIds.filter((roomTypeId) =>
        (prices[roomTypeId].available));

  getRoomByType =
    (roomTypeIds, selectedRoomType) =>
      roomTypeIds.find((roomTypeId) =>
        (roomTypeId === selectedRoomType));

  getRatePlanDetails = (sortedAvailablePrices) => {
    const {
      ratePlanDetails,
      props: { selectedRatePlan },
    } = this;
    const isSingleRoomType = sortedAvailablePrices.length === 1;
    const isSingleRatePlan = isSingleRoomType
      && (Object.keys(sortedAvailablePrices[0].ratePlans).length === 1);
    if (!sortedAvailablePrices.length) {
      return ratePlanDetails.singleRefundableRatePlan;
    } else if (isSingleRatePlan) {
      const ratePlan = Object.keys(sortedAvailablePrices[0].ratePlans)[0];
      const roomPrice = sortedAvailablePrices[0].ratePlans[ratePlan];
      return roomPrice.type === 'non-refundable' ?
        ratePlanDetails.singleNonRefundableRatePlan : ratePlanDetails.singleRefundableRatePlan;
    }
    return selectedRatePlan.code === 'NRP'
      ? ratePlanDetails.nonRefundable : ratePlanDetails.refundable;
  }

  ratePlanDetails = {
    refundable: {
      seeOtherRooms: true,
      tag: 'Refundable',
      type: 'refundable',
      text: 'Free cancellation, 24 hours before Check-in',
      cta: 'View other rooms',
    },
    nonRefundable: {
      seeOtherRooms: true,
      tag: 'Non Refundable',
      type: 'non-refundable',
      text: 'Prepaid only. Pay at Hotel not available.',
      cta: 'View Refundable Plan',
    },
    singleNonRefundableRatePlan: {
      seeOtherRooms: false,
      tag: 'Non Refundable',
      type: 'non-refundable',
      text: 'Prepaid only. Pay at Hotel not available.',
      cta: 'No other plans available',
    },
    singleRefundableRatePlan: {
      seeOtherRooms: false,
      tag: 'Refundable',
      type: 'refundable',
      text: 'Free cancellation, 24 hours before Check-in',
      cta: 'No other plans available',
    },
  }

  isSelectedRoomBig =
    (sortedAvailablePrices, selectedRoomTypeId) =>
      (sortedAvailablePrices.findIndex((price) =>
        (price.id === selectedRoomTypeId)) > 0)

  toggleBiggerRoomsVisibility = () => {
    const { showBiggerRooms } = this.state;
    if (!showBiggerRooms) analyticsService.showBiggerRoomsClicked();
    this.setState({
      showBiggerRooms: !showBiggerRooms,
    });
  }

  render() {
    const {
      sortedAvailablePrices,
      location,
    } = this.props;
    const hdUrl = `${config.hostUrl}${location.pathname.replace('amp/', '')}?modal=bigger-rooms`;

    const ratePlanDetails = this.getRatePlanDetails(sortedAvailablePrices);

    return (
      <Space margin={[2]} padding={[2]}>
        <Card backgroundColor="greyLighter">
          <Flex>
            <View>
              <Text size="s" color="greyDarker" weight="normal">
                {ratePlanDetails.tag}
              </Text>
              <Space margin={[1, 0, 0, 0]}>
                <Text size="s" color="greyDarker" weight="normal">
                  {ratePlanDetails.text}
                </Text>
              </Space>
              {
                ratePlanDetails.seeOtherRooms ? (
                  <Space margin={[2, 0, 1, 0]}>
                    <Link>
                      <Flex
                        flexDirection="row"
                        alignItems="center"
                      >
                        <View>
                          <Text size="s" weight="medium">
                            <Link
                              id="t-ampViewOtherRooms"
                              href={hdUrl}
                            >
                              {ratePlanDetails.cta}
                            </Link>
                          </Text>
                          <Icon name="chevron_right" color="blue" />
                        </View>
                      </Flex>
                    </Link>
                  </Space>
                ) : null
              }
            </View>
          </Flex>
        </Card>
      </Space>
    );
  }
}

RoomType.propTypes = {
  selectedPrice: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
  sortedAvailablePrices: PropTypes.array.isRequired,
  prices: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

export default withRouter(RoomType);
