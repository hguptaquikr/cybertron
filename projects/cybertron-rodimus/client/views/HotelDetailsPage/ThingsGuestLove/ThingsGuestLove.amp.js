import React from 'react';
import PropTypes from 'prop-types';
import kebabCase from 'lodash/kebabCase';
import Image from 'leaf-ui/cjs/Image/amp';
import Space from 'leaf-ui/cjs/Space/amp';
import Size from 'leaf-ui/cjs/Size/amp';
import Text from 'leaf-ui/cjs/Text/amp';
import Flex from 'leaf-ui/cjs/Flex/amp';
import View from 'leaf-ui/cjs/View/amp';

const ThingsGuestLove = ({ feedbacks }) => (
  <Space padding={[1, 2, 0, 2]}>
    <Flex>
      <View>
        <Space margin={[2, 0]}>
          <Text
            size="m"
            color="black"
            weight="medium"
          >
            {feedbacks.title}
          </Text>
        </Space>
        {
        feedbacks.items.map(({ title, description }) => (
          <Space margin={[0, 0, 3, 0]} key={title}>
            <Flex flexDirection="row">
              <View>
                <Space padding={[0.5]}>
                  <Size
                    width="24px"
                    height="24px"
                  >
                    <Image
                      src={`https://images.treebohotels.com/images/thing_guest_love/${kebabCase(title)}.svg`}
                      alt={kebabCase(title)}
                      width="20"
                      height="20"
                    />
                  </Size>
                </Space>
                <Space margin={[0, 0, 0, 1]}>
                  <Flex>
                    <View>
                      <Text
                        size="s"
                        color="greyDarker"
                        weight="medium"
                        component="p"
                      >
                        {title}
                      </Text>
                      <Text size="xs" color="greyDarker" component="p">
                        {`"${description}"`}
                      </Text>
                    </View>
                  </Flex>
                </Space>
              </View>
            </Flex>
          </Space>
        ))
      }
      </View>
    </Flex>
  </Space>
);

ThingsGuestLove.propTypes = {
  feedbacks: PropTypes.object.isRequired,
};

export default ThingsGuestLove;
