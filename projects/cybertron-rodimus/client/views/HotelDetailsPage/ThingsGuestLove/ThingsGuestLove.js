import React from 'react';
import PropTypes from 'prop-types';
import kebabCase from 'lodash/kebabCase';
import { Text } from '../../../components/Typography/Typography';
import './thingsGuestLove.css';

const ThingsGuestLove = ({ feedbacks }) => (
  <div className="tgl">
    <h2 className="heading-3 tgl__header">{feedbacks.title}</h2>
    {
      feedbacks.items.map(({ title, description }, index) => (
        <div className="tgl__list-item" key={`${title}${index + 1}`}>
          <Text type="3" className="tgl__title">
            <img
              className="old-image tgl__default-icon"
              src={`https://images.treebohotels.com/images/thing_guest_love/${kebabCase(title)}.svg`}
              alt=""
            />
            {title}
          </Text>
          <Text className="tgl__description" type="3">
            {`"${description}"`}
          </Text>
        </div>
      ))
    }
  </div>
);

ThingsGuestLove.propTypes = {
  feedbacks: PropTypes.object.isRequired,
};

export default ThingsGuestLove;
