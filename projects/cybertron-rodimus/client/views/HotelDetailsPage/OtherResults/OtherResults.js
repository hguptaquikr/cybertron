import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import { Text } from '../../../components/Typography/Typography';
import Accordion from '../../../components/Accordion/Accordion';
import contentService from '../../../services/content/contentService';
import './otherResults.css';

class OtherResults extends Component {
  renderLink = (place, index) => {
    const footerUrl = contentService.getSeoPathName(place);
    const label = place.label.split(',')[0];
    return (
      <Link to={footerUrl} key={`place${index}`}>
        <Text className="other-results__links">{label}</Text>
      </Link>
    );
  }

  render() {
    const { links } = this.props;
    return (
      <div className="other-results">
        {
          !isEmpty(links) ? (
            <Accordion.Container>
              {
                links.map((group, index) => (
                  !isEmpty(group.content) ? (
                    <Accordion.Section key={`group${index + 1}`}>
                      <Accordion.Title>
                        <Text className="other-results__title">
                          {group.title}
                        </Text>
                      </Accordion.Title>
                      <Accordion.Content>
                        <div className="other-results__content">
                          {group.content.map(this.renderLink)}
                        </div>
                      </Accordion.Content>
                    </Accordion.Section>
                  ) : null
                ))
              }
            </Accordion.Container>
          ) : null
        }
      </div>
    );
  }
}

OtherResults.propTypes = {
  links: PropTypes.array,
};

export default OtherResults;
