import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Heading, Subheading } from '../../components/Typography/Typography';
import Ripple from '../../components/Ripple/Ripple';
import PageHeader from '../../components/PageHeader/PageHeader';
import SeoSearchResultsPage from '../SeoSearchResultsPage';
import * as contentActionCreators from '../../services/content/contentDuck';
import analyticsService from '../../services/analytics/analyticsService';
import './citiesPage.css';

class CitiesPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(contentActionCreators.getCitiesList()));
    return Promise.all(promises);
  }

  componentDidMount() {
    const { contentActions, route, match } = this.props;
    SeoSearchResultsPage.preload();
    contentActions.getSeoContent(route, match.params);
    contentActions.getCitiesList();
  }

  renderCityItem = (city) => (
    <li key={city.name}>
      <Ripple>
        <Link
          to={`/hotels-in-${city.slug}/`}
          onClick={() => analyticsService.cityClicked(city.name, 'Cities')}
        >
          <Subheading className="cities__name" type="2">{city.name}</Subheading>
        </Link>
      </Ripple>
    </li>
  );

  render() {
    const { content: { popularCities, cities } } = this.props;
    const populatCityNames = popularCities.map((popularCity) => popularCity.name);
    const otherCities = cities.filter((city) => !populatCityNames.includes(city.name));

    return (
      <div className="cities">
        <PageHeader>
          <Heading>All Cities ({cities.length})</Heading>
        </PageHeader>
        <Subheading className="cities__section-header" tag="div">Popular Cities</Subheading>
        <ul>{popularCities.map(this.renderCityItem)}</ul>
        <Subheading className="cities__section-header" tag="div">Other Cities</Subheading>
        <ul>{otherCities.map(this.renderCityItem)}</ul>
      </div>
    );
  }
}

CitiesPage.propTypes = {
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CitiesPage);
