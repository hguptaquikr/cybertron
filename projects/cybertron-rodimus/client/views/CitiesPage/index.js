import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('CitiesPage');
    return import('./CitiesPage' /* webpackChunkName: 'CitiesPage' */);
  },
  loading: () => null,
});
