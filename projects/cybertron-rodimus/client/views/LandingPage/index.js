import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('LandingPage');
    return import('./LandingPage' /* webpackChunkName: 'LandingPage' */);
  },
  loading: () => null,
});
