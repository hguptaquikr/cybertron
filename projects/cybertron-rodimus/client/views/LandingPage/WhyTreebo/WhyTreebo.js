import React from 'react';
import PropTypes from 'prop-types';
import { Heading, Text } from '../../../components/Typography/Typography';
import './whyTreebo.css';

const WhyTreebo = ({ whyTreebo }) => (
  <ul className="wt">
    {
      whyTreebo.map((promise) => (
        <li className="wt__promise" key={promise.title}>
          <Heading tag="h3" type="2" className="wt__heading">
            <img className="old-image wt__image" src={promise.mobileImageUrl} alt="" />
            {promise.title}
          </Heading>
          <Text type="2" className="wt__text">{promise.description}</Text>
        </li>
      ))
    }
  </ul>
);

WhyTreebo.propTypes = {
  whyTreebo: PropTypes.array.isRequired,
};

export default WhyTreebo;
