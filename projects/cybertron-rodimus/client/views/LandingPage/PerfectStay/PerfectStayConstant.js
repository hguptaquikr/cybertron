const PerfectStayConstant = [
  {
    icon: 'icon-comfortable-room',
    alt: 'Comfortable Rooms',
    text: 'Comfortable Rooms',
  },
  {
    icon: 'icon-best-service',
    alt: 'Best-in-class Service',
    text: 'Best-in-class Service',
  },
  {
    icon: 'icon-breakfast',
    alt: 'Wholesome Breakfast',
    text: 'Wholesome Breakfast',
  },
  {
    icon: 'icon-room-amenities',
    alt: 'Assured Room Amenities',
    text: 'Assured Room Amenities',
  },
];

export default PerfectStayConstant;
