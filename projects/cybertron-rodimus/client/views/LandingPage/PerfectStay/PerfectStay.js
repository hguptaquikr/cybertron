import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Image from 'leaf-ui/cjs/Image/web';
import Text from 'leaf-ui/cjs/Text/web';
import { Subheading, Heading } from '../../../components/Typography/Typography';
import PerfectStayConstant from './PerfectStayConstant';
import analyticsService from '../../../services/analytics/analyticsService';
import './perfectStay.css';

class PerfectStay extends Component {
  knowMoreClick = () => {
    analyticsService.perfectStayKnowMoreClicked('Home');
  }

  render() {
    return (
      <div className="lp-ps">
        <div className="lp-ps__section">
          <div className="lp-ps__section__title">
            <Heading className="lp-ps__section__title__heading">
              {'Perfect Stay or Don’t Pay'}
            </Heading>
            <Subheading type="2" className="lp-ps__section__title__subheading">
              <Text type="2" className="ps__text">100% money-back promise</Text>
            </Subheading>
            <Image
              src="https://images.treebohotels.com/images/perfect-stay.svg"
              alt="Best Rate"
              width="200px"
              height="100px"
            />
          </div>
          <div className="lp-ps__section__content">
            {
              PerfectStayConstant ? (
                PerfectStayConstant.map((image) => (
                  <div className="lp-ps__section__content__item" key={image.icon}>
                    <i className={`lp-ps__section__content__icon ${image.icon}`} />
                    <Text color="greyDarker" weight="medium">
                      {image.text}
                    </Text>
                  </div>
                ))) : null
            }
            <Link to="/perfect-stay/" className="lp-ps__section__content__link" onClick={this.knowMoreClick}>
            Know more
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default PerfectStay;
