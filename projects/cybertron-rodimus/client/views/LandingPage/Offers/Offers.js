import React from 'react';
import PropTypes from 'prop-types';
import SlideShow from '../../../components/SlideShow/SlideShow';
import { Heading } from '../../../components/Typography/Typography';

const constructImages = (offers) => offers.map((offer) => ({
  url: offer.offerUrl,
  tagline: offer.description,
}));

const Offers = ({ offers }) => (
  <div className="lp__section">
    <Heading type="3" className="lp__section__heading">BEST OFFERS!</Heading>
    <SlideShow
      images={constructImages(offers)}
      aspectRatio={{ width: 4, height: 1.5 }}
    />
  </div>
);

Offers.propTypes = {
  offers: PropTypes.array.isRequired,
};

export default Offers;
