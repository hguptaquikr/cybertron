import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Heading, Text } from '../../../components/Typography/Typography';
import analyticsService from '../../../services/analytics/analyticsService';
import bannerImage from '!!url-loader!./banner.jpg'; // eslint-disable-line
import './hero.css';

class Hero extends Component {
  onSearchClick = () => {
    const { uiActions } = this.props;
    uiActions.showSearchWidget(true);
    analyticsService.searchBarClicked('Home');
  }

  render() {
    const { banner, ui } = this.props;
    return (
      <div className="hero">
        <div className="hero__image" style={{ backgroundImage: `url(${banner.imageUrl}), url(${bannerImage})` }} />
        <div className="hero__content">
          <div className="hero__topbar">
            <img className="old-image hero__topbar__logo" src="//images.treebohotels.com/images/treebo-white-logo.svg" alt="Treebo Hotels" />
          </div>
          <div>
            <Heading className="hero__heading" tag="h1" type="2">{banner.title}</Heading>
            <Text className="hero__subheading">{banner.description}</Text>
            <div
              id="hero__search"
              className={cx('hero__search', { 'hero__search--animate': ui.isSearchWidgetOpen })}
              onClick={this.onSearchClick}
            >
              <div className="hero__search__text">Search for Hotels</div>
              <i className="icon-search hero__search__icon" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Hero.propTypes = {
  banner: PropTypes.object.isRequired,
  uiActions: PropTypes.object.isRequired,
  ui: PropTypes.object.isRequired,
};

export default Hero;
