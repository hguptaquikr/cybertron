import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import qs from 'query-string';
import Image from '../../../components/Image/Image';
import analyticsService from '../../../services/analytics/analyticsService';
import './rewardsIntro.css';

class RewardsIntro extends Component {
  redirectToIntroducingTreeboRewardPage = () => {
    const { history } = this.props;
    analyticsService.rewardsBannerClicked();
    history.push({
      pathname: '/introducing-rewards/',
      search: qs.stringify({
        flow: 'banner',
      }),
    });
  }

  render() {
    const { rewards } = this.props;
    return (
      <div className="ri">
        <div
          className="ri__introducing-treebo-rewards"
          onClick={this.redirectToIntroducingTreeboRewardPage}
        >
          <Image
            src={rewards.landingPage.imageUrl}
            alt={rewards.landingPage.tagLine}
            aspectRatio={{ width: 4, height: 1.5 }}
          />
        </div>
      </div>
    );
  }
}

RewardsIntro.propTypes = {
  history: PropTypes.object.isRequired,
  rewards: PropTypes.object.isRequired,
};

export default withRouter(RewardsIntro);
