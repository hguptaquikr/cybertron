import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import kebabCase from 'lodash/kebabCase';
import cities from '../../CitiesPage';
import { Heading, Subheading, Text } from '../../../components/Typography/Typography';
import { Col, Row } from '../../../components/Flex/Flex';
import Ripple from '../../../components/Ripple/Ripple';
import Image from '../../../components/Image/Image';
import analyticsService from '../../../services/analytics/analyticsService';
import './popularCities.css';

const PopularCities = ({ popularCities }) => (
  <div className="pc">
    <Ripple eagerLoad={cities.preload}>
      <Link
        to="/cities/"
        onClick={() => analyticsService.cityClicked('All Cities', 'Home')}
      >
        <div className="pc__header">
          <Row>
            <Col size="7">
              <Heading type="3" className="lp__section__heading">
                POPULAR CITIES
              </Heading>
            </Col>
            <Col className="text-right" size="5">
              <span className="angle-link">
                View All
              </span>
            </Col>
          </Row>
          <Subheading type="3" className="pc__subtitle">
            75 Cities, 400 Hotels & 10000 Rooms
          </Subheading>
        </div>
      </Link>
    </Ripple>
    <div className="pc__content">
      <Row middle>
        {
          popularCities.slice(0, 4).map((city) => (
            <Ripple key={city.name}>
              <Link
                className="col text-center pc__city"
                to={`/hotels-in-${kebabCase(city.name)}/`}
                onClick={() => analyticsService.cityClicked(city.name, 'Home')}
              >
                <Image
                  src={city.image}
                  alt={city.name}
                  className="pc__city__img"
                  aspectRatio={{ width: 1, height: 1 }}
                />
                <Text className="pc__city__name">{city.name}</Text>
              </Link>
            </Ripple>
          ))
        }
      </Row>
    </div>
  </div>
);

PopularCities.propTypes = {
  popularCities: PropTypes.array.isRequired,
};

export default PopularCities;
