import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import qs from 'query-string';
import { Text } from '../../components/Typography/Typography';
import NavigationDrawer from '../../components/NavigationDrawer/NavigationDrawer';
import SearchWidgetAsync from '../SearchWidget/SearchWidgetAsync';
import SeoSearchResultsPage from '../SeoSearchResultsPage';
import PopularCities from './PopularCities/PopularCities';
import Hero from './Hero/Hero';
import Offers from './Offers/Offers';
import RewardsIntro from './RewardsIntro/RewardsIntro';
import * as walletActionCreators from '../../services/wallet/walletDuck';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as uiActionCreators from '../../services/ui/uiDuck';
import analyticsService from '../../services/analytics/analyticsService';
import PerfectStay from './PerfectStay/PerfectStay';
import './landingPage.css';

class LandingPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match, req }) => {
    const promises = [];
    dispatch(searchActionCreators.setSearchState({ ...req.query, ...match.params }));
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(contentActionCreators.getLandingContent()));
    return Promise.all(promises);
  }

  componentDidMount() {
    const {
      auth,
      walletActions,
      content,
      contentActions,
      searchActions,
      match,
      location,
      route,
    } = this.props;

    searchActions.setSearchState({ ...qs.parse(location.search), ...match.params });
    contentActions.getSeoContent(route, match.params);
    if (isEmpty(content.offers)) contentActions.getLandingContent();
    this.getCitiesList();
    if (auth.isAuthenticated) walletActions.getWalletBalance();
    analyticsService.homeContentViewed();
    SearchWidgetAsync.preload();
    SeoSearchResultsPage.preload();
  }

  getCitiesList = () => {
    const { content, contentActions } = this.props;
    if (isEmpty(content.cities)) {
      contentActions.getCitiesList();
    }
  }

  render() {
    const {
      content: {
        banner,
        offers,
        popularCities,
        rewards,
      },
      ui,
      uiActions,
    } = this.props;

    return (
      <div className="lp">
        <NavigationDrawer />
        <Hero banner={banner} ui={ui} uiActions={uiActions} />
        <PopularCities popularCities={popularCities} />
        <PerfectStay />
        <RewardsIntro rewards={rewards} />
        <Offers offers={offers} />
        <Text className="lp__copyright text-center" type="2" tag="div">
          Copyright &copy; 2014 - 2018, Treebo.com
        </Text>
      </div>
    );
  }
}

LandingPage.propTypes = {
  route: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  ui: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
  uiActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  ui: state.ui,
  auth: state.auth,
  content: state.content,
  search: state.search,
});

const mapDispatchToProps = (dispatch) => ({
  uiActions: bindActionCreators(uiActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LandingPage);
