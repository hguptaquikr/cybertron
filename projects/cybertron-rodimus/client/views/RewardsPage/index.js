import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('RewardsPage');
    return import('./RewardsPage' /* webpackChunkName: 'RewardsPage' */);
  },
  loading: () => null,
});
