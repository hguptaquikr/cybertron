import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import qs from 'query-string';
import BackButton from '../../components/BackButton/BackButton';
import Price from '../../components/Price/Price';
import Ripple from '../../components/Ripple/Ripple';
import { Subheading } from '../../components/Typography/Typography';
import RewardsTransactionsList from '../../components/Rewards/RewardsTransactionsList/RewardsTransactionsList';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as walletActionCreators from '../../services/wallet/walletDuck';
import analyticsService from '../../services/analytics/analyticsService';
import './rewardsPage.css';

class RewardsPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    return Promise.all(promises);
  }

  componentDidMount() {
    const { contentActions, walletActions, route, match } = this.props;
    contentActions.getSeoContent(route, match.params);
    contentActions.getRewardsContent();
    walletActions.getWalletBalance();
    walletActions.getWalletStatement();
  }

  redirectToIntroducingTreeboRewardPage = () => {
    const { history } = this.props;
    analyticsService.treeboRewardsClicked();
    history.push({
      pathname: '/introducing-rewards/',
      search: qs.stringify({ flow: 'Rewards Page' }),
    });
  }

  render() {
    const { wallet } = this.props;
    const treeboPoints = wallet.byType.TP || wallet.byType[''];
    return (
      <div className="rewards">
        <section className="rewards__header">
          <div className="rewards__nav-bar">
            <BackButton className="rewards__nav-bar__back-btn" />
            <Ripple>
              <span
                className="rewards__nav-bar__view-details"
                onClick={this.redirectToIntroducingTreeboRewardPage}
              >
                View Details
              </span>
            </Ripple>
          </div>
          <div className="rewards__balance text-center">
            <Price
              className="rewards__balance__amount"
              size="x-large"
              price={treeboPoints.totalBalance}
              preview={!treeboPoints.totalBalance}
              round={false}
            />
            <Subheading className="rewards__balance__title">worth of Treebo Points</Subheading>
          </div>
        </section>
        {
          treeboPoints.totalBalance ? (
            <div className="rewards__tnc">
              <Subheading className="rewards__tnc__details">
                Treebo Points get credited to primary traveller
                post mobile verification during checkout.
              </Subheading>
            </div>
          ) : null
        }
        <RewardsTransactionsList />
      </div>
    );
  }
}

RewardsPage.propTypes = {
  route: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  wallet: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  wallet: state.wallet,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RewardsPage);
