import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('FeedbackChange');
    return import('./FeedbackChange' /* webpackChunkName: 'FeedbackChange' */);
  },
  loading: () => null,
});
