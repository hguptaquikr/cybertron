import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import qs from 'query-string';
import Flex from 'leaf-ui/cjs/Flex/web';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import Size from 'leaf-ui/cjs/Size/web';
import View from 'leaf-ui/cjs/View/web';
import * as contentActionCreators from '../../../services/content/contentDuck';
import analyticsService from '../../../services/analytics/analyticsService';

class FeedbackChange extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(contentActionCreators.getFeedbackTags(match.params.feedbackId)));
    return Promise.all(promises);
  }

  componentDidMount() {
    const { contentActions, route, match } = this.props;
    contentActions.getSeoContent(route, match.params);
    contentActions.getFeedbackTags(match.params.feedbackId);
  }

  submitFeedback = () => {
    const {
      content: { feedback },
      contentActions,
      location,
    } = this.props;
    const query = qs.parse(location.search);
    const updatedFeedback = {
      ...feedback,
      source: query.source,
    };
    contentActions.submitFeedback(updatedFeedback);
    const analyticsProperty = {
      device: 'Desktop',
      source: feedback.source,
      booking: feedback.booking,
      user: feedback.user,
      rating: feedback.feedback_flow.value,
    };
    analyticsService.feedbackDetail('Click on rating', analyticsProperty);
  }

  render() {
    const {
      content: { feedback },
      location,
      match,
    } = this.props;
    const query = qs.parse(location.search);
    return (
      <Flex>
        <View>
          <Space margin={[4, 'auto', 0]}>
            <Size width="80%">
              <Flex justifyContent="center">
                <View>
                  <img
                    src="http://images.treebohotels.com/images/logo_treebo.svg"
                    alt="Treebo Hotels"
                    className="old-image"
                  />
                </View>
              </Flex>
            </Size>
          </Space>
          <Space margin={[4, 'auto', 0]} width="80%">
            <Size width="80%">
              <Flex
                flexDirection="row"
                justifyContent="center"
              >
                <View>
                  <Text
                    size="s"
                    color="greyDarker"
                  >
                    Hi {feedback.user.name},
                  </Text>
                </View>
              </Flex>
            </Size>
          </Space>
          <Space margin={[1, 'auto', 0]}>
            <Size width="80%">
              <Flex
                flexDirection="row"
                justifyContent="center"
              >
                <View>
                  <Text
                    size="l"
                    color="greyDark"
                    style={{ textAlign: 'center', lineHeight: '1.2' }}
                  >
                    How was your recent stay with us,
                    at <strong>{feedback.booking.hotel_name}</strong>?
                  </Text>
                </View>
              </Flex>
            </Size>
          </Space>
          <Space padding={[6, 0]} margin={[0, 'auto']}>
            <Size width="90%">
              <Flex
                flexDirection="row"
                justifyContent="space-around"
              >
                <View>
                  {
                    ['terrible', 'veryBad', 'avg', 'good', 'amazing'].map((expression) => (
                      <View key={expression}>
                        { expression === 'amazing' ?
                          (
                            <Link
                              to={{
                                pathname: `/tripadvisor/reviews-${feedback.booking.hotel_id}/`,
                                search: qs.stringify({
                                  flow: 'amazing',
                                  source: query.source,
                                  feedbackId: feedback.id,
                                  taWidgetUrl: decodeURIComponent(feedback.ta_url),
                                }),
                              }}
                              onClick={this.submitFeedback}
                            >
                              <img
                                src={`http://images.treebohotels.com/images/Email/${expression}.png`}
                                alt="Awesome Experience"
                                style={{ height: '5em' }}
                                className="old-image"
                              />
                            </Link>
                          ) : (
                            <Link
                              to={`/feedback/${match.params.feedbackId}/?source=${query.source}&expression=${expression}`}
                              onClick={this.submitFeedback}
                            >
                              <img
                                src={`http://images.treebohotels.com/images/Email/${expression}.png`}
                                alt="Awesome Experience"
                                style={{ height: '5em' }}
                                className="old-image"
                              />
                            </Link>
                              )
                            }
                      </View>
                    ))
                  }
                </View>
              </Flex>
            </Size>
          </Space>
          <img
            src="http://images.treebohotels.com/images/Email/hr.png"
            alt="hr"
            className="old-image"
          />
          <Space margin={[4, 0]} >
            <Flex
              flexDirection="row"
              justifyContent="center"
              alignItems="center"
            >
              <View>
                <Text
                  color="greyDark"
                  size="s"
                >
                  YOUR STAY DETAILS
                </Text>
              </View>
            </Flex>
          </Space>
          <Space margin={[2, 'auto', 0]}>
            <Size width="90%">
              <Flex justifyContent="center">
                <View>
                  <Text
                    weight="bold"
                    color="greyDarker"
                    size="m"
                  >
                    {feedback.booking.hotel_name}
                  </Text>
                  <Text
                    weight="normal"
                    size="m"
                    color="greyDark"
                  >
                    {feedback.booking.hotel_address}
                  </Text>
                </View>
              </Flex>
            </Size>
          </Space>
          <Space margin={[3, 'auto']}>
            <Size width="90%">
              <hr />
            </Size>
          </Space>
          <Space margin={[2, 'auto', 0]} >
            <Size width="90%">
              <Text
                weight="bold"
                size="m"
                color="greyDark"
              >
                {
              feedback.booking.no_of_room > 1 ? (
                `${feedback.booking.no_of_room} Rooms`
              ) : (
                `${feedback.booking.no_of_room} Room`
              )
            }
              </Text>
            </Size>
          </Space>
          <Space margin={[2, 'auto', 0]}>
            <Size width="90%">
              <Text
                size="m"
                color="greyDark"
              >
                {feedback.booking.from_date} - {feedback.booking.to_date}
              </Text>
            </Size>
          </Space>
        </View>
      </Flex>
    );
  }
}

FeedbackChange.propTypes = {
  route: PropTypes.object.isRequired,
  location: PropTypes.object,
  match: PropTypes.object.isRequired,
  content: PropTypes.object,
  contentActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
  review: state.review,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FeedbackChange);
