import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('FeedbackThanks');
    return import('./FeedbackThanks' /* webpackChunkName: 'FeedbackThanks' */);
  },
  loading: () => null,
});
