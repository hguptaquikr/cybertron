import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Flex from 'leaf-ui/cjs/Flex/web';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import View from 'leaf-ui/cjs/View/web';
import * as contentActionCreators from '../../../services/content/contentDuck';

class FeedbackThanks extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    return Promise.all(promises);
  }

  componentDidMount() {
    const {
      contentActions,
      route,
      match,
    } = this.props;
    contentActions.getFeedbackTags(match.params.feedbackId)
      .then((res) => {
        const { data: feedbackData } = res.payload;
        const completeFeedback = {
          ...feedbackData,
          state: 'COMPLETED',
        };
        contentActions.submitFeedback(completeFeedback);
      });
    contentActions.getSeoContent(route, match.params);
  }

  render() {
    const {
      content: { feedback },
    } = this.props;
    return (
      <Flex>
        <View>
          <Space padding={[10, 0, 5]}>
            <Flex
              flexDirection="row"
              justifyContent="center"
            >
              <View>
                <img
                  src="http://images.treebohotels.com/images/Email/thanks.png"
                  alt="Thank You"
                  style={{ height: '107px' }}
                  className="old-image"
                />
              </View>
            </Flex>
          </Space>
          <Flex justifyContent="center">
            <View>
              <Space margin={[0, 0, 2]}>
                <Flex
                  flexDirection="row"
                  justifyContent="center"
                >
                  <View>
                    <Text
                      color="greyDarker"
                      size="xl"
                      weight="bold"
                    >
                    Thank you {feedback.user ? feedback.user.name : ''}!
                    </Text>
                  </View>
                </Flex>
              </Space>
              <Space margin={[0, 2, 2]}>
                <Flex
                  flexDirection="row"
                  justifyContent="center"
                  alignItems="center"
                >
                  <View>
                    <Text
                      color="greyDark"
                      size="s"
                      style={{ textAlign: 'center', lineHeight: 1.5 }}
                    >
                      We are very sorry to hear that you had a poor experience with us.
                      This is definitely not the experience we strive to deliver.
                      Thank you for taking your time out to share your detailed feedback,
                      it goes a long way in helping us get better.
                    </Text>
                  </View>
                </Flex>
              </Space>
              <Space margin={[2, 0, 0]}>
                <Flex alignItems="center">
                  <View>
                    <Text
                      size="m"
                      color="greyDark"
                      weight="bold"
                    >
                    - Sidharth Gupta,
                    </Text>
                    <Text
                      size="xs"
                      color="greyDark"
                    >
                      Co-founder @ Treebo Hotels
                    </Text>
                  </View>
                </Flex>
              </Space>
            </View>
          </Flex>
        </View>
      </Flex>
    );
  }
}

FeedbackThanks.propTypes = {
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  content: PropTypes.object,
  contentActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FeedbackThanks);
