import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import qs from 'query-string';
import Flex from 'leaf-ui/cjs/Flex/web';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import Size from 'leaf-ui/cjs/Size/web';
import View from 'leaf-ui/cjs/View/web';
import Button from 'leaf-ui/cjs/Button/web';
import TextInput from 'leaf-ui/cjs/TextInput/web';
import * as contentActionCreators from '../../../services/content/contentDuck';
import analyticsService from '../../../services/analytics/analyticsService';

class Feedback extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(contentActionCreators.getFeedbackTags(match.params.feedbackId)));
    return Promise.all(promises);
  }

  state = {
    showInput: false,
    feedbackTag: true,
  };

  componentDidMount() {
    const { contentActions, route, match, location } = this.props;
    const query = qs.parse(location.search);
    contentActions.getSeoContent(route, match.params);
    contentActions.getFeedbackTags(match.params.feedbackId)
      .then((feedbackData) => {
        contentActions.submitFeedback(this.makeFeedbackData(feedbackData.payload.data, query));
      });
  }

  makeFeedbackData = (feedback, query) => (
    {
      ...feedback,
      source: query.source,
      feedback_flow: {
        ...feedback.feedback_flow,
        value: query.expression,
      },
    }
  )

  updateTag = (tag) => (e) => {
    const {
      content: { feedback },
      contentActions,
    } = this.props;
    if (tag.name === 'other') {
      this.setState({ showInput: true });
      contentActions.updateFeedbackTags('Others', e.target.value);
    } else {
      contentActions.updateFeedbackTags(tag.name, true);
    }

    const analyticsProperty = {
      device: 'Desktop',
      source: feedback.source,
      booking: feedback.booking,
      user: feedback.user,
      tagSelected: tag.name,
      rating: feedback.feedback_flow.value,
    };
    analyticsService.feedbackDetail('Click on Tag', analyticsProperty);
  }

  updateTag = (tag) => () => {
    const {
      contentActions,
    } = this.props;
    const { feedbackTag } = this.state;
    this.setState({ feedbackTag: !feedbackTag });
    if (tag.type === 'text') {
      this.setState({ showInput: feedbackTag });
    } else {
      contentActions.updateFeedbackTags(tag.name, feedbackTag);
    }
  }

  updateValue = () => (e) => {
    const {
      contentActions,
      content: { feedback },
    } = this.props;
    contentActions.updateFeedbackTags('Other', e.target.value);
    const analyticsProperty = {
      device: 'Desktop',
      source: feedback.source,
      booking: feedback.booking,
      user: feedback.user,
      OtherComment: e.target.value,
      rating: feedback.feedback_flow.value,
    };
    analyticsService.feedbackDetail('Click on Others', analyticsProperty);
  }

  submitFeedback = () => {
    const {
      history,
      content,
      contentActions,
      location,
      match,
    } = this.props;
    const query = qs.parse(location.search);
    contentActions.submitFeedback(this.makeFeedbackData(content.feedback, query))
      .then(() => {
        if (query.expression === 'terrible' || query.expression === 'veryBad') {
          history.push(`/feedback/${content.feedback.id}/thanks/`);
        } else if (query.expression === 'avg' || query.expression === 'good') {
          history.push({
            pathname: `/tripadvisor/reviews-${content.feedback.booking.hotel_id}/`,
            search: qs.stringify({
              flow: query.expression,
              feedbackId: match.params.feedbackId,
            }),
          });
        }
      });

    const analyticsProperty = {
      device: 'Desktop',
      source: content.feedback.source,
      booking: content.feedback.booking,
      user: content.feedback.user,
      value: content.feedback.current.sections.tag_section.values,
      rating: content.feedback.feedback_flow.value,
    };
    analyticsService.feedbackDetail('Click on Submit', analyticsProperty);
  }

  render() {
    const { showInput } = this.state;
    const { content: { feedback }, location } = this.props;
    const query = qs.parse(location.search);
    const enableSubmitBtn = feedback.current.sections.tag_section.values.some((tags) =>
      (tags.body.value.length || tags.body.value === true));
    return (
      <Size height="100%">
        <View className="feedback">
          {
          feedback.state === 'EXPIRED' || feedback.state === 'COMPLETED' ? (

            <Flex justifyContent="space-between">
              <Size height="100%">
                <View>
                  <View>
                    <Space
                      padding={[20, 0, 8]}
                      width="auto"
                      height="40%"
                    >
                      <Flex
                        flexDirection="row"
                        justifyContent="center"
                        alignItems="center"
                      >
                        <View>
                          <img
                            className="old-image"
                            src="http://images.treebohotels.com/images/Email/oops.svg"
                            alt="Thank You"
                            style={{ height: '104px' }}
                          />
                        </View>
                      </Flex>
                    </Space>
                    <Space margin={[0, 2, 1]}>
                      <Flex
                        justifyContent="center"
                        alignItems="center"
                      >
                        <View>
                          <Text
                            size="s"
                            style={{ textAlign: 'center' }}
                          >
                            This link is no longer available, but we would love to hear from you.
                          </Text>
                        </View>
                      </Flex>
                    </Space>
                    <Space margin={[0, 2, 1]}>
                      <Text
                        size="s"
                        style={{ textAlign: 'center' }}
                      >
                        Write to us at <strong>feedback@treebohotels.com</strong>
                      </Text>
                    </Space>
                  </View>
                  <a href="mailto:feedback@treebohotels.com" target="_top">
                    <Button kind="filled" block>WRITE YOUR FEEDBACK</Button>
                  </a>
                </View>
              </Size>
            </Flex>
          ) : (
            <Flex justifyContent="space-between">
              <Size height="100%">
                <View>
                  <View>
                    <Space margin={[0, 'auto']} padding={[6, 0, 0]}>
                      <Size width="25%">
                        <Flex alignItems="center">
                          <View>
                            <img
                              className="old-image"
                              src={`http://images.treebohotels.com/images/Email/${query.expression}.png`}
                              alt="Awesome Experience"
                            />
                            <Space margin={[1, 'auto']}>
                              <Link to={`/feedback/${feedback.id}/change/?source=${query.source}`}>
                                <Text size="s" color="blue">Change</Text>
                              </Link>
                            </Space>
                          </View>
                        </Flex>
                      </Size>
                    </Space>
                    <Space margin={[2, 4, 0]}>
                      {
                        query.expression === 'terrible' || query.expression === 'veryBad' ?
                        (
                          <Text
                            weight="normal"
                            size="s"
                            style={{ textAlign: 'center' }}
                          >
                            We are very sorry to hear about your poor experience with us.
                          </Text>
                        ) : (
                          <Text
                            weight="normal"
                            size="s"
                            style={{ textAlign: 'center' }}
                          >
                            We are sorry we couldn’t deliver our best experience.
                          </Text>
                        )
                      }
                    </Space>
                    <Space margin={[4, 4, 0]}>
                      <Flex
                        justifyContent="center"
                        alignItems="center"
                      >
                        <View>
                          {
                        query.expression === 'terrible' || query.expression === 'veryBad' ?
                        (
                          <Text
                            size="xl"
                            weight="bold"
                            color="greyDarker"
                          >
                            What could be improved?
                          </Text>
                        ) : (
                          <Text
                            size="xl"
                            weight="bold"
                            color="greyDarker"
                          >
                            Please tell us areas of improvement?
                          </Text>
                        )
                      }
                        </View>
                      </Flex>
                    </Space>
                    <Space margin={[6, 'auto']} width="90%">
                      <Size width="90%">
                        <Flex
                          flexDirection="row"
                          justifyContent="space-around"
                          flexWrap="wrap"
                        >
                          <View>
                            {
                          Object.values(feedback.current.sections.tag_section.values).map((tag) => (
                            <Space
                              key={tag.name}
                              margin={[0, 0, 2, 0]}
                            >
                              <Button
                                size="small"
                                color={tag.body.value === true || showInput ? 'lagoon' : 'greyDark'}
                                kind={tag.body.value || (tag.type === 'text' && showInput) ? 'filled' : 'outlined'}
                                onClick={this.updateTag(tag)}
                              >
                                {tag.name}
                              </Button>
                            </Space>
                          ))
                        }
                          </View>
                        </Flex>
                      </Size>
                    </Space>
                    {
                  showInput ?
                  (
                    <Flex
                      flexDirection="row"
                      justifyContent="space-around"
                    >
                      <View>
                        <Space margin={[5, 'auto']} >
                          <Size width="90%">
                            <TextInput
                              type="text"
                              block
                              onChange={this.updateValue()}
                            />
                          </Size>
                        </Space>
                      </View>
                    </Flex>
                  ) : null
                }
                  </View>
                  <Button
                    block
                    disabled={!enableSubmitBtn}
                    onClick={this.submitFeedback}
                  >
                  SUBMIT
                  </Button>
                </View>
              </Size>
            </Flex>
          )
        }
        </View>
      </Size>
    );
  }
}

Feedback.propTypes = {
  route: PropTypes.object.isRequired,
  location: PropTypes.object,
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  contentActions: PropTypes.object,
  content: PropTypes.object,
};

const mapStateToProps = (state) => ({
  content: state.content,
  review: state.review,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Feedback);
