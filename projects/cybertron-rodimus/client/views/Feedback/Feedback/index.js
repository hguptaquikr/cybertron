import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('Feedback');
    return import('./Feedback' /* webpackChunkName: 'Feedback' */);
  },
  loading: () => null,
});
