import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import PageHeader from '../../../components/PageHeader/PageHeader';
import { Heading, Text } from '../../../components/Typography/Typography';
import Image from '../../../components/Image/Image';
import Price from '../../../components/Price/Price';
import Modal from '../../../components/Modal/Modal';
import Button from '../../../components/Button/Button';
import Ripple from '../../../components/Ripple/Ripple';
import BookingHistoryDetailsItinerary from './BookingHistoryDetailsItinerary';
import BookingHistoryDetailsPayment from './BookingHistoryDetailsPayment';
import BookingHistoryDetailsPriceBreakup from './BookingHistoryDetailsPriceBreakup';
import BookingHistoryCancellationPage from '../BookingHistoryCancellationPage';
import * as contentActionCreators from '../../../services/content/contentDuck';
import * as authActionCreators from '../../../services/auth/authDuck';
import { constants } from '../../../services/utils';
import './bookingHistoryDetails.css';

class BookingHistoryDetailsPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    return Promise.all(promises);
  }

  state = { showPriceBreakupModal: false }

  componentDidMount() {
    const { authActions, contentActions, route, match } = this.props;
    contentActions.getSeoContent(route, match.params);
    authActions.getBookingHistory();
    BookingHistoryCancellationPage.preload();
  }

  toggleModal = (modalStateControl, isOpen) => (e) => {
    e.preventDefault();
    this.setState({
      [modalStateControl]: isOpen,
    });
  }

  render() {
    const { showPriceBreakupModal } = this.state;
    const {
      bookingDetails: {
        id,
        amountPaid,
        partPayLink,
        partPayAmount,
        isPastBooking,
        isPayAtHotel,
        status,
        hotel,
        dates,
        price,
        coupon,
        roomConfig,
        guest,
        cancellation,
      },
    } = this.props;

    let bookingStatus = 'cancelled';
    if (partPayLink && status === 'RESERVED') {
      bookingStatus = 'temporary';
    } else if (status === 'RESERVED') {
      bookingStatus = 'confirmed';
    }

    return (
      <div>
        <PageHeader>
          <Heading>Booking Details</Heading>
        </PageHeader>
        <div className="bhdp">
          <div
            className={cx('bhdp__image', {
              'bhdp__image--grayscale': isPastBooking,
            })}
            style={{ backgroundImage: `url(${Image.getCdnUrl(hotel.imageUrl)})` }}
          >
            <Text
              className={cx(`bhdp__status bhdp__status--${bookingStatus}`, {
                'bhdp__status--past': isPastBooking,
              })}
              preview={!id}
              previewStyle={{ width: '40%' }}
            >
              {
                bookingStatus === 'temporary' ? (
                  'Payment Pending'
                ) : (
                  `${bookingStatus} booking`
                )
              }
            </Text>
          </div>
        </div>
        <div className="bhdp__details">
          <BookingHistoryDetailsItinerary
            bookingId={id}
            hotel={hotel}
            dates={dates}
            roomConfig={roomConfig}
            guest={guest}
          />
          <BookingHistoryDetailsPayment
            bookingStatus={bookingStatus}
            isPayAtHotel={isPayAtHotel}
            isPastBooking={isPastBooking}
            price={price}
            amountPaid={amountPaid}
            showPriceBreakup={this.toggleModal('showPriceBreakupModal', true)}
          />
          <div className="bhdp__contact-us">
            <Text type="1">For queries regarding booking & cancellation</Text>
            <a href={`tel:${constants.customerCareNumber}`}>
              <Text
                type="2"
                modifier="link"
              >
                Contact Customer Care
              </Text>
            </a>
          </div>
          {
            !isPastBooking && bookingStatus !== 'cancelled' && cancellation.hash !== 'None' ? (
              <Ripple>
                <Link to={`/c-${cancellation.hash}/`}>
                  <Button
                    block
                    modifier="secondary"
                    onClick={this.toggleModal('showCancellationModal', true)}
                  >
                    Cancel Booking
                  </Button>
                </Link>
              </Ripple>
            ) : null
          }
          {
            !isPastBooking && bookingStatus === 'temporary' ? (
              <div className="bhdp__partpay-btn-container">
                <Button flat block large className="floaty">
                  <a href={partPayLink} target="_blank" rel="noopener noreferrer">
                    Pay&nbsp;
                    <Price
                      type="default"
                      size="medium"
                      price={partPayAmount}
                    />
                    &nbsp;To Confirm
                  </a>
                </Button>
              </div>
            ) : null
          }
        </div>
        <Modal
          label="view-breakup-modal"
          closeIcon="close"
          isOpen={showPriceBreakupModal}
          onClose={this.toggleModal('showPriceBreakupModal', false)}
          header={<Heading>Price Breakup</Heading>}
          bodyClassName="bhdp__pb-container"
        >
          <BookingHistoryDetailsPriceBreakup
            amountPaid={amountPaid}
            coupon={coupon}
            price={price}
          />
        </Modal>
      </div>
    );
  }
}

BookingHistoryDetailsPage.propTypes = {
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  authActions: PropTypes.object.isRequired,
  bookingDetails: PropTypes.object.isRequired,
};

const mapStateToProps = (state, { match }) => ({
  bookingDetails:
    state.auth.bookingHistory.results[match.params.bookingId]
    || state.auth.bookingHistory.results[0],
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  authActions: bindActionCreators(authActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BookingHistoryDetailsPage);
