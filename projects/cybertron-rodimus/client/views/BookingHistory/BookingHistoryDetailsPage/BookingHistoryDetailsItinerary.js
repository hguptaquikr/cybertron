import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from '../../../components/Flex/Flex';
import { Subheading, Text } from '../../../components/Typography/Typography';
import NewYearRateTag from '../../../components/NewYearRateTag/NewYearRateTag';
import { pluralize, getMapsUrl } from '../../../services/utils';
import config from '../../../../config';

const BookingHistoryDetailsItinerary = ({
  bookingId,
  hotel,
  hotel: { coordinates, id },
  dates,
  guest,
  roomConfig: {
    adults,
    kids,
    roomCount,
    roomType,
  },
}) => (
  <div>
    <Text type="2" tag="span">Booking ID &nbsp;&nbsp;&nbsp;</Text>
    <Subheading
      className="bhdp__field-value"
      tag="span"
      preview={!hotel.name}
      previewStyle={{ width: '50%' }}
    >
      {bookingId}
    </Subheading>
    <div className="bhdp__hotel-name-container">
      <Subheading
        className="bhdp__field-value"
        tag="div"
        preview={!hotel.name}
        previewStyle={{ width: '75%' }}
      >
        {hotel.name}
      </Subheading>
      <a href={`tel:${hotel.contact.replace(' ', '')}`}>
        <i className="icon-phone text--primary bhdp__field-value bhdp__call-hotel" />
      </a>
    </div>
    <Text
      type="2"
      preview={!hotel.address}
      previewStyle={{ width: '100%' }}
    >
      {hotel.address}
    </Text>
    <a
      href={getMapsUrl(coordinates.lat, coordinates.lng)}
      target="_blank"
      rel="noopener noreferrer"
    >
      <Text type="2" modifier="link">Get Directions</Text>
    </a>
    <Row middle className="bhdp__dates-container">
      <Col size="4">
        <Text type="2">Check In</Text>
        <Subheading
          className="bhdp__field-value"
          tag="div"
          preview={!dates.checkIn}
          previewStyle={{ width: '100%' }}
        >
          {dates.checkIn}
        </Subheading>
      </Col>
      <Col size="2" className="text-center">
        <i className="icon-clock bhdp__field_value" />
        <Text
          type="2"
          tag="div"
          preview={!dates.durationOfStay}
          previewStyle={{ width: '100%' }}
        >
          {`${dates.durationOfStay} ${pluralize(dates.durationOfStay, 'Night')}`}
        </Text>
      </Col>
      <Col size="4">
        <Text type="2">Check Out</Text>
        <Subheading
          className="bhdp__field-value"
          tag="div"
          preview={!dates.checkOut}
          previewStyle={{ width: '100%' }}
        >
          {dates.checkOut}
        </Subheading>
      </Col>
    </Row>
    <div>
      <Text type="2">Primary Traveller</Text>
      <Subheading
        className="bhdp__field-value"
        tag="div"
        preview={!guest.name}
        previewStyle={{ width: '60%' }}
      >
        {guest.name}
      </Subheading>
    </div>
    <Row className="bhdp__room-config">
      <Col size="5">
        <Text type="2">Guests</Text>
        <Subheading
          className="bhdp__field-value"
          tag="div"
          preview={!adults}
          previewStyle={{ width: '80%' }}
        >
          {adults} {pluralize(adults, 'Adult')}&nbsp;
          {kids ? `,${kids} ${pluralize(kids, 'Kid')}` : ''}
        </Subheading>
      </Col>
      <Col size="7">
        <Text type="2">Rooms</Text>
        <Subheading
          className="bhdp__field-value"
          tag="div"
          preview={!roomType || !roomCount}
          previewStyle={{ width: '60%' }}
        >
          {roomCount} x {roomType}
        </Subheading>
      </Col>
    </Row>
    <a
      href={`${config.hostUrl}/api/web/v1/checkout/printpdf?order_id=${bookingId}`}
      target="_blank"
    >
      <Text type="2" modifier="link">Download Booking Details</Text>
    </a>
    <NewYearRateTag
      hotelId={id}
      range={{
        start: dates.checkIn,
        end: dates.checkOut,
      }}
    />
  </div>
);

BookingHistoryDetailsItinerary.propTypes = {
  bookingId: PropTypes.string.isRequired,
  hotel: PropTypes.object.isRequired,
  dates: PropTypes.object.isRequired,
  guest: PropTypes.object.isRequired,
  roomConfig: PropTypes.object.isRequired,
};

export default BookingHistoryDetailsItinerary;
