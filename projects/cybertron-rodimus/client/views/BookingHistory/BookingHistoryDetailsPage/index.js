import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('BookingHistoryDetailsPage');
    return import('./BookingHistoryDetailsPage' /* webpackChunkName: 'BookingHistoryDetailsPage' */);
  },
  loading: () => null,
});
