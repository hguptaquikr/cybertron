import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from '../../../components/Flex/Flex';
import { Subheading } from '../../../components/Typography/Typography';
import Price from '../../../components/Price/Price';
import RatePlanLabel from '../../../components/RatePlanLabel/RatePlanLabel';

const BookingHistoryDetailsPriceBreakup = ({
  price: {
    basePrice,
    tax,
    totalPrice,
    memberDiscount,
    treeboPointsUsed,
    voucherPrice,
    ratePlan,
    amountPaidViaGateway,
  },
  coupon: { code: couponCode, amount: couponAmount },
  amountPaid,
}) => (
  <div>
    <Row between middle className="bhdp__pb-field">
      <Col>
        <Subheading type="1" tag="div">Room Tariff</Subheading>
      </Col>
      <Col className="text-right">
        <Price price={+basePrice} bold size="regular" />
      </Col>
    </Row>
    <Row between middle className="bhdp__pb-field">
      <Col>
        <Subheading type="1" tag="div">Tax</Subheading>
      </Col>
      <Col className="text-right">
        <Price price={+tax} bold size="regular" />
      </Col>
    </Row>
    {
      +voucherPrice > 0 ? (
        <Row between middle className="bhdp__pb-field">
          <Col>
            <Subheading type="1" tag="div">Amount Paid</Subheading>
          </Col>
          <Col className="text-right">
            - <Price price={+voucherPrice} bold size="regular" type="discount" />
          </Col>
        </Row>
      ) : null
    }
    {
      couponCode !== '' && +couponAmount > 0 ? (
        <Row between middle className="bhdp__pb-field">
          <Col size="8">
            <Subheading type="1" tag="div">
              <q>{couponCode}</q> Applied
            </Subheading>
          </Col>
          <Col className="text-right" size="4">
            - <Price price={+couponAmount} bold size="regular" type="discount" />
          </Col>
        </Row>
      ) : null
    }
    {
      +memberDiscount > 0 ? (
        <Row between middle className="bhdp__pb-field">
          <Col>
            <Subheading type="1" tag="div">Member Rates</Subheading>
          </Col>
          <Col className="text-right">
            - <Price
              price={+memberDiscount}
              bold
              size="regular"
              type="discount"
            />
          </Col>
        </Row>
      ) : null
    }
    {
      +totalPrice > 0 ? (
        <Row between middle className="bhdp__pb-field--pd">
          <Col>
            <Subheading type="1" tag="div">
              <b>Total Tariff</b>
            </Subheading>
          </Col>
          <Col className="text-right">
            <Price price={+totalPrice} bold size="regular" />
          </Col>
        </Row>
      ) : null
    }
    {
      +treeboPointsUsed > 0 ? (
        <Row between middle className="bhdp__pb-field--pd">
          <Col size="8">
            <Subheading type="1" tag="div"><b>Treebo Points Applied</b></Subheading>
          </Col>
          <Col className="text-right" size="4">
            - <Price
              price={+treeboPointsUsed}
              bold
              size="regular"
              type="discount"
            />
          </Col>
        </Row>
      ) : null
    }
    {
      +amountPaid > 0 ? (
        <Row between middle className="bhdp__pb-field--pd">
          <Col>
            <Subheading type="1" tag="div">
              <b>Amount Paid</b>
            </Subheading>
          </Col>
          <Col className="text-right">
            - <Price
              price={+treeboPointsUsed > 0 ? +amountPaidViaGateway : +amountPaid}
              bold
              size="regular"
              type="discount"
            />
          </Col>
        </Row>
      ) : null
    }
    <Row between middle className="bhdp__pb-field--pd">
      <Col>
        <Subheading type="1" tag="div">
          <b>Total Payable</b>
        </Subheading>
      </Col>
      <Col className="text-right">
        <Price price={+amountPaidViaGateway} bold size="regular" />
      </Col>
    </Row>
    {
      ratePlan && ratePlan.code ? (
        <Row end>
          <RatePlanLabel
            tag={ratePlan.tag}
            type={ratePlan.type}
          />
        </Row>
      ) : null
    }
  </div>
);

BookingHistoryDetailsPriceBreakup.propTypes = {
  price: PropTypes.shape({
    basePrice: PropTypes.string,
    tax: PropTypes.string,
    totalPrice: PropTypes.string,
    pendingAmount: PropTypes.string,
    memberDiscount: PropTypes.string,
    treeboPointsUsed: PropTypes.string,
    voucherPrice: PropTypes.string,
    ratePlan: PropTypes.object,
    amountPaidViaGateway: PropTypes.string,
  }).isRequired,
  coupon: PropTypes.shape({
    code: PropTypes.string,
    amount: PropTypes.string,
  }).isRequired,
  amountPaid: PropTypes.string.isRequired,
};

export default BookingHistoryDetailsPriceBreakup;
