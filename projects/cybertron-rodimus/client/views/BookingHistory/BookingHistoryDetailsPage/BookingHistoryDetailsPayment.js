import React from 'react';
import PropTypes from 'prop-types';
import Price from '../../../components/Price/Price';
import { Row, Col } from '../../../components/Flex/Flex';
import RatePlanLabel from '../../../components/RatePlanLabel/RatePlanLabel';
import { Heading, Subheading, Text } from '../../../components/Typography/Typography';

const BookingHistoryDetailsPayment = ({
  bookingStatus,
  isPayAtHotel,
  isPastBooking,
  amountPaid,
  price,
  price: { totalPrice, amountPaidViaGateway },
  showPriceBreakup,
}) => {
  const renderPriceBreakup = (
    <Row between middle>
      <Col size="7">
        <Text type="2" tag="span">Tax Inclusive:</Text>
        <Text
          tag="span"
          type="2"
          modifier="link"
          onClick={showPriceBreakup}
        >
          View Breakup
        </Text>
      </Col>
      <Col size="5" className="text-right">
        {
          price && price.ratePlan && price.ratePlan.code ? (
            <Row end>
              <RatePlanLabel
                tag={price.ratePlan.tag}
                type={price.ratePlan.type}
              />
            </Row>
          ) : null
        }
      </Col>
    </Row>
  );

  if (bookingStatus === 'cancelled' || isPastBooking) {
    return (
      <div className="bhdp__payment-container">
        <Row middle between>
          <Col size="5">
            <Text type="2">Total</Text>
          </Col>
          <Col size="5" className="text-right">
            <Heading>
              <Price
                price={totalPrice}
                type="default"
                size="large"
              />
            </Heading>
          </Col>
        </Row>
        {renderPriceBreakup}
      </div>
    );
  } else if (bookingStatus === 'confirmed' && !isPayAtHotel) {
    return (
      <div className="bhdp__payment-container">
        <Row middle between>
          <Col size="5">
            <Text type="2">Amount Paid</Text>
          </Col>
          <Col size="5" className="text-right">
            <Heading>
              <Price
                price={amountPaidViaGateway}
                type="default"
              />
            </Heading>
          </Col>
        </Row>
        {renderPriceBreakup}
      </div>
    );
  }
  // if the booking isn't confirmed or cancelled, its either part-pay or temp
  return (
    <div className="bhdp__payment-container">
      {
        isPayAtHotel ? (
          <div>
            <Row middle between className="bhdp__payment-detail">
              <Col size="5">
                <Text type="2">Total Amount</Text>
              </Col>
              <Col size="5" className="text-right">
                <Subheading type="1" tag="p">
                  <Price
                    price={totalPrice}
                    type="default"
                  />
                </Subheading>
              </Col>
            </Row>
            <Row middle between className="bhdp__payment-detail">
              <Col size="5">
                <Text type="2">Amount Paid</Text>
              </Col>
              <Col size="5" className="text-right">
                <Subheading type="1" tag="p">
                  <Price
                    price={amountPaid}
                    type="default"
                  />
                </Subheading>
              </Col>
            </Row>
          </div>
        ) : null
      }
      <Row middle between className="bhdp__payment-detail">
        <Col size="5">
          <Text type="2">Payable at Hotel</Text>
        </Col>
        <Col size="5" className="text-right">
          <Heading
            preview={!totalPrice}
            previewStyle={{ width: '50%' }}
          >
            <Price
              price={totalPrice - amountPaid}
              type="default"
              size="large"
            />
          </Heading>
        </Col>
      </Row>
      {renderPriceBreakup}
    </div>
  );
};

BookingHistoryDetailsPayment.propTypes = {
  bookingStatus: PropTypes.string.isRequired,
  isPayAtHotel: PropTypes.bool.isRequired,
  isPastBooking: PropTypes.bool.isRequired,
  amountPaid: PropTypes.string.isRequired,
  price: PropTypes.object.isRequired,
  showPriceBreakup: PropTypes.func.isRequired,
};

export default BookingHistoryDetailsPayment;
