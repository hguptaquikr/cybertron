import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('BookingHistoryCancellationPage');
    return import('./BookingHistoryCancellationPage' /* webpackChunkName: 'BookingHistoryCancellationPage' */);
  },
  loading: () => null,
});
