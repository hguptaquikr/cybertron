import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Formsy from 'formsy-react';
import PageHeader from '../../../components/PageHeader/PageHeader';
import { Heading, Subheading } from '../../../components/Typography/Typography';
import RadioGroup from '../../../components/RadioGroup/RadioGroup';
import Input from '../../../components/Input/Input';
import NotificationModal from '../../../components/NotificationModal/NotificationModal';
import Button from '../../../components/Button/Button';
import * as contentActionCreators from '../../../services/content/contentDuck';
import * as authActionCreators from '../../../services/auth/authDuck';
import analyticsService from '../../../services/analytics/analyticsService';
import './bookingHistoryCancellationPage.css';

class BookingHistoryCancellationPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(authActionCreators.initiateCancellation(match.params.cancellationHash)));
    return Promise.all(promises);
  }

  state = {
    submittable: false,
    showMessageInput: false,
    showCancellationForm: true,
  }

  componentDidMount() {
    const { contentActions, authActions, route, match } = this.props;
    contentActions.getSeoContent(route, match.params);
    authActions.initiateCancellation(match.params.cancellationHash);
  }

  toggleSubmittable = (submittable) => () => {
    this.setState({ submittable });
  }

  cancelBooking = ({ reason, message }) => {
    const {
      match: { params: { cancellationHash } },
      bookingDetails: { bookingId, cancellation },
      authActions,
    } = this.props;
    const selectedReason = cancellation.reasons[reason - 1];
    authActions.confirmCancellation(
      cancellationHash,
      bookingId,
      reason,
      reason === 'other' ? message : '',
    ).then(({ payload }) =>
      analyticsService.bookingCancellationSuccess(payload.bookingId, selectedReason));
  }

  reasonChanged = ({ reason }) => {
    const { bookingDetails: { cancellation: { reasons } } } = this.props;
    if (reason === reasons[reasons.length - 1].value) this.setState({ showMessageInput: true });
    else this.setState({ showMessageInput: false });
  }

  renderLoading = () => (
    <div>
      <Heading preview previewStyle={{ width: '100%', margin: '10px 0' }} />
      <Subheading className="bhcp__message" preview previewStyle={{ width: '87%' }} />
      {
        [1, 2, 3, 4, 5].map(() => (
          <Subheading preview previewStyle={{ width: '75%', margin: '20px 0' }} />
        ))
      }
    </div>
  );

  renderCancellation = () => {
    const { showCancellationForm } = this.state;
    const { bookingDetails: { cancellation } } = this.props;
    return (
      <div>
        {
          cancellation.status === 'RESERVED' && showCancellationForm ? (
            this.renderCancellationForm()
          ) : (
            this.renderCancellationConfirmation()
          )
        }
      </div>
    );
  }

  renderCancellationForm = () => {
    const { showMessageInput, submittable } = this.state;
    const { bookingDetails: { cancellation } } = this.props;

    return (
      <div className="bhcp">
        <Heading tag="p" className="bhcp__message">
          Please Select a reason for your cancellation
        </Heading>
        <div className="bhcp__reasons-container">
          <Formsy
            onChange={this.reasonChanged}
            onValidSubmit={this.cancelBooking}
            onValid={this.toggleSubmittable(true)}
            onInvalid={this.toggleSubmittable(false)}
          >
            <RadioGroup
              name="reason"
              options={cancellation.reasons}
              containerClassName="bhcp__reason"
              required
            />
            {
              showMessageInput ? (
                <Input
                  type="textarea"
                  name="message"
                  placeholder="Give us your reason for cancelling..."
                  required={showMessageInput}
                />
              ) : null
            }
            <Button
              flat
              block
              large
              className="floaty"
              type="submit"
              disabled={!submittable}
            >
              Proceed with Cancellation
            </Button>
          </Formsy>
        </div>
      </div>
    );
  }

  renderCancellationConfirmation = () => {
    const { history, bookingDetails: { cancellation } } = this.props;
    return (
      <div>
        <NotificationModal
          show
          title={cancellation.message}
          imageUrl="//images.treebohotels.com/images/booking_cancel_expire.png"
          redirectUrl={{ pathname: '/' }}
          onClose={() => { history.push('/'); }}
        />
      </div>
    );
  }

  render() {
    const { bookingDetails } = this.props;
    return (
      <div>
        <PageHeader>
          <Heading>Cancellation</Heading>
        </PageHeader>
        <div className="bhcp__container">
          {
            !bookingDetails.cancellation.status ? (
              this.renderLoading()
            ) : (
              this.renderCancellation()
            )
          }
        </div>
      </div>
    );
  }
}

BookingHistoryCancellationPage.propTypes = {
  route: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  authActions: PropTypes.object.isRequired,
  bookingDetails: PropTypes.object.isRequired,
};

const mapStateToProps = (state, { match }) => ({
  bookingDetails: Object.values(state.auth.bookingHistory.results)
    .find((booking) => booking.cancellation.hash === match.params.cancellationHash)
    || state.auth.bookingHistory.results[0],
});

const mapDispatchToProps = (dispatch) => ({
  authActions: bindActionCreators(authActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BookingHistoryCancellationPage);
