import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'react-router-dom';
import { Subheading, Text } from '../../../../components/Typography/Typography';
import Image from '../../../../components/Image/Image';
import Price from '../../../../components/Price/Price';
import Ripple from '../../../../components/Ripple/Ripple';
import { Row, Col } from '../../../../components/Flex/Flex';
import { getMapsUrl } from '../../../../services/utils';
import './bookingHistoryListItem.css';

const BookingHistoryItem = ({ bookingDetails }) => {
  const {
    status,
    id,
    hotel,
    dates,
    partPayAmount,
    partPayLink,
    isPastBooking,
  } = bookingDetails;
  const { coordinates } = hotel;

  let bookingStatus = 'cancelled';
  if (partPayLink && status === 'RESERVED') {
    bookingStatus = 'temporary';
  } else if (status === 'RESERVED') {
    bookingStatus = 'confirmed';
  }

  return (
    <Ripple>
      <Link to={`/account/booking-history/${id}/`}>
        <div className="bhli">
          <div
            className={cx('bhli__image', {
              'bhli__image--grayscale': isPastBooking,
              'bhli__image--preview': !id,
            })}
            style={{ backgroundImage: `url(${Image.getCdnUrl(hotel.imageUrl)})` }}
          >
            <Text
              className={cx(`bhli__status bhli__status bhli__status--${bookingStatus}`, {
                'bhli__status--past': isPastBooking,
              })}
              preview={!id}
              previewStyle={{ visibility: 'hidden' }}
            >
              {
                bookingStatus === 'temporary' ? (
                  'Payment Pending'
                ) : (
                  `${bookingStatus} booking`
                )
              }
            </Text>
          </div>
          <div className="bhli__details">
            <Row baseline>
              <Col size="8">
                <Subheading
                  className="bhli__name"
                  preview={!hotel.name}
                  previewStyle={{ width: '100%' }}
                >
                  {hotel.name}
                </Subheading>
              </Col>
              <div className="text-right">
                {
                  !isPastBooking && coordinates.lat && coordinates.lng ? (
                    <a
                      href={getMapsUrl(coordinates.lat, coordinates.lng)}
                      target="_blank"
                      rel="noopener noreferrer"
                      onClick={(e) => e.stopPropagation()}
                    >
                      <Text
                        className="text-right bhli__link"
                        modifier="link"
                        tag="div"
                      >
                        Get Directions
                      </Text>
                    </a>
                  ) : null
                }
              </div>
            </Row>
            <Subheading className="bhli__description" tag="p">{hotel.city}</Subheading>
            <Row className="bhli__dates-container">
              <Col size="4">
                <Text type="2">Check in</Text>
                <Subheading
                  className="bhli__date"
                  tag="p"
                  preview={!dates.checkIn}
                  previewStyle={{ width: '100%' }}
                >
                  {dates.checkIn}
                </Subheading>
              </Col>
              <Col size="8">
                <Text type="2">Check out</Text>
                <Subheading
                  className="bhli__date"
                  tag="p"
                  preview={!dates.checkOut}
                  previewStyle={{ width: '50%' }}
                >
                  {dates.checkOut}
                </Subheading>
              </Col>
            </Row>
          </div>
          {
            (partPayAmount && bookingStatus !== 'past') ? (
              <div className="bhli__info-box">
                <div className=" text--center bhli__info-container">
                  <Text className="bhli__info" tag="div">
                    Pay &nbsp;
                    <Price
                      type="default"
                      size="small"
                      price={+partPayAmount}
                    />
                    &nbsp; to confirm booking
                  </Text>
                  <a
                    href={partPayLink}
                    target="_blank"
                    rel="noopener noreferrer"
                    onClick={(e) => e.stopPropagation()}
                  >
                    <Text modifier="link" tag="div">Pay Now</Text>
                  </a>
                </div>
              </div>
            ) : null
          }
        </div>
      </Link>
    </Ripple>
  );
};

BookingHistoryItem.propTypes = {
  bookingDetails: PropTypes.object.isRequired,
};

export default BookingHistoryItem;
