import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import PageHeader from '../../../components/PageHeader/PageHeader';
import { Heading, Subheading } from '../../../components/Typography/Typography';
import BookingHistoryListItem from './BookingHistoryListItem/BookingHistoryListItem';
import BookingHistoryDetailsPage from '../BookingHistoryDetailsPage';
import * as contentActionCreators from '../../../services/content/contentDuck';
import * as authActionCreators from '../../../services/auth/authDuck';
import { safeKeys } from '../../../services/utils';
import './bookingHistoryListPage.css';

class BookingHistoryListPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    return Promise.all(promises);
  }

  componentDidMount() {
    const { authActions, contentActions, route, match } = this.props;
    contentActions.getSeoContent(route, match.params);
    authActions.getBookingHistory();
    BookingHistoryDetailsPage.preload();
  }

  renderBookingHistoryListLoading = () => {
    const { bookingHistory: { results } } = this.props;
    return Array.from(Array(2)).map(() => (
      <BookingHistoryListItem bookingDetails={results[0]} />
    ));
  }

  renderBookingHistoryList = () => {
    const {
      bookingHistory: {
        pastSortOrder,
        upcomingSortOrder,
        results,
      },
    } = this.props;

    return (
      <div>
        {
          upcomingSortOrder.length ? (
            <div>
              <Subheading className="bhlp__section-heading">Upcoming Bookings</Subheading>
              {
                upcomingSortOrder.map(
                  (id) => <BookingHistoryListItem bookingDetails={results[id]} />,
                )
              }
            </div>
          ) : null
        }
        {
          pastSortOrder.length ? (
            <div>
              <Subheading className="bhlp__section-heading">Past Bookings</Subheading>
              {
                pastSortOrder.map(
                  (id) => <BookingHistoryListItem bookingDetails={results[id]} />,
                )
              }
            </div>
          ) : null
        }
        {
          isEmpty(safeKeys(results)) ? (
            <Subheading className="bhlp__section-heading">You have no bookings</Subheading>
          ) : null
        }
      </div>
    );
  }

  render() {
    const {
      bookingHistory: {
        isBookingHistoryLoading,
      },
    } = this.props;

    return (
      <div className="bhlp">
        <PageHeader>
          <Heading>Booking History</Heading>
        </PageHeader>
        {
          isBookingHistoryLoading ? (
            this.renderBookingHistoryListLoading()
          ) : (
            this.renderBookingHistoryList()
          )
        }
      </div>
    );
  }
}

BookingHistoryListPage.propTypes = {
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  authActions: PropTypes.object.isRequired,
  bookingHistory: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  bookingHistory: state.auth.bookingHistory,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  authActions: bindActionCreators(authActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BookingHistoryListPage);
