import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('BookingHistoryListPage');
    return import('./BookingHistoryListPage' /* webpackChunkName: 'BookingHistoryListPage' */);
  },
  loading: () => null,
});
