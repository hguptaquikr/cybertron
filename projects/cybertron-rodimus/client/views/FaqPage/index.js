import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('FaqPage');
    return import('./FaqPage' /* webpackChunkName: 'FaqPage' */);
  },
  loading: () => null,
});
