import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import startCase from 'lodash/startCase';
import PageHeader from '../../components/PageHeader/PageHeader';
import TapSection from '../../components/TapSection/TapSection';
import Modal from '../../components/Modal/Modal';
import { Heading } from '../../components/Typography/Typography';
import * as contentActionCreators from '../../services/content/contentDuck';
import FaqContent from './FaqContent/FaqContent';
import './faqPage.css';

class FaqPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(contentActionCreators.getFaqContent()));
    return Promise.all(promises);
  }

  state = {
    selectedFaq: null,
  }

  componentDidMount() {
    const { contentActions, route, match } = this.props;
    contentActions.getSeoContent(route, match.params);
    contentActions.getFaqContent();
  }

  setSelectedFaq = (index) => () => {
    this.setState({
      selectedFaq: index,
    });
  }

  render() {
    const { selectedFaq } = this.state;
    const { content: { faq } } = this.props;

    return (
      <div>
        <PageHeader>
          <Heading>FAQs</Heading>
        </PageHeader>
        <ul>
          {
            faq.map((value, index) => (
              <TapSection
                key={value.sideTitle}
                title={startCase(value.sideTitle.toLowerCase())}
                onClick={this.setSelectedFaq(index)}
                border="top"
                ripple
              />
            ))
          }
        </ul>
        {
          !isEmpty(faq[selectedFaq]) ? (
            <Modal
              isOpen={selectedFaq !== null}
              closeIcon="back"
              label="faqModal"
              header={startCase(faq[selectedFaq].sideTitle.toLowerCase())}
              onClose={this.setSelectedFaq(null)}
              headerClassName="heading-1"
            >
              <FaqContent faqContent={faq[selectedFaq]} />
            </Modal>
          ) : null
        }
      </div>
    );
  }
}

FaqPage.propTypes = {
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  content: PropTypes.object,
  contentActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FaqPage);
