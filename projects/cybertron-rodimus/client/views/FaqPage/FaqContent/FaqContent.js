import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import startCase from 'lodash/startCase';
import { Text, Subheading } from '../../../components/Typography/Typography';

class FaqItem extends Component {
  constructContent = (faqContent, title, hideQATitle) => {
    if (isEmpty(faqContent)) {
      return null;
    }

    return faqContent.type === 'html'
      ? this.renderHtml(faqContent.data, title)
      : this.renderQa(faqContent.data, title, hideQATitle);
  }

  renderQa = (qas, title, hideQATitle) => qas.map((qa, index) => (
    <div className="faq__container">
      { index === 0 && !hideQATitle ? (<Subheading className="faq__title" type="1">{startCase(title.toLowerCase())}</Subheading>) : null }
      <Subheading className="faq__ques" dangerouslySetInnerHTML={{ __html: qa.q }} />
      <Text className="faq__ans" type="3" dangerouslySetInnerHTML={{ __html: qa.a }} />
    </div>
  )).filter(Boolean);

  renderHtml = (htmlContent, title) => (
    htmlContent ? (
      <div className="faq__container">
        <Subheading className="faq__ques">{startCase(title.toLowerCase())}</Subheading>
        <Text className="faq__ans" type="3" dangerouslySetInnerHTML={{ __html: htmlContent }} />
      </div>
    ) : null
  )

  render() {
    const {
      faqContent: {
        content = [],
        subcontent = [],
        mainTitle,
      },
    } = this.props;

    return (
      <div className="faq">
        {
          content
            .map((mainContQA) => this.constructContent(mainContQA, mainTitle, true))
            .filter(Boolean)
        }
        {
          subcontent
            .map((subCont) =>
              subCont.content.map((subContQA) =>
                this.constructContent(subContQA, subCont.title)))
            .filter(Boolean)
        }
      </div>
    );
  }
}

FaqItem.propTypes = {
  faqContent: PropTypes.object,
};

export default FaqItem;
