import React from 'react';
import { Row, Col } from '../../../components/Flex/Flex';
import { Text, Heading } from '../../../components/Typography/Typography';
import './assuredAmenities.css';

const amenities = [
  { name: 'WiFi', className: 'amenity-wifi' },
  { name: 'Free Breakfast', className: 'amenity-breakfast' },
  { name: 'AC*', className: 'amenity-ac' },
  { name: 'TV', className: 'amenity-tv' },
  { name: 'Branded Toiletries', className: 'amenity-toiletries' },
];

const AssuredAmenities = () => (
  <div className="ssrp__aa" id="t-assured-amenities">
    <Heading tag="h2" type="3" className="ssrp__aa__title">ASSURED ESSENTIALS</Heading>
    <Text className="ssrp__aa__subtitle">Gauranteed at all our hotels</Text>
    <Row between className="ssrp__aa__list">
      {
        amenities.map(({ className, name }) => (
          <Col key={className} className="ssrp__aa__item">
            <i className={`ssrp__aa__icon icon-${className}`} />
            {name}
          </Col>
        ))
      }
    </Row>
    <Text className="ssrp__aa__info">*AC not available in hill stations</Text>
  </div>
);

export default AssuredAmenities;
