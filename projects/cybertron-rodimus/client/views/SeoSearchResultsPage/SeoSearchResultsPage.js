import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import qs from 'query-string';
import startCase from 'lodash/startCase';
import Size from 'leaf-ui/cjs/Size/web';
import Image from 'leaf-ui/cjs/Image/web';
import Gallery from 'leaf-ui/cjs/Gallery/web';
import Space from 'leaf-ui/cjs/Space/web';
import Tag from 'leaf-ui/cjs/Tag/web';
import View from 'leaf-ui/cjs/View/web';
import Ripple from '../../components/Ripple/Ripple';
import { Row, Col } from '../../components/Flex/Flex';
import { Heading, Text } from '../../components/Typography/Typography';
import QuestionAndAnswer from '../../views/QuestionAndAnswer/QuestionAndAnswer';
import PerfectStayBanner from '../../components/PerfectStayBanner/PerfectStayBanner';
import SearchResultsRefine from '../SearchResultsPage/SearchResultsRefine/SearchResultsRefine';
import SearchWidgetAsync from '../SearchWidget/SearchWidgetAsync';
import HotelDetailsPage from './../HotelDetailsPage';
import AssuredAmenities from './AssuredAmenities/AssuredAmenities';
import PlaceSummary from './PlaceSummary/PlaceSummary';
import JointlyCollectedReviews from '../ReviewsAndRatings/JointlyCollectedReviews/JointlyCollectedReviews';
import SearchResultsItem from '../SearchResultsPage/SearchResultsItem/SearchResultsItem';
import Seo from '../../components/Seo/Seo';
import SearchResultsHeader from './SearchResultsHeader/SearchResultsHeader';
import SearchHeader from '../../components/SearchHeader/SearchHeader';
import ErrorBoundary from '../../components/ErrorBoundary/ErrorBoundary';
import SearchResultsFooter from './SearchResultsFooter/SearchResultsFooter';
import Blogs from './Blogs/Blogs';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as hotelActionCreators from '../../services/hotel/hotelDuck';
import * as filterActionCreators from '../../services/filter/filterDuck';
import * as priceActionCreators from '../../services/price/priceDuck';
import * as toastActionCreators from '../../services/toast/toastDuck';
import * as walletActionCreators from '../../services/wallet/walletDuck';
import * as hotelService from '../../services/hotel/hotelService';
import * as uiActionCreators from '../../services/ui/uiDuck';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as abService from '../../services/ab/abService';
import filterService from '../../services/filter/filterService';
import priceService from '../../services/price/priceService';
import searchService from '../../services/search/searchService';
import analyticsService from '../../services/analytics/analyticsService';
import { constants } from '../../services/utils';
import './seoSearchResultsPage.css';

class SeoSearchResultsPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match, req }) => {
    const promises = [];
    dispatch(searchActionCreators.setSearchState({ ...req.query, ...match.params }));
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(hotelActionCreators.getHotelResults(req.query, match.params))
      .then(() => dispatch(hotelActionCreators.addDistanceToResults()))
      .then(() => dispatch(filterActionCreators.refineResults())));
    return Promise.all(promises);
  }

  state = {
    showRefineResultsModal: false || qs.parse(this.props.location.search).modal === 'refine-modal',
  };

  componentDidMount() {
    const {
      contentActions,
      searchActions,
      walletActions,
      location,
      match,
      auth,
      route,
    } = this.props;

    searchActions.setSearchState({ ...qs.parse(location.search), ...match.params });
    contentActions.getSeoContent(route, match.params);
    if (auth.isAuthenticated) walletActions.getWalletBalance();
    this.getHotelResults(this.props);
    contentActions.getAdsBannerContent();
    this.getGeoLocation();
    SearchWidgetAsync.preload();
    HotelDetailsPage.preload();
  }

  componentWillReceiveProps(nextProps) {
    const { props } = this;
    const { searchActions } = this.props;
    const nextQuery = qs.parse(nextProps.location.search);

    if (
      props.match.params.city !== nextProps.match.params.city
        || props.match.params.locality !== nextProps.match.params.locality
        || props.match.params.landmark !== nextProps.match.params.landmark
        || props.match.params.category !== nextProps.match.params.category
    ) {
      searchActions.setSearchState({ ...nextQuery, ...nextProps.match.params });
      nextProps.contentActions.getSeoContent(nextProps.route, nextProps.match.params);
      this.getHotelResults(nextProps);
      this.getGeoLocation();
    }
  }

  componentWillUnmount() {
    const { filterActions } = this.props;
    filterActions.clearAllFilters();
  }

  onSearchResultsItemClicked =
    (index, hotelId, noOfRefinedResults) =>
      (cheapestPrice, cheapestRatePlan, isQuickBook) => {
        const {
          location,
          search: {
            searchInput,
          },
          $roomType,
          content: { psychologicalTriggers },
        } = this.props;
        const psychologicalTrigger = psychologicalTriggers.byRoomTypeId[cheapestPrice.id];
        const analyticsProperties = {
          hotelId,
          cheapestPrice,
          cheapestRatePlan,
          $roomType,
          search_rank: index + 1,
          destination: searchInput.place.q,
          num_results: noOfRefinedResults,
          page_url: `${location.pathname}${location.search}`,
          trigger_displayed: !isEmpty(psychologicalTrigger),
          trigger_type: !isEmpty(psychologicalTrigger) ? psychologicalTrigger.type : '',
          flow: 'browse',
        };
        if (isQuickBook) analyticsService.quickBookClicked(analyticsProperties);
        else analyticsService.hotelViewDetailsClicked(analyticsProperties);
      };

  getSearchPrices = (hotelIds) => {
    const {
      searchActions,
      priceActions,
      filterActions,
      contentActions,
    } = this.props;
    if (searchActions.isSearchWidgetValid() && !isEmpty(hotelIds)) {
      priceActions.getSearchPrices(hotelIds)
        .then((res) => {
          this.showAutoAppliedCouponToast(res);
          filterActions.refineResults();
          filterActions.constructPriceRangesFilter();
          contentActions.getPsychologicalTriggers(res.payload.hotel.ids);
          this.captureAnalytics();
        });
    }
  }

  getGeoLocation = async () => {
    const { match } = this.props;
    if (match.params.landmark === 'me') {
      const res = await searchService.getIpLocation();
      filterService.sortByNearMe(this.props, res.city);
    }
  }

  getHotelResults = (props = this.props) => {
    const { hotelActions, filterActions, location, match } = props;
    filterActions.clearAllFilters();
    hotelActions.getHotelResults(qs.parse(location.search), match.params)
      .then((res) => {
        hotelActions.addDistanceToResults();
        this.getSearchPrices(res.payload.ids);
        filterActions.constructLocalitiesFilter();
      });
  }

  showAutoAppliedCouponToast = ({ payload = { price: {} } }) => {
    const { toastActions } = this.props;
    const couponCode = priceService.getAutoAppliedCoupon(payload.price.byRoomTypeId);
    if (couponCode) toastActions.showToast(`${couponCode} pre applied`, 'capsular');
  }

  isMemberDiscountAvailable = () => {
    const { price } = this.props;
    return Object.values(price.byRoomTypeId)
      .some((priceObj) => priceObj.memberDiscount.isAvailable);
  }

  isCoupleFriendlyHotelAvailable = (hotels) =>
    hotels.ids.some((hotelId) => {
      const hotel = hotels.byId[hotelId];
      return hotelService.isCoupleFriendly(hotel.policies);
    });

  showRefineResultsModal = (status) => {
    this.setState({ showRefineResultsModal: status });
  };

  captureAnalytics = () => {
    const {
      hotel,
      search: {
        roomConfig,
        datePicker: {
          range,
        },
      },
      location,
      match,
    } = this.props;

    const query = qs.parse(location.search);
    const { adults, kids, rooms } =
      searchService.formattedRoomConfig(roomConfig.rooms);
    const { checkIn, checkOut, numberOfNights, abwDuration } =
      searchService.formattedDatePicker(range, constants.dateFormat.query);

    analyticsService.listContentViewed({
      flow: 'browse',
      destination: match.params.city || query.q,
      resultCount: hotel.ids.length,
      checkin: checkIn,
      checkout: checkOut,
      lengthOfStay: numberOfNights.count,
      adults: adults.count,
      kids: kids.count,
      rooms: rooms.count,
      abwDays: abwDuration,
      ...abService.impressionProperties(['quickBookCTAMobile', 'perfectStayBanner']),
    });
  }

  renderAdsBanner = () => {
    const { content } = this.props;
    return !isEmpty(content.adsBanner) ? (
      <div className="sri" itemScope>
        <div className="sri__link">
          <Size width="100%" height="150px">
            <Gallery>
              {
                content.adsBanner.map((image, index) => (
                  <Link to={image.urlRedirect} key={`${image}${index + 1}`}>
                    <Image
                      src={image.url}
                      alt={image.alt}
                      width="200px"
                      height="150px"
                    />
                  </Link>
                ))
              }
            </Gallery>
          </Size>
        </div>
      </div>
    ) : null;
  }

  renderResults = () => {
    const {
      hotel,
      price,
      filter: { refinedHotelIds },
      $roomType,
      location,
      search,
      match,
    } = this.props;
    const noOfTotalResults = hotel.ids.length;
    const noOfRefinedResults = refinedHotelIds.length;

    if (hotel.isHotelResultsLoading) {
      return [0, 1, 2, 3].map((key) => (
        <SearchResultsItem
          key={key}
          hotel={hotel.byId[0]}
          price={price}
          $roomType={$roomType}
          search={search}
          location={location}
        />
      ));
    } else if (noOfRefinedResults || price.isPricesLoading) {
      return refinedHotelIds.map((hotelId, index) => (
        <SearchResultsItem
          key={hotelId}
          hotel={hotel.byId[hotelId]}
          $roomType={$roomType}
          price={price}
          search={search}
          location={location}
          isSimilarTreebo={false}
          onClick={this.onSearchResultsItemClicked(index, hotelId, noOfRefinedResults)}
        />
      ));
    } else if (
      !price.isPricesLoading
        && !hotel.isHotelResultsLoading
        && !noOfRefinedResults
        && match.params.landmark !== 'me'
    ) {
      return (
        <div>
          <Helmet meta={[{ name: 'robots', content: 'noindex, follow' }]} />
          {this.renderNoResultsText(noOfTotalResults)}
        </div>
      );
    }
    return null;
  }

  renderResultsWithAds = () => {
    const { filter: { refinedHotelIds } } = this.props;
    const noOfRefinedResults = refinedHotelIds.length;

    return noOfRefinedResults <= 3 ? (
      <div>
        {this.renderResults()}
        {this.renderAdsBanner()}
      </div>
    ) : (
      React.Children.map(this.renderResults(), (result, index) => (
        index === 3 ? (
          <div>
            {result}
            {this.renderAdsBanner()}
          </div>
        ) : result
      ))
    );
  }

  renderNoResultsText = (noOfTotalResults) => (
    <div className="ssrp__no-results">
      <Heading className="ssrp__no-results__head">Sorry</Heading>
      <Heading className="ssrp__no-results__title" type="3">
        No results found matching your {noOfTotalResults ? 'filters' : 'search'}!
      </Heading>
    </div>
  );

  render() {
    const { showRefineResultsModal } = this.state;
    const {
      hotel,
      price,
      filter: { sort },
      content: { seo },
      search,
      location,
      route,
      match,
    } = this.props;
    const noOfTotalResults = hotel.ids.length;
    const isCoupleFriendlyHotelAvailable = this.isCoupleFriendlyHotelAvailable(hotel);
    const isPerfectStayBannerOpen = get(abService.getExperiments(), 'perfectStayBanner.variant') === 'open';
    return (
      <div className="ssrp" id="t-ssrp">
        <SearchHeader />
        <SearchResultsHeader
          heading={startCase(location.pathname)}
          minPrice={seo.content.minPrice}
          search={search}
        />
        <div className="ssrp__content" id="t-ssrp-content">
          <Space padding={[2]}>
            <View>
              <PerfectStayBanner isOpen={isPerfectStayBannerOpen} />
            </View>
          </Space>
          <Seo index={match.params.landmark !== 'me'}>
            {this.renderResultsWithAds()}
          </Seo>
          <AssuredAmenities />
          <ErrorBoundary render={() => null}>
            {
              !isEmpty(seo.content.description) ? (
                <div className="ssrp__city-summary ssrp__section" id="t-city-name">
                  <Heading tag="p" type="3" className="ssrp__section-heading">
                    {seo.content.description.title}
                  </Heading>
                  <PlaceSummary
                    description={seo.content.description}
                  />
                </div>
              ) : null
            }
          </ErrorBoundary>
          <ErrorBoundary render={() => null}>
            {
              !isEmpty(seo.content.blogs) ? (
                <div className="ssrp__blogs ssrp__section" id="t-ssrp-blogs">
                  <Heading tag="h2" type="3" className="ssrp__section-heading">
                    {seo.content.blogs.title}
                  </Heading>
                  <Blogs blogs={seo.content.blogs.blogsList} />
                </div>
              ) : null
            }
          </ErrorBoundary>
          <ErrorBoundary render={() => null}>
            {
              !isEmpty(seo.content.reviews) ? (
                <div className="ssrp__reviews ssrp__section" id="t-ssrp-reviews">
                  <Heading tag="h2" type="3" className="ssrp__section-heading">
                    {seo.content.reviews.title}
                  </Heading>
                  <JointlyCollectedReviews
                    hotels={hotel.byId}
                    price={price}
                    reviews={seo.content.reviews.reviewsList}
                    location={location}
                  />
                </div>
              ) : null
            }
          </ErrorBoundary>
          <ErrorBoundary render={() => null}>
            {
              !isEmpty(seo.content.qna) ? (
                <div className="ssrp__reviews ssrp__section" id="t-ssrp-qna">
                  <Heading tag="h2" type="3" className="ssrp__section-heading">
                    {seo.content.qna.title}
                  </Heading>
                  <QuestionAndAnswer qna={seo.content.qna.content} />
                </div>

              ) : null
            }
          </ErrorBoundary>
          <ErrorBoundary render={() => null}>
            {
              !isEmpty(seo.content.links) ? (
                <SearchResultsFooter
                  links={seo.content.links}
                />
              ) : null
            }
          </ErrorBoundary>
        </div>
        {
          noOfTotalResults >= 3 ? (
            <div>
              <Ripple>
                <Row
                  center
                  className="floaty ssrp__filter"
                  onClick={() => this.showRefineResultsModal(true)}
                >
                  <Col size="6" className="ssrp__filter__item"><Text className="ssrp__filter__item__sort">SORTED BY {sort.by}</Text></Col>
                  <Col size="6" className="ssrp__filter__item">
                    <Text className="ssrp__filter__item__filter">
                      FILTERS
                      {
                        isCoupleFriendlyHotelAvailable ? (
                          <Space margin={[0, 0, 0, 0.5]}>
                            <Tag
                              color="yellowLight"
                              shape="capsular"
                              kind="filled"
                              size="small"
                            >
                              New
                            </Tag>
                          </Space>
                        ) : null
                      }
                    </Text>
                  </Col>
                </Row>
              </Ripple>
            </div>
          ) : null
        }
        {
          showRefineResultsModal ? (
            <SearchResultsRefine
              route={route}
              isCoupleFriendlyHotelAvailable={isCoupleFriendlyHotelAvailable}
              onClose={() => this.showRefineResultsModal(false)}
            />
          ) : null
        }
      </div>
    );
  }
}

SeoSearchResultsPage.propTypes = {
  route: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  filterActions: PropTypes.object.isRequired,
  priceActions: PropTypes.object.isRequired,
  toastActions: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  hotel: state.hotel,
  price: state.price,
  filter: state.filter,
  search: state.search,
  content: state.content,
  auth: state.auth,
  $roomType: state.$roomType,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
  filterActions: bindActionCreators(filterActionCreators, dispatch),
  priceActions: bindActionCreators(priceActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
  uiActions: bindActionCreators(uiActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SeoSearchResultsPage);
