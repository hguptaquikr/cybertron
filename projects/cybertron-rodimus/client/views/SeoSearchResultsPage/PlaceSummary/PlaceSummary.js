import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ShowMore from '../../../components/ShowMore/ShowMore';
import { scrollIntoView } from '../../../services/utils';
import './placeSummary.css';

const HtmlDescription = ({ content }) => (
  <div dangerouslySetInnerHTML={{ __html: content }} /> // eslint-disable-line
);

HtmlDescription.propTypes = {
  content: PropTypes.string.isRequired,
};

class PlaceSummary extends Component {
  onShowLess = () => {
    if (this.citySummaryRef) {
      scrollIntoView(this.citySummaryRef, -90);
    }
  };

  storeCitySummaryRef = (ref) => {
    if (ref) this.citySummaryRef = ref;
  };

  render() {
    const { description } = this.props;
    return (
      <div className="ssrp__cs" ref={this.storeCitySummaryRef}>
        <ShowMore
          className="ssrp__cs__description-container"
          initialHeight={160}
          showMoreText="READ MORE"
          showLessText="READ LESS"
          onShowLess={this.onShowLess}
        >
          <HtmlDescription content={description.content} />
        </ShowMore>
      </div>
    );
  }
}

PlaceSummary.propTypes = {
  description: PropTypes.object.isRequired,
};

export default PlaceSummary;
