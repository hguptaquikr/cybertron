import React from 'react';
import PropTypes from 'prop-types';
import { Heading, Subheading, Text } from '../../../components/Typography/Typography';
import './review.css';

const Review = ({ review: { ratingImage, hotelName, title, text, user } }) => (
  <div className="ssrp__review">
    <Subheading className="ssrp__review__hotel ">{hotelName}</Subheading>
    <Heading className="ssrp__review__title" type="3">{title}</Heading>
    <div className="ssrp__review__sub-title">
      <img
        src={ratingImage}
        alt="rating"
        className="old-image ssrp__review__rating-image"
      />
      <Subheading className="ssrp__review__user-name" tag="span">{user}</Subheading>
    </div>
    <Text className="ssrp__review__text">
      “{text}”
    </Text>
  </div>
);

Review.propTypes = {
  review: PropTypes.shape({
    ratingImage: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
  }).isRequired,
};

export default Review;
