import searchService from '../../services/search/searchService';

const makePageName = (url) => url.replace(/\//g, ' ').replace(/-/g, ' ').toUpperCase().trim();
const makeHotelClickEvent = (hotel, name, page, selectorName) =>
  hotel.ids.map((hotelId) => ({
    selector: `#t-${selectorName}-${hotelId}`,
    name,
    properties: {
      hotelId,
      hotelName: hotel.byId[hotelId].name,
      pageType: 'AMP',
      page,
      nonInteraction: 0,
    },
  }));

const analyticsData = ({ store: { getState }, match }) => {
  const {
    search: {
      datePicker: { range },
    },
    hotel,
  } = getState();
  const pageName = makePageName(match.url);
  const { checkIn, checkOut } = searchService.formattedDatePicker(range, 'D MMM');

  return {
    pageEventName: 'AMP Results Page Viewed',
    properties: {
      num_results: hotel.ids.length,
      checkin: checkIn,
      checkout: checkOut,
      pageType: 'AMP',
      page: pageName,
    },
    events: [
      ...makeHotelClickEvent(hotel, 'AMP Results Page Item Clicked', pageName, 'resultItem'),
      ...makeHotelClickEvent(hotel, 'AMP Review HD Link Clicked', pageName, 'reviewHDLink'),
      {
        selector: '#t-ampHome',
        name: 'Home Button Clicked',
        properties: {
          nonInteraction: 0,
        },
      },
      {
        selector: '#t-ampSearchHeader',
        name: 'Search Header Date Clicked',
        properties: {
          nonInteraction: 0,
        },
      },
      {
        selector: '#t-ampSearchHeaderIcon',
        name: 'Search Header Icon Clicked',
        properties: {
          nonInteraction: 0,
        },
      },
      {
        selector: '#t-ampPerfectStayBanner',
        name: 'Perfect Stay Banner Expanded',
        properties: {
          nonInteraction: 0,
        },
      },
      {
        selector: '#t-ampPerfectStayKnowMore',
        name: 'Perfect Stay Know More Clicked',
        properties: {
          nonInteraction: 0,
        },
      },
      {
        selector: '#t-refineResults',
        name: 'Refine Results Clicked',
        properties: {
          nonInteraction: 0,
        },
      },
    ],
  };
};

export default analyticsData;
