import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import startCase from 'lodash/startCase';
import qs from 'query-string';
import Space from 'leaf-ui/cjs/Space/amp';
import View from 'leaf-ui/cjs/View/amp';
import Flex from 'leaf-ui/cjs/Flex/amp';
import Tag from 'leaf-ui/cjs/Tag/amp';
import Text from 'leaf-ui/cjs/Text/amp';
import Position from 'leaf-ui/cjs/Position/amp';
import Size from 'leaf-ui/cjs/Size/amp';
import Card from 'leaf-ui/cjs/Card/amp';
import PerfectStayBanner from '../../components/PerfectStayBanner/PerfectStayBanner.amp';
import JointlyCollectedReviews from '../ReviewsAndRatings/JointlyCollectedReviews/JointlyCollectedReviews.amp';
import SearchResultsItem from '../SearchResultsPage/SearchResultsItem/SearchResultsItem.amp';
import SearchResultsHeader from './SearchResultsHeader/SearchResultsHeader.amp';
import SearchHeader from '../../components/SearchHeader/SearchHeader.amp';
import analyticsData from './SeoSearchResultsPageAnalytics.amp';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as hotelActionCreators from '../../services/hotel/hotelDuck';
import * as filterActionCreators from '../../services/filter/filterDuck';
import * as priceActionCreators from '../../services/price/priceDuck';
import * as walletActionCreators from '../../services/wallet/walletDuck';
import * as hotelService from '../../services/hotel/hotelService';
import * as uiActionCreators from '../../services/ui/uiDuck';
import * as contentActionCreators from '../../services/content/contentDuck';
import config from '../../../config';

class SeoSearchResultsPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match, req }) => {
    const promises = [];
    dispatch(searchActionCreators.setSearchState({ ...req.query, ...match.params }));
    dispatch(uiActionCreators.setSearchParameters(qs.stringify(req.query)));
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(
      dispatch(hotelActionCreators.getHotelResults(req.query, match.params))
        .then((res) => {
          dispatch(hotelActionCreators.addDistanceToResults());
          return dispatch(priceActionCreators.getSearchPrices(res.payload.ids))
            .then(() => dispatch(filterActionCreators.refineResults()));
        }),
    );
    return Promise.all(promises);
  }

  static analytics = (context) => analyticsData(context);

  isCoupleFriendlyHotelAvailable = (hotels) =>
    hotels.ids.some((hotelId) => {
      const hotel = hotels.byId[hotelId];
      return hotelService.isCoupleFriendly(hotel.policies);
    });

  renderResults = () => {
    const {
      hotel,
      price,
      filter: { refinedHotelIds },
      $roomType,
      location,
      search,
      match,
    } = this.props;
    const noOfTotalResults = hotel.ids.length;
    const noOfRefinedResults = refinedHotelIds.length;

    if (hotel.isHotelResultsLoading) {
      return [0, 1, 2, 3].map((key) => (
        <SearchResultsItem
          key={key}
          hotel={hotel.byId[0]}
          price={price}
          $roomType={$roomType}
          search={search}
          location={location}
        />
      ));
    } else if (noOfRefinedResults || price.isPricesLoading) {
      return refinedHotelIds.map((hotelId) => (
        <SearchResultsItem
          key={hotelId}
          hotel={hotel.byId[hotelId]}
          $roomType={$roomType}
          price={price}
          search={search}
          location={location}
          isSimilarTreebo={false}
        />
      ));
    } else if (
      !price.isPricesLoading
        && !hotel.isHotelResultsLoading
        && !noOfRefinedResults
        && match.params.landmark !== 'me'
    ) {
      return (
        <div>
          <Helmet meta={[{ name: 'robots', content: 'noindex, follow' }]} />
          {this.renderNoResultsText(noOfTotalResults)}
        </div>
      );
    }
    return null;
  }

  renderNoResultsText = (noOfTotalResults) => (
    <div>
      <Text
        size="l"
        weight="medium"
        color="greyDarker"
        align="center"
      >
        Sorry
      </Text>
      <Text
        size="m"
        weight="medium"
        color="greyDarker"
        align="center"
      >
        No results found matching your {noOfTotalResults ? 'filters' : 'search'}!
      </Text>
    </div>
  );

  render() {
    const {
      hotel,
      price,
      filter: { sort },
      content: { seo },
      search,
      location,
    } = this.props;
    const noOfTotalResults = hotel.ids.length;
    const isCoupleFriendlyHotelAvailable = this.isCoupleFriendlyHotelAvailable(hotel);
    const refineModalUrl = `${config.hostUrl}${location.pathname.replace('/amp', '')}?${location.search}&modal=refine-modal`;
    return (
      <div id="t-ssrp">
        <SearchHeader />
        <SearchResultsHeader
          heading={startCase(location.pathname).replace('Amp', '')}
          minPrice={seo.content.minPrice}
          search={search}
        />
        <div id="t-ssrp-content">
          <Space margin={[2]}>
            <View>
              <PerfectStayBanner />
            </View>
          </Space>
          {this.renderResults()}
          {
            !isEmpty(seo.content.reviews) ? (
              <React.Fragment>
                <Size height={[1]}>
                  <Card backgroundColor="greyLighter" />
                </Size>
                <Space margin={[3, 2, 9, 2]}>
                  <View id="t-ssrp-reviews">
                    <Text
                      size="l"
                      weight="medium"
                      color="greyDarker"
                    >
                      {seo.content.reviews.title}
                    </Text>
                    <JointlyCollectedReviews
                      hotels={hotel.byId}
                      price={price}
                      reviews={seo.content.reviews.reviewsList}
                      location={location}
                    />
                  </View>
                </Space>
              </React.Fragment>
            ) : null
          }
        </div>
        {
          noOfTotalResults >= 3 ? (
            <Position position="fixed" bottom="0" zIndex="20" className="refine-bar">
              <Size width="100%">
                <Card backgroundColor="white">
                  <a
                    id="t-refineResults"
                    className="link"
                    href={refineModalUrl}
                  >
                    <Flex
                      flexDirection="row"
                      alignItems="center"
                      justifyContent="center"
                    >
                      <View>
                        <Space padding={[1]}>
                          <Size width="50%">
                            <View>
                              <Text size="xxs" color="greyDarker" weight="semibold">SORTED BY {sort.by.toUpperCase()}</Text>
                            </View>
                          </Size>
                        </Space>
                        <View className="seperator" />
                        <Space padding={[1]}>
                          <Size width="40%">
                            <Flex
                              flexDirection="row"
                              justifyContent="center"
                              alignItems="center"
                            >
                              <Text
                                size="xxs"
                                color="greyDarker"
                                weight="semibold"
                              >
                                    FILTERS
                                {
                                  isCoupleFriendlyHotelAvailable ? (
                                    <Space margin={[0, 0, 0, 0.5]}>
                                      <Tag
                                        color="yellowLight"
                                        shape="capsular"
                                        kind="filled"
                                        size="small"
                                      >
                                        New
                                      </Tag>
                                    </Space>
                                  ) : null
                                }
                              </Text>
                            </Flex>
                          </Size>
                        </Space>
                      </View>
                    </Flex>
                  </a>
                </Card>
              </Size>
            </Position>
          ) : null
        }
      </div>
    );
  }
}

SeoSearchResultsPage.propTypes = {
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  hotel: state.hotel,
  price: state.price,
  filter: state.filter,
  search: state.search,
  content: state.content,
  auth: state.auth,
  $roomType: state.$roomType,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
  filterActions: bindActionCreators(filterActionCreators, dispatch),
  priceActions: bindActionCreators(priceActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
  uiActions: bindActionCreators(uiActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SeoSearchResultsPage);
