/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import { Subheading } from '../../../components/Typography/Typography';
import Accordion from '../../../components/Accordion/Accordion';
import contentService from '../../../services/content/contentService';
import './searchResultsFooter.css';

class SearchResultsFooter extends Component {
  state = {
    showMoreDescription: false,
  }

  toggleShowMoreDescription = () => {
    const { showMoreDescription } = this.state;
    this.setState({ showMoreDescription: !showMoreDescription });
  }

  renderLinks = (group, index) => (
    !isEmpty(group.content) ? (
      <Accordion.Section key={`${group.title}${index + 1}`}>
        <Accordion.Title>
          <Subheading tag="h3" type="3" className="ssrf__links__title">
            {group.title}
          </Subheading>
        </Accordion.Title>
        <Accordion.Content>
          {group.content.map(this.renderLink)}
        </Accordion.Content>
      </Accordion.Section>
    ) : null
  )

  renderLink = (place, index) => {
    const url = contentService.getSeoPathName(place);
    let label = place.label.split(',')[0];
    if (place.type === 'city') {
      label = `Hotels in ${label}`;
    } else if (place.type === 'category') {
      label = `${label} in ${get(place, 'area.city')}`;
    }
    return (
      <Link className="ssrf__links__item" to={url} key={`${url}${index + 1}`}>
        {label}
      </Link>
    );
  }

  render() {
    const { links } = this.props;
    return (
      <div className="ssrf">
        {
          !isEmpty(links) ? (
            <Accordion.Container>
              {links.map(this.renderLinks)}
            </Accordion.Container>
          ) : null
        }
      </div>
    );
  }
}

SearchResultsFooter.propTypes = {
  links: PropTypes.array.isRequired,
};

export default SearchResultsFooter;
