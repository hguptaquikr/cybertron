import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('SeoSearchResultsPage');
    return import('./SeoSearchResultsPage' /* webpackChunkName: 'SeoSearchResultsPage' */);
  },
  loading: () => null,
});
