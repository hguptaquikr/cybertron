import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Image from '../../../components/Image/Image';
import { Subheading } from '../../../components/Typography/Typography';
import ShowMore from '../../../components/ShowMore/ShowMore';
import { scrollIntoView } from '../../../services/utils';
import './blogs.css';

class Blogs extends Component {
  onShowLess = () => {
    if (this.blogsRef) {
      scrollIntoView(this.blogsRef, -90);
    }
  };

  storeBlogsRef = (ref) => {
    if (ref) this.blogsRef = ref;
  };

  render() {
    const { blogs } = this.props;
    return (
      <div ref={this.storeBlogsRef}>
        <ShowMore
          className="ssrp__blogs__container"
          onShowLess={this.onShowLess}
          initialHeight={520}
          id="t-ssrp-blogs-container"
        >
          {
            blogs.map(({ blogUrl, name, imageUrl }, index) => (
              <div className="ssrp__blogs__blog" key={`name${index + 1}`} id="t-ssrp-blogs-blog">
                <a href={blogUrl} target="_blank" rel="noopener noreferrer">
                  <Image
                    src={imageUrl}
                    alt={name}
                  />
                  <Subheading className="ssrp__blogs__blog-name" id="t-ssrp-blogs-blog-name">
                    {name}
                  </Subheading>
                </a>
              </div>
            ))
          }
        </ShowMore>
      </div>
    );
  }
}

Blogs.propTypes = {
  blogs: PropTypes.arrayOf(
    PropTypes.shape({
      blogUrl: PropTypes.string.isRequired,
      imageUrl: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }).isRequired,
  ).isRequired,
};

export default Blogs;
