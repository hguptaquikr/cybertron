import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col } from '../../../components/Flex/Flex';
import { Text, Subheading } from '../../../components/Typography/Typography';
import Ripple from '../../../components/Ripple/Ripple';
import searchService from '../../../services/search/searchService';
import * as uiActionCreators from '../../../services/ui/uiDuck';
import './searchHeader.css';

class SearchHeader extends Component {
  onSearchClick = () => {
    const { uiActions } = this.props;
    uiActions.showSearchWidget(true);
  };

  render() {
    const {
      search: {
        searchInput: { place },
        datePicker: { range },
        roomConfig,
      },
    } = this.props;
    const { checkIn, checkOut } = searchService.formattedDatePicker(range, 'D MMM');
    const { adults, kids, rooms } = searchService.formattedRoomConfig(roomConfig.rooms);
    let searchText = place.q || '';
    if (place.area) {
      if (place.area.city) searchText = `All locations, ${place.area.city}`;
      else if (place.area.landmark) searchText = place.area.landmark;
      else if (place.area.locality) searchText = `${place.area.locality}, ${place.area.city}`;
    }

    return (
      <div className="ssh">
        <Ripple>
          <div onClick={this.onSearchClick}>
            <div className="ssh__q-container">
              <i className="icon-search ssh__search-icon" />
              <Subheading className="ssh__q truncate">{searchText}</Subheading>
            </div>
            <Row className="ssh__info-container">
              <Col size="6" className="ssh__info">
                <Text className="ssh__info-header">Stay Dates</Text>
                <Text className="ssh__info-content">{checkIn} - {checkOut}</Text>
              </Col>
              <Col size="6" className="ssh__info">
                <Text className="ssh__info-header">{rooms.text}</Text>
                <Text className="ssh__info-content">
                  {adults.text}
                  {kids.count ? `, ${kids.text}` : ''}
                </Text>
              </Col>
            </Row>
          </div>
        </Ripple>
      </div>
    );
  }
}

SearchHeader.propTypes = {
  search: PropTypes.object.isRequired,
  uiActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

const mapDispatchToProps = (dispatch) => ({
  uiActions: bindActionCreators(uiActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchHeader);
