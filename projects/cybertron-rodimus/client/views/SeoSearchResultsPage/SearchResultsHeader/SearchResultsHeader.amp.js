import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Text from 'leaf-ui/cjs/Text/amp';
import Space from 'leaf-ui/cjs/Space/amp';
import View from 'leaf-ui/cjs/View/amp';

const SearchResultsHeader = ({
  minPrice,
  heading,
  search,
}) => {
  const stayDuration = search.datePicker.range.end ? (
    moment(search.datePicker.range.end).diff(moment(search.datePicker.range.start), 'days')
  ) : '';

  return (
    <Space margin={[2]}>
      <View>
        <Text size="s">
          {heading}
        </Text>
        <Space margin={[1, 0, 0, 0]}>
          <View>
            <Text size="xs" color="grey">
              List of {heading} with tariff
              {minPrice ? <span> - Prices start from &#x20b9;{minPrice} </span> : ''}
                  Tax inclusive price for { stayDuration } night
            </Text>
          </View>
        </Space>
      </View>
    </Space>
  );
};

SearchResultsHeader.propTypes = {
  heading: PropTypes.string.isRequired,
  minPrice: PropTypes.number,
  search: PropTypes.object.isRequired,
};

export default SearchResultsHeader;
