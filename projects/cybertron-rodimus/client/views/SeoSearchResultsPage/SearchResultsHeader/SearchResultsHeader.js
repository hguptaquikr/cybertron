import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Subheading } from '../../../components/Typography/Typography';
import './searchResultsHeader.css';

const SearchResultsHeader = ({
  minPrice,
  heading,
  search,
}) => {
  const stayDuration = search.datePicker.range.end ? (
    moment(search.datePicker.range.end).diff(moment(search.datePicker.range.start), 'days')
  ) : '';

  return (
    <div className="ssrp__header">
      <Subheading
        id="t-ssrp-header-location"
        type="1"
        tag="div"
        className="ssrp__header__location"
      >
        {heading}
      </Subheading>

      <Subheading
        type="3"
        tag="div"
        className="ssrp__header__heading"
      >
        List of {heading} with tariff
        {minPrice ? <span> - Prices start from &#x20b9;{minPrice} </span> : ''}
            Tax inclusive price for { stayDuration } night
      </Subheading>
    </div>
  );
};

SearchResultsHeader.propTypes = {
  heading: PropTypes.string.isRequired,
  minPrice: PropTypes.number,
  search: PropTypes.object.isRequired,
};

export default SearchResultsHeader;
