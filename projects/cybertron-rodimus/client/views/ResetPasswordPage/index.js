import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

export default Loadable({
  loader: () => {
    importCss('ResetPasswordPage');
    return import('./ResetPasswordPage' /* webpackChunkName: 'ResetPasswordPage' */);
  },
  loading: () => null,
});
