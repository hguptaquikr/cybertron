import React, { Component } from 'react';
import Formsy from 'formsy-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Button from '../../components/Button/Button';
import Input from '../../components/Input/Input';
import PageHeader from '../../components/PageHeader/PageHeader';
import { Heading, Subheading } from '../../components/Typography/Typography';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as authActionCreators from '../../services/auth/authDuck';
import * as toastActionCreators from '../../services/toast/toastDuck';
import analyticsService from '../../services/analytics/analyticsService';
import './resetPasswordPage.css';

class ResetPasswordPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    return Promise.all(promises);
  }

  componentDidMount() {
    const { contentActions, route, match } = this.props;
    contentActions.getSeoContent(route, match.params);
  }

  onResetPassword = ({ password }) => {
    const {
      authActions,
      match,
      history,
      toastActions,
    } = this.props;

    authActions.resetPassword(password, match.params.resetToken)
      .then((res) => {
        if (res.error) {
          toastActions.showToast('Unable to reset password, Try again.', 'capsular');
        } else {
          const { firstName, email, phone } = res.payload.profile;
          const toastLabel = firstName || email || phone;
          analyticsService.loginSuccess('Reset Password');
          analyticsService.loginDetails(res.payload.profile, 'Reset Password');
          setTimeout(() => {
            toastActions.showToast(`Logged in as ${toastLabel}`, 'capsular');
          }, 700);
          history.push({ pathname: '/' });
        }
      });
  }

  render() {
    return (
      <div className="rp">
        <PageHeader />
        <div className="rp__container">
          <Heading
            className="rp__heading"
            type="3"
          >
            Reset Your Password
          </Heading>
          <Subheading className="rp__sub-heading">
            Type your new password
          </Subheading>
          <Formsy onValidSubmit={this.onResetPassword}>
            <Input
              type="password"
              name="password"
              label="Password"
              required
            />
            <Button
              block
              className="floaty"
              flat
              large
              type="submit"
            >SUBMIT & LOGIN
            </Button>
          </Formsy>
        </div>
      </div>
    );
  }
}

ResetPasswordPage.propTypes = {
  route: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  authActions: PropTypes.object.isRequired,
  toastActions: PropTypes.object.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  authActions: bindActionCreators(authActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
});

export default connect(
  null,
  mapDispatchToProps,
)(ResetPasswordPage);
