import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Card from 'leaf-ui/cjs/Card/web';
import Text from 'leaf-ui/cjs/Text/web';
import View from 'leaf-ui/cjs/View/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import Size from 'leaf-ui/cjs/Size/web';
import Space from 'leaf-ui/cjs/Space/web';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as confirmationActionCreators from '../../services/booking/bookingDuck';
import '../../components/Loader/loader.css';

const paymentPendingHeading = 'This is taking longer than expected.';
const paymentPendingText = 'We will share the updated status as soon as we have it. We will now redirect you to the homepage.';
const paymentFailureHeading = 'Sorry! Your payment failed.';
const paymentFailureText = 'You will now be redirected back to the itinerary page to restart the payment process.';
const paymentDefaultHeading = 'Please wait.';
const paymentDefaultText = 'We’re processing your payment.';

class PaymentProcess extends Component {
  static componentWillServerRender = ({ store: { dispatch }, match }) =>
    dispatch(confirmationActionCreators.getPaymentConfirmationDetails(match.params.bookingId));

  componentDidMount() {
    const {
      match,
      confirmationActions,
      booking: { paymentDetails },
    } = this.props;
    this.interval = setInterval(() => {
      if (paymentDetails.status !== 'success' || paymentDetails.status !== 'failure') {
        confirmationActions.getPaymentConfirmationDetails(match.params.bookingId);
      }
    }, 5000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  getPaymentProcessLabel =() => {
    const {
      booking: {
        attempts,
        maxAttempts,
        paymentDetails,
      },
    } = this.props;
    switch (true) {
      case attempts >= maxAttempts && paymentDetails.status === 'pending':
        return {
          paymentStatusHeading: paymentPendingHeading,
          paymentStatusText: paymentPendingText,
        };
      case paymentDetails.status === 'failure':
        return {
          paymentStatusHeading: paymentFailureHeading,
          paymentStatusText: paymentFailureText,
        };
      default:
        return {
          paymentStatusHeading: paymentDefaultHeading,
          paymentStatusText: paymentDefaultText,
        };
    }
  }

  shouldStopFetchingPaymentStatus = () => {
    const {
      match,
      history,
      booking: {
        attempts,
        maxAttempts,
        paymentDetails: {
          status: paymentStatus,
          checkin,
          checkout,
          hotel_id: hotelId,
          rateplan,
          roomconfig,
          roomtype,
        },
      },
    } = this.props;

    setTimeout(() => {
      if (paymentStatus === 'pending' && attempts >= maxAttempts) {
        clearInterval(this.interval);
        history.push('/');
      }
      if (paymentStatus === 'success') {
        history.push(`/confirmation/${match.params.bookingId}/`);
      } else if (paymentStatus === 'failure') {
        history.push(`/itinerary/?checkin=${checkin}&checkout=${checkout}&hotel_id=${hotelId}&rateplan=${rateplan}&roomconfig=${roomconfig}&roomtype=${roomtype && roomtype.toLowerCase()}`);
      }
    }, 5000);
  }

  render() {
    const {
      booking: { isError },
    } = this.props;
    const label = this.getPaymentProcessLabel();
    this.shouldStopFetchingPaymentStatus();
    if (isError) clearInterval(this.interval);
    return (
      <View>
        <Card backgroundColor="white">
          <Size height="500px">
            <Flex
              alignItems="center"
              justifyContent="center"
            >
              <View>
                <Text component="span" className="loader__circle" />
                <Space padding={[3, 0, 2]}>
                  <Text
                    component="p"
                    weight="bold"
                    size="m"
                    color="greyDark"
                    align="center"
                  >
                    {label.paymentStatusHeading}
                  </Text>
                </Space>
                <Space padding={[0, 0, 2]}>
                  <Size width="80%">
                    <Text
                      component="p"
                      color="greyDark"
                      align="center"
                    >
                      {label.paymentStatusText}
                    </Text>
                  </Size>
                </Space>
                <Size width="80%">
                  <Text
                    component="p"
                    color="greyDark"
                    align="center"
                  >
                    Please do not press back, refresh
                    or close this tab while the payment is being processed.
                  </Text>
                </Size>
              </View>
            </Flex>
          </Size>
        </Card>
      </View>
    );
  }
}

PaymentProcess.propTypes = {
  booking: PropTypes.object.isRequired,
  match: PropTypes.object,
  history: PropTypes.object,
  confirmationActions: PropTypes.object,
};

const mapStateToProps = (state) => ({
  booking: state.booking,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  confirmationActions: bindActionCreators(confirmationActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PaymentProcess);
