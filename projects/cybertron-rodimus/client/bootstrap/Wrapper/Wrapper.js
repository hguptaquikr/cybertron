import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import Size from 'leaf-ui/cjs/Size/web';
import View from 'leaf-ui/cjs/View/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import get from 'lodash/get';
import qs from 'query-string';
import * as routeService from '../../services/route/routeService';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as contentActionCreators from '../../services/content/contentDuck';
import analyticsService from '../../services/analytics/analyticsService';
import googleService from '../../services/google/googleService';
import { performanceMark } from '../../services/utils';
import Toast from '../../components/Toast/Toast';
import Loader from '../../components/Loader/Loader';
import SearchWidgetWrapper from '../../views/SearchWidget/SearchWidgetWrapper';
import theme from '../theme';

class Wrapper extends Component {
  componentDidMount() {
    const { location } = this.props;
    performanceMark('first-interaction');
    googleService.initHandlers();
    analyticsService.recordPerformance(location.pathname);
    analyticsService.exposeQueryParameters(qs.stringify(location.search));
  }

  render() {
    const {
      route,
      content: { seo },
    } = this.props;
    return (
      <ThemeProvider theme={theme}>
        <Size
          width="100%"
          height="100%"
          minWidth={40}
        >
          <View id="wrapper">
            <Helmet
              title={get(seo, 'page.title', 'Treebo Hotels')}
              meta={seo.meta}
              link={seo.link}
              script={seo.schema}
            />
            <Flex>
              <View>
                <Size height="100%">
                  <View>
                    {routeService.renderRoutes(route.routes)}
                  </View>
                </Size>
              </View>
            </Flex>
            <SearchWidgetWrapper />
            <Loader />
            <Toast />
          </View>
        </Size>
      </ThemeProvider>
    );
  }
}

Wrapper.propTypes = {
  route: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  contentAction: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Wrapper);
