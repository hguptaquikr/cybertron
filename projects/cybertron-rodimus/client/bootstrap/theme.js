/* eslint-disable no-unused-expressions */
import { injectGlobal } from 'styled-components';
import theme, { injectBaseStyles } from 'leaf-ui/cjs/theme';

injectBaseStyles(theme);
injectGlobal`
  body {
    background: ${theme.color.white};
  }
`;

export default theme;
