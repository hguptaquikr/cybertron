import local from './local';
import development from './development';
import staging from './staging';
import production from './production';
import gdc from './gdc';

const config = {
  local,
  development,
  staging,
  production,
  gdc,
};

export default config[__APP_ENV__];
