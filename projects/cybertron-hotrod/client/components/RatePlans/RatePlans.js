import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import RatePlanTag from '../../components/RatePlanTag/RatePlanTag';
import ABButton from '../../components/ABButton/ABButton';
import './ratePlans.css';

const RatePlans = (
  {
    plan,
    rateType,
    roomType,
    selectedRatePlan,
    isSelected,
    changeRatePlan,
    discountPercentage = '',
    sellingPrice,
    strikedPrice = '',
    selectionType,
    onBookNowClicked,
  },
) => {
  const ratePlansText = {
    'non-refundable': {
      name: 'Non-Refundable',
      terms: ['Online Payment Only.', 'This booking is not refundable.'],
    },
    refundable: {
      name: 'Refundable',
      terms: ['Pay now or later at hotel.', 'Zero cancellation fee if cancelled 24 hours prior to checkin.'],
    },
  };
  const ratePlansClass = cx('rp', {
    'rp--selected': isSelected,
  });

  return (
    <div className={ratePlansClass}>
      <div className="row">
        <div className="col-4 rp__price-details">
          <RatePlanTag rateType={rateType} className="rp__tag" />
          <div>
            <span className="rp__price-details__selling-price">
              &#8377;{Math.round(sellingPrice)}
            </span>
            <div>
              {
                strikedPrice && (strikedPrice - sellingPrice) ? (
                  <del>&#x20b9;{Math.round(strikedPrice)}</del>
                ) : null
              }
              {
                discountPercentage ? (
                  <span className="rp__price-details__discount-percentage">
                    {discountPercentage}% off
                  </span>
                ) : null
              }
            </div>
          </div>
          <div className="rp__price-details__taxes">
            Inclusive of all taxes
          </div>
        </div>
        <div className="col-5 rp__toc">
          {
            ratePlansText[rateType].terms.map((term) => (
              <div className="col-12 rp__toc--conditions" key={term}>
                <div className="row">
                  <div className="col-1">
                    <i className="icon-check-circle rp__toc--green-check" />
                  </div>
                  <div className="col-11">
                    <span className="rp__toc-text"> {term} </span>
                  </div>
                </div>
              </div>
            ))
          }
        </div>
        <div className="col-3 rp__book-option">
          {
            selectionType === 'button' ? (
              <ABButton
                id="t-hdRoomTypeBookCTA"
                onClick={() => onBookNowClicked(roomType, plan)}
                experiment="hdCtaDesktop"
                large
                render={(text) => text || 'Book Now'}
              />
           ) : (
             <input
               type="radio"
               name="ratePlan"
               id="t-ratePlanRadio"
               className="rp__book-option--radio"
               value={plan}
               onClick={() => changeRatePlan(plan)}
               defaultChecked={plan === selectedRatePlan}
             />
           )
          }
        </div>
      </div>
    </div>
  );
};

RatePlans.propTypes = {
  plan: PropTypes.string,
  rateType: PropTypes.string,
  roomType: PropTypes.string,
  sellingPrice: PropTypes.number,
  strikedPrice: PropTypes.number,
  discountPercentage: PropTypes.number,
  selectionType: PropTypes.string,
  selectedRatePlan: PropTypes.string,
  isSelected: PropTypes.bool,
  changeRatePlan: PropTypes.func,
  onBookNowClicked: PropTypes.func,
};

export default RatePlans;
