import React from 'react';
import { default as ReduxLoadingBar } from 'react-redux-loading-bar';

const LoadingBar = () => (
  <ReduxLoadingBar
    style={{
      backgroundColor: '#11d05d',
      position: 'fixed',
      top: '0',
      zIndex: 9999,
    }}
  />
);

export default LoadingBar;
