import React from 'react';
import PropTypes from 'prop-types';

const TooltipContent = ({ children }) => (
  <div className="tooltip">
    <div className="tooltip__arrow" />
    <div className="tooltip__content text-2">
      {children}
    </div>
  </div>
);

TooltipContent.propTypes = {
  children: PropTypes.node,
};

export default TooltipContent;
