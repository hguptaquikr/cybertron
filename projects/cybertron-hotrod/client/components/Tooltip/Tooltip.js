import React from 'react';
import PropTypes from 'prop-types';
import { reactChildrenByType } from '../../services/utils';
import TooltipTrigger from './TooltipTrigger';
import TooltipContent from './TooltipContent';
import './tooltip.css';

class Tooltip extends React.Component {
  state = {
    show: false,
  }

  open = () => {
    this.setState({ show: true });
  }

  close = () => {
    this.setState({ show: false });
  }

  render() {
    const { children } = this.props;
    const childrenByType = reactChildrenByType(children);

    return (
      <div>
        {
          React.cloneElement(childrenByType[TooltipTrigger], {
            onMouseEnter: this.open,
            onMouseLeave: this.close,
          })
        }
        {this.state.show ? childrenByType[TooltipContent] : null}
      </div>
    );
  }
}

Tooltip.propTypes = {
  children: PropTypes.node,
};

Tooltip.Trigger = TooltipTrigger;
Tooltip.Content = TooltipContent;

export default Tooltip;
