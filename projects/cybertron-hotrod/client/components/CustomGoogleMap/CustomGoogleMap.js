import React from 'react';
import PropTypes from 'prop-types';
import config from '../../../config';
import './customGoogleMap.css';

const CustomGoogleMap = ({
  placeCoordinate: {
    lat,
    lng,
  },
  zoom,
}) => {
  const mapUrl = `https://www.google.com/maps/embed/v1/view?center=${lat},${lng}&zoom=${zoom}&key=${config.googleMapsApiKey}`;
  return (
    <div className="custom-map">
      <iframe
        title="google map"
        className="custom-map__map"
        src={mapUrl}
        allowFullScreen
      />
    </div>
  );
};

CustomGoogleMap.propTypes = {
  placeCoordinate: PropTypes.object.isRequired,
  zoom: PropTypes.number.isRequired,
};

export default CustomGoogleMap;
