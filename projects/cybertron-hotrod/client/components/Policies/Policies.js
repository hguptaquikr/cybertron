import React from 'react';
import PropTypes from 'prop-types';

const Policies = ({ policies }) => (
  <ul>
    {
      policies.map(({ title, description }) => (
        <li key={title}>
          {title && <strong>{title} - </strong>}
          <span className="hd-policy__desc">{description}</span>
        </li>
      ))
    }
  </ul>
);

Policies.propTypes = {
  policies: PropTypes.array.isRequired,
};

export default Policies;
