import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './chip.css';

const Chip = ({ text, color, className }) => {
  const chipClass = cx('chip', `chip--${color}`, className);

  return (
    <span className={chipClass}>{text}</span>
  );
};

Chip.propTypes = {
  text: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default Chip;
