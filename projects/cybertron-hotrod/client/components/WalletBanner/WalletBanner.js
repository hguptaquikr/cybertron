import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'react-router-dom';
import analyticsService from '../../services/analytics/analyticsService';
import './walletBanner.css';

class WalletBanner extends Component {
  componentDidMount() {
    const { walletAmount, src } = this.props;
    const bannerType = walletAmount ? 'Burn' : 'Earn';
    analyticsService.bannerViewed(src, bannerType);
  }

  render() {
    const { walletAmount, guestContactNumber } = this.props;
    return (
      <div className="wallet-banner">
        <div className="row">
          <div className="col-12 wallet-banner__info">
            <div className="wallet-banner__info__banner-img">
              {
                !guestContactNumber ? (
                  <img
                    src={cx({
                      'https://images.treebohotels.com/images/hotrod/menu-loyalty.svg': walletAmount,
                      'https://s3-ap-southeast-1.amazonaws.com/treebo/static/images/hotrod/redeem-points-loyalty-40-px.svg': !walletAmount,
                    })}
                    className="wallet-banner__info__banner-img__img"
                    alt="wallet"
                  />
                ) : null
              }
            </div>
            <div id="wallet-banner" className="wallet-banner__info__price">
              {
                walletAmount ? (
                  `Treebo Reward points worth ₹${walletAmount} is pre applied on price.`
                ) : null
              }
              {
                !walletAmount && !guestContactNumber ? (
                  'To earn Treebo Points on your stay - Book Now!'
                ) : null
              }
              {
                guestContactNumber ? (
                  <span>
                    {`Treebo Points will be credited to +91 ${guestContactNumber} post checkout.`}
                    <Link
                      id="terms"
                      to="/faq/"
                      target="_blank"
                      className="text-link"
                    >
                      View T&Cs.
                    </Link>
                  </span>
                ) : null
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

WalletBanner.propTypes = {
  walletAmount: PropTypes.number,
  guestContactNumber: PropTypes.number,
  src: PropTypes.string,
};

export default WalletBanner;
