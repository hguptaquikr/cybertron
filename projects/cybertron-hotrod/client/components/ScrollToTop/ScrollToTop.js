import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import analyticsService from '../../services/analytics/analyticsService';

class ScrollToTop extends React.Component {
  componentDidMount() {
    analyticsService.pageTransition();
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      window.scrollTo(0, 0);
      analyticsService.pageTransition();
    }
  }

  render() {
    return this.props.children;
  }
}

ScrollToTop.propTypes = {
  location: PropTypes.object.isRequired,
  children: PropTypes.object.isRequired,
};

export default withRouter(ScrollToTop);
