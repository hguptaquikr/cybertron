import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import './autoApplyCouponBanner.css';

class AutoApplyCouponBanner extends Component {
  state = {
    isOpen: true,
  }

  onCloseBanner = () => {
    this.setState({
      isOpen: false,
    });
  }

  getAutoAppliedCoupon = (prices) => {
    const priceWithCoupon = Object.values(prices)
      .find((price) => !!price.couponCode);
    return priceWithCoupon && priceWithCoupon.couponCode;
  }

  render() {
    const { isOpen } = this.state;
    const {
      price,
    } = this.props;
    const autoAppliedCouponCode = this.getAutoAppliedCoupon(price.byRoomTypeId);
    return isOpen && autoAppliedCouponCode ? (
      <div className="row row--middle row--between coupon-banner">
        <div className="col col-8 coupon-banner__title">
          <span className="subheading-1 coupon-banner__title__text">{`"${autoAppliedCouponCode}"`} Coupon Code is Pre-applied</span>
        </div>
        <i className="icon-cross coupon-banner__close" onClick={this.onCloseBanner} />
      </div>
    ) : null;
  }
}

AutoApplyCouponBanner.propTypes = {
  price: PropTypes.object,
};

const mapStateToProps = (state) => ({
  price: state.price,
});

export default compose(
  connect(mapStateToProps),
)(AutoApplyCouponBanner);
