import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './button.css';

const Button = ({
  children,
  className,
  modifier,
  block,
  flat,
  large,
  bold,
  round,
  disabled,
  preview,
  ...props
}) => {
  const buttonClass = cx(`custom-button custom-button--${modifier}`, {
    'custom-button--block': block,
    'custom-button--flat': flat,
    'custom-button--large': large,
    'custom-button--disabled': disabled,
    'custom-button--bold': bold,
    'custom-button--round': round,
    'custom-button--preview': preview,
  }, className);
  return (
    <button
      className={buttonClass}
      disabled={disabled || preview}
      {...props}
    >
      {children}
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  block: PropTypes.bool,
  flat: PropTypes.bool,
  large: PropTypes.bool,
  bold: PropTypes.bool,
  round: PropTypes.bool,
  preview: PropTypes.bool,
  disabled: PropTypes.bool,
  modifier: PropTypes.oneOf([
    'primary',
    'secondary',
    'tertiary',
    'link',
  ]),
};

Button.defaultProps = {
  modifier: 'primary',
};

export default Button;
