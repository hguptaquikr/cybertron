import get from 'lodash/get';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';
import { ThemeProvider } from 'styled-components';
import leafUiTheme from 'leaf-ui/cjs/theme';
import * as routeService from '../../services/route/routeService';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as userActionCreators from '../../services/user/userDuck';
import Navbar from '../Navbar/Navbar';
import LoadingBar from '../LoadingBar/LoadingBar';
import Footer from '../Footer/Footer';
import google from '../../services/google/googleService';
import { performanceMark } from '../../services/utils';
import analyticsService from '../../services/analytics/analyticsService';
import './wrapper.css';

class Wrapper extends React.Component {
  componentDidMount() {
    const { userActions, location } = this.props;
    performanceMark('first-interaction');
    google.initHandlers();
    userActions.checkIfUserAuthenticated();
    analyticsService.recordPerformance(location.pathname);
    analyticsService.exposeSearchParamenters(location.search);
  }

  render() {
    const {
      route,
      content: { seo },
      location,
    } = this.props;

    return (
      <ThemeProvider theme={leafUiTheme}>
        <div id="wrapper" className="wrapper">
          <Helmet
            title={get(seo, 'page.title', 'Treebo Hotels')}
            meta={seo.meta}
            link={seo.link}
            script={seo.schema}
          />
          <LoadingBar />
          <Navbar />
          {routeService.renderRoutes(route.routes)}
          {location.pathname !== '/' ? <Footer /> : null}
        </div>
      </ThemeProvider>
    );
  }
}

Wrapper.propTypes = {
  route: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Wrapper);
