import React from 'react';
import PropTypes from 'prop-types';
import Modal from '../Modal/Modal';
import './loader.css';

const Loader = ({ text, img }) => (
  <Modal
    isOpen
    label="Loader"
    onClose={() => {}}
  >
    <div className="loader text-center">
      <div className="text--center">
        <span className="loader__circle" />
      </div>
      <div className="loader__body">
        {
          img ? (
            <img
              src={img}
              className="loader__body__img"
              alt="loader"
            />
          ) : null
        }
        <span className="loader__body__text">
          {text}
        </span>
      </div>
    </div>
  </Modal>
);

Loader.propTypes = {
  text: PropTypes.string.isRequired,
  img: PropTypes.string,
};

export default Loader;
