import React from 'react';
import PropTypes from 'prop-types';
import Loading from '../Loading/Loading';
import config from '../../../config';

import './staticMap.css';

const StaticMap = ({ className, coordinates: { lat, lng }, width, height, zoom, scale }) => {
  const imgSrc = [
    `https://maps.googleapis.com/maps/api/staticmap?zoom=${zoom}`,
    `key=${config.googleMapsApiKey}`,
    `size=${Math.ceil(width / scale)}x${Math.ceil(height / scale)}`,
    `scale=${scale}`,
    `markers=size:mid|${lat},${lng}`,
    'format=png',
  ].join('&');

  return (
    <div>
      <a
        style={{ display: 'block', fontSize: 0 }}
        className={className}
        href={`https://www.google.com/maps/search/?api=1&query=${lat},${lng}`}
        target="_blank"
        rel="noopener noreferrer"
      >
        {
          lat ? (
            <img style={{ width: '100%' }} src={imgSrc} alt="Treebo Hotel Location" />
          ) : (<Loading height={height} />)
        }
      </a>
    </div>
  );
};

StaticMap.propTypes = {
  className: PropTypes.string,
  coordinates: PropTypes.object.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  zoom: PropTypes.number,
  scale: PropTypes.number,
};

StaticMap.defaultProps = {
  zoom: 13,
  scale: 2,
};

export default StaticMap;
