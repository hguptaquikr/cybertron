import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import cx from 'classnames';
import * as contentActionCreators from '../../services/content/contentDuck';
import analyticsService from '../../services/analytics/analyticsService';
import './qualityGuaranteeBanner.css';

const qualityGuaranteePromises = [{
  icon: 'icon-room',
  title: 'Comfortable Rooms',
}, {
  icon: 'icon-staff',
  title: 'Best-in-class Service',
}, {
  icon: 'icon-breakfast',
  title: 'Wholesome Breakfast',
}, {
  icon: 'icon-ac',
  title: 'Assured Room Amenities',
}];

class QualityGuaranteeBanner extends Component {
  componentDidMount() {
    const { contentActions } = this.props;
    contentActions.getPerfectStayContent();
  }

  render() {
    const { perfectStay, image, page } = this.props;
    return (
      <div className="row row--middle qgb__container">
        {
          image ? (
            <img
              className="qgb__img"
              src={perfectStay.illustration}
              alt="Quality guarantee"
            />
          ) : null
        }
        <div className="qgb__description">
          <h2 className="text-1 qgb__title">Perfect Stay or Don’t Pay</h2>
          <div className="qgb__subtitle">
            <span className="text-2 qgb__subtitle__text">100% money-back promise</span>
            <Link
              className="text-2 qgb__subtitle__link"
              to="/perfect-stay/"
              target="_blank"
              onClick={() => analyticsService.perfectStayKnowMoreClicked(page)}
            >
              Know More
            </Link>
          </div>
          <ul className="row qgb__promises">
            {
              qualityGuaranteePromises.map(({
                icon,
                title,
              }) => (
                <li key={title} className="col-3 qgb__promise">
                  <i className={cx('qgb__icon', icon)} />
                  <span className="text-3 qgb__text">{title}</span>
                </li>
              ))
            }
          </ul>
        </div>
      </div>
    );
  }
}

QualityGuaranteeBanner.propTypes = {
  image: PropTypes.bool,
  page: PropTypes.string.isRequired,
  perfectStay: PropTypes.object,
  contentActions: PropTypes.object.isRequired,
};

QualityGuaranteeBanner.defaultProps = {
  image: false,
};

const mapStateToProps = (state) => ({
  perfectStay: state.content.perfectStay,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(QualityGuaranteeBanner);
