import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { default as Slick } from 'react-slick';
import 'slick-carousel/slick/slick.css';
import { safeKeys } from '../../services/utils';
import './slider.css';

const Prev = ({ arrowClass, ...rest }) => (
  <button {...rest} className={`button slider__arrow slider__arrow--left ${arrowClass}`}>
    <i className="icon-angle-left" />
  </button>
);

Prev.propTypes = {
  arrowClass: PropTypes.string,
};

const Next = ({ arrowClass, ...rest }) => (
  <button {...rest} className={`button slider__arrow slider__arrow--right ${arrowClass}`}>
    <i className="icon-angle-right" />
  </button>
);

Next.propTypes = {
  arrowClass: PropTypes.string,
};

class Slider extends React.Component {
  componentDidMount() {
    const { children } = this.props;
    // TODO: assume its not always lazy loading second level nested img.src
    if (safeKeys(children).length > 1) {
      (new window.Image()).src = get(children[1], 'props.children.props.src');
    }
  }

  afterChange = (current) => {
    let currentSlide = current;
    const { preloadNextImage, afterChange, children, slidesToShow } = this.props;
    if (slidesToShow > 1) currentSlide += slidesToShow;
    // TODO: assume its not always lazy loading second level nested img.src
    if (preloadNextImage && currentSlide < safeKeys(children).length - 1) {
      (new window.Image()).src = get(children[currentSlide + 1], 'props.children.props.src');
    }
    if (afterChange) afterChange(currentSlide);
  }

  render() {
    const { getSlider, children, arrowClass, ...rest } = this.props;
    return (
      <div>
        {
          safeKeys(children).length ?
            <Slick
              className="slider"
              ref={getSlider}
              {...rest}
              afterChange={this.afterChange}
              nextArrow={<Next arrowClass={arrowClass} />}
              prevArrow={<Prev arrowClass={arrowClass} />}
            >
              {children}
            </Slick>
          : null
        }
      </div>
    );
  }
}

Slider.defaultProps = {
  focusOnSelect: true,
  dots: false,
  lazyLoad: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  dotsClass: 'slider__dots',
  getSlider: () => {},
  preloadNextImage: true,
};

Slider.propTypes = {
  children: PropTypes.array.isRequired,
  getSlider: PropTypes.func,
  preloadNextImage: PropTypes.bool.isRequired,
  afterChange: PropTypes.func,
  arrowClass: PropTypes.string,
  slidesToShow: PropTypes.number,
};

export default Slider;
