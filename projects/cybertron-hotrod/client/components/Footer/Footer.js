import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import './footer.css';

class Footer extends React.Component {
  excludedRoutes = [
    '/itinerary/',
    '/login/',
    '/signup/',
    '/forgot-password/',
  ];

  showFooter = () => {
    const { location } = this.props;
    return !this.excludedRoutes.some((excludedRoute) =>
      location.pathname === excludedRoute);
  };

  render() {
    return this.showFooter()
      ? (
        <footer className="footer" id="t-mainFooter">
          <div className="container">
            <div className="footer__links">
              <div>
                <div className="heading-1 footer__links__title">COMPANY</div>
                <ul className="text-1 footer__links__list">
                  <li><a href="/aboutus/">About</a></li>
                  <li><a href="/contactus/">Contact</a></li>
                  <li>
                    <a
                      href="//careers.treebohotels.com"
                      rel="nofollow"
                    >
                      Careers
                    </a>
                  </li>
                  <li><Link to="/faq/" rel="nofollow">Faq</Link></li>
                  <li><a href="/terms/" rel="nofollow">Terms of Service</a></li>
                  <li><a href="/policy/" rel="nofollow">Privacy Policy</a></li>
                  <li><a href="mailto:feedback@treebohotels.com" rel="nofollow">Feedback</a></li>
                  <li><a href="https://blog.treebo.com/">Blog</a></li>
                  <li><a href="https://tech.treebo.com/">Tech Blog</a></li>
                  <li><a href="/security/" rel="nofollow">Responsible Disclosure</a></li>
                </ul>
              </div>
              <div>
                <div className="footer__subheading heading-1 footer__links__title">BUSINESS</div>
                <ul className="text-1 footer__links__list">
                  <li><a href="/joinus/" target="_blank" rel="nofollow">Join our Network</a></li>
                  <li><a href="https://business.treebo.com" rel="nofollow">Corporate Enquiry</a></li>
                  <li><a href="mailto:ta@treebohotels.com" rel="nofollow">Travel Agents</a></li>
                </ul>
              </div>
              <div>
                <div className="footer__subheading heading-1 footer__links__title">DISCOVER</div>
                <ul className="text-1 footer__links__list">
                  <li>
                    <Link to="/hotels-near-me/">Hotels near me</Link>
                  </li>
                  <li>
                    <a
                      href="//business.treebo.com"
                      target="_blank"
                      rel="noopener noreferrer dofollow"
                    >Treebo for Business
                    </a>
                  </li>
                  <li>
                    <a
                      href="/fot/"
                      target="_blank"
                      rel="noopener noreferrer"
                    >Friends of Treebo
                    </a>
                  </li>
                </ul>
              </div>
              <div>
                <div className="footer__subheading heading-1 footer__links__title">SOCIAL</div>
                <ul className="text-1 footer__links__list">
                  <li>
                    <a
                      href="https://www.facebook.com/TreeboHotels/"
                      target="_blank"
                      rel="noopener noreferrer nofollow"
                    >
                      <i className="footer__social-icon icon-facebook" />Facebook
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://twitter.com/TreeboHotels"
                      target="_blank"
                      rel="noopener noreferrer nofollow"
                    >
                      <i className="footer__social-icon icon-twitter" />Twitter
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://www.linkedin.com/company/treebohotels"
                      target="_blank"
                      rel="noopener noreferrer nofollow"
                    >
                      <i className="footer__social-icon icon-linkedin" />Linkedin
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://www.instagram.com/treebo_hotels/"
                      target="_blank"
                      rel="noopener noreferrer nofollow"
                    >
                      <i className="footer__social-icon icon-instagram" />Instagram
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://plus.google.com/101173360836250828959"
                      target="_blank"
                      rel="noopener noreferrer nofollow"
                    >
                      <i className="footer__social-icon icon-google-plus" />Google Plus
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      ) : (null);
  }
}

Footer.propTypes = {
  location: PropTypes.object.isRequired,
};

export default withRouter(Footer);
