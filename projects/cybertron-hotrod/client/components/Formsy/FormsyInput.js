import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { withFormsy, propTypes } from 'formsy-react';
import './formsyInput.css';

class FormsyInput extends React.Component {
  state = {
    value: this.props.value || undefined,
    isDirty: false,
  };

  componentWillReceiveProps({ value, setValue }) {
    if (value !== this.props.value) {
      this.setState({ value });
      setValue(value);
    }
  }

  onChange = (e) => {
    const { setValue, onChange } = this.props;
    const value = e.currentTarget.value || undefined;
    setValue(value);
    this.setState({ value });
    if (onChange) onChange(e);
  };

  onBlur = () => {
    this.setState({ isDirty: true });
  };

  getErrorMessage = () => {
    const { label, showRequired, showError, isFormSubmitted, getErrorMessage } = this.props;
    const { isDirty } = this.state;
    const showRequiredMessage = showRequired() && (isDirty || isFormSubmitted());
    const showErrorMessage = (showError() && isDirty) || isFormSubmitted();
    let errorMessage = showRequiredMessage ? `${label} is a required field` : '';
    errorMessage = showErrorMessage && getErrorMessage() ? getErrorMessage() : errorMessage;
    return errorMessage;
  };

  render() {
    const {
      containerClassName,
      inputClassName,
      errorClassName,
      type,
      name,
      label,
      isValid,
    } = this.props;
    const { value } = this.state;

    const errorMessage = this.getErrorMessage();
    const containerClassNames = cx('formsy-input', containerClassName);
    const inputClassNames = cx('formsy-input__field', inputClassName, {
      'formsy-input__field--error': errorMessage,
      'formsy-input__field--valid': isValid() && value,
    });
    const labelClassNames = cx('formsy-input__label', {
      'formsy-input__label--has-value': value,
      'formsy-input__label--valid': isValid() && value,
      'formsy-input__label--error': errorMessage,
    });
    const errorClassNames = cx('formsy-input__error-message', errorClassName);

    return (
      <div className={containerClassNames}>
        <input
          type={type}
          name={name}
          id={name}
          className={inputClassNames}
          onChange={this.onChange}
          onBlur={this.onBlur}
          value={value}
        />
        <label htmlFor={name} className={labelClassNames}>
          {label}
        </label>
        {
          errorMessage ? (
            <div className={errorClassNames} id="t-formErrorText">
              {errorMessage}
            </div>
          ) : (null)
        }
      </div>
    );
  }
}

FormsyInput.propTypes = {
  containerClassName: PropTypes.string,
  inputClassName: PropTypes.string,
  errorClassName: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string,
  label: PropTypes.string.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func,
  isValid: PropTypes.func.isRequired,
  setValue: PropTypes.func.isRequired,
  showRequired: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
  isFormSubmitted: PropTypes.func.isRequired,
  getErrorMessage: PropTypes.func.isRequired,
  ...propTypes,
};

export default withFormsy(FormsyInput);
