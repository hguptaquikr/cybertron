import React from 'react';
import PropTypes from 'prop-types';
import './loading.css';

const Loading = ({ className, color, ...styles }) => (
  <div
    className={`loading loading--${color} ${className}`}
    style={styles}
  />
);

Loading.propTypes = {
  className: PropTypes.string,
  color: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  display: PropTypes.string,
};

Loading.defaultProps = {
  className: '',
  color: 'quaternary',
  width: '100%',
  height: '0.8rem',
  display: 'block',
};

export default Loading;
