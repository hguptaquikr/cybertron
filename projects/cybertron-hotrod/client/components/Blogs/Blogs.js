import React from 'react';
import PropTypes from 'prop-types';
import Slider from '../../components/Slider/Slider';
import './blogs.css';

const Blogs = ({ blogs }) => {
  const { title, blogsList } = blogs;
  return (
    <section className="blogs">
      <div className="container blogs__desc-title">
        <h2>{title}</h2>
      </div>
      <div className="container blogs__list">
        <Slider
          dots={false}
          infinite={false}
          lazyLoad={false}
          speed={500}
          slidesToShow={3}
          slidesToScroll={1}
          className="blogs__slide container"
          arrowClass="slider__arrow--large"
        >
          {
            blogsList.map(({ name: blogDesc, blogUrl, imageUrl }) => (
              <a href={blogUrl} target="_blank" rel="noopener noreferrer">
                <div className="blogs__blog">
                  <img src={imageUrl} className="blogs__blog-image" alt={blogDesc} />
                  <div className="text-2 blogs__blog-desc">
                    {blogDesc}
                  </div>
                </div>
              </a>
            ))
          }
        </Slider>
      </div>
    </section>
  );
};

Blogs.propTypes = {
  blogs: PropTypes.object.isRequired,
};

export default Blogs;
