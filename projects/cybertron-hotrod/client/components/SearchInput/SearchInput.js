import React from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import Autosuggest from 'react-autosuggest';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { pluralize } from '../../services/utils';
import * as searchActionCreators from '../../services/search/searchDuck';
import './searchInput.css';

class SearchInput extends React.Component {
  state = {
    value: this.props.searchInput.location.label,
  };

  componentWillReceiveProps(nextProps) {
    const { props } = this;
    if (props.searchInput.location.label !== nextProps.searchInput.location.label) {
      this.setState({ value: nextProps.searchInput.location.label });
    }
  }

  onChange = (event, { newValue }) => {
    const { searchLocationChange } = this.props;
    this.setState({ value: newValue });
    if (!newValue) searchLocationChange({});
  };

  onBlur = (e, { focusedSuggestion }) => {
    const { onBlur } = this.props;
    if (focusedSuggestion) this.onSuggestionSelected(e, { suggestion: focusedSuggestion });
    if (onBlur) onBlur(e);
  }

  onSuggestionsFetchRequested = debounce(({ value }) => {
    const { getSearchSuggestions } = this.props;
    getSearchSuggestions(value.trim());
  }, 300);

  onSuggestionSelected = debounce((e, { suggestion }) => {
    const { searchLocationChange, searchActions } = this.props;
    searchLocationChange(suggestion);
    if (suggestion.type === 'google_api') searchActions.getLocationDetails(suggestion);
  }, 300);

  getSuggestionValue = (suggestion) => suggestion.label;

  shouldRenderSuggestions = (value) => {
    const { wasSearchButtonClicked } = this.props;
    return (wasSearchButtonClicked && !value) || value.trim().length >= 3;
  }

  renderSuggestion = ({ label, type, hotel_count: hotelCount, icon }) => {
    let rightElem = type;
    if (hotelCount) rightElem = `${hotelCount} ${pluralize(hotelCount, 'Hotel')}`;
    else if (icon) rightElem = <i className={icon} />;

    const badge = type !== 'google_api' ? (
      <span className="react-autosuggest__suggestion__type">
        { rightElem }
      </span>
    ) : (null);
    return (
      <div className="react-autosuggest__suggestion__container">
        <span className="react-autosuggest__suggestion__label">{label}</span>
        {badge}
      </div>
    );
  };

  render() {
    const {
      searchInput: { isLoading, suggestions },
      storeSearchInputRef,
      clearSearchSuggestions,
      toggleSearchButton,
    } = this.props;
    const { value } = this.state;

    const inputProps = {
      placeholder: 'Search by city, locality, hotel name',
      onChange: this.onChange,
      onBlur: this.onBlur,
      value: value || '',
    };

    return (
      <div className="col-4 search-input" onClick={toggleSearchButton}>
        <Autosuggest
          ref={storeSearchInputRef}
          suggestions={suggestions}
          shouldRenderSuggestions={this.shouldRenderSuggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={clearSearchSuggestions}
          getSuggestionValue={this.getSuggestionValue}
          renderSuggestion={this.renderSuggestion}
          onSuggestionSelected={this.onSuggestionSelected}
          inputProps={inputProps}
          focusFirstSuggestion
          focusInputOnSuggestionClick={false}
        />
        {
          isLoading ? (
            <span className="search-input__loader" />
          ) : (null)
        }
      </div>
    );
  }
}

SearchInput.propTypes = {
  searchInput: PropTypes.object.isRequired,
  storeSearchInputRef: PropTypes.func.isRequired,
  getSearchSuggestions: PropTypes.func.isRequired,
  clearSearchSuggestions: PropTypes.func.isRequired,
  searchLocationChange: PropTypes.func.isRequired,
  wasSearchButtonClicked: PropTypes.bool.isRequired,
  onBlur: PropTypes.func,
  searchActions: PropTypes.object.isRequired,
  toggleSearchButton: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchInput);
