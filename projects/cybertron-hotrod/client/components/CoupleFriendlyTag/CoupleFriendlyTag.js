import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './coupleFriendlyTag.css';

const CoupleFriendlyTag = ({ className, ...props }) => (
  <div id="t-coupleFriendlyTag" className={cx('couple-friendly-tag', className)} {...props}>
    <i className="icon-couple" />
    <span className="couple-friendly-tag__text">Couple Friendly</span>
  </div>
);

CoupleFriendlyTag.propTypes = {
  className: PropTypes.string,
};

export default CoupleFriendlyTag;
