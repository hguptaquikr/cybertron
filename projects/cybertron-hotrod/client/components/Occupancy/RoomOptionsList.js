/* eslint-disable react/no-array-index-key, react/no-find-dom-node */
import React from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import RoomOption from './RoomOption';

class RoomOptionsList extends React.Component {
  componentDidUpdate = () => {
    findDOMNode(this).scrollTop = 100000;
  }
  render() {
    const {
      rooms,
      occupancyAdultsChange,
      occupancyKidsChange,
      occupancyRemoveRoom,
    } = this.props;

    return (
      <ul className="config-options__list">
        {
          rooms.map((room, i) => (
            <RoomOption
              id={`t-room${i}`}
              key={i}
              index={i}
              room={room}
              occupancyAdultsChange={occupancyAdultsChange}
              occupancyKidsChange={occupancyKidsChange}
              occupancyRemoveRoom={occupancyRemoveRoom}
            />
            ))
        }
      </ul>
    );
  }
}
RoomOptionsList.propTypes = {
  rooms: PropTypes.array.isRequired,
  occupancyAdultsChange: PropTypes.func.isRequired,
  occupancyKidsChange: PropTypes.func.isRequired,
  occupancyRemoveRoom: PropTypes.func.isRequired,
};

export default RoomOptionsList;
