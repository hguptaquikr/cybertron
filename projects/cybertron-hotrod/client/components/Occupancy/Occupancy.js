/* eslint-disable react/no-array-index-key */
import React from 'react';
import PropTypes from 'prop-types';
import onClickOutside from 'react-onclickoutside';
import cx from 'classnames';
import RoomConfig from './RoomConfig';
import DefaultRoomOption from './DefaultRoomOption';
import { getOccupancyStr, getTotalGuests } from '../../services/utils';
import './occupancy.css';

const occupancyOptions = [{
  label: '1 Adult, 1 Room',
  value: {
    adults: 1,
  },
}, {
  label: '2 Adults, 1 Room',
  value: {
    adults: 2,
  },
}];

class Occupancy extends React.Component {
  componentDidMount() {
    const {
      occupancy: { rooms, customRoomVisible },
      occupancyCustomRoomsVisibilty,
    } = this.props;
    const { adults, kids } = getTotalGuests(rooms);
    if ((rooms.length > 1 || kids > 0 || adults > 2) && !customRoomVisible) {
      occupancyCustomRoomsVisibilty(true);
    }
  }

  getOccupancy = () => {
    const { occupancy: { rooms } } = this.props;
    return rooms.length ? getOccupancyStr(rooms) : 'Occupancy';
  }

  handleClickOutside = () => {
    const { occupancy, occupancyVisibilty } = this.props;
    if (occupancy.visible) occupancyVisibilty(false);
  }

  occupancyRoomsChange = (rooms) => {
    const { occupancyRoomsChange } = this.props;
    occupancyRoomsChange(rooms);
  }

  occupancyCustomRoomsVisibilty = () => {
    const { occupancyCustomRoomsVisibilty } = this.props;
    occupancyCustomRoomsVisibilty(true);
  }

  occupancyVisibilty = () => {
    const { occupancy, occupancyVisibilty } = this.props;
    occupancyVisibilty(!occupancy.visible);
  }

  render() {
    const {
      occupancy,
      occupancyKidsChange,
      occupancyAdultsChange,
      occupancyAddRoom,
      occupancyRemoveRoom,
    } = this.props;

    const dropdownClasses = cx('occupancy__dd pos-abs', {
      'occupancy__dd--show': occupancy.visible,
    });

    const optionsClasses = cx('occupancy__options', {
      'occupancy__options--show': !occupancy.customRoomVisible,
    });
    const customRoomsClasses = cx('occupancy__room-config', {
      'occupancy__room-config--show': occupancy.customRoomVisible,
    });

    const titleClasses = cx('occupancy__title search-box__label', {
      'search-box__placeholder': !(occupancy.rooms && occupancy.rooms.length > 0),
    });

    const caretClasses = cx('icon-caret-down occupancy__caret', {
      'occupancy__caret--open': occupancy.visible,
    });

    return (
      <div className="row occupancy pos-rel">
        <div className="occupancy__header" id="t-occupancy__header" onClick={this.occupancyVisibilty}>
          <div className={titleClasses}>{this.getOccupancy()}</div>
          <i className={caretClasses} />
        </div>
        <div className={dropdownClasses}>
          <div className={optionsClasses}>
            {
            occupancyOptions.map((option, i) => (
              <DefaultRoomOption
                option={option}
                key={i}
                id={`t-occupancy__options__item${i + 1}`}
                occupancyRoomsChange={this.occupancyRoomsChange}
                selectedRoom={occupancy.rooms}
              />))
            }
            <div
              id="t-occupancy__options__item__multiple"
              className="occupancy__options__item"
              onClick={this.occupancyCustomRoomsVisibilty}
            >Multiple Rooms
            </div>
          </div>
          <div className={customRoomsClasses}>
            <RoomConfig
              rooms={occupancy.rooms}
              occupancyAdultsChange={occupancyAdultsChange}
              occupancyKidsChange={occupancyKidsChange}
              occupancyAddRoom={occupancyAddRoom}
              occupancyRemoveRoom={occupancyRemoveRoom}
              occupancyVisibilty={this.occupancyVisibilty}
            />
          </div>
        </div>
      </div>
    );
  }
}

Occupancy.propTypes = {
  occupancyVisibilty: PropTypes.func.isRequired,
  occupancyCustomRoomsVisibilty: PropTypes.func.isRequired,
  occupancyRoomsChange: PropTypes.func.isRequired,
  occupancy: PropTypes.object.isRequired,
  occupancyAdultsChange: PropTypes.func.isRequired,
  occupancyKidsChange: PropTypes.func.isRequired,
  occupancyAddRoom: PropTypes.func.isRequired,
  occupancyRemoveRoom: PropTypes.func.isRequired,
};

export default onClickOutside(Occupancy);
