import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import RoomOptionsList from './RoomOptionsList';

const RoomConfig = ({
  rooms,
  occupancyAdultsChange,
  occupancyKidsChange,
  occupancyAddRoom,
  occupancyRemoveRoom,
  occupancyVisibilty,
}) => (
  <div className="config-options">
    <RoomOptionsList
      rooms={rooms}
      occupancyAdultsChange={occupancyAdultsChange}
      occupancyKidsChange={occupancyKidsChange}
      occupancyAddRoom={occupancyAddRoom}
      occupancyRemoveRoom={occupancyRemoveRoom}
    />
    <div className="config-options__item room-config__action flex-row">
      <button
        id="t-add__room"
        className={cx('button button--tertiary button--round',
          'room-config__action__item room-config__action__item--add')
        }

        onClick={occupancyAddRoom}
      >Add room
      </button>
      <button
        id="t-confirm__room"
        className={cx('button button--secondary button--round',
          'room-config__action__item room-config__action__item--confirm')
        }
        onClick={occupancyVisibilty}
      >Confirm
      </button>
    </div>
  </div>
);

RoomConfig.propTypes = {
  rooms: PropTypes.array.isRequired,
  occupancyAdultsChange: PropTypes.func.isRequired,
  occupancyKidsChange: PropTypes.func.isRequired,
  occupancyAddRoom: PropTypes.func.isRequired,
  occupancyRemoveRoom: PropTypes.func.isRequired,
  occupancyVisibilty: PropTypes.func.isRequired,
};

export default RoomConfig;
