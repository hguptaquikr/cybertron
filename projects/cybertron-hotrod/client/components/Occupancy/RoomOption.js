import React from 'react';
import PropTypes from 'prop-types';

const RoomOption = ({
  room,
  index,
  occupancyAdultsChange,
  occupancyKidsChange,
  occupancyRemoveRoom,
  id,
}) => {
  const removeLink = index !== 0 ? (
    <span
      href="#"
      className="text-3 room-config__remove-link"
      onClick={() => occupancyRemoveRoom(index)}
    >
      Remove
    </span>
  ) : (null);
  const kidsAgeInfo = index === 0 ? (
    <span className="text-3">(2-8 years)</span>
  ) : (null);
  const { adults = 1, kids = 0 } = room;

  return (
    <li className="config-options__item room-config" id={id}>
      <div className="flex-row row--middle">
        <div className="room-config__rooms">Room {`${index + 1}`}</div>
        <div className="select-box room-config__adults">
          <select
            className="select select--small"
            value={adults}
            onChange={(e) => occupancyAdultsChange(index, e.target.value)}
          >
            <option value="1">1 Adult</option>
            <option value="2">2 Adults</option>
            <option value="3">3 Adults</option>
            <option value="4">4 Adults</option>
            <option value="5">5 Adults</option>
            <option value="6">6 Adults</option>
            <option value="7">7 Adults</option>
            <option value="8">8 Adults</option>
          </select>
        </div>
        <div className="room-config__kids">
          <div className="select-box">
            <select
              className="select select--small"
              value={kids}
              onChange={(e) => occupancyKidsChange(index, e.target.value)}
            >
              <option value="0">Kids</option>
              <option value="1">1 Kid</option>
              <option value="2">2 Kids</option>
              <option value="3">3 Kids</option>
              <option value="4">4 Kids</option>
              <option value="5">5 Kids</option>
              <option value="6">6 Kids</option>
              <option value="7">7 Kids</option>
              <option value="8">8 Kids</option>
            </select>
          </div>
        </div>
        {kidsAgeInfo}
        {removeLink}
      </div>
    </li>
  );
};

RoomOption.propTypes = {
  room: PropTypes.object.isRequired,
  occupancyAdultsChange: PropTypes.func.isRequired,
  occupancyKidsChange: PropTypes.func.isRequired,
  occupancyRemoveRoom: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  id: PropTypes.string,
};

RoomOption.defaultProps = {
  id: '',
};

export default RoomOption;
