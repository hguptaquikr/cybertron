import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const DefaultRoomOption = ({ option, occupancyRoomsChange, selectedRoom, id }) => {
  const selectedClass = cx('occupancy__options__item', {
    'occupancy__options__item--selected': selectedRoom.length
      && +selectedRoom[0].adults === +option.value.adults,
  });
  return (
    <div
      id={id}
      className={selectedClass}
      onClick={() => occupancyRoomsChange([option.value])}
    >
      {option.label}
    </div>
  );
};

DefaultRoomOption.propTypes = {
  option: PropTypes.object.isRequired,
  occupancyRoomsChange: PropTypes.func.isRequired,
  selectedRoom: PropTypes.array.isRequired,
  id: PropTypes.string,
};

DefaultRoomOption.defaultProps = {
  id: '',
};

export default DefaultRoomOption;
