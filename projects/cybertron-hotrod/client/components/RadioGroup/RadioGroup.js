import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withFormsy } from 'formsy-react';
import '../Formsy/formsyInput.css';

class RadioGroup extends Component {
  handleChange = (e) => {
    const { setValue } = this.props;
    setValue(e.currentTarget.value);
  }

  render() {
    const { options, name, containerClassName } = this.props;

    return (
      <div>
        {
          options.map(({ value, label }) => (
            <div className={`formsy-input ${containerClassName}`} key={value}>
              <input
                name={name}
                value={value}
                id={value}
                type="radio"
                className="formsy-input__field"
                onChange={this.handleChange}
              />
              <label
                className="formsy-input__label"
                htmlFor={value}
              >
                {label}
              </label>
            </div>
          ))
        }
      </div>
    );
  }
}

RadioGroup.propTypes = {
  options: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  setValue: PropTypes.func.isRequired,
  containerClassName: PropTypes.string,
};

export default withFormsy(RadioGroup);
