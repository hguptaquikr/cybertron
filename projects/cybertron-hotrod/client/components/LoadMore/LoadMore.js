import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './loadMore.css';

class LoadMore extends Component {
  state = {
    stepCount: 0,
  }

  onLoadMore = () => {
    const { onLoadMore } = this.props;
    const { stepCount } = this.state;
    if (!onLoadMore) {
      this.setState({
        stepCount: stepCount + 1,
      });
    } else {
      onLoadMore();
    }
  }

  render() {
    const { stepCount } = this.state;
    const {
      children,
      childrenOnly,
      containerClassName,
      actionClassName,
      loadMoreText,
      onLoadMore,
    } = this.props;
    const childrenToShow = childrenOnly.initial + (childrenOnly.step * stepCount);
    const isLoadMoreEnabled = !!onLoadMore || React.Children.count(children) > childrenToShow;

    return (
      <div className={containerClassName}>
        {
          !onLoadMore ? (
            <div>
              {
                React.Children.toArray(children).slice(0, childrenToShow)
              }
              {
                isLoadMoreEnabled ? (
                  <div className="hide">
                    {
                      React.Children.toArray(children).slice(childrenToShow)
                    }
                  </div>
                ) : null
              }
            </div>
          ) : children
        }
        {
          isLoadMoreEnabled ? (
            <button
              id="t-qnaLoadMore"
              className={`button ${actionClassName}`}
              onClick={this.onLoadMore}
            >
              {loadMoreText}
            </button>
          ) : null
        }
      </div>
    );
  }
}

LoadMore.propTypes = {
  children: PropTypes.node.isRequired,
  childrenOnly: PropTypes.shape({
    initial: PropTypes.number,
    step: PropTypes.number,
  }),
  containerClassName: PropTypes.string,
  actionClassName: PropTypes.string,
  loadMoreText: PropTypes.string,
  onLoadMore: PropTypes.func,
};

LoadMore.defaultProps = {
  childrenOnly: { initial: 1, step: 1 },
  containerClassName: '',
  actionClassName: '',
  loadMoreText: 'Load more',
};

export default LoadMore;
