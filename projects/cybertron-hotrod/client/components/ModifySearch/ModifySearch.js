import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import SearchWidget from '../../views/SearchWidget/SearchWidget';
import './modifySearch.css';

class ModifySearch extends React.Component {
  state = {
    isSearchWidgetVisible: false,
  };

  componentWillReceiveProps({
    search: {
      searchInput: { location },
      datePicker: { range },
      occupancy: { rooms },
    },
  }) {
    const { isSearchWidgetVisible } = this.state;
    if (
      !(location.label && range.start && range.end && rooms.length)
      && !isSearchWidgetVisible
    ) {
      this.setState({ isSearchWidgetVisible: true });
    }
  }

  toggleSearchWidgetVisibility = () => {
    const { onModifyClick } = this.props;
    this.setState({ isSearchWidgetVisible: !this.state.isSearchWidgetVisible });
    if (onModifyClick) onModifyClick();
  }

  render() {
    const { isSearchWidgetVisible } = this.state;
    const { children, modifyButtonAbove = false } = this.props;

    return (
      <div className="modify">
        {
          isSearchWidgetVisible ? (
            <SearchWidget onSearchClick={this.toggleSearchWidgetVisibility} />
          ) : (
            <div className={cx({ 'flex-row row--middle': modifyButtonAbove })}>
              {children}
              <button
                className="button button--secondary modify__button"
                onClick={this.toggleSearchWidgetVisibility}
              >Modify Search
              </button>
            </div>
          )
        }
      </div>
    );
  }
}

ModifySearch.propTypes = {
  search: PropTypes.object.isRequired,
  children: PropTypes.element.isRequired,
  onModifyClick: PropTypes.func.isRequired,
  modifyButtonAbove: PropTypes.bool,
};

export default ModifySearch;
