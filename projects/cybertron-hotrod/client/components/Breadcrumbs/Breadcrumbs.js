import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import './breadcrumbs.css';

const Breadcrumbs = ({ seo }) => {
  const breadcrumbs = seo.content.breadcrumbs || [];
  return !isEmpty(breadcrumbs) ? (
    <ul className="breadcrumb flex-row" id="t-breadCrumbs" itemScope itemType="http://schema.org/BreadcrumbList">
      {
        breadcrumbs.map((breadcrumb, index) => (
          <li
            key={breadcrumb.label}
            className="breadcrumb__item"
          >
            <span
              itemProp="itemListElement"
              itemScope
              itemType="http://schema.org/ListItem"
            >
              <meta itemProp="position" content={index + 1} />
              <meta itemProp="name" content={breadcrumb.schema} />
              <Link itemProp="item" className="breadcrumb__link" to={breadcrumb.destination || '#'}>
                <span>{breadcrumb.label}</span>
              </Link>
            </span>
          </li>
        ))
      }
    </ul>
  ) : (
    <div style={{ height: '13px' }} />
  );
};

Breadcrumbs.propTypes = {
  seo: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  seo: state.content.seo,
});

export default compose(
  connect(mapStateToProps),
)(Breadcrumbs);
