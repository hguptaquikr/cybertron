import React from 'react';
import PropTypes from 'prop-types';
import './percentageBar.css';

const PercentageBar = ({
  percentage,
}) => (
  <div className="percentage">
    <div
      className="percentage__fill"
      style={{ width: `${percentage}%` }}
    />
  </div>
);

PercentageBar.propTypes = {
  percentage: PropTypes.number.isRequired,
};

export default PercentageBar;
