import React from 'react';
import PropTypes from 'prop-types';
import ReactModal2 from 'react-modal2';
import cx from 'classnames';
import './modal.css';

const Modal = ({ isOpen, children, backdropClassName, modalClassName, ...rest }) => (
  <div>
    {
      isOpen ? (
        <ReactModal2
          backdropClassName={cx('modal', backdropClassName)}
          modalClassName={cx('modal__dialog', modalClassName)}
          {...rest}
        >
          {children}
        </ReactModal2>
      ) : (null)
    }
  </div>
);

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  backdropClassName: PropTypes.string,
  modalClassName: PropTypes.string,
  closeOnEsc: PropTypes.bool,
  closeOnBackdropClick: PropTypes.bool,
};

Modal.defaultProps = {
  closeOnEsc: true,
  closeOnBackdropClick: true,
  onClose: () => {},
};

export default Modal;
