import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import cx from 'classnames';
import { DropdownMenu } from 'react-dd-menu';
import 'react-dd-menu/dist/react-dd-menu.css';
import * as userActionCreators from '../../services/user/userDuck';
import analyticsService from '../../services/analytics/analyticsService';
import Chip from '../../components/Chip/Chip';
import logoWhite from './logo-white.svg';
import logoBlack from './logo-black.svg';
import './navbar.css';

class Navbar extends React.Component {
  state = {
    isDropDownOpen: false,
  }

  onLogout = () => {
    const { userActions, history, location } = this.props;
    userActions.userLogout()
      .then(() => {
        if (location.pathname === '/rewards/') history.push('/introducing-rewards/?flow=rewards');
        analyticsService.logoutClicked();
      });
  }

  closeDropDown = () => {
    this.setState({ isDropDownOpen: false });
  }

  toggleDropDown = () => {
    this.setState({ isDropDownOpen: !this.state.isDropDownOpen });
  }

  alternateNavbarRoutes = [
    '^/$',
    '[a-zA-z]+(-in-)[a-zA-Z-]+/$',
    '[a-zA-z]+(-near-)[a-zA-Z-]+/$',
  ]

  hiddenNavbarRoutes = [
    '/tripadvisor/thank-you/',
  ]

  render() {
    const { isDropDownOpen } = this.state;
    const { user, location, wallet } = this.props;
    const isAlternateNavbar = this.alternateNavbarRoutes.some((route) =>
      location.pathname.match(route));
    const isNavbarHidden = this.hiddenNavbarRoutes.includes(location.pathname);
    const treeboPoints = wallet.byType.TP;
    const showRewardsLink = location.pathname === '/';
    const hasSpendableAmount = treeboPoints.isApplied && treeboPoints.usableBalance;
    const RewardsBadge = {
      text: hasSpendableAmount ? `₹${treeboPoints.usableBalance}` : 'New',
      color: hasSpendableAmount ? 'blue' : 'golden-tainoi',
    };

    return (
      <nav
        className={cx('navbar', {
          'navbar--transparent': isAlternateNavbar,
          hide: isNavbarHidden,
        })}
      >
        <div className="container navbar__header">
          <Link className="navbar__logo" to="/">
            <img
              className="navbar__logo__img"
              src={isAlternateNavbar ? logoWhite : logoBlack}
              alt="Treebo"
            />
          </Link>
          <span className="navbar__menu-items-container">
            <span className="navbar__contact">
              {
                showRewardsLink ? (
                  <Link
                    id="rewards-link"
                    to="/introducing-rewards/?flow=menu"
                    onClick={analyticsService.treeboRewardsClicked}
                  >
                    Treebo Rewards <Chip text={RewardsBadge.text} color={RewardsBadge.color} />
                  </Link>
                ) : null
              }
              <i className="navbar__icon navbar__icon-call icon-phone-call" /><Link to="tel:+919322800100">9322 800 100</Link>
            </span>
          </span>
          {
            user.isAuthenticated ? (
              <DropdownMenu
                isOpen={isDropDownOpen}
                close={this.closeDropDown}
                menuAlign="right"
                animAlign="right"
                textAlign="left"
                toggle={
                  <div
                    id="t-navMyAccount"
                    className="navbar__link navbar__login"
                    onClick={this.toggleDropDown}
                  >My Account
                  </div>
                }
              >
                <li>
                  <Link to="/account/booking-history/" id="t-navBookingHistory">
                    <i className="menu-icon icon-paragraph-left" />
                    BOOKING HISTORY
                  </Link>
                </li>
                <li>
                  <a href="/account/settings" id="t-navChangePassword">
                    <i className="menu-icon icon-lock" />
                    CHANGE PASSWORD
                  </a>
                </li>
                <li>
                  <a onClick={this.onLogout} id="t-navLogout">
                    <i className="menu-icon icon-power-off" />
                    LOGOUT
                  </a>
                </li>
              </DropdownMenu>
            ) : (
              <Link className="navbar__link navbar__login" id="t-navLogin" to="/login/">
                Login
              </Link>
            )
          }
        </div>
      </nav>
    );
  }
}

Navbar.propTypes = {
  user: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  wallet: PropTypes.object,
};

const mapStateToProps = (state) => ({
  user: state.user,
  wallet: state.wallet,
});

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(Navbar);
