import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './ratePlanTag.css';

const RatePlanTag = ({ className, rateType }) => {
  const ratePlanText = {
    'non-refundable': 'Non-Refundable',
    refundable: 'Refundable',
  };
  const ratePlansClass = cx('rate-plan-tag', {
    'rate-plan-tag--non-refundable': rateType === 'non-refundable',
    'rate-plan-tag--refundable': rateType === 'refundable',
  }, className);
  return (
    <span className={ratePlansClass} id="t-ratePlanTag">
      {ratePlanText[rateType]}
    </span>
  );
};

RatePlanTag.propTypes = {
  rateType: PropTypes.string,
  className: PropTypes.string,
};

export default RatePlanTag;
