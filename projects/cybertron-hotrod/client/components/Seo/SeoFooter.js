import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import logoBlack from '../Navbar/logo-black.svg';
import './seoLinksFooter.css';

const SeoFooter = ({ cities, content: [noOfCities, noOfHotels] }) => (
  <section className="seo-footer">
    <div className="container">
      <h2 className="heading-2 seo-footer__copyright">Copyright © 2014 - 2018, Treebo.com</h2>
      <p className="seo-footer__text">
        Treebo Hotels provides transparent online hotel booking experience
        to all its customers. It’s India’s #1 branded budget hotels with
        its presence in {parseInt(noOfCities, 10)}+ cities (including the metro cities of Delhi,
        Mumbai, Pune, Ahmedabad, Bangalore, Chennai, Hyderabad, Kolkata)
        and more than {parseInt(noOfHotels, 10)} hotels.
      </p>
      <p className="seo-footer__text">
        At Treebo Hotels you get quality stays, affordable rates, amazing
        offers, flexibilities such as book now pay later, instant hotel
        booking confirmations, 100% money back guarantee,
        free cancellations, and a 24x7 available Guest Delight team to
        answer booking or travel related queries. All these give our guests
        the freedom to book a hotel first and plan their travel next.
      </p>
      <p className="seo-footer__text">
        With no compromise on quality, every Treebo booking comes along with
        three promises – clean and fresh rooms, assured essentials and
        30-minute service guarantee.
      </p>
      <p className="seo-footer__text">
        We have a set of standard Treebo offerings such as air-conditioned
        rooms (not applicable for hill stations), free Wi-Fi, complimentary
        breakfast, TV with DTH connection, complimentary bottled water,
        electric kettle, fully functional bathroom, spotless, fresh linens
        (towels, bedsheets) and Treebo toiletries. So, if you find any of
        these dysfunctional, let us know and we’ll fix it within 30 minutes.
      </p>
      <p className="seo-footer__text">
        Treebo is the fastest growing chain of budget hotels and has been
        recognised for its good reviews across all travel platforms as well.
        Book a stay with us to experience delightful service, interactive,
        enthusiastic, warm staff behaviour, the best amenities, and believe
        it yourself.
      </p>
      <h2 className="subheading-3 seo-footer__title">Cities</h2>
      <div className="seo-footer__text seo-footer__cities">
        {
          cities.map((city) => (
            <a className="seo-footer__link" href={`${city.url}/`} key={city.name}>
              {city.name}
            </a>
          ))
        }
      </div>
      <Link className="seo-footer__logo" to="/">
        <img className="seo-footer__logo__image" src={logoBlack} alt="Treebo" />
      </Link>
    </div>
  </section>
);

SeoFooter.propTypes = {
  cities: PropTypes.array.isRequired,
  content: PropTypes.array.isRequired,
};

export default SeoFooter;
