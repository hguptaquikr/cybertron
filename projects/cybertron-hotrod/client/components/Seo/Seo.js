/* eslint-disable react/no-danger */
import React from 'react';
import PropTypes from 'prop-types';

const Seo = ({
  index,
  children,
}) => {
  if (index === false) {
    return (
      <div>
        <div dangerouslySetInnerHTML={{ __html: '<!--googleoff: all-->' }} />
        {children}
        <div dangerouslySetInnerHTML={{ __html: '<!--googleon: all-->' }} />
      </div>
    );
  }
  return (
    <div>{children}</div>
  );
};

Seo.propTypes = {
  children: PropTypes.node,
  index: PropTypes.bool,
};

export default Seo;
