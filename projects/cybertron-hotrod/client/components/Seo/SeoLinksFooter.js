import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import get from 'lodash/get';
import contentService from '../../services/content/contentService';
import analyticsService from '../../services/analytics/analyticsService';
import './seoLinksFooter.css';

class SeoFooter extends React.Component {
  state = {
    selectedHeaderIndex: 0,
  };

  componentWillReceiveProps() {
    this.setState({ selectedHeaderIndex: 0 });
  }

  selectHeader = (selectedHeaderIndex, title) => () => {
    this.setState({ selectedHeaderIndex });
    analyticsService.seoFooterHeaderClicked(title);
  };

  renderSeoFooterItem = (group, show) => (
    <div
      className={cx('seo-linksfooter__list row row--middle', {
        hide: !show,
      })}
    >
      {
        group.content.map((place) => {
          let label = place.label.split(',')[0];
          if (place.type === 'city') {
            label = `Hotels in ${label}`;
          } else if (place.type === 'category') {
            label = `${label} in ${get(place, 'area.city')}`;
          }
          return (
            <Link
              id="t-seoLinksFooterLink"
              key={label}
              className="text-2 seo-linksfooter__list-item col-3"
              to={contentService.getSeoPathname(place)}
              onClick={() => analyticsService.seoFooterItemClicked(group.key, label)}
            >
              {label}
            </Link>
          );
        })
      }
    </div>
  )

  renderSeoFooterGroups = (groups) => {
    const { selectedHeaderIndex } = this.state;
    return (
      <div className="container seo-linksfooter__content">
        {
          groups.map((group, i) =>
            this.renderSeoFooterItem(group, selectedHeaderIndex === i))
        }
      </div>
    );
  }

  render() {
    const { selectedHeaderIndex } = this.state;
    const { links, match } = this.props;
    let groups = links.filter(({ content }) => content.length);
    if (match.params.locality || match.params.landmark) {
      groups = groups.filter((link) => link.key !== 'hotel_types');
    } else if (match.params.city) {
      groups = groups.filter((link) => link.key !== 'popular_amenities');
    }

    return groups.length ? (
      <section className="seo-linksfooter">
        <div className="container">
          <div>
            <ul className="seo-linksfooter__filters-list">
              {
                groups.map((group, index) => (
                  <li
                    id="t-seoLinksFooterTab"
                    key={group.title}
                    className={cx('text-1 seo-linksfooter__filters-item', {
                      'seo-linksfooter__filters-item--active': index === selectedHeaderIndex,
                    })}
                    onClick={this.selectHeader(index, group.title)}
                  >
                    <div>{group.title.toUpperCase()}</div>
                  </li>
                ))
              }
            </ul>
          </div>
        </div>
        {this.renderSeoFooterGroups(groups)}
      </section>
    ) : null;
  }
}

SeoFooter.propTypes = {
  match: PropTypes.object,
  links: PropTypes.array,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

export default compose(
  withRouter,
  connect(mapStateToProps),
)(SeoFooter);
