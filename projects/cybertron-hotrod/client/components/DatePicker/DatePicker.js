import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { DateRangePicker } from 'react-dates';
import { START_DATE, END_DATE } from 'react-dates/constants';
import 'react-dates/lib/css/_datepicker.css';
import { constants } from '../../services/utils';
import './datePicker.css';

class DatePicker extends React.Component {
  state = {
    focusedInput: null,
  }

  componentWillMount() {
    if (__BROWSER__) {
      this.setState({ focusedInput: this.getInputToFocus(this.props) });
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ focusedInput: this.getInputToFocus(nextProps) });
  }

  onDatesChange = ({ startDate, endDate }) => {
    const { dateRangeChange } = this.props;
    dateRangeChange({
      start: startDate,
      end: endDate,
    });
    this.props.toggleSearchButton();
  }

  onFocusChange = (focusedInput) => {
    this.setState({ focusedInput });
  }

  getInputToFocus = ({ datePicker: { range, isVisible } }) => {
    if (range.start && !range.end) {
      return END_DATE;
    } else if (isVisible) {
      if (!range.start) return START_DATE;
      else if (!range.end) return END_DATE;
      return null;
    }
    return null;
  }

  initialVisibleMonth = () => {
    const { datePicker: { range } } = this.props;
    const { focusedInput } = this.state;
    if (focusedInput === START_DATE && range.start) return range.start;
    else if (focusedInput === END_DATE && range.end) return range.end;
    return moment();
  }

  render() {
    const { datePicker: { range } } = this.props;
    const { focusedInput } = this.state;

    return (
      <div className="row date-picker">
        <DateRangePicker
          startDatePlaceholderText="Check In"
          endDatePlaceholderText="Check Out"
          startDate={range.start}
          startDateId={`${range.start}`}
          endDate={range.end}
          endDateId={`${range.end}`}
          focusedInput={focusedInput}
          onFocusChange={this.onFocusChange}
          onDatesChange={this.onDatesChange}
          displayFormat={constants.dateFormat.view}
          initialVisibleMonth={this.initialVisibleMonth}
          customArrowIcon={<i className="icon-arrow-right" />}
          navPrev={<i className="icon-arrow-left" />}
          navNext={<i className="icon-arrow-right" />}
          phrases={{ keyboardNavigationInstructions: '' }}
        />
      </div>
    );
  }
}

DatePicker.propTypes = {
  datePicker: PropTypes.object.isRequired,
  dateRangeChange: PropTypes.func.isRequired,
  toggleSearchButton: PropTypes.func.isRequired,
};

export default DatePicker;
