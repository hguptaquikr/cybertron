import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const ApplicableCoupons = ({ coupons, couponCode, setCoupon }) => (
  <div className="coupon__applicable">
    <ul className="coupon__list">
      {
        coupons.map(({
          code,
          coupon_amount: couponAmount,
          description,
          tagline,
        }) => (
          <li
            key={code}
            className={cx('coupon__list-item', {
              'coupon__list-item--active': couponCode === code,
            })}
            id="t-couponListItem"
            onClick={setCoupon(code)}
          >
            <input
              className="coupon__radio"
              type="radio"
              name="coupon"
              value={code}
              checked={couponCode === code}
            />
            <div className="coupon__details text-3">
              <span className="coupon__code">{code}</span>
              <strong>
                Max. discount upto <span className="coupon__amt">&#x20b9;{couponAmount}</span>
              </strong>
            </div>
            <p className="text-3 coupon__description">{tagline}</p>
            <p className="text-3 coupon__description">{description}</p>
          </li>
        ))
      }
    </ul>
  </div>
);

ApplicableCoupons.propTypes = {
  coupons: PropTypes.array,
  couponCode: PropTypes.string,
  setCoupon: PropTypes.func,
};

export default ApplicableCoupons;
