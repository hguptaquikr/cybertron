import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import get from 'lodash/get';
import Modal from '../Modal/Modal';
import ApplicableCoupons from './ApplicableCoupons';
import * as abService from '../../services/ab/abService';
import './coupon.css';

class Coupon extends React.Component {
  state = {
    couponCode: this.props.couponCode,
  };

  onInputChange = ({ target }) => {
    this.setState({ couponCode: target.value });
  };

  onSubmit = (e) => {
    const { onSubmit } = this.props;
    const { couponCode } = this.state;
    e.preventDefault();
    onSubmit(couponCode);
  };

  setCoupon = (couponCode) => () => {
    const { onSubmit } = this.props;
    this.setState({ couponCode });
    onSubmit(couponCode);
  };

  render() {
    const { couponCode } = this.state;
    const { isCouponLoading, isOpen, onClose, coupons, couponError } = this.props;
    const applyCtaText = get(abService.getExperiments(), 'checkoutCouponDesktop.payload.text', 'Apply');

    return (
      <Modal modalClassName="coupon" isOpen={isOpen} onClose={onClose}>
        <div className="coupon__header">
          <h2 className="heading-1">Apply Coupon Code</h2>
          <i className="icon-cross coupon__close" onClick={onClose} />
        </div>
        <div className="coupon__body">
          <form className="coupon__form" onSubmit={this.onSubmit}>
            <input
              id="t-couponInput"
              className="coupon__input"
              type="text"
              disabled={isCouponLoading}
              onChange={this.onInputChange}
              value={couponCode}
            />
            <input
              className={cx('button coupon__button', {
                'coupon__button--disabled': !couponCode,
                'coupon__button--loading': isCouponLoading,
              })}
              id="t-applyCoupon"
              type="submit"
              disabled={isCouponLoading || !couponCode}
              value={isCouponLoading ? 'Applying...' : applyCtaText}
            />
          </form>
          {
            couponError ? (
              <div className="text-error coupon__error" id="t-couponError">{couponError}</div>
            ) : null
          }
          <ApplicableCoupons
            coupons={coupons}
            couponCode={couponCode}
            setCoupon={this.setCoupon}
          />
        </div>
      </Modal>
    );
  }
}

Coupon.propTypes = {
  couponCode: PropTypes.string,
  couponError: PropTypes.string,
  isCouponLoading: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  coupons: PropTypes.array,
};

const CouponWrapper = ({ isOpen, ...props }) => (
  isOpen ? <Coupon isOpen={isOpen} {...props} /> : null
);

CouponWrapper.propTypes = {
  isOpen: PropTypes.bool.isRequired,
};

export default CouponWrapper;
