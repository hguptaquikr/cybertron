import React from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import './OtpScreen.css';
import FormsyInput from '../Formsy/FormsyInput';

const OtpScreen = ({
  onResend,
  onVerify,
  onEdit,
  mobile,
  isVerifyingOtp,
}) => (
  <div className="otp-screen">
    <h2 className="heading-1">Verify your Mobile Number</h2>
    <div className="otp-screen__subtitle">
      <p>
        A One Time Password (OTP) has been sent via SMS to <strong>{mobile}</strong>.
        <small className="otp-screen__edit text-link" onClick={onEdit} id="t-changeNumber"> Change Number</small>
      </p>
    </div>
    <Formsy className="checkout__form" onValidSubmit={onVerify}>
      <FormsyInput
        id="t-otpInput"
        type="text"
        name="otp"
        validations="isNumeric"
        validationError="Please enter a valid OTP"
        label="OTP"
        hintText="Enter the OTP"
        required
      />
      <p className="otp-screen__text">
        Haven&apos;t received the code yet?
        <small href="" className="otp-screen__action text-link" id="resend" onClick={onResend}>Resend OTP</small>
      </p>
      <div>
        <button
          type="submit"
          id="verify"
          className="button otp-screen__verify-btn"
          disabled={isVerifyingOtp}
        >
          {isVerifyingOtp ? 'Verifying...' : 'Verify'}
        </button>
      </div>
    </Formsy>
  </div>
);

OtpScreen.propTypes = {
  onResend: PropTypes.func.isRequired,
  onVerify: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  mobile: PropTypes.string.isRequired,
  isVerifyingOtp: PropTypes.bool.isRequired,
};
export default OtpScreen;
