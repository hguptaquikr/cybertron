import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import OtpScreen from './OtpScreen';
import NumScreen from './NumScreen';

const Otp = ({
  onReject,
  onResend,
  sendOtp,
  onVerifyOtp,
  onEditMobile,
  onNumberSubmit,
  mobile,
  user,
  otp,
}) => (
  <div>
    {
      mobile ?
        <OtpScreen
          onVerify={onVerifyOtp}
          onReject={onReject}
          onResend={onResend}
          onEdit={onEditMobile}
          mobile={mobile}
          isVerifyingOtp={user.isVerifyingOtp || otp.isVerifyingOtp}
        />
        : null
    }
    {
      !mobile ?
        <NumScreen
          sendOtp={sendOtp}
          onSubmit={onNumberSubmit}
          onReject={onReject}
        />
        : null
    }
  </div>
);

Otp.propTypes = {
  onReject: PropTypes.func,
  mobile: PropTypes.string.isRequired,
  onResend: PropTypes.func.isRequired,
  onEditMobile: PropTypes.func.isRequired,
  sendOtp: PropTypes.func.isRequired,
  onVerifyOtp: PropTypes.func.isRequired,
  onNumberSubmit: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  otp: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  otp: state.otp,
});

export default connect(
  mapStateToProps,
)(Otp);
