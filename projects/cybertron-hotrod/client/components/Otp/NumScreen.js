import React from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import FormsyInput from '../Formsy/FormsyInput';

const NumScreen = ({ sendOtp, onSubmit }) => (
  <div className="otp-screen">
    <h1 className="heading-1 au-header">Continue with your mobile no.</h1>
    <p className="otp-screen__subtitle">
      An OTP will be  sent via SMS to verify your number.
    </p>
    <Formsy className="checkout__form" onSubmit={onSubmit} onValidSubmit={sendOtp}>
      <FormsyInput
        type="text"
        name="mobile"
        validations="isNumeric,isLength:10"
        validationError="Please enter a valid Mobile Number"
        label="Mobile Number"
        required
      />
      <div>
        <button
          type="submit"
          id="verify"
          className="button otp-screen__verify-btn"
        >Verify
        </button>
      </div>
    </Formsy>
  </div>
);

NumScreen.propTypes = {
  sendOtp: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default NumScreen;
