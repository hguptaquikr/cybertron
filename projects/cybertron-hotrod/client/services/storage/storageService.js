import cookies from 'js-cookie';

const serverStorage = {};

export default {
  setItem(key, value, expiry = 30) {
    if (__BROWSER__) {
      if (window.localStorage) {
        window.localStorage.setItem(key, value);
      } else if (window.sessionStorage) {
        window.sessionStorage.setItem(key, value);
      } else {
        cookies.set(key, value, { expires: expiry });
      }
    } else {
      serverStorage[key] = value;
    }
  },

  getItem(key) {
    if (__BROWSER__) {
      if (window.localStorage) {
        return window.localStorage.getItem(key);
      } else if (window.sessionStorage) {
        return window.sessionStorage.getItem(key);
      }
      return cookies.get(key);
    }
    return serverStorage[key];
  },

  removeItem(key) {
    if (__BROWSER__) {
      if (window.localStorage) {
        window.localStorage.removeItem(key);
      } else if (window.sessionStorage) {
        window.sessionStorage.removeItem(key);
      } else {
        cookies.remove(key);
      }
    } else {
      serverStorage[key] = undefined;
    }
  },
};
