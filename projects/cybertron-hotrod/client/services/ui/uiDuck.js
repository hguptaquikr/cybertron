import cookies from 'js-cookie';
import qs from 'query-string';

const SET_UTM_PARAMS = 'SET_UTM_PARAMS';

const initialState = {
  utmParams: {
    utm_source: '',
    utm_medium: '',
    utm_campaign: '',
  },
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_UTM_PARAMS:
      return {
        ...state,
        utmParams: payload,
      };
    default:
      return state;
  }
};

export const setUtmParams = (search) => (dispatch) => {
  const queryParams = qs.parse(search);
  queryParams.utm_source = queryParams.utm_source || 'direct';
  cookies.set('utm_source', queryParams.utm_source, { expires: 30 });
  dispatch({
    type: SET_UTM_PARAMS,
    payload: {
      utm_source: queryParams.utm_source,
      utm_medium: queryParams.utm_medium,
      utm_campaign: queryParams.utm_campaign,
    },
  });
};
