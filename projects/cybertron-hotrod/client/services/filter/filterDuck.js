import groupBy from 'lodash/groupBy';
import countBy from 'lodash/countBy';
import isEmpty from 'lodash/isEmpty';
import { omitKeys } from '../utils';
import priceService from '../price/priceService';
import contentService from '../content/contentService';
import * as hotelService from '../hotel/hotelService';

const SORT_CHANGE = 'SORT_CHANGE';
const CLEAR_ALL_FILTERS = 'CLEAR_ALL_FILTERS';
const GET_RESULTS_REQUEST = 'GET_RESULTS_REQUEST';
const GET_HOTEL_RESULTS_SUCCESS = 'GET_HOTEL_RESULTS_SUCCESS';
const FILTERS_CHECKBOX_CHANGE = 'FILTERS_CHECKBOX_CHANGE';
const GET_AMENITIES_FILTER_PENDING = 'GET_AMENITIES_FILTER_PENDING';
const GET_AMENITIES_FILTER_RESOLVED = 'GET_AMENITIES_FILTER_RESOLVED';
const GET_AMENITIES_FILTER_REJECTED = 'GET_AMENITIES_FILTER_REJECTED';
const CONSTRUCT_LOCALITIES_FILTER = 'CONSTRUCT_LOCALITIES_FILTER';
const CONSTRUCT_PRICE_RANGES_FILTER = 'CONSTRUCT_PRICE_RANGES_FILTER';
const CLEAR_RESULTS = 'CLEAR_RESULTS';
const SET_REFINED_HOTEL_IDS = 'SET_REFINED_HOTEL_IDS';

const initialState = {
  filters: {
    priceRanges: [],
    localities: [],
    amenities: [],
    showAvailableOnly: [{ checked: false }],
    showCoupleFriendlyOnly: [{ checked: false }],
    showTopRatedOnly: [{ checked: false }],
    showFrequentlyBookedOnly: [{ checked: false }],
    showValueForMoneyOnly: [{ checked: false }],
    distanceCap: null,
  },
  sort: {
    by: 'price',
    list: ['price'],
    coordinates: { lat: '', lng: '' },
  },
  refinedHotelIds: [],
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_RESULTS_REQUEST:
      return {
        ...state,
        refinedHotelIds: [],
      };

    case GET_HOTEL_RESULTS_SUCCESS:
      return {
        ...state,
        filters: {
          ...state.filters,
          distanceCap: payload.response.filters.distanceCap,
        },
        sort: {
          ...state.sort,
          list: payload.sort.list || payload.response.sort.list,
          by: payload.sort.by || payload.response.sort.by,
          coordinates: payload.coordinates.lat
            ? payload.coordinates
            : payload.response.sort.coordinates,
        },
        refinedHotelIds: payload.response.ids,
      };

    case SORT_CHANGE:
      return {
        ...state,
        sort: {
          ...state.sort,
          by: payload.by,
        },
      };
    case CLEAR_ALL_FILTERS:
      return {
        ...state,
        filters: payload.clearedFilters,
      };
    case CONSTRUCT_LOCALITIES_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          localities: payload.localities,
        },
      };
    case CONSTRUCT_PRICE_RANGES_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          priceRanges: payload.priceRanges,
        },
      };
    case GET_AMENITIES_FILTER_RESOLVED:
      return {
        ...state,
        filters: {
          ...state.filters,
          amenities: payload.response
            .map((amenity) => ({ ...amenity, checked: false })),
        },
      };
    case FILTERS_CHECKBOX_CHANGE: {
      const { filterKey, i: index } = payload;
      return {
        ...state,
        filters: {
          ...state.filters,
          [filterKey]: [
            ...state.filters[filterKey].slice(0, index),
            {
              ...state.filters[filterKey][index],
              checked: payload.checked,
            },
            ...state.filters[filterKey].slice(index + 1),
          ],
        },
      };
    }
    case SET_REFINED_HOTEL_IDS:
      return {
        ...state,
        refinedHotelIds: payload.refinedHotelIds,
      };
    case CLEAR_RESULTS:
      return {
        ...state,
        refinedHotelIds: [],
      };
    default:
      return state;
  }
};

export const clearResults = () => ({
  type: CLEAR_RESULTS,
});

export const sortChange = (by) => ({
  type: SORT_CHANGE,
  payload: { by },
});

const _sortResults = (hotelIds, getState) => {
  const {
    search: { searchInput },
    hotel,
    price,
    filter: { sort },
  } = getState();

  // break if no results to sort
  if (isEmpty(hotelIds)) return hotelIds;

  // sort by
  let sortedHotelIds = [...hotelIds];
  const hasPrices = price.roomTypeIds.length > 0;

  if (sort.by === 'price' && hasPrices) {
    sortedHotelIds.sort((hotelIdA, hotelIdB) => {
      const hotelPricesA = priceService
        .getPricesForRoomTypeIds(hotel.byId[hotelIdA].roomTypeIds, price.byRoomTypeId);
      const cheapestPriceA = priceService.sortPrice(hotelPricesA)[0];
      const cheapestRatePlanA = priceService.sortRatePlan(cheapestPriceA.ratePlans)[0] || {};

      const hotelPricesB = priceService
        .getPricesForRoomTypeIds(hotel.byId[hotelIdB].roomTypeIds, price.byRoomTypeId);
      const cheapestPriceB = priceService.sortPrice(hotelPricesB)[0];
      const cheapestRatePlanB = priceService.sortRatePlan(cheapestPriceB.ratePlans)[0] || {};
      return cheapestRatePlanA.sellingPrice - cheapestRatePlanB.sellingPrice;
    });
  } else if (sort.by === 'distance') {
    sortedHotelIds.sort((a, b) => hotel.byId[a].distance - hotel.byId[b].distance);
  } else if (sort.by === 'recommended' && hasPrices) {
    sortedHotelIds.sort((a, b) =>
      (hotel.byId[a].rank.recommended - hotel.byId[b].rank.recommended));
  }

  // group by availability
  if (hasPrices) {
    const groupedResults = groupBy(
      sortedHotelIds,
      (hotelId) => {
        if (hotel.byId[hotelId].distance === 0) {
          return true;
        }
        const hotelPrices = priceService.getPricesForRoomTypeIds(
          hotel.byId[hotelId].roomTypeIds,
          price.byRoomTypeId,
        );
        const cheapestRoom = priceService.sortPrice(hotelPrices)[0];
        return !!cheapestRoom.available;
      },
    );
    sortedHotelIds = [];
    sortedHotelIds = groupedResults.true
      ? sortedHotelIds.concat(groupedResults.true) : sortedHotelIds;
    sortedHotelIds = groupedResults.false
      ? sortedHotelIds.concat(groupedResults.false) : sortedHotelIds;
  }

  // move searched unavailable hotel to top
  if (searchInput.location.hotel_id) {
    const hotelId = searchInput.location.hotel_id;
    sortedHotelIds = sortedHotelIds.reduce((acc, result) => {
      if (result.id === +hotelId) acc.unshift(result);
      else acc.push(result);
      return acc;
    }, []);
  }

  return sortedHotelIds;
};

export const clearAllFilters = () => (dispatch, getState) => {
  const { filters } = getState().filter;
  const clearedFilters = { ...filters };
  Object
    .keys(clearedFilters)
    .forEach((filterKey) => {
      if (filterKey === 'distanceCap') return;
      filters[filterKey].forEach((filterObj) => {
        filterObj.checked = false; // eslint-disable-line
      });
    });

  dispatch({
    type: CLEAR_ALL_FILTERS,
    payload: { clearedFilters },
  });
};

export const filtersCheckboxChange = (filterKey, i, checked) => ({
  type: FILTERS_CHECKBOX_CHANGE,
  payload: {
    filterKey,
    i,
    checked,
  },
});

export const constructPriceRangesFilter = () => (dispatch, getState) => {
  const { price, hotel } = getState();
  const sortedPrices = hotel.ids
    .map((hotelId) => {
      const hotelPrices = priceService.getPricesForRoomTypeIds(
        hotel.byId[hotelId].roomTypeIds,
        price.byRoomTypeId,
      );
      const cheapestPrice = priceService.sortPrice(hotelPrices)[0];
      const cheapestRatePlan = priceService.sortRatePlan(cheapestPrice.ratePlans)[0];
      return cheapestRatePlan.sellingPrice;
    })
    .sort((a, b) => (a - b));
  const minPrice = sortedPrices[0];
  const maxPrice = sortedPrices[sortedPrices.length - 1];
  const incPrice = Math.round((maxPrice - minPrice) / 3);
  let currentPrice = Math.round(minPrice);
  const priceRanges = [0, 1, 2].map(() => {
    const priceRange = {};
    priceRange.start = currentPrice;
    priceRange.end = currentPrice + incPrice;
    priceRange.name = `&#x20b9;${priceRange.start} - &#x20b9;${priceRange.end}`;
    priceRange.checked = false;
    currentPrice += incPrice;
    return priceRange;
  });

  dispatch({
    type: CONSTRUCT_PRICE_RANGES_FILTER,
    payload: { priceRanges },
  });
};

export const constructLocalitiesFilter = () => (dispatch, getState) => {
  const { hotel } = getState();
  const distinctLocalities = countBy(omitKeys(hotel.byId, ['0']), 'address.locality');
  const sortedDistinctLocalities =
    Object.keys(distinctLocalities).sort((a, b) => distinctLocalities[b] - distinctLocalities[a])
      .map((locality) => ({
        name: locality,
        checked: false,
      }));

  dispatch({
    type: CONSTRUCT_LOCALITIES_FILTER,
    payload: { localities: sortedDistinctLocalities },
  });
};

export const getAmenitiesFilter = (route, params) => (dispatch, getState, { api }) => {
  const seoQuery = contentService.makeSeoQuery(route, params);

  dispatch({
    types: [
      GET_AMENITIES_FILTER_PENDING,
      GET_AMENITIES_FILTER_RESOLVED,
      GET_AMENITIES_FILTER_REJECTED,
    ],
    promise: () => api.get('/api/web/v1/facilities/', seoQuery),
  });
};

const _filterResults = (hotelIds, getState) => {
  const {
    price,
    hotel,
    filter: { filters },
  } = getState();
  const hasPrices = price.roomTypeIds.length > 0;
  const checkedPriceRanges = filters.priceRanges.filter((priceRange) => priceRange.checked);
  const checkedLocalities = filters.localities.filter((locality) => locality.checked);
  const checkedAmenities = filters.amenities.filter((amenity) => amenity.checked);
  const shouldFilterWithinDistanceCap = !!filters.distanceCap;
  const shouldFilterShowAvailableOnly = filters.showAvailableOnly[0].checked && hasPrices;
  const shouldFilterPriceRanges = checkedPriceRanges.length && hasPrices;
  const shouldFilterLocalities = checkedLocalities.length;
  const shouldFilterAmenities = checkedAmenities.length;
  const shouldFilterCoupleFriendlyOnly = filters.showCoupleFriendlyOnly[0].checked;

  const withinDistanceCap = (hotelDetail) => hotelDetail.distance <= filters.distanceCap;

  const isAvailable = (priceDetail) => !!priceDetail.available;

  const inPriceRange = (priceRanges, ratePlan) =>
    priceRanges.some((priceRange) =>
      Math.round(ratePlan.sellingPrice) >= Math.round(priceRange.start)
      && Math.round(ratePlan.sellingPrice) <= Math.round(priceRange.end));

  const inLocalities = (localities, result) =>
    localities.some((locality) => result.address.locality === locality.name);

  const hasAmenities = (amenities, result) =>
    amenities.every((amenity) => result.amenities.includes(amenity.id));

  const filteredHotelIds = hotelIds.filter((hotelId) => {
    const selectedHotel = hotel.byId[hotelId];
    const hotelPrices = priceService.getPricesForRoomTypeIds(
      selectedHotel.roomTypeIds,
      price.byRoomTypeId,
    );
    const cheapestRoomPrice = priceService.sortPrice(hotelPrices)[0];
    const cheapestRatePlan = priceService.sortRatePlan(cheapestRoomPrice.ratePlans)[0];
    let status = true;
    if (shouldFilterWithinDistanceCap) {
      status = status && withinDistanceCap(selectedHotel);
    }
    if (shouldFilterShowAvailableOnly) {
      status = status && isAvailable(cheapestRoomPrice);
    }
    if (shouldFilterPriceRanges) {
      status = status && inPriceRange(checkedPriceRanges, cheapestRatePlan);
    }
    if (shouldFilterLocalities) {
      status = status && inLocalities(checkedLocalities, selectedHotel);
    }
    if (shouldFilterAmenities) {
      status = status && hasAmenities(checkedAmenities, selectedHotel);
    }
    if (shouldFilterCoupleFriendlyOnly) {
      status = status && hotelService.isCoupleFriendly(selectedHotel.policies);
    }
    return status;
  });
  return filteredHotelIds;
};

export const refineResults = () => (dispatch, getState) => {
  const { hotel } = getState();
  const filteredHotelIds = _filterResults(hotel.ids, getState);
  const sortedHotelIds = _sortResults(filteredHotelIds, getState);

  dispatch({
    type: SET_REFINED_HOTEL_IDS,
    payload: { refinedHotelIds: sortedHotelIds },
  });
};

export const sortResultsBy = (selected) => (dispatch) => {
  dispatch(sortChange(selected));
  dispatch(refineResults());
};
