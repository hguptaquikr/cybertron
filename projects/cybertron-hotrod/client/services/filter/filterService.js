import qs from 'query-string';
import searchService from '../search/searchService';

export default {
  sortByNearMe({ location, history, city }) {
    searchService.getGeoLocation()
      .then((coordinates) => {
        const query = qs.parse(location.search);
        history.replace({
          ...location,
          pathname: '/search/',
          search: qs.stringify({
            ...query,
            city: city || query.city,
            lat: coordinates.lat,
            lng: coordinates.lng,
            q: city || query.q,
          }),
        });
      });
  },
};
