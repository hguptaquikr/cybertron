import { combineReducers } from 'redux';
import { loadingBarReducer } from 'react-redux-loading-bar';
import userReducer from '../user/userDuck';
import searchReducer from '../search/searchDuck';
import filterReducer from '../filter/filterDuck';
import hotelReducer from '../hotel/hotelDuck';
import roomTypeReducer from '../roomType/roomTypeDuck';
import priceReducer from '../price/priceDuck';
import itineraryReducer from '../checkout/checkoutDuck';
import confirmationReducer from '../booking/bookingDuck';
import otpReducer from '../otp/otpDuck';
import contentReducer from '../content/contentDuck';
import cancelReducer from '../user/cancelDuck';
import walletReducer from '../wallet/walletDuck';
import uiReducer from '../ui/uiDuck';

export default combineReducers({
  loadingBar: loadingBarReducer,
  user: userReducer,
  hotel: hotelReducer,
  $roomType: roomTypeReducer,
  price: priceReducer,
  search: searchReducer,
  filter: filterReducer,
  itinerary: itineraryReducer,
  confirmation: confirmationReducer,
  otp: otpReducer,
  content: contentReducer,
  cancellation: cancelReducer,
  wallet: walletReducer,
  ui: uiReducer,
});
