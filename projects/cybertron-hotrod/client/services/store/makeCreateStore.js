import { createStore, compose, applyMiddleware } from 'redux';
import { default as reduxThunk } from 'redux-thunk';
import { loadingBarMiddleware } from 'react-redux-loading-bar';
import apiService from '../../services/api/apiService';
import asyncMiddleware from '../api/asyncReduxMiddleware';
import authMiddleware from '../auth/authReduxMiddleware';
import rootReducer from './rootReducer';

const makeCreateStore = ({
  history,
}) => {
  const middlewares = [
    reduxThunk.withExtraArgument({ api: apiService, history }),
    asyncMiddleware,
    authMiddleware({ history }),
    loadingBarMiddleware({ promiseTypeSuffixes: ['REQUEST', 'SUCCESS', 'FAILURE'] }),
  ].filter(Boolean);

  const storeEnhancers = [
    applyMiddleware(...middlewares),
    __LOCAL__ && __BROWSER__ && window.devToolsExtension ? window.devToolsExtension() : (f) => f,
  ].filter(Boolean);

  return (initialState) => createStore(
    rootReducer,
    initialState,
    compose(...storeEnhancers),
  );
};

export default makeCreateStore;
