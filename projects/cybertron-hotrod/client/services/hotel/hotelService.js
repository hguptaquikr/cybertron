import Hashids from 'hashids';
import qs from 'query-string';
import kebabCase from 'lodash/kebabCase';
import roomTypeService from '../roomType/roomTypeService';

const coupleFriendlyPolicies = ['is_couple_friendly', 'is_local_id_allowed'];

const sortRoomType = (data) => {
  const roomTypePriority = ['common', 'acacia', 'oak', 'maple', 'mahogany'];
  const byRoomTypePriority = (a, b) =>
    roomTypePriority.indexOf(a.type || a) - roomTypePriority.indexOf(b.type || b);
  return data.sort(byRoomTypePriority);
};

export const transformPolicies = (policies) => {
  const sortedPolicies = [];
  const otherPolicies = [];
  policies.forEach((policy) => {
    const formattedPolicy = {
      isPolicy: policy.display_in_policy,
      isNeedToKnow: policy.display_in_need_to_know,
      description: policy.description,
      title: policy.title,
      type: policy.policy_type,
    };
    if (formattedPolicy.type === coupleFriendlyPolicies[0]) {
      sortedPolicies[0] = formattedPolicy;
    } else if (formattedPolicy.type === coupleFriendlyPolicies[1]) {
      sortedPolicies[1] = formattedPolicy;
    } else otherPolicies.push(formattedPolicy);
  });
  return [
    ...sortedPolicies.filter(Boolean),
    ...otherPolicies,
  ];
};

export const transformHotelResultsApi = ({ data }) => ({
  data: {
    byId: data.result.reduce((obj, result) => ({
      ...obj,
      [result.id]: {
        id: result.id,
        name: result.hotelName,
        coordinates: result.coordinates,
        address: result.area,
        roomTypeIds: [],
        description: '',
        amenities: result.amenities,
        accessibilities: [],
        trilights: [],
        isActive: result.is_active,
        images: result.images,
        isTopRated: result.isFourPlusRated,
        isFrequentlyBooked: result.isFrequentlyBooked,
        isValueForMoney: result.isValueForMoney,
        reviews: {
          award: result.ta_review.award,
          isTaEnabled: result.ta_review.ta_enabled,
          image: result.ta_review.image,
          overallRating: {
            rating: result.ta_review.ta_enabled ? result.ta_review.rating : '',
            ratingCount: result.ta_review.ta_enabled ? result.ta_review.count : '',
          },
          amenitiesRatings: [],
          userReviews: [],
        },
        policies: transformPolicies(result.hotel_policies),
      },
    }), {}),
    ids: data.result.map((hotel) => hotel.id),
    filters: {
      distanceCap: data.filters.distance_cap,
    },
    sort: {
      by: data.sort.by,
      list: ['price', data.sort.by],
      coordinates: data.sort.coordinates,
    },
  },
});

export const transformHotelDetailsApi = ({ data }) => ({
  data: {
    hotel: {
      id: data.id,
      name: data.name,
      isActive: data.is_active,
      coordinates: data.coordinates || {},
      address: data.address || {},
      roomTypeIds: data.rooms.map((room) =>
        roomTypeService.constructRoomTypeId(data.id, room.type)),
      description: data.description,
      amenities: data.facilities || [],
      accessibilities: data.accessibilities,
      trilights: data.trilights,
      images: data.images && sortRoomType(Object.keys(data.images))
        .reduce((arr, roomType) => [
          ...arr,
          ...data.images[roomType].map((image) => ({ ...image, roomType })),
        ], []),
      reviews: {},
      policies: transformPolicies(data.hotel_policies),
    },
    roomType: {
      byId: data.rooms.reduce((roomType, room) => ({
        ...roomType,
        [roomTypeService.constructRoomTypeId(data.id, room.type)]: {
          type: room.type,
          amenities: room.facilities,
          maxOccupancy: {
            adults: room.max_adults,
            kids: room.max_children,
            guests: room.max_guests,
            occupancy: room.occupancy,
          },
          area: room.size,
        },
      }), {}),
      ids: data.rooms.map((room) => roomTypeService.constructRoomTypeId(data.id, room.type)),
    },
    content: {
      whyTreebo: data.promises,
    },
  },
});

export const transformHotelAvailabilityApi = ({ data }) => {
  const availableRooms = Object.keys(data.availability)
    .map((room) => ({
      type: room.toLowerCase(),
      count: data.availability[room],
    }));

  return {
    rooms: sortRoomType(availableRooms),
    available: data.available,
  };
};

export const constructAddress = ({ street, locality = '', city, pincode = '' }) =>
  street ? `${street.replace(locality, '')} ${locality}, ${city} ${pincode}` : null;

const hashids = new Hashids('bqoksIlEnWtsL9lmaiZD', 8);
export const decodeHotelId = (hotelId) =>
  !Number.isFinite(+hotelId) ? hashids.decode(hotelId)[0] : false;

export const getDistanceBetween = (start, end, accuracy = 1, precision = 0) => {
  /* eslint-disable no-param-reassign, no-mixed-operators, no-plusplus, max-len */
  accuracy = Math.floor(accuracy);
  precision = Math.floor(precision);
  const toRad = Math.PI / 180;
  const a = 6378137;
  const b = 6356752.314245;
  const f = 1 / 298.257223563; // WGS-84 ellipsoid params
  const L = (end.lng - start.lng) * toRad;
  let cosSigma;
  let sigma;
  let sinAlpha;
  let cosSqAlpha;
  let cos2SigmaM;
  let sinSigma;
  const U1 = Math.atan((1 - f) * Math.tan(parseFloat(start.lat) * toRad));
  const U2 = Math.atan((1 - f) * Math.tan(parseFloat(end.lat) * toRad));
  const sinU1 = Math.sin(U1);
  const cosU1 = Math.cos(U1);
  const sinU2 = Math.sin(U2);
  const cosU2 = Math.cos(U2);
  let lambda = L;
  let lambdaP;
  let iterLimit = 100;
  do {
    const sinLambda = Math.sin(lambda);
    const cosLambda = Math.cos(lambda);
    sinSigma = Math.sqrt(cosU2 * sinLambda * (cosU2 * sinLambda) + (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
    if (sinSigma === 0) return 0; // co-incident points
    cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
    sigma = Math.atan2(sinSigma, cosSigma);
    sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
    cosSqAlpha = 1 - sinAlpha * sinAlpha;
    cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
    if (Number.isNaN(cos2SigmaM)) cos2SigmaM = 0; // equatorial line: cosSqAlpha=0 (§6)
    const C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
    lambdaP = lambda;
    lambda = L + (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));
  } while (Math.abs(lambda - lambdaP) > 1e-12 && --iterLimit > 0);
  if (iterLimit === 0) return NaN; // formula failed to converge
  const uSq = cosSqAlpha * (a * a - b * b) / (b * b);
  const A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
  const B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
  const deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
  const distance = (b * A * (sigma - deltaSigma)).toFixed(precision); // round to 1mm precision
  return Math.round(distance * (10 ** precision) / accuracy) * accuracy / (10 ** precision);
  /* eslint-enable no-param-reassign, no-mixed-operators, no-plusplus, max-len */
};

export const isCoupleFriendly = (policies) =>
  policies.length && policies.some((policy) => policy && (policy.type === 'is_couple_friendly'));

export const getCoupleFriendlyPolicyText = (policies) => policies
  .filter((policy) => coupleFriendlyPolicies.includes(policy.type))
  .reduce((policiesText, policy) => (`${policiesText} ${policy.description} `), '');

export const getHotelDetailsTarget = ({
  hotelName,
  hotelId,
  area,
  roomType,
  ratePlan,
  locationQuery,
  shouldSetParams,
}) => {
  const city = kebabCase(area.city);
  const hotelSlug = kebabCase(`${hotelName} ${area.locality}`);
  const params = shouldSetParams ? {
    search: qs.stringify({
      ...locationQuery,
      q: `${hotelName}, ${area.locality}`,
      hotel_id: hotelId,
      rateplan: ratePlan || '',
      roomtype: roomType && roomType.toLowerCase(),
    }),
  } : {};
  return {
    pathname: `/hotels-in-${city}/${hotelSlug}-${hotelId}/`,
    ...params,
  };
};
