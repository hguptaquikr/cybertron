import startCase from 'lodash/startCase';
import get from 'lodash/get';
import merge from 'lodash/merge';
import pick from 'lodash/pick';
import sortBy from 'lodash/sortBy';
import * as hotelService from './hotelService';
import searchService from '../search/searchService';
import analyticsService from '../analytics/analyticsService';
import { omitKeys, augmentWithDistance } from '../../services/utils';

const ADD_DISTANCE_TO_RESULTS = 'ADD_DISTANCE_TO_RESULTS';
const GET_HOTEL_RESULTS_REQUEST = 'GET_HOTEL_RESULTS_REQUEST';
const GET_HOTEL_RESULTS_SUCCESS = 'GET_HOTEL_RESULTS_SUCCESS';
const GET_HOTEL_RESULTS_FAILURE = 'GET_HOTEL_RESULTS_FAILURE';
const GET_SEARCH_PRICES_SUCCESS = 'GET_SEARCH_PRICES_SUCCESS';
const GET_NEARBY_SEARCH_PRICES_SUCCESS = 'GET_NEARBY_SEARCH_PRICES_SUCCESS';
const GET_NEARBY_HOTEL_RESULTS_REQUEST = 'GET_NEARBY_HOTEL_RESULTS_REQUEST';
const GET_NEARBY_HOTEL_RESULTS_SUCCESS = 'GET_NEARBY_HOTEL_RESULTS_SUCCESS';
const GET_NEARBY_HOTEL_RESULTS_FAILURE = 'GET_NEARBY_HOTEL_RESULTS_FAILURE';
const GET_HOTEL_DETAILS_REQUEST = 'GET_HOTEL_DETAILS_REQUEST';
const GET_HOTEL_DETAILS_SUCCESS = 'GET_HOTEL_DETAILS_SUCCESS';
const GET_HOTEL_DETAILS_FAILURE = 'GET_HOTEL_DETAILS_FAILURE ';
const GET_ROOM_PRICES_SUCCESS = 'GET_ROOM_PRICES_SUCCESS';
const BOOK_NOW_CLICKED = 'BOOK_NOW_CLICKED';
const SORTED_SIMILAR_TREEBOS = 'SORTED_SIMILAR_TREEBOS';

const initialState = {
  byId: {
    0: {
      id: '',
      name: '',
      isActive: true,
      coordinates: {
        lat: 0,
        lang: 0,
      },
      address: {},
      roomTypeIds: [],
      description: '',
      amenities: [],
      accessibilities: [],
      trilights: [],
      images: [],
      reviews: {},
      policies: [],
    },
  },
  ids: [],
  similarHotels: [],
  isHotelResultsLoading: false, // TO BE MOVED TO UI DUCK
  isHotelDetailsLoading: false, // TO BE MOVED TO UI DUCK
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case ADD_DISTANCE_TO_RESULTS:
      return {
        ...state,
        byId: merge({}, state.byId, payload.byId),
      };

    case GET_HOTEL_RESULTS_REQUEST:
      return {
        ...state,
        byId: payload.byId,
        ids: payload.ids,
        isHotelResultsLoading: true,
      };

    case GET_HOTEL_RESULTS_SUCCESS:
      return {
        ...state,
        byId: merge({}, state.byId, payload.response.byId),
        ids: Array.from(
          new Set([
            ...state.ids,
            ...payload.response.ids,
          ]),
        ),
        isHotelResultsLoading: false,
      };

    case GET_NEARBY_HOTEL_RESULTS_SUCCESS:
      return {
        ...state,
        byId: merge({}, state.byId, payload.response.byId),
        ids: Array.from(
          new Set([
            ...state.ids,
            ...payload.response.ids,
          ]),
        ),
      };

    case GET_SEARCH_PRICES_SUCCESS:
      return {
        ...state,
        byId: merge({}, state.byId, pick(payload.response.hotel.byId, Object.keys(state.byId))),
      };

    case GET_NEARBY_SEARCH_PRICES_SUCCESS:
      return {
        ...state,
        byId: merge({}, state.byId, payload.response.hotel.byId),
      };

    case GET_HOTEL_DETAILS_REQUEST:
      return {
        ...state,
        isHotelDetailsLoading: true,
      };

    case GET_HOTEL_DETAILS_SUCCESS:
      return {
        ...state,
        isHotelDetailsLoading: false,
        byId: {
          ...state.byId,
          [payload.hotelId]:
            merge({}, state.byId[payload.hotelId], payload.response.hotel),
        },
        ids: Array.from(
          new Set([
            ...state.ids,
            payload.hotelId,
          ]),
        ),
      };

    case GET_ROOM_PRICES_SUCCESS:
      return {
        ...state,
        byId: {
          ...state.byId,
          [payload.hotelId]: {
            ...state.byId[payload.hotelId],
            roomTypeIds: payload.response.price.roomTypeIds,
          },
        },
      };

    case SORTED_SIMILAR_TREEBOS: {
      return {
        ...state,
        similarHotels: payload.sortedHotels,
      };
    }

    default:
      return state;
  }
};

export const sortSimilarTreebos = (hotelId) => (dispatch, getState) => {
  const { hotel, price } = getState();
  const referenceHotel = hotel.byId[hotelId];
  const unavailableHotelIds = Object.values(hotel.byId).filter((singleHotel) => {
    const updateAvailableHotels = singleHotel.roomTypeIds.length
      ? !price.byRoomTypeId[singleHotel.roomTypeIds[0]].available : null;
    return updateAvailableHotels;
  }).map((item) => `${item.id}`);
  const availableHotels = omitKeys(hotel.byId, [...unavailableHotelIds, hotelId, '0']);
  const hotelsWithDistance = augmentWithDistance(referenceHotel, availableHotels);
  const sortedHotels = sortBy(hotelsWithDistance, 'distance');
  dispatch({
    type: SORTED_SIMILAR_TREEBOS,
    payload: { sortedHotels },
  });
};

export const getHotelResults = (query, params) => async (dispatch, getState, { api }) => {
  const searchResultsQuery = {
    hotel_id: query.hotel_id || params.hotel_id,
    nearby: !!query.hotel_id || !!params.hotel_id,
    state: startCase(query.state || params.state),
    city: startCase(query.city || params.city),
    locality: startCase(query.locality || params.locality),
    landmark: startCase(query.landmark || params.landmark),
    category: startCase(query.category || params.category),
    amenity: startCase(query.amenity || params.amenity),
    distance_cap: !!(
      query.locality
      || query.landmark
      || params.locality
      || params.landmark
    ),
  };

  // talk to backend about better fallbacks
  if (searchResultsQuery.landmark === 'Me') {
    if (!searchResultsQuery.city) {
      const res = await searchService.getIpLocation();
      searchResultsQuery.city = res.city;
    }
    searchResultsQuery.landmark = '';
  }

  return dispatch({
    types: [
      GET_HOTEL_RESULTS_REQUEST,
      GET_HOTEL_RESULTS_SUCCESS,
      GET_HOTEL_RESULTS_FAILURE,
    ],
    payload: {
      byId: initialState.byId,
      ids: initialState.ids,
      sort: {
        by: query.lat && query.lng ? 'distance' : null,
        list: query.lat && query.lng ? ['price', 'distance'] : null,
      },
      coordinates: {
        lat: query.lat,
        lng: query.lng,
      },
    },
    promise: () => api.get('/api/web/v3/search/', searchResultsQuery)
      .then(hotelService.transformHotelResultsApi),
  });
};

export const getNearbyHotelResults = (hotelId) => (dispatch, getState, { api }) => {
  const nearbyHotelResultsQuery = {
    hotel_id: hotelId,
    nearby: true,
  };

  return dispatch({
    types: [
      GET_NEARBY_HOTEL_RESULTS_REQUEST,
      GET_NEARBY_HOTEL_RESULTS_SUCCESS,
      GET_NEARBY_HOTEL_RESULTS_FAILURE,
    ],
    promise: () => api
      .get('/api/web/v3/search/', nearbyHotelResultsQuery)
      .then(hotelService.transformHotelResultsApi)
      .then(({ data }) => ({
        data: {
          ...data,
          byId: omitKeys(data.byId, [hotelId]),
        },
      })),
  });
};

export const getHotelDetails = (hotelId) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_HOTEL_DETAILS_REQUEST,
      GET_HOTEL_DETAILS_SUCCESS,
      GET_HOTEL_DETAILS_FAILURE,
    ],
    payload: { hotelId },
    promise: () => api.get(`/api/web/v3/hotels/${hotelId}/details/`)
      .then(hotelService.transformHotelDetailsApi),
  });

export const bookNowClicked = (hotel, analyticsProperties) =>
  (dispatch, getState) => {
    const { search: { datePicker, occupancy }, price } = getState();
    const hotelPrice = price.byRoomTypeId[hotel.roomTypeIds[0]].ratePlans;
    const itineraryPrefill = {
      hotel: {
        id: hotel.id,
        name: hotel.name,
        ...hotel.address,
      },
      date: {
        checkin: get(datePicker, 'range.start'),
        checkout: get(datePicker, 'range.end'),
      },
      room: {
        count: occupancy.rooms.length,
        type: occupancy.roomType,
      },
      price: {
        pretax_price: Object.values(
          hotelPrice,
        )[0].basePrice,
        total_tax: Object.values(
          hotelPrice,
        )[0].tax,
        actual_total_cost: Object.values(
          hotelPrice,
        )[0].totalPrice,
      },
    };

    dispatch({
      type: BOOK_NOW_CLICKED,
      payload: itineraryPrefill,
    });
    if (analyticsProperties) analyticsService.quickBookClicked(hotel, analyticsProperties);
    else analyticsService.bookNowClicked(hotel);
  };

export const addDistanceToResults = () => (dispatch, getState) => {
  const {
    filter: { sort },
    hotel,
  } = getState();
  return dispatch({
    type: ADD_DISTANCE_TO_RESULTS,
    payload: {
      byId: hotel.ids.reduce((obj, hotelId) => ({
        ...obj,
        [hotelId]: {
          ...hotel.byId[hotelId],
          distance: hotelService.getDistanceBetween(
            sort.coordinates,
            hotel.byId[hotelId].coordinates,
          ),
        },
      }), {}),
    },
  });
};
