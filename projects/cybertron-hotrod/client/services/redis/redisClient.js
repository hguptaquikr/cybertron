import redis from 'redis';
import logger from 'cybertron-utils/logger';
import config from '../../../config';
import * as packageJson from '../../../package.json';

let redisClient = false;
let isRedisReady = false;

export const init = () => {
  logger.info(`Redis will connect on ${config.redisUrl}:${config.redisPort}`);
  redisClient = redis.createClient({ host: config.redisUrl, port: config.redisPort });
  redisClient.on('connect', () => {
    logger.info('Redis Connected');
  });

  redisClient.on('ready', () => {
    isRedisReady = true;
    logger.info('Redis Ready');
  });

  redisClient.on('error', (err) => {
    logger.error(`Redis Error ${err}`);
  });

  redisClient.on('end', () => {
    isRedisReady = false;
    logger.error('Redis End');
  });

  redisClient.select(config.redisDb, (err) => {
    if (err) {
      logger.error(`Redis Database Select Failed: ${err}`);
      throw err;
    }
  });
};

export const get = (key) =>
  new Promise((resolve, reject) => {
    if (isRedisReady && redisClient) {
      redisClient.get(`${packageJson.name}|${packageJson.version}|${key}`, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    } else {
      reject('Redis client not available.');
    }
  });

export const set = (key, value, time = 0) =>
  new Promise((resolve, reject) => {
    if (isRedisReady && redisClient) {
      if (time > 0) {
        redisClient.set(`${packageJson.name}|${packageJson.version}|${key}`, value, 'PX', time);
        resolve();
      } else {
        reject('Redis expiry time cannot be 0');
      }
    } else {
      reject('Redis client not available.');
    }
  });
