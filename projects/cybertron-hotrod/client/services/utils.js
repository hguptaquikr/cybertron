import React from 'react';
import * as hotelService from '../services/hotel/hotelService';

export const constants = {
  treeboTalkiesUrl: 'http://talkies.treebo.com',
  dateFormat: {
    query: 'YYYY-MM-DD',
    view: 'ddd, D MMM',
    history: 'ddd,D MMM YYYY',
  },
};

export const reactChildrenByType = (children) => {
  const childrenByType = {};
  React.Children.forEach(children, (child) => {
    childrenByType[child.type] = child;
  });
  return childrenByType;
};

export const pluralize = (value, text, suffix = 's') => (
  +value > 1 ? `${text}${suffix}` : text
);

export const getTotalGuests = (rooms) =>
  rooms.reduce((total, room) => ({
    adults: total.adults + +room.adults,
    kids: total.kids + (+room.kids ? +room.kids : 0),
  }), { adults: 0, kids: 0 });

export const getOccupancy = (rooms) => {
  const { adults, kids } = getTotalGuests(rooms);
  const kidStr = kids ? `${kids} ${pluralize(kids, 'Kid')}` : '';
  const adultsStr = `${adults} ${pluralize(adults, 'Adult')}`;
  const roomsStr = `${rooms.length} ${pluralize(rooms.length, 'Room')}`;
  return { adults: adultsStr, kids: kidStr, room: roomsStr };
};

export const getOccupancyStr = (rooms) => {
  const { adults, kids, room } = getOccupancy(rooms);
  return [adults, kids, room].filter((v) => v).join(', ');
};

export const getDiscountPercent = (final = 0, rack = 0) => {
  const discount = rack - final;
  return discount > 0 ?
    Math.round((discount / rack) * 100) :
    null;
};

export const getNoOfDays = (start, end) => {
  const days = end ? end.diff(start, 'days') : '';
  return days && `${days} ${pluralize(days, 'day')}`;
};

export const getDistanceWithUnit = (distance) => `${(distance / 1000).toFixed(1)} km`;

export const augmentWithDistance = (referencePoint, hotels) => {
  const ret = {};
  Object.values(hotels).forEach((hotel) => {
    ret[hotel.id] = {
      ...hotel,
      distance: hotelService.getDistanceBetween(referencePoint.coordinates, hotel.coordinates),
    };
  });
  return ret;
};

export const performanceMark = (name) => {
  if (window.performance && window.performance.mark) {
    window.performance.mark(name);
  }
};

export const getPerformanceDuration = (name, start, end) => {
  try {
    if (
      window.performance
        && window.performance.mark
        && window.performance.measure
    ) {
      window.performance.measure(name, start, end);
      const perf = window.performance.getEntriesByName(name);
      window.performance.clearMarks(start);
      window.performance.clearMarks(end);
      window.performance.clearMeasures(name);
      return perf[0].duration;
    }
    return false;
  } catch (e) {
    return false;
  }
};

// returns a new object, removing the keysToOmit
export const omitKeys = (ob = {}, keysToOmit = []) =>
  Object.keys(ob).reduce((obj, key) => (
    keysToOmit.includes(key) ? obj : {
      ...obj,
      [key]: ob[key],
    }
  ), {});

// returns an array of keys, removing the keys '0', ''
export const safeKeys = (ob = {}) =>
  Object.keys(ob).filter((v) => v.length && v !== '0');
