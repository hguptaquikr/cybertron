import isObject from 'lodash/isObject';
import bookingHistoryService from './userService';
import auth from '../auth/authService';
import analyticsService from '../analytics/analyticsService';

const GET_BOOKING_HISTORY_REQUEST = 'GET_BOOKING_HISTORY_REQUEST';
const GET_BOOKING_HISTORY_SUCCESS = 'GET_BOOOKING_HISTORY_SUCCESS';
const GET_BOOKING_HISTORY_FAILURE = 'GET_BOOKING_HISTORY_FAILURE';
const USER_LOGIN_REQUEST = 'USER_LOGIN_REQUEST';
const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
const USER_LOGIN_FAILURE = 'USER_LOGIN_FAILURE';
const USER_LOGOUT_REQUEST = 'USER_LOGOUT_REQUEST';
const USER_LOGOUT_SUCCESS = 'USER_LOGOUT_SUCCESS';
const USER_LOGOUT_FAILURE = 'USER_LOGOUT_FAILURE';
const USER_SIGNUP_REQUEST = 'USER_SIGNUP_REQUEST';
const USER_SIGNUP_SUCCESS = 'USER_SIGNUP_SUCCESS';
const USER_SIGNUP_FAILURE = 'USER_SIGNUP_FAILURE';
const FORGOT_PASSWORD_REQUEST = 'FORGOT_PASSWORD_REQUEST';
const FORGOT_PASSWORD_SUCCESS = 'FORGOT_PASSWORD_SUCCESS';
const FORGOT_PASSWORD_FAILURE = 'FORGOT_PASSWORD_FAILURE';
const SET_AUTHENTICATED_USER = 'SET_AUTHENTICATED_USER';
const UNSET_AUTHENTICATED_USER = 'UNSET_AUTHENTICATED_USER';
const SEND_LOGIN_OTP_REQUEST = 'SEND_LOGIN_OTP_REQUEST';
const SEND_LOGIN_OTP_SUCCESS = 'SEND_LOGIN_OTP_SUCCESS';
const SEND_LOGIN_OTP_FAILURE = 'SEND_LOGIN_OTP_FAILURE';
const VERIFY_OTP_REQUEST = 'VERIFY_OTP_REQUEST';
const VERIFY_OTP_SUCCESS = 'VERIFY_OTP_SUCCESS';
const VERIFY_OTP_FAILURE = 'VERIFY_OTP_FAILURE';

const initialState = {
  isVerifyingOtp: false,
  isAuthenticated: false,
  profile: {
    email: '',
    firstName: '',
    lastName: '',
    mobile: '',
    name: '',
    userId: '',
  },
  bookingHistory: {
    results: {
      0: {
        id: '',
        status: '',
        amountPaid: '',
        partPayAmount: '',
        partPayLink: '',
        isPayAtHotel: '',
        type: '',
        hotel: {
          name: '',
          city: '',
          address: '',
          locality: '',
          state: '',
          imageUrl: '',
          contact: '',
          coordinates: {
            lat: '',
            lng: '',
          },
        },
        dates: {
          booking: '',
          checkIn: '',
          checkOut: '',
          durationOfStay: '',
          cancel: '',
        },
        time: {
          checkIn: '',
          checkOut: '',
        },
        roomConfig: {
          adults: '',
          kids: '',
          count: '',
          type: '',
        },
        price: {
          basePrice: '',
          tax: '',
          total: '',
          voucherPrice: '',
          memberDiscountPrice: '',
        },
        coupon: {
          code: '',
          amount: '',
        },
        guest: {
          contact: '',
          name: '',
          email: '',
        },
        cancellation: {
          hash: '',
        },
      },
    },
    upcomingBookings: [],
    pastBookings: [],
  },
};

export default (state = initialState, action) => {
  const { payload, type } = action;
  switch (type) {
    case GET_BOOKING_HISTORY_SUCCESS: {
      return {
        ...state,
        bookingHistory: { ...payload.response },
      };
    }
    case SET_AUTHENTICATED_USER: {
      return {
        ...state,
        isAuthenticated: true,
        profile: action.payload.user,
      };
    }
    case UNSET_AUTHENTICATED_USER: {
      return {
        ...state,
        isAuthenticated: false,
        profile: {},
      };
    }
    case USER_LOGIN_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      };
    case USER_SIGNUP_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      };
    case SEND_LOGIN_OTP_SUCCESS:
      return {
        ...state,
        profile: {
          ...state.profile,
          mobile: payload.mobile,
        },
      };
    case VERIFY_OTP_REQUEST:
      return {
        ...state,
        isVerifyingOtp: true,
      };
    case VERIFY_OTP_FAILURE:
    case VERIFY_OTP_SUCCESS:
      return {
        ...state,
        isVerifyingOtp: false,
      };

    default:
      return state;
  }
};

export const getBookingHistory = () => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_BOOKING_HISTORY_REQUEST,
      GET_BOOKING_HISTORY_SUCCESS,
      GET_BOOKING_HISTORY_FAILURE,
    ],
    promise: () => api.post('/api/web/v1/booking/booking-history/')
      .then((res) => bookingHistoryService.transformBookingHistoryApi(res)),
  });

export const sendBookingDetailsToEmail = (email, id) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_BOOKING_HISTORY_REQUEST,
      GET_BOOKING_HISTORY_SUCCESS,
      GET_BOOKING_HISTORY_FAILURE,
    ],
    promise: () => api.post('/api/web/v1/checkout/resendemail/', { email, order_id: id }),
  });

export const setAuthenticatedUser = (user) => ({
  type: SET_AUTHENTICATED_USER,
  payload: { user },
});

export const unsetAuthenticatedUser = () => ({
  type: UNSET_AUTHENTICATED_USER,
});

export const userSignup = (userData) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      USER_SIGNUP_REQUEST,
      USER_SIGNUP_SUCCESS,
      USER_SIGNUP_FAILURE,
    ],
    promise: () => api.post('/api/web/v2/auth/register/', userData),
  })
    .then((res) => {
      const authData = auth.transformLoginApi(res);
      auth.setUser(authData.user);
      auth.setAuthToken(authData.token);
      dispatch(setAuthenticatedUser(authData.user));
      return res;
    });

export const userLogin = (credentials) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      USER_LOGIN_REQUEST,
      USER_LOGIN_SUCCESS,
      USER_LOGIN_FAILURE,
    ],
    promise: () => api.post('/api/web/v2/auth/login/email/', credentials),
  })
    .then((res) => {
      const authData = auth.transformLoginApi(res);
      auth.setUser(authData.user);
      auth.setAuthToken(authData.token);
      dispatch(setAuthenticatedUser(authData.user));
      analyticsService.loginDetails();
      return res;
    });

export const facebookLogin = (facebookResponse) => (dispatch, getState, { api }) => {
  const userData = {
    token: facebookResponse.accessToken,
  };
  return dispatch({
    types: [
      USER_LOGIN_REQUEST,
      USER_LOGIN_SUCCESS,
      USER_LOGIN_FAILURE,
    ],
    promise: () => api.post('/api/web/v2/auth/login/facebook/', userData),
  })
    .then((res) => {
      const authData = auth.transformLoginApi(res);
      auth.setUser(authData.user);
      auth.setAuthToken(authData.token);
      dispatch(setAuthenticatedUser(authData.user));
      analyticsService.loginDetails();
      return res;
    });
};

export const googleLogin = (googleResponse) => (dispatch, getState, { api }) => {
  const userData = {
    token: googleResponse.tokenId,
  };

  return dispatch({
    types: [
      USER_LOGIN_REQUEST,
      USER_LOGIN_SUCCESS,
      USER_LOGIN_FAILURE,
    ],
    promise: () => api.post('/api/web/v2/auth/login/google/', userData),
  })
    .then((res) => {
      const authData = auth.transformLoginApi(res);
      auth.setUser(authData.user);
      auth.setAuthToken(authData.token);
      dispatch(setAuthenticatedUser(authData.user));
      analyticsService.loginDetails();
      return res;
    });
};

export const sendLoginOtp = (mobile) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      SEND_LOGIN_OTP_REQUEST,
      SEND_LOGIN_OTP_SUCCESS,
      SEND_LOGIN_OTP_FAILURE,
    ],
    payload: { mobile },
    promise: () => api.post('/api/web/v2/auth/login/otp/', { phone_number: mobile }),
  });

export const verifyLoginOtp = (mobile, otp) => (dispatch, getState, { api }) => {
  const verifyOtpData = {
    phone_number: mobile,
    verification_code: otp,
  };
  return dispatch({
    types: [
      VERIFY_OTP_REQUEST,
      VERIFY_OTP_SUCCESS,
      VERIFY_OTP_FAILURE,
    ],
    promise: () => api.post('/api/web/v2/auth/login/otp/verify/', verifyOtpData)
      .then((res) => {
        const authData = auth.transformLoginApi(res.data);
        auth.setUser(authData.user);
        auth.setAuthToken(authData.token);
        dispatch(setAuthenticatedUser(authData.user));
        analyticsService.loginDetails();
        return res;
      }),
  });
};

export const forgotPassword = (email) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      FORGOT_PASSWORD_REQUEST,
      FORGOT_PASSWORD_SUCCESS,
      FORGOT_PASSWORD_FAILURE,
    ],
    promise: () => api.post('/api/web/v2/auth/forgot-password/', { email }),
  });

export const userLogout = () => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      USER_LOGOUT_REQUEST,
      USER_LOGOUT_SUCCESS,
      USER_LOGOUT_FAILURE,
    ],
    promise: () => api.get('/api/web/v2/auth/logout/'),
  })
    .then((res) => {
      auth.unsetAuthData();
      dispatch(unsetAuthenticatedUser());
      return res;
    });

export const checkIfUserAuthenticated = () => (dispatch) => {
  const user = auth.getUser();
  if (isObject(user)) dispatch(setAuthenticatedUser(user));
};
