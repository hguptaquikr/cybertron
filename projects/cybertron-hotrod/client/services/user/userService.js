export default {
  transformBookingDetails(booking, type, bookingStatus) {
    return {
      id: booking.order_id,
      status: booking.status,
      amountPaid: booking.payment_detail.paid_amount,
      partPayAmount: booking.partial_payment_amount,
      partPayLink: booking.partial_payment_link,
      isPayAtHotel: booking.paid_at_hotel,
      type,
      bookingStatus,
      hotel: {
        name: booking.hotel_name,
        city: booking.hotel_detail.city,
        address: booking.hotel_detail.address,
        locality: booking.hotel_detail.locality,
        state: booking.hotel_detail.state,
        imageUrl: booking.hotel_detail.showcased_image_url,
        contact: booking.hotel_detail.contact_number,
        coordinates: {
          lat: booking.hotel_detail.latitude,
          lng: booking.hotel_detail.longitude,
        },
      },
      dates: {
        booking: booking.booking_date,
        checkIn: booking.checkin_date,
        checkOut: booking.checkout_date,
        durationOfStay: booking.total_number_of_nights,
        cancel: booking.cancel_date,
      },
      time: {
        checkIn: booking.checkin_time,
        checkOut: booking.checkout_time,
      },
      roomConfig: {
        adults: booking.room_config.no_of_adults,
        kids: booking.room_config.no_of_childs,
        count: booking.room_config.room_count,
        type: booking.room_config.room_type,
      },
      price: {
        basePrice: booking.payment_detail.room_price,
        tax: booking.payment_detail.tax,
        total: booking.payment_detail.total,
        voucherPrice: booking.payment_detail.voucher_amount,
        memberDiscountPrice: booking.payment_detail.member_discount,
      },
      coupon: {
        code: booking.payment_detail.coupon_code,
        amount: booking.payment_detail.discount,
      },
      guest: {
        contact: booking.user_detail.contact_number,
        name: booking.user_detail.name,
        email: booking.user_detail.email,
      },
      cancellation: {
        hash: booking.cancel_hash,
      },
    };
  },

  transformBookingHistoryApi({ data: { context } }) {
    const transformedData = {
      results: {},
      upcomingBookings: [],
      pastBookings: [],
    };
    const calculateBookingStatus = (booking) => {
      if (booking.status === 'RESERVED' && booking.partial_payment_link) {
        return 'temporary';
      } else if (booking.status === 'RESERVED') {
        return 'confirmed';
      }
      return 'cancelled';
    };
    transformedData.upcomingBookings = context.upcoming.map((booking) => {
      const upcomingBookingStatus = calculateBookingStatus(booking);
      transformedData.results[booking.order_id] = this.transformBookingDetails(booking, 'upcoming', upcomingBookingStatus);
      return booking.order_id;
    });
    transformedData.pastBookings = context.previous.map((booking) => {
      const pastBookingStatus = calculateBookingStatus(booking);
      transformedData.results[booking.order_id] = this.transformBookingDetails(booking, 'past', pastBookingStatus);
      return booking.order_id;
    });
    return { data: transformedData };
  },
};
