const GET_BOOKING_CONTENT_REQUEST = 'GET_BOOKING_CONTENT_REQUEST';
const GET_BOOKING_CONTENT_SUCCESS = 'GET_BOOKING_CONTENT_SUCCESS';
const GET_BOOKING_CONTENT_FAILURE = 'GET_BOOKING_CONTENT_FAILURE';
const CANCEL_BOOKING_REQUEST = 'CANCEL_BOOKING_REQUEST';
const CANCEL_BOOKING_SUCCESS = 'CANCEL_BOOKING_SUCCESS';
const CANCEL_BOOKING_FAILURE = 'CANCEL_BOOKING_FAILURE';

const initialState = {
  bookingDetail: {},
  bookingStatus: {
    message: '',
    status: '',
    cancelled_dateTime: '',
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_BOOKING_CONTENT_SUCCESS:
      return {
        ...state,
        bookingDetail: action.payload.response.booking_details,
        bookingStatus: action.payload.response.booking_status,
      };
    case GET_BOOKING_CONTENT_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      };
    case CANCEL_BOOKING_SUCCESS:
      return {
        ...state,
        bookingStatus: action.payload.response.booking_status,
      };
    default:
      return state;
  }
};

export const getBookingDetails = (bookingHash) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_BOOKING_CONTENT_REQUEST,
      GET_BOOKING_CONTENT_SUCCESS,
      GET_BOOKING_CONTENT_FAILURE,
    ],
    promise: () => api.get('/api/web/v1/growth/bookingdata/', { booking_hash: bookingHash }),
  });

export const cancelBooking = (
  bookingHash,
  orderId,
  reason,
  message,
) => (dispatch, getState, { api }) => {
  const requestData = {
    booking_hash: bookingHash,
    order_id: orderId,
    reason,
    message,
  };

  return dispatch({
    types: [
      CANCEL_BOOKING_REQUEST,
      CANCEL_BOOKING_SUCCESS,
      CANCEL_BOOKING_FAILURE,
    ],
    promise: () => api.post('/api/web/v1/growth/submitcancel', requestData),
  });
};
