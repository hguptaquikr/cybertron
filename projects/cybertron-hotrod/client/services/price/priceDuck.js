import analyticsService from '../analytics/analyticsService';
import priceService from './priceService';
import * as routeService from '../route/routeService';
import { sortSimilarTreebos } from '../hotel/hotelDuck';
import config from '../../../config';

const GET_SEARCH_PRICES_REQUEST = 'GET_SEARCH_PRICES_REQUEST';
const GET_SEARCH_PRICES_SUCCESS = 'GET_SEARCH_PRICES_SUCCESS';
const GET_SEARCH_PRICES_FAILURE = 'GET_SEARCH_PRICES_FAILURE';
const GET_NEARBY_SEARCH_PRICES_REQUEST = 'GET_NEARBY_SEARCH_PRICES_REQUEST';
const GET_NEARBY_SEARCH_PRICES_SUCCESS = 'GET_NEARBY_SEARCH_PRICES_SUCCESS';
const GET_NEARBY_SEARCH_PRICES_FAILURE = 'GET_NEARBY_SEARCH_PRICES_FAILURE';
const GET_ROOM_PRICES_REQUEST = 'GET_ROOM_PRICES_REQUEST';
const GET_ROOM_PRICES_SUCCESS = 'GET_ROOM_PRICES_SUCCESS';
const GET_ROOM_PRICES_FAILURE = 'GET_ROOM_PRICES_FAILURE';

const initialState = {
  byRoomTypeId: {
    '0|oak': {
      id: '0|oak',
      available: 1,
      ratePlans: {},
      memberDiscount: {},
    },
  },
  roomTypeIds: [],
  isPricesLoading: false, // TO BE MOVED TO UI DUCK
  isRoomPricesLoading: false, // TO BE MOVED TO UI DUCK
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_SEARCH_PRICES_REQUEST: {
      return {
        ...state,
        byRoomTypeId: payload.byRoomTypeId,
        roomTypeIds: payload.roomTypeIds,
        isPricesLoading: true,
      };
    }
    case GET_SEARCH_PRICES_SUCCESS: {
      return {
        ...state,
        isPricesLoading: false,
        byRoomTypeId: {
          ...state.byRoomTypeId,
          ...payload.response.price.byRoomTypeId,
        },
        roomTypeIds: payload.response.price.roomTypeIds,
      };
    }
    case GET_SEARCH_PRICES_FAILURE: {
      return {
        ...state,
        isPricesLoading: false,
      };
    }
    case GET_NEARBY_SEARCH_PRICES_REQUEST: {
      return {
        ...state,
        byRoomTypeId: {
          ...state.byRoomTypeId,
        },
      };
    }
    case GET_NEARBY_SEARCH_PRICES_SUCCESS: {
      return {
        ...state,
        byRoomTypeId: {
          ...state.byRoomTypeId,
          ...payload.response.price.byRoomTypeId,
        },
      };
    }
    case GET_ROOM_PRICES_REQUEST:
      return {
        ...state,
        isRoomPricesLoading: true,
      };
    case GET_ROOM_PRICES_SUCCESS:
      return {
        ...state,
        isRoomPricesLoading: false,
        byRoomTypeId: {
          ...state.byRoomTypeId,
          ...payload.response.price.byRoomTypeId,
        },
        roomTypeIds: payload.response.price.roomTypeIds,
      };
    default:
      return state;
  }
};

export const getSearchPrices = (hotelIds) => (dispatch, getState, { api }) => {
  const { search, ui, filter } = getState();

  const searchPricesQuery = {
    hotel_id: hotelIds.filter(Number).join(','),
    ...routeService.makeDateRangeQuery(search.datePicker.range.start, search.datePicker.range.end),
    roomconfig: search.occupancy.rooms.map((room) => `${room.adults}-${room.kids || 0}`).join(','),
    ...ui.utmParams,
    sort: filter.sort.by,
    channel: config.channel,
  };

  return dispatch({
    types: [
      GET_SEARCH_PRICES_REQUEST,
      GET_SEARCH_PRICES_SUCCESS,
      GET_SEARCH_PRICES_FAILURE,
    ],
    payload: initialState,
    promise: () => api.get('/api/web/v5/pricing/hotels/', searchPricesQuery)
      .then(priceService.transformSearchPricesApi),
  });
};

export const getNearbySearchPrices = (hotelId, hotelIds) => (dispatch, getState, { api }) => {
  const {
    search: { datePicker, occupancy },
    ui,
  } = getState();

  const similarTreebosPricesQuery = {
    hotel_id: hotelIds.filter((hId) => hId !== parseInt(hotelId, 0)).join(','),
    ...routeService.makeDateRangeQuery(datePicker.range.start, datePicker.range.end),
    ...routeService.makeRoomConfigQuery(occupancy.rooms),
    ...ui.utmParams,
    channel: config.channel,
  };

  return dispatch({
    types: [
      GET_NEARBY_SEARCH_PRICES_REQUEST,
      GET_NEARBY_SEARCH_PRICES_SUCCESS,
      GET_NEARBY_SEARCH_PRICES_FAILURE,
    ],
    promise: () => api.get('/api/web/v5/pricing/hotels/', similarTreebosPricesQuery)
      .then(priceService.transformSearchPricesApi),
  }).then((res) => {
    dispatch(sortSimilarTreebos(hotelId));
    return res;
  });
};

export const getRoomPrices = (hotelId) => (dispatch, getState, { api }) => {
  const {
    search: { datePicker, occupancy },
    ui,
  } = getState();
  const pricingAndAvailabilityQuery = {
    hotel_id: hotelId,
    ...routeService.makeDateRangeQuery(datePicker.range.start, datePicker.range.end),
    ...routeService.makeRoomConfigQuery(occupancy.rooms),
    ...routeService.makeRoomTypeQuery(occupancy.roomType),
    ...ui.utmParams,
    channel: config.channel,
  };
  return dispatch({
    types: [
      GET_ROOM_PRICES_REQUEST,
      GET_ROOM_PRICES_SUCCESS,
      GET_ROOM_PRICES_FAILURE,
    ],
    payload: { hotelId },
    promise: () => api.get(`/api/web/v5/pricing/hotels/${hotelId}/room-prices/`, pricingAndAvailabilityQuery)
      .then((res) => priceService.transformRoomPricesApi(res, hotelId))
      .then((res) => {
        analyticsService.googleRemarketing('Hotel Detail');
        return res;
      }),
  });
};
