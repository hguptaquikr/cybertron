import isEmpty from 'lodash/isEmpty';
import * as hotelService from '../hotel/hotelService';
import roomTypeService from '../roomType/roomTypeService';

export default {
  transformSearchPricesApi({ data: hotels }) {
    return {
      data: {
        price: Object.keys(hotels)
          .reduce((obj, hotelId) => ({
            byRoomTypeId: {
              ...obj.byRoomTypeId,
              [roomTypeService.constructRoomTypeId(
                hotelId,
                hotels[hotelId].cheapest_room.room_type,
              )]: {
                couponCode: hotels[hotelId].coupon_code,
                available: hotels[hotelId].cheapest_room.availability,
                id: roomTypeService.constructRoomTypeId(
                  hotelId,
                  hotels[hotelId].cheapest_room.room_type,
                ),
                memberDiscount: {
                  isAvailable: hotels[hotelId].member_discount_available,
                  isApplied: hotels[hotelId].member_discount_applied,
                },
                ratePlans: [hotels[hotelId].rate_plan].reduce((ratePlans, ratePlan) => ({
                  ...ratePlans,
                  [ratePlan.code]: {
                    code: ratePlan.code,
                    tag: ratePlan.tag,
                    type: ratePlan.refundable ? 'refundable' : 'non-refundable',
                    description: ratePlan.description,
                    basePrice: hotels[hotelId].pretax_price,
                    treeboPointsUsed: hotels[hotelId].wallet_deduction,
                    sellingPrice: hotels[hotelId].net_payable_amount,
                    strikedPrice: hotels[hotelId].rack_rate,
                    totalPrice: hotels[hotelId].sell_price,
                    tax: hotels[hotelId].tax,
                    discountPercentage: hotels[hotelId].total_discount_percent,
                  },
                }), {}),
              },
            },
            roomTypeIds: [
              ...obj.roomTypeIds,
              roomTypeService.constructRoomTypeId(hotelId, hotels[hotelId].cheapest_room.room_type),
            ],
          }), {
            byRoomTypeId: {},
            roomTypeIds: [],
          }),
        hotel: Object.keys(hotels)
          .reduce((obj, hotelId) => ({
            byId: {
              ...obj.byId,
              [hotelId]: {
                rank: {
                  recommended: hotels[hotelId].sort_index,
                },
                roomTypeIds:
                  [roomTypeService.constructRoomTypeId(
                    hotelId,
                    hotels[hotelId].cheapest_room.room_type,
                  )],
              },
            },
            ids: [...obj.ids, hotelId],
          }), { ids: [] }),
        roomType: Object.keys(hotels)
          .reduce((obj, hotelId) => ({
            byId: {
              ...obj.byId,
              [roomTypeService.constructRoomTypeId(
                hotelId,
                hotels[hotelId].cheapest_room.room_type,
              )]: {
                type: hotels[hotelId].cheapest_room.room_type,
                maxOccupancy: {},
                amenities: [],
              },
            },
            ids: [
              ...obj.ids,
              roomTypeService.constructRoomTypeId(hotelId, hotels[hotelId].cheapest_room.room_type),
            ],
          }), { ids: [] }),
      },
    };
  },

  transformRoomPricesApi({ data: { total_price: prices } }, hotelId) {
    return {
      data: {
        price: Object.keys(prices).reduce((obj, roomType) => ({
          byRoomTypeId: {
            ...obj.byRoomTypeId,
            [roomTypeService.constructRoomTypeId(hotelId, roomType)]: {
              id: roomTypeService.constructRoomTypeId(hotelId, roomType),
              available: prices[roomType].available,
              couponCode: prices[roomType].coupon_code,
              memberDiscount: {
                isAvailable: prices[roomType].member_discount_available,
                isApplied: prices[roomType].member_discount_applied,
              },
              ratePlans: prices[roomType].rate_plans.reduce((ratePlans, ratePlan) => ({
                ...ratePlans,
                [ratePlan.rate_plan.code]: {
                  code: ratePlan.rate_plan.code,
                  tag: ratePlan.rate_plan.tag,
                  type: ratePlan.rate_plan.refundable ? 'refundable' : 'non-refundable',
                  description: ratePlan.rate_plan.description,
                  basePrice: ratePlan.price.pretax_price,
                  treeboPointsUsed: ratePlan.price.wallet_deduction,
                  sellingPrice: ratePlan.price.net_payable_amount,
                  strikedPrice: ratePlan.price.rack_rate,
                  totalPrice: ratePlan.price.sell_price,
                  tax: ratePlan.price.tax,
                  discountPercentage: ratePlan.price.total_discount_percent,
                },
              }), {}),
            },
          },
          roomTypeIds: [...obj.roomTypeIds, roomTypeService.constructRoomTypeId(hotelId, roomType)],
        }), {
          byRoomTypeId: {},
          roomTypeIds: [],
        }),
      },
    };
  },

  transformItineraryPricingApi({ data }) {
    const transformedPricingObject = this.reduceMultiplePlans(data.price);
    const transformedWalletObject = this.transformWalletInfo(data.price);
    return {
      data: {
        ...data,
        hotel: {
          id: data.hotel.id,
          name: data.hotel.name,
          cityId: data.hotel.city_id,
          city: data.hotel.city,
          locality: data.hotel.locality,
          pincode: data.hotel.pincode,
          street: data.hotel.street,
          state: data.hotel.state,
          coordinates: data.hotel.coordinates || {},
          policies: hotelService.transformPolicies(data.hotel.hotel_policies),
          imageUrl: data.hotel.image_url,
        },
        price: {
          selectedRatePlan: data.price.selected_rate_plan.rate_plan.code,
          memberDiscountApplied: data.price.member_discount_applied,
          ratePlans: transformedPricingObject,
          wallet: transformedWalletObject,
        },
      },
    };
  },

  transformCouponPricingApi({ data }, couponCode) {
    const transformedPricingObject = this.reduceMultiplePlans(data);
    const transformedWalletObject = this.transformWalletInfo(data);
    return {
      data: {
        ...data,
        price: {
          ratePlans: transformedPricingObject,
          selectedRatePlan: data.selected_rate_plan.rate_plan.code,
          wallet: transformedWalletObject,
        },
        couponCode,
      },
    };
  },

  transformWalletPricingApi({ data }) {
    const transformedPricingObject = this.reduceMultiplePlans(data);
    const transformedWalletObject = this.transformWalletInfo(data);
    return {
      data: {
        ...data,
        price: {
          ratePlans: transformedPricingObject,
          selectedRatePlan: data.selected_rate_plan.rate_plan.code,
          wallet: transformedWalletObject,
        },
      },
    };
  },

  transformConfirmationPricingApi({ data }) {
    const { price, rate_plan: ratePlan } = data.price.selected_rate_plan;
    const transformedWalletObject = this.transformWalletInfo(price);
    const transformedPricingObject = {
      ...data,
      hotel: {
        id: data.hotel.id,
        name: data.hotel.name,
        city: data.hotel.city,
        locality: data.hotel.locality,
        pincode: data.hotel.pincode,
        street: data.hotel.street,
        state: data.hotel.state,
        coordinates: data.hotel.coordinates || {},
        directions: data.hotel.directions,
        policies: hotelService.transformPolicies(data.hotel.hotel_policies),
        imageUrl: data.hotel.image_url,
      },
      price: {
        ratePlans: {
          [ratePlan.code]: {
            code: ratePlan.code,
            type: ratePlan.code === 'NRP' ? 'non-refundable' : 'refundable',
            pretaxPrice: price.pretax_price,
            tax: price.tax,
            couponDiscount: price.total_discount,
            totalPrice: price.sell_price,
            sellingPrice: price.net_payable_amount,
            tag: ratePlan.tag,
          },
        },
        wallet: transformedWalletObject,
      },
      ratePlan: {
        code: ratePlan.code,
        description: ratePlan.description,
      },
    };
    return { data: transformedPricingObject };
  },

  sortedRatePlans(ratePlans) {
    return Object.keys(ratePlans).sort((a, b) =>
      ratePlans[a].sellingPrice - ratePlans[b].sellingPrice);
  },

  reduceMultiplePlans({
    all_rate_plans: ratePlans,
    selected_rate_plan: selectedRatePlan,
  }) {
    return ratePlans.reduce((priceObject, singlePlan) => {
      let nightsBreakup = [];
      if (singlePlan.rate_plan.code === selectedRatePlan.rate_plan.code) {
        nightsBreakup = selectedRatePlan.price.nights_breakup;
      }
      const { rate_plan: ratePlan, price } = singlePlan;
      return {
        ...priceObject,
        [ratePlan.code]: {
          code: ratePlan.code,
          type: ratePlan.code === 'NRP' ? 'non-refundable' : 'refundable',
          basePrice: price.base_price,
          strikedPrice: price.rack_rate,
          basePriceTax: price.base_price_tax,
          promoDiscount: price.promo_discount,
          memberDiscount: price.member_discount,
          finalPretaxPrice: price.final_pretax_price,
          couponDiscount: price.coupon_discount,
          pretaxPrice: price.pretax_price,
          voucherAmount: price.voucher_amount,
          tag: ratePlan.tag,
          walletApplied: price.wallet_applied,
          walletDeduction: price.wallet_deduction,
          sellingPrice: price.net_payable_amount,
          totalPrice: price.sell_price,
          discountPercentage: Math.round(price.total_discount_percent),
          tax: price.tax,
          nightsBreakup: nightsBreakup.map((oneNight) => ({
            date: oneNight.date,
            tax: oneNight.tax,
            pretaxPrice: oneNight.pretax_price,
            totalDiscount: oneNight.total_discount,
            sellingPrice: oneNight.sell_price,
          })),
        },
      };
    }, {});
  },

  transformWalletInfo({ wallet_info: walletInfo }) {
    return {
      [walletInfo.wallet_type]: {
        type: walletInfo.wallet_type,
        points: walletInfo.total_wallet_points,
        totalBalance: walletInfo.total_wallet_balance,
        usableBalance: walletInfo.wallet_deductable_amount,
        isApplied: walletInfo.wallet_applied,
      },
    };
  },

  transformPayNowApi({ data }) {
    return {
      data: {
        gatewayOrderId: data.pg_order_id,
        serviceOrderId: data.ps_order_id,
        order_id: data.order_id,
      },
    };
  },

  sortPrice(price) {
    return Object.values(price).sort((priceA, priceB) =>
      this.sortRatePlan(priceA.ratePlans)[0].sellingPrice -
      this.sortRatePlan(priceB.ratePlans)[0].sellingPrice);
  },

  sortRatePlan(ratePlans) {
    return Object.values(ratePlans)
      .sort((ratePlanA, ratePlanB) =>
        ratePlanA.sellingPrice - ratePlanB.sellingPrice);
  },

  getPricesForRoomTypeIds(roomTypeIds, price) {
    return (
      isEmpty(roomTypeIds)
        ? [price['0|oak']]
        : roomTypeIds.map((roomTypeId) => price[roomTypeId]).filter(Boolean)
    );
  },
};
