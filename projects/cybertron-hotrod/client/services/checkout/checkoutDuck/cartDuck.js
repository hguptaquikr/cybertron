import priceService from '../../price/priceService';
import analyticsService from '../../analytics/analyticsService';
import * as routeService from '../../route/routeService';
import config from '../../../../config';

const BOOK_NOW_CLICKED = 'BOOK_NOW_CLICKED';
const GET_CART_CONTENT_REQUEST = 'GET_CART_CONTENT_REQUEST';
const GET_CART_CONTENT_SUCCESS = 'GET_CART_CONTENT_SUCCESS';
const GET_CART_CONTENT_FAILURE = 'GET_CART_CONTENT_FAILURE';
const APPLY_REMOVE_COUPON_REQUEST = 'APPLY_REMOVE_COUPON_REQUEST';
const APPLY_REMOVE_COUPON_SUCCESS = 'APPLY_REMOVE_COUPON_SUCCESS';
const APPLY_REMOVE_COUPON_FAILURE = 'APPLY_REMOVE_COUPON_FAILURE';
const GET_COUPONS_REQUEST = 'GET_COUPONS_REQUEST';
const GET_COUPONS_SUCCESS = 'GET_COUPONS_SUCCESS';
const GET_COUPONS_FAILURE = 'GET_COUPONS_FAILURE';
const APPLY_WALLET_SUCCESS = 'APPLY_WALLET_SUCCESS';

const initialState = {
  hotel: {},
  date: {},
  room: {},
  price: {},
  bid: 0,
  coupon_code: '',
  coupons: [],
  isCouponLoading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case BOOK_NOW_CLICKED: {
      return {
        ...state,
        ...action.payload,
      };
    }
    case GET_CART_CONTENT_SUCCESS: {
      const {
        hotel,
        date,
        room,
        price,
        bid,
        coupon_code: couponCode,
      } = action.payload.response;
      return {
        ...state,
        hotel,
        date,
        room,
        price,
        bid,
        coupon_code: couponCode,
      };
    }
    case GET_CART_CONTENT_FAILURE:
      return {
        ...state,
        error: action.payload.error.msg,
      };
    case APPLY_REMOVE_COUPON_REQUEST:
      return {
        ...state,
        isCouponLoading: true,
      };
    case APPLY_REMOVE_COUPON_SUCCESS: {
      return {
        ...state,
        price: {
          ...state.price,
          ratePlans: {
            ...state.price.ratePlans,
            ...action.payload.response.price.ratePlans,
          },
        },
        coupon_code: action.payload.couponCode,
        isCouponLoading: false,
      };
    }
    case APPLY_WALLET_SUCCESS: {
      return {
        ...state,
        price: {
          ...state.price,
          ratePlans: {
            ...state.price.ratePlans,
            ...action.payload.response.price.ratePlans,
          },
        },
      };
    }
    case APPLY_REMOVE_COUPON_FAILURE: {
      return {
        ...state,
        price: {
          ...state.price,
          discount_error: action.payload.error[0].message,
        },
        isCouponLoading: false,
      };
    }
    case GET_COUPONS_SUCCESS:
      return {
        ...state,
        coupons: action.payload.response,
      };
    default:
      return state;
  }
};

export const getCartContent = (bookingId, ratePlan) => (dispatch, getState, { api }) => {
  const {
    search: {
      searchInput,
      datePicker,
      occupancy,
    },
    itinerary: {
      cart: { coupon_code: couponCode },
    },
    ui,
  } = getState();

  const itineraryQuery = {
    ...routeService.makeHotelIdQuery(searchInput.location.hotel_id),
    ...routeService.makeDateRangeQuery(datePicker.range.start, datePicker.range.end),
    ...routeService.makeRoomConfigQuery(occupancy.rooms),
    ...routeService.makeRoomTypeQuery(occupancy.roomType),
    ...routeService.makeRatePlanQuery(ratePlan),
    ...ui.utmParams,
    bid: bookingId,
    channel: config.channel,
    couponcode: couponCode,
  };

  return dispatch({
    types: [
      GET_CART_CONTENT_REQUEST,
      GET_CART_CONTENT_SUCCESS,
      GET_CART_CONTENT_FAILURE,
    ],
    promise: () => api.get('/api/web/v6/checkout/itinerary/', itineraryQuery)
      .then((response) => {
        analyticsService.googleRemarketing('Itinerary');
        return priceService.transformItineraryPricingApi(response);
      }),
  });
};

export const applyCoupon = (couponcode) => (dispatch, getState, { api }) => {
  const {
    itinerary: {
      cart: {
        bid,
      },
    },
    wallet: {
      byType,
    },
    ui,
  } = getState();
  const applyCouponQuery = {
    bid,
    couponcode,
    channel: config.channel,
    apply_wallet: byType.TP.isApplied,
    ...ui.utmParams,
  };

  return dispatch({
    types: [
      APPLY_REMOVE_COUPON_REQUEST,
      APPLY_REMOVE_COUPON_SUCCESS,
      APPLY_REMOVE_COUPON_FAILURE,
    ],
    payload: { couponCode: couponcode },
    promise: () => api.post('/api/web/v5/checkout/coupon/', applyCouponQuery)
      .then((response) => {
        analyticsService.couponApplied(couponcode, true);
        return priceService.transformCouponPricingApi(response, couponcode);
      }).catch((response) => {
        analyticsService.couponApplied(couponcode, false);
        throw response;
      }),
  });
};

export const removeCoupon = () => (dispatch, getState, { api }) => {
  const {
    itinerary: {
      cart: {
        bid,
      },
    },
    wallet: {
      byType,
    },
  } = getState();
  const removeCouponQuery = {
    bid,
    channel: config.channel,
    apply_wallet: byType.TP.isApplied,
  };

  return dispatch({
    types: [
      APPLY_REMOVE_COUPON_REQUEST,
      APPLY_REMOVE_COUPON_SUCCESS,
      APPLY_REMOVE_COUPON_FAILURE,
    ],
    payload: { couponCode: '' },
    promise: () => api.post('/api/web/v5/checkout/coupon/remove/', removeCouponQuery)
      .then((res) => priceService.transformCouponPricingApi(res)),
  });
};

export const getCoupons = (bid) => (dispatch, getState, { api }) => {
  const { ui } = getState();
  const getCouponsQuery = {
    bid,
    channel: config.channel,
    ...ui.utmParams,
  };
  return dispatch({
    types: [
      GET_COUPONS_REQUEST,
      GET_COUPONS_SUCCESS,
      GET_COUPONS_FAILURE,
    ],
    promise: () => api.get('/api/web/v2/discount/featured/', getCouponsQuery),
  });
};
