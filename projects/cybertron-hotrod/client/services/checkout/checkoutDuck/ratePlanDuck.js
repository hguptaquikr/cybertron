import recycleState from 'redux-recycle';
import analyticsService from '../../analytics/analyticsService';
import * as abService from '../../../services/ab/abService';

const CONTINUE_RATEPLAN = 'CONTINUE_RATEPLAN';
const TOGGLE_PANEL = 'TOGGLE_PANEL';
const GET_HOTEL_DETAILS_CONTENT_SUCCESS = 'GET_HOTEL_DETAILS_CONTENT_SUCCESS';
const GET_RESULTS_SUCCESS = 'GET_RESULTS_SUCCESS';
const GET_CONFIRMATION_CONTENT_SUCCESS = 'GET_CONFIRMATION_CONTENT_SUCCESS';
const SKIP_RATE_PLAN_STEP = 'SKIP_RATE_PLAN_STEP';

const initialState = {
  isOpen: true,
  isEditable: false,
  isVisible: true,
  section: 'ratePlans',
  summary: {
    title: 'SELECT PLAN',
    subtitle: 'Select a rate plan',
  },
};

export default recycleState(
  (state = initialState, action) => {
    switch (action.type) {
      case CONTINUE_RATEPLAN: {
        return {
          ...state,
          ...action.payload,
          isEditable: true,
          isOpen: false,
        };
      }
      case SKIP_RATE_PLAN_STEP: {
        return {
          ...state,
          ...action.payload,
          isEditable: true,
          isOpen: false,
        };
      }
      case TOGGLE_PANEL:
        return {
          ...state,
          isOpen: action.payload.panel === 'ratePlans',
        };
      default:
        return state;
    }
  }, [
    GET_CONFIRMATION_CONTENT_SUCCESS,
    GET_HOTEL_DETAILS_CONTENT_SUCCESS,
    GET_RESULTS_SUCCESS,
  ],
);

export const continueRatePLan = ({ ratePlan, newSummary }) => (dispatch) => {
  dispatch({
    type: CONTINUE_RATEPLAN,
    payload: newSummary,
  });
  analyticsService.checkoutStep('Rate Plan', {
    ratePlan,
    ...abService.actionProperties(['checkoutCtaDesktop']),
  });
};

export const skipRatePlanStep = (payload) => (dispatch) =>
  dispatch({ type: SKIP_RATE_PLAN_STEP, payload });
