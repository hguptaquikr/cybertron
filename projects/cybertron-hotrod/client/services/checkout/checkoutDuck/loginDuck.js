import analyticsService from '../../analytics/analyticsService';

const CONTINUE_LOGIN = 'CONTINUE_LOGIN';
const TOGGLE_PANEL = 'TOGGLE_PANEL';
const SET_AUTHENTICATED_USER = 'SET_AUTHENTICATED_USER';
const UNSET_AUTHENTICATED_USER = 'UNSET_AUTHENTICATED_USER';
const EXISTING_ACCOUNT = 'EXISTING_ACCOUNT';

const initialState = {
  existingAccount: false,
  isEditable: false,
  isVisible: true,
  section: 'login',
  isOpen: true,
  summary: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CONTINUE_LOGIN: {
      const { email } = action.payload;
      return {
        ...state,
        isEditable: true,
        isOpen: false,
        summary: {
          title: 'Welcome Guest',
          subtitle: `Booking confirmation will be sent to ${email}`,
        },
        email,
      };
    }
    case EXISTING_ACCOUNT:
      return {
        ...state,
        existingAccount: action.value,
      };
    case TOGGLE_PANEL:
      return {
        ...state,
        isOpen: action.payload.panel === 'login',
      };
    case SET_AUTHENTICATED_USER: {
      const { email, name } = action.payload.user;
      return {
        ...state,
        isOpen: false,
        isEditable: false,
        summary: {
          title: `Welcome ${name}`,
          subtitle: `Booking confirmation will be sent to ${email}`,
        },
        email,
      };
    }
    case UNSET_AUTHENTICATED_USER:
      return {
        ...state,
        isOpen: true,
        summary: {},
        email: '',
      };
    default:
      return state;
  }
};

export const continueLogin = (payload) => (dispatch) => {
  dispatch({
    type: CONTINUE_LOGIN,
    payload,
  });
  analyticsService.checkoutStep('Contact Info', payload);
};

// Move this into iternaryDuck
export const togglePanel = (panel) => ({
  type: TOGGLE_PANEL,
  payload: { panel },
});
