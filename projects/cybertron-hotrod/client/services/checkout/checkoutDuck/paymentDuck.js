import recycleState from 'redux-recycle';
import priceService from '../../price/priceService';
import analyticsService from '../../analytics/analyticsService';
import * as abService from '../../../services/ab/abService';

const TOGGLE_PANEL = 'TOGGLE_PANEL';
const CHECKOUT_PAY_NOW_PAYMENT_REQUEST = 'CHECKOUT_PAY_NOW_PAYMENT_REQUEST';
const CHECKOUT_PAY_NOW_PAYMENT_SUCCESS = 'CHECKOUT_PAY_NOW_PAYMENT_SUCCESS';
const CHECKOUT_PAY_NOW_PAYMENT_FAILURE = 'CHECKOUT_PAY_NOW_PAYMENT_FAILURE';
const CHECKOUT_PAY_AT_HOTEL_REQUEST = 'CHECKOUT_PAY_AT_HOTEL_REQUEST';
const CHECKOUT_PAY_AT_HOTEL_SUCCESS = 'CHECKOUT_PAY_AT_HOTEL_SUCCESS';
const CHECKOUT_PAY_AT_HOTEL_FAILURE = 'CHECKOUT_PAY_AT_HOTEL_FAILURE';
const CHECKOUT_CONFIRM_BOOKING_REQUEST = 'CHECKOUT_CONFIRM_BOOKING_REQUEST';
const CHECKOUT_CONFIRM_BOOKING_SUCCESS = 'CHECKOUT_CONFIRM_BOOKING_SUCCESS';
const CHECKOUT_CONFIRM_BOOKING_FAILURE = 'CHECKOUT_CONFIRM_BOOKING_FAILURE';
const CHECKOUT_OFFER_CONTENT_REQUEST = 'CHECKOUT_OFFER_CONTENT_REQUEST';
const CHECKOUT_OFFER_CONTENT_SUCCESS = 'CHECKOUT_OFFER_CONTENT_SUCCESS';
const CHECKOUT_OFFER_CONTENT_FAILURE = 'CHECKOUT_OFFER_CONTENT_FAILURE';
const GET_CART_CONTENT_REQUEST = 'GET_CART_CONTENT_REQUEST';
const GET_CART_CONTENT_SUCCESS = 'GET_CART_CONTENT_SUCCESS';
const GET_CONFIRMATION_CONTENT_SUCCESS = 'GET_CONFIRMATION_CONTENT_SUCCESS';
const GET_HOTEL_DETAILS_CONTENT_SUCCESS = 'GET_HOTEL_DETAILS_CONTENT_SUCCESS';
const GET_RESULTS_SUCCESS = 'GET_RESULTS_SUCCESS';
const UNSET_AUTHENTICATED_USER = 'UNSET_AUTHENTICATED_USER';
const APPLY_REMOVE_COUPON_SUCCESS = 'APPLY_REMOVE_COUPON_SUCCESS';
const CONTINUE_DETAILS = 'CONTINUE_DETAILS';
const DETAILS_CONFIRMATION = 'DETAILS_CONFIRMATION';
const APPLY_WALLET_SUCCESS = 'APPLY_WALLET_SUCCESS';
const HIDE_PAYMENT_OPTION = 'HIDE_PAYMENT_OPTION';

const initialState = {
  isOpen: false,
  isEditable: false,
  isVisible: true,
  section: 'payment',
  summary: {
    title: 'Payment options',
    subtitle: '',
  },
  order_id: '',
  offers: [],
  gateway: '',
  gatewayOrderId: '',
  serviceOrderId: '',
  isPayAtHotelEnabled: true,
  payNowEnabled: true,
  bookingApiError: '',
  isPrepaidOnly: false,
};

export default recycleState(
  (state = initialState, action) => {
    switch (action.type) {
      case TOGGLE_PANEL:
        return {
          ...state,
          isOpen: action.payload.panel === 'payment',
        };
      case GET_CART_CONTENT_SUCCESS: {
        const isPayAtHotelEnabled = 'pah_enabled' in action.payload.response
          ? action.payload.response.pah_enabled : true;
        const payNowEnabled = 'paynow_enabled' in action.payload.response
          ? action.payload.response.paynow_enabled : true;
        const isPrepaidOnly = 'is_prepaid' in action.payload.response
          ? action.payload.response.is_prepaid : false;
        const responseData = action.payload.response.price;
        return {
          ...state,
          isPayAtHotelEnabled,
          payNowEnabled,
          isPrepaidOnly,
          order_id: '',
          amount_to_send: responseData.ratePlans[responseData.selectedRatePlan].sellingPrice,
        };
      }
      case GET_CART_CONTENT_REQUEST:
      case CHECKOUT_PAY_NOW_PAYMENT_REQUEST:
      case CHECKOUT_PAY_AT_HOTEL_REQUEST:
        return {
          ...state,
          bookingApiError: '',
        };
      case CHECKOUT_PAY_NOW_PAYMENT_FAILURE:
      case CHECKOUT_PAY_AT_HOTEL_FAILURE:
        return {
          ...state,
          bookingApiError: action.payload.error.msg,
        };
      case CHECKOUT_PAY_NOW_PAYMENT_SUCCESS:
        return {
          ...state,
          ...action.payload.response,
        };
      case APPLY_REMOVE_COUPON_SUCCESS: {
        const { is_prepaid: isPrepaid, price } = action.payload.response;
        return {
          ...state,
          isPrepaidOnly: isPrepaid || false,
          amount_to_send: price.ratePlans[price.selectedRatePlan].sellingPrice,
        };
      }
      case APPLY_WALLET_SUCCESS: {
        const { price } = action.payload.response;
        return {
          ...state,
          amount_to_send: price.ratePlans[price.selectedRatePlan].sellingPrice,
        };
      }
      case CONTINUE_DETAILS: {
        return {
          ...state,
          isOpen: true,
          email: action.payload.email,
        };
      }
      case HIDE_PAYMENT_OPTION: {
        return {
          ...state,
          isOpen: false,
        };
      }
      case DETAILS_CONFIRMATION: {
        return {
          ...state,
          email: action.payload.email,
        };
      }
      case CHECKOUT_PAY_AT_HOTEL_SUCCESS:
        return {
          ...state,
          ...action.payload.response,
        };
      case UNSET_AUTHENTICATED_USER:
        return {
          ...state,
          isOpen: false,
          isEditable: false,
        };
      case CHECKOUT_OFFER_CONTENT_SUCCESS:
        return {
          ...state,
          offers: action.payload.response.value,
        };
      default:
        return state;
    }
  }, [
    GET_CONFIRMATION_CONTENT_SUCCESS,
    GET_HOTEL_DETAILS_CONTENT_SUCCESS,
    GET_RESULTS_SUCCESS,
    UNSET_AUTHENTICATED_USER,
  ],
);

const _buildPayData = ({
  itinerary: {
    cart: { bid, price },
    details: {
      name,
      mobile,
      email,
      hasGstin,
      companyName,
      companyAddress,
      gstin,
    },
  },
  otp,
}, ratePlan, gateway) => {
  const payData = {
    name,
    mobile,
    email,
    bid,
    gateway,
    check_in_time: '12pm',
    check_out_time: '11am',
    message: '',
    actual_total_cost: price.ratePlans[ratePlan].sellingPrice,
    otp_verified_number: otp.mobile,
  };
  if (hasGstin) {
    return {
      ...payData,
      organization_name: companyName,
      organization_address: companyAddress,
      organization_taxcode: gstin,
    };
  }
  return payData;
};

export const payNowPayment = (ratePlan, gateway) => (dispatch, getState, { api }) => {
  const payData = _buildPayData(getState(), ratePlan, gateway);
  const payNowPromise = dispatch({
    types: [
      CHECKOUT_PAY_NOW_PAYMENT_REQUEST,
      CHECKOUT_PAY_NOW_PAYMENT_SUCCESS,
      CHECKOUT_PAY_NOW_PAYMENT_FAILURE,
    ],
    promise: () => api.post('/api/web/v5/checkout/paynow/', payData)
      .then((res) => {
        if (gateway === 'razorpay') {
          return priceService.transformPayNowApi(res);
        } return res;
      }),
  });

  return payNowPromise;
};

export const payAtHotel = (ratePlan) => (dispatch, getState, { api }) => {
  const payData = _buildPayData(getState(), ratePlan);
  const {
    itinerary: {
      cart: {
        price: { ratePlans },
      },
    },
  } = getState();
  const price = ratePlans[ratePlan];
  const payAtHotelDispatch = dispatch({
    types: [
      CHECKOUT_PAY_AT_HOTEL_REQUEST,
      CHECKOUT_PAY_AT_HOTEL_SUCCESS,
      CHECKOUT_PAY_AT_HOTEL_FAILURE,
    ],
    promise: () => api.post('/api/web/v5/checkout/payathotel/', payData),
  }).then((response) => {
    analyticsService.exposeGlobally({
      confirm_order_analytics: response.order_event_args,
    });
    analyticsService.adwordsCompletedOrder({
      revenue: payData.actual_total_cost,
      price,
      ...abService.actionProperties(['pahCtaDesktop']),
    });
    return response;
  });

  return payAtHotelDispatch;
};

export const confirmBooking = ({ gateway, id, signature, selectedRatePlan: ratePlan }) =>
  (dispatch, getState, { api }) => {
    const {
      itinerary: {
        payment,
        cart: {
          price: { ratePlans },
        },
      },
    } = getState();

    const price = ratePlans[ratePlan];

    const bookingData = {
      order_id: payment.order_id,
      gateway,
      ps_order_id: payment.serviceOrderId,
      pg_order_id: payment.gatewayOrderId,
      pg_meta: {
        pg_payment_id: id,
        pg_signature: signature,
      },
    };

    const confirmBookingPromise = dispatch({
      types: [
        CHECKOUT_CONFIRM_BOOKING_REQUEST,
        CHECKOUT_CONFIRM_BOOKING_SUCCESS,
        CHECKOUT_CONFIRM_BOOKING_FAILURE,
      ],
      promise: () => api.post('/api/web/v5/checkout/confirmbooking/ ', bookingData),
    }).then((response) => {
      analyticsService.exposeGlobally({
        confirm_order_analytics: response.order_event_args,
      });
      analyticsService.adwordsCompletedOrder({
        revenue: payment.amount_to_store,
        price,
      });
      return response;
    });

    analyticsService.checkoutStep('Payment Confirmed', {
      orderId: payment.order_id,
      mode: gateway,
      psOrderId: payment.serviceOrderId,
      pgOrderId: payment.gatewayOrderId,
      pgPaymentId: id,
      price: Math.round(price.sellingPrice),
      totalPrice: Math.round(price.totalPrice),
    });
    return confirmBookingPromise;
  };

export const getOfferContent = () => (dispatch, getState, { api }) => {
  const query = {
    key: 'payment_gateway',
    version: 1,
  };

  return dispatch({
    types: [
      CHECKOUT_OFFER_CONTENT_REQUEST,
      CHECKOUT_OFFER_CONTENT_SUCCESS,
      CHECKOUT_OFFER_CONTENT_FAILURE,
    ],
    promise: () => api.get('/api/web/v1/contents/', query),
  });
};

export const hidePaymentOption = () => (dispatch) => dispatch({
  type: HIDE_PAYMENT_OPTION,
});
