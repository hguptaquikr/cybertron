import { combineReducers } from 'redux';

import cart, * as cartActionCreators from './cartDuck';
import login, * as loginActionCreators from './loginDuck';
import ratePlans, * as ratePlansActionCreators from './ratePlanDuck';
import details, * as detailsActionCreators from './detailsDuck';
import payment, * as paymentActionCreators from './paymentDuck';

export default combineReducers({
  cart,
  ratePlans,
  login,
  details,
  payment,
});

export const itineraryActionCreators = {
  ...cartActionCreators,
  ...loginActionCreators,
  ...ratePlansActionCreators,
  ...detailsActionCreators,
  ...paymentActionCreators,
};
