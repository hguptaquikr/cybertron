import recycleState from 'redux-recycle';
import analyticsService from '../../analytics/analyticsService';
import * as abService from '../../../services/ab/abService';

const CONTINUE_RATEPLAN = 'CONTINUE_RATEPLAN';
const CONTINUE_DETAILS = 'CONTINUE_DETAILS';
const TOGGLE_PANEL = 'TOGGLE_PANEL';
const SET_AUTHENTICATED_USER = 'SET_AUTHENTICATED_USER';
const UNSET_AUTHENTICATED_USER = 'UNSET_AUTHENTICATED_USER';
const CHECK_USER_REGISTER_REQUEST = 'CHECK_USER_REGISTER_REQUEST';
const CHECK_USER_REGISTER_SUCCESS = 'CHECK_USER_REGISTER_SUCCESS';
const CHECK_USER_REGISTER_FAILURE = 'CHECK_USER_REGISTER_FAILURE';
const SKIP_RATE_PLAN_STEP = 'SKIP_RATE_PLAN_STEP';
const GET_CONFIRMATION_CONTENT_SUCCESS = 'GET_CONFIRMATION_CONTENT_SUCCESS';
const DETAILS_CONFIRMATION = 'DETAILS_CONFIRMATION';
const HIDE_PAYMENT_OPTION = 'HIDE_PAYMENT_OPTION';

const initialState = {
  isOpen: false,
  hasGstin: false,
  isEditable: false,
  isVisible: true,
  section: 'details',
  name: '',
  mobile: '',
  email: '',
  companyName: '',
  companyAddress: '',
  gstin: '',
  summary: {
    title: 'Primary Traveller',
    subtitle: 'Who is the main traveller',
  },
};

export default recycleState(
  (state = initialState, action) => {
    switch (action.type) {
      case CONTINUE_RATEPLAN:
      case SKIP_RATE_PLAN_STEP:
      case HIDE_PAYMENT_OPTION:
        return {
          ...state,
          isOpen: true,
        };
      case CONTINUE_DETAILS: {
        const { name, mobile, gstin } = action.payload;
        return {
          ...state,
          isEditable: true,
          isOpen: false,
          hasGstin: !!gstin,
          summary: {
            title: 'Primary Traveller',
            subtitle: `${name}, ${mobile}`,
          },
          ...action.payload,
        };
      }
      case DETAILS_CONFIRMATION: {
        const { gstin } = action.payload;
        return {
          ...state,
          hasGstin: !!gstin,
          ...action.payload,
        };
      }
      case TOGGLE_PANEL:
        return {
          ...state,
          isOpen: action.payload.panel === 'details',
        };
      case SET_AUTHENTICATED_USER: {
        const { name, mobile, email } = action.payload.user;
        return {
          ...state,
          name,
          mobile,
          email,
        };
      }
      case UNSET_AUTHENTICATED_USER:
        return {
          ...state,
          name: '',
          mobile: '',
          email: '',
          isOpen: true,
        };
      default:
        return state;
    }
  }, [
    GET_CONFIRMATION_CONTENT_SUCCESS,
  ],
);

export const continueDetails = (payload) => (dispatch, getState, { api }) => {
  dispatch({
    type: CONTINUE_DETAILS,
    payload,
  });
  analyticsService.checkoutStep('Traveller Info Added', {
    ...payload,
    ...abService.actionProperties(['checkoutDetailsCtaDesktop']),
  });
  analyticsService.primaryTravelerDetail(payload);
  return dispatch({
    types: [
      CHECK_USER_REGISTER_REQUEST,
      CHECK_USER_REGISTER_SUCCESS,
      CHECK_USER_REGISTER_FAILURE,
    ],
    promise: () => api.get('/api/web/v1/booking/booking-request/', payload),
  });
};

export const detailsConfirmation = (payload) => (dispatch, getState, { api }) => {
  dispatch({
    type: DETAILS_CONFIRMATION,
    payload,
  });
  analyticsService.checkoutStep('Traveller Info Added', payload);
  analyticsService.primaryTravelerDetail(payload);
  return dispatch({
    types: [
      CHECK_USER_REGISTER_REQUEST,
      CHECK_USER_REGISTER_SUCCESS,
      CHECK_USER_REGISTER_FAILURE,
    ],
    promise: () => api.get('/api/web/v1/booking/booking-request/', payload),
  });
};
