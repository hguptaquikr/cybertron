import zip from 'lodash/zip';
import startCase from 'lodash/startCase';
import moment from 'moment';
import analyticsService from '../analytics/analyticsService';
import authService from '../auth/authService';
import { constants, pluralize } from '../utils';
import config from '../../../config';

export default {
  getGeoLocation() {
    return new Promise(((resolve, reject) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(({ coords: { latitude, longitude } }) => {
          analyticsService.browserLocationTracked(true, latitude, longitude);
          resolve({
            lat: latitude,
            lng: longitude,
          });
        }, () => {
          analyticsService.browserLocationTracked(false);
          reject(false);
        });
      } else {
        analyticsService.browserLocationTracked(false);
        reject(false);
      }
    }));
  },

  async getIpLocation() {
    const response = await fetch(`https://pro.ip-api.com/json/${authService.userIp}?key=${config.ipApiKey}`);
    const json = await response.json();
    if (response.ok) {
      analyticsService.ipLocationTracked(true, json.city);
      return {
        city: json.city,
      };
    }
    analyticsService.ipLocationTracked(false);
    return {
      city: '',
    };
  },

  parseRoomConfig(roomConfig) {
    const configSep = roomConfig[1];
    const isNormalConfig = configSep === '-';
    const roomSep = isNormalConfig ? ',' : '-';
    let configArr = roomConfig.split(roomSep).map((room) => room.split(configSep));

    if (!isNormalConfig) {
      configArr = zip(...configArr);
    }

    return configArr.map(([adults, kids]) => ({ adults, kids }));
  },

  formattedDatePicker({ start, end }, dateFormat = constants.dateFormat.view) {
    const checkIn = start ? start.format(dateFormat) : '';
    const checkOut = end ? end.format(dateFormat) : '';
    const numberOfNights = start && end ? end.diff(start, 'days') : '';
    const abwDays = moment(start).startOf('day').diff(moment().startOf('day'), 'days');
    return {
      checkIn,
      checkOut,
      abwDays,
      numberOfNights: {
        count: numberOfNights,
        text: `${numberOfNights} ${pluralize(numberOfNights, 'night')}`,
      },
    };
  },

  getSearchState(combinedQuery) {
    const searchState = {};

    let label = '';
    if (combinedQuery.q) {
      label = combinedQuery.q;
    } else if (combinedQuery.hotelSlug) {
      label = `${startCase(combinedQuery.hotelSlug)}, ${startCase(combinedQuery.city)}`;
    } else if (combinedQuery.locality || combinedQuery.landmark) {
      label = startCase(combinedQuery.locality || combinedQuery.landmark);
    } else if (combinedQuery.city) {
      label = startCase(combinedQuery.city);
    }

    searchState.location = {
      label,
      hotel_id: combinedQuery.hotelId || combinedQuery.hotel_id,
      area: {
        landmark: startCase(combinedQuery.landmark),
        locality: startCase(combinedQuery.locality),
        city: startCase(combinedQuery.city),
        state: combinedQuery.state,
        country: combinedQuery.country || 'India',
      },
      coordinates: {
        lat: combinedQuery.lat,
        lng: combinedQuery.lng,
      },
    };

    if (__BROWSER__) {
      const today = moment();
      let checkin = moment(combinedQuery.checkin);
      let checkout = moment(combinedQuery.checkout);
      checkin = checkin.diff(today, 'days') >= 0 ? checkin : today;
      checkout = checkout.diff(checkin, 'days') >= 1 ? checkout : moment(checkin).add(1, 'days');
      searchState.range = {
        start: checkin,
        end: checkout,
      };
    }

    if (combinedQuery.roomconfig) {
      searchState.rooms = this.parseRoomConfig(combinedQuery.roomconfig);
    }

    searchState.roomType = combinedQuery.roomtype;

    return searchState;
  },
};
