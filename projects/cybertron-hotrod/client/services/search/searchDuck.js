import isEmpty from 'lodash/isEmpty';
import google from '../../services/google/googleService';
import searchService from './searchService';
import * as routeService from '../route/routeService';

const SET_SEARCH_STATE = 'SET_SEARCH_STATE';
const GET_SEARCH_SUGGESTIONS_PENDING = 'GET_SEARCH_SUGGESTIONS_PENDING';
const GET_SEARCH_SUGGESTIONS_RESOLVED = 'GET_SEARCH_SUGGESTIONS_RESOLVED';
const GET_SEARCH_SUGGESTIONS_REJECTED = 'GET_SEARCH_SUGGESTIONS_REJECTED';
const CLEAR_SEARCH_SUGGESTIONS = 'CLEAR_SEARCH_SUGGESTIONS';
const SEARCH_LOCATION_CHANGE = 'SEARCH_LOCATION_CHANGE';
const GET_LOCATION_DETAILS_PENDING = 'GET_LOCATION_DETAILS_PENDING';
const GET_LOCATION_DETAILS_RESOLVED = 'GET_LOCATION_DETAILS_RESOLVED';
const GET_LOCATION_DETAILS_REJECTED = 'GET_LOCATION_DETAILS_REJECTED';
const SET_OCCUPANCY_VISIBILITY = 'SET_OCCUPANCY_VISIBILITY';
const SET_OCCUPANCY_ROOMS = 'SET_OCCUPANCY_ROOMS';
const SET_OCCUPANCY_CUSTOMROOM_VISIBILITY = 'SET_OCCUPANCY_CUSTOMROOM_VISIBILITY';
const SET_OCCUPANCY_ADULTS = 'SET_OCCUPANCY_ADULTS';
const SET_OCCUPANCY_KIDS = 'SET_OCCUPANCY_KIDS';
const ADD_OCCUPANCY_ROOM = 'ADD_OCCUPANCY_ROOM';
const REMOVE_OCCUPANCY_ROOM = 'REMOVE_OCCUPANCY_ROOM';
const SET_DATE_PICKER_VISIBILITY = 'SET_DATE_PICKER_VISIBILITY';
const DATE_RANGE_CHANGE = 'DATE_RANGE_CHANGE';
const CHECK_HOTEL_AVAILABILITY_REQUEST = 'CHECK_HOTEL_AVAILABILITY_REQUEST';
const CHECK_HOTEL_AVAILABILITY_SUCCESS = 'CHECK_HOTEL_AVAILABILITY_SUCCESS';
const CHECK_HOTEL_AVAILABILITY_FAILURE = 'CHECK_HOTEL_AVAILABILITY_FAILURE';

const initialState = {
  searchInput: {
    isLoading: false,
    location: {},
    suggestions: [],
  },
  datePicker: {
    isVisible: false,
    highlight: {
      checkIn: false,
      checkOut: false,
    },
    range: {
      start: null,
      end: null,
    },
  },
  occupancy: {
    visible: false,
    customRoomVisible: false,
    rooms: [{ adults: 1 }],
    roomType: '',
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_SEARCH_STATE:
      return {
        ...state,
        searchInput: {
          ...state.searchInput,
          location: action.payload.location || state.searchInput.location,
        },
        occupancy: {
          ...state.occupancy,
          rooms: action.payload.rooms || state.occupancy.rooms,
          roomType: action.payload.roomType || state.occupancy.roomType,
        },
        datePicker: {
          ...state.datePicker,
          range: action.payload.range || state.datePicker.range,
        },
      };
    case GET_SEARCH_SUGGESTIONS_PENDING:
      return {
        ...state,
        searchInput: {
          ...state.searchInput,
          isLoading: true,
        },
      };
    case GET_SEARCH_SUGGESTIONS_RESOLVED:
      return {
        ...state,
        searchInput: {
          ...state.searchInput,
          isLoading: false,
          suggestions: action.payload.response,
        },
      };
    case GET_SEARCH_SUGGESTIONS_REJECTED:
      return {
        ...state,
        searchInput: {
          ...state.searchInput,
          isLoading: false,
          error: action.payload.error,
        },
      };
    case CLEAR_SEARCH_SUGGESTIONS:
      return {
        ...state,
        searchInput: {
          ...state.searchInput,
          suggestions: [],
        },
      };
    case SEARCH_LOCATION_CHANGE:
      return {
        ...state,
        searchInput: {
          ...state.searchInput,
          location: action.payload.location,
        },
      };
    case GET_LOCATION_DETAILS_PENDING:
      return state;
    case GET_LOCATION_DETAILS_RESOLVED:
      return {
        ...state,
        searchInput: {
          ...state.searchInput,
          location: {
            ...state.searchInput.location,
            ...action.payload.location,
          },
        },
      };
    case GET_LOCATION_DETAILS_REJECTED:
      return {
        ...state,
        error: action.payload.error,
      };
    case SET_OCCUPANCY_VISIBILITY:
      return {
        ...state,
        occupancy: {
          ...state.occupancy,
          visible: action.payload.visibility,
        },
      };
    case SET_OCCUPANCY_CUSTOMROOM_VISIBILITY:
      return {
        ...state,
        occupancy: {
          ...state.occupancy,
          customRoomVisible: action.payload.customRoomVisible,
        },
      };
    case SET_OCCUPANCY_ROOMS:
      return {
        ...state,
        occupancy: {
          ...state.occupancy,
          visible: false,
          rooms: action.payload.rooms,
        },
      };
    case SET_OCCUPANCY_ADULTS: {
      const { rooms } = state.occupancy;
      const { index } = action.payload;
      const updatedRoom = {
        adults: +action.payload.value,
        kids: +rooms[index].kids,
      };
      return {
        ...state,
        occupancy: {
          ...state.occupancy,
          rooms: [
            ...rooms.slice(0, index),
            updatedRoom,
            ...rooms.slice(index + 1),
          ],
        },
      };
    }
    case SET_OCCUPANCY_KIDS: {
      const { rooms } = state.occupancy;
      const { index } = action.payload;
      const updatedRoom = {
        adults: +rooms[index].adults,
        kids: +action.payload.value,
      };
      return {
        ...state,
        occupancy: {
          ...state.occupancy,
          rooms: [
            ...rooms.slice(0, index),
            updatedRoom,
            ...rooms.slice(index + 1),
          ],
        },
      };
    }
    case ADD_OCCUPANCY_ROOM: {
      const { rooms } = state.occupancy;
      return {
        ...state,
        occupancy: {
          ...state.occupancy,
          rooms: [
            ...rooms.slice(0),
            { adults: 1, kids: 0 },
          ],
        },
      };
    }
    case REMOVE_OCCUPANCY_ROOM: {
      const { rooms } = state.occupancy;
      const { index } = action.payload;
      return {
        ...state,
        occupancy: {
          ...state.occupancy,
          rooms: [
            ...rooms.slice(0, index),
            ...rooms.slice(index + 1),
          ],
        },
      };
    }
    case SET_DATE_PICKER_VISIBILITY:
      return {
        ...state,
        datePicker: {
          ...state.datePicker,
          isVisible: action.payload.isVisible,
          highlight: action.payload.highlight,
        },
      };
    case DATE_RANGE_CHANGE:
      return {
        ...state,
        datePicker: {
          ...state.datePicker,
          range: action.payload.range,
        },
      };
    default:
      return state;
  }
};

export const getSearchSuggestions = (input) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_SEARCH_SUGGESTIONS_PENDING,
      GET_SEARCH_SUGGESTIONS_RESOLVED,
      GET_SEARCH_SUGGESTIONS_REJECTED,
    ],
    promise: () => api.get('/api/web/v1/search/autocomplete/', {
      search_string: input,
    })
      .then((res) => {
        if ('near me'.includes(input.toLowerCase())) {
          res.data.unshift({
            label: 'Near Me',
            type: 'near_me',
            icon: 'icon-location',
            area: { country: 'India' },
          });
        }
        return res;
      }),
  });

export const occupancyVisibilty = (visibility) => ({
  type: SET_OCCUPANCY_VISIBILITY,
  payload: {
    visibility,
  },
});

export const occupancyRoomsChange = (rooms) => ({
  type: SET_OCCUPANCY_ROOMS,
  payload: {
    rooms,
  },
});

export const occupancyAdultsChange = (index, value) => ({
  type: SET_OCCUPANCY_ADULTS,
  payload: {
    index,
    value,
  },
});

export const occupancyKidsChange = (index, value) => ({
  type: SET_OCCUPANCY_KIDS,
  payload: {
    index,
    value,
  },
});

export const occupancyAddRoom = () => ({
  type: ADD_OCCUPANCY_ROOM,
});

export const occupancyRemoveRoom = (index) => ({
  type: REMOVE_OCCUPANCY_ROOM,
  payload: {
    index,
  },
});

export const occupancyCustomRoomsVisibilty = (customRoomVisible) => (dispatch, getState) => {
  const { search: { occupancy } } = getState();
  dispatch({
    type: SET_OCCUPANCY_CUSTOMROOM_VISIBILITY,
    payload: { customRoomVisible },
  });
  if (!occupancy.rooms.length) dispatch(occupancyAddRoom());
};

export const setDatePickerVisibility = (isVisible, highlight) => (dispatch) => {
  const highlightBasedOnVisibility = {};
  if (isVisible) {
    highlightBasedOnVisibility.checkIn = true;
    highlightBasedOnVisibility.checkOut = false;
  } else {
    highlightBasedOnVisibility.checkIn = false;
    highlightBasedOnVisibility.checkOut = false;
  }

  dispatch({
    type: SET_DATE_PICKER_VISIBILITY,
    payload: {
      isVisible,
      highlight: highlight || highlightBasedOnVisibility,
    },
  });
};

export const dateRangeChange = (range) => ({
  type: DATE_RANGE_CHANGE,
  payload: { range },
});

export const checkHotelAvailability = () => (dispatch, getState, { api }) => {
  const { searchInput, datePicker, occupancy } = getState().search;
  const hotelId = searchInput.location.hotel_id;
  const availabilityQuery = {
    ...routeService.makeDateRangeQuery(datePicker.range.start, datePicker.range.end),
    ...routeService.makeRoomConfigQuery(occupancy.rooms),
  };

  return dispatch({
    types: [
      CHECK_HOTEL_AVAILABILITY_REQUEST,
      CHECK_HOTEL_AVAILABILITY_SUCCESS,
      CHECK_HOTEL_AVAILABILITY_FAILURE,
    ],
    promise: () => api.get(`/api/web/v2/hotels/${hotelId}/availability/`, availabilityQuery),
  });
};

export const validateSearch = (searchInputRef) => (dispatch, getState) => {
  const { search: { searchInput, datePicker, occupancy } } = getState();
  if (!searchInput.location.label || isEmpty(searchInput.location.area)) {
    if (searchInputRef) searchInputRef.focus();
    return false;
  } else if (!datePicker.range.start || !datePicker.range.end) {
    dispatch(setDatePickerVisibility(true));
    return false;
  } else if (!occupancy.rooms.length) {
    dispatch(occupancyVisibilty(true));
    return false;
  }
  return true;
};

export const isSearchValid = () => (dispatch, getState) => {
  const { search: { searchInput, datePicker, occupancy } } = getState();
  if (!searchInput.location.label || isEmpty(searchInput.location.area)) {
    return false;
  } else if (!datePicker.range.start || !datePicker.range.end) {
    return false;
  } else if (!occupancy.rooms.length) {
    return false;
  }
  return true;
};

export const clearSearchSuggestions = () => ({
  type: CLEAR_SEARCH_SUGGESTIONS,
});

export const searchLocationChange = (location) => (dispatch, getState) => {
  const { search: { datePicker } } = getState();
  dispatch({
    type: SEARCH_LOCATION_CHANGE,
    payload: { location },
  });
  if (location.label && (!datePicker.range.start || !datePicker.range.end)) {
    dispatch(setDatePickerVisibility(true));
  }
};

export const getLocationDetails = (location) => (dispatch) => {
  dispatch({ type: GET_LOCATION_DETAILS_PENDING });

  google.placesHandler.getDetails({
    placeId: location.place_id,
  }, (place, status) => {
    if (status === 'OK') {
      dispatch({
        type: GET_LOCATION_DETAILS_RESOLVED,
        payload: { location: google.constructLocation(place) },
      });
    } else {
      dispatch({
        type: GET_LOCATION_DETAILS_REJECTED,
        payload: { error: status },
      });
    }
  });
};

export const setSearchState = (urlQuery) => (dispatch, getState) => {
  const { search: { datePicker: { range } } } = getState();
  let dataQuery = urlQuery;
  if (!urlQuery.checkin && range.start) {
    dataQuery = {
      ...urlQuery,
      checkin: range.start,
      checkout: range.end,
    };
  }
  dispatch({
    type: SET_SEARCH_STATE,
    payload: searchService.getSearchState(dataQuery),
  });
  dispatch(validateSearch());
};
