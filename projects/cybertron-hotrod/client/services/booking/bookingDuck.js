import priceService from '../price/priceService';
import analyticsService from '../analytics/analyticsService';

const GET_CONFIRMATION_CONTENT_REQUEST = 'GET_CONFIRMATION_CONTENT_REQUEST';
const GET_CONFIRMATION_CONTENT_SUCCESS = 'GET_CONFIRMATION_CONTENT_SUCCESS';
const GET_CONFIRMATION_CONTENT_FAILURE = 'GET_CONFIRMATION_CONTENT_FAILURE';
const GET_PAYMENT_CONFIRMATION_DETAILS_REQUEST = 'GET_PAYMENT_CONFIRMATION_DETAILS_REQUEST';
const GET_PAYMENT_CONFIRMATION_DETAILS_SUCCESS = 'GET_PAYMENT_CONFIRMATION_DETAILS_SUCCESS';
const GET_PAYMENT_CONFIRMATION_DETAILS_FAILURE = 'GET_PAYMENT_CONFIRMATION_DETAILS_FAILURE';

const initialState = {
  user: {
    name: '',
    email: '',
    contact_number: '',
  },
  hotel: {
    id: '',
    city: '',
    name: '',
    locality: '',
    street: '',
    image_url: '',
    directions: [],
    coordinates: {},
    policies: [],
  },
  date: {},
  price: {},
  ratePlan: {},
  room: {},
  paymentDetails: {
    status: '',
  },
  attempts: 0,
  maxAttempts: 10,
  isError: false,
};

export default (state = initialState, action) => {
  const { payload, type } = action;
  switch (type) {
    case GET_CONFIRMATION_CONTENT_REQUEST:
      return state;
    case GET_CONFIRMATION_CONTENT_SUCCESS:
      return {
        ...state,
        ...payload.response,
      };
    case GET_CONFIRMATION_CONTENT_FAILURE:
      return {
        ...state,
        error: payload.error,
      };
    case GET_PAYMENT_CONFIRMATION_DETAILS_SUCCESS: {
      return {
        ...state,
        paymentDetails: payload.response,
        attempts: state.attempts + 1,
      };
    }
    case GET_PAYMENT_CONFIRMATION_DETAILS_FAILURE:
      return {
        ...state,
        error: payload.error,
      };
    default:
      return state;
  }
};

export const getConfirmationContent = (bookingId) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_CONFIRMATION_CONTENT_REQUEST,
      GET_CONFIRMATION_CONTENT_SUCCESS,
      GET_CONFIRMATION_CONTENT_FAILURE,
    ],
    promise: () => api.get(`/api/web/v5/checkout/confirmed-booking/${bookingId}/`)
      .then((response) => {
        analyticsService.googleRemarketing('Confirmation');
        return priceService.transformConfirmationPricingApi(response);
      }),
  });

export const getPaymentConfirmationDetails = (bookingId) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_PAYMENT_CONFIRMATION_DETAILS_REQUEST,
      GET_PAYMENT_CONFIRMATION_DETAILS_SUCCESS,
      GET_PAYMENT_CONFIRMATION_DETAILS_FAILURE,
    ],
    promise: () => api.get(`/api/web/v5/checkout/payment/status/${bookingId}/`),
  });
