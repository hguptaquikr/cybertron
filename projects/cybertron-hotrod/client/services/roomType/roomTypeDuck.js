import merge from 'lodash/merge';

const GET_HOTEL_DETAILS_SUCCESS = 'GET_HOTEL_DETAILS_SUCCESS';
const GET_SEARCH_PRICES_SUCCESS = 'GET_SEARCH_PRICES_SUCCESS';
const GET_NEARBY_SEARCH_PRICES_SUCCESS = 'GET_NEARBY_SEARCH_PRICES_SUCCESS';

const initialState = {
  byId: {
    '0|oak': {
      type: 'oak',
      maxOccupancy: {},
    },
  },
  ids: [],
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_SEARCH_PRICES_SUCCESS:
    case GET_NEARBY_SEARCH_PRICES_SUCCESS:
    case GET_HOTEL_DETAILS_SUCCESS:
      return {
        byId: merge(
          {},
          state.byId,
          payload.response.roomType.byId,
        ),
        ids: Array.from(
          new Set([
            ...state.ids,
            ...payload.response.roomType.ids,
          ]),
        ),
      };
    default:
      return state;
  }
};
