export default {
  constructRoomTypeId(hotelId, roomType) {
    return `${hotelId}|${roomType}`;
  },
};
