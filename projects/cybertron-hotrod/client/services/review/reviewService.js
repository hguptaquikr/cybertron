import api from '../api/apiService';

export default {
  updateSubmitReviewStatus(bookingId) {
    api.get('/api/web/v1/growth/communication/update_review_status/', { booking_id: bookingId });
  },

  getTAReviews(reviews) {
    return !__BROWSER__ ? reviews.filter((review) => review.review_detail.is_crawlable) : reviews;
  },

  getJCR(reviews) {
    return !__BROWSER__ ? reviews.filter((review) => review.isCrawlable) : reviews;
  },
};
