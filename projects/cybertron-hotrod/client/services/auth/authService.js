import qs from 'query-string';
import config from '../../../config';
import storageService from '../storage/storageService';

export default {
  userIp: '',

  hasSeenRewardsLandingPage(location, history) {
    const query = qs.parse(location.search);
    if (__BROWSER__) {
      const hasSeenWalletLandingPage = window.localStorage.getItem('wallet_landing_page');
      if (this.isLoggedIn()) {
        if (!hasSeenWalletLandingPage) {
          this.setRewardsLandingPageViewed();
          return true;
        }
        if (query.intro) {
          return true;
        }
      } else {
        return true;
      }
      history.replace(`/rewards/?flow=${query.flow}`);
    }
    return true;
  },

  isLoggedIn() {
    return !!this.getAuthToken() && !!this.getUser();
  },

  getUser() {
    const user = storageService.getItem('user');
    return JSON.parse(user || 'false');
  },

  getAuthToken() {
    if (__BROWSER__) {
      return window.localStorage.getItem('auth_token');
    }
    return true; // during ssr
  },

  refreshAuthToken(newAuthToken) {
    if (newAuthToken) {
      this.setAuthToken(newAuthToken.split(' ')[1]);
    }
  },

  setAuthToken(token) {
    storageService.setItem('auth_token', token);
  },

  setUser(user) {
    storageService.setItem('user', JSON.stringify(user));
  },

  unsetAuthData() {
    storageService.removeItem('auth_token');
    storageService.removeItem('user');
  },

  setRewardsLandingPageViewed() {
    if (__BROWSER__) {
      window.localStorage.setItem('wallet_landing_page', true);
    }
  },

  unsetRewardsLandingPageViewed() {
    if (__BROWSER__) {
      window.localStorage.removeItem('wallet_landing_page');
    }
  },

  transformLoginApi(res) {
    return {
      user: {
        email: res.email,
        mobile: res.phone_number,
        firstName: res.first_name,
        lastName: res.last_name,
        name: `${res.first_name}${res.last_name ? ` ${res.last_name}` : ''}`,
        userId: res.id,
      },
      token: res.token,
    };
  },

  verifyDomain(origin) {
    if (
      __APP_ENV__ === 'production'
      && __BROWSER__
      && origin
      && !config.hostUrl.startsWith(origin)
    ) {
      throw new Error('Invalid Host');
    }
  },
};
