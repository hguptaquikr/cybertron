import authService from './authService';

export default ({ history }) => ({ dispatch }) => (next) => (action) => {
  if (action.payload && action.payload.status === 401) {
    authService.unsetAuthData();
    const dispatchResult = dispatch({ type: 'UNSET_AUTHENTICATED_USER' });
    history.push('/login/');
    return dispatchResult;
  }
  return next(action);
};
