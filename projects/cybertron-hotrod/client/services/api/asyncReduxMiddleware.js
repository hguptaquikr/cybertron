export default () => (next) => (action) => {
  const { types, promise, payload } = action;

  if (!promise) {
    return next(action);
  }

  const [REQUEST, SUCCESS, FAILURE] = types;

  next({
    type: REQUEST,
    payload,
  });

  return promise()
    .then((res) => {
      next({
        type: SUCCESS,
        payload: {
          ...payload,
          response: res.data,
        },
      });
      return res.data;
    })
    .catch((res) => {
      next({
        type: FAILURE,
        payload: {
          ...payload,
          status: res.status,
          error: res.error || res.errors,
        },
      });
      return Promise.reject(res.error || res.errors);
    });
};
