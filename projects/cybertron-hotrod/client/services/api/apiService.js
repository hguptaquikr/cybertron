import fetch from 'isomorphic-fetch';
import queryString from 'query-string';
import config from '../../../config';
import * as abService from '../ab/abService';
import analyticsService from '../analytics/analyticsService';
import authService from '../auth/authService';

const fireRequest = async (method, url, data) => {
  const fullUrl = `${config.hostUrl}${url}`;
  const options = {
    method,
    body: JSON.stringify(data),
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
      abUserId: abService.getUser().id,
      Authorization: `Bearer ${authService.getAuthToken()}`,
    },
  };

  analyticsService.trackPerformanceStart('apiCall', url);
  const response = await fetch(fullUrl, options);
  const json = await response.json();
  analyticsService.trackPerformanceEnd('apiCall', url, { status: response.status });
  authService.refreshAuthToken(response.headers.get('x-auth-token'));
  return response.ok ? json : Promise.reject(json);
};

export default {
  get(url, query) {
    const qs = queryString.stringify(query, { arrayFormat: 'index' });
    return fireRequest('GET', `${url}?${qs}`);
  },

  post(url, data) {
    return fireRequest('POST', url, data);
  },

  put(url, data) {
    return fireRequest('PUT', url, data);
  },

  delete(url) {
    return fireRequest('DELETE', url);
  },
};
