/* eslint-disable max-len */
import get from 'lodash/get';
import set from 'lodash/set';
import cookies from 'js-cookie';
import qs from 'query-string';
import startCase from 'cybertron-utils/startCase';
import moment from 'moment';
import { matchRoutes } from 'react-router-config';
import analyticsService from 'cybertron-service-analytics/lib/analytics';
import * as analyticsConstants from 'cybertron-service-analytics/lib/analytics/constants';
import * as abService from '../ab/abService';
import authService from '../auth/authService';
import routes from '../route/routes';
import { getTotalGuests, performanceMark, getPerformanceDuration, constants } from '../utils';

export default {
  getState: () => ({}),

  history: {},

  init(getState, history) {
    this.getState = getState;
    this.history = history;
    analyticsService.init({ platform: 'desktop' });
  },

  pageTransition() {
    const pageName = this.getCurrentRoute().name;
    this.pageViewed(pageName);
  },

  remarketingProperties(name) {
    const state = this.getState();
    const properties = {
      hrental_startdate: get(state, 'search.datePicker.range.start') ? (get(state, 'search.datePicker.range.start')).toDate() : '',
      hrental_enddate: get(state, 'search.datePicker.range.end') ? (get(state, 'search.datePicker.range.end')).toDate() : '',
    };
    if (name === 'Home') {
      properties.hrental_pagetype = 'home';
    } else if (name === 'Search' || name === 'Hotels in City') {
      properties.hrental_pagetype = 'searchresults';
    } else if (name === 'Hotel Detail') {
      properties.hrental_pagetype = 'offerdetail';
      properties.hrental_id = get(state, 'search.searchInput.location.hotel_id');
      properties.hrental_totalvalue = state.hotel.byId[properties.hrental_id] &&
        state.price.byRoomTypeId[state.hotel.byId[properties.hrental_id].roomTypeIds[0]] ?
        Object.values(state.price.byRoomTypeId[state.hotel.byId[properties.hrental_id].roomTypeIds[0]].ratePlans)[0].sellingPrice
        : null;
    } else if (name === 'Itinerary') {
      properties.hrental_pagetype = 'conversionintent';
      properties.hrental_id = get(state, 'itinerary.cart.hotel.id');
      properties.hrental_totalvalue = get(state, 'itinerary.cart.price.ratePlans') ?
        Object.values(get(state, 'itinerary.cart.price.ratePlans'))[0].sellingPrice : null;
    } else if (name === 'Confirmation') {
      properties.hrental_pagetype = 'conversion';
      properties.hrental_id = get(state, 'confirmation.hotel.id');
      properties.hrental_totalvalue = get(state, 'confirmation.price.actual_total_cost');
    }
    return properties;
  },

  pageViewed(page, properties = {}) {
    analyticsService.page(page, {
      ...properties,
      nonInteraction: 1,
      ...this.remarketingProperties(page),
    });
  },

  trackNonInteractiveEvents(eventName, properties = {}) {
    analyticsService.track(`${eventName} Content Viewed`, {
      ...properties,
      nonInteraction: 1,
      pageName: startCase(this.getCurrentRoute().name),
    });
  },

  trackInteractiveEvents(eventName, properties = {}) {
    analyticsService.track(`${eventName}`, {
      ...properties,
      pageName: startCase(this.getCurrentRoute().name),
    });
  },

  googleRemarketing(page) {
    const analyticsProperties = {
      ...this.remarketingProperties(page),
    };
    window.analytics.track('Google Remarketing', analyticsProperties);
  },

  recordPerformance(pathname) {
    if (window.performance && window.performance.getEntriesByType) {
      const performanceProperties = [
        ...window.performance.getEntriesByName('first-paint'),
        ...window.performance.getEntriesByName('first-contentful-paint'),
        ...window.performance.getEntriesByName('first-interaction'),
      ].reduce((properties, entry) => ({
        ...properties,
        [entry.name]: entry.startTime,
      }), {});
      let connectionMeta = {};
      if (navigator && navigator.connection) {
        connectionMeta = {
          connectionEffectiveType: navigator.connection.effectiveType,
          connectionDownlink: navigator.connection.downlink,
        };
      }
      window.analytics.track('Record Performance', {
        ...performanceProperties,
        ...connectionMeta,
        pathname,
      });
    }
  },

  trackPerformanceMeta: {},

  trackPerformanceStart(type, name, meta = {}) {
    if (__BROWSER__) {
      performanceMark(`${type}|${name}|start`);
      this.trackPerformanceMeta[`${type}|${name}`] = meta;
    }
  },

  trackPerformanceEnd(type, name, meta = {}) {
    if (__BROWSER__) {
      performanceMark(`${type}|${name}|end`);
      const duration = getPerformanceDuration(
        `${type}|${name}`,
        `${type}|${name}|start`,
        `${type}|${name}|end`,
      );
      if (duration) {
        let connectionMeta = {};
        if (navigator && navigator.connection) {
          connectionMeta = {
            connectionEffectiveType: navigator.connection.effectiveType,
            connectionDownlink: navigator.connection.downlink,
          };
        }
        window.analytics.track('Track Performance', {
          ...this.trackPerformanceMeta[`${type}|${name}`],
          ...meta,
          ...connectionMeta,
          nonInteraction: 1,
          type,
          name,
          duration,
        });
      }
    }
  },

  getCurrentRoute() {
    const branches = matchRoutes(routes, this.history.location.pathname);
    const branch = branches[branches.length - 1];
    return branch.route || {};
  },

  exposeGlobally(properties = {}) {
    const {
      search: {
        datePicker: { range },
        occupancy: { rooms },
      },
      hotel,
    } = this.getState();

    const { adults, kids } = getTotalGuests(rooms);
    window.rt = {
      ...window.rt,
      ...properties,
      checkInDate: range.start && range.start.format(constants.dateFormat.query),
      checkOutDate: range.end && range.end.format(constants.dateFormat.query),
      getCheckin() { return this.checkInDate; },
      getCheckout() { return this.checkOutDate; },
      getAdults() { return adults; },
      getKids() { return kids; },
      getRooms() { return rooms.length; },
      getTopFiveHotels() { return hotel.ids.slice(0, 5); },
    };
  },

  searchData() {
    const { search } = this.getState();
    const getEntity = (location = {}) => {
      if (location.type) {
        return location.type;
      } else if (location.hotel_id) {
        return 'hotel';
      } else if (location.area && location.area.landmark) {
        return 'landmark';
      } else if (location.area && location.area.locality) {
        return 'locality';
      } else if (location.area && location.area.city) {
        return 'city';
      } else if (location.area && location.area.state) {
        return 'state';
      }
      return 'google_api';
    };

    const properties = {
      q: get(search, 'searchInput.location.label'),
      entity: getEntity(get(search, 'searchInput.location')),
      hotel_id: get(search, 'searchInput.location.hotel_id'),
      city: get(search, 'searchInput.location.area.city'),
      state: get(search, 'searchInput.location.area.search'),
      checkin: get(search, 'datePicker.range.start', '').toString(),
      checkout: get(search, 'datePicker.range.end', '').toString(),
      roomconfig: get(search, 'occupancy.rooms', [])
        .map((room) => `${room.adults}-${room.kids || 0}`).join(','),
      roomtype: get(search, 'occupancy.roomType'),
      abwDays: get(search, 'datePicker.range.start').startOf('day')
        .diff(moment().startOf('day'), 'days'),
      lengthOfStay: get(search, 'datePicker.range.end').startOf('day')
        .diff(get(search, 'datePicker.range.start').startOf('day'), 'days'),
    };
    return properties;
  },

  searchTreebo() {
    const analyticsProperties = {
      ...this.searchData(),
      ...abService.actionProperties(['findPageCtaDesktop']),
    };
    this.trackInteractiveEvents(analyticsConstants.SEARCH_TREEBO, analyticsProperties);
    window.clevertap.event.push(analyticsConstants.SEARCH_TREEBO, analyticsProperties);
    this.exposeGlobally();
  },

  memberDiscountApplied(page) {
    const analyticsProperties = {
      page,
    };
    window.analytics.track(analyticsConstants.MEMBER_DISCOUNT_APPLIED, analyticsProperties);
  },

  filterClicked(type, name) {
    const analyticsProperties = {
      type,
      name,
    };
    window.analytics.track(analyticsConstants.FILTER_ITEM_CLICKED, analyticsProperties);
  },

  bannerViewed(page, type) {
    const analyticsProperties = {
      page,
      type,
    };
    window.analytics.track(analyticsConstants.BANNER_VIEWED, analyticsProperties);
  },

  cityClicked(city) {
    const analyticsProperties = {
      city,
    };
    this.trackInteractiveEvents(analyticsConstants.CITY_CLICKED, analyticsProperties);
    this.exposeGlobally();
  },

  rewardsBannerClicked() {
    window.analytics.track(analyticsConstants.REWARDS_BANNER_CLICKED, {});
  },

  bookingHistoryDownloadClicked(section) {
    window.analytics.track(`${section} Clicked`, {});
  },

  quickBookClicked(hotelDetail, analyticsData) {
    const {
      hotel,
      price,
      wallet: {
        byType,
      },
    } = this.getState();
    const searchQuery = this.searchData(this.getState());
    const priceDetail = price.byRoomTypeId[hotel.byId[hotelDetail.id].roomTypeIds[0]];
    const analyticsProperties = {
      hotelId: hotelDetail.id,
      hotelName: hotelDetail.name,
      price: Math.round(Object.values(priceDetail.ratePlans)[0].sellingPrice),
      totalPrice: Math.round(Object.values(priceDetail.ratePlans)[0].totalPrice),
      isUsingTreeboPoints: Object.values(byType)[0].isApplied,
      treeboPoints: Object.values(byType)[0].points,
      abwDays: searchQuery.abwDays,
      checkin: searchQuery.checkin,
      checkout: searchQuery.checkout,
      roomConfig: searchQuery.roomconfig,
      lengthOfStay: searchQuery.lengthOfStay,
      ...hotelDetail.address,
      ...analyticsData,
      ...abService.actionProperties(['quickBookCTADesktop']),
    };
    this.trackInteractiveEvents(analyticsConstants.QUICK_BOOK_CLICKED, analyticsProperties);
    window.clevertap.event.push(analyticsConstants.QUICK_BOOK_CLICKED, analyticsProperties);
  },

  bookNowClicked(hotel) {
    const {
      price,
      wallet: { byType },
    } = this.getState();

    const searchQuery = this.searchData(this.getState());
    const priceDetail = price.byRoomTypeId[hotel.roomTypeIds[0]];
    const analyticsProperties = {
      hotelId: hotel.id,
      hotelName: hotel.name,
      price: Math.round(Object.values(priceDetail.ratePlans)[0].sellingPrice),
      totalPrice: Math.round(Object.values(priceDetail.ratePlans)[0].totalPrice),
      isUsingTreeboPoints: Object.values(byType)[0].isApplied,
      treeboPoints: Object.values(byType)[0].points,
      abwDays: searchQuery.abwDays,
      checkin: searchQuery.checkin,
      checkout: searchQuery.checkout,
      roomConfig: searchQuery.roomconfig,
      lengthOfStay: searchQuery.lengthOfStay,
      ...hotel.address,
      ...abService.actionProperties(['hdCtaDesktop']),
    };
    this.trackInteractiveEvents(analyticsConstants.BOOK_NOW_CLICKED, analyticsProperties);
  },

  ratePlanSelected({
    page,
    newRoomType,
    oldRoomType,
    newRatePlan,
    oldRatePlan,
    priceDifference,
    abwDays,
  }) {
    const analyticsProperties = {
      page,
      newRoomType,
      oldRoomType,
      newRatePlan,
      oldRatePlan,
      abwDays,
      roomTypeChanged: !(newRoomType === oldRoomType),
      ratePlanChanged: !(newRatePlan === oldRatePlan),
      priceDifference,
    };
    this.trackInteractiveEvents(analyticsConstants.RATE_PLAN_SELECTED, analyticsProperties);
  },

  checkoutStep(step, analyticsProperties) {
    this.trackInteractiveEvents(`${step}`, analyticsProperties);
    window.clevertap.event.push(`${step}`, analyticsProperties);
  },

  paymentModeSelected(method, sellingPrice, ratePlan) {
    const {
      itinerary: {
        cart: {
          date: { checkin, checkout },
          hotel: { id, name },
          coupon_code: couponCode,
          price: { ratePlans, selectedRatePlan },
        },
      },
      wallet: { byType },
    } = this.getState();
    const selectedPrice = ratePlans[selectedRatePlan];
    const analyticsProperties = {
      type: method,
      hotelId: id,
      hotelName: name,
      price: Math.round(sellingPrice),
      checkin,
      checkout,
      coupon: couponCode,
      totalPrice: Math.round(get(selectedPrice, 'totalPrice')),
      isUsingTreeboPoints: Object.values(byType)[0].isApplied,
      treeboPoints: Object.values(byType)[0].points,
      // memberDiscountApplied: get(price, 'memberDiscountApplied'),
      ratePlanType: ratePlan,
    };
    this.trackInteractiveEvents(analyticsConstants.PAYMENT_INITIATED, analyticsProperties);
  },

  adwordsCompletedOrder({ revenue, price, ...abProperties }) {
    const {
      wallet: { byType },
    } = this.getState();
    const analyticsProperties = {
      revenue,
      price: get(price, 'sellingPrice'),
      total_price: get(price, 'totalPrice'),
      is_using_treebo_points: Object.values(byType)[0].isApplied,
      treebo_points: Object.values(byType)[0].points,
      ...abService.actionProperties(['coupleFriendlyHotel']),
      ...abProperties,
    };
    window.analytics.track('Adwords Completed Order', analyticsProperties);
  },

  gstinClicked() {
    window.analytics.track(analyticsConstants.GSTIN_CLICKED, {});
  },

  ipLocationTracked(success, city) {
    if (__BROWSER__) {
      window.analytics.track(analyticsConstants.IP_LOCATION, {
        city,
        success,
      });
    }
  },

  browserLocationTracked(allow, lat, long) {
    window.analytics.track(analyticsConstants.BROWSER_LOCATION, {
      allow,
      lat,
      long,
    });
  },

  onOtpContinueClicked(mobile, page) {
    const analyticsProperties = {
      mobile,
      page,
    };
    window.analytics.track(analyticsConstants.OTP_CONTINUE_CLICKED, analyticsProperties);
  },

  verifyOtpStatus(status, page) {
    const analyticsProperties = {
      status,
      page,
    };
    window.analytics.track(analyticsConstants.VERIFY_OTP_STATUS, analyticsProperties);
  },

  verifyOtpClicked(mobile, page) {
    const analyticsProperties = {
      mobile,
      page,
    };
    window.analytics.track(analyticsConstants.VERIFY_OTP_CLICKED, analyticsProperties);
  },

  resendOtpClicked(mobile, page) {
    const analyticsProperties = {
      mobile,
      page,
    };
    window.analytics.track(analyticsConstants.VERIFY_OTP_STATUS, analyticsProperties);
  },

  hotelViewDetailsClicked(data, analyticsData) {
    const {
      price: {
        byRoomTypeId: prices,
      },
    } = this.getState();
    const searchQuery = this.searchData(this.getState());
    const priceDetail = prices[data.hotel.roomTypeIds[0]];
    const analyticsProperties = {
      hotelId: data.hotel.id,
      hotelName: data.hotel.name,
      checkin: searchQuery.checkin,
      checkout: searchQuery.checkout,
      roomConfig: searchQuery.roomconfig,
      abwDays: searchQuery.abwDays,
      lengthOfStay: searchQuery.lengthOfStay,
      price: Math.round(Object.values(priceDetail.ratePlans)[0].sellingPrice),
      // memberDiscountApplied: priceDetail.memberDiscount.isApplied,
      ...analyticsData,
    };
    this.trackInteractiveEvents(analyticsConstants.HOTEL_CLICKED, analyticsProperties);
    window.clevertap.event.push(analyticsConstants.HOTEL_CLICKED, analyticsProperties);
  },

  loginTypeSelected(type) {
    const analyticsProperties = {
      type,
    };
    window.analytics.track(analyticsConstants.LOGIN_TYPE_SELECTED, analyticsProperties);
  },

  couponApplied(coupon, success) {
    const analyticsProperties = {
      coupon,
      success,
      ...abService.actionProperties(['checkoutCouponDesktop']),
    };
    window.analytics.track(analyticsConstants.COUPON_APPLIED, analyticsProperties);
  },

  editMobileClicked(mobile, page) {
    const analyticsProperties = {
      mobile,
      page,
    };
    window.analytics.track(analyticsConstants.EDIT_MOBILE_CLICKED, analyticsProperties);
  },

  applyCouponLinkClicked() {
    window.analytics.track(analyticsConstants.ADD_COUPON_LINK_CLICKED, {
      ...abService.actionProperties(['checkoutCouponDesktop']),
    });
  },

  emptyRewardsCtaClicked() {
    window.analytics.track(analyticsConstants.REWARDS_EMPTY_CTA_CLICKED, {});
  },

  useTreeboPoints(hasApplied) {
    const analyticsProperties = {
      hasApplied,
    };
    window.analytics.track(analyticsConstants.WALLET_ACTION_CLICKED, analyticsProperties);
  },

  isLoggedIn() { authService.isLoggedIn(); },

  rewardsLandingActionClicked() {
    const analyticsProperties = {
      isLoggedIn: this.isLoggedIn(),
    };
    window.analytics.track(analyticsConstants.REWARDS_LANDING_ACTION_CLICKED, analyticsProperties);
  },

  treeboRewardsClicked() {
    window.analytics.track(analyticsConstants.TREEBO_REWARDS_CLICKED, {});
  },

  showMoreClicked(type, page) {
    const analyticsProperties = {
      type,
      page,
    };
    window.analytics.track(analyticsConstants.SHOW_MORE_CLICKED, analyticsProperties);
  },

  cancelBookingClicked(type, bookingDetail) {
    const analyticsProperties = {
      type,
      bookingId: bookingDetail.booking_id,
      hotelName: bookingDetail.hotel_name,
      checkin: bookingDetail.checkin_date,
      payAtHotel: bookingDetail.pay_at_hotel,
    };
    window.analytics.track(analyticsConstants.CANCEL_BOOKING_CLICKED, analyticsProperties);
  },

  seoFooterItemClicked(type, name) {
    const analyticsProperties = {
      type,
      name,
    };
    window.analytics.track(analyticsConstants.SEO_FOOTER_ITEM_CLICKED, analyticsProperties);
  },

  seoFooterHeaderClicked(name) {
    const analyticsProperties = {
      name,
    };
    window.analytics.track(analyticsConstants.SEO_FOOTER_HEADER_CLICKED, analyticsProperties);
  },

  taReviewImageClicked(page, hotelName) {
    const analyticsProperties = {
      page,
      hotelName,
    };
    window.analytics.track(analyticsConstants.TA_REVIEW_IMAGE_CLICKED, analyticsProperties);
  },

  readAllReviewClicked(hotelName) {
    const analyticsProperties = {
      hotelName,
    };
    window.analytics.track(analyticsConstants.READ_ALL_REVIEW_CLICKED, analyticsProperties);
  },

  writeReviewClicked(hotelName) {
    const analyticsProperties = {
      hotelName,
    };
    window.analytics.track(analyticsConstants.WRITE_REVIEW_CLICKED, analyticsProperties);
  },

  similarTreeboClicked(clickedHotel) {
    const {
      hotel: { byId },
    } = this.getState();
    const searchQuery = this.searchData(this.getState());
    const hotelDetail = byId[searchQuery.hotel_id];
    const analyticsProperties = {
      baseHotelName: hotelDetail.name,
      baseHotelId: hotelDetail.id,
      otherHotelName: clickedHotel.name,
      otherHotelId: clickedHotel.id,
    };
    window.analytics.track(analyticsConstants.SIMILAR_TREEBO_CLICKED, analyticsProperties);
  },

  viewOtherPlansClicked(name, hotelId, price = {}) {
    const {
      hotel: { byId },
      wallet: { byType },
    } = this.getState();
    const searchQuery = this.searchData(this.getState());
    const hotelDetail = byId[searchQuery.hotel_id];
    const analyticsProperties = {
      hotelId: hotelDetail.id,
      hotelName: hotelDetail.name,
      price: get(price, 'sellingPrice'),
      totalPrice: get(price, 'totalPrice'),
      isUsingTreeboPoints: Object.values(byType)[0].isApplied,
      treeboPoints: Object.values(byType)[0].points,
      checkin: searchQuery.checkin,
      checkout: searchQuery.checkout,
      roomConfig: searchQuery.roomconfig,
    };
    window.analytics.track(analyticsConstants.VIEW_OTHER_PLANS_CLICKED, analyticsProperties);
  },

  viewBiggerRoomsClicked(price) {
    const {
      hotel: { byId },
      wallet: { byType },
    } = this.getState();
    const searchQuery = this.searchData(this.getState());
    const hotelDetail = byId[searchQuery.hotel_id];
    const analyticsProperties = {
      hotelId: hotelDetail.id,
      hotelName: hotelDetail.name,
      price: get(price, 'sellingPrice'),
      totalPrice: get(price, 'totalPrice'),
      isUsingTreeboPoints: Object.values(byType)[0].isApplied,
      treeboPoints: Object.values(byType)[0].points,
      checkin: searchQuery.checkin,
      checkout: searchQuery.checkout,
      roomConfig: searchQuery.roomconfig,
    };
    window.analytics.track(analyticsConstants.VIEW_BIGGER_ROOMS_CLICKED, analyticsProperties);
  },

  loginSuccess(type) {
    const eventData = {
      type,
    };
    this.trackNonInteractiveEvents(analyticsConstants.LOGIN_SUCCESS, eventData);
  },

  loginDetails() {
    const { user: { profile } } = this.getState();
    const analyticsLoginProperties = {
      firstName: profile.firstName,
      lastName: profile.lastName,
      phone: `+91${profile.mobile}`,
      email: profile.email,
      userId: profile.userId,
    };
    const analyticsGuestLoginProperties = {
      firstName: profile.firstName,
      lastName: profile.lastName,
      phone: `+91${profile.mobile}`,
      email: profile.email,
    };
    window.analytics.track(analyticsConstants.LOGIN_DETAILS, analyticsLoginProperties);
    window.analytics.identify(profile.userId, analyticsGuestLoginProperties);
    window.clevertap.profile.push({
      Site: {
        Name: profile.name,
        Phone: `+91${profile.mobile}`,
        Email: profile.email,
      },
    });
  },

  logoutClicked() {
    window.clevertap.profile.pop();
  },

  primaryTravelerDetail(user) {
    const analyticsProperties = {
      firstName: user.name,
      phone: `+91${user.mobile}`,
      email: user.email,
    };
    window.analytics.identify('', analyticsProperties);
  },

  exposeSearchParamenters(search = '') {
    const query = qs.parse(search);
    const expose = {};
    const allCookies = cookies.get();
    const queryExposePath = {
      utm_name: 'analytics.utm_params.name',
      utm_source: 'analytics.utm_params.source',
      utm_medium: 'analytics.utm_params.medium',
      utm_campaign: 'analytics.utm_params.campaign',
      utm_keyword: 'analytics.utm_params.keyword',
      avclk: 'analytics.referral_params.avclk',
      refid: 'analytics.refid',
    };

    Object.keys(queryExposePath).forEach((parameter) => {
      if (query[parameter]) {
        cookies.set(parameter, query[parameter], { expires: 30 });
        set(expose, queryExposePath[parameter], query[parameter]);
      } else {
        set(expose, queryExposePath[parameter], allCookies[parameter]);
      }
    });
    this.exposeGlobally(expose);
  },

  feedbackDetail(analyticsStmt, analyticsProperties) {
    window.analytics.track(analyticsStmt, analyticsProperties);
  },

  paymentSectionViewed() {
    window.analytics.track(analyticsConstants.PAYMENT_SECTION_VIEWED, abService.impressionProperties(['pahCtaDesktop']));
  },

  perfectStayKnowMoreClicked(page) {
    this.trackInteractiveEvents(analyticsConstants.PERFECT_STAY_KNOW_MORE_CLICKED, {
      page,
    });
  },

  // View Events
  itineraryContentViewed(properties) {
    this.trackNonInteractiveEvents(analyticsConstants.ITINERARY, properties);
  },

  hotelContentViewed(properties) {
    this.trackNonInteractiveEvents(analyticsConstants.HOTEL_DETAIL, properties);
  },

  homeContentViewed(properties) {
    this.trackNonInteractiveEvents(analyticsConstants.HOME, properties);
  },

  listContentViewed(properties) {
    this.trackNonInteractiveEvents(analyticsConstants.HOTEL_LIST, properties);
  },

  rewardsContentViewed(properties) {
    this.trackNonInteractiveEvents(analyticsConstants.REWARDS, properties);
  },

  rewardsLandingContentViewed(properties) {
    this.trackNonInteractiveEvents(analyticsConstants.REWARDS_LANDING, properties);
  },

  bookingHistoryContentViewed(properties) {
    this.trackNonInteractiveEvents(analyticsConstants.BOOKING_HISTORY_DETAILS, properties);
  },
};
