import config from '../../../config';

export default {
  initHandlers() {
    if (__BROWSER__) {
      const GoogleMapsLoader = require('google-maps'); // eslint-disable-line
      GoogleMapsLoader.KEY = config.googleMapsApiKey;
      GoogleMapsLoader.REGION = ['IN'];
      GoogleMapsLoader.LIBRARIES = ['places'];
      GoogleMapsLoader.load((google) => {
        this.placesHandler = new google.maps.places.PlacesService(
          document.getElementById('wrapper').appendChild(document.createElement('div')),
        );
      });
    }
  },

  constructLocation(result) {
    const googleAddressPieceMap = {
      city: 'locality',
      state: 'administrative_area_level_1',
      country: 'country',
    };

    const area = result.address_components.reduce((areaObj, addressComponent) => {
      Object.keys(googleAddressPieceMap)
        .forEach((addressPiece) => {
          areaObj[addressPiece] = // eslint-disable-line
            addressComponent.types.includes(googleAddressPieceMap[addressPiece])
              ? addressComponent.long_name : areaObj[addressPiece];
        });
      return areaObj;
    }, {});

    const coordinates = {
      lat: result.geometry.location.lat(),
      lng: result.geometry.location.lng(),
    };

    return { area, coordinates };
  },
};
