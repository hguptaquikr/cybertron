import isEmpty from 'lodash/isEmpty';
import kebabCase from 'lodash/kebabCase';
import moment from 'moment';
import { constants } from '../utils';

export default {
  transformNewYearRateTagApi({ data }) {
    const response = { data: {} };
    Object.keys(data.special_rate_hotels).forEach((key) => {
      response.data[key] =
        data.special_rate_hotels[key].special_rate_dates.map((value) => ({
          text: value.text,
          startDate: value.start_date,
          endDate: value.end_date,
        }));
    });
    return response;
  },

  transformSeoContentsApi({ data }) {
    return {
      data: {
        content: {
          // common
          links: data.content.links || [],
          // searchResultsPage
          breadcrumbs: data.content.breadcrumbs,
          description: data.content.description,
          // hotelDetailsPage
          aboutHotel: data.content.about_hotel || {},
          policies: data.content.policies || {},
          facilities: data.content.facilities || {},
          guestFeedbacks: data.content.positive_feedback || {},
          nearbyPlaces: data.content.nearby || {},
          leftNav: data.content.left_nav || {},
          qna: !isEmpty(data.content.posts)
            ? {
              title: data.content.posts.title,
              content: data.content.posts.content.map((QnA) => ({
                question: {
                  text: QnA.question,
                  author: QnA.user,
                  postedOn: QnA.posted_date,
                },
                answers: QnA.answers.map((answer) => ({
                  text: answer.text,
                  author: answer.user,
                  postedOn: answer.posted_date,
                  isVerified: answer.verified,
                })),
              })),
            } : {},
          cityImageUrl: data.content.city_image || '',
          minPrice: data.content.min_price || '',
          weather: !isEmpty(data.content.weather_widget) ? {
            title: data.content.weather_widget.content.header,
            content: data.content.weather_widget.content.map((daysWeather) => ({
              avgTemp: daysWeather.avg_temp,
              date: daysWeather.date,
              icon: daysWeather.icon,
              summary: daysWeather.summary,
            })),
          } : {},
          coordinates: data.content.place_coordinate,
          blogs: !isEmpty(data.content.blogs) ? {
            title: data.content.blogs.title,
            blogsList: data.content.blogs.content.map((blog) => ({
              name: blog.title,
              blogUrl: blog.blog_url,
              imageUrl: blog.image_url,
              priority: blog.priority,
            })),
          } : {},
          reviews: !isEmpty(data.content.top_reviews) ? {
            title: data.content.top_reviews.title,
            reviewsList: data.content.top_reviews.content.map((review) => ({
              title: review.review_title,
              rating: review.review_ratings,
              hotel: {
                name: review.hotel.name,
                id: review.hotel.id,
              },
              isCrawlable: review.is_crawlable,
              publishDate: moment(review.review_date)
                .format(constants.dateFormat.history).substring(4),
              ratingImage: review.rating_image,
              text: review.review_text,
              user: review.user,
            })).sort((prevReview, nextReview) => prevReview.rating < nextReview.rating),
          } : null,
        },
        link: data.link,
        meta: data.meta,
        schema: data.schema,
        page: data.page,
      },
    };
  },

  transformTripAdvisorApi({ data: { ta_reviews: reviews } }) {
    return {
      data: {
        isTripAdvisorEnabled: reviews.ta_enabled,
        awards: reviews.awards,
        hotelId: reviews.hotel_id,
        noOfReviews: reviews.num_reviews,
        overallRanking: reviews.overall_ranking,
        overallRating: reviews.overall_rating,
        readReviewsUrl: reviews.read_reviews_url,
        subRatings: reviews.subratings,
        topReviews: reviews.top_reviews,
        writeReviewUrl: reviews.write_review_url,
      },
    };
  },

  getSeoPathname(place) {
    const city = place.area.city_slug;
    if (place.type === 'landmark') {
      const landmark = place.area.landmark_slug || kebabCase(`${place.area.landmark}-${city}`);
      return `/hotels-near-${landmark}/`;
    } else if (place.type === 'locality') {
      const locality = kebabCase(place.area.locality);
      return `/hotels-in-${locality}-${city}/`;
    } else if (place.type === 'city') {
      return `/hotels-in-${city}/`;
    } else if (place.type === 'category') {
      const category = kebabCase(place.category);
      return `/${category}-in-${city}/`;
    } else if (place.type === 'amenity') {
      const amenity = kebabCase(place.amenity);
      return `/hotels-in-${city}-with-${amenity}/`;
    } else if (place.type === 'hotel_type') {
      const category = kebabCase(place.category);
      const isLocality = place.area.locality;
      const slug = isLocality ? kebabCase(place.area.locality) : place.area.landmark_slug;
      return isLocality ? `/${category}-in-${slug}-${city}/` : `/${category}-near-${slug}/`;
    } else if (place.type === 'hotel') {
      const hotel = kebabCase(place.area.hotel);
      const locality = kebabCase(place.area.locality);
      const hotelId = place.hotel_id;
      return `/hotels-in-${city}/${hotel}-${locality}-${hotelId}/`;
    }
    return '';
  },

  makeSeoQuery(route, params) {
    const seoQuery = {
      page: 'common',
      is_amp: false,
    };

    if (route.path === '/cities/') {
      seoQuery.q = 'cities';
    } else if (route.path === '/:category-near-:landmark/') {
      seoQuery.page = 'landmark-category';
      seoQuery.q = `${params.category}-near-${params.landmark}`;
      seoQuery.is_amp = true;
    } else if (route.path === '/:category-in-:locality-:city(\\w+)/') {
      seoQuery.page = 'locality-category';
      seoQuery.q = `${params.category}-in-${params.locality}-${params.city}`;
      seoQuery.is_amp = true;
    } else if (route.path === '/hotels-in-:city-with-:amenity/') {
      seoQuery.page = 'city-amenity';
      seoQuery.q = `${kebabCase(params.amenity)}-${params.city}`;
      seoQuery.is_amp = true;
    } else if (route.path === '/hotels-in-:city/') {
      seoQuery.page = 'city';
      seoQuery.q = params.city;
      seoQuery.is_amp = true;
    } else if (route.path === '/hotels-in-:locality-:city(\\w+)/') {
      seoQuery.page = 'locality';
      seoQuery.q = `${params.locality}-${params.city}`;
      seoQuery.is_amp = true;
    } else if (route.path === '/:category-in-:city/') {
      seoQuery.page = 'category';
      seoQuery.q = `${params.category}-${params.city}`;
      seoQuery.is_amp = true;
    } else if (route.path === '/hotels-near-:landmark/') {
      seoQuery.page = params.landmark === 'me' ? 'near' : 'landmark';
      seoQuery.q = params.landmark;
      seoQuery.is_amp = true;
    } else if (route.path === '/hotels-in-:city/:hotelSlug-:hotelId(\\w+)/') {
      seoQuery.page = 'hd';
      seoQuery.q = params.hotelId;
      seoQuery.is_amp = true;
    } else if (route.path === '/faq/') {
      seoQuery.q = 'faq';
    } else if (route.path === '/introducing-rewards/') {
      seoQuery.q = 'rewards';
    }

    return seoQuery;
  },
};
