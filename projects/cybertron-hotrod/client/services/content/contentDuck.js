import union from 'lodash/union';
import contentService from './contentService';

const GET_SEO_CONTENT_REQUEST = 'GET_SEO_CONTENT_REQUEST';
const GET_SEO_CONTENT_SUCCESS = 'GET_SEO_CONTENT_SUCCESS';
const GET_SEO_CONTENT_FAILURE = 'GET_SEO_CONTENT_FAILURE';
const GET_PERFECT_STAY_CONTENT_REQUEST = 'GET_PERFECT_STAY_CONTENT_REQUEST';
const GET_PERFECT_STAY_CONTENT_SUCCESS = 'GET_PERFECT_STAY_CONTENT_SUCCESS';
const GET_PERFECT_STAY_CONTENT_FAILURE = 'GET_PERFECT_STAY_CONTENT_FAILURE';
const GET_POLICY_CONTRIBUTORS_CONTENT_REQUEST = 'GET_POLICY_CONTRIBUTORS_CONTENT_REQUEST';
const GET_POLICY_CONTRIBUTORS_CONTENT_SUCCESS = 'GET_POLICY_CONTRIBUTORS_CONTENT_SUCCESS';
const GET_POLICY_CONTRIBUTORS_CONTENT_FAILURE = 'GET_POLICY_CONTRIBUTORS_CONTENT_FAILURE';
const GET_WALLETS_PROMOTION_CONTENT_REQUEST = 'GET_WALLETS_PROMOTION_CONTENT_REQUEST';
const GET_WALLETS_PROMOTION_CONTENT_SUCCESS = 'GET_WALLETS_PROMOTION_CONTENT_SUCCESS';
const GET_WALLETS_PROMOTION_CONTENT_FAILURE = 'GET_WALLETS_PROMOTION_CONTENT_FAILURE';
const GET_REVIEWS_REQUEST = 'GET_REVIEWS_REQUEST';
const GET_REVIEWS_SUCCESS = 'GET_REVIEWS_SUCCESS';
const GET_REVIEWS_FAILURE = 'GET_REVIEWS_FAILURE';
const GET_NON_JCR_REQUEST = 'GET_NON_JCR_REQUEST';
const GET_NON_JCR_SUCCESS = 'GET_NON_JCR_SUCCESS';
const GET_NON_JCR_FAILURE = 'GET_NON_JCR_FAILURE';
const GET_FAQ_CONTENT_REQUEST = 'GET_FAQ_CONTENT_REQUEST';
const GET_FAQ_CONTENT_SUCCESS = 'GET_FAQ_CONTENT_SUCCESS';
const GET_FAQ_CONTENT_FAILURE = 'GET_FAQ_CONTENT_FAILURE';
const SUBMIT_QUESTION_REQUEST = 'SUBMIT_QUESTION_REQUEST';
const SUBMIT_QUESTION_SUCCESS = 'SUBMIT_QUESTION_SUCCESS';
const SUBMIT_QUESTION_FAILURE = 'SUBMIT_QUESTION_FAILURE';
const GET_LANDING_CONTENT_REQUEST = 'GET_LANDING_CONTENT_REQUEST';
const GET_LANDING_CONTENT_SUCCESS = 'GET_LANDING_CONTENT_SUCCESS';
const GET_LANDING_CONTENT_FAILURE = 'GET_LANDING_CONTENT_FAILURE';
const GET_NEW_YEAR_RATE_TAG_REQUEST = 'GET_NEW_YEAR_RATE_TAG_REQUEST';
const GET_NEW_YEAR_RATE_TAG_SUCCESS = 'GET_NEW_YEAR_RATE_TAG_SUCCESS';
const GET_NEW_YEAR_RATE_TAG_FAILURE = 'GET_NEW_YEAR_RATE_TAG_FAILURE';
const GET_FEEDBACK_TAGS_REQUEST = 'GET_FEEDBACK_TAGS_REQUEST';
const GET_FEEDBACK_TAGS_SUCCESS = 'GET_FEEDBACK_TAGS_SUCCESS';
const GET_FEEDBACK_TAGS_FAILURE = 'GET_FEEDBACK_TAGS_FAILURE';
const UPDATE_FEEDBACK_TAGS = 'UPDATE_FEEDBACK_TAGS';
const SUBMIT_FEEDBACK_REQUEST = 'SUBMIT_FEEDBACK_REQUEST';
const SUBMIT_FEEDBACK_SUCCESS = 'SUBMIT_FEEDBACK_SUCCESS';
const SUBMIT_FEEDBACK_FAILURE = 'SUBMIT_FEEDBACK_FAILURE';

const initialState = {
  seo: {
    content: {
      links: [],
      cityImageUrl: '',
      minPrice: '',
      leftNav: {},
      guestFeedbacks: {},
      aboutHotel: {},
      facilities: {},
      policies: {},
      nearbyPlaces: {},
      qna: {},

    },
    link: [],
    meta: [],
    schema: [],
    page: [],
  },
  reviews: {
    isTripAdvisorEnabled: false,
    hotelId: '',
    noOfReviews: '',
    overallRating: {},
    awards: [],
    writeReviewUrl: '',
    readReviewsUrl: '',
    overallRanking: {},
    subRatings: [],
    topReviews: [],
  },
  faq: [],
  banners: [{
    image_url: '',
    title: 'INDIA\'S TOP RATED HOTEL CHAIN',
    description:
      'All essentials covered. Air Conditioner, WiFi, Breakfast, Toiletries, Bed & Bath Linen',
  }],
  brand_video: {},
  cities: [],
  promises: [],
  awards: [],
  offers: [],
  content_block: {
    blocks: [],
    title: '',
    subtitles: [],
  },
  testimonials: [],
  initiatives: [],
  joinus_banner: {},
  press_release: [],
  newYearRateTag: {},
  feedback: {
    state: '',
    user: {
      name: '',
    },
    booking: {
      hotel_name: '',
      hotel_address: '',
      from_date: '',
      to_date: '',
      guest: '',
    },
    current: {
      sections: {
        tag_section: {
          values: [],
        },
      },
    },
  },
  perfectStay: {
    illustration: 'https://images.treebohotels.com/images/perfect-stay.svg',
  },
  policyContributors: [],
  walletsPromotion: {
    phonePe: '',
  },
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_SEO_CONTENT_SUCCESS:
      return {
        ...state,
        seo: payload.response,
      };

    case GET_REVIEWS_SUCCESS:
      return {
        ...state,
        reviews: payload.response,
      };

    case GET_NON_JCR_SUCCESS:
      return {
        ...state,
        reviews: {
          ...state.reviews,
          topReviews: union(
            [],
            state.reviews.topReviews,
            payload.response.ta_non_jcr_reviews.top_reviews,
          ),
        },
      };

    case GET_FAQ_CONTENT_SUCCESS:
      return {
        ...state,
        faq: payload.response.value,
      };

    case GET_LANDING_CONTENT_SUCCESS:
      return {
        ...state,
        ...payload.response,
      };

    case GET_NEW_YEAR_RATE_TAG_SUCCESS:
      return {
        ...state,
        newYearRateTag: payload.response,
      };

    case GET_PERFECT_STAY_CONTENT_SUCCESS:
      return {
        ...state,
        perfectStay: payload.response.perfect_stay,
      };

    case GET_POLICY_CONTRIBUTORS_CONTENT_SUCCESS:
      return {
        ...state,
        policyContributors: payload.response.value,
      };

    case GET_WALLETS_PROMOTION_CONTENT_SUCCESS:
      return {
        ...state,
        walletsPromotion: payload.response.wallets_promotion,
      };

    case GET_FEEDBACK_TAGS_SUCCESS:
      return {
        ...state,
        feedback: payload.response,
      };

    case UPDATE_FEEDBACK_TAGS:
      return {
        ...state,
        feedback: payload.feedback,
      };

    default:
      return state;
  }
};

export const getSeoContent = (route, params) => (dispatch, getState, { api }) => {
  const seoQuery = contentService.makeSeoQuery(route, params);

  return dispatch({
    types: [
      GET_SEO_CONTENT_REQUEST,
      GET_SEO_CONTENT_SUCCESS,
      GET_SEO_CONTENT_FAILURE,
    ],
    promise: () => api.get('/api/web/v3/contents/meta/', seoQuery)
      .then(contentService.transformSeoContentsApi),
  });
};

export const getFaqContent = () => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_FAQ_CONTENT_REQUEST,
      GET_FAQ_CONTENT_SUCCESS,
      GET_FAQ_CONTENT_FAILURE,
    ],
    promise: () => api.get('/api/web/v1/contents/', { key: 'faq_content' }),
  });

export const getHotelReviews = (hotelId) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_REVIEWS_REQUEST,
      GET_REVIEWS_SUCCESS,
      GET_REVIEWS_FAILURE,
    ],
    promise: () => api.get('/api/web/v1/reviews/tripadvisor/hotel_review_data/', { hotel_id: hotelId })
      .then(contentService.transformTripAdvisorApi),
  });

export const getNonJCR = (hotelId) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_NON_JCR_REQUEST,
      GET_NON_JCR_SUCCESS,
      GET_NON_JCR_FAILURE,
    ],
    promise: () => api.get('/api/web/v1/reviews/tripadvisor/hotel_non_jcr_review_data/', { hotel_id: hotelId }),
  });

export const submitQuestion = (question, page) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      SUBMIT_QUESTION_REQUEST,
      SUBMIT_QUESTION_SUCCESS,
      SUBMIT_QUESTION_FAILURE,
    ],
    promise: () => api.post('/api/web/v1/posts/add/question/', { question, page }),
  });

export const getLandingContent = () => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_LANDING_CONTENT_REQUEST,
      GET_LANDING_CONTENT_SUCCESS,
      GET_LANDING_CONTENT_FAILURE,
    ],
    promise: () => api.get('/api/web/v1/contents/landing-page/', {
      keys: 'promises,brand_video,press_release,joinus_banner,initiatives,offers,awards,testimonials,content_block,banners,cities',
    }),
  });

export const getNewYearRateTag = () => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_NEW_YEAR_RATE_TAG_REQUEST,
      GET_NEW_YEAR_RATE_TAG_SUCCESS,
      GET_NEW_YEAR_RATE_TAG_FAILURE,
    ],
    promise: () => api.get('/api/web/v2/contents/', { keys: 'special_rate_hotels' })
      .then(contentService.transformNewYearRateTagApi),
  });

export const getFeedbackTags = (feedbackId) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_FEEDBACK_TAGS_REQUEST,
      GET_FEEDBACK_TAGS_SUCCESS,
      GET_FEEDBACK_TAGS_FAILURE,
    ],
    promise: () => api.get(`/api/web/feedback/v1/feedbacks/${feedbackId}/`),
  });

export const updateFeedbackTags = (tagName, tagValue) => (dispatch, getState) => {
  const { content: { feedback } } = getState();
  const updatedFeedback = {
    ...feedback,
    current: {
      ...feedback.current,
      sections: {
        ...feedback.current.sections,
        tag_section: {
          ...feedback.current.sections.tag_section,
          values: feedback.current.sections.tag_section.values.map((value) => ({
            ...value,
            body: {
              name: value.name,
              value: value.name === tagName ? tagValue : value.body.value,
            },
          })),
        },
      },
    },
  };
  return dispatch({
    type: UPDATE_FEEDBACK_TAGS,
    payload: { feedback: updatedFeedback },
  });
};

export const submitFeedback = (query) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      SUBMIT_FEEDBACK_REQUEST,
      SUBMIT_FEEDBACK_SUCCESS,
      SUBMIT_FEEDBACK_FAILURE,
    ],
    payload: query,
    promise: () => api.put('/api/web/feedback/v1/feedbacks/', query),
  });

export const getPerfectStayContent = () => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_PERFECT_STAY_CONTENT_REQUEST,
      GET_PERFECT_STAY_CONTENT_SUCCESS,
      GET_PERFECT_STAY_CONTENT_FAILURE,
    ],
    promise: () => api.get('/api/web/v2/contents/', { keys: 'perfect_stay' }),
  });

export const getPolicyContributorsContent = () => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_POLICY_CONTRIBUTORS_CONTENT_REQUEST,
      GET_POLICY_CONTRIBUTORS_CONTENT_SUCCESS,
      GET_POLICY_CONTRIBUTORS_CONTENT_FAILURE,
    ],
    promise: () => api.get('/api/web/v1/contents/', { key: 'policy_contributors' }),
  });

export const getWalletsPromotion = () => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_WALLETS_PROMOTION_CONTENT_REQUEST,
      GET_WALLETS_PROMOTION_CONTENT_SUCCESS,
      GET_WALLETS_PROMOTION_CONTENT_FAILURE,
    ],
    promise: () => api.get('/api/web/v2/contents/', { keys: 'wallets_promotion' }),
  });
