const GET_CART_CONTENT_SUCCESS = 'GET_CART_CONTENT_SUCCESS';
const OTP_ISVERIFIED_REQUEST = 'OTP_ISVERIFIED_REQUEST';
const OTP_ISVERIFIED_SUCCESS = 'OTP_ISVERIFIED_SUCCESS';
const OTP_ISVERIFIED_FAILURE = 'OTP_ISVERIFIED_FAILURE';
const OTP_VERIFY_REQUEST = 'OTP_VERIFY_REQUEST';
const OTP_VERIFY_SUCCESS = 'OTP_VERIFY_SUCCESS';
const OTP_VERIFY_FAILURE = 'OTP_VERIFY_FAILURE';
const SEND_OTP_REQUEST = 'SEND_OTP_REQUEST';
const SEND_OTP_SUCCESS = 'SEND_OTP_SUCCESS';
const SEND_OTP_FAILURE = 'SEND_OTP_FAILURE';

const initialState = {
  isOtpEnabled: true,
  isVerifyingOtp: false,
  isOtpVerified: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_CART_CONTENT_SUCCESS:
      return {
        ...state,
        isOtpEnabled: action.payload.response.isOtpEnabled,
      };
    case OTP_ISVERIFIED_SUCCESS:
      return {
        ...state,
        isOtpVerified: action.payload.response.is_verified,
      };
    case OTP_ISVERIFIED_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      };
    case OTP_VERIFY_SUCCESS:
    case OTP_VERIFY_FAILURE:
      return {
        ...state,
        isVerifyingOtp: false,
      };
    case OTP_VERIFY_REQUEST:
      return {
        ...state,
        isVerifyingOtp: true,
      };
    default:
      return state;
  }
};

export const checkOtpVerified = (mobile) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      OTP_ISVERIFIED_REQUEST,
      OTP_ISVERIFIED_SUCCESS,
      OTP_ISVERIFIED_FAILURE,
    ],
    promise: () => api.post('/api/web/v2/otp/is-verified/', { phone_number: mobile }),
  });

export const onVerifyOTP = (mobile, otp) => (dispatch, getState, { api }) => {
  const verificationData = {
    phone_number: mobile,
    verification_code: otp,
  };
  return dispatch({
    types: [
      OTP_VERIFY_REQUEST,
      OTP_VERIFY_SUCCESS,
      OTP_VERIFY_FAILURE,
    ],
    payload: { mobile },
    promise: () => api.post('/api/web/v2/otp/verify/', verificationData),
  });
};

export const sendOtp = (mobile) => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      SEND_OTP_REQUEST,
      SEND_OTP_SUCCESS,
      SEND_OTP_FAILURE,
    ],
    promise: () => api.post('/api/web/v2/otp/', { phone_number: mobile }),
  });
