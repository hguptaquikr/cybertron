export default {
  transformBalanceApi({ data }) {
    const transformedWalletObject = data.wallets.reduce((obj, walletType) => ({
      ...obj,
      [walletType.type]: {
        type: walletType.type,
        points: walletType.total_points,
        totalBalance: walletType.total_balance,
        usableBalance: walletType.spendable_amount,
        isApplied: true,
      },
    }), {});

    const walletTypes = data.wallets.map((wallet) => wallet.type);

    return {
      data: {
        wallet: transformedWalletObject,
        types: walletTypes,
      },
    };
  },

  transformStatementApi({ data }) {
    const transformObject = (statement) => ({
      particular: statement.particular,
      amount: statement.amount,
      type: statement.type,
      isPerishable: statement.is_perishable,
      expiresOn: statement.expires_on,
      createdAt: statement.created_on,
      status: statement.status.toLowerCase(),
      id: statement.custom_field_one,
    });

    const transformedStatements = data.wallet_statement.map((statement) => ({
      ...transformObject(statement),
      breakup: statement.related_credits.map((subStatement) => ({
        ...transformObject(subStatement),
      })),
    }));

    return {
      data: {
        statements: transformedStatements,
      },
    };
  },
};
