import recycleState from 'redux-recycle';
import walletService from '../wallet/walletService';
import priceService from '../price/priceService';
import config from '../../../config';

const GET_WALLET_STATEMENT_REQUEST = 'GET_WALLET_STATEMENT_REQUEST';
const GET_WALLET_STATEMENT_SUCCESS = 'GET_WALLET_STATEMENT_SUCCESS';
const GET_WALLET_STATEMENT_FAILURE = 'GET_WALLET_STATEMENT_FAILURE';
const GET_WALLET_BALANCE_REQUEST = 'GET_WALLET_BALANCE_REQUEST';
const GET_WALLET_BALANCE_SUCCESS = 'GET_WALLET_BALANCE_SUCCESS';
const GET_WALLET_BALANCE_FAILURE = 'GET_WALLET_BALANCE_FAILURE';
const GET_CART_CONTENT_SUCCESS = 'GET_CART_CONTENT_SUCCESS';
const APPLY_WALLET_REQUEST = 'APPLY_WALLET_REQUEST';
const APPLY_WALLET_SUCCESS = 'APPLY_WALLET_SUCCESS';
const APPLY_WALLET_FAILURE = 'APPLY_WALLET_FAILURE';
const UNSET_AUTHENTICATED_USER = 'UNSET_AUTHENTICATED_USER';
const GET_CONFIRMATION_CONTENT_SUCCESS = 'GET_CONFIRMATION_CONTENT_SUCCESS';
const APPLY_REMOVE_COUPON_SUCCESS = 'APPLY_REMOVE_COUPON_SUCCESS';
const HIDE_PAYMENT_OPTION = 'HIDE_PAYMENT_OPTION';

const initialState = {
  byType: {
    TP: {
      type: 'TP',
      points: 0,
      totalBalance: 0,
      usableBalance: 0,
      isApplied: false,
    },
  },
  types: ['TP'],
  statements: [],
};

export default recycleState(
  (state = initialState, action) => {
    const { payload, type } = action;
    switch (type) {
      case GET_WALLET_BALANCE_SUCCESS: {
        return {
          ...state,
          byType: {
            ...state.byType,
            ...payload.response.wallet,
          },
          types: payload.response.types,
        };
      }
      case GET_WALLET_STATEMENT_SUCCESS: {
        return {
          ...state,
          statements: payload.response.statements,
        };
      }
      case GET_CART_CONTENT_SUCCESS: {
        return {
          ...state,
          byType: {
            ...state.byType,
            ...payload.response.price.wallet,
          },
        };
      }
      case APPLY_WALLET_SUCCESS: {
        return {
          ...state,
          byType: {
            ...state.byType,
            ...payload.response.price.wallet,
          },
        };
      }
      case APPLY_REMOVE_COUPON_SUCCESS: {
        return {
          ...state,
          byType: {
            ...state.byType,
            ...payload.response.price.wallet,
          },
        };
      }
      case GET_CONFIRMATION_CONTENT_SUCCESS: {
        return {
          ...state,
          byType: {
            ...state.byType,
            ...payload.response.price.wallet,
          },
        };
      }
      default:
        return state;
    }
  }, [
    UNSET_AUTHENTICATED_USER,
  ],
);

export const getWalletStatement = () => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_WALLET_STATEMENT_REQUEST,
      GET_WALLET_STATEMENT_SUCCESS,
      GET_WALLET_STATEMENT_FAILURE,
    ],
    promise: () => api.get('/api/web/v1/reward/statement/')
      .then(walletService.transformStatementApi),
  });

export const getWalletBalance = () => (dispatch, getState, { api }) =>
  dispatch({
    types: [
      GET_WALLET_BALANCE_REQUEST,
      GET_WALLET_BALANCE_SUCCESS,
      GET_WALLET_BALANCE_FAILURE,
    ],
    promise: () => api.get('/api/web/v1/reward/balance/')
      .then(walletService.transformBalanceApi),
  });

export const applyWallet = () => (dispatch, getState, { api }) => {
  const {
    itinerary: {
      cart: {
        bid,
      },
    },
    ui,
  } = getState();

  const applyWalletQuery = { bid, ...ui.utmParams, channel: config.channel };

  return dispatch({
    types: [
      APPLY_WALLET_REQUEST,
      APPLY_WALLET_SUCCESS,
      APPLY_WALLET_FAILURE,
    ],
    promise: () => api.post('/api/web/v5/checkout/wallet/apply/', applyWalletQuery)
      .then((res) => priceService.transformWalletPricingApi(res)),
  })
    .then((response) => {
      const { price } = response;
      if (!price.ratePlans[price.selectedRatePlan].sellingPrice) {
        dispatch({
          type: HIDE_PAYMENT_OPTION,
        });
      }
    });
};

export const removeWallet = () => (dispatch, getState, { api }) => {
  const {
    itinerary: {
      cart: {
        bid,
      },
    },
    ui,
  } = getState();

  const removeWalletQuery = { bid, ...ui.utmParams, channel: config.channel };

  return dispatch({
    types: [
      APPLY_WALLET_REQUEST,
      APPLY_WALLET_SUCCESS,
      APPLY_WALLET_FAILURE,
    ],
    promise: () => api.post('/api/web/v5/checkout/wallet/remove/', removeWalletQuery)
      .then((res) => priceService.transformWalletPricingApi(res)),
  });
};
