/* eslint-disable max-len */
import Wrapper from '../../components/Wrapper/Wrapper';
import NotFound from '../../views/NotFoundPage';
import LandingPage from '../../views/LandingPage';
import SearchPage from '../../views/ResultsPage';
import SeoResultsPage from '../../views/SeoResultsPage';
import HotelDetailsPage from '../../views/HotelDetailsPage';
import ItineraryPage from '../../views/CheckoutPage';
import CitiesPage from '../../views/CitiesPage';
import ConfirmationPage from '../../views/ConfirmationPage';
import AuthenticationPage from '../../views/Authentication/AuthPage';
import BookingHistoryPage from '../../views/BookingHistory/BookingHistoryPage';
import BookingHistoryDetailsPage from '../../views/BookingHistory/BookingHistoryDetailsPage';
import BookingHistoryCancellationPage from '../../views/BookingHistory/BookingHistoryCancellationPage';
import ReviewCollectPage from '../../views/ReviewsAndRatings/CollectReviewPage';
import ReviewSubmitted from '../../views/ReviewsAndRatings/ReviewSubmittedPage';
import FaqPage from '../../views/FaqPage';
import RewardsLandingPage from '../../views/Rewards/RewardsLandingPage';
import RewardsPage from '../../views/Rewards/RewardsPage';
import FeedbackPage from '../../views/Feedback';
import FeedbackChangePage from '../../views/Feedback/FeedbackChange';
import FeedbackThanksPage from '../../views/Feedback/FeedbackThanks';
import ResponsibleDisclosurePolicy from '../../views/ResponsibleDisclosurePolicy/ResponsibleDisclosurePolicy';
import QualityGuaranteePage from '../../views/QualityGuaranteePage';
import PaymentProcess from '../../views/PaymentProcess/PaymentProcess';
import * as routeService from './routeService';

const defaultCacheDuration = 3600000; // in ms
const routes = [{
  component: Wrapper,
  routes: [{
    path: '/',
    exact: true,
    strict: true,
    name: 'Home',
    component: LandingPage,
    cache: defaultCacheDuration,
  }, {
    path: '/cities/',
    exact: true,
    strict: true,
    name: 'Cities',
    component: CitiesPage,
    cache: defaultCacheDuration,
  }, {
    path: '/search/',
    exact: true,
    strict: true,
    name: 'Search',
    component: SearchPage,
    cache: 0,
  }, {
    path: '/hotels-in-:city-with-:amenity/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoResultsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/hotels-in-:locality-:city(\\w+)/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoResultsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/hotels-in-:city/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoResultsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/:category-in-:locality-:city(\\w+)/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoResultsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/:category-in-:city/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoResultsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/hotels-near-:landmark/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoResultsPage,
    cache: [{
      path: '*',
      duration: defaultCacheDuration,
    }, {
      path: '/hotels-near-me/',
      duration: 0,
    }],
  }, {
    path: '/:category-near-:landmark/',
    exact: true,
    strict: true,
    name: 'Hotel Listing',
    component: SeoResultsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/hotels-in-:city/:hotelSlug-:hotelId(\\w+)/',
    exact: true,
    name: 'Hotel Detail',
    component: HotelDetailsPage,
    cache: defaultCacheDuration,
  }, {
    path: '/itinerary/',
    exact: true,
    strict: true,
    name: 'Itinerary',
    component: ItineraryPage,
    cache: 0,
  }, {
    path: '/confirmation/:bookingId/',
    exact: true,
    strict: true,
    name: 'Confirmation',
    component: ConfirmationPage,
    cache: 0,
  }, {
    path: '/login/',
    exact: true,
    strict: true,
    name: 'Login',
    check: routeService.check.guest('/'),
    component: AuthenticationPage,
    cache: defaultCacheDuration,
  }, {
    path: '/forgot-password/',
    exact: true,
    strict: true,
    name: 'Forgot Password',
    component: AuthenticationPage,
    cache: defaultCacheDuration,
  }, {
    path: '/account/booking-history/',
    exact: true,
    strict: true,
    name: 'Booking History List',
    check: routeService.check.member(),
    component: BookingHistoryPage,
    cache: 0,
  }, {
    path: '/account/booking-history/:bookingId/',
    exact: true,
    strict: true,
    name: 'Booking History Details',
    check: routeService.check.member(),
    component: BookingHistoryDetailsPage,
    cache: 0,
  }, {
    path: '/c-:bookingHash/',
    exact: true,
    strict: true,
    name: 'Booking History Cancellation',
    component: BookingHistoryCancellationPage,
    cache: 0,
  }, {
    path: '/tripadvisor/reviews-:hotelId/',
    exact: true,
    strict: true,
    name: 'Reviews',
    component: ReviewCollectPage,
    cache: 0,
  }, {
    path: '/tripadvisor/thank-you/',
    exact: true,
    strict: true,
    name: 'Reviews Submitted',
    component: ReviewSubmitted,
    cache: defaultCacheDuration,
  }, {
    path: '/faq/',
    exact: true,
    strict: true,
    name: 'Faq',
    component: FaqPage,
    cache: defaultCacheDuration,
  }, {
    path: '/introducing-rewards/',
    exact: true,
    strict: true,
    name: 'Introducing Rewards',
    component: RewardsLandingPage,
    cache: defaultCacheDuration,
  }, {
    path: '/rewards/',
    exact: true,
    strict: true,
    name: 'Rewards',
    check: routeService.check.member(),
    component: RewardsPage,
    cache: 0,
  }, {
    path: '/feedback/:feedbackId/',
    exact: true,
    strict: true,
    name: 'Feedback',
    component: FeedbackPage,
    cache: 0,
  }, {
    path: '/feedback/:feedbackId/change/',
    exact: true,
    strict: true,
    name: 'Feedback Change',
    component: FeedbackChangePage,
    cache: 0,
  }, {
    path: '/feedback/:feedbackId/thanks/',
    exact: true,
    strict: true,
    name: 'Feedback Submit',
    component: FeedbackThanksPage,
    cache: 0,
  }, {
    path: '/security/',
    exact: true,
    strict: true,
    name: 'Security',
    component: ResponsibleDisclosurePolicy,
    cache: defaultCacheDuration,
  }, {
    path: '/perfect-stay/',
    exact: true,
    strict: true,
    name: 'Perfect Stay',
    component: QualityGuaranteePage,
  }, {
    path: '/payment-process/:bookingId/',
    exact: true,
    strict: true,
    name: 'paymentProcess',
    component: PaymentProcess,
    cache: 0,
  }, {
    name: 'Not Found',
    component: NotFound,
  }],
}];

export default routes;
