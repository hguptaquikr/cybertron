import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { constants } from '../utils';
import authService from '../auth/authService';

export const renderRoutes = (routes, extraProps = {}, switchProps = {}) => (
  routes ? (
    <Switch {...switchProps}>
      {
        routes.map((route, i) => (
          <Route
            key={route.key || i}
            path={route.path}
            exact={route.exact}
            strict={route.strict}
            render={(props) => {
              const location = route.check && route.check(props);
              if (location) {
                props.history.push(location);
                return null;
              }
              return (
                <route.component
                  {...props}
                  {...extraProps}
                  route={route}
                />
              );
            }}
          />
        ))
      }
    </Switch>
  ) : null
);

export const makeHotelIdQuery = (hotelId) => ({
  hotel_id: hotelId,
});

export const makeDateRangeQuery = (start, end) => ({
  checkin: start && start.format(constants.dateFormat.query),
  checkout: end && end.format(constants.dateFormat.query),
});

export const makeRoomConfigQuery = (rooms = []) => ({
  roomconfig: rooms
    .map((room) => `${room.adults}-${room.kids || 0}`)
    .join(','),
});

export const makeRoomTypeQuery = (roomType) => ({
  roomtype: roomType,
});

export const makeRatePlanQuery = (ratePlan) => ({
  rateplan: ratePlan,
});

export const makeCoordinatesQuery = (coordinates = {}) => ({
  lat: coordinates.lat,
  lng: coordinates.lng,
});

export const check = {
  member: (location = '/login/') => () => {
    if (!authService.isLoggedIn()) {
      return location;
    }
    return null;
  },
  guest: (location) => () => {
    if (authService.isLoggedIn()) {
      return location;
    }
    return null;
  },
};
