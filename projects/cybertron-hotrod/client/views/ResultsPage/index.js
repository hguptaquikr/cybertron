import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

const ResultsPage = Loadable({
  loader: () => {
    importCss('ResultsPage');
    return import('./ResultsPage' /* webpackChunkName: 'ResultsPage' */);
  },
  loading: () => null,
});

export default ResultsPage;
