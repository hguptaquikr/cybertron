import React from 'react';
import Loading from '../../components/Loading/Loading';

const ResultsLoading = () => (
  <div className="r-list__item r-hotel flex-row">
    <div className="r-hotel__image">
      <Loading height="100%" />
    </div>
    <div className="r-hotel__content flex-column">
      <div className="r-hotel__head flex-row">
        <div className="r-hotel__info">
          <Loading width="24rem" marginBottom="1.6rem" />
          <Loading width="12rem" />
        </div>
        <Loading className="r-hotel__price" width="5rem" />
      </div>
      <div className="r-hotel__footer flex-row">
        <Loading className="r-hotel__action" width="16rem" height="4rem" />
      </div>
    </div>
  </div>
);

export default ResultsLoading;
