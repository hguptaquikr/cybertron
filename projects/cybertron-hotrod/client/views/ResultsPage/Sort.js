import React from 'react';
import qs from 'query-string';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import cx from 'classnames';

const SortItem = ({ sortItem, selectedSortItem, sortResultsBy }) => (
  <div
    id="t-sortType"
    className={cx('text-2 rp-sort__item', {
      'rp-sort__item--selected': sortItem === selectedSortItem,
    })}
    onClick={() => sortResultsBy(sortItem)}
  >
    {sortItem}
  </div>
);

SortItem.propTypes = {
  sortItem: PropTypes.string.isRequired,
  selectedSortItem: PropTypes.string.isRequired,
  sortResultsBy: PropTypes.func.isRequired,
};

class Sort extends React.Component {
  state = {
    isVisible: false,
    showSorts: {
      price: true,
      distance: true,
      recommended: true,
    },
  };

  componentWillReceiveProps() {
    const { filters, location } = this.props;
    const locationQuery = qs.parse(location.search);
    if (filters.distanceCap || (locationQuery.lat && locationQuery.lng)) {
      this.showSorts('distance', true);
    } else {
      this.showSorts('distance', false);
      this.showSorts('recommended', true);
    }
  }

  showSorts(sort, visiblity) {
    this.setState({
      showSorts: {
        ...this.state.showSorts,
        [sort]: visiblity,
      },
    });
  }

  sortResultsBy = (selectedSort) => {
    const { filterActions } = this.props;
    this.toggleSortSection();
    filterActions.sortResultsBy(selectedSort);
  }

  toggleSortSection = () => {
    this.setState({
      isVisible: !this.state.isVisible,
    });
  };

  render() {
    const { isVisible, showSorts } = this.state;
    const { sort } = this.props;
    const numberOfShownSorts = Object.keys(showSorts)
      .filter((sortItem) => showSorts[sortItem]).length;
    return (
      <div className="rp-aside">
        <div className="rp-aside__section">
          <div className="rp-aside__section__header">
            <div className="subheading-3">SORT BY</div>
            {
              numberOfShownSorts > 1 ? (
                <i
                  id="t-sortTypeToggle"
                  className={cx('rp-aside__toggle-icon', {
                    'icon-minus': isVisible,
                    'icon-plus': !isVisible,
                  })}
                  onClick={this.toggleSortSection}
                />
              ) : (null)
            }
          </div>
          <div
            className={cx('text-primary rp-sort__active', { hide: isVisible })}
            onClick={this.toggleSortSection}
          >
            {sort.by}
          </div>
          <div className={cx({ hide: !isVisible })}>
            {
              sort.list.map((sortItem) => showSorts[sortItem] && (
                <SortItem
                  key={`sortItem-${sortItem}`}
                  sortItem={sortItem}
                  selectedSortItem={sort.by}
                  sortResultsBy={this.sortResultsBy}
                />
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

Sort.propTypes = {
  sort: PropTypes.object.isRequired,
  filters: PropTypes.object.isRequired,
  filterActions: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

export default withRouter(Sort);
