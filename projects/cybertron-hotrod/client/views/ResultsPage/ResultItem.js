import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import qs from 'query-string';
import cx from 'classnames';
import isEmpty from 'lodash/isEmpty';
import CoupleFriendlyTag from '../../components/CoupleFriendlyTag/CoupleFriendlyTag';
import ABButton from '../../components/ABButton/ABButton';
import Loading from '../../components/Loading/Loading';
import { getDistanceWithUnit } from '../../services/utils';
import priceService from '../../services/price/priceService';
import analyticsService from '../../services/analytics/analyticsService';
import * as routeService from '../../services/route/routeService';
import * as hotelService from '../../services/hotel/hotelService';
import searchService from '../../services/search/searchService';

const ResultItemButton = ({
  prices,
  isAvailable,
  bookNowTarget,
  bookNowClicked,
  checkPricing,
}) => {
  if (Object.keys(prices).length > 1) {
    if (isAvailable) {
      return (
        <Link
          to={bookNowTarget}
          onClick={() => bookNowClicked(bookNowTarget.hotel)}
        >
          <ABButton
            id="t-quickBookCTA"
            experiment="quickBookCTADesktop"
            render={(text) => (
              <div><i className="r-hotel__action__icon icon-flash" />
                {text || 'QUICK BOOK'}
              </div>
            )}
          />
        </Link>
      );
    }
    return null;
  }
  return (
    <button
      className="button button--secondary r-hotel__action"
      onClick={checkPricing}
    >Check Pricing
    </button>
  );
};

ResultItemButton.propTypes = {
  prices: PropTypes.object,
  isAvailable: PropTypes.bool,
  bookNowTarget: PropTypes.object,
  bookNowClicked: PropTypes.func.isRequired,
  checkPricing: PropTypes.func.isRequired,
};

const ResultItem = ({
  hotel,
  search: {
    datePicker: { range },
    occupancy: { rooms },
  },
  bookNowClicked,
  isPricesLoading,
  checkPricing,
  location,
  history,
  prices,
  $roomType,
  setHotelDetails,
  shouldSetParams,
}) => {
  const {
    id,
    name,
    images,
    distance,
    reviews: {
      image: ratingImage,
      overallRating: {
        rating,
        ratingCount,
      },
      award,
    },
    roomTypeIds,
    policies,
  } = hotel;

  const hotelPrices = priceService.getPricesForRoomTypeIds(roomTypeIds, prices);
  const cheapestPrice = priceService.sortPrice(hotelPrices)[0];
  const cheapestRatePlan = priceService.sortRatePlan(cheapestPrice.ratePlans)[0] || {};
  const cheapestRoomTypeId = cheapestPrice.id;
  const cheapestRoomType = cheapestRoomTypeId && $roomType.byId[cheapestRoomTypeId].type;
  const isCoupleFriendly = hotelService.isCoupleFriendly(policies);
  const isAvailable = !!cheapestPrice.available;
  const { numberOfNights } = searchService.formattedDatePicker(range, 'D MMM');

  let distanceHtml = null;
  if (distance) {
    distanceHtml = (
      <div className="r-hotel__distance">
        {`Distance: ${getDistanceWithUnit(distance)}`}
      </div>
    );
  }

  const sellingPrice = !isEmpty(cheapestRatePlan) ?
    Math.round(cheapestRatePlan.sellingPrice) : null;
  const strikedPrice = !isEmpty(cheapestRatePlan) ?
    Math.round(cheapestRatePlan.strikedPrice) : null;

  const discountedPriceJsx = (cheapestRatePlan && cheapestRatePlan.discountPercentage > 0) ? (
    <div className="r-hotel__price__discount-info r-discount flex-row">
      <div className="r-discount__coupon">{cheapestRatePlan.discountPercentage}% off</div>
      <div className="r-discount__price">&#x20b9;{strikedPrice}</div>
    </div>
  ) : (null);

  const priceJsx = !isPricesLoading && sellingPrice !== null ? (
    <div>
      <div className="heading-1 r-hotel__price" id="t-searchItemSellingPrice">&#x20b9;{sellingPrice}</div>
      {discountedPriceJsx}
      <div className="r-hotel__incl-tax incl-tax">
        Tax incl. price for {numberOfNights.text}
      </div>
    </div>
  ) : (
    <div style={{ textAlign: 'right' }}>
      <Loading width="5rem" display="inline-block" height="0.6rem" />
      <Loading width="8rem" height="0.6rem" marginTop="0.8rem" />
    </div>
  );

  const hotelDetailsTarget = hotelService.getHotelDetailsTarget({
    hotelName: hotel.name,
    hotelId: hotel.id,
    area: hotel.address,
    roomType: cheapestRoomType,
    ratePlan: cheapestRatePlan.code,
    locationQuery: {
      ...routeService.makeDateRangeQuery(range.start, range.end),
      ...location.query,
    },
    shouldSetParams,
  });

  const hotelDetailsPrefill = {
    hotel: {
      name,
      id,
      roomTypeIds,
    },
  };

  const bookNowTarget = {
    pathname: '/itinerary/',
    search: qs.stringify({
      hotel_id: hotel.id,
      ...routeService.makeDateRangeQuery(range.start, range.end),
      ...routeService.makeRoomTypeQuery(cheapestRoomType),
      ...routeService.makeRatePlanQuery(cheapestRatePlan.code),
      ...routeService.makeRoomConfigQuery(rooms),
    }),
    hotel,
  };

  const hotelImageClasses = cx('r-hotel__image', {
    'r-hotel__image--soldout': !isAvailable,
  });

  return (
    <div
      id="t-searchResultItem"
      className="r-list__item r-hotel flex-row"
      itemScope
      itemType="http://schema.org/Hotel"
    >
      <div
        className={hotelImageClasses}
        onClick={() => {
          setHotelDetails(hotelDetailsPrefill, 'Image');
          history.push(hotelDetailsTarget);
        }}
      >
        <img src={`${images[0] ? images[0].url : ''}?w=180&h=135&fit=crop&fm=pjpg`} alt={hotel.name} />
      </div>
      <div className="r-hotel__content flex-column">
        <div className="r-hotel__head flex-row">
          <div className="r-hotel__info">
            <h3 className="heading-1">
              <Link
                id="t-searchItemHDLink"
                className="r-hotel__name"
                to={hotelDetailsTarget}
                onClick={() => setHotelDetails(hotelDetailsPrefill, 'Title')}
                itemProp="url"
              >
                <span itemProp="name">{hotel.name}</span>
              </Link>
              {
                !isEmpty(award) ? (
                  <img
                    id="t-searchItemHDImage"
                    className="r-hotel__award-img"
                    src={award.image}
                    alt="TripAdvisor Reward"
                    title={award.description}
                  />
                ) : (null)
              }

            </h3>
            <meta itemProp="telephone" content="+91-9322800100" />
            <meta itemProp="email" content="hello@treebohotels.com" />
            <div
              className="r-hotel__address text-2"
              itemProp="address"
              itemScope
              itemType="http://schema.org/PostalAddress"
            >
              <meta itemProp="addressLocality" content={hotel.address.locality} />
              <meta itemProp="addressRegion" content={hotel.address.city} />
              <span>{hotel.address.locality}, {hotel.address.city}</span>
            </div>
            <div className="r-hotel__rating-reviews">
              <div className="flex-row row--middle">
                {
                  ratingImage ? (
                    <div
                      className="r-hotel__rating-reviews__link"
                      onClick={() => {
                        analyticsService.taReviewImageClicked('Search Page', hotel.name);
                        history.push(hotelDetailsTarget);
                      }}
                      itemProp="aggregateRating"
                      itemScope
                      itemType="http://schema.org/AggregateRating"
                    >
                      <meta itemProp="ratingValue" content={rating} />
                      <meta itemProp="ratingCount" content={ratingCount} />
                      <meta itemProp="bestRating" content="5" />
                      <img
                        className="r-hotel__rating-reviews__image"
                        src={ratingImage}
                        alt={`TripAdvisor Rating ${rating}`}
                      />
                    </div>
                  ) : (null)
                }
                {
                  ratingCount ? (
                    <div
                      className="r-hotel__rating-reviews__total text-3"
                      onClick={() => history.push(hotelDetailsTarget)}
                    >
                      {ratingCount} reviews
                    </div>
                  ) : null
                }
              </div>
            </div>
          </div>
          {
            isAvailable ? (
              priceJsx
            ) : (
              <div className="r-hotel__price--soldout">SOLD OUT</div>
            )
          }
        </div>
        <div className="r-hotel__footer flex-row row--bottom">
          <div>
            {isCoupleFriendly ? <CoupleFriendlyTag /> : null}
            {
              distanceHtml ? (
                <div className="row row--middle">
                  {distanceHtml}
                </div>
              ) : null
            }
          </div>
          {
            !isPricesLoading ? (
              <ResultItemButton
                prices={prices}
                isAvailable={isAvailable}
                bookNowTarget={bookNowTarget}
                bookNowClicked={bookNowClicked}
                checkPricing={checkPricing}
              />
            ) : null
          }
        </div>
      </div>
    </div>
  );
};

ResultItem.propTypes = {
  hotel: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  bookNowClicked: PropTypes.func.isRequired,
  isPricesLoading: PropTypes.bool.isRequired,
  checkPricing: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  prices: PropTypes.object.isRequired,
  setHotelDetails: PropTypes.func.isRequired,
  $roomType: PropTypes.object.isRequired,
  shouldSetParams: PropTypes.bool.isRequired,
};

export default withRouter(ResultItem);
