import React from 'react';
import PropTypes from 'prop-types';
import qs from 'query-string';
import isEmpty from 'lodash/isEmpty';
import startCase from 'lodash/startCase';
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';
import SearchWidget from '../../views/SearchWidget/SearchWidget';

const Header = ({ search, hotels, location }) => {
  const {
    searchInput,
    datePicker: { range },
  } = search;

  const noOfTotalResults = hotels.length;
  const resultCountText = `Showing ${noOfTotalResults} best hotels in ${startCase(qs.parse(location.search).q)}`;

  return (
    <div className="container results-page__head">
      <Breadcrumbs />
      <div className="results-page__info">
        <div className="results-page__query-container">
          <div className="results-page__query-container__title">
            {startCase(resultCountText)}
          </div>
        </div>
        <SearchWidget
          isSearchDisabled={
            range.start
            && range.end
            && !isEmpty(searchInput.location)
          }
        />
      </div>
    </div>
  );
};

Header.propTypes = {
  search: PropTypes.object.isRequired,
  hotels: PropTypes.array.isRequired,
  location: PropTypes.array.isRequired,
};

export default Header;
