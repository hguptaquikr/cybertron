/* eslint-disable react/no-danger */
import isEqual from 'lodash/isEqual';
import React from 'react';
import { findDOMNode } from 'react-dom';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Chip from '../../components/Chip/Chip';
import analyticsService from '../../services/analytics/analyticsService';

const FilterSectionHeader = ({ title, isOpen, filterKey, toggleFiltersSection }) => (
  <div className="rp-aside__section__header">
    <div className="subheading-3">{title}</div>
    <i
      className={cx('rp-aside__toggle-icon', {
        'icon-minus': isOpen[filterKey],
        'icon-plus': !isOpen[filterKey],
      })}
      onClick={() => toggleFiltersSection(filterKey)}
    />
  </div>
);

FilterSectionHeader.propTypes = {
  title: PropTypes.string.isRequired,
  filterKey: PropTypes.string.isRequired,
  isOpen: PropTypes.object.isRequired,
  toggleFiltersSection: PropTypes.func.isRequired,
};

const FilterInputs = ({
  className,
  filterItems,
  filterKey,
  filtersCheckboxChange,
  showActionClick,
  showAction,
}) => {
  let filterItemsToMap = filterItems;
  if (filterItems.length > 5 && !showAction) filterItemsToMap = filterItems.slice(0, 5);

  return (
    <div className={className}>
      {
        filterItemsToMap.map((filterItem, i) => {
          const key = `${filterKey}-${i}`;
          return (
            <div className="text-2 rp-filter__list__item" key={key} title={filterItem.name}>
              <label htmlFor={key}>
                <input
                  id={key}
                  type="checkbox"
                  onChange={(e) => filtersCheckboxChange(filterKey, i, e.target.checked)}
                  onClick={() => analyticsService.filterClicked(filterKey, filterItem.name)}
                  checked={filterItem.checked}
                />
                <span dangerouslySetInnerHTML={{ __html: filterItem.name }} />
              </label>
            </div>
          );
        })
      }
      {
        filterItems.length > 4 ? (
          <div
            className="text-link rp-filter__show-action"
            onClick={() => showActionClick(filterKey)}
          >{showAction ? 'Show Less' : 'Show More'}
          </div>
        ) : (null)
      }
    </div>
  );
};

FilterInputs.propTypes = {
  className: PropTypes.string.isRequired,
  filterItems: PropTypes.array.isRequired,
  filterKey: PropTypes.string.isRequired,
  filtersCheckboxChange: PropTypes.func.isRequired,
  showActionClick: PropTypes.func,
  showAction: PropTypes.bool,
};

class Filters extends React.Component {
  state = {
    isOpen: {
      priceRanges: true,
      localities: true,
      amenities: true,
    },
    showAction: {
      localities: false,
      amenities: false,
    },
  }

  componentDidMount() {
    const { filters, filterActions, route, match } = this.props;
    if (!filters.amenities.length) {
      filterActions.getAmenitiesFilter(route, match.params);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (!isEqual(prevProps, this.props)) {
      this.scrollToRef();
    } else if (!isEqual(prevState, this.state)) {
      window.scrollBy(0, -1);
      setTimeout(() => window.scrollBy(0, 1));
    }
  }

  scrollToRef = () => {
    const { scrollRef } = this.props;
    const scrollNode = findDOMNode(scrollRef); // eslint-disable-line
    if (scrollNode && window.scrollY > scrollNode.offsetTop - 16) {
      window.scroll(0, scrollNode.offsetTop - 16);
    }
  }

  showActionClick = (filterKey) => {
    const { showAction } = this.state;
    this.setState({
      showAction: {
        ...showAction,
        [filterKey]: !showAction[filterKey],
      },
    });
    if (showAction[filterKey]) this.scrollToRef();
  }

  clearAllFilters = () => {
    const { filterActions } = this.props;
    filterActions.clearAllFilters();
    filterActions.refineResults();
    this.scrollToRef();
  }

  toggleFiltersSection = (filterKey) => {
    this.setState({
      isOpen: {
        ...this.state.isOpen,
        [filterKey]: !this.state.isOpen[filterKey],
      },
    });
  }

  filtersCheckboxChange = (filterKey, i, checked) => {
    const { filterActions } = this.props;
    filterActions.filtersCheckboxChange(filterKey, i, checked);
    filterActions.refineResults();
  }

  render() {
    const {
      noOfResults,
      filters,
      filterClicked,
      isCoupleFriendlyHotelAvailable,
    } = this.props;
    const { isOpen, showAction } = this.state;
    const showPriceRangesFilter = noOfResults >= 3 && filters.priceRanges.length;
    return (
      <div>
        <div className="rp-aside">
          <div className="rp-aside__section">
            <div className="rp-aside__section__header">
              <div className="subheading-3">FILTERS</div>
              <div className="text-link rp-aside__clear-all" onClick={this.clearAllFilters}>
                CLEAR ALL
              </div>
            </div>
          </div>
          {
            isCoupleFriendlyHotelAvailable ? (
              <div className="rp-aside__section rp-filter__couple-friendly">
                <span className="text-2">Introducing</span>
                <div className="text-2 rp-filter__couple-friendly__header">
                  <b>Couple Friendly Treebos</b>
                  <Chip
                    text="New"
                    color="golden-tainoi"
                    className="rp-filter__couple-friendly__chip"
                  />
                </div>
                <div className="rp-aside__section__header">
                  <input
                    id="couple-friendly"
                    className="rp-filter__available__input"
                    type="checkbox"
                    onChange={(e) => this.filtersCheckboxChange('showCoupleFriendlyOnly', 0, e.target.checked)}
                    onClick={(e) => analyticsService.filterClicked('showCoupleFriendlyOnly', e.target.checked)}
                    checked={filters.showCoupleFriendlyOnly[0].checked}
                  />
                  <label htmlFor="couple-friendly">
                    <span className="rp-filter__couple-friendly__input-label">View All</span>
                  </label>
                </div>
              </div>
            ) : null
          }
        </div>
        <div className="rp-aside">
          {
            showPriceRangesFilter ? (
              <div className="rp-aside__section">
                <FilterSectionHeader
                  title="PRICE RANGE"
                  isOpen={isOpen}
                  filterKey="priceRanges"
                  toggleFiltersSection={this.toggleFiltersSection}
                />
                <FilterInputs
                  className={cx('rp-filter__list', { hide: !isOpen.priceRanges })}
                  filterItems={filters.priceRanges}
                  filterKey="priceRanges"
                  filtersCheckboxChange={this.filtersCheckboxChange}
                />
              </div>
            ) : null
          }
          <div className="rp-aside__section">
            <div className="rp-aside__section__header">
              <label htmlFor="availability" className="subheading-3">
                <span>Show only available hotels</span>
              </label>
              <input
                id="availability"
                className="rp-filter__available__input"
                type="checkbox"
                onChange={(e) => this.filtersCheckboxChange('showAvailableOnly', 0, e.target.checked)}
                checked={filters.showAvailableOnly[0].checked}
              />
            </div>
          </div>
        </div>
        <div className="rp-aside">
          <div className="rp-aside__section">
            <FilterSectionHeader
              title="LOCALITIES"
              isOpen={isOpen}
              filterKey="localities"
              toggleFiltersSection={this.toggleFiltersSection}
            />
            <FilterInputs
              className={cx('rp-filter__list', { hide: !isOpen.localities })}
              filterItems={filters.localities}
              filterKey="localities"
              filtersCheckboxChange={this.filtersCheckboxChange}
              filterClicked={filterClicked}
              showAction={showAction.localities}
              showActionClick={this.showActionClick}
            />
          </div>
        </div>
        <div className="rp-aside">
          <div className="rp-aside__section">
            <FilterSectionHeader
              title="AMENITIES"
              isOpen={isOpen}
              filterKey="amenities"
              toggleFiltersSection={this.toggleFiltersSection}
            />
            <FilterInputs
              className={cx('rp-filter__list', { hide: !isOpen.amenities })}
              filterItems={filters.amenities}
              filterKey="amenities"
              filtersCheckboxChange={this.filtersCheckboxChange}
              filterClicked={filterClicked}
              showAction={showAction.amenities}
              showActionClick={this.showActionClick}
            />
          </div>
        </div>

      </div>
    );
  }
}

Filters.propTypes = {
  noOfResults: PropTypes.number.isRequired,
  filters: PropTypes.object.isRequired,
  filterActions: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  filterClicked: PropTypes.func,
  scrollRef: PropTypes.object,
  isCoupleFriendlyHotelAvailable: PropTypes.bool,
};

export default withRouter(Filters);
