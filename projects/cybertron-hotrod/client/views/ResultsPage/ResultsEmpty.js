import React from 'react';
import PropTypes from 'prop-types';

const ResultsEmpty = ({ isTrulyEmpty }) => {
  let title = 'We do not have any hotels to show at the moment!';
  if (!isTrulyEmpty) {
    title = 'We don\'t have hotels meeting your filter criteria!';
  }

  return (
    <div className="results-empty">
      <div className="results-empty__image"><i className="results-empty__icon icon-search" /></div>
      <div className="results-empty__head heading-1">{title}</div>
      <div className="results-empty__title">Please call 9322 800 100 for immediate assistance.</div>
      <div className="results-empty__sub">We hope to serve you soon!</div>
    </div>
  );
};

ResultsEmpty.propTypes = {
  isTrulyEmpty: PropTypes.bool.isRequired,
};

export default ResultsEmpty;
