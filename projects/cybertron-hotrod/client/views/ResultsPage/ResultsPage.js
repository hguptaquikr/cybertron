import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Sticky from 'react-stickynode';
import qs from 'query-string';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as filterActionCreators from '../../services/filter/filterDuck';
import * as hotelActionCreators from '../../services/hotel/hotelDuck';
import * as priceActionCreators from '../../services/price/priceDuck';
import * as walletActionCreators from '../../services/wallet/walletDuck';
import authService from '../../services/auth/authService';
import searchService from '../../services/search/searchService';
import HotelDetailsPage from '../HotelDetailsPage';
import CheckoutPage from '../CheckoutPage';
import Filters from './Filters';
import Header from './Header';
import ResultItem from './ResultItem';
import ResultsEmpty from './ResultsEmpty';
import ResultsLoading from './ResultsLoading';
import Sort from './Sort';
// import WalletBanner from '../../components/WalletBanner/WalletBanner';
// import AutoApplyCouponBanner from '../../components/AutoApplyCouponBanner/AutoApplyCouponBanner';
import QualityGuaranteeBanner from '../../components/QualityGuaranteeBanner/QualityGuaranteeBanner';
import { getTotalGuests, safeKeys, constants } from '../../services/utils';
import analyticsService from '../../services/analytics/analyticsService';
import * as hotelService from '../../services/hotel/hotelService';
import * as abService from '../../services/ab/abService';
import './resultsPage.css';

class ResultsPage extends React.Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match, req }) => {
    const promises = [];

    dispatch(searchActionCreators.setSearchState({ ...req.query, ...match.params }));

    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(
      dispatch(hotelActionCreators.getHotelResults(req.query, match.params))
        .then(() => dispatch(hotelActionCreators.addDistanceToResults()))
        .then(() => dispatch(filterActionCreators.refineResults())),
    );

    return Promise.all(promises);
  }

  componentDidMount() {
    const {
      contentActions,
      searchActions,
      walletActions,
      location,
      match,
      route,
    } = this.props;

    searchActions.setSearchState({ ...qs.parse(location.search), ...match.params });
    contentActions.getSeoContent(route, match.params);
    this.getHotelResults();
    if (authService.isLoggedIn()) walletActions.getWalletBalance();
    HotelDetailsPage.preload();
    CheckoutPage.preload();
  }

  componentWillReceiveProps(nextProps) {
    const { props } = this;
    if (props.location.pathname === nextProps.location.pathname) {
      if (
        props.location.search.toLowerCase() !== nextProps.location.search.toLowerCase()
        || (props.user.isAuthenticated && !nextProps.user.isAuthenticated)
      ) {
        this.getHotelResults(nextProps);
      }
    }
  }

  componentWillUnmount() {
    const { filterActions } = this.props;
    filterActions.clearResults();
    filterActions.clearAllFilters();
  }

  onLoginSuccess = () => {
    const {
      hotel,
    } = this.props;
    const hotelIds = Object.keys(hotel.byId);
    this.getSearchPrices(hotelIds);
  }

  getHotelResults = ({ location, match, filterActions, hotelActions } = this.props) => {
    hotelActions.getHotelResults(qs.parse(location.search), match.params).then((res) => {
      hotelActions.addDistanceToResults();
      if (res.ids.length) this.getSearchPrices(res.ids);
      filterActions.clearAllFilters();
      filterActions.constructLocalitiesFilter();
    });
  }

  getSearchPrices = (hotelIds) => {
    const {
      search: { datePicker, occupancy },
      priceActions,
      filterActions,
      location,
      match,
    } = this.props;
    if (datePicker.range.start && datePicker.range.end && occupancy.rooms.length) {
      priceActions.getSearchPrices(hotelIds, location.query, match.params)
        .then(() => {
          const isMemberDiscountApplied = this.isMemberDiscountApplied();
          if (isMemberDiscountApplied) {
            analyticsService.memberDiscountApplied('Search Result Page');
          }
          filterActions.refineResults();
          filterActions.constructPriceRangesFilter();
          this.captureAnalytics(qs.parse(location.search));
        });
    }
  }

  checkPricing = () => {
    const { searchActions, hotel } = this.props;
    const hotelIds = Object.keys(hotel.byId);
    if (searchActions.validateSearch()) {
      this.getSearchPrices(hotelIds);
    }
  }

  captureAnalytics = ({ q: destination }) => { // Check on this
    const {
      match: {
        params: {
          city,
        },
      },
      search: {
        occupancy: { rooms },
        datePicker: { range },
      },
      hotel: {
        ids,
      },
    } = this.props;
    const { checkIn, checkOut, numberOfNights, abwDays } =
    searchService.formattedDatePicker(range, constants.dateFormat.query);
    analyticsService.listContentViewed({
      flow: 'search',
      destination: city || destination,
      checkin: checkIn,
      checkout: checkOut,
      lengthOfStay: numberOfNights.count,
      abwDays,
      ...getTotalGuests(rooms),
      rooms: rooms.length,
      resultCount: ids.length,
      ...abService.impressionProperties(['findPageCtaDesktop', 'quickBookCTADesktop']),
    });
  }

  isMemberDiscountApplied = () => {
    const { price: { byRoomTypeId: prices } } = this.props;
    return Object.values(prices).some((price) => price.memberDiscount.isApplied);
  }

  isCoupleFriendlyHotelAvailable = (hotels, refinedHotelIds) =>
    !!refinedHotelIds.length && refinedHotelIds.some((hotelId) => {
      const hotel = hotels.byId[hotelId];
      return hotel && hotelService.isCoupleFriendly(hotel.policies);
    });

  storeScrollRef = (scrollRef) => {
    if (scrollRef !== null) this.scrollRef = scrollRef;
  }

  render() {
    const {
      match,
      location,
      search,
      hotelActions,
      filterActions,
      filter: { refinedHotelIds, sort, filters },
      price,
      hotel,
      // wallet,
      route,
      $roomType,
    } = this.props;
    const isCoupleFriendlyHotelAvailable =
      this.isCoupleFriendlyHotelAvailable(hotel, refinedHotelIds);
    const analyticsBookClicked = (args, index) => (
      hotelActions.bookNowClicked(args, {
        searchRank: index + 1,
        destination: search.searchInput.location.label,
        resultCount: refinedHotelIds.length,
      })
    );

    const analyticsSetHotelDetails = (prefillHotel, type, hotelData, index) => (
      analyticsService.hotelViewDetailsClicked(prefillHotel, {
        searchRank: index + 1,
        destination: search.searchInput.location.label,
        resultCount: refinedHotelIds.length,
        type,
      })
    );

    let resultsJsx;
    if (hotel.isHotelResultsLoading) {
      resultsJsx = [0, 1, 2, 3, 4].map((key) => <ResultsLoading key={key} />);
    } else if (refinedHotelIds.length || price.isPricesLoading) {
      resultsJsx = refinedHotelIds.map((hotelId, index) =>
        (<ResultItem
          key={hotelId}
          hotel={hotel.byId[hotelId]}
          prices={price.byRoomTypeId}
          search={search}
          setHotelDetails={
            (...args) => analyticsSetHotelDetails(...args, hotel.byId[hotelId], index)
          }
          bookNowClicked={(args) => analyticsBookClicked(args, index)}
          isPricesLoading={price.isPricesLoading}
          checkPricing={this.checkPricing}
          location={location}
          params={match.params}
          $roomType={$roomType}
          shouldSetParams
        />));
    } else {
      resultsJsx = <ResultsEmpty isTrulyEmpty={!hotel.ids.length} />;
    }

    return (
      <div className="results-page">
        <Header
          search={search}
          hotels={refinedHotelIds}
          location={location}
        />
        <div className="container results-page__body" id="resultsPageBody">
          <div className="row">
            <div className="col-3">
              <Sticky top={16} bottomBoundary="#resultsPageBody">
                <Sort
                  sort={sort}
                  filters={filters}
                  filterActions={filterActions}
                />
                <Filters
                  route={route}
                  noOfResults={hotel.ids.length}
                  filters={filters}
                  filterActions={filterActions}
                  scrollRef={this.scrollRef}
                  isCoupleFriendlyHotelAvailable={isCoupleFriendlyHotelAvailable}
                />
              </Sticky>
            </div>
            <div className="col-9 ">
              {/*
                {
                  wallet.byType.TP.isApplied ? (
                    <WalletBanner
                      walletAmount={wallet.byType.TP.usableBalance}
                      src="srp"
                    />
                  ) : null
                }
                {
                  !price.isPricesLoading ? (
                    <AutoApplyCouponBanner />
                  ) : null
                }
              */}
              <div className="results-page__quality-guarantee-banner">
                <QualityGuaranteeBanner
                  page={match.params.city ? 'Hotels In City' : 'Search'}
                />
              </div>
              <meta itemScope itemType="http://schema.org/WebPage" />
              <meta itemProp="name" content={route.name} />
              <div
                ref={this.storeScrollRef}
                className="results-page__results r-list"
              >{resultsJsx}
              </div>
              {
                safeKeys(hotel.byId).length === 1 ? (
                  <div className="subheading-2 results-page__single-result-text">
                    That&apos;s all folks,<br />
                    There&apos;s only one and it&apos;s the best option available in this location.
                  </div>
                ) : (null)
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ResultsPage.propTypes = {
  search: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  priceActions: PropTypes.object.isRequired,
  hotelActions: PropTypes.object.isRequired,
  filterActions: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  // wallet: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired, // eslint-disable-line
  price: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  wallet: state.wallet,
  search: state.search,
  price: state.price,
  filter: state.filter,
  content: state.content,
  hotel: state.hotel,
  $roomType: state.$roomType,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  priceActions: bindActionCreators(priceActionCreators, dispatch),
  filterActions: bindActionCreators(filterActionCreators, dispatch),
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(ResultsPage);
