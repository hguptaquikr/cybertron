import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

const BookingHistoryDetailsPage = Loadable({
  loader: () => {
    importCss('BookingHistoryDetailsPage');
    return import('./BookingHistoryDetailsPage' /* webpackChunkName: 'BookingHistoryDetailsPage' */);
  },
  loading: () => null,
});

export default BookingHistoryDetailsPage;
