import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import { DropdownMenu } from 'react-dd-menu';
import { Link, withRouter } from 'react-router-dom';
import Formsy from 'formsy-react';
import * as userActionCreators from '../../../services/user/userDuck';
import * as contentActionCreators from '../../../services/content/contentDuck';
import { pluralize, constants } from '../../../services/utils';
import FormsyInput from '../../../components/Formsy/FormsyInput';
import NewYearRateTag from '../../../components/NewYearRateTag/NewYearRateTag';
import analyticsService from '../../../services/analytics/analyticsService';
import './bookingHistoryDetailsPage.css';

class BookingHistoryDetailsPage extends React.Component {
  static componentWillServerRender = ({ store: { dispatch }, match, route }) =>
    dispatch(contentActionCreators.getSeoContent(route, match.params));

  state = {
    isDropDownOpen: false,
    isEmailSent: false,
  }

  componentDidMount() {
    const {
      userActions,
      route,
      match,
      contentActions,
    } = this.props;

    contentActions.getSeoContent(route, match.params);
    userActions.getBookingHistory()
      .then((res) => {
        const bookingDetails = res.results[match.params.bookingId];
        analyticsService.bookingHistoryContentViewed({
          checkIn: bookingDetails.dates.checkIn,
          checkOut: bookingDetails.dates.checkOut,
          partPayAmount: bookingDetails.partPayAmount || 0,
        });
      });
  }

  onLinkClicked = (link) => {
    analyticsService.bookingHistoryDownloadClicked(link);
  };

  onSubmit = ({ email }) => {
    const {
      userActions,
      match,
    } = this.props;

    userActions.sendBookingDetailsToEmail(email, match.params.bookingId)
      .then(() => this.setState({ isEmailSent: true }))
      .catch(() => this.setState({ isEmailSent: false }));
  }

  toggleDropDown = () => {
    this.setState({ isDropDownOpen: !this.state.isDropDownOpen });
  }

  formatDate = (date) => moment(date).format(constants.dateFormat.history);

  render() {
    const { isDropDownOpen, isEmailSent } = this.state;
    const {
      bookingHistory: {
        results,
      },
      match,
    } = this.props;

    const booking = results[match.params.bookingId] || results[0];

    return (
      <div className="bhd">
        <div className="container bhd__container">
          <div className="row">
            <div className="col-6">
              <Link to="/account/booking-history/">
                <i className="icon-angle-left" /> Back to booking history
              </Link>
            </div>
            <div className="col-2">
              <a
                onClick={() => this.onLinkClicked('Print')}
                href={`/api/web/v1/checkout/display?name=${booking.guest.name}&order_id=${booking.id}`}
              >
                <div className="row">
                  <div className="col-2">
                    <i className="icon-printer-text bhd__icon" />
                  </div>
                  <div className="col-10">
                    <p className="bhd__email-text">PRINT</p>
                  </div>
                </div>
              </a>
            </div>
            <div className="col-2">
              <a
                onClick={() => this.onLinkClicked('Download')}
                href={`/api/web/v1/checkout/printpdf?name=${booking.guest.name}&order_id=${booking.id}`}
              >
                <div className="row">
                  <div className="col-2">
                    <i className="icon-download bhd__icon" />
                  </div>
                  <div className="col-10">
                    <p className="bhd__email-text">DOWNLOAD</p>
                  </div>
                </div>
              </a>
            </div>
            <div className="col-2">
              <DropdownMenu
                isOpen={isDropDownOpen}
                close={this.toggleDropDown}
                menuAlign="right"
                animAlign="right"
                size="md"
                closeOnInsideClick={false}
                toggle={
                  <div
                    className="bhd__email row"
                    onClick={this.toggleDropDown}
                  >
                    <div className="col-4">
                      <i className="icon-mail-envelope-closed bhd__icon" />
                    </div>
                    <div className="col-8">
                      <p className="bhd__email-text">EMAIL</p>
                    </div>
                  </div>
                }
              >
                <Formsy className="bhd__form" onValidSubmit={this.onSubmit}>
                  <div className="row">
                    <div className="col-12">
                      {
                        isEmailSent ? (
                          <p className="subheading-3 bhd__email-sent">Email Sent</p>
                        ) : null
                      }
                    </div>
                    <div className="col-8">
                      <FormsyInput
                        type="text"
                        name="email"
                        label="Email"
                        validations="isEmail"
                        validationError="Please enter a valid email"
                        required
                      />
                    </div>
                    <div className="col-4">
                      <button
                        onClick={() => this.onLinkClicked('Email Download')}
                        type="submit"
                        name="button"
                        className="bhd__form__submit button"
                      >Submit
                      </button>
                    </div>
                  </div>
                </Formsy>
              </DropdownMenu>
            </div>
          </div>
          <div>
            <div className="row bhd__heading">
              <div className="col-10">
                <p className="heading-1">{booking.hotel.name}</p>
                <p className="subheading-3">Booking Id: {booking.id}</p>
                <p className="subheading-3">
                  Booking Date: {this.formatDate(booking.dates.booking)}
                </p>
              </div>
              {
                booking.status === 'CANCELLED' ? (
                  <p className="subheading-1 bhd__cancelled">
                     Booking successfully cancelled on {this.formatDate(booking.dates.cancel)}
                  </p>
                ) : null
              }
              <div className="col-2">
                {
                  booking.status === 'RESERVED' && booking.partPayLink ?
                  (
                    <div>
                      <button className="button">
                        <a href={booking.partPayLink}>
                          PAY &#8377;{booking.partPayAmount} NOW
                        </a>
                      </button>
                      <p className="bhd__part-pay-text">
                        Part pay to confirm booking and avoid inconvenience
                      </p>
                    </div>
                  ) : null
                }
              </div>
            </div>
            <h2 className="heading-2 bhd__title">HOTEL INFORMATION</h2>
            <div className="row row--middle">
              <div className="col-6">
                <p className="subheading-1">{booking.hotel.name}</p>
                <p className="text-2">
                  {booking.hotel.locality}, {booking.hotel.city}
                </p>
                <p>
                  {
                    `${booking.roomConfig.count} Room
                    (${booking.roomConfig.type}), ${booking.roomConfig.adults}
                       ${pluralize(booking.roomConfig.adults, 'Adult')}`
                  }
                  {
                    booking.roomConfig.kids ? (
                      `, ${booking.roomConfig.kids}
                        ${pluralize(booking.roomConfig.kids, 'kid')}`
                    ) : null
                  }
                </p>
              </div>
              <div className="col-3">
                <p>Check In</p>
                <p className="subheading-1">{this.formatDate(booking.dates.checkIn)}</p>
              </div>
              <div className="col-1">
                <i className="icon-arrow-right" /><br />
                <i className="icon-arrow-left" />
              </div>
              <div className="col-2">
                <p>Check Out</p>
                <p className="subheading-1">{this.formatDate(booking.dates.checkOut)}</p>
              </div>
            </div>
            <h2 className="heading-2 bhd__title">TRAVELLER INFORMATION</h2>
            <div className="row row--middle">
              <div className="col-1">
                <i className="icon-users" />
              </div>
              <div className="col-4">
                <p className="subheading-1">{booking.guest.name}</p>
                <p>{booking.guest.email}</p>
              </div>
              <div className="col-4">
                <p>{booking.guest.contact}</p>
              </div>
            </div>
            <h2 className="heading-2 bhd__title">PAYMENT INFORMATION</h2>
            <NewYearRateTag
              hotelId={booking.hotel.id}
              range={{
                start: booking.dates.checkIn,
                end: booking.dates.checkOut,
              }}
            />
            <p className="bhd__info-payment">Total:
              <span className="subheading-1">
                &#8377; {booking.price.total}
              </span>
            </p>
            <p>Payment Mode:
              {
                booking.isPayAtHotel ? ' To be paid at Hotel' : ' Already paid'
              }
            </p>
            {
              booking.type !== 'past' && booking.status !== 'CANCELLED' ? (
                <button className="button bhd__cancel">
                  <Link to={`/c-${booking.cancellation.hash}/`}>
                    CANCEL BOOKING
                  </Link>
                </button>
              ) : null
            }
          </div>
        </div>
      </div>
    );
  }
}

BookingHistoryDetailsPage.propTypes = {
  bookingHistory: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  bookingHistory: state.user.bookingHistory,
});

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(BookingHistoryDetailsPage);
