import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import CancellationForm from './CancellationForm';
import * as cancelActionCreators from '../../../services/user/cancelDuck';
import * as contentActionCreators from '../../../services/content/contentDuck';
import './bookingHistoryCancellationPage.css';

const CancellationStatus = ({
  bookingDetail: {
    hotel_name: hotelName,
    hotel_address: hotelAddress,
  },
  bookingStatus: {
    message,
  },
  cancelledDate,
}) => (
  <div className="bhcp-cancellation-status">
    <div className="bhcp-cancellation-status__result">
      <div className="bhcp-cancellation-status__image">
        <img src="http://images.treebohotels.com/images/booking_cancel_expire.png?h=100" alt="" />
      </div>
      <div className="text-1 bhcp-cancellation-status__message">
        {message}
      </div>
      <div className="bhcp-cancellation-status__details">
        <div className=" text-2 bhcp-cancellation-status__details__hotel">
          {hotelName}
        </div>
        <div className="bhcp-cancellation-status__details__hotel-address">
          {hotelAddress}
        </div>
        <div className="bhcp-cancellation-status__details__hotel">
          Cancelled dates : {cancelledDate}
        </div>
        <div className="bhcp-cancellation-status__btn">
          <Link to="/" className="">
            <button className="button">
              SEARCH OTHER TREEBO HOTELS
            </button>
          </Link>
        </div>
      </div>
    </div>
  </div>
);

CancellationStatus.propTypes = {
  bookingDetail: PropTypes.object.isRequired,
  bookingStatus: PropTypes.object.isRequired,
  cancelledDate: PropTypes.string.isRequired,
};

class BookingHistoryCancellationPage extends React.Component {
  static componentWillServerRender = ({ store: { dispatch }, match, route }) => {
    const promises = [];

    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));

    promises.push(dispatch(cancelActionCreators.getBookingDetails(match.params.bookingHash)));

    return Promise.all(promises);
  }

  state = {
    msg: 'Please wait while we are processing your request..........',
  }

  componentDidMount() {
    const { cancelActions, contentActions, match, route } = this.props;
    contentActions.getSeoContent(route, match.params);
    cancelActions.getBookingDetails(match.params.bookingHash);
  }

  render() {
    const {
      cancellation: {
        bookingDetail,
        bookingStatus,
        error,
      },
      cancelActions,
      match,
    } = this.props;
    let { msg } = this.state;
    let cancelPageJsx = '';

    if (!isEmpty(bookingDetail)) {
      const cancelledDate = bookingDetail.cancelled_dateTime || bookingStatus.cancelled_dateTime;
      cancelPageJsx = (bookingStatus.status === 'reserved') ? (
        <div>
          <CancellationForm
            cancelActions={cancelActions}
            bookingDetail={bookingDetail}
            params={match.params}
          />
        </div>
      ) : (
        <div>
          <CancellationStatus
            bookingDetail={bookingDetail}
            bookingStatus={bookingStatus}
            cancelledDate={cancelledDate}
          />
        </div>
      );
    } else if (error) {
      // eslint-disable-next-line
      msg = error.msg;
    }

    return (
      <div className="bhcp">
        <div className="container">
          {
            !isEmpty(bookingDetail) ? (
              <div>
                {cancelPageJsx}
              </div>
            ) : (
              <div className="bhcp-wait-page">
                <div className="bhcp-wait-page__message heading-2">
                  {msg}
                </div>
              </div>
            )
          }
        </div>
      </div>
    );
  }
}

BookingHistoryCancellationPage.propTypes = {
  cancelActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  cancellation: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  cities: state.content.cities,
  cancellation: state.cancellation,
});

const mapDispatchToProps = (dispatch) => ({
  cancelActions: bindActionCreators(cancelActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(BookingHistoryCancellationPage);
