import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

const BookingHistoryCancellationPage = Loadable({
  loader: () => {
    importCss('BookingHistoryCancellationPage');
    return import('./BookingHistoryCancellationPage' /* webpackChunkName: 'BookingHistoryCancellationPage' */);
  },
  loading: () => null,
});

export default BookingHistoryCancellationPage;
