import React from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import RadioGroup from '../../../components/RadioGroup/RadioGroup';
import FormsyInput from '../../../components/Formsy/FormsyInput';
import analyticsService from '../../../services/analytics/analyticsService';

class CancellationForm extends React.Component {
  state = {
    otherReason: false,
  }

  onChange = (values) => {
    const { bookingDetail } = this.props;
    const options = bookingDetail.reasons;

    if (values.reason === options[options.length - 1].value.toString()) {
      this.setState({
        otherReason: true,
      });
    } else {
      this.setState({
        otherReason: false,
      });
    }
  };

  cancel = (data) => {
    const { cancelActions, params, bookingDetail } = this.props;
    const { reason, message = '' } = data;
    cancelActions.cancelBooking(params.bookingHash, bookingDetail.booking_id, reason, message);
  };

  render() {
    const { otherReason } = this.state;
    const { bookingDetail } = this.props;
    const {
      guest_name: guestName,
      hotel_name: hotelName,
      hotel_address: hotelAddress,
      checkin_date: checkIn,
      checkout_date: checkOut,
      reasons: options,
      pay_at_hotel: pah,
    } = bookingDetail;

    return (
      <div>
        <div className="text-1">Booked by {guestName}</div>
        <div className="bhcp__booking-details">
          <div className="bhcp__booking-details__hotel-name heading-1">
            {hotelName}
          </div>
          <div className="bhcp__booking-details__hotel-address text-2">
            {hotelAddress}
          </div>
          <div className="bhcp__booking-details__stay-date text-2">
            Stay dates : {checkIn} - {checkOut}
          </div>
          <div className="bhcp__booking-details__pay-mode text-3">
            {pah ? 'PAY AT HOTEL' : 'PREPAID'}
          </div>
        </div>

        <div className="bhcp__reasons">
          <div className="bhcp__reasons__section text-2">
            <span className="subheading-4">Please select a reason for cancellation:</span>
          </div>

          <Formsy className="bhcp__reasons__list" onValidSubmit={this.cancel} onChange={this.onChange} >
            <RadioGroup
              containerClassName="bhcp__reasons__input"
              options={options}
              name="reason"
              required
            />
            {
              otherReason ? (
                <FormsyInput
                  inputClassName="bhcp__reasons__message"
                  name="message"
                  label="Reason"
                  required={otherReason}
                  placeholder="Please mention your reason here"
                />
              ) : (null)
            }
            <div className="bhcp__reasons__btn row row--middle">
              <button type="submit" className="button" onClick={() => analyticsService.cancelBookingClicked('cancel', bookingDetail)}>Cancel Booking</button>
              <span className="bhcp__policy text-1">Know more about our <a href="https://www.treebo.com/faq/#Policies" className="text-link" target="_blank" rel="noopener noreferrer">Cancellation Policy</a></span>
            </div>
          </Formsy>
        </div>
      </div>
    );
  }
}

CancellationForm.propTypes = {
  cancelActions: PropTypes.object.isRequired,
  bookingDetail: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
};

export default CancellationForm;
