import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { pluralize, constants } from '../../../services/utils';

const formatDate = (date) => moment(date).format(constants.dateFormat.history).substring(4);
const BookingHistoryItem = ({ booking }) => (
  <div className="row bhi">
    <div className="col-1 row--middle">
      <i className="icon-location bhi__location" />
    </div>
    <div className="col-3">
      <p className="subheading-2">{booking.hotel.city}</p>
      <p>
        {formatDate(booking.dates.checkIn)} <i className="icon-arrow-right" />
        {` ${formatDate(booking.dates.checkOut)}`}
      </p>
    </div>
    <div className="col-6 bhi__hotel">
      <p className="subheading-2">{booking.hotel.name}</p>
      <p>Booking ID: {booking.id}</p>
      <p>
        {
          `${booking.roomConfig.count} Room (${booking.roomConfig.type}), ${booking.roomConfig.adults}
             ${pluralize(booking.roomConfig.adults, 'Adult')}`
        }
        {
          booking.roomConfig.kids ? (
            `, ${booking.roomConfig.kids}
              ${pluralize(booking.roomConfig.kids, 'child')}`
          ) : null
        }
      </p>
      <p>&#8377;
        {
          `${booking.price.total} (${booking.isPayAtHotel ? 'To be paid at Hotel' : 'Already paid'})`
        }
      </p>
    </div>
    <div className="col-2">
      <p className={`bhi__${booking.bookingStatus} subheading-2`}>
        {`${booking.bookingStatus[0].toUpperCase()}${booking.bookingStatus.substring(1)}`}
      </p>
      <p>
        {
          booking.bookingStatus === 'temporary' ? (
            <button className="button">
              <a href={booking.partPayLink}>
                PAY &#8377;{booking.partPayAmount} NOW
              </a>
            </button>
          ) : null
        }
      </p>
      <Link to={`/account/booking-history/${booking.id}/`} className="more-link">
        View Details
      </Link>
    </div>
  </div>
);

BookingHistoryItem.propTypes = {
  booking: PropTypes.object.isRequired,
};

export default BookingHistoryItem;
