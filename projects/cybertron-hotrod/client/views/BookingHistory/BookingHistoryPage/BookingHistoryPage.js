import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as userActionCreators from '../../../services/user/userDuck';
import * as contentActionCreators from '../../../services/content/contentDuck';
import BookingHistoryListItem from './BookingHistoryListItem';
import './bookingHistoryPage.css';

class BookingHistoryPage extends React.Component {
   static componentWillServerRender = ({ store: { dispatch }, match, route }) =>
     dispatch(contentActionCreators.getSeoContent(route, match.params));

   componentDidMount() {
     const { userActions, contentActions, route, match } = this.props;
     userActions.getBookingHistory();
     contentActions.getSeoContent(route, match.params);
   }

   render() {
     const {
       bookingHistory: {
         results,
         pastBookings,
         upcomingBookings,
       },
     } = this.props;
     return (
       <div className="bhp">
         <div className="container bhp__container">
           <p className="heading-1">BOOKING HISTORY</p>
           {
            upcomingBookings && upcomingBookings.length ? (
              <div>
                <p className="subheading-1 bhp__heading">
                  UPCOMING STAYS
                </p>
                {
                  upcomingBookings.map((id) =>
                    <BookingHistoryListItem booking={results[id]} key={id} />)
                }
              </div>
            ) : null
          }
           {
            pastBookings && pastBookings.length ? (
              <div>
                <p className="subheading-1 bhp__heading">
                  PREVIOUS STAYS
                </p>
                {
                  pastBookings.map((id) =>
                    <BookingHistoryListItem booking={results[id]} key={id} />)
                }
              </div>
            ) : null
          }
         </div>
       </div>
     );
   }
}

BookingHistoryPage.propTypes = {
  bookingHistory: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  bookingHistory: state.user.bookingHistory,
});

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(BookingHistoryPage);
