import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

const BookingHistoryPage = Loadable({
  loader: () => {
    importCss('BookingHistoryPage');
    return import('./BookingHistoryPage' /* webpackChunkName: 'BookingHistoryPage' */);
  },
  loading: () => null,
});

export default BookingHistoryPage;
