import React from 'react';
import PropTypes from 'prop-types';
import PercentageBar from '../../../components/PercentageBar/PercentageBar';
import './overallRatings.css';

const OverallRatings = ({
  overallRating: {
    image,
    rating: overallRating,
    rating_percent: reviewRatingPercent,
  },
}) => (
  <div className="overall-ratings">
    <div className="overall-ratings__header subheading-4">OVERALL RATINGS</div>
    <div className="row">
      <div className="col-6">
        <div className="overall-ratings__rating">
          <span className="overall-ratings__rating--bold">
            {overallRating}
          </span> / 5
        </div>
        <img src={image} alt={`TripAdvisor Overall Rating: ${overallRating}`} />
      </div>
      <div className="col-6">
        <ul>
          {
            reviewRatingPercent.map(({ rating, percent, review_str: reviewStr }) => (
              <li className="overall-ratings__rating-item text-2 row row--middle" key={rating}>
                <div className="col-1">{rating}</div>
                <div className="col-6">
                  <PercentageBar percentage={percent} />
                </div>
                <div className="col-5">{reviewStr}</div>
              </li>
            ))
          }
        </ul>
      </div>
    </div>
  </div>
);

OverallRatings.propTypes = {
  overallRating: PropTypes.object.isRequired,
};

export default OverallRatings;
