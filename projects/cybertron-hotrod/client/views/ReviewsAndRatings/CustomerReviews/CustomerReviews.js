import React from 'react';
import PropTypes from 'prop-types';
import reviewService from '../../../services/review/reviewService';
import './customerReviews.css';

const CustomerReview = ({
  reviews,
}) => {
  const reviewsToDisplay = reviewService.getTAReviews(reviews);
  return (
    <div>
      {
        reviewsToDisplay.map(({ user, review_detail: review }) => (
          <div className="customer-review">
            <div className="row">
              <div className="col-2 text--center">
                <img src={user.image_url} className="customer-review__image" alt="TripAdvisor Guest" />
                <div className="text-2 customer-review__name">
                  {user.user_name}
                </div>
                <div className="customer-review__date">
                  {user.review_date}
                </div>
              </div>
              <div className="col-10">
                <div className="customer-review__detail">
                  <h3 className="customer-review__header subheading-4">
                    {review.title}
                  </h3>
                  <div className="customer-review__rating-img">
                    <img src={review.image} alt={`TripAdvisor Rating ${review.user_rating}`} />
                  </div>
                  <div className="customer-review__review text-3">
                    {review.text}
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))
      }
    </div>
  );
};

CustomerReview.propTypes = {
  reviews: PropTypes.array.isRequired,
};

export default CustomerReview;
