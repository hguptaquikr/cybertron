import React from 'react';
import PropTypes from 'prop-types';
import './amenityRatings.css';

const AmenitiesRatings = ({
  subRatings,
}) => (
  <div className="amenities-ratings">
    <div className="amenities-ratings__header subheading-4">AMENITIES RATINGS</div>
    <ul>
      {
        subRatings.map(({ name, image }) => (
          <li key={name} className="amenities-ratings__item row row--middle">
            <div className="col-6">{name}</div>
            <div className="col-6"><img src={image} alt="Amenities Rating" /></div>
          </li>
        ))
      }
    </ul>
  </div>
);

AmenitiesRatings.propTypes = {
  subRatings: PropTypes.array.isRequired,
};

export default AmenitiesRatings;
