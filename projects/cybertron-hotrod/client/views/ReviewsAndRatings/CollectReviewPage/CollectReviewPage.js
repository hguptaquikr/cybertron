import React, { Component } from 'react';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import qs from 'query-string';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Flex from 'leaf-ui/cjs/Flex/web';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import Card from 'leaf-ui/cjs/Card/web';
import View from 'leaf-ui/cjs/View/web';
import Size from 'leaf-ui/cjs/Size/web';
import * as contentActionCreators from '../../../services/content/contentDuck';
import ArrowDown from '../../Feedback/ArrowDown/ArrowDown';
import './collectReviewPage.css';

class CollectReviewPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match, req }) => {
    const promises = [];

    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));

    promises.push(dispatch(contentActionCreators.getFeedbackTags(req.query.feedbackId)));

    return Promise.all(promises);
  }

  componentDidMount() {
    const {
      content: { feedback },
      contentActions,
      location,
      match,
      route,
    } = this.props;

    const query = qs.parse(location.search);
    contentActions.getSeoContent(route, match.params);
    contentActions.getFeedbackTags(query.feedbackId);
    const overallFeedback = {
      ...feedback,
      source: query.source,
      feedback_flow: {
        ...feedback.feedback_flow,
        value: query.flow,
      },
    };
    if (query.source && query.flow) {
      contentActions.submitFeedback(overallFeedback);
    }
  }
  render() {
    const {
      location,
      content: { feedback },
    } = this.props;

    const query = qs.parse(location.search);
    let taWidgetUrl;
    if (query.flow === 'avg' || query.flow === 'good') {
      taWidgetUrl = decodeURIComponent(feedback.ta_url);
    } else if (query.flow === 'amazing') {
      taWidgetUrl = decodeURIComponent(query.taWidgetUrl);
    } else {
      const [taWidgetPathname, taWidgetQueryString] = query.taWidgetUrl.split('?');
      const taWidgetQuery = qs.parse(taWidgetQueryString);
      taWidgetQuery.overallRating = query.overallRating || 5;
      taWidgetUrl = `${taWidgetPathname}?${qs.stringify(taWidgetQuery)}`;
    }

    return (
      <View className="container">
        <Space margin={[2, 'auto']}>
          <Card backgroundColor="greyLight">
            {
              query.flow ? (
                <Space margin={[0, 'auto']} padding={[5, 0]}>
                  <Size width="44%">
                    <Flex
                      flexDirection="row"
                      justifyContent="space-around"
                    >
                      <View>
                        <Flex>
                          <View>
                            <img src={`http://images.treebohotels.com/images/Email/${query.flow}.png`} alt="Awesome Experience" />
                            <Space margin={[1, 'auto']}>
                              <Link to={`/feedback/${query.feedbackId}/change/?source=${feedback.source}`}>
                                <Text size="s" color="blue">Change</Text>
                              </Link>
                            </Space>
                          </View>
                        </Flex>
                        <Space margin={[0, 0, 0, 4]}>
                          <Flex justifyContent="center">
                            <View>
                              <Text
                                size="m"
                                color="greyDarker"
                              >
                                Hi {feedback.user.name},
                              </Text>
                              <Text
                                size="xl"
                                color="greyDarker"
                              >
                                {
                                  query.flow === 'avg' || query.flow === 'good' ?
                                    'We are sorry we couldn’t deliver our best experience.'
                                  :
                                    'We are very humbled by your review.'
                                }
                              </Text>
                            </View>
                          </Flex>
                        </Space>
                      </View>
                    </Flex>
                  </Size>
                </Space>
              ) : null
            }
            <View className="reviews-section">
              {
                query.flow ? (
                  <Card
                    backgroundColor="white"
                    style={{ position: 'relative' }}
                  >
                    <ArrowDown />
                    <Space padding={[7, 0, 2]}>
                      <Flex
                        justifyContent="space-around"
                        alignItems="center"
                      >
                        <View>
                          <Text
                            size="xxxl"
                            weight="bold"
                            color="greyDarker"
                          >
                            How about writing a review, then ?
                          </Text>
                          <Text
                            size="s"
                            color="greyDark"
                          >
                            Please take a minute to review us on TripAdvisor.
                          </Text>
                        </View>
                      </Flex>
                    </Space>
                  </Card>
                ) : (
                  <View>
                    <View className="reviews-section__title subheading-1">
                      Tripadvisor Review
                    </View>
                    <View className="reviews-section__sub-title">
                      It just takes 2 mins to complete
                    </View>
                  </View>
                )
              }
              <View className="reviews-section__form">
                <View>
                  <iframe
                    title="reviews"
                    className="reviews-section__review-frame"
                    src={taWidgetUrl}
                    frameBorder="0"
                    allowFullScreen
                  />
                </View>
              </View>
              <View className="reviews__bottom-img">
                <img
                  src="https://images.treebohotels.com/images/review-bottom.png"
                  alt="Treebo Hotel Review"
                />
              </View>
            </View>
          </Card>
        </Space>
      </View>
    );
  }
}

CollectReviewPage.propTypes = {
  content: PropTypes.object,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(CollectReviewPage);
