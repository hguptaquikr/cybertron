import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

const CollectReviewPage = Loadable({
  loader: () => {
    importCss('CollectReviewPage');
    return import('./CollectReviewPage' /* webpackChunkName: 'CollectReviewPage' */);
  },
  loading: () => null,
});

export default CollectReviewPage;
