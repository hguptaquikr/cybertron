import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

const ReviewSubmittedPage = Loadable({
  loader: () => {
    importCss('ReviewSubmittedPage');
    return import('./ReviewSubmittedPage' /* webpackChunkName: 'ReviewSubmittedPage' */);
  },
  loading: () => null,
});

export default ReviewSubmittedPage;
