import React from 'react';
import PropTypes from 'prop-types';
import qs from 'query-string';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as contentActionCreators from '../../../services/content/contentDuck';
import reviewService from '../../../services/review/reviewService';
import './reviewSubmittedPage.css';

class ReviewSubmittedPage extends React.Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match, req }) => {
    const promises = [];

    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));

    if (!req.query.feedbackId) {
      promises.push(contentActionCreators.getFeedbackTags(match.params.feedbackId));
    }

    return Promise.all(promises);
  }

  componentDidMount() {
    const { contentActions, route, match, content: { feedback } } = this.props;
    contentActions.getSeoContent(route, match.params);
    contentActions.getFeedbackTags(match.params.feedbackId);
    if (window.parent !== window) {
      const query = qs.parse(window.parent.location.search);
      if (query.bookingId) {
        reviewService.updateSubmitReviewStatus(query.bookingId);
      }
      if (query.feedbackId) {
        const completeFeedback = {
          ...feedback,
          state: 'COMPLETED',
        };
        contentActions.submitFeedback(completeFeedback);
      }
    }
  }

  render() {
    return (
      <div className="container thank-you-page">
        <div className="thank-you-page__content">
          <img src="https://images.treebohotels.com/images/thankyou-2x.png?w=230&h=125&fm=pjpg&auto=compress" alt="thanks" />
          <p className="thank-you-page__content__title text-1">
            Thank you!
          </p>
          <p>
            Thank you for sharing your experience! It was our pleasure hosting you and
            we look forward to the same opportunity again.
          </p>
        </div>
        <div>
          <img
            className="thank-you-page__bottom-img"
            src="https://images.treebohotels.com/images/review-bottom.png"
            alt="Treebo Hotel Review"
          />
        </div>
      </div>
    );
  }
}

ReviewSubmittedPage.propTypes = {
  contentActions: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(ReviewSubmittedPage);
