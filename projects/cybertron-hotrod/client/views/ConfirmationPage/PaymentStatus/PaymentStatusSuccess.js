import React from 'react';
import Text from 'leaf-ui/cjs/Text/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import Space from 'leaf-ui/cjs/Space/web';
import Card from 'leaf-ui/cjs/Card/web';
import Image from 'leaf-ui/cjs/Image/web';
import View from 'leaf-ui/cjs/View/web';

const PaymentStatusSuccess = () => (
  <Space
    padding={[2]}
    margin={[2, 0, 0]}
    style={{ border: 'solid 1px #6ed396' }}
  >
    <Card backgroundColor="primaryLighter">
      <Flex
        flexDirection="row"
        alignItems="center"
      >
        <View>
          <Image
            src="http://images.treebohotels.com/images/computer.svg"
            alt="Treebo"
            height="16px"
            width="16px"
          />
          <Space margin={[0, 1, 0]}>
            <Text
              color="greyDarker"
              size="s"
            >
            Payment Successful
            </Text>
          </Space>
        </View>
      </Flex>
    </Card>
  </Space>
);

export default PaymentStatusSuccess;
