import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

const ConfirmationPage = Loadable({
  loader: () => {
    importCss('ConfirmationPage');
    return import('./ConfirmationPage' /* webpackChunkName: 'ConfirmationPage' */);
  },
  loading: () => null,
});

export default ConfirmationPage;
