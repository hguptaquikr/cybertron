import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import Image from 'leaf-ui/cjs/Image/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';
import * as confirmationActionCreators from '../../services/booking/bookingDuck';
import * as contentActionCreators from '../../services/content/contentDuck';
import Policies from '../../components/Policies/Policies';
import Cart from '../Cart/Cart';
import StaticMap from '../../components/StaticMap/StaticMap';
import WalletBanner from '../../components/WalletBanner/WalletBanner';
import NewYearRateTag from '../../components/NewYearRateTag/NewYearRateTag';
import * as hotelService from '../../services/hotel/hotelService';
import PaymentStatusSuccess from './PaymentStatus/PaymentStatusSuccess';
import './confirmationPage.css';

class ConfirmationPage extends React.Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) =>
    dispatch(contentActionCreators.getSeoContent(route, match.params));

  componentDidMount() {
    const {
      match,
      contentActions,
      confirmationActions,
      route,
    } = this.props;
    contentActions.getSeoContent(route, match.params);
    confirmationActions.getConfirmationContent(match.params.bookingId);
  }

  render() {
    const {
      confirmation: {
        order_id: orderId,
        user: { name, email, contact_number: guestContactNumber },
        hotel,
        hotel: { coordinates, directions },
        date,
        room,
        price,
        ratePlan,
        pay_at_hotel: payAtHotel,
        partial_payment_link: partPayLink,
        partial_payment_amount: partPayAmount,
        partial_payment_message: partPayMessage,
      },
      wallet,
      user,
    } = this.props;
    return (
      <View className="container confirmation-page">
        <View className="row">
          <View className="col-8">
            <View className="confirmation-page__booking-info cp">
              <View className="confirmation-page__header confirmation-page__item">
                <Flex flexDirection="row">
                  <View>
                    <View className="confirmation-page__header__text cph">
                      <View className="cph__head heading-1">CONGRATULATIONS!</View>
                      {
                      partPayLink ? (
                        <View>
                          <View className="text-1 cph__subhead">Your booking is temporarily confirmed.</View>
                          <View className="text-2 cph__subhead-info">
                            You will receive a booking email from us shortly.
                            Please note that this is temporary
                            and can be confirmed by making a partial payment.
                          </View>
                        </View>
                    ) : (
                      <View>
                        <View className="text-1 cph__subhead">Your booking is confirmed.</View>
                        <View className="text-2 cph__subhead-info">
                          You will receive booking confirmation email shortly.
                          Wish you a happy stay!
                        </View>
                      </View>
                    )
                  }
                    </View>
                    <View className="confirmation-page__bookin-id">
                      <View className="confirmation-page__bookin-id__title">Booking Id</View>
                      <View className="confirmation-page__bookin-id__bid" id="t-confirmationOrderId">{orderId}</View>
                    </View>
                  </View>
                </Flex>
                <PaymentStatusSuccess />
              </View>
              <View className="confirmation-page__guests confirmation-page__item">
                <View className="confirmation-page__guests__title heading-1">GUEST DETAILS:</View>
                <View className="confirmation-page__guests__info guest-info">
                  <View className="guest-info">
                    <View className="guest-info__item guest-info__name" id="t-confirmationGuestName">{name}</View>
                    <View className="guest-info__item" id="t-confirmationGuestEmail">{email}</View>
                    <View className="guest-info__item" id="t-confirmationGuestNumber">{guestContactNumber}</View>
                    <View className="guest-info__item guest-info__confirmation">
                      Confirmation Sent
                    </View>
                  </View>
                </View>
              </View>

              {
                partPayLink ? (
                  <View className="confirmation-page__item confirmation-page__item--gray">
                    <View className="row row--middle">
                      <View className="col-8 text-2 confirmation-page__partpay__text">
                        {partPayMessage}
                      </View>
                      <a href={partPayLink} className="col-offset-1 col-3" target="_blank" rel="noopener noreferrer">
                        <button className="button confirmation-page__partpay__button" id="t-confirmationPartPay">
                            Pay &#x20b9;{partPayAmount} now
                        </button>
                      </a>
                    </View>
                  </View>
                ) : null
              }
              {
                <View className="confirmation-page__item confirmation-page__item--gray confirmation-page__wallet-banner">
                  <WalletBanner
                    guestContactNumber={guestContactNumber}
                    src="confirmation"
                  />
                </View>
              }
              {
                !user.isAuthenticated ? (
                  <View className="confirmation-page__item--gray confirmation-page__login" id="t-confirmationLoginBanner">
                    <View className="confirmation-page__login__banner">
                      <View className="row">
                        <View className="col-3 text--center">
                          <Image
                            src="https://images.treebohotels.com/images/hotrod/illustration.svg"
                            alt="login"
                          />
                        </View>
                        <View className="col-6">
                          <View className="confirmation-page__login__banner--benefits">
                            LOGIN TO TREEBO
                            <ul>
                              <li>Manage your bookings on the go</li>
                              <li>View your Treebo Points</li>
                            </ul>
                          </View>
                        </View>
                        <View className="col-3 confirmation-page__login__banner--cta">
                          <Link
                            to="/login/"
                            className="button  confirmation-page__login__banner--cta--button"
                          >
                            LOGIN NOW
                          </Link>
                        </View>
                      </View>
                    </View>
                  </View>
                ) : null
              }
              {
                !isEmpty(hotel.policies) ? (
                  <View className="confirmation-page__policy confirmation-page__item">
                    <View className="confirmation-page__policy__head heading-1">POLICIES:</View>
                    <View className="confirmation-page__policy__info">
                      <Policies policies={hotel.policies} />
                    </View>
                    <View className="confirmation-page__item confirmation-page__policy__more">
                      <a className="more-link" href="/faq/#important-policies/" id="t-confirmationPoliciesMore">KNOW MORE</a>
                    </View>
                  </View>
                ) : (null)
              }
            </View>
            <NewYearRateTag hotelId={hotel.id} />
            <View className="confirmation-page__hotel-location cp-hl">
              <View className="confirmation-page__item confirmation-page__item--with-link">
                <View className="heading-1">GET DIRECTIONS</View>
                <a
                  className="more-link"
                  id="t-confirmationMap"
                  href={`https://www.google.com/maps/search/?api=1&query=${coordinates.lat},${coordinates.lng}`}
                >LOCATE ON GOOGLE MAPS
                </a>
              </View>
              <View className="confirmation-page__item">
                <View className="cp-hl__hotel-name">{hotel.name}</View>
                <View className="cp-hl__hotel-address">
                  {hotelService.constructAddress(hotel)}
                </View>
              </View>
              {
                directions.length ? (
                  <View className="cp-hl__landmark confirmation-page__item">
                    <View className="cp-hl__landmark__title">
                      Directions from the nearest landmark
                    </View>
                    {
                      directions.map((direction) => (
                        <View key={direction.landmark.name} className="cp-hl__landmark__block">
                          <View className="cp-hl__landmark__name">
                            From {direction.landmark.name}
                          </View>
                          <ul className="cp-hl__landmark__directions">
                            {
                              direction.steps.map((step) => (
                                <li
                                  key={step}
                                  className="cp-hl__landmark__directions__item"
                                >{step}
                                </li>
                              ))
                            }
                          </ul>
                        </View>
                      ))
                    }
                  </View>
                ) : (null)
              }
              <View className="cp-hl__landmark__map">
                <StaticMap coordinates={coordinates} width={680} height={376} />
              </View>
            </View>
          </View>
          <View className="col-4">
            <View className="confirmation-page__cart">
              <Cart
                cart={{ hotel, price, date, room }}
                selectedRatePlan={ratePlan.code}
                showApplyCoupon={false}
                showTreeboPointsMessage={false}
                showApplyTreeboPoints={false}
                wallet={wallet}
                showViewBreakup={false}
                totalPaid={!payAtHotel}
              />
            </View>

          </View>
        </View>
      </View>
    );
  }
}

ConfirmationPage.propTypes = {
  confirmation: PropTypes.object.isRequired,
  confirmationActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  wallet: PropTypes.object,
  user: PropTypes.object,
};

const mapStateToProps = (state) => ({
  confirmation: state.confirmation,
  wallet: state.wallet,
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  confirmationActions: bindActionCreators(confirmationActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(ConfirmationPage);
