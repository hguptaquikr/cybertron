import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import Coupon from '../../components/Coupon/Coupon';
import analyticsService from '../../services/analytics/analyticsService';
import * as abService from '../../services/ab/abService';

class ApplyCoupon extends React.Component {
  state = {
    isInputVisible: false,
  }

  onApplyCoupon = (couponCode) => {
    const { applyCoupon, selectedRatePlan, payment, itineraryActions } = this.props;
    if (payment.order_id) {
      return itineraryActions.getCartContent(null, selectedRatePlan)
        .then(() => applyCoupon(couponCode))
        .then(this.toggleApplyCoupon);
    }
    return applyCoupon(couponCode, selectedRatePlan)
      .then(this.toggleApplyCoupon)
      .catch(() => { /* do error handling here */ });
  }

  onRemoveCoupon = (e) => {
    const { removeCoupon, selectedRatePlan, payment, itineraryActions } = this.props;
    e.preventDefault();
    if (payment.order_id) {
      return itineraryActions.getCartContent(null, selectedRatePlan)
        .then(() => removeCoupon());
    }
    return removeCoupon();
  }

  toggleApplyCoupon = () => {
    this.setState({ isInputVisible: !this.state.isInputVisible });
  }

  showCoupon = () => {
    analyticsService.applyCouponLinkClicked();
    this.toggleApplyCoupon();
  }

  render() {
    const { isInputVisible } = this.state;
    const {
      couponApplied,
      couponError,
      isCouponLoading,
      couponCode,
      coupons,
    } = this.props;

    const callout = couponApplied ? (
      <div>
        <div className="apply-coupon__placeholder">
          <span>&#39;<b className="apply-coupon__placeholder__coupon">{couponCode.toUpperCase()}&#39;</b> applied.
            <span className="text-link" id="t-removeCoupon" onClick={this.onRemoveCoupon}>Remove</span>
          </span>
        </div>
      </div>
    ) : (
      <div className="apply-coupon__placeholder">
        <span>Have a coupon code? </span>
        <span className="text-link" id="t-viewCoupons" onClick={this.showCoupon}>
          {get(abService.getExperiments(), 'checkoutCouponDesktop.payload.applyCouponText', 'View Available Coupons')}
        </span>
      </div>
    );

    return (
      <div className="apply-coupon">
        {callout}
        <Coupon
          isOpen={isInputVisible}
          onSubmit={this.onApplyCoupon}
          isCouponLoading={isCouponLoading}
          couponCode={couponCode}
          couponError={couponError}
          onClose={this.toggleApplyCoupon}
          coupons={coupons}
        />
      </div>
    );
  }
}

ApplyCoupon.propTypes = {
  couponCode: PropTypes.string,
  selectedRatePlan: PropTypes.string,
  applyCoupon: PropTypes.func.isRequired,
  removeCoupon: PropTypes.func.isRequired,
  couponApplied: PropTypes.bool.isRequired,
  couponError: PropTypes.string,
  isCouponLoading: PropTypes.bool.isRequired,
  coupons: PropTypes.array,
  itineraryActions: PropTypes.object.isRequired,
  payment: PropTypes.object.isRequired,
};

export default ApplyCoupon;
