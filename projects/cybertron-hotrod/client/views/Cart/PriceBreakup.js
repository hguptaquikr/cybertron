import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { pluralize } from '../../services/utils';
import searchService from '../../services/search/searchService';

const PriceBreakup = ({
  nightsBreakup,
  allNights,
  wallet,
  search: { datePicker },
}) => {
  const { numberOfNights } = searchService.formattedDatePicker(datePicker.range, 'D MMM');
  const showDiscount = !!allNights.discountAmount;

  return (
    <div className="cart-breakup">
      <div className="cart-breakup__header heading-1">DAILY PRICE BREAKUP</div>
      <div className="cart-breakup__body">
        <table className="cart-breakup__prices">
          <thead>
            <tr>
              <th className="cart-breakup__prices-head">DATE</th>
              <th className="cart-breakup__prices-head">BASE PRICE</th>
              <th className="cart-breakup__prices-head">TAX</th>
              {
                showDiscount ? (
                  <th className="cart-breakup__prices-head">SAVINGS</th>
                ) : (null)
              }
              <th className="cart-breakup__prices-head">FINAL PRICE</th>
            </tr>
          </thead>
          <tbody>
            {
              nightsBreakup.map(({
                date,
                pretaxPrice,
                tax,
                totalDiscount,
                sellingPrice,
              }) => (
                <tr key={date}>
                  <td className="cart-breakup__prices-body">{date}</td>
                  <td className="cart-breakup__prices-body">&#x20b9;{pretaxPrice}</td>
                  <td className="cart-breakup__prices-body">&#x20b9;{tax}</td>
                  {
                    showDiscount ? (
                      <td className="cart-breakup__prices-body">- &#x20b9;{totalDiscount}</td>
                    ) : (null)
                  }
                  <td className="cart-breakup__prices-body">&#x20b9;{sellingPrice}</td>
                </tr>
              ))
            }
          </tbody>
        </table>
        <div className="row">
          <div className="col-1" />
          <div className="col-10">
            <div className="cart-breakup__final">
              <div>Total Cost for {allNights.nights} {pluralize(allNights.nights, 'Night')}</div>
              <div className="cart__bill__price">&#x20b9;{allNights.pretaxPrice}</div>
            </div>
            <div className="cart-breakup__final">
              <div>Taxes</div>
              <div className="cart__bill__price">&#x20b9;{allNights.tax}</div>
            </div>
            <div className="cart-breakup__final">
              {
                allNights.memberDiscount ? (
                  <div className="cart__bill">
                    <div className="row cart__bill__sd">
                      Member Rate
                    </div>
                    <div className="cart__bill__price--discount">- &#x20b9;{allNights.memberDiscount}</div>
                  </div>
                ) : null
              }
            </div>
            {
              allNights.couponDiscount ? (
                <div className="cart__bill cart-breakup__final">
                  <div>
                    Savings
                  </div>
                  <div className="cart__bill__price--discount">
                    - &#x20b9;{allNights.couponDiscount}
                  </div>
                </div>
              ) : null
            }
            {
              wallet.byType.TP.isApplied && wallet.byType.TP.usableBalance ? (
                <div className="cart-breakup__final cart__bill__tariff">
                  <div className="subheading-4 cart__bill__tariff__text">Total Tariff</div>
                  <div> &#x20b9;{allNights.totalPrice}</div>
                </div>
              ) : (null)
            }
            {
              wallet.byType.TP.isApplied && wallet.byType.TP.usableBalance ? (
                <div className="cart-breakup__final cart__bill__treebo-points">
                  <div className="subheading-4 cart__bill__treebo-points--text">Treebo Points Applied</div>
                  <div className="cart__bill__price--discount">- &#x20b9;{wallet.byType.TP.usableBalance}</div>
                </div>
              ) : (null)
            }
            <div className="cart-breakup__final cart-breakup__final__total">
              <div className="cart-breakup__final-text">TOTAL PAYABALE</div>
              <div className="cart-breakup__final-price">
                &#x20b9;{allNights.sellingPrice && Math.round(allNights.sellingPrice)}
                <div className="cart-breakup__final-incl">(Tax incl. price for {numberOfNights.text})</div>
              </div>
            </div>
          </div>
          <div className="col-1" />
        </div>
      </div>
    </div>
  );
};

PriceBreakup.propTypes = {
  nightsBreakup: PropTypes.array,
  allNights: PropTypes.object.isRequired,
  wallet: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

export default connect(mapStateToProps)(PriceBreakup);
