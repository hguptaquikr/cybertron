import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import moment from 'moment';
import qs from 'query-string';
import Modal from '../../components/Modal/Modal';
import Loading from '../../components/Loading/Loading';
import PriceBreakup from './PriceBreakup';
import ApplyCoupon from './ApplyCoupon';
import RatePlanTag from '../../components/RatePlanTag/RatePlanTag';
import * as hotelService from '../../services/hotel/hotelService';
import { pluralize, constants } from '../../services/utils';
import analyticsService from '../../services/analytics/analyticsService';
import './cart.css';

class Cart extends React.Component {
  state = {
    showPriceBreakup: false,
  };

  toggleBreakup = () => {
    this.setState({ showPriceBreakup: !this.state.showPriceBreakup });
  }

  refreshBookingId = async () => {
    const { payment, itineraryActions: { getCartContent }, selectedRatePlan } = this.props;
    const isOrderIdFetched = payment.order_id;
    if (isOrderIdFetched) await getCartContent(null, selectedRatePlan);
  }

  toggleWallet = async () => {
    const {
      wallet: {
        byType: { TP: treeboPoints },
      },
      walletActions,
      selectedRatePlan,
    } = this.props;
    await this.refreshBookingId();
    if (treeboPoints.isApplied) {
      walletActions.removeWallet(selectedRatePlan);
      analyticsService.useTreeboPoints(false);
    } else {
      walletActions.applyWallet(selectedRatePlan);
      analyticsService.useTreeboPoints(true);
    }
  }

  render() {
    const { showPriceBreakup } = this.state;
    const {
      cart: {
        hotel,
        date: {
          checkin: checkinDate,
          checkout: checkoutDate,
        },
        room,
        price: {
          ratePlans = {},
          discount_error: discountError,
        },
        coupon_code: couponCode,
        coupons,
        isCouponLoading,
      },
      payment,
      wallet,
      itineraryActions,
      showApplyCoupon,
      showViewBreakup,
      totalPaid,
      selectedRatePlan,
      showTreeboPointsMessage,
      showApplyTreeboPoints,
      location,
    } = this.props;

    const {
      pretaxPrice,
      finalPretaxPrice,
      tax,
      type,
      sellingPrice,
      nightsBreakup,
      memberDiscount,
      couponDiscount,
      totalPrice,
    } = ratePlans[selectedRatePlan] || {};

    const hotelImageUrl = hotel.imageUrl;
    let checkin = moment(checkinDate);
    let checkout = moment(checkoutDate);
    const nights = checkout.diff(checkin, 'days');
    checkin = checkin.format(constants.dateFormat.view);
    checkout = checkout.format(constants.dateFormat.view);
    const hotelDetailsTarget = hotelService.getHotelDetailsTarget({
      hotelName: hotel.name,
      hotelId: hotel.id,
      area: { city: hotel.city, locality: hotel.locality },
      roomType: room.type,
      locationQuery: qs.parse(location.search),
    });
    const treeboPoints = wallet.byType.TP;

    return (
      <div className="cart">
        {
          hotelImageUrl ? (
            <img
              className="cart__image"
              src={`${hotelImageUrl}?w=332&h=166&fm=pjpg&fit=crop`}
              alt="CartPic"
            />
          ) : <Loading height="16.6rem" />
        }
        <div className="cart__section">
          <Link to={hotelDetailsTarget}>
            <h2 className="subheading-1" id="t-cartHotelName">
              {hotel.name || <Loading width="20rem" marginBottom="0.8rem" />}
            </h2>
          </Link>
          <div className="cart__address">
            {
              hotel.street ? (
                hotelService.constructAddress(hotel)
              ) : <Loading width="16rem" marginBottom="2rem" />
            }
          </div>
          <div className="cart__dates">
            <div>
              <div className="subheading-1 cart__dates__title" id="t-cartCheckin">{checkin}</div>
              <div>Check In</div>
            </div>
            <i className="icon-arrow-right cart__dates__arrow" />
            <div>
              <div className="subheading-1 cart__dates__title" id="t-cartCheckout">{checkout}</div>
              <div>Check Out</div>
            </div>
            <div>
              <div className="subheading-1 cart__dates__title" id="t-cartNights">{nights}</div>
              <div>{pluralize(nights, 'Night')}</div>
            </div>
          </div>
        </div>
        <div className="cart__section">
          <div className="cart__rooms">
            <div className="subheading-1 cart__rooms__title" id="t-cartRoomCount">{room.count}</div>
            <div>{pluralize(room.count, 'Room')}</div>
          </div>
          <div className="cart__rooms">
            <div className="subheading-1 cart__rooms__title" id="t-cartRoomType">{room.type}</div>
            <div>Room Type</div>
          </div>
        </div>
        {
          treeboPoints.usableBalance && showApplyTreeboPoints ? (
            <div className="cart__section">
              <div className="">
                <label htmlFor="WALLET-PAY" className="subheading-4 cart__apply-wallet">
                  <input
                    id="WALLET_PAY"
                    type="checkbox"
                    checked={treeboPoints.isApplied}
                    onChange={this.toggleWallet}
                  />
                   Pay ₹{treeboPoints.usableBalance} using Treebo Points.
                </label>
              </div>
            </div>
          ) : null
        }
        <div className="cart__section cart__section__summary">
          <div className="cart__bill">
            <div>Total Cost for {nights} {pluralize(nights, 'Night')}</div>
            <div className="cart__bill__price" id="t-cartPretaxPrice">&#x20b9;{pretaxPrice}</div>
          </div>
          <div className="cart__bill">
            <div>Taxes</div>
            <div className="cart__bill__price" id="t-cartTaxes">&#x20b9;{tax}</div>
          </div>
          {
            memberDiscount ? (
              <div className="cart__bill">
                <div className="row cart__bill__sd">
                  Member Rate
                </div>
                <div className="cart__bill__price--discount" id="t-cartMemberRate">- &#x20b9;{memberDiscount}</div>
              </div>
            ) : null
          }
          <div className="cart__bill">
            {
              showApplyCoupon ? (
                <ApplyCoupon
                  couponCode={couponCode}
                  payment={payment}
                  itineraryActions={itineraryActions}
                  applyCoupon={itineraryActions.applyCoupon}
                  removeCoupon={itineraryActions.removeCoupon}
                  selectedRatePlan={selectedRatePlan}
                  couponApplied={!!couponDiscount}
                  couponError={discountError}
                  isCouponLoading={isCouponLoading}
                  coupons={coupons}
                />
              ) : (null)
            }
            {
              couponDiscount && showApplyCoupon ? (
                <div className="cart__bill__price--discount cart__bill__coupon-discount" id="t-cartCouponDiscount">- &#x20b9;{couponDiscount}</div>
              ) : null
            }
            {
              couponDiscount && !showApplyCoupon ? (
                <div className="cart__bill">
                  <div>Savings</div>
                  <div className="cart__bill__price--discount" id="t-confirmationCouponDiscount">- &#x20b9;{couponDiscount}</div>
                </div>
              ) : null
            }
          </div>
          {
            treeboPoints.isApplied && treeboPoints.usableBalance ? (
              <div className="cart__bill cart__bill__tariff">
                <div className="subheading-4 cart__bill__tariff__text">Total Tariff</div>
                <div id="t-cartTotalWithoutWallet"> &#x20b9;{totalPrice}</div>
              </div>
            ) : (null)
          }
          {
            treeboPoints.isApplied && treeboPoints.usableBalance ? (
              <div className="cart__bill cart__bill__treebo-points">
                <div className="subheading-4 cart__bill__treebo-points__text">Treebo Points Applied</div>
                <div className="cart__bill__price--discount" id="t-cartPointsApplied">- &#x20b9;{treeboPoints.usableBalance}</div>
              </div>
            ) : (null)
          }
        </div>
        <div className="cart__section">
          <div className="text-1 cart__total-text" id="t-cartTotalText">
            { totalPaid ? 'Total Paid' : 'Total Payable' }
            {
              showViewBreakup && (
                <div>
                  <div className="text-3 text-link" onClick={this.toggleBreakup}>
                    View Breakup
                  </div>
                  <Modal isOpen={showPriceBreakup} onClose={this.toggleBreakup}>
                    <PriceBreakup
                      nightsBreakup={nightsBreakup}
                      wallet={wallet}
                      allNights={{
                        nights,
                        finalPretaxPrice,
                        pretaxPrice,
                        tax,
                        couponDiscount,
                        sellingPrice,
                        totalPrice,
                        memberDiscount,
                      }}
                    />
                  </Modal>
                </div>
              )
            }
          </div>
          <div className="cart__total">
            <div className="subheading-1 cart__total-price" id="t-cartTotalWithWallet">
              &#x20b9;{sellingPrice && Math.round(sellingPrice)}
            </div>
            <RatePlanTag rateType={type} className="cart__rate-plan-tag" />
          </div>
        </div>
        {
          showTreeboPointsMessage ? (
            <div id="credit-points" className="cart__wallet-credit">
              Treebo Points will be credited to Primary
              Traveller post checkout.
              <Link
                id="terms"
                to="/faq/"
                target="_blank"
                className="text-link cart__wallet-credit__faq"
              >
                View T&Cs.
              </Link>
            </div>
          ) : null
        }
      </div>
    );
  }
}

Cart.propTypes = {
  cart: PropTypes.object.isRequired,
  payment: PropTypes.object.isRequired,
  wallet: PropTypes.object,
  showApplyCoupon: PropTypes.bool,
  showViewBreakup: PropTypes.bool,
  itineraryActions: PropTypes.object,
  totalPaid: PropTypes.bool,
  selectedRatePlan: PropTypes.string,
  walletActions: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  showTreeboPointsMessage: PropTypes.bool,
  showApplyTreeboPoints: PropTypes.bool,
};

Cart.defaultProps = {
  showApplyCoupon: true,
  showViewBreakup: true,
  showTreeboPointsMessage: true,
  showApplyTreeboPoints: true,
  totalPaid: false,
};

export default withRouter(Cart);
