import React from 'react';
import PropTypes from 'prop-types';
import Flex from 'leaf-ui/cjs/Flex/web';
import Text from 'leaf-ui/cjs/Text/web';
import Image from 'leaf-ui/cjs/Image/web';
import Button from 'leaf-ui/cjs/Button/web';
import Space from 'leaf-ui/cjs/Space/web';
import View from 'leaf-ui/cjs/View/web';
import RadioButton from 'leaf-ui/cjs/RadioButton/web';
import Price from 'leaf-ui/cjs/Price/web';

const PhonePe = ({
  onPaymentChange,
  ratePlan,
  toggleWallet,
  isPaymentDisabled,
  selectedWallet,
  promotionText,
}) => (
  <Space margin={[2, 2, 4]}>
    <Flex justifyContent="space-between">
      <View>
        <Flex
          flexDirection="row"
          alignItems="center"
        >
          <View>
            <RadioButton
              onClick={toggleWallet}
              checked={selectedWallet === 'PhonePe'}
              value="PhonePe"
              label=""
            />
            <Space margin={[0, 1, 0, 0]}>
              <Image alt="phonepe" src="https://images.treebohotels.com/images/PhonePe.svg" />
            </Space>
            <Text color="greyDarker" size="m">
              PhonePe BHIM UPI
            </Text>
          </View>
        </Flex>
        <Space margin={[1, 0, 0, 0]}>
          <Text
            color="grey"
            size="xs"
            component="p"
          >
            {promotionText}
          </Text>
        </Space>
        {
          selectedWallet === 'PhonePe' ? (
            <Space margin={[2, 0, 0]}>
              <Button
                onClick={onPaymentChange}
                disabled={isPaymentDisabled}
              >
                PROCEED TO PAY <Price>{ratePlan.sellingPrice}</Price>
              </Button>
            </Space>
          ) : null
        }
      </View>
    </Flex>
  </Space>
);

PhonePe.propTypes = {
  onPaymentChange: PropTypes.func,
  toggleWallet: PropTypes.func,
  isPaymentDisabled: PropTypes.bool,
  ratePlan: PropTypes.object,
  selectedWallet: PropTypes.string,
  promotionText: PropTypes.string,
};

export default PhonePe;
