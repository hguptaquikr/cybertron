import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

class Netbank extends React.Component {
  state = {
    value: '',
    showError: false,
  };

  handleFieldChange = (e) => {
    this.setState({
      value: e.target.value,
      showError: false,
    });
  };

  pay = (e) => {
    e.preventDefault();
    const { value } = this.state;

    if (!value) {
      this.setState({ showError: true });
    } else {
      this.props.makePayment({
        method: 'netbanking',
        bank: value,
      }, 'razorpay');
    }
  };

  render() {
    const { defaultBanks, banks, payEnabled, isProcessingPayment } = this.props;
    const { value, showError } = this.state;

    return (
      <form className="netbank">
        <ul className="netbank__default">
          {defaultBanks.map(({ key, value: val }) => (
            <li
              key={key}
              className={cx('netbank__default-item', {
                'netbank__default-item--active': value === key,
              })}
            >
              <label htmlFor={key} className="netbank__input-container">
                <input
                  id={key}
                  className="netbank__input"
                  type="radio"
                  name="bank"
                  value={key}
                  onChange={this.handleFieldChange}
                  checked={value === key}
                />
                <span
                  className={`netbank__display netbank__display--${val.toLowerCase()}`}
                />
              </label>
            </li>
          ))}
        </ul>
        <div className="netbank__list">
          <div className="select-box checkout__select">
            <select
              value={value}
              onChange={this.handleFieldChange}
              className="select netbank__list-select"
            >
              <option value="">OTHER BANKS</option>
              {
                banks.map(({ key, value: v }) => (<option key={key} value={key}>{v}</option>))
              }
            </select>
          </div>
          <p className="netbank__error text-error">{showError ? 'Please select a bank' : null}</p>
        </div>
        <div className="netbank__action">
          <button
            type="submit"
            id="t-payNow"
            name="button"
            onClick={this.pay}
            className={cx('checkout__action button', {
              'button--disabled': !payEnabled || isProcessingPayment,
            })}
          >
            Pay Now
          </button>
        </div>
      </form>
    );
  }
}

Netbank.propTypes = {
  makePayment: PropTypes.func.isRequired,
  defaultBanks: PropTypes.array.isRequired,
  banks: PropTypes.array.isRequired,
  payEnabled: PropTypes.bool,
  isProcessingPayment: PropTypes.bool.isRequired,
};

export default Netbank;
