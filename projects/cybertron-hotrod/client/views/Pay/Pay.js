/* eslint-disable react/no-find-dom-node */
import React from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import get from 'lodash/get';
import Space from 'leaf-ui/cjs/Space/web';
import Tag from 'leaf-ui/cjs/Tag/web';
import analyticsService from '../../services/analytics/analyticsService';
import * as abService from '../../services/ab/abService';

import CardPay from './CardPay';
import Netbank from './Netbank';
import Wallet from './Wallet';
import PayAtHotel from './PayAtHotel';

import './pay.css';

/* global Razorpay */
class Pay extends React.Component {
  state = {
    defaultBanks: [
      { key: 'SBIN', value: 'SBI' },
      { key: 'HDFC', value: 'HDFC' },
      { key: 'ICIC', value: 'ICICI' },
      { key: 'UTIB', value: 'Axis' },
      { key: 'KKBK', value: 'Kotak' },
      { key: 'CITI', value: 'Citibank' },
    ],
    banks: [],
    wallet: [
      'mobikwik',
      'phonePe',
    ],
    payEnabled: true,
    isProcessingPayment: false,
  };

  componentDidMount() {
    const { paymentGatewayConfig } = this.props;
    const s = document.createElement('script');
    s.setAttribute('src', 'https://checkout.razorpay.com/v1/razorpay.js');
    s.onload = () => {
      Razorpay.configure(paymentGatewayConfig);
      Razorpay.payment.getMethods(({ netbanking }) => {
        this.setState({
          banks: Object.keys(netbanking).map((key) => ({ key, value: netbanking[key] })),
          // wallet: Object.keys(wallet),
        });
      });
    };
    document.body.appendChild(s);
  }

  onPayNow = async (gateway) => {
    const {
      onPay,
      selectedRatePlan,
      itineraryActions,
      hidePaymentOption,
      refreshBookingId,
    } = this.props;
    await refreshBookingId();
    return onPay(selectedRatePlan, gateway)
      .then(() => {
        this.setState({ payEnabled: true });
      })
      .catch((res) => {
        if (res.code === '5006') {
          itineraryActions.getCartContent(null, selectedRatePlan);
          hidePaymentOption();
        }
      });
  }

  onPayAtHotel = () => {
    const { payAtHotel } = this.props;
    this.togglePayNowButton();
    payAtHotel();
  }

  togglePayNowButton = () => {
    this.setState({
      isProcessingPayment: !this.state.isProcessingPayment,
    });
  }

  invokeRazorpay = (data, promise) => {
    const {
      onSuccessPay,
      onErrorPay,
    } = this.props;
    const razorpay = new Razorpay({ });
    razorpay.createPayment({}, {
      paused: true,
      message: 'Confirming order...',
    });

    razorpay.on('payment.success', (res) => {
      this.togglePayNowButton();
      onSuccessPay('razorpay', res.razorpay_payment_id, res.razorpay_signature).catch((err) => {
        onErrorPay(err.msg);
      });
    });

    razorpay.on('payment.error', (res) => {
      this.togglePayNowButton();
      const errorMsg = this.errorMsg || res.error.description;
      onErrorPay(errorMsg);

      this.errorMsg = '';
    });

    promise.then(() => {
      const { user, orderId, gatewayOrderId, amountToSend } = this.props;
      const amount = +(amountToSend * 100).toFixed(2);
      const paymentData = {
        amount,
        currency: 'INR',
        ...data,
      };
      razorpay.emit('payment.resume', {
        ...paymentData,
        order_id: gatewayOrderId,
        ...user,
        notes: {
          order_id: orderId,
        },
      });
    }, () => {
      this.errorMsg = 'Sorry, the last room just got booked.';
      this.togglePayNowButton();
      razorpay.emit('payment.cancel');
    });
  };

  makePayment = async (data, gateway) => {
    const {
      amountToSend,
      payAtHotel,
      selectedRatePlan,
    } = this.props;
    this.setState({ payEnabled: false });
    analyticsService.paymentModeSelected(data.method, amountToSend, selectedRatePlan);
    if (!amountToSend) {
      payAtHotel();
      return;
    }
    const payPromise = this.onPayNow(gateway);
    this.togglePayNowButton();
    this.invokeRazorpay(data, payPromise);
  };

  render() {
    const { banks, defaultBanks, wallet, payEnabled, isProcessingPayment } = this.state;
    const pahTitle = get(abService.getExperiments(), 'pahCtaDesktop.payload.title', 'Pay at Hotel');
    const {
      offers,
      bookingApiError,
      isPrepaidOnly,
      isPayAtHotelEnabled,
      selectedRatePlan,
      ratePlans,
      itineraryActions,
    } = this.props;

    return (
      <div>
        <Tabs
          className="pay-tabs"
          ref={(tabs) => tabs && findDOMNode(tabs).classList.remove('react-tabs')}
        >
          <TabList>
            <Tab id="t-creditTab">Credit/Debit Card</Tab>
            <Tab id="t-netBankingTab">Net Banking</Tab>
            <Tab>
              <div id="t-walletTab">
               Wallet/ UPI
                <Space margin={[0, 1, 0]}>
                  <Tag
                    color="yellow"
                    shape="capsular"
                    kind="filled"
                    size="small"
                  >
                    NEW
                  </Tag>
                </Space>
              </div>
              {
                offers.map(({ short_text: shortText }) =>
                  <div className="pay-tabs__wallet-offer" key={shortText}>{shortText}</div>)
              }
            </Tab>
            <Tab id="t-pahTab">{pahTitle}</Tab>
          </TabList>
          <TabPanel>
            <CardPay
              makePayment={this.makePayment}
              payEnabled={payEnabled}
              isProcessingPayment={isProcessingPayment}
            />
          </TabPanel>
          <TabPanel>
            <Netbank
              makePayment={this.makePayment}
              banks={banks}
              defaultBanks={defaultBanks}
              payEnabled={payEnabled}
              isProcessingPayment={isProcessingPayment}
            />
          </TabPanel>
          <TabPanel>
            <Wallet
              makePayment={this.makePayment}
              wallet={wallet}
              selectedRatePlan={selectedRatePlan}
              ratePlans={ratePlans}
              payNowPayment={itineraryActions.payNowPayment}
              offers={offers}
              payEnabled={payEnabled}
              isProcessingPayment={isProcessingPayment}
            />
          </TabPanel>
          <TabPanel>
            <PayAtHotel
              isPayAtHotelEnabled={isPayAtHotelEnabled}
              isPrepaidOnly={isPrepaidOnly}
              payAtHotel={this.onPayAtHotel}
              selectedRatePlan={selectedRatePlan}
              isProcessingPayment={isProcessingPayment}
              ratePlans={ratePlans}
            />
          </TabPanel>
        </Tabs>
        {
          bookingApiError ? (
            <div className="text-error">{bookingApiError}</div>
          ) : (null)
        }
      </div>
    );
  }
}

Pay.propTypes = {
  orderId: PropTypes.string.isRequired,
  gatewayOrderId: PropTypes.string.isRequired,
  amountToSend: PropTypes.number.isRequired,
  user: PropTypes.object.isRequired,
  paymentGatewayConfig: PropTypes.object.isRequired,
  hidePaymentOption: PropTypes.object.isRequired,
  onPay: PropTypes.func.isRequired,
  itineraryActions: PropTypes.object.isRequired,
  onSuccessPay: PropTypes.func.isRequired,
  onErrorPay: PropTypes.func.isRequired,
  refreshBookingId: PropTypes.func.isRequired,
  payAtHotel: PropTypes.func.isRequired,
  offers: PropTypes.array.isRequired,
  bookingApiError: PropTypes.string,
  isPayAtHotelEnabled: PropTypes.bool,
  isPrepaidOnly: PropTypes.bool,
  selectedRatePlan: PropTypes.string,
  ratePlans: PropTypes.object,
};

export default Pay;
