import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { withFormsy } from 'formsy-react';

class ExpiryInput extends React.Component {
  handleFieldChange = (e) => {
    const { value, setValue, onChange } = this.props;
    const val = {
      ...value,
      [e.target.name]: e.target.value,
    };

    setValue(val);
    onChange(val);
  };

  render() {
    const currentYear = new Date().getFullYear();
    const {
      value: {
        month,
        year,
      },
      label,
      isFormSubmitted,
      getErrorMessage,
      isValid,
    } = this.props;

    const isError = isFormSubmitted() && !isValid();

    const labelClassNames = cx('card-pay__label', {
      'formsy-input__label--valid': isValid(),
      'formsy-input__label--error': isError,
    });

    return (
      <div className="card-pay__expiry">
        <div className={labelClassNames}>{label}</div>
        <div className="card-pay__expiry-container">
          <div className="select-box checkout__select">
            <select
              className="select card-pay__expiry-field"
              name="month"
              value={month}
              onChange={this.handleFieldChange}
            >
              <option value="">MONTH</option>
              {Array.from(Array(12), (v, i) => <option key={i + 1}>{i + 1}</option>)}
            </select>
          </div>
          <div className="select-box checkout__select">
            <select
              className="select card-pay__expiry-field"
              name="year"
              value={year}
              onChange={this.handleFieldChange}
            >
              <option value="">YEAR</option>
              {Array(36).fill().map((v, i) => <option key={v}>{i + currentYear}</option>)}
            </select>
          </div>
        </div>
        <p className="card-pay__error formsy-input__error-message">
          {isFormSubmitted() ? getErrorMessage() : ''}
        </p>
      </div>
    );
  }
}

ExpiryInput.propTypes = {
  value: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  isValid: PropTypes.func.isRequired,
  isFormSubmitted: PropTypes.func.isRequired,
  getErrorMessage: PropTypes.func.isRequired,
  setValue: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default withFormsy(ExpiryInput);
