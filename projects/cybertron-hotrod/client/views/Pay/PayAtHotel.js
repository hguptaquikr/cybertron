import React from 'react';
import PropTypes from 'prop-types';
import ABButton from '../../components/ABButton/ABButton';

const PayAtHotel = ({
  isPrepaidOnly,
  payAtHotel,
  isPayAtHotelEnabled,
  selectedRatePlan,
  isProcessingPayment,
  ratePlans,
}) => {
  const getPriceDiff = (plans) =>
    Math.abs(Object.values(plans).reduce((a, b) => a.sellingPrice - b.sellingPrice));
  const isNrp = selectedRatePlan === 'NRP' || false;
  const priceDiff = isNrp ? Math.round(getPriceDiff(ratePlans)) : 0;

  const renderPAHDetails = () => {
    if (!isPayAtHotelEnabled) {
      return (
        <div>
          Due to high demand, Pay At Hotel is not available on this hotel.
          Please choose from one of the other payment methods.
        </div>
      );
    }
    return isNrp ? (
      <div>
        <div>
          {
            `You have selected a non-refundable rate plan. On this plan you end up saving
            ₹${priceDiff} and are required to pay in advance.`
          }
        </div>
        <div className="pah__text">
          If you would like to Pay at the hotel, please go back to the first step and select
          the other plan option.
        </div>
      </div>
    ) : (
      <div>
        You might be required to confirm your booking over a phone call or by making a part
        payment a few days before your checkin date.
      </div>
    );
  };

  return (
    <div>
      <div className="col-12 pah">
        {
          renderPAHDetails()
        }
        <div className="pah__button">
          {
            !isPrepaidOnly ? (
              <ABButton
                id="t-itineraryPahCTA"
                onClick={payAtHotel}
                block
                experiment="pahCtaDesktop"
                disabled={isNrp || isProcessingPayment || !isPayAtHotelEnabled}
                render={(text) => text || 'PROCEED TO BOOK'}
              />
            ) : (null)
          }
        </div>
      </div>
    </div>
  );
};

PayAtHotel.propTypes = {
  isPayAtHotelEnabled: PropTypes.bool,
  isPrepaidOnly: PropTypes.bool,
  payAtHotel: PropTypes.func,
  selectedRatePlan: PropTypes.string,
  isProcessingPayment: PropTypes.bool.isRequired,
  ratePlans: PropTypes.object,
};

export default PayAtHotel;
