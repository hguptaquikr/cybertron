import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { withFormsy } from 'formsy-react';

class CardInput extends React.Component {
  state = {
    isDirty: false,
  };

  getCardTypeDetails = (num) => this.props.getCardArray().find(
    (v) => v.pattern.test(num),
  );

  handleFieldChange = (e) => {
    const current = e.target.value.replace(/ /g, '');
    const { formatCardNumber, setValue, onChange } = this.props;

    if (current && !/^\d+$/.test(current)) {
      return;
    }

    const cardType = this.getCardTypeDetails(current);
    const formattedValue = (formatCardNumber(current) || '').trim();
    setValue(formattedValue);
    onChange({
      value: formattedValue,
      type: cardType,
    });
  };

  render() {
    const {
      data: { value, type },
      name,
      label,
      isFormSubmitted,
      isValid,
      getErrorMessage,
    } = this.props;

    const { isDirty } = this.state;
    const isError = (isDirty || isFormSubmitted()) && !isValid();

    const inputClassNames = cx('formsy-input__field',
      `card-pay__input card-pay__number card-pay__number--${type.type}`, {
        'formsy-input__field--error': isError,
      });
    const labelClassNames = cx('formsy-input__label', {
      'formsy-input__label--has-value': value,
      'formsy-input__label--valid': isValid() && value,
      'formsy-input__label--error': isError,
    });

    return (
      <div className="formsy-input card-pay__input-container">
        <input
          id={name}
          className={inputClassNames}
          type="text"
          value={value}
          onChange={this.handleFieldChange}
          onBlur={() => this.setState({ isDirty: true })}
        />
        <label htmlFor={name} className={labelClassNames}>
          {label}
        </label>
        {
          isError ? (
            <div className="card-pay__error formsy-input__error-message">
              {getErrorMessage()}
            </div>
          ) : (null)
        }
      </div>
    );
  }
}

CardInput.propTypes = {
  data: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  isValid: PropTypes.func.isRequired,
  isFormSubmitted: PropTypes.func.isRequired,
  getErrorMessage: PropTypes.func.isRequired,
  setValue: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  formatCardNumber: PropTypes.func.isRequired,
  getCardArray: PropTypes.func.isRequired,
};

export default withFormsy(CardInput);
