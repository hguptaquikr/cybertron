import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import get from 'lodash/get';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';
import AmazonPay from './AmazonPay';
import PhonePe from './PhonePe';
import * as contentActionCreators from '../../services/content/contentDuck';

class Wallet extends React.Component {
  state = {
    selectedWallet: 'PhonePe',
    isPaymentDisabled: false,
  };

  componentDidMount() {
    const { contentActions } = this.props;
    contentActions.getWalletsPromotion();
  }

  onPaymentChange= (gateWay) => () => {
    const { payNowPayment, selectedRatePlan } = this.props;
    this.setState({ isPaymentDisabled: true },
      () =>
        payNowPayment(selectedRatePlan, gateWay).then((value) => {
          window.location.replace(value.redirect_url);
        }));
  }

  toggleWallet = (value) => () => {
    this.setState({ selectedWallet: value });
  }

  mobikwikPay = () => () => {
    this.props.makePayment({
      method: 'wallet',
      wallet: this.state.selectedWallet,
    });
  }

  render() {
    const { selectedWallet, isPaymentDisabled, showError } = this.state;
    const { offers, ratePlans, selectedRatePlan, content: { walletsPromotion } } = this.props;
    const ratePlan = get(ratePlans, selectedRatePlan);
    return (
      <Flex>
        <View>
          <PhonePe
            onPaymentChange={this.onPaymentChange('phonepe')}
            ratePlan={ratePlan}
            toggleWallet={this.toggleWallet('PhonePe')}
            selectedWallet={selectedWallet}
            isPaymentDisabled={isPaymentDisabled}
            promotionText={walletsPromotion.phonePe}
          />
          <AmazonPay
            onPaymentChange={this.onPaymentChange('amazonpay')}
            ratePlan={ratePlan}
            toggleWallet={this.toggleWallet('AmazonPay')}
            selectedWallet={selectedWallet}
            isPaymentDisabled={isPaymentDisabled}
          />
          { offers.map(({ text }) => <small key={text}>{text}</small>) }
          { showError ? <p className="text-error">Please select a wallet</p> : null }
        </View>
      </Flex>
    );
  }
}

Wallet.propTypes = {
  contentActions: PropTypes.object,
  content: PropTypes.object,
  makePayment: PropTypes.func.isRequired,
  payNowPayment: PropTypes.func.isRequired,
  offers: PropTypes.object.isRequired,
  ratePlans: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Wallet);
