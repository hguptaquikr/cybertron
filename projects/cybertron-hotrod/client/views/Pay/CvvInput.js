import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { withFormsy } from 'formsy-react';

class CvvInput extends React.Component {
  state = {
    isDirty: false,
  };

  handleFieldChange = (e) => {
    const { cvcLength, setValue, onChange } = this.props;
    const maxLength = cvcLength.slice(-1)[0];
    const current = e.target.value.slice(0, maxLength);

    if (current && !/^\d+$/.test(current)) {
      return;
    }

    setValue(current);
    onChange(current);
  };

  render() {
    const {
      value,
      name,
      isFormSubmitted,
      getErrorMessage,
      isValid,
      label,
    } = this.props;

    const { isDirty } = this.state;
    const isError = (isDirty || isFormSubmitted()) && !isValid();

    const inputClassNames = cx('formsy-input__field card-pay__cvv', {
      'formsy-input__field--error': isError,
    });
    const labelClassNames = cx('formsy-input__label', {
      'formsy-input__label--has-value': value,
      'formsy-input__label--valid': isValid() && value,
      'formsy-input__label--error': isError,
    });

    return (
      <div>
        <div className="card-pay__cvv-container">
          <div className="formsy-input">
            <input
              id={name}
              className={inputClassNames}
              onBlur={() => this.setState({ isDirty: true })}
              type="text"
              value={value}
              onChange={this.handleFieldChange}
            />
            <label htmlFor={name} className={labelClassNames}>
              {label}
            </label>
          </div>
          <div className="card-pay__cvv-details">
            <span className="card-pay__cvv-image" />
            <div className="card-pay__cvv-text">
              Last 3 digits printed on the back of the card
            </div>
          </div>
        </div>
        {
          isError ? (
            <div className="card-pay__error card-pay__cvv-error formsy-input__error-message">
              {getErrorMessage()}
            </div>
          ) : (null)
        }
      </div>
    );
  }
}

CvvInput.propTypes = {
  value: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  isValid: PropTypes.func.isRequired,
  isFormSubmitted: PropTypes.func.isRequired,
  getErrorMessage: PropTypes.func.isRequired,
  setValue: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  cvcLength: PropTypes.array.isRequired,
};

export default withFormsy(CvvInput);
