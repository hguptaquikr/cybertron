import React from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import cx from 'classnames';
import Payment, { fns as PayHelpers } from 'payment';
import FormsyInput from '../../components/Formsy/FormsyInput';
import CardInput from './CardInput';
import ExpiryInput from './ExpiryInput';
import CvvInput from './CvvInput';

Payment.addToCardArray({
  type: 'rupay',
  pattern: /^(508|606|607|608|652|653)/,
  format: /(\d{1,4})/g,
  length: [16],
  cvcLength: [3],
  luhn: true,
});

// Add this to the end of the array to get default values
Payment.addToCardArray({
  type: 'default',
  pattern: /^\d*/,
  format: /(\d{1,4})/g,
  length: [16],
  cvcLength: [3],
  luhn: false,
});

class CardPay extends React.Component {
  state = {
    card: {
      type: {
        type: 'default',
        cvcLength: [3],
        length: [16],
      },
      value: '',
    },
    name: '',
    expiry: {
      month: '',
      year: '',
    },
    cvv: '',
  };

  onChange = (field, value) => {
    this.setState({ [field]: value });
  };

  pay = () => {
    const { card, name, expiry, cvv } = this.state;
    const data = {
      method: 'card',
      'card[name]': name,
      'card[number]': card.value,
      'card[cvv]': cvv,
      'card[expiry_month]': expiry.month,
      'card[expiry_year]': expiry.year,
    };

    this.props.makePayment(data, 'razorpay');
  };

  render() {
    const { card, name, expiry, cvv } = this.state;
    const { payEnabled, isProcessingPayment } = this.props;
    return (
      <Formsy className="card-pay" onValidSubmit={this.pay} >
        <CardInput
          getCardArray={Payment.getCardArray}
          formatCardNumber={PayHelpers.formatCardNumber}
          onChange={(v) => this.onChange('card', v)}
          name="card"
          data={card}
          validations={{
            validCard: (vs, v) => !!PayHelpers.validateCardNumber(v),
          }}
          validatationError="Please Enter the Card Number"
          validationErrors={{
            validCard: 'Card Number Invalid',
            isDefaultRequiredValue: 'Please Enter the Card Number',
          }}
          label="Card Number"
          required
        />
        <FormsyInput
          type="text"
          name="name"
          label="Card Holder's Name"
          value={name}
          errorClassName="card-pay__error"
          inputClassName="card-pay__input"
          containerClassName="card-pay__input-container"
          onChange={(v) => this.onChange('name', v.target.value)}
          required
        />
        <ExpiryInput
          onChange={(v) => this.onChange('expiry', v)}
          value={expiry}
          name="expiry"
          label="Expiry Date"
          validations={{
            validExpiry: (vs, v) => !!(v && v.month && v.year),
          }}
          validationErrors={{
            validExpiry: 'Please select the expiry Month and Year',
          }}
        />
        <CvvInput
          cvcLength={card.type.cvcLength}
          onChange={(v) => this.onChange('cvv', v)}
          label="CVV"
          value={cvv}
          name="cvv"
          validations={{
            validCvv: (vs, v) => !!PayHelpers.validateCardCVC(v, card.type.type),
          }}
          validationErrors={{
            validCvv: 'Invalid CVV',
            isDefaultRequiredValue: 'Please Enter the CVV',
          }}
          required
        />

        <button
          type="submit"
          name="button"
          id="t-payNow"
          className={cx('checkout__action button', {
            'button--disabled': !payEnabled || isProcessingPayment,
          })}
        >Pay Now
        </button>
      </Formsy>
    );
  }
}

CardPay.propTypes = {
  makePayment: PropTypes.func.isRequired,
  payEnabled: PropTypes.bool,
  isProcessingPayment: PropTypes.bool.isRequired,
};

export default CardPay;
