import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

const FaqPage = Loadable({
  loader: () => {
    importCss('FaqPage');
    return import('./FaqPage' /* webpackChunkName: 'FaqPage' */);
  },
  loading: () => null,
});

export default FaqPage;
