/* eslint-disable react/no-danger */
import React from 'react';
import PropTypes from 'prop-types';
import FaqAccordion from './FaqAccordion';

const FaqInnerContent = ({ contents }) => (
  <div>
    {
      contents.map((content) => (
        <div key={content.type}>
          {
            content.type === 'html' ? (
              <div className="faq-data" dangerouslySetInnerHTML={{ __html: content.data }} />
            ) : (
              <div className="qa">
                {
                  content.data.map((qa) =>
                    <FaqAccordion key={qa.q} qa={qa} />)
                }
              </div>
            )
          }
        </div>
      ))
    }
  </div>
);

FaqInnerContent.propTypes = {
  contents: PropTypes.array.isRequired,
};

export default FaqInnerContent;
