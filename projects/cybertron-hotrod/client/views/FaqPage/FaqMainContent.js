import React from 'react';
import PropTypes from 'prop-types';
import FaqInnerContent from './FaqInnerContent';

const FaqMainContent = ({ faq }) => (
  <div className="faq-main">
    {
      faq.map(({ mainTitle, sideTitle, content, subcontent }) => (
        <div className="faq-main__content" key={mainTitle}>
          <h3 className="subheading-1 faq-main__title" id={sideTitle}>
            {mainTitle}
          </h3>
          <FaqInnerContent contents={content} />
          {
            subcontent.length ? (
              <div className="faq-main__subcontent">
                {
                  subcontent.map((sub) => (
                    <div key={sub.title}>
                      <h4 className="subheading-2 faq-main-subtitle">{sub.title}</h4>
                      <FaqInnerContent contents={sub.content} />
                    </div>
                  ))
                }
              </div>
            ) : null
          }
        </div>
      ))
    }
  </div>
);

FaqMainContent.propTypes = {
  faq: PropTypes.array.isRequired,
};

export default FaqMainContent;
