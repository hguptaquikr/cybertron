import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as contentActionCreators from '../../services/content/contentDuck';
import FaqSidebar from './FaqSidebar';
import FaqMainContent from './FaqMainContent';
import './faqPage.css';

class FaqPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];

    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(contentActionCreators.getFaqContent()));

    return Promise.all(promises);
  }

  componentDidMount() {
    const { contentActions, route, match } = this.props;
    contentActions.getSeoContent(route, match.params);
    contentActions.getFaqContent();
  }

  render() {
    const { faq } = this.props;

    return (
      <div className="faq-page">
        <div className="container faq-page__container">
          <p className="heading-1">FREQUENTLY ASKED QUESTIONS</p>
          <div className="faq-page__intro">
            <p className="subheading-4 faq-p"> Hello there,</p>
            <p className="subheading-4 faq-p">We are thrilled that you want to reach out to us!
              Here are a few resources that should come in real handy.
            </p>
          </div>
          <div className="row">
            <div className="col-3">
              <FaqSidebar faq={faq} />
            </div>
            <div className="col-9">
              <FaqMainContent faq={faq} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

FaqPage.propTypes = {
  contentActions: PropTypes.object.isRequired,
  faq: PropTypes.array.isRequired,
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  faq: state.content.faq,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(FaqPage);
