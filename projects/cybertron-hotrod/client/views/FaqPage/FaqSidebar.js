import React from 'react';
import PropTypes from 'prop-types';

const pointToSpecific = (title) => {
  const element = document.getElementById(title);
  element.scrollIntoView();
};

const SideBar = ({ faq }) => (
  <ul className="sidebar">
    {
      faq.map(({ sideTitle }) => (
        <li className="sidebar__title" key={sideTitle}>
          <p className="sidebar__link" onClick={() => pointToSpecific(sideTitle)}>{sideTitle}</p>
        </li>
      ))
    }
  </ul>
);

SideBar.propTypes = {
  faq: PropTypes.array.isRequired,
};

export default SideBar;
