/* eslint-disable react/no-danger */
import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

class FaqAccordion extends React.Component {
  state = {
    isOpen: false,
  };

  toggleExpansion = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    const { qa } = this.props;
    const { isOpen } = this.state;

    return (
      <div>
        <div className="qa__q" onClick={this.toggleExpansion}>
          <div className="row row--middle">
            <div className="col-1">
              <span className="qa__q-ques">Q.</span>
            </div>
            <div className="col-10" dangerouslySetInnerHTML={{ __html: qa.q }} />
            <div className="col-1">
              <i className={cx({ 'icon-angle-right': !isOpen, 'icon-angle-down': isOpen })} />
            </div>
          </div>
        </div>
        {
          isOpen ? (
            <div className="qa__a">
              <div className="row">
                <div className="col-1">
                  <span className="qa__a-ans">A.</span>
                </div>
                <div className="col-10">
                  <div dangerouslySetInnerHTML={{ __html: qa.a }} />
                </div>
              </div>
            </div>
          ) : null
        }
      </div>
    );
  }
}

FaqAccordion.propTypes = {
  qa: PropTypes.object.isRequired,
};

export default FaqAccordion;
