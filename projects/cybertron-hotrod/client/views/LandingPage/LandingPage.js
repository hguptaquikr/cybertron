import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import qs from 'query-string';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as walletActionCreators from '../../services/wallet/walletDuck';
import authService from '../../services/auth/authService';
import Footer from '../../components/Footer/Footer';
import SeoFooter from '../../components/Seo/SeoFooter';
import CitiesPage from '../../views/CitiesPage';
import ResultsPage from '../../views/ResultsPage';
import SeoResultsPage from '../../views/SeoResultsPage';
import Banner from './Banner';
import RewardsBanner from './RewardsBanner';
import Offers from './Offers';
import Marketing from './Marketing';
import QualityGuarantee from './QualityGuarantee/QualityGuarantee';
import Awards from './Awards';
import Testimonial from './Testimonial';
import Initiatives from './Initiatives';
import analyticsService from '../../services/analytics/analyticsService';
import * as abService from '../../services/ab/abService';
import './landingPage.css';

class LandingPage extends React.Component {
  static componentWillServerRender = ({ store: { dispatch, getState }, route, match, req }) => {
    const promises = [];

    dispatch(searchActionCreators.setSearchState({ ...req.query, ...match.params }));

    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));

    if (isEmpty(getState().content.cities)) {
      promises.push(dispatch(contentActionCreators.getLandingContent()));
    }

    return Promise.all(promises);
  }

  componentDidMount() {
    const {
      searchActions,
      walletActions,
      contentActions,
      route,
      match,
      location,
    } = this.props;

    searchActions.setSearchState({ ...qs.parse(location.search), ...match.params });
    contentActions.getSeoContent(route, match.params);
    contentActions.getLandingContent();
    if (authService.isLoggedIn()) {
      walletActions.getWalletBalance();
    }
    this.captureAnalytics();
    CitiesPage.preload();
    ResultsPage.preload();
    SeoResultsPage.preload();
  }

  captureAnalytics = () => {
    analyticsService.homeContentViewed({
      ...abService.impressionProperties(['findPageCtaDesktop']),
    });
  }

  render() {
    const {
      content: {
        banners,
        cities,
        offers,
        content_block: contentBlock,
        awards,
        testimonials,
        initiatives,
        joinus_banner: joinusBanner,
        press_release: pressRelease,
      },
      search,
    } = this.props;
    return (
      <div className="landing-page">
        <Banner
          cities={cities}
          banner={banners[0]}
          search={search}
        />
        <QualityGuarantee />
        <RewardsBanner />
        { !isEmpty(offers) ? <Offers offers={offers} /> : null }
        { !isEmpty(contentBlock.title) ? <Marketing content={contentBlock} /> : null }
        { !isEmpty(awards) ? <Awards awards={awards} /> : null }
        { !isEmpty(testimonials) ? <Testimonial testimonials={testimonials} /> : null }
        {
          !isEmpty(initiatives) ? (
            <Initiatives
              initiatives={initiatives}
              joinusBanner={joinusBanner}
              pressRelease={pressRelease}
            />
          ) : (null)
        }
        <Footer />
        <SeoFooter cities={cities} content={contentBlock.subtitles} />
      </div>
    );
  }
}

LandingPage.propTypes = {
  walletActions: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
  search: state.search,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LandingPage);
