import React from 'react';
import PropTypes from 'prop-types';

const Initiatives = ({
  initiatives,
  joinusBanner: {
    image_url: joinImage,
  },
  pressRelease,
}) => (
  <section className="section lp-initiatives">
    <div className="container">
      {initiatives.length ? (
        <ul className="row lp-initiatives__item">
          {
            initiatives.map(({
              title,
              image_url: imageUrl,
              description,
              know_more_link: knowMoreLink,
            }) => (
              <li key={title} className="col-4" >
                <h3 className="lp-initiatives__heading subheading-1">{title}</h3>
                <a href={knowMoreLink}>
                  <img
                    className="lp-initiatives__img"
                    src={`${imageUrl}?w=330&h=325&fm=pjpg&fit=crop&auto=compress`}
                    alt={title}
                  />
                </a>
                <p className="text-2 lp-initiatives__info">{description}</p>
                <a
                  className="more-link"
                  href={knowMoreLink}
                  target="_blank"
                  rel="noopener noreferrer"
                >KNOW MORE
                </a>
              </li>
            ))
          }
        </ul>
      ) : (null)}

      {
        joinImage ? (
          <div className="lp-joinus lp-initiatives__item">
            <a href="/joinus/">
              <img className="lp-joinus__image" src={`${joinImage}?fm=pjpg&w=1024&auto=compress`} alt="Join Us" />
            </a>
          </div>
        ) : (null)
      }

      {
        pressRelease.length ? (
          <ul className="row lp-initiatives__item">
            {
              pressRelease.map(({
                image_url: imageUrl,
                target_url: targetUrl,
              }) => (
                <li className="col" key={targetUrl}>
                  <a href={targetUrl}>
                    <img src={imageUrl} alt="" />
                  </a>
                </li>
              ))
            }
          </ul>
        ) : (null)
      }

    </div>
  </section>
);

Initiatives.propTypes = {
  initiatives: PropTypes.array.isRequired,
  joinusBanner: PropTypes.object,
  pressRelease: PropTypes.array,
};

export default Initiatives;
