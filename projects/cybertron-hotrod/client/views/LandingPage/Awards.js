import React from 'react';
import PropTypes from 'prop-types';

const Awards = ({ awards }) => (
  <section className="section">
    <div className="container">
      <h2 className="heading-1 section__title">AWARDS & RECOGNITIONS</h2>
      <p className="section__subtitle">
        Pinky promise we did not pay them. You could ask!
      </p>
      <ul className="lp-awards row">
        {
          awards.map(({
            image_url: imageUrl,
            title,
            description,
          }) => (
            <li key={title} className="col-4 lp-awards__item">
              <div>
                <img
                  alt="TripAdvisor Reward"
                  className="lp-awards__img"
                  src={`${imageUrl}?w=165&h=60&auto=compress`}
                />
              </div>
              <h3 className="subheading-1 lp-awards__title">{title}</h3>
              <p className="text-2 lp-awards__text">{description}</p>
            </li>
          ))
        }
      </ul>
    </div>
  </section>
);

Awards.propTypes = {
  awards: PropTypes.array.isRequired,
};

export default Awards;
