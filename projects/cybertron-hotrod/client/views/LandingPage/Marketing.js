import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import config from '../../../config';

const Marketing = ({ content: { title, subtitles, blocks } }) => (
  <section className="section">
    <div className="container">
      <h2 className="heading-1 section__title">{title}</h2>
      <p className="section__subtitle">
        {
          subtitles.map((subtitle) => (
            <span key={subtitle} className="section__subtitle-items">{subtitle}</span>
          ))
        }
      </p>
      <ul className="row lp-marketing">
        {
          blocks.map(({ image_url: imageUrl, label, link }) => (
            <li className="lp-marketing__item" key={label}>
              <Link id="t-marketingCity" to={{ pathname: `${link}/` }}>
                <img
                  src={`${config.imageUrl}/files/${imageUrl}?w=330&h=325&fm=pjpg&fit=crop&auto=compress`}
                  alt={label}
                />
                <h3 className="heading-1 lp-marketing__text">{label}</h3>
              </Link>
            </li>
          ))
        }
      </ul>
    </div>
  </section>
);

Marketing.propTypes = {
  content: PropTypes.object.isRequired,
};

export default Marketing;
