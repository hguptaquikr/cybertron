import React from 'react';
import PropTypes from 'prop-types';

const Offers = ({ offers }) => (
  <section className="section lp-offers">
    <div className="container">
      <h2 className="heading-1 section__title">OFFERS ZONE</h2>
      <p className="section__subtitle">
        Discover the best of best deals right here!
      </p>
      <div className="row">
        {
          offers.map(({ offer_url: offerUrl, offer_link: offerLink }) => (
            <div className="col lp-offers__item" key={offerUrl}>
              {offerLink ? (
                <a href={offerLink} target="_blank" rel="noopener noreferrer">
                  <img
                    className="lp-offers__image"
                    src={`${offerUrl}?fm=pjpg`}
                    alt="treebo offers"
                  />
                </a>
              ) : (
                <img
                  className="lp-offers__image"
                  src={`${offerUrl}?fm=pjpg&auto=compress`}
                  alt="treebo offers"
                />
              )
            }
            </div>
          ))
        }
      </div>
    </div>
  </section>
);

Offers.propTypes = {
  offers: PropTypes.array,
};

export default Offers;
