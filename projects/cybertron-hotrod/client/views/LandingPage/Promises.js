import React from 'react';
import PropTypes from 'prop-types';

const Promises = ({ promises }) => (
  <section className="section">
    <div className="container">
      <h2 className="heading-1 section__title">BEST WAY TO STAY WHERE EVER YOU GO</h2>
      <p className="section__subtitle">
        All essentials covered. Air Conditioner, WiFi, Breakfast, Toiletries, Bed & Bath Linen
      </p>
      <ul className="lp-promises row">
        {
          promises.map(({
            image_url: imageUrl,
            title,
            description,
          }) => (
            <li key={title} className="col-4">
              <img
                src={`${imageUrl}?w=130&h=100`}
                alt=""
                className="lp-promises__img"
              />
              <h3 className="subheading-1 lp-promises__title">{title}</h3>
              <p className="text-2 lp-promises__text">{description}</p>
            </li>
          ))
        }
      </ul>
    </div>
  </section>
);

Promises.propTypes = {
  promises: PropTypes.array.isRequired,
};

export default Promises;
