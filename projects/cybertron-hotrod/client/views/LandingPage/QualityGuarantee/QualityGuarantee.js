import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import config from '../../../../config';
import * as contentActionCreators from '../../../services/content/contentDuck';
import analyticsService from '../../../services/analytics/analyticsService';
import './qualityGuarantee.css';

const qualityGuaranteePromises = [{
  imageUrl: `${config.imageUrl}/images/hotrod/comfortable-rooms.svg`,
  title: 'Comfortable Rooms',
}, {
  imageUrl: `${config.imageUrl}/images/hotrod/best-in-class-service.svg`,
  title: 'Best-in-class Service',
}, {
  imageUrl: `${config.imageUrl}/images/hotrod/wholesome-breakfast.svg`,
  title: 'Wholesome Breakfast',
}, {
  imageUrl: `${config.imageUrl}/images/hotrod/assured-room-amenities.svg`,
  title: 'Assured Room Amenities',
}];

class QualityGuarantee extends Component {
  componentDidMount() {
    const { contentActions } = this.props;
    contentActions.getPerfectStayContent();
  }

  render() {
    const { perfectStay } = this.props;
    return (
      <section className="section">
        <div className="container">
          <div className="qg">
            <h1 className="heading-1 section__title text--center">Perfect Stay or Don’t Pay</h1>
            <p className="subheading-1 qg__subtitle text--center">
              100% money-back promise
            </p>
            <div className="qg__proposition">
              <div className="qg__proposition__banner">
                <img
                  className="qg__proposition__banner__img"
                  src={perfectStay.illustration}
                  alt="Quality guarantee assured"
                />
                <div className="qg__proposition__banner__details">
                  <h2 className="subheading-1 qg__proposition__title">
                    400+ Treebo Hotels across 75+ cities now with money-back promise
                  </h2>
                  <Link
                    to="/perfect-stay/"
                    target="_blank"
                    onClick={() => analyticsService.perfectStayKnowMoreClicked('Home')}
                  >
                    <p className="subheading-1 qg__proposition__link">
                      {'How it works >'}
                    </p>
                  </Link>
                </div>
              </div>
              <ul className="row qg__proposition__promises">
                {
                  qualityGuaranteePromises.map(({
                    imageUrl,
                    title,
                  }) => (
                    <li key={title} className="col-3 text--center">
                      <img
                        src={imageUrl}
                        alt=""
                        className="qg__proposition__img"
                      />
                      <h3 className="subheading-1 qg__proposition__text">{title}</h3>
                    </li>
                  ))
                }
              </ul>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

QualityGuarantee.propTypes = {
  perfectStay: PropTypes.object,
  contentActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  perfectStay: state.content.perfectStay,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(QualityGuarantee);
