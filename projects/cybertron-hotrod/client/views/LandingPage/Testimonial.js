import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

class Testimonial extends React.Component {
  state = {
    active: 1,
  };

  showTestimonial = (index) => () => {
    this.setState({ active: index });
  }

  render() {
    const { testimonials } = this.props;
    const { active } = this.state;

    return (
      <section className="section lp-testimonials">
        <div className="container">
          <h2 className="heading-1 section__title">WHAT ARE PEOPLE SAYING ABOUT US</h2>
          <p className="section__subtitle">
            Only honest testimonials. Straight from the horse&apos;s mouth.
          </p>
          <ul className="row lp-testimonials__list">
            {
              testimonials.map(({ name, image_url: imageUrl }, i) => (
                <li
                  key={name}
                  className={cx('lp-testimonials__item', {
                    'lp-testimonials__item--active': active === i,
                  })}
                >
                  <img
                    className={cx('lp-testimonials__img', {
                      'lp-testimonials__img--active': active === i,
                    })}
                    src={`${imageUrl}?w=102&h=102&fm=pjpg&fit=crop&auto=compress`}
                    alt={name}
                    onClick={this.showTestimonial(i)}
                  />
                  <div
                    className={cx('subheading-1 lp-testimonials__user', {
                      'lp-testimonials__user--visible': active === i,
                    })}
                  >{name}
                  </div>
                </li>
              ))
            }
          </ul>
          <p className="text--center lp-testimonials__text text-1">
            { testimonials[active].description }
          </p>
        </div>
      </section>
    );
  }
}

Testimonial.propTypes = {
  testimonials: PropTypes.array.isRequired,
};

export default Testimonial;
