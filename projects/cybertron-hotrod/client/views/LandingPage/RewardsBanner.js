import React from 'react';
import { Link } from 'react-router-dom';
import analyticsService from '../../services/analytics/analyticsService';

const Rewards = () => (
  <section className="section">
    <div className="container">
      <div className="lp-rewards">
        <h2 className="heading-1 section__title">INTRODUCING TREEBO REWARDS</h2>
        <p className="section__subtitle lp-rewards__subtitle">
          Booking directly on Treebo Pays!
        </p>
        <div className="row">
          <div className="col-4">
            <img
              src="https://images.treebohotels.com/images/hotrod/treebo-rewards.svg"
              className="lp-rewards--logo"
              alt="Treebo"
            />
            <div className="lp-rewards__intro">
              Book on Treebo.com to enjoy exclusivity.
              Treebo Rewards guarantees best rates while earning Treebo Points. Use these for to get
              discounts for your stays at Treebo.
            </div>
            <Link
              id="treebo-points"
              to="/introducing-rewards/?flow=banner"
              onClick={analyticsService.rewardsBannerClicked}
              className="text-link lp-rewards__explore"
            >
              Explore Treebo Rewards
            </Link>
          </div>
          <div className="col-4 lp-rewards__tile">
            <div id="tile-1" className="lp-rewards__tile__body">
              <img
                src="https://images.treebohotels.com/images/hotrod/login-special-deals-60-px-copy.svg"
                className="lp-rewards__tile__body--img"
                alt="Treebo"
              />
              <div className="text-1 lp-rewards__tile__body--header">
                Our Best Rates
              </div>
              <div className="lp-rewards__tile__body--subheader">
                Members of the program get <br /> our best rates.
              </div>
            </div>
          </div>
          <div className="col-4">
            <div id="tile-2" className="lp-rewards__tile__body">
              <img
                src="https://images.treebohotels.com/images/hotrod/login-loyalty-60-px-copy.svg"
                className="lp-rewards__tile__body--img"
                alt="Treebo"
              />
              <div className="text-1 lp-rewards__tile__body--header">
                Earn Treebo Points
              </div>
              <div className="lp-rewards__tile__body--subheader">
                Earn for every stay booked <br /> directly with us.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default Rewards;
