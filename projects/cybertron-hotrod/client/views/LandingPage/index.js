import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

const LandingPage = Loadable({
  loader: () => {
    importCss('LandingPage');
    return import('./LandingPage' /* webpackChunkName: 'LandingPage' */);
  },
  loading: () => null,
});

export default LandingPage;
