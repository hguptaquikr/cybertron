import React from 'react';
import PropTypes from 'prop-types';
import Sticky from 'react-stickynode';
import { Link } from 'react-router-dom';
import SearchWidget from '../../views/SearchWidget/SearchWidget';
import analyticsService from '../../services/analytics/analyticsService';

const Banner = ({
  cities,
  banner: { image_url: bannerUrl, title, description },
}) => (
  <section className="landing-page__banner">
    <div
      className="section landing-page__banner__body"
      style={{ backgroundImage: `url(${bannerUrl})` }}
    >
      <div className="landing-page__search-container">
        <div className="container">
          <h1 className="heading-1 landing-page__banner__title">{title}</h1>
          <h2 className="landing-page__banner__subtitle">{description}</h2>
        </div>
        <Sticky activeClass="lp-search-widget--sticky">
          <div className="lp-search-widget">
            <SearchWidget className="landing-page__search-widget" />
          </div>
        </Sticky>
      </div>
    </div>
    <div className="landing-page__banner__city all-city">
      <div className="container">
        <ul className="all-city__list">
          {
            cities.slice(0, 8).map(({ name, url }) => (
              <li key={name} className="all-city__item">
                <Link
                  id="t-popularCities"
                  onClick={() => analyticsService.cityClicked(name)}
                  to={{ pathname: `${url}/` }}
                  className="all-city__link"
                >{name}
                </Link>
              </li>
            ))
          }
          <li className="all-city__item">
            <Link
              id="t-allCities"
              onClick={() => analyticsService.cityClicked('All Cities')}
              to="/cities/"
              className="all-city__link all-city__link--all"
            >All Cities
            </Link>
          </li>
        </ul>
      </div>
    </div>
  </section>
);

Banner.propTypes = {
  cities: PropTypes.array.isRequired,
  banner: PropTypes.object.isRequired,
};

export default Banner;
