import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import qs from 'query-string';
import * as hotelService from '../../services/hotel/hotelService';
import { getDistanceWithUnit } from '../../services/utils';
import analyticsService from '../../services/analytics/analyticsService';

const SimilarHotels = ({
  similarHotels,
  $roomType,
  prices,
  location,
}) => (
  <div className="hd-similarHotels" id="t-hdSimilarHotels">
    <ul className="row hd-similar__list">
      {
         similarHotels.slice(0, 4).map(({
          name: hotelName,
          address: area,
          id,
          images,
          distance,
          roomTypeIds,
        }) => {
            const price = prices[roomTypeIds[0]];
            const roomType = price ? $roomType.byId[price.id].type : null;
            const ratePlan = price ? Object.keys(price.ratePlans)[0] : null;
            const hotelDetailsTarget = hotelService.getHotelDetailsTarget({
              hotelName,
              hotelId: id,
              area,
              roomType,
              ratePlan,
              locationQuery: qs.parse(location.search),
            });
            const selectedRatePlan = price ? price.ratePlans[ratePlan] : {};
            const sellingPrice = price ? (
              <span className="hd-similar__details__price">
              &#x20b9;{Math.round(selectedRatePlan.sellingPrice)}
              </span>
          ) : (null);
            const strikedPrice = price && selectedRatePlan.discountPercentage ? (
              <span className="hd-similar__details__price--striked">
              &#x20b9;{Math.round(selectedRatePlan.strikedPrice)}
              </span>
          ) : (null);
            const distanceUnits = distance && getDistanceWithUnit(distance);
            return (
              <li className="col-3" key={id}>
                <Link
                  className="hd-similar__item"
                  to={hotelDetailsTarget}
                  onClick={() => analyticsService.similarTreeboClicked({ id, name: hotelName })}
                >
                  <img
                    className="hd-similar__image"
                    src={`${images[0].url}?w=230&h=136&fm=pjpg&fit=crop`}
                    alt={hotelName}
                  />
                  <div className="hd-similar__details">
                    <h4 className="subheading-2 hd-similar__title">{hotelName}</h4>
                    <div>{sellingPrice} {strikedPrice}</div>
                    {
                    distanceUnits ? (
                      <div className="hd-similar__details__distance">{distanceUnits} away</div>
                    ) : (null)
                  }
                  </div>
                </Link>
              </li>
            );
          })
      }
    </ul>
  </div>
);

SimilarHotels.propTypes = {
  similarHotels: PropTypes.array.isRequired,
  $roomType: PropTypes.object.isRequired,
  prices: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

export default withRouter(SimilarHotels);
