import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import Scroll from 'react-scroll';
import Carousel from './Carousel';
import HotelIndicator from './HotelIndicator/HotelIndicator';
import Loading from '../../components/Loading/Loading';
import RatePlanTag from '../../components/RatePlanTag/RatePlanTag';
import NewYearRateTag from '../../components/NewYearRateTag/NewYearRateTag';
import ABButton from '../../components/ABButton/ABButton';
import analyticsService from '../../services/analytics/analyticsService';
import searchService from '../../services/search/searchService';

class Hotel extends Component {
  getMetaAmenities = (amenities = [], defaultAmenities) => {
    let hotelAmenities = defaultAmenities;
    if (!amenities.some((amenity) => amenity.css_class === 'ac')) {
      hotelAmenities = defaultAmenities.slice(1);
    }
    return [...hotelAmenities, ...amenities].map((amenity) => amenity.name);
  }

  render() {
    const {
      isPriceLoading,
      soldOut,
      pricing: { couponCode, ratePlans = {} },
      wallet,
      details,
      details: { name, address, images, coordinates, amenities, id: hotelId },
      availableRoomTypeIds,
      selectedRatePlan,
      selectedRoomType,
      similarHotelsAvailable,
      redirectToItinerary,
      checkPricing,
      mapThumbnail,
      defaultAmenities,
      taReviews,
      policies,
      search: { datePicker },
    } = this.props;
    const {
      sellingPrice = 0,
      totalPrice,
      discountPercentage,
      strikedPrice,
      type,
    } = ratePlans[selectedRatePlan] || {};
    const { numberOfNights } = searchService.formattedDatePicker(datePicker.range, 'D MMM');
    const metaAmenities = this.getMetaAmenities(amenities, defaultAmenities);
    const addressJsx = address.locality
      ? `${address.locality}, ${address.city}` : address.city;
    const loadingJsx = isPriceLoading ? (
      <div>
        <Loading width="6rem" height="0.6rem" />
        <Loading width="10rem" height="0.6rem" marginTop="1.2rem" />
      </div>
    ) : (
      <button
        className="button button--secondary hd-hotel__book"
        onClick={() => checkPricing()}
      >
        Check Pricing
      </button>
    );

    return (
      <div className="hd-hotel">
        <div
          itemProp="amenityFeature"
          itemScope
          itemType="http://schema.org/LocationFeatureSpecification"
        >
          {
            [...new Set(metaAmenities)].map((amenity) => (
              <meta key={amenity} itemProp="name" content={amenity} />
            ))
          }
        </div>
        <div className="row">
          <Carousel
            soldOut={soldOut}
            images={images}
            name={name}
            coordinates={coordinates}
            mapThumbnail={mapThumbnail}
          />
          <div className="hd-hotel__info col-5">
            <div>
              <h1 className="heading-1 hd-hotel__title" id="t-hdHotelName">
                {name || <Loading width="15rem" />}
                {
                  !isEmpty(taReviews.awards) ? (
                    taReviews.awards.map(({ image, description }) => (
                      <img
                        key={image}
                        className="hd-hotel__award-img"
                        src={image}
                        alt="TripAdvisor Award"
                        title={description}
                      />
                    ))
                  ) : null
                }
              </h1>
              <p className="text-1 hd-hotel__address">
                { addressJsx || <Loading width="15rem" /> }
              </p>
              <HotelIndicator
                details={details}
                policies={policies}
                taReviews={taReviews}
              />
              <NewYearRateTag hotelId={hotelId} />
            </div>
            <div className="hd-hotel__book-info">
              <h3 className="hd-hotel__price">
                {
                  (
                    soldOut && (
                      <div>
                        <div className="text-error hd-hotel__sold-text">This Treebo is not available on selected dates.</div>
                        {
                          similarHotelsAvailable ? (
                            <div className="hd-hotel__sold-out">
                              <Scroll.Link
                                activeClass="hd-details__nav-item--active"
                                to="TABS_NEARBY_HOTELS"
                                spy
                                smooth
                                offset={-120}
                                isDynamic
                              >
                                <button className="button button--secondary">CHECK NEARBY TREEBOS</button>
                              </Scroll.Link>
                            </div>
                          ) : null
                        }
                      </div>
                    )
                  ) || (
                    sellingPrice || totalPrice ? (
                      <span>
                        <span className="hd-hotel__price-icon" id="t-hdSellingPrice">&#x20b9;</span>{Math.round(sellingPrice)}
                        {
                          selectedRatePlan !== '' ? (
                            <RatePlanTag rateType={type} className="hd-hotel__rate-plan" />
                          ) : null
                        }
                        {
                          discountPercentage ? (
                            <span>
                              <span className="hd-hotel__price-start hide">
                                &#x20b9;{Math.round(strikedPrice)}
                              </span>
                              <span className="text-primary hd-hotel__price-discount hide">
                                {discountPercentage}% off
                              </span>
                            </span>
                          ) : (null)
                        }
                      </span>
                    ) : loadingJsx
                  )
                }
              </h3>
              {
                (!(sellingPrice || totalPrice) || soldOut) || (
                  <span>
                    <p className="text-2">Tax incl. price for {numberOfNights.text}</p>
                    {
                      wallet.byType.TP.usableBalance ? (
                        <div id="wallet-price" className="hd-hotel__wallet-price">
                          {`Treebo Points worth ₹${wallet.byType.TP.usableBalance} is pre-applied on Price`}
                        </div>
                      ) : null
                    }
                    {
                      couponCode ? (
                        <div className="hd-hotel__coupon">
                          <hr width="80%" />
                          <span className="text-1 hd-hotel__coupon__code">{`"${couponCode}"`}</span> Coupon Code Pre-applied
                        </div>
                      ) : null
                    }
                    <ABButton
                      id="t-hdBookCTA"
                      onClick={() => redirectToItinerary(selectedRoomType, selectedRatePlan)}
                      experiment="hdCtaDesktop"
                      large
                      render={(text) => text || 'BOOK NOW'}
                    />
                    <p className="hd-hotel__payment-info">
                      {
                        type === 'non-refundable' ? 'Online payment Only.' : ''
                      }
                      {
                        availableRoomTypeIds.length > 1 || Object.keys(ratePlans).length > 1 ? (
                          <Scroll.Link
                            activeClass="hd-details__nav-item--active"
                            to="TABS_ROOM_TYPES"
                            spy
                            smooth
                            offset={-80}
                            isDynamic
                          >
                            <span
                              onClick={() => analyticsService.viewOtherPlansClicked(
                                name,
                                hotelId,
                                ratePlans[selectedRatePlan],
                              )}
                              className="text-link"
                            >
                              View other Room rates
                            </span>
                          </Scroll.Link>
                        ) : null
                      }
                    </p>
                  </span>
                )
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Hotel.propTypes = {
  details: PropTypes.object.isRequired,
  pricing: PropTypes.object.isRequired,
  soldOut: PropTypes.bool,
  similarHotelsAvailable: PropTypes.bool,
  isPriceLoading: PropTypes.bool,
  mapThumbnail: PropTypes.string,
  defaultAmenities: PropTypes.array,
  redirectToItinerary: PropTypes.func.isRequired,
  availableRoomTypeIds: PropTypes.array,
  checkPricing: PropTypes.func,
  taReviews: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.string.isRequired,
  wallet: PropTypes.object.isRequired,
  selectedRoomType: PropTypes.string.isRequired,
  policies: PropTypes.array.isRequired,
  search: PropTypes.object,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

export default connect(mapStateToProps)(Hotel);
