import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { withRouter } from 'react-router-dom';
import qs from 'query-string';
import isEmpty from 'lodash/isEmpty';
import startCase from 'lodash/startCase';
import moment from 'moment';
import * as priceActionCreators from '../../services/price/priceDuck';
import * as hotelActionCreators from '../../services/hotel/hotelDuck';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as walletActionCreators from '../../services/wallet/walletDuck';
import CheckoutPage from '../CheckoutPage';
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';
import SearchWidget from '../../views/SearchWidget/SearchWidget';
import Hotel from './Hotel';
import Details from './Details';
import mapThumbnail from './map-thumbnail.png';
import * as abService from '../../services/ab/abService';
import priceService from '../../services/price/priceService';
import searchService from '../../services/search/searchService';
import roomTypeService from '../../services/roomType/roomTypeService';
import analyticsService from '../../services/analytics/analyticsService';
import { getOccupancy, getNoOfDays, constants } from '../../services/utils';
import * as hotelService from '../../services/hotel/hotelService';
import authService from '../../services/auth/authService';
import * as routeService from '../../services/route/routeService';
import './hotelDetailsPage.css';

class HotelDetailsPage extends React.Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match, history, req }) => {
    const decodedHotelId = hotelService.decodeHotelId(match.params.hotelId);
    if (decodedHotelId) {
      return history.replace({
        pathname: history.location.pathname.replace(match.params.hotelId, decodedHotelId),
        search: history.location.search,
      });
    }
    const promises = [];
    dispatch(searchActionCreators.setSearchState({ ...req.query, ...match.params }));

    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(hotelActionCreators.getHotelDetails(match.params.hotelId)));

    promises.push(dispatch(contentActionCreators.getHotelReviews(match.params.hotelId)));

    return Promise.all(promises);
  }

  state = {
    soldOut: false,
    defaultAmenities: [
      { key: 'd1', name: 'AC Room', css_class: 'ac' },
      { key: 'd2', name: 'TV', css_class: 'tv' },
      { key: 'd3', name: 'WiFi', css_class: 'wifi' },
      { key: 'd4', name: 'Breakfast', css_class: 'breakfast' },
      { key: 'd5', name: 'Toiletries', css_class: 'bathtub' },
    ],
  };

  componentDidMount() {
    const {
      searchActions,
      hotelActions,
      contentActions,
      location,
      match,
      route,
    } = this.props;
    searchActions.setSearchState({ ...qs.parse(location.search), ...match.params });
    this.updateSearchParams(this.props);
    contentActions.getSeoContent(route, match.params);
    hotelActions.getHotelDetails(match.params.hotelId);
    contentActions.getHotelReviews(match.params.hotelId)
      .then(() => contentActions.getNonJCR(match.params.hotelId));
    this.getPricing(match.params.hotelId);
    CheckoutPage.preload();
  }

  componentWillReceiveProps(nextProps) {
    const { props } = this;
    const propsLocationQuery = qs.parse(props.location.search);
    const nextPropsLocationQuery = qs.parse(nextProps.location.search);
    const { contentActions, hotelActions, searchActions } = this.props;
    if (props.location.pathname !== nextProps.location.pathname) {
      this.updateSearchParams(nextProps);
    }
    if (
      (
        propsLocationQuery.checkin
          || propsLocationQuery.checkout
      ) && (
        props.match.params.hotelId !== nextProps.match.params.hotelId
        || propsLocationQuery.hotel_id !== nextPropsLocationQuery.hotel_id
        || propsLocationQuery.checkin !== nextPropsLocationQuery.checkin
        || propsLocationQuery.checkout !== nextPropsLocationQuery.checkout
        || propsLocationQuery.roomconfig !== nextPropsLocationQuery.roomconfig
        || (props.user.isAuthenticated && !nextProps.user.isAuthenticated)
      )
    ) {
      if (nextProps.match.params.hotelId) {
        searchActions.setSearchState({
          ...qs.parse(nextProps.location.search), ...nextProps.match.params,
        });
        contentActions.getSeoContent(nextProps.route, nextProps.match.params);
        hotelActions.getHotelDetails(nextProps.match.params.hotelId);
        this.getPricing(nextProps.match.params.hotelId);
        contentActions.getHotelReviews(nextProps.match.params.hotelId);
      }
    }
  }

  getPricing = (hotelId = this.props.match.params.hotelId) => {
    const { searchActions, hotelActions, walletActions, priceActions } = this.props;
    if (searchActions.validateSearch()) {
      hotelActions.getNearbyHotelResults(hotelId)
        .then((res) => {
          if (!isEmpty(res.ids)) priceActions.getNearbySearchPrices(hotelId, res.ids);
        });
      return priceActions.getRoomPrices(hotelId)
        .then((response) => {
          this.updateRoomQuery();
          if (authService.isLoggedIn()) walletActions.getWalletBalance();
          this.captureAnalytics();
          return response;
        });
    }
    return Promise.reject();
  }

  getAvailableRoomTypeIds = (roomTypeIds, prices) =>
    roomTypeIds.filter((roomTypeId) => prices[roomTypeId] && !!prices[roomTypeId].available)

  getPrice = (hotel, roomType) => {
    const { price } = this.props;
    return price.byRoomTypeId[roomTypeService.constructRoomTypeId(hotel, roomType)];
  }

  updateRoomQuery = (roomType, ratePlan) => {
    const {
      price,
      match,
      location,
      history,
      $roomType,
      hotel,
    } = this.props;

    const locationQuery = qs.parse(location.search);

    const availableRoomTypeIds =
      this.getAvailableRoomTypeIds(hotel.roomTypeIds, price.byRoomTypeId);
    const hotelPrices =
      priceService.getPricesForRoomTypeIds(availableRoomTypeIds, price.byRoomTypeId);
    const sortedPrices = priceService.sortPrice(hotelPrices);

    const selectedPrice = this.getPrice(match.params.hotelId, locationQuery.roomtype);
    const isSelectedRoomAvailable = selectedPrice && !!selectedPrice.available;
    const selectedRoomType = roomType || (isSelectedRoomAvailable ?
      locationQuery.roomtype : $roomType.byId[sortedPrices[0].id].type);

    const sortedRatePlans = priceService.sortRatePlan(
      this.getPrice(match.params.hotelId, selectedRoomType).ratePlans,
    );
    const selectedRatePlan = ratePlan || locationQuery.rateplan
      || sortedRatePlans[0].code;

    if (selectedRoomType && selectedRatePlan) {
      history.replace({
        pathname: location.pathname,
        search: qs.stringify({
          ...locationQuery,
          ...routeService.makeRoomTypeQuery(selectedRoomType),
          ...routeService.makeRatePlanQuery(selectedRatePlan),
        }),
      });
    }
  };

  updateSearchParams = (props) => {
    const {
      search: {
        datePicker: {
          range: {
            start,
            end,
          },
        },
        searchInput,
        occupancy,
      },
      history,
      location,
    } = props;
    const query = qs.parse(location.search);
    if (
      !query.checkin
      && !query.checkout
      && !query.roomconfig
    ) {
      const checkin = start ? moment(start) : moment();
      const checkout = end ? moment(end) : moment().add(1, 'days');
      history.replace({
        search: qs.stringify({
          ...query,
          ...routeService.makeRoomConfigQuery(occupancy.rooms),
          ...routeService.makeHotelIdQuery(searchInput.location.hotel_id),
          checkin: checkin.format(constants.dateFormat.query),
          checkout: checkout.format(constants.dateFormat.query),
        }),
      });
    }
  };

  redirectToItinerary = (roomType, ratePlan) => {
    const {
      search: {
        searchInput,
        datePicker,
        occupancy,
      },
      history,
      hotelActions,
      hotel,
      location,
    } = this.props;

    const locationQuery = qs.parse(location.search);

    hotelActions.bookNowClicked(hotel);
    this.updateRoomQuery(roomType, ratePlan);
    const { numberOfNights, abwDays } =
    searchService.formattedDatePicker(datePicker.range, constants.dateFormat.query);
    analyticsService.ratePlanSelected({
      page: 'HD page',
      newRoomType: roomType,
      oldRoomType: locationQuery.roomtype,
      newRatePlan: ratePlan,
      oldRatePlan: locationQuery.rateplan,
      lengthOfStay: numberOfNights.count,
      abwDays,
      priceDifference: Math.abs(
        Math.round(
          this.getPrice(hotel.id, locationQuery.roomtype)
            .ratePlans[locationQuery.rateplan].sellingPrice -
          this.getPrice(hotel.id, roomType)
            .ratePlans[ratePlan].sellingPrice,
        ),
      ),
    });

    history.push({
      pathname: '/itinerary/',
      search: qs.stringify({
        q: searchInput.location.label,
        landmark: searchInput.location.area.landmark,
        locality: searchInput.location.area.locality,
        city: searchInput.location.area.city,
        state: searchInput.location.area.state,
        ...routeService.makeCoordinatesQuery(searchInput.location.coordinates),
        ...routeService.makeHotelIdQuery(searchInput.location.hotel_id),
        ...routeService.makeDateRangeQuery(datePicker.range.start, datePicker.range.end),
        ...routeService.makeRoomConfigQuery(occupancy.rooms),
        ...routeService.makeRoomTypeQuery(roomType),
        ...routeService.makeRatePlanQuery(ratePlan),
      }),
    });
  }

  captureAnalytics = () => {
    const {
      search,
      location,
      match,
      price,
    } = this.props;

    const locationQuery = qs.parse(location.search);
    const selectedRoomType = locationQuery.roomtype;
    const selectedPrice = this.getPrice(match.params.hotelId, selectedRoomType) || price.byRoomTypeId['0|oak'];

    const { checkIn, checkOut, numberOfNights, abwDays } =
    searchService.formattedDatePicker(search.datePicker.range, constants.dateFormat.query);

    const analyticsProperties = {
      hotelId: match.params.hotelId,
      hotelName: startCase(match.params.hotelSlug),
      pageUrl: location.pathname,
      checkin: checkIn,
      checkout: checkOut,
      lengthOfStay: numberOfNights.count,
      price: Math.round(Object.values(selectedPrice.ratePlans)[0].sellingPrice),
      roomConfig: search.occupancy.rooms.map((room) => `${room.adults}-${room.kids || 0}`).join(','),
      abwDays,
      ...abService.impressionProperties(['coupleFriendlyHotel', 'findPageCtaDesktop', 'hdCtaDesktop']),
    };
    analyticsService.hotelContentViewed(analyticsProperties);
  }

  render() {
    const { soldOut, defaultAmenities } = this.state;
    const {
      hotel,
      price,
      search,
      match,
      $roomType,
      hotelActions,
      content: { seo, reviews },
      wallet,
      isRoomPricesLoading,
      location,
      similarHotels,
    } = this.props;

    const {
      searchInput,
      datePicker,
      occupancy,
    } = search;
    const locationQuery = qs.parse(location.search);
    const days = getNoOfDays(datePicker.range.start, datePicker.range.end);
    const { room } = getOccupancy(occupancy.rooms);
    const availableRoomTypeIds = hotel ?
      this.getAvailableRoomTypeIds(hotel.roomTypeIds, price.byRoomTypeId)
      : [];
    const selectedRoomType = locationQuery.roomtype;
    const selectedRatePlan = locationQuery.rateplan;
    const selectedPrice = this.getPrice(match.params.hotelId, selectedRoomType) || price.byRoomTypeId['0|oak'];
    const isMemberDiscountAvailable = !isRoomPricesLoading ?
      selectedPrice && selectedPrice.memberDiscount && selectedPrice.memberDiscount.isAvailable
      : false;
    const taReviews = isEmpty(reviews) ? hotel.reviews : reviews;
    const isSoldOut = !hotel.isActive
      || (price.roomTypeIds.length && !availableRoomTypeIds.length)
      || soldOut;
    const similarHotelsAvailable = similarHotels.length > 0;

    return (
      <div className="hotel-details">
        {
          !hotel.isActive ? (
            <Helmet meta={[{ name: 'robots', content: 'noindex, follow' }]} />
          ) : null
       }
        <section className="hd-section hd-hotel-section">
          <div className="container">
            <Breadcrumbs />
            <div className="hd-config">
              <SearchWidget
                isSearchDisabled={
                  datePicker.range.start
                    && datePicker.range.end
                    && !isEmpty(searchInput.location)
                }
              />
            </div>
            <Hotel
              soldOut={isSoldOut}
              similarHotelsAvailable={similarHotelsAvailable}
              details={hotel}
              $roomType={$roomType}
              pricing={selectedPrice}
              selectedRoomType={selectedRoomType}
              redirectToItinerary={this.redirectToItinerary}
              selectedRatePlan={selectedRatePlan}
              occupancy={{ room, days }}
              availableRoomTypeIds={availableRoomTypeIds}
              mapThumbnail={mapThumbnail}
              bookNowClicked={hotelActions.bookNowClicked}
              defaultAmenities={defaultAmenities}
              checkPricing={this.getPricing}
              wallet={wallet}
              isPriceLoading={isRoomPricesLoading}
              taReviews={reviews}
              policies={hotel.policies}
            />
          </div>
        </section>
        <Details
          defaultAmenities={defaultAmenities}
          soldOut={isSoldOut}
          details={hotel}
          hotelId={match.params.hotelId}
          prices={price.byRoomTypeId}
          $roomType={$roomType}
          availableRoomTypeIds={availableRoomTypeIds}
          similarHotels={similarHotels}
          wallet={wallet}
          selectedRoomType={selectedRoomType}
          selectedRatePlan={selectedRatePlan}
          roomPrice={selectedPrice}
          redirectToItinerary={this.redirectToItinerary}
          guestFeedbacks={seo.content.positive_feedback}
          taReviews={taReviews}
          seoContentLinks={seo.content.links}
          QnAs={seo.content.qna}
          aboutHotel={seo.content.aboutHotel}
          hotelFacilities={seo.content.facilities}
          policies={hotel.policies}
          nearByPlaces={seo.content.nearbyPlaces}
          isMemberDiscountAvailable={isMemberDiscountAvailable}
          onLoginSuccess={this.getPricing}
        />
      </div>
    );
  }
}

HotelDetailsPage.propTypes = {
  searchActions: PropTypes.object.isRequired,
  hotelActions: PropTypes.object.isRequired,
  priceActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  wallet: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  isRoomPricesLoading: PropTypes.bool.isRequired,
  similarHotels: PropTypes.array.isRequired,
};

const mapStateToProps = (state, { match }) => ({
  user: state.user,
  hotel: state.hotel.byId[match.params.hotelId] || state.hotel.byId['0'],
  isHotelDetailsLoading: state.hotel.isHotelDetailsLoading,
  price: state.price,
  $roomType: state.$roomType,
  isRoomPricesLoading: state.price.isRoomPricesLoading,
  search: state.search,
  content: state.content,
  wallet: state.wallet,
  similarHotels: state.hotel.similarHotels,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
  priceActions: bindActionCreators(priceActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(HotelDetailsPage);
