import React from 'react';
import PropTypes from 'prop-types';

const Promises = ({ promises }) => (
  <div className="hd-promises">
    <ul className="row">
      {
        promises.map(({
          image_url: imageUrl,
          title,
          description,
          know_more_link: knowMoreLink,
        }) => (
          <li key={title} className="col">
            <img src={`${imageUrl}?w=110&h=100`} alt="" className="hd-promises__img" />
            <h3 className="subheading-1 hd-promises__title">{title}</h3>
            <p className="text-2 hd-promises__text">{description}</p>
            <a
              className="more-link"
              href={knowMoreLink}
              target="_blank"
              rel="noopener noreferrer"
            >KNOW MORE
            </a>
          </li>
        ))
      }
    </ul>
  </div>
);

Promises.propTypes = {
  promises: PropTypes.array,
};

export default Promises;
