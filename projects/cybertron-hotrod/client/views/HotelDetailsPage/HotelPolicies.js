import React from 'react';
import PropTypes from 'prop-types';
import ShowMore from '../../components/ShowMore/ShowMore';

const HotelPolicies = ({ policies }) => (
  <ShowMore
    actionClassName="hd-details__show-more-less"
    initialHeight={300}
    showMoreText="Show more"
    showLessText="Show less"
    viewId="TABS_POLICIES"
  >
    <div name="about" className="hd-section" id="t-hdPolicies">
      <h2 className="hd-section__header heading-1">Policies</h2>
      <div className="hd-section__body">
        {
          policies.map(({ title, description }) => (
            <div key={title} className="hd-policies">
              <h3 className="hd-policies__title">{title}</h3>
              <p className="text-2 hd-policies__description">
                {description}
              </p>
            </div>
          ))
        }
      </div>
    </div>
  </ShowMore>
);

HotelPolicies.propTypes = {
  policies: PropTypes.object.isRequired,
};

export default HotelPolicies;
