import React from 'react';
import PropTypes from 'prop-types';
import kebabCase from 'lodash/kebabCase';
import ShowMore from '../../components/ShowMore/ShowMore';

const FacilitiesGroup = ({
  title,
  items,
}) => (
  <div className="hd-facilities">
    <h3 className="hd-facilities__title">{title}</h3>
    <ul>
      {
        items.map((item) => (
          <li key={item.name} className="hd-facilities__item">
            <img
              className="facilities-icon-image"
              src={`https://images.treebohotels.com/images/facilities/${kebabCase(item.name)}.svg?123`}
              alt=""
            />
            <span>
              {item.name}
            </span>
          </li>
          ))
      }
    </ul>
  </div>
);

FacilitiesGroup.propTypes = {
  title: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
};

const Facilities = ({
  hotelFacilities: {
    title: facilitiesTitle,
    content,
  },
}) => (
  <ShowMore
    actionClassName="hd-details__show-more-less"
    initialHeight={300}
    showMoreText="Show more"
    showLessText="Show less"
    viewId="TABS_FACILITIES"
  >
    <div name="about" className="hd-section" id="t-hdFacilities">
      <h2 className="hd-section__header heading-1">{facilitiesTitle}</h2>
      <div className="hd-section__body">
        <div className="row">
          {
            content.map(({ title, items }) => (
              <div className="col-4">
                <FacilitiesGroup title={title} items={items} />
              </div>
            ))
          }
        </div>
      </div>
    </div>
  </ShowMore>
);

Facilities.propTypes = {
  hotelFacilities: PropTypes.object.isRequired,
};

export default Facilities;
