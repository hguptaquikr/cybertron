import React from 'react';
import PropTypes from 'prop-types';
import kebabCase from 'lodash/kebabCase';

const PositiveFeedback = ({
  guestFeedbacks: {
    title: feedbackTitle,
    items,
  },
}) => (
  <div>
    <h2 className="hd-section__header heading-1">{feedbackTitle}</h2>
    <div className="hd-section__body positive-feedback">
      <div className="row">
        {
          items && items.map(({ title, description }) => (
            <div className="col-6 positive-feedback__list-item">
              <div className="positive-feedback__icon">
                <img
                  className="feedback-icon-image"
                  src={`https://images.treebohotels.com/images/thing_guest_love/${kebabCase(title)}.svg`}
                  alt=""
                />
              </div>
              <div className="positive-feedback__feedback">
                <div className="text-2 positive-feedback__title">
                  {title}
                </div>
                <div className="text-3 positive-feedback__description">
                  {`“${description}”`}
                </div>
              </div>
            </div>
          ))
        }
      </div>
    </div>
  </div>
);

PositiveFeedback.propTypes = {
  guestFeedbacks: PropTypes.object.isRequired,
};

export default PositiveFeedback;
