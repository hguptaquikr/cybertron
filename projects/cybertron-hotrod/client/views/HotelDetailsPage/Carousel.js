import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import Gallery from './Gallery';
import Slider from '../../components/Slider/Slider';
import Modal from '../../components/Modal/Modal';
import config from '../../../config';

class Carousel extends React.Component {
  state = {
    showGallery: false,
  };

  componentDidMount() {
    const { images = {} } = this.props;
    if (images.common) {
      (new window.Image()).src = `${images.common[0].url}?w=1000&h=550&fm=pjpg&fit=crop`;
    }
  }

  showGallery = () => {
    this.setState({ showGallery: true });
  };

  hideGallery = () => {
    this.setState({ showGallery: false });
  };

  goToSlide = (currentSlide) => {
    if (this.slider) {
      this.slider.slickGoTo(currentSlide);
    }
  };

  render() {
    const {
      images = {},
      name,
      coordinates: { lat, lng },
      mapThumbnail,
      soldOut,
    } = this.props;

    const traversedImage = {};
    config.roomPriority.forEach((key) => {
      traversedImage[key] = images.filter ? images.filter((img) => img.roomType === key) : [];
    });

    const imagesArr = Object.keys(traversedImage)
      .sort((a, b) =>
        config.roomPriority.indexOf(a) - config.roomPriority.indexOf(b)).reduce((prev, image) =>
        prev.concat(traversedImage[image]), []);
    const { showGallery } = this.state;
    return (
      <div
        className={cx('hd-hotel__carousel col-7', {
          'hd-hotel__carousel--soldout': soldOut,
        })}

      >
        {
          imagesArr.slice(0, 1).map(({ url }) => (
            <meta key={url} itemProp="image" content={`https:${url}`} />
          ))
        }
        <div onClick={this.showGallery}>
          <Slider
            getSlider={(slider) => {
              this.slider = slider;
            }}
            draggable={false}
            arrows={false}
          >
            {
              imagesArr.map(({ url, tagline }) => (
                <div key={url}>
                  <img src={`${url}?w=591&h=352&fm=pjpg&fit=crop`} alt={tagline} />
                </div>
              ))
            }
          </Slider>
        </div>
        <div className="hd-hotel__secondary">
          <div className="hd-hotel__mini-carousel">
            <Slider
              slidesToShow={5}
              beforeChange={(current, next) => this.goToSlide(next)}
            >
              {
                imagesArr.map(({ url, tagline }) => (
                  <div key={url}>
                    <img src={`${url}?w=92&h=60&fm=pjpg&fit=crop`} alt={tagline} />
                  </div>
                ))
              }
            </Slider>
            <p className="more-link hd-hotel__secondary-label" onClick={this.showGallery}>
              VIEW ALL {imagesArr.length} PHOTOS
            </p>
            <Modal isOpen={showGallery} onClose={this.hideGallery}>
              <Gallery images={traversedImage} title={name} onClose={this.hideGallery} />
            </Modal>
          </div>
          <a
            className="hd-hotel__map-thumbail"
            href={`https://www.google.com/maps/search/?api=1&query=${lat},${lng}`}
            target="_blank"
            rel="noopener noreferrer nofollow"
          >
            <img className="hd-hotel__map-thumbail__image" src={mapThumbnail} alt="View map" />
            <p className="more-link hd-hotel__secondary-label hd-hotel__map-label">
              VIEW MAPS
            </p>
          </a>
        </div>
      </div>
    );
  }
}

Carousel.propTypes = {
  images: PropTypes.array,
  coordinates: PropTypes.object,
  name: PropTypes.string,
  mapThumbnail: PropTypes.string,
  soldOut: PropTypes.bool,
};

export default Carousel;
