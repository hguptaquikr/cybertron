import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import CoupleFriendlyTag from '../../../components/CoupleFriendlyTag/CoupleFriendlyTag';
import Tooltip from '../../../components/Tooltip/Tooltip';
import analyticsService from '../../../services/analytics/analyticsService';
import * as hotelService from '../../../services/hotel/hotelService';

const HotelIndicator = ({
  details: { name },
  taReviews: {
    noOfReviews,
    overallRating,
    overallRanking,
  },
  policies,
}) => {
  const isCoupleFriendly = !isEmpty(policies) ? hotelService.isCoupleFriendly(policies) : false;
  return (
    <div>
      {
        isCoupleFriendly ? (
          <Tooltip placement="bottom">
            <Tooltip.Trigger>
              <CoupleFriendlyTag />
            </Tooltip.Trigger>
            <Tooltip.Content>
              {hotelService.getCoupleFriendlyPolicyText(policies)}
            </Tooltip.Content>
          </Tooltip>
        ) : null
      }
      <div className="text-1 hd-hotel__overall-rating">
        {
          !isEmpty(overallRating) ? (
            <div
              className="hd-hotel__rating"
              onClick={() => {
                const element = document.getElementById('guestReviews');
                element.scrollIntoView();
              }}
            >
              <img
                className="hd-hotel__rating-img"
                src={overallRating.image}
                alt={`TripAdvisor Overall Rating: ${overallRating.rating}`}
                onClick={() => analyticsService.taReviewImageClicked('HD Page', name)}
              />
              <span className="hd-hotel__review-count text-2">{noOfReviews} reviews</span>
            </div>
          ) : (null)
        }
        <div className="hd-hotel__award">
          {
            !isEmpty(overallRanking) ? (
              <span className="text-2">{overallRanking.rank_string}</span>
            ) : (null)
          }
        </div>
      </div>
    </div>
  );
};

HotelIndicator.propTypes = {
  details: PropTypes.object.isRequired,
  taReviews: PropTypes.object.isRequired,
  policies: PropTypes.array.isRequired,
};

export default HotelIndicator;
