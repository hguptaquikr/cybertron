import Loadable from 'react-loadable';
import get from 'lodash/get';
import * as abService from '../../../services/ab/abService';
import importCss from '../../../services/importCss';

export default {
  tag: Loadable({
    loader: () => {
      importCss('HotelIndicator.coupleFriendlyTag');
      return import('./HotelIndicator.coupleFriendlyTag' /* webpackChunkName: 'HotelIndicator.coupleFriendlyTag' */);
    },
    loading: () => null,
  }),
  text: Loadable({
    loader: () => {
      importCss('HotelIndicator.coupleFriendlyText');
      return import('./HotelIndicator.coupleFriendlyText' /* webpackChunkName: 'HotelIndicator.coupleFriendlyText' */);
    },
    loading: () => null,
  }),
}[get(abService.getExperiments(), 'coupleFriendlyHotel.variant') || 'tag'];
