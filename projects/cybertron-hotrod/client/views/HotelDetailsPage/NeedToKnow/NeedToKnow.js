import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

const NeedToKnow = (({ policies }) => !isEmpty(policies) ? (
  <div name="about" className="hd-section">
    <h2 className="hd-section__header heading-1">Need To Know</h2>
    <div className="hd-section__body">
      {
        policies.map(({ title, description }) => (
          <div key={title} className="hd-policies">
            <h3 className="hd-policies__title">{title}</h3>
            <p className="text-2 hd-policies__description">
              {description}
            </p>
          </div>
        ))
      }
    </div>
  </div>
) : null);

NeedToKnow.propTypes = {
  policies: PropTypes.object.isRequired,
};

export default NeedToKnow;
