import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

const HotelDetailsPage = Loadable({
  loader: () => {
    importCss('HotelDetailsPage');
    return import('./HotelDetailsPage' /* webpackChunkName: 'HotelDetailsPage' */);
  },
  loading: () => null,
});

export default HotelDetailsPage;
