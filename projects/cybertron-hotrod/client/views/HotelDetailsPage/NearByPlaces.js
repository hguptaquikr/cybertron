import React from 'react';
import PropTypes from 'prop-types';
import ShowMore from '../../components/ShowMore/ShowMore';

const NearByPlacesGroup = ({
  title,
  items,
}) => (
  <div className="hd-nearby-place">
    <h3 className="hd-nearby-place__title">{title}</h3>
    <ul>
      {
        items.map((item) => (
          <li key={item.name} className="hd-nearby-place__item">
            <div className="row">
              <div className="col-8">
                {item.name}
              </div>
              <div className="col-3">
                {item.distance} Km
              </div>
            </div>
          </li>
        ))
      }
    </ul>
  </div>
);

NearByPlacesGroup.propTypes = {
  title: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
};

const NearByPlaces = ({
  nearByPlaces: {
    title: nearByTitle,
    content,
  },
}) => (
  <ShowMore
    actionClassName="hd-details__show-more-less"
    initialHeight={320}
    showMoreText="Show more"
    showLessText="Show less"
    viewId="TABS_NEARBY_PLACES"
  >
    <div name="about" className="hd-section" id="t-nearbyPlaces">
      <h2 className="hd-section__header heading-1">{nearByTitle}</h2>
      <div className="hd-section__body">
        <div className="row">
          {
            content.map(({ title, items }) => (
              <div className="col-6">
                <NearByPlacesGroup title={title} items={items} />
              </div>
            ))
          }
        </div>
      </div>
    </div>
  </ShowMore>
);

NearByPlaces.propTypes = {
  nearByPlaces: PropTypes.object.isRequired,
};

export default NearByPlaces;
