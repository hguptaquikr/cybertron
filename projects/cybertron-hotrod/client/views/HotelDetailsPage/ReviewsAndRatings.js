import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import qs from 'query-string';
import OverallRatings from '../../views/ReviewsAndRatings/OverallRatings/OverallRatings';
import AmenityRatings from '../../views/ReviewsAndRatings/AmenityRatings/AmenityRatings';
import CustomerReviews from '../../views/ReviewsAndRatings/CustomerReviews/CustomerReviews';
import analyticsService from '../../services/analytics/analyticsService';

const ReviewsRatings = ({
  taReviews: {
    overallRating,
    subRatings,
    topReviews,
    writeReviewUrl,
    readReviewsUrl,
    noOfReviews,
  },
  hotelName,
  hotelId,
}) => (
  <div className="reviews-ratings">
    {
      !isEmpty(overallRating) || !isEmpty(subRatings) ? (
        <div className="reviews-ratings__ratings">
          <div className="row">
            {
              !isEmpty(overallRating) ? (
                <div className="col-6">
                  <OverallRatings overallRating={overallRating} />
                </div>
              ) : (null)
            }
            {
              !isEmpty(subRatings) ? (
                <div className="col-6">
                  <AmenityRatings subRatings={subRatings} />
                </div>
              ) : (null)
            }
          </div>
        </div>
      ) : (null)
    }

    {
      !isEmpty(topReviews) ? (
        <div className="reviews-ratings__reviews">
          <CustomerReviews reviews={topReviews} />
        </div>
      ) : (null)
    }

    <div className="reviews-ratings__write-read">
      <div className="row row--middle">
        {
          writeReviewUrl ? (
            <div className="col-9">
              <Link
                to={{
                  pathname: `/tripadvisor/reviews-${hotelId}/`,
                  search: qs.stringify({ taWidgetUrl: `${writeReviewUrl}` }),
                }}
                rel="nofollow"
                className="reviews-ratings__write-read__write-link text-link text-2"
                onClick={() => analyticsService.writeReviewClicked(hotelName)}
                target="_blank"
              >
                <button className="reviews-ratings__write-read__write-btn button button--secondary">Write Your Review</button>
              </Link>
            </div>
          ) : (null)
        }

        {
          readReviewsUrl ? (
            <div className="col-3">
              <a
                href={readReviewsUrl}
                className="reviews-ratings__write-read__read-link more-link text-2"
                onClick={() => analyticsService.readAllReviewClicked(hotelName, 'Write Review')}
                target="_blank"
                rel="noopener noreferrer nofollow"
              >
                VIEW ALL {noOfReviews} REVIEWS
              </a>
            </div>
          ) : (null)
        }
      </div>
    </div>

    <div className="reviews-ratings__partnership">
      <span className="text-2">In partnership with</span>
      <img
        src="https://images.treebohotels.com/images/ta-horizontal-img.png"
        className="reviews-ratings__partnership__img"
        alt="TripAdvisor Logo"
      />
    </div>
  </div>
);

ReviewsRatings.propTypes = {
  taReviews: PropTypes.object.isRequired,
  hotelName: PropTypes.string.isRequired,
  hotelId: PropTypes.string.isRequired,
};

export default ReviewsRatings;
