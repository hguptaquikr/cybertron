import React from 'react';
import PropTypes from 'prop-types';
import Sticky from 'react-stickynode';
import { Link, withRouter } from 'react-router-dom';
import Scroll from 'react-scroll';
import cx from 'classnames';
import qs from 'query-string';
import isEmpty from 'lodash/isEmpty';
import RoomTypeWithPrice from './RoomTypeWithPrice/RoomTypeWithPrice';
import PositiveFeedback from './PositiveFeedback';
import ReviewsRatings from './ReviewsAndRatings';
import SimilarHotels from './SimilarHotels';
import Facilities from './Facilities';
import HotelPolicies from './HotelPolicies';
import NearByPlaces from './NearByPlaces';
import NeedToKnow from './NeedToKnow/NeedToKnow';
import SeoFooter from '../../components/Seo/SeoLinksFooter';
import ShowMore from '../../components/ShowMore/ShowMore';
import AboutHotel from './AboutHotel/AboutHotel';
import QuestionsAndAnswers from '../../views/QuestionsAndAnswers/QuestionsAndAnswers';
// import SpecialDeals from '../SpecialDeals/SpecialDeals';
import Modal from '../../components/Modal/Modal';
// import WalletBanner from '../../components/WalletBanner/WalletBanner';
import ABButton from '../../components/ABButton/ABButton';
import QualityGuaranteeBanner from '../../components/QualityGuaranteeBanner/QualityGuaranteeBanner';
import priceService from '../../services/price/priceService';
import roomTypeService from '../../services/roomType/roomTypeService';
import analyticsService from '../../services/analytics/analyticsService';

const NavPrice = (
  {
    sellingPrice,
    totalPrice,
    strikedPrice,
    discountPercentage,
    redirectToItinerary,
    selectedRoomType,
    selectedRatePlan,
  },
) => (
  <div className="col-4  hd-details__nav-price-container">
    <div className="hd-details__nav-price">
      <span>
        {
          sellingPrice || totalPrice ? (
            <span>&#x20b9;{Math.round(sellingPrice)}</span>
          ) : null
        }
      </span>
      {
        discountPercentage ? (
          <span>
            <small className="text-primary hd-hotel__price-discount">
              {discountPercentage}% off
            </small>
            <span className="hd-hotel__price-start">
              &#x20b9;{Math.round(strikedPrice)}
            </span>
          </span>
        ) : null
      }
    </div>
    {
      sellingPrice || totalPrice ? (
        <ABButton
          id="t-hdNavBookCTA"
          onClick={() => redirectToItinerary(selectedRoomType, selectedRatePlan)}
          experiment="hdCtaDesktop"
          large
          render={(text) => text || 'BOOK NOW'}
        />
      ) : null
    }
  </div>
);

NavPrice.propTypes = {
  sellingPrice: PropTypes.number,
  totalPrice: PropTypes.number,
  strikedPrice: PropTypes.number,
  discountPercentage: PropTypes.number,
  redirectToItinerary: PropTypes.func.isRequired,
  selectedRatePlan: PropTypes.string.isRequired,
  selectedRoomType: PropTypes.string.isRequired,
};

const NavItemBar = ({
  soldOut,
  nearByPlaces,
  hotelFacilities,
  isTripAdvisorEnabled,
}) => (
  <div className="col-8">
    <ul className="hd-details__nav-items">
      {
        !soldOut ? (
          <li className="hd-details__nav-item">
            <Scroll.Link href="#TABS_ROOM_TYPES" activeClass="hd-details__nav-item--active" to="TABS_ROOM_TYPES" spy smooth offset={-80} isDynamic>
              <span className="hd-details__nav-item-highlight">Room types</span>
            </Scroll.Link>
          </li>
        ) : null
      }
      {
        isTripAdvisorEnabled ? (
          <li className="hd-details__nav-item">
            <Scroll.Link href="#TABS_REVIEWS" activeClass="hd-details__nav-item--active" to="TABS_REVIEWS" spy smooth offset={-80} isDynamic>
              <span className="hd-details__nav-item-highlight">Rating &amp; Reviews</span>
            </Scroll.Link>
          </li>
        ) : null
      }
      <li className="hd-details__nav-item">
        <Scroll.Link href="#TABS_ABOUT_HOTEL" activeClass="hd-details__nav-item--active" to="TABS_ABOUT_HOTEL" spy smooth offset={-80} isDynamic>
          <span className="hd-details__nav-item-highlight">About Hotel</span>
        </Scroll.Link>
      </li>
      {
        !isEmpty(hotelFacilities) ? (
          <li className="hd-details__nav-item">
            <Scroll.Link href="#TABS_FACILITIES" activeClass="hd-details__nav-item--active" to="TABS_FACILITIES" spy smooth offset={-80} isDynamic>
              <span className="hd-details__nav-item-highlight">Facilities</span>
            </Scroll.Link>
          </li>
        ) : null
      }
      <li className="hd-details__nav-item">
        <Scroll.Link href="#TABS_POLICIES" activeClass="hd-details__nav-item--active" to="TABS_POLICIES" spy smooth offset={-60} isDynamic>
          <span className="hd-details__nav-item-highlight">Policies</span>
        </Scroll.Link>
      </li>
      {
        !isEmpty(nearByPlaces) ? (
          <li className="hd-details__nav-item">
            <Scroll.Link href="#TABS_NEARBY_PLACES" activeClass="hd-details__nav-item--active" to="TABS_NEARBY_PLACES" spy smooth offset={-60} isDynamic>
              <span className="hd-details__nav-item-highlight">Nearby Places</span>
            </Scroll.Link>
          </li>
        ) : null
      }
    </ul>
  </div>
);

NavItemBar.propTypes = {
  soldOut: PropTypes.bool.isRequired,
  nearByPlaces: PropTypes.object,
  hotelFacilities: PropTypes.object,
  isTripAdvisorEnabled: PropTypes.bool.isRequired,
};

class Details extends React.Component {
  state = {
    isModalOpen: false,
  }

  getNeedToKnowPolicies = (policies) =>
    policies.filter((policy) => policy.isNeedToKnow)

  tranformSeoContentLinks = (links) => {
    const seoContentLinks = links.map((link) => {
      const trimmedLink = { ...link };
      if (link.key === 'near_by_localities'
            || link.key === 'near_by_landmarks') {
        trimmedLink.content = link.content.slice(3);
      }
      return trimmedLink;
    });

    return !isEmpty(seoContentLinks) ? (
      <div name="similar" className="hd-section">
        <SeoFooter links={seoContentLinks} />
      </div>
    ) : null;
  };

  toggleModal = () => {
    const {
      roomPrice: { ratePlans = {} },
      selectedRatePlan,
    } = this.props;
    this.setState({ isModalOpen: !this.state.isModalOpen });
    analyticsService.viewBiggerRoomsClicked(ratePlans[selectedRatePlan]);
  }

  render() {
    const { isModalOpen } = this.state;
    const {
      details: {
        name,
        coordinates,
      },
      $roomType = {},
      prices,
      similarHotels,
      selectedRoomType,
      hotelId,
      roomPrice: { ratePlans = {} },
      soldOut,
      selectedRatePlan,
      guestFeedbacks,
      taReviews,
      redirectToItinerary,
      aboutHotel,
      hotelFacilities,
      policies,
      nearByPlaces,
      QnAs,
      availableRoomTypeIds,
      seoContentLinks,
      location,
    } = this.props;
    const {
      sellingPrice = 0,
      discountPercentage,
      strikedPrice,
      totalPrice,
    } = ratePlans[selectedRatePlan] || {};
    const hotelPrices = priceService.getPricesForRoomTypeIds(availableRoomTypeIds, prices);
    const sortedPrices = priceService.sortPrice(hotelPrices) || [];
    const selectedRoomTypeId = roomTypeService.constructRoomTypeId(hotelId, selectedRoomType);

    return (
      <div className="hd-details pos-rel">
        <div className="container">
          <QualityGuaranteeBanner
            image
            page="Hotel Detail"
          />
        </div>
        <Sticky activeClass="hd-details__navbar--sticky">
          <div className="hd-details__navbar">
            <div className="container">
              <div className="row hd-details__navbar-container">
                <NavItemBar
                  nearByPlaces={nearByPlaces}
                  hotelFacilities={hotelFacilities}
                  isTripAdvisorEnabled={taReviews.isTripAdvisorEnabled}
                  soldOut={soldOut}
                />
                {
                  !soldOut &&
                    <NavPrice
                      sellingPrice={sellingPrice}
                      totalPrice={totalPrice}
                      strikedPrice={strikedPrice}
                      discountPercentage={discountPercentage}
                      redirectToItinerary={redirectToItinerary}
                      selectedRatePlan={selectedRatePlan}
                      selectedRoomType={selectedRoomType}
                    />
                }
              </div>
            </div>
          </div>
        </Sticky>
        <div className="container">
          <Scroll.Element id="TABS_ROOM_TYPES" name="TABS_ROOM_TYPES">
            <div name="facilities" className={cx('hd-section', { hide: soldOut })} >
              <h2 className="hd-section__header heading-1" id="hd-section__room-facilities">ROOM TYPES</h2>
              {/*
                <div className="hd-section__banner">
                  {
                    isMemberDiscountAvailable && !wallet.byType.TP.isApplied ? (
                      <SpecialDeals
                        className="hd-section"
                        onLoginSuccess={onLoginSuccess}
                        src="HD"
                      />
                    ) : null
                  }
                  {
                    wallet.byType.TP.isApplied ? (
                      <WalletBanner
                        walletAmount={wallet.byType.TP.usableBalance}
                        src="HD"
                      />
                    ) : null
                  }
                </div>
              */}
              <RoomTypeWithPrice
                roomType={$roomType.byId[selectedRoomTypeId]}
                price={prices[selectedRoomTypeId]}
                selectedRatePlan={selectedRatePlan}
                onBookNowClicked={redirectToItinerary}
              />
              <div className="hd-section__view-other-rooms">
                {
                  sortedPrices.filter((priceObj) => priceObj && priceObj.available).length > 1 ? (
                    <span
                      id="t-hdViewBiggerRooms"
                      onClick={() => this.toggleModal()}
                      className="more-link hd-section__view-other-rooms__link"
                    >
                    VIEW BIGGER ROOMS
                    </span>
                  ) : (
                    <span> No Other Room Types Available </span>
                  )
                }
              </div>
              <Modal
                isOpen={isModalOpen}
                onClose={this.toggleModal}
                closeOnEsc
                closeOnBackdropClick
                backdropClassName="hd-section__view-other-rooms__modal"
              >
                <div id="room-types-modal" className="container hd-section__other-rooms">
                  <h2 className="hd-section__header heading-1" id="hd-section__room-facilities">ROOM TYPES</h2>
                  {/*
                    <div className="hd-section__banner">
                      {
                        isMemberDiscountAvailable && !wallet.byType.TP.isApplied ? (
                          <SpecialDeals
                            className="hd-section"
                            onLoginSuccess={onLoginSuccess}
                            src="HD"
                          />
                        ) : null
                      }
                      {
                        wallet.byType.TP.isApplied ? (
                          <WalletBanner
                            walletAmount={wallet.byType.TP.usableBalance}
                            src="HD"
                          />
                        ) : null
                      }
                    </div>
                  */}
                  <div className="hd-section__other-rooms__close">
                    <div className="hd-section__other-rooms__close__icon-div">
                      <i
                        className="icon-cross hd-section__other-rooms__close__icon-div-icon"
                        onClick={this.toggleModal}
                      />
                    </div>
                  </div>
                  {
                    sortedPrices.map((priceObj) => {
                      const ratePlan = priceObj.id === selectedRoomTypeId ? selectedRatePlan : '';
                      return (
                        <RoomTypeWithPrice
                          key={priceObj.id}
                          roomType={$roomType.byId[priceObj.id]}
                          price={priceObj}
                          selectedRatePlan={ratePlan}
                          onBookNowClicked={redirectToItinerary}
                        />
                      );
                    })
                  }
                </div>
              </Modal>
            </div>
          </Scroll.Element>
          <Scroll.Element id="TABS_REVIEWS" name="TABS_REVIEWS">
            <div className="hd-section" id="t-hdTripAdvisor">
              {
                !isEmpty(guestFeedbacks) ? (
                  <div name="things-guest-love">
                    <PositiveFeedback guestFeedbacks={guestFeedbacks} />
                  </div>
                ) : null
              }
              {
                taReviews.isTripAdvisorEnabled ? (
                  <ShowMore
                    actionClassName="hd-details__show-more-less"
                    initialHeight={600}
                    showMoreText="Show more"
                    showLessText="Show less"
                    viewId="TABS_REVIEWS"
                  >
                    <div name="reviews" className="ta-section" id="review-section">
                      <h2 className="hd-section__header heading-1" data-tab="TABS_REVIEWS">TRIPADVISOR RATINGS & REVIEWS FOR {name}</h2>
                      <div className="hd-section__body hd-section__reviews">
                        <div className="hd-section__text">
                          <ReviewsRatings
                            taReviews={taReviews}
                            hotelName={name}
                            hotelId={hotelId}
                          />
                        </div>
                      </div>
                    </div>
                  </ShowMore>
                ) : null
              }
            </div>
          </Scroll.Element>

          <Scroll.Element id="TABS_ABOUT_HOTEL" name="TABS_ABOUT_HOTEL">
            {
              !isEmpty(aboutHotel) ? (
                <AboutHotel
                  aboutHotel={aboutHotel}
                  seoContentLinks={seoContentLinks}
                />
              ) : null
            }
          </Scroll.Element>

          <Scroll.Element id="TABS_FACILITIES" name="TABS_FACILITIES">
            {
              !isEmpty(hotelFacilities) ? (
                <Facilities hotelFacilities={hotelFacilities} />
              ) : null
            }
          </Scroll.Element>

          <Scroll.Element>
            {
              !isEmpty(policies) ? (
                <NeedToKnow policies={this.getNeedToKnowPolicies(policies)} />
              ) : null
            }
          </Scroll.Element>

          <Scroll.Element id="TABS_POLICIES" name="TABS_POLICIES">
            {
              !isEmpty(policies) ? (
                <HotelPolicies policies={policies} />
              ) : null
            }
          </Scroll.Element>

          <Scroll.Element id="TABS_NEARBY_PLACES" name="TABS_NEARBY_PLACES">
            {
              !isEmpty(nearByPlaces) ? (
                <NearByPlaces nearByPlaces={nearByPlaces} />
              ) : null
            }
          </Scroll.Element>
          <Scroll.Element id="TABS_QNAS" name="TABS_QNAS">
            {
              !isEmpty(QnAs) ? (
                <div className="hd-section">
                  <div className="hd-section__body">
                    <QuestionsAndAnswers QnAs={QnAs} />
                  </div>
                </div>
              ) : null
            }
          </Scroll.Element>
          <Scroll.Element id="TABS_NEARBY_HOTELS" name="TABS_NEARBY_HOTELS">
            {
              similarHotels.length ? (
                <div name="similar" className="hd-section">
                  <h2 className="hd-section__header heading-1">
                    SIMILAR TREEBOS AROUND {name}
                    <Link
                      className="text-link more-link"
                      to={{
                        pathname: '/search/',
                        search: qs.stringify({
                          ...qs.parse(location.search),
                          ...coordinates,
                          hotel_id: hotelId,
                        }),
                      }}
                    >VIEW ALL
                    </Link>
                  </h2>
                  <div className="hd-section__body">
                    <SimilarHotels
                      similarHotels={similarHotels}
                      prices={prices}
                      $roomType={$roomType}
                    />
                  </div>
                </div>
              ) : null
            }
          </Scroll.Element>
          {
            this.tranformSeoContentLinks(seoContentLinks)
          }
        </div>
      </div>
    );
  }
}

Details.propTypes = {
  details: PropTypes.object.isRequired,
  prices: PropTypes.object.isRequired,
  similarHotels: PropTypes.array,
  selectedRoomType: PropTypes.string,
  selectedRatePlan: PropTypes.string.isRequired,
  hotelId: PropTypes.string,
  roomPrice: PropTypes.object,
  $roomType: PropTypes.object.isRequired,
  soldOut: PropTypes.bool,
  redirectToItinerary: PropTypes.func.isRequired,
  guestFeedbacks: PropTypes.object,
  taReviews: PropTypes.object.isRequired,
  aboutHotel: PropTypes.object,
  availableRoomTypeIds: PropTypes.array.isRequired,
  location: PropTypes.object.isRequired,
  hotelFacilities: PropTypes.object,
  policies: PropTypes.object,
  nearByPlaces: PropTypes.object,
  QnAs: PropTypes.object,
  seoContentLinks: PropTypes.array,
};

export default withRouter(Details);
