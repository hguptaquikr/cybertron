import React from 'react';
import PropTypes from 'prop-types';
import Amenities from '../Amenities';
import RatePlans from '../../../components/RatePlans/RatePlans';
import priceService from '../../../services/price/priceService';
import './roomTypeWithPrice.css';

const RoomTypeWithPrice = ({
  roomType = {},
  price = {},
  selectedRatePlan,
  onBookNowClicked,
}) => {
  const sortedRatePlans = price.ratePlans ? priceService.sortedRatePlans(price.ratePlans) : [];
  return price.available ? (
    <div key={roomType.type} className="rtwp">
      <div className="rtwp__details">
        <h3 className="flex-row rtwp__type heading-2" id="t-hdRoomType">{roomType.type}</h3>
        <p className="rtwp__desc text-2">
          <span className="rtwp__size">
            <i className="icon-sq-feet rtwp__desc-icons" />
            {roomType.area} Sqft
          </span>
          <span className="rtwp__desc-items">
            <i className="icon-guests rtwp__desc-icons" />{roomType.maxOccupancy.occupancy}
          </span>
        </p>
      </div>
      <Amenities
        section={`${roomType.type}`}
        amenities={roomType.amenities}
        amenitiesToRender={4}
      />
      <div className="rtwp__rate-plans">
        {
          sortedRatePlans.map((plan) => (
            price.ratePlans[plan] ? (
              <RatePlans
                key={plan}
                plan={plan}
                rateType={price.ratePlans[plan].type}
                roomType={roomType.type}
                isSelected={selectedRatePlan === plan}
                selectedRatePlan={selectedRatePlan}
                onBookNowClicked={onBookNowClicked}
                sellingPrice={price.ratePlans[plan].sellingPrice}
                strikedPrice={price.ratePlans[plan].strikedPrice}
                discountPercentage={price.ratePlans[plan].discountPercentage}
                selectionType="button"
              />
            ) : null
          ))
        }
      </div>
    </div>
  ) : null;
};

RoomTypeWithPrice.propTypes = {
  roomType: PropTypes.object,
  price: PropTypes.object,
  selectedRatePlan: PropTypes.string,
  onBookNowClicked: PropTypes.func.isRequired,
};

export default RoomTypeWithPrice;
