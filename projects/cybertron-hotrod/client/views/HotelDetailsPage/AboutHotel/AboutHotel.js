import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import { Link } from 'react-router-dom';
import contentService from '../../../services/content/contentService';
import StaticMap from '../../../components/StaticMap/StaticMap';
import './aboutHotel.css';

const TopReasons = ({
  reasons: {
    title,
    items,
  },
}) => (
  <div className="top-reasons">
    <h3 className="top-reasons__header">{title}</h3>
    <div className="row">
      {
        items.map((item) => (
          <div className="col-3 text-2">
            <i className="icon-extra-amenities" /> {item}
          </div>
        ))
      }
    </div>
  </div>
);

TopReasons.propTypes = {
  reasons: PropTypes.object.isRequired,
};

class AboutHotel extends React.Component {
  state = {
    showMoreText: false,
  };

  getSeoLinksContent = (category) => {
    const { seoContentLinks } = this.props;
    return seoContentLinks.find((link) => link.key === category).content;
  }

  getNearbyData = () => ({
    localities: this.getSeoLinksContent('near_by_localities').slice(0, 3),
    landmarks: this.getSeoLinksContent('near_by_landmarks').slice(0, 3),
    budgetHotel: this.getSeoLinksContent('category')
      .find((categoryData) => categoryData.category === 'Budget Hotels'),
    serviceApartment: this.getSeoLinksContent('category')
      .find((categoryData) => categoryData.category === 'Service Apartments'),
    coupleFriendly: this.getSeoLinksContent('category')
      .find((categoryData) => categoryData.category === 'Couple Friendly Hotels'),
  });

  toggleShowMoreText = () => {
    const { showMoreText } = this.state;
    this.setState({
      showMoreText: !showMoreText,
    });
  };

  render() {
    const { showMoreText } = this.state;
    const {
      aboutHotel: {
        title,
        content: {
          reasons,
          coordinates,
          description: {
            address,
            about,
          },
        },
      },
    } = this.props;

    const textLength = about.length;
    const showMoreTextEnable = textLength > 850;
    const lessText = showMoreTextEnable ? `${about.slice(0, 850)}` : about;
    const moreText = showMoreTextEnable ? ` ${about.slice(851, textLength)}` : '';
    const googleMapUrl = `https://www.google.com/maps/search/?api=1&query=${coordinates.lat},${coordinates.lng}`;
    const nearbyData = this.getNearbyData();

    return (
      <div name="about" className="hd-section about-hotel" id="t-aboutHotel">
        <h2 className="hd-section__header heading-1">{title}</h2>
        <div className="hd-section__body">
          {
            !isEmpty(reasons) ? (
              <TopReasons reasons={reasons} />
            ) : (null)
          }
          <div className="about-hotel__title">
            Address
          </div>
          <div className="row">
            <div className="col-12">
              <div className="text-1 about-hotel__address">{address}</div>
              <StaticMap coordinates={coordinates} zoom={14} width={937} height={248} />
              <a href={googleMapUrl} target="_blank" rel="noopener noreferrer">
                <button className="btn about-hotel__google-btn">
                  Locate on Google Maps <i className="icon-angle-right about-hotel__google-btn-icon" />
                </button>
              </a>
            </div>
            <div className="col-12 about-hotel__section">
              <span className="text-2 about-hotel__description" dangerouslySetInnerHTML={{ __html: lessText }} />
              {
                showMoreText ? (
                  <span className="text-2 about-hotel__description" dangerouslySetInnerHTML={{ __html: moreText }} />
                ) : null
              }
              {
                showMoreTextEnable ? (
                  <span className="about-hotel__read-link" onClick={this.toggleShowMoreText}>
                    {showMoreText ? ' Read less' : ' Read more'}
                  </span>
                ) : null
              }
              <div className="row">
                {
                  nearbyData.localities.length ? (
                    <div className="col-4 about-hotel__nearby">
                      <div className="subheading-4 about-hotel__nearby__heading">Nearby Localities</div>
                      {
                        nearbyData.localities.map((locality) => (
                          <Link
                            key={locality.area.locality}
                            className="text-link about-hotel__nearby__link"
                            to={contentService.getSeoPathname(locality)}
                          >
                            Hotels in {locality.area.locality}
                          </Link>
                        ))
                      }
                    </div>
                  ) : null
                }

                {
                  nearbyData.landmarks.length ? (
                    <div className="col-4 about-hotel__nearby">
                      <div className="subheading-4 about-hotel__nearby__heading">Nearby Landmarks</div>
                      {
                      nearbyData.landmarks.map((landmark) => (
                        <Link
                          key={landmark.area.locality}
                          className="text-link about-hotel__nearby__link"
                          to={contentService.getSeoPathname(landmark)}
                        >
                          Hotels near {landmark.area.landmark}
                        </Link>
                      ))
                    }
                    </div>
                  ) : null
                  }
                {
                  nearbyData.budgetHotel || nearbyData.serviceApartment
                    || nearbyData.coupleFriendly ? (
                      <div className="col-4 about-hotel__nearby">
                        <div className="subheading-4 about-hotel__nearby__heading">Hotel Types</div>
                        {
                          nearbyData.budgetHotel ? (
                            <Link
                              className="text-link about-hotel__nearby__link"
                              to={contentService.getSeoPathname(nearbyData.budgetHotel)}
                            >
                              Budget Hotels in {nearbyData.budgetHotel.area.city}
                            </Link>
                          ) : null
                        }
                        {
                          nearbyData.serviceApartment ? (
                            <Link
                              className="text-link about-hotel__nearby__link"
                              to={contentService.getSeoPathname(nearbyData.serviceApartment)}
                            >
                              Service Apartments in {nearbyData.serviceApartment.area.city}
                            </Link>
                          ) : null
                        }
                        {
                          nearbyData.coupleFriendly ? (
                            <Link
                              className="text-link about-hotel__nearby__link"
                              to={contentService.getSeoPathname(nearbyData.coupleFriendly)}
                            >
                              Couple Friendly Hotels in {nearbyData.coupleFriendly.area.city}
                            </Link>
                          ) : null
                        }
                      </div>
                  ) : null
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AboutHotel.propTypes = {
  aboutHotel: PropTypes.object.isRequired,
  seoContentLinks: PropTypes.object,
};

export default AboutHotel;
