import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import Slider from '../../components/Slider/Slider';

class Gallery extends React.Component {
  state = {
    filter: 'common',
  };

  componentDidMount() {
    const { images = {} } = this.props;
    const traversedImages = Object.keys(images).filter((key) => (images[key].length > 0));
    traversedImages.forEach((img) => {
      (new window.Image()).src =
        `${images[img][0].url}?w=1000&h=${this.getImageHeight()}&fm=pjpg&fit=crop`;
    });
  }

  setFilter = (filter) => {
    this.setState({ filter });
  };

  getImageHeight = () => {
    const wh = __BROWSER__ ? window.innerHeight : 760;
    let imgHeight = 550;
    if (wh < 700) {
      imgHeight = wh - 150 - (wh % 50);
    }
    return imgHeight;
  }

  sortPriority = [
    'acacia',
    'oak',
    'maple',
    'mahogany',
    'common',
  ];

  render() {
    const { title, images, onClose } = this.props;
    const { filter } = this.state;
    const traversedImages = Object.keys(images).filter((key) => (images[key].length > 0));
    return (
      <div className="hd-gallery">
        <h2 className="heading-1 hd-gallery__title">
          {title}
          <i className="icon-cross hd-gallery__close" onClick={onClose} />
        </h2>
        <div className="hd-gallery__filters">
          <ul className="hd-gallery__filters-list">
            {
              traversedImages
                .sort((a, b) => (this.sortPriority.indexOf(a) - this.sortPriority.indexOf(b)))
                .map((img) => (
                  <li
                    className={cx('text-1 hd-gallery__filters-item', {
                      'hd-gallery__filters-item--active': img === filter,
                    })}
                    key={img}
                    onClick={() => this.setFilter(img)}
                  >
                    {img}
                  </li>
                ))
            }
          </ul>
        </div>
        <div>
          {
            Object.keys(images).map((img) => (
              <div key={img}>
                {
                  filter === img ? (
                    <Slider
                      dots
                      lazyLoad
                      infinite={false}
                      arrowClass="slider__arrow--large"
                    >
                      {
                        images[img].map(({ url, tagline }) => (
                          <div key={url}>
                            <img
                              style={{ height: `${this.getImageHeight()}px` }}
                              src={`${url}?w=1000&h=${this.getImageHeight()}&fm=pjpg&fit=crop`}
                              alt={tagline}
                            />
                          </div>
                        ))
                      }
                    </Slider>
                  ) : null
                }
              </div>
            ))
          }
        </div>
      </div>
    );
  }
}

Gallery.propTypes = {
  images: PropTypes.object,
  title: PropTypes.string,
  onClose: PropTypes.func,
};

export default Gallery;
