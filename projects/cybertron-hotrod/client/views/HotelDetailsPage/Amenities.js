/* eslint-disable react/no-array-index-key */
import React from 'react';
import PropTypes from 'prop-types';
import kebabCase from 'lodash/kebabCase';

class Amenities extends React.Component {
  state = {
    showMoreAmenities: false,
  }
  showMoreAmenities = () => {
    this.setState({ showMoreAmenities: true });
  }
  renderAmenities = (amenities, section) =>
    amenities.map((amenity, i) => {
      const imgUrl = `https://images.treebohotels.com/images/facilities/${kebabCase(amenity.name)}.svg`;
      return (
        <li key={`${section}${i}`} className="hd-amenities__item">
          <img className="hd-amenities__images" src={imgUrl} alt={amenity.name} />
          {amenity.name}
        </li>
      );
    })
  renderMoreAmenities = (amenities, section) => {
    if (amenities.length) {
      const { showMoreAmenities } = this.state;
      return showMoreAmenities ? this.renderAmenities(amenities, section) : (
        <li onClick={this.showMoreAmenities}>
          <div className="hd-amenities__more">
            <span className="hd-amenities__more-number"> + {amenities.length}
            </span>
            <br />
            <span className="hd-amenities__more-text">More </span>
          </div>
        </li>
      );
    }
    return null;
  }
  render() {
    const {
      amenities,
      section,
      amenitiesToRender,
    } = this.props;

    const defaultShowAmenities = amenities.slice(0, amenitiesToRender);
    const restAmenities = amenities.slice(amenitiesToRender);
    return (
      <div className="hd-amenities" id="t-hdAmenitites">
        <ul className="hd-room__amenities hd-amenities__list">
          {
            this.renderAmenities(defaultShowAmenities, section)
          }
          {
            this.renderMoreAmenities(restAmenities, section)
          }
        </ul>
      </div>
    );
  }
}

Amenities.propTypes = {
  amenities: PropTypes.array.isRequired,
  section: PropTypes.string.isRequired,
  amenitiesToRender: PropTypes.number.isRequired,
};

export default Amenities;
