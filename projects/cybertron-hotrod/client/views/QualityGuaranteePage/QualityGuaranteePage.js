import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import * as contentActionCreators from '../../services/content/contentDuck';
import ShowMore from '../../components/ShowMore/ShowMore';
import config from '../../../config';
import './qualityGuaranteePage.css';

class QualityGuaranteePage extends Component {
  static componentWillServerRender = ({ store: { dispatch } }) =>
    dispatch(contentActionCreators.getPerfectStayContent());

  componentDidMount() {
    const { contentActions } = this.props;
    contentActions.getPerfectStayContent();
  }

  render() {
    const { perfectStay } = this.props;
    return (
      <div className="qgp">
        <Helmet meta={perfectStay.metas} />
        <div className="qgp__content container">
          <div className="row row--middle qgp__header">
            <img
              src={perfectStay.illustration}
              className="qgp__header__img"
              alt="Quality Guarantee"
            />
            <div className="qgp__header__details">
              <h1 className="heading-1 qgp__header__title"> Perfect Stay or Don&apos;t Pay </h1>
              <p className="subheading-1 qgp__header__subtitle">100% money-back promise</p>
            </div>
          </div>
          <div className="qgp__container">
            <div className="qgp__htc">
              <p className="qgp__htc__title">Need help with your stay?</p>
              <ul className="row row--between">
                <li className="col-3">
                  <img
                    src={`${config.imageUrl}/images/hotrod/contact-us.svg`}
                    className="qgp__htc__img"
                    alt="Not a perfect stay"
                  />
                  <p className="text-3">Step 1</p>
                  <p className="subheading-1 qgp__htc__step">Let us know</p>
                  <span className="text-2 qgp__htc__subtitle">Call us at +91 9322800100</span>
                </li>
                <li className="col-3">
                  <img
                    src={`${config.imageUrl}/images/hotrod/give-us-30-mins.svg`}
                    className="qgp__htc__img"
                    alt="Let us know"
                  />
                  <p className="text-3">Step 2</p>
                  <p className="subheading-1 qgp__htc__step">Give us 30 minutes</p>
                  <span className="text-2 qgp__htc__subtitle">We will resolve the issue</span>
                </li>
                <li className="col-3">
                  <img
                    src={`${config.imageUrl}/images/hotrod/refund.svg`}
                    className="qgp__htc__img"
                    alt="And if we can't"
                  />
                  <p className="text-3">Step 3</p>
                  <p className="subheading-1 qgp__htc__step">And if we can’t</p>
                  <span className="text-2 qgp__htc__subtitle">100% money-back upto ₹1200</span>
                </li>
              </ul>
            </div>
            <div className="qgp__promises">
              <h2 className="heading-1 qgp__promises__title">What does this cover?</h2>
              <div className="row">
                <ul className="col-6">
                  <li className="qgp__promise">
                    <i className="icon-room qgp__promise__icon" />
                    <div className="qgp__promise__desc">
                      <p className="subheading-1 qgp__promise__title">Comfortable Rooms</p>
                      <span className="text-1 qgp__promise__text">Fresh linen & clean washroom</span>
                    </div>
                  </li>
                  <li className="qgp__promise">
                    <i className="icon-breakfast qgp__promise__icon" />
                    <div className="qgp__promise__desc">
                      <p className="subheading-1 qgp__promise__title">Wholesome Breakfast</p>
                      <span className="text-1 qgp__promise__text">
                        A minimum of 1 main dish, tea/coffee, juice/cut fruits
                      </span>
                    </div>
                  </li>

                </ul>
                <ul className="col-6">
                  <li className="qgp__promise">
                    <i className="icon-staff qgp__promise__icon " />
                    <div className="qgp__promise__desc">
                      <p className="subheading-1 qgp__promise__title">Best-in-class Service</p>
                      <span className="text-1 qgp__promise__text">
                        Well trained staff and 24x7 support
                      </span>
                    </div>
                  </li>
                  <li className="qgp__promise">
                    <i className="icon-ac qgp__promise__icon" />
                    <div className="qgp__promise__desc">
                      <p className="subheading-1 qgp__promise__title">Assured Room Amenities</p>
                      <span className="text-1 qgp__promise__text">
                        DTH or Cable TV, WiFi* with min speed of 1 mbps (upto 500 MB), AC*.
                      </span>
                    </div>
                  </li>
                </ul>
              </div>
              <p className="qgp__promise__tnc">*Not applicable at hill stations</p>
            </div>
            <div className="qgp__everything-else">
              <h2 className="heading-1 qgp__everything-else__title">Other issues?</h2>
              <p className="text-1 qgp__everything-else__subtitle">
                We are here to resolve any issue you face while staying at a Treebo.
              </p>
              <div className="row qgp__everything-else__details">
                <div className="col-3">
                  <p className="text-1 qgp__everything-else__text">Mail us at</p>
                  <a
                    className="text-link"
                    href="mailto:hello@treebohotels.com?Subject=Perfect Stay Or Don't Pay "
                  >
                    hello@treebohotels.com
                  </a>
                </div>
                <div className="col-3">
                  <p className="text-1 qgp__everything-else__text">Call us on</p>
                  <p className="text-link">+91 9322800100</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="qgp__tnc container">
          <ShowMore
            actionClassName="qgp__show-more-less"
            initialHeight={200}
            showMoreText="Show more"
            showLessText="Show less"
          >
            <div>
              <h2 className="subheading-1 qgp__tnc__title">Terms and Conditions</h2>
              <ul className="qgp__tnc__details">
                <li className="qgp__tnc__details__item">
                  Upon receipt of any complaint from the customer against
                  the Guaranteed Services offered by Treebo,Treebo will pay
                  a maximum refund amount of ₹1200/-
                  (Rupees One Thousand and Two Hundred Only) per booking.
                </li>
                <li className="qgp__tnc__details__item">
                  Complaints shall be raised during
                  Customer’s stay at Treebo Hotels only.
                  Any Complaints raised post check-out
                  from Treebo Hotels shall be entertained at Treebo’s discretion.
                </li>
                <li className="qgp__tnc__details__item">
                  Complaints can be raised either by calling
                  Treebo customer care or through an email at hello@treebohotels.com.
                </li>
                <li className="qgp__tnc__details__item">
                  This offer shall not be applicable in the event Treebo
                  rectifies any complaint received by the
                  Customer within 30 minutes from the receipt of such Complaint.
                </li>
                <li className="qgp__tnc__details__item">
                  Terms of Service and Privacy Policy as mentioned at
                  www.treebo.com shall be applicable.
                </li>
                <li className="qgp__tnc__details__item">
                  Treebo reserves the right to rescind this offer at its sole discretion.
                </li>
              </ul>
            </div>
          </ShowMore>
        </div>
      </div>
    );
  }
}

QualityGuaranteePage.propTypes = {
  perfectStay: PropTypes.object,
  contentActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  perfectStay: state.content.perfectStay,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(QualityGuaranteePage);
