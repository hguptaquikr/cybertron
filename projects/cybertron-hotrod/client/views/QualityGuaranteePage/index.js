import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

const QualityGuaranteePage = Loadable({
  loader: () => {
    importCss('QualityGuaranteePage');
    return import('./QualityGuaranteePage' /* webpackChunkName: 'QualityGuaranteePage' */);
  },
  loading: () => null,
});

export default QualityGuaranteePage;
