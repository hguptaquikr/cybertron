import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import * as contentActionCreators from '../../../services/content/contentDuck';
import analyticsService from '../../../services/analytics/analyticsService';
import AuthenticateUser from '../AuthenticateUser/AuthenticateUser';

class AuthPage extends Component {
  static componentWillServerRender = ({ store: { dispatch, getState }, match, route }) => {
    const promises = [];

    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));

    if (isEmpty(getState().content.cities)) {
      promises.push(dispatch(contentActionCreators.getLandingContent()));
    }

    return Promise.all(promises);
  }

  componentDidMount() {
    const { cities, contentActions, route, match } = this.props;

    contentActions.getSeoContent(route, match.params);
    if (isEmpty(cities)) contentActions.getLandingContent();
  }

  onLoginSuccess = (type) => {
    const { history } = this.props;
    analyticsService.loginSuccess(type);
    history.push('/');
  }

  render() {
    return (
      <AuthenticateUser
        isModal={false}
        onLoginSuccess={this.onLoginSuccess}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  cities: state.content.cities,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

AuthPage.propTypes = {
  history: PropTypes.object.isRequired,
  cities: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AuthPage);
