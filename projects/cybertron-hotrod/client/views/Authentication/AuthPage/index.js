import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

const AuthPage = Loadable({
  loader: () => {
    importCss('AuthPage');
    return import('./AuthPage' /* webpackChunkName: 'AuthPage' */);
  },
  loading: () => null,
});

export default AuthPage;
