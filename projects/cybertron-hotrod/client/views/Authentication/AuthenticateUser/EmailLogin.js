import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import FormsyInput from '../../../components/Formsy/FormsyInput';

class Login extends Component {
  componentDidMount() {
  }

  render() {
    const { onUserLogin, setFlow } = this.props;
    return (
      <div className="col-4 email-login">
        <div className="email-login__form">
          <Formsy onValidSubmit={onUserLogin}>
            <FormsyInput
              type="text"
              name="email"
              validations="isEmail"
              validationError="Please enter a valid email"
              inputClassName="email-login__form__email"
              label="Email"
              required
            />
            <FormsyInput
              type="password"
              name="password"
              label="Password"
              inputClassName="email-login__form__password"
              required
            />
            <a
              onClick={() => setFlow('/forgot-password/')}
              id="forgot-password"
              className="text-link"
            >
              Forgot Password?
            </a>
            <button
              id="t-loginButton"
              type="submit"
              name="button"
              className="button au__button login__button"
            >Login
            </button>
          </Formsy>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  onUserLogin: PropTypes.func.isRequired,
  setFlow: PropTypes.func.isRequired,
};

export default Login;
