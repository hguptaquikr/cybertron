import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import config from '../../../../config';
import Otp from '../../../components/Otp/Otp';
import EmailLogin from './EmailLogin';
import analyticsService from '../../../services/analytics/analyticsService';

class MobileLogin extends Component {
  state = {
    mobile: '',
    showOtp: true,
  };

  onEditMobile = () => {
    const { mobile } = this.state;
    analyticsService.editMobileClicked(mobile, 'Login');
    this.setState({
      mobile: '',
    });
  };

  onNumberSubmit = ({ mobile }) => {
    analyticsService.onOtpContinueClicked(mobile, 'Login');
  }

  resendOtp = () => {
    const { mobile } = this.state;
    const {
      userActions: { sendLoginOtp },
    } = this.props;
    sendLoginOtp(mobile);
    analyticsService.resendOtpClicked(mobile, 'Login');
  }

  toggleShowOtp = () => {
    this.setState({
      showOtp: !this.state.showOtp,
    });
  }

  verifyOtp = ({ otp }, reset, invalidate) => {
    const { mobile } = this.state;
    const {
      onLoginSuccess,
      userActions: { verifyLoginOtp },
    } = this.props;
    analyticsService.verifyOtpClicked(mobile, 'Login');
    verifyLoginOtp(mobile, otp)
      .then(() => {
        this.setState({ mobile: '' });
        analyticsService.verifyOtpStatus(true, 'Login');
        onLoginSuccess('phone');
      })
      .catch((err) => {
        analyticsService.verifyOtpStatus(false, 'Login');
        invalidate({ otp: err[0].message });
      });
  }

  sendOtp = ({ mobile }, reset, invalidate) => {
    const {
      userActions: { sendLoginOtp },
    } = this.props;
    sendLoginOtp(mobile)
      .then((res) => {
        if (res.error) {
          invalidate({ mobile: res.payload.errors[0].message });
        } else {
          this.setState({ mobile });
        }
      })
      .catch((err) => invalidate({ mobile: err[0].message }));
  }

  render() {
    const { mobile, showOtp } = this.state;
    const {
      onError,
      onFacebookLogin,
      onGoogleLogin,
      setFlow,
      onUserLogin,
      errorMsg,
    } = this.props;
    return (
      <div className="container login__page">
        <div className="row">
          <div className="col-5">
            <img
              src="https://s3-ap-southeast-1.amazonaws.com/treebo/static/images/hotrod/why-treebo-login.svg"
              className="login__page__why-treebo"
              alt="wallet"
            />
          </div>
          <div className="col-1" />
          {
            showOtp ? (
              <div className="col-4 mb-login">
                <Otp
                  onEditMobile={this.onEditMobile}
                  mobile={mobile}
                  onResend={this.resendOtp}
                  sendOtp={this.sendOtp}
                  onVerifyOtp={this.verifyOtp}
                  onNumberSubmit={this.onNumberSubmit}
                />
                {
                  !mobile ? (
                    <div>
                      <h3 className="au__subheader text--center">or login with</h3>
                      <div className="au__social">
                        <FacebookLogin
                          appId={config.facebookAppId}
                          autoload
                          xfbml
                          cookie
                          scope="email"
                          fields="name, email"
                          callback={onFacebookLogin}
                          version="2.5"
                          cssClass="button button--facebook"
                          icon={<img src="https://images.treebohotels.com/images/hotrod/facebook-letter-logo.svg" className="icon-facebook" alt="facebook" />}
                          textButton="FACEBOOK"
                        />
                        <GoogleLogin
                          className="button button--google"
                          clientId={config.googleAuthId}
                          onSuccess={onGoogleLogin}
                          onFailure={onError}
                          tag="button"
                        >
                          <img src="https://images.treebohotels.com/images/hotrod/group-7.svg" className="icon-google-plus" alt="google" />
                          GOOGLE
                        </GoogleLogin>
                      </div>
                      <h3 className="au__email-subheader">Already have an account?
                        <span
                          onClick={this.toggleShowOtp}
                          className="text-link"
                          id="email"
                        > Login with email
                        </span>
                      </h3>
                    </div>
                  ) : null
                }
              </div>
            ) : (
              <EmailLogin
                setFlow={setFlow}
                onUserLogin={onUserLogin}
              />
            )
          }
          <div className="col-2" />
          <div className="col-4" />
          <div className="col-8 login__page__error">
            {
              errorMsg ? (
                <p className="au__alert">
                  {errorMsg}
                  <span
                    className="text-link"
                    id="continue"
                    onClick={this.toggleShowOtp}
                  >
                     Continue with mobile no.
                  </span>
                </p>
              ) : null
            }
          </div>
        </div>
      </div>
    );
  }
}

MobileLogin.propTypes = {
  onUserLogin: PropTypes.func.isRequired,
  userActions: PropTypes.object.isRequired,
  onLoginSuccess: PropTypes.func.isRequired,
  onFacebookLogin: PropTypes.func.isRequired,
  onGoogleLogin: PropTypes.func.isRequired,
  errorMsg: PropTypes.string,
  onError: PropTypes.func.isRequired,
  setFlow: PropTypes.func.isRequired,
};

export default MobileLogin;
