import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import MobileLogin from './MobileLogin';
import ForgotPassword from './ForgotPassword';
import * as userActionCreators from '../../../services/user/userDuck';
import * as otpActionCreators from '../../../services/otp/otpDuck';
import analyticsService from '../../../services/analytics/analyticsService';
import './authenticateUser.css';

class AuthenticateUser extends Component {
  state = {
    resetPasswordEmailId: '',
    errorMsg: '',
    successMsg: '',
    flow: '/login/',
  };

  componentWillMount() {
    this.autoDecideFlow();
  }

  onUserLogin = (credentials) => {
    const { userActions: { userLogin }, onLoginSuccess } = this.props;
    analyticsService.loginTypeSelected('login');
    userLogin(credentials)
      .then(onLoginSuccess)
      .catch(this.showError);
  }

  onFacebookLogin = (response) => {
    const { userActions: { facebookLogin }, onLoginSuccess } = this.props;
    analyticsService.loginTypeSelected('facebook');
    if (response.accessToken) {
      facebookLogin(response)
        .then(() => {
          onLoginSuccess('facebook');
        })
        .catch(this.showError);
    } else {
      this.showError(response);
    }
  }

  onGoogleLogin = (response) => {
    const { userActions: { googleLogin }, onLoginSuccess } = this.props;
    analyticsService.loginTypeSelected('google');
    googleLogin(response)
      .then(() => {
        onLoginSuccess('google');
      })
      .catch(this.showError);
  }

  onForgotPassword = (userData) => {
    this.sendResetLink(userData.email);
    this.setState({ resetPasswordEmailId: userData.email });
  }

  onResendResetPassword = () => {
    const { resetPasswordEmailId } = this.state;
    this.sendResetLink(resetPasswordEmailId);
  }

  setFlow = (flow) => {
    const { isModal, history } = this.props;
    this.setState({
      flow,
      errorMsg: '',
      successMsg: '',
    });
    if (!isModal) {
      history.push(flow);
    }
  }

  sendResetLink = (emailId) => {
    const { userActions: { forgotPassword } } = this.props;
    forgotPassword(emailId)
      .then(() => {
        this.showSuccess(
          `An email with a link to reset your password
          has been sent to`,
        );
      })
      .catch(this.showError);
  }

  autoDecideFlow = () => {
    const { isModal, location } = this.props;
    if (!isModal) {
      this.setState({ flow: location.pathname });
    }
  }

  showError = (errors = []) => {
    const errorMsg = errors[0] ? errors[0].message : '';
    this.setState({
      errorMsg,
      successMsg: '',
    });
  }

  showSuccess = (successMsg) => {
    this.setState({
      successMsg,
      errorMsg: '',
    });
  }

  render() {
    const { flow, successMsg, errorMsg, resetPasswordEmailId } = this.state;
    const {
      userActions,
      onLoginSuccess,
    } = this.props;

    return (
      <div className="au">
        <div className="au__container">
          {
            {
              '/login/': (
                <MobileLogin
                  userActions={userActions}
                  onLoginSuccess={onLoginSuccess}
                  onUserLogin={this.onUserLogin}
                  onFacebookLogin={this.onFacebookLogin}
                  onGoogleLogin={this.onGoogleLogin}
                  onError={this.showError}
                  setFlow={this.setFlow}
                  errorMsg={errorMsg}
                />
              ),
              '/forgot-password/': (
                <ForgotPassword
                  resetPasswordEmailId={resetPasswordEmailId}
                  onResend={this.onResendResetPassword}
                  onForgotPassword={this.onForgotPassword}
                  successMsg={successMsg}
                  errorMsg={errorMsg}
                  setFlow={this.setFlow}
                />
              ),
            }[flow]
          }
        </div>
      </div>
    );
  }
}

AuthenticateUser.propTypes = {
  userActions: PropTypes.object.isRequired,
  isModal: PropTypes.bool.isRequired,
  onLoginSuccess: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
  otpActions: bindActionCreators(otpActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(null, mapDispatchToProps),
)(AuthenticateUser);
