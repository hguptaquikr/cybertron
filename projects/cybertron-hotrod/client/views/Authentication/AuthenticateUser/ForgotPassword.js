import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import FormsyInput from '../../../components/Formsy/FormsyInput';

class ForgotPassword extends Component {
  componentDidMount() {
  }
  render() {
    const {
      onForgotPassword,
      setFlow,
      successMsg,
      errorMsg,
      onResend,
      resetPasswordEmailId,
    } = this.props;
    return (
      <div className="container forgot-pass">
        <div className="row">
          <div className="col-4">
            <img
              src="https://images.treebohotels.com/images/hotrod/login-why-treebo.svg"
              className="login__page__why-treebo"
              alt="wallet"
            />
          </div>
          <div className="col-8 forgot-pass__body">
            <div className="forgot-pass__form">
              {
                !successMsg ? (
                  <div>
                    <div className="subheading-4 forgot-pass__body--text">
                      Please enter your registered email to receive a
                      verification link to reset your password
                    </div>
                    <Formsy onValidSubmit={onForgotPassword}>
                      <FormsyInput
                        type="text"
                        name="email"
                        validations="isEmail"
                        validationError="Please enter a valid email"
                        label="Email"
                        value={resetPasswordEmailId}
                        required
                      />
                      <button
                        id="t-resetLink"
                        type="submit"
                        name="button"
                        className="button au__button login__button"
                      >SEND RESET LINK
                      </button>
                    </Formsy>
                  </div>
                ) : (
                  <div>
                    <p className="forgot-pass__body--text">
                      {successMsg} <b>{resetPasswordEmailId}</b>
                      <span
                        className="text-link"
                        id="edit"
                        onClick={() => setFlow('/forgot-password/')}
                      > Edit
                      </span>
                    </p>
                    <p className="forgot-pass__body--text">
                      Didn’t receive the email?
                      <span
                        className="text-link"
                        id="resend"
                        onClick={onResend}
                      > Resend
                      </span>
                    </p>
                    <button
                      className="button au__button login__button"
                      onClick={() => setFlow('/login/')}
                    >
                      Done
                    </button>
                  </div>
                )
              }
            </div>
            {
              errorMsg ? (
                <p className="au__alert">
                  {errorMsg}
                  <span
                    className="text-link"
                    id="continue"
                    onClick={() => setFlow('/login/')}
                  >
                     Continue with mobile no.
                  </span>
                </p>
              ) : null
            }
          </div>
        </div>
      </div>
    );
  }
}

ForgotPassword.propTypes = {
  onForgotPassword: PropTypes.func.isRequired,
  onResend: PropTypes.func.isRequired,
  resetPasswordEmailId: PropTypes.string.isRequired,
  successMsg: PropTypes.string,
  setFlow: PropTypes.func.isRequired,
  errorMsg: PropTypes.string,
};

export default ForgotPassword;
