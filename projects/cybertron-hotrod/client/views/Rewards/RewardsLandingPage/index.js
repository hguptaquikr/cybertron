import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

const RewardsLandingPage = Loadable({
  loader: () => {
    importCss('RewardsLandingPage');
    return import('./RewardsLandingPage' /* webpackChunkName: 'RewardsLandingPage' */);
  },
  loading: () => null,
});

export default RewardsLandingPage;
