import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import qs from 'query-string';
import * as contentActionCreators from '../../../services/content/contentDuck';
import Modal from '../../../components/Modal/Modal';
import AuthenticateUser from '../../Authentication/AuthenticateUser/AuthenticateUser';
import analyticsService from '../../../services/analytics/analyticsService';
import authService from '../../../services/auth/authService';
import './rewardsLandingPage.css';

class RewardsLandingPage extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) =>
    dispatch(contentActionCreators.getSeoContent(route, match.params))

  state = {
    isLoginModalOpen: false,
  }

  componentWillMount() {
    const { location, history } = this.props;

    authService.hasSeenRewardsLandingPage(location, history);
  }

  componentDidMount() {
    const { contentActions, route, match, location } = this.props;
    contentActions.getSeoContent(route, match.params);
    const query = qs.parse(location.search);
    analyticsService.rewardsLandingContentViewed({
      source: query.flow,
    });
  }

  onLoginSuccess = () => {
    this.toggleLoginModal();
    this.props.history.push('/rewards/');
  }

  toggleLoginModal = () => {
    this.setState({ isLoginModalOpen: !this.state.isLoginModalOpen });
  }

  handleSignUp = () => {
    this.toggleLoginModal();
    analyticsService.rewardsLandingActionClicked();
  }

  render() {
    const { isLoginModalOpen } = this.state;
    const { user } = this.props;
    return (
      <div className="rwl">
        <img
          src="https://images.treebohotels.com/images/hotrod/RewardsLandingBanner.jpg"
          className="container rwl__banner"
          alt="earn"
        />
        <div id="earn" className="container rwl__container">
          <div className="rwl__intro text--center">
            BOOKING DIRECTLY ON TREEBO PAYS!
          </div>
          <div className="rwl__intro--sub text--center">
            Get our best rates and earn Treebo Points!
          </div>
          <div className="row rwl__info">
            <div className="col-6">
              <div className="row">
                <div className="col-1 rwl__info--rate-img">
                  <img
                    src="https://images.treebohotels.com/images/hotrod/our-lowest-rates-40-px.svg"
                    alt="Treebo"
                  />
                </div>
                <div className="col-11 rwl__info--rate-text">
                  <div className="text-1 rwl__info__earn-text--header rwl--margin-bottom">Our Best Rates</div>
                  <div className="rwl--margin-bottom">Members of the program get our best rates.</div>
                </div>
                <div className="col-1 rwl__info--earn-img">
                  <img
                    src="https://s3-ap-southeast-1.amazonaws.com/treebo/static/images/hotrod/redeem-points-loyalty-40-px.svg"
                    alt="Treebo"
                  />
                </div>
                <div className="col-11 rwl__info__earn-text">
                  <div className="text-1 rwl__info__earn-text--header rwl--margin-bottom">Earn Treebo Points</div>
                  <div className="rwl--margin-bottom">
                    Earn for every stay booked directly with us.
                  </div>
                </div>
                {
                  user.isAuthenticated ? (
                    <Link
                      id="book"
                      onClick={analyticsService.rewardsLandingActionClicked}
                      className="button rwl__book-button"
                      to="/rewards/"
                    >
                      VIEW YOUR TREEBO POINTS
                    </Link>
                  ) : (
                    <button
                      id="book"
                      className="button rwl__book-button"
                      onClick={this.handleSignUp}
                    >
                      SIGN UP FOR REWARD POINTS
                    </button>
                  )
                }
                <Modal
                  isOpen={isLoginModalOpen}
                  onClose={this.toggleLoginModal}
                  closeOnEsc
                  closeOnBackdropClick
                >
                  <AuthenticateUser
                    isModal
                    onLoginSuccess={this.onLoginSuccess}
                  />
                </Modal>
                <div className="rwl__more">
                  Want more details? View detailed
                  <Link
                    id="terms"
                    to="/faq/"
                    target="_blank"
                    className="text-link rwl__more--link"
                  >
                    T&Cs.
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-6">
              <img
                src="https://images.treebohotels.com/images/hotrod/group-9.svg"
                alt="Treebo"
              />
            </div>
          </div>
        </div>
        <div id="steps" className="container rwl__earn-info">
          <div className="container rwl__earn-info__container">
            <div className="rwl__intro text--center">
              How do Treebo Points Work?
            </div>
            <div className="rwl__intro--sub text--center">
              A step by step guide on earning and using Treebo Points
            </div>
            <div className="row">
              <div className="col-4">
                <div className="rwl__earn-info--step-img">
                  <img
                    src="https://images.treebohotels.com/images/hotrod/loyalty-book-a-treebo-72-px.svg"
                    alt="Treebo"
                  />
                </div>
                <div className="text-1 rwl__earn-info--step-header">Book via Treebo</div>
                <div className="rwl__earn-info--step-subheader">Book a Treebo Hotel, directly on <br />treebo.com or the Treebo apps.</div>
              </div>
              <div className="col-4">
                <div className="rwl__earn-info--step-img">
                  <img
                    src="https://images.treebohotels.com/images/hotrod/loyalty-stay-and-earn-72-px.svg"
                    alt="Treebo"
                  />
                </div>
                <div className="text-1 rwl__earn-info--step-header">Stay and Earn</div>
                <div className="rwl__earn-info--step-subheader">
                  Treebo Points get credited to your <br />
                  account on checkout. <b> 1 Treebo Point = ₹1 </b>
                </div>
              </div>
              <div className="col-4">
                <div className="rwl__earn-info--step-img">
                  <img
                    src="https://images.treebohotels.com/images/hotrod/loyalty-redeem-points-72-px.svg"
                    alt="Treebo"
                  />
                </div>
                <div className="text-1 rwl__earn-info--step-header">Use Treebo Points</div>
                <div className="rwl__earn-info--step-subheader">
                  Points earned can be used on your future stays with us,
                  booked directly on Treebo.
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

RewardsLandingPage.propTypes = {
  location: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(RewardsLandingPage);
