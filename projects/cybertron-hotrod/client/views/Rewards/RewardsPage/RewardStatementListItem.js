import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import cx from 'classnames';
import { constants } from '../../../services/utils';

const RewardStatementListItem = ({ statement }) => {
  const formatDate = (date) => moment(date).format(constants.dateFormat.history).substring(4);
  const isDebit = statement.type === 'Dr';
  const isExpired = statement.status === 'expired';
  let walletImage = 'https://images.treebohotels.com/images/hotrod/loyalty-expired.svg';
  if (isDebit && !isExpired) {
    walletImage = 'https://images.treebohotels.com/images/hotrod/loyalty_burned.svg';
  } else if (!isDebit && !isExpired) {
    walletImage = 'https://images.treebohotels.com/images/hotrod/loyalty-earned.svg';
  }

  return (
    <div
      id={cx({
        expired: isExpired,
        debit: isDebit,
        credit: !isDebit,
      })}
      className="row wallet-statement-item"
    >
      <div className="col-4 wallet-statement-item__reward-details">
        <div className="row">
          <div className="col-1">
            <img
              src={walletImage}
              alt="wallet"
            />
          </div>
          <div className="col-11">
            <div className={cx('subheading-4 wallet-statement-item--particular', {
              'wallet-statement-item--expired': isExpired,
            })}
            >
              {statement.particular}
            </div>
            <div className="wallet-statement-item__reward-details--expiry">
              {
                isExpired ? (
                  <span>
                    <span className="wallet-statement-item__reward-details--expired">Expired</span>
                    <span className="wallet-statement-item__reward-details--expired-date">
                      {`on ${formatDate(statement.expiresOn)}`}
                    </span>
                  </span>
                ) : null
              }
              {
                !isExpired && statement.expiresOn ? `Expires on ${formatDate(statement.expiresOn)}` : null
              }
            </div>
          </div>
        </div>
      </div>
      <div className="col-5 wallet-statement-item__transaction-info">
        <div>
          {statement.id}
        </div>
        <div className="wallet-statement-item__transaction-info--created-at">
          {formatDate(statement.createdAt)}
        </div>
        {
          statement.breakup.length ? (
            statement.breakup.map((subStatement) => (
              <div>
                <div className="wallet-statement-item__transaction-breakup">
                  <div className={cx('subheading-4 wallet-statement-item__transaction-breakup--header', {
                    'wallet-statement-item--expired': subStatement.status === 'expired',
                  })}
                  >
                    {subStatement.particular}
                  </div>
                  <div className="wallet-statement-item__transaction-breakup--expiry">
                    {
                      subStatement.status !== 'expired' ? (
                        <span>
                          <span className="wallet-statement-item__reward-details--expired">Expired</span>
                          <span>
                            {`on ${formatDate(subStatement.expiresOn)}`}
                          </span>
                        </span>
                      ) : null
                    }
                    {
                      subStatement.status === 'expired' && subStatement.expiresOn ?
                        `Expires on ${formatDate(subStatement.expiresOn)}` : null
                    }
                  </div>
                </div>
              </div>
            ))
          ) : null
        }
      </div>
      <div className="col-3">
        <div className={cx('text-primary wallet-statement-item__reward-amount', {
          'wallet-statement-item__reward-amount--credit': !isDebit,
          'wallet-statement-item__reward-amount--debit': isDebit,
        })}
        >
          {
            isDebit ? `- ₹${statement.amount}` : `+ ₹${statement.amount}`
          }
        </div>
        {
          statement.breakup.length ? (
            statement.breakup.map((subStatement) => (
              <div>
                <div className="wallet-statement-item__reward-breakup">
                  <div className="text-primary wallet-statement-item__reward-breakup--amount">
                    {
                      subStatement.type === 'Dr' ? `- ₹${subStatement.amount}` : `+ ₹${subStatement.amount}`
                    }
                  </div>
                </div>
              </div>
            ))
          ) : null
        }
      </div>
    </div>
  );
};

RewardStatementListItem.propTypes = {
  statement: PropTypes.object.isRequired,
};

export default RewardStatementListItem;
