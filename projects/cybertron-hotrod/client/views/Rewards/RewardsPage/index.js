import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

const RewardsPage = Loadable({
  loader: () => {
    importCss('RewardsPage');
    return import('./RewardsPage' /* webpackChunkName: 'RewardsPage' */);
  },
  loading: () => null,
});

export default RewardsPage;
