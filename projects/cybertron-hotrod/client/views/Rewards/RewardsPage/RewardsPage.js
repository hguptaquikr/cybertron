import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import qs from 'query-string';
import RewardStatement from './RewardStatement';
import analyticsService from '../../../services/analytics/analyticsService';
import * as walletActionCreators from '../../../services/wallet/walletDuck';
import * as contentActionCreators from '../../../services/content/contentDuck';
import './rewardsPage.css';

class Rewards extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) =>
    dispatch(contentActionCreators.getSeoContent(route, match.params));

  componentDidMount() {
    const {
      contentActions,
      walletActions,
      location,
      route,
      match,
    } = this.props;
    contentActions.getSeoContent(route, match.params);
    walletActions.getWalletBalance();
    walletActions.getWalletStatement()
      .then((response) => {
        analyticsService.rewardsContentViewed({
          isWalletUsed: !!response.length,
          source: qs.parse(location.search).flow,
        });
      });
  }

  render() {
    const {
      wallet: {
        byType: {
          TP,
        },
        statements,
      },
    } = this.props;
    return (
      <div className="wallet">
        <img
          src="https://images.treebohotels.com/images/hotrod/rewards-steps.jpg"
          className="container wallet__banner"
          alt="banner"
        />
        <div className="container wallet__container">
          <div className="row wallet__balance">
            <div className="col-4">
              <i className="" />
              <div className="subheading-4 wallet__balance--conversion">
                Treebo Points
              </div>
              <div>
                1 Treebo Point =  ₹ 1
                <Link
                  to="/introducing-rewards/?intro=true&flow=rewards"
                  className="text-link wallet__balance--landing-link"
                >
                  View Details
                </Link>
              </div>
            </div>
            <div className="col-5" />
            <div className="col-3">
              <div id="amount" className="heading-2 wallet__balance--amount">
                ₹{TP.totalBalance}
              </div>
              <div>
                worth of Treebo Points
              </div>
            </div>
          </div>
        </div>
        {
          !statements.length ? (
            <div id="wallet-statement" className="container text--center wallet__new-user">
              <img
                src="https://images.treebohotels.com/images/hotrod/empty-state-treebo-points-empty.svg"
                className="wallet__new-user--img"
                alt="earn"
              />
              <div className="subheading-4 wallet__new-user--text">
                Earn Treebo Points
              </div>
              <div className="wallet__new-user--info">
                Book and stay with us to start earning <br />
                Treebo Points!
              </div>
              <Link
                onClick={analyticsService.emptyRewardsCtaClicked}
                id="book"
                to="/"
              >
                <button className="button wallet__new-user--book-button">
                  BOOK A TREEBO
                </button>
              </Link>
            </div>
          ) : (
            <RewardStatement statements={statements} />
          )
        }
      </div>
    );
  }
}

Rewards.propTypes = {
  location: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  wallet: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  wallet: state.wallet,
});

const mapDispatchToProps = (dispatch) => ({
  walletActions: bindActionCreators(walletActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(Rewards);
