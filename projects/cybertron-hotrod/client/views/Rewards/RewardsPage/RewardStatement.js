import React from 'react';
import PropTypes from 'prop-types';
import RewardStatementListItem from './RewardStatementListItem';

const RewardStatement = ({ statements }) => (
  <div id="wallet-statement" className="container wallet-statement">
    <div className="row wallet-statement__header">
      <div className="col-4 subheading-4 wallet-statement__header--reward">
        Reward Details
      </div>
      <div className="col-5 subheading-4 wallet-statement__header--transaction-info">
        Transaction Information
      </div>
      <div className="col-3 subheading-4 wallet-statement__header--amount">
        Reward Amount
      </div>
    </div>
    <div className="wallet-statement__statements">
      {
        statements.map((statement) =>
          <RewardStatementListItem statement={statement} />)
      }
    </div>
  </div>
);

RewardStatement.propTypes = {
  statements: PropTypes.array.isRequired,
};

export default RewardStatement;
