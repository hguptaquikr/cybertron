import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Card from 'leaf-ui/cjs/Card/web';
import Text from 'leaf-ui/cjs/Text/web';
import View from 'leaf-ui/cjs/View/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import Size from 'leaf-ui/cjs/Size/web';
import Space from 'leaf-ui/cjs/Space/web';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as confirmationActionCreators from '../../services/booking/bookingDuck';
import '../../components/Loader/loader.css';

const paymentPending = 'This is taking longer than expected. We will share the updated status as soon as we have it. We will now redirect you to the homepage.';
const paymentFailure = 'Sorry! Your payment failed. You will now be redirected back to the itinerary page to restart the payment process.';
const paymentDefault = 'Please wait. We’re processing your payment.';

class PaymentProcess extends Component {
  static componentWillServerRender = ({ store: { dispatch }, match }) =>
    dispatch(confirmationActionCreators.getPaymentConfirmationDetails(match.params.bookingId));

  componentDidMount() {
    const {
      match,
      confirmationActions,
      confirmation: {
        paymentDetails,
      },
    } = this.props;
    this.interval = setInterval(() => {
      if (paymentDetails.status !== 'success' || paymentDetails.status !== 'failure') {
        confirmationActions.getPaymentConfirmationDetails(match.params.bookingId);
      }
    }, 5000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  getPaymentProcessLabel =() => {
    const {
      confirmation: {
        attempts,
        maxAttempts,
        paymentDetails,
      },
    } = this.props;
    switch (true) {
      case attempts >= maxAttempts && paymentDetails.status === 'pending':
        return paymentPending;
      case paymentDetails.status === 'failure':
        return paymentFailure;
      default:
        return paymentDefault;
    }
  }

  shouldStopFetchingPaymentStatus = () => {
    const {
      match,
      history,
      confirmation: {
        attempts,
        maxAttempts,
        paymentDetails: {
          status: paymentStatus,
          checkin,
          checkout,
          hotel_id: hotelId,
          rateplan,
          roomconfig,
          roomtype,
        },
      },
    } = this.props;
    setTimeout(() => {
      if (paymentStatus === 'pending' && attempts >= maxAttempts) {
        history.push('/');
      }
      if (paymentStatus === 'success') {
        history.push(`/confirmation/${match.params.bookingId}/`);
      } else if (paymentStatus === 'failure') {
        history.push(`/itinerary/?checkin=${checkin}&checkout=${checkout}&hotel_id=${hotelId}&rateplan=${rateplan}&roomconfig=${roomconfig}&roomtype=${roomtype}`);
      }
    }, 5000);
  }

  render() {
    const {
      confirmation: { isError },
    } = this.props;
    const label = this.getPaymentProcessLabel();
    this.shouldStopFetchingPaymentStatus();
    if (isError) clearInterval(this.interval);
    return (
      <View className="container">
        <Card backgroundColor="white">
          <Size height="500px">
            <Flex
              alignItems="center"
              justifyContent="center"
            >
              <View>
                <Text component="span" className="loader__circle" />
                <Space padding={[3, 0, 2]}>
                  <Size width="70%">
                    <Text
                      component="p"
                      weight="bold"
                      size="l"
                      color="greyDark"
                      align="center"
                    >
                      {label}
                    </Text>
                  </Size>
                </Space>
                <Size width="55%">
                  <Text
                    component="p"
                    color="greyDark"
                    align="center"
                    size="m"
                  >
                    Please do not press back, refresh
                    or close this tab while the payment is being processed.
                  </Text>
                </Size>
              </View>
            </Flex>
          </Size>
        </Card>
      </View>
    );
  }
}

PaymentProcess.propTypes = {
  confirmation: PropTypes.object.isRequired,
  match: PropTypes.object,
  history: PropTypes.object,
  confirmationActions: PropTypes.object,
};

const mapStateToProps = (state) => ({
  confirmation: state.confirmation,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  confirmationActions: bindActionCreators(confirmationActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PaymentProcess);
