import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import * as contentActionCreators from '../../services/content/contentDuck';
import Button from '../../components/Button/Button';
import Modal from '../../components/Modal/Modal';
import LoadMore from '../../components/LoadMore/LoadMore';
import AuthenticateUser from '../../views/Authentication/AuthenticateUser/AuthenticateUser';
import Search from './Search';
import Question from './Question';
import Answer from './Answer';
import './questionsAndAnswers.css';

class QuestionsAndAnswers extends Component {
  state = {
    searchText: '',
    isLoginModalOpen: false,
    isQuestionSubmitted: false,
  }

  onClose = () => {
    this.setState({
      isLoginModalOpen: false,
    });
  }

  onChange = (e) => {
    e.stopPropagation();
    this.setState({
      searchText: e.target.value,
      isQuestionSubmitted: false,
    });
  }

  onLoginModalClose = () => {
    this.setState({
      isLoginModalOpen: false,
    });
  }

  onLoginSuccess = () => {
    const { searchText: question } = this.state;
    this.submitQuestion(question);
    this.setState({
      isLoginModalOpen: false,
    });
  }

  onSubmitQuestion = () => {
    const { searchText: question } = this.state;
    const { user } = this.props;
    if (user.isAuthenticated) {
      this.submitQuestion(question);
    } else {
      this.setState({
        isLoginModalOpen: true,
      });
    }
  }

  getFilteredQnAs = () => {
    const { searchText } = this.state;
    const { QnAs } = this.props;
    return QnAs.content.filter(({ question }) => (
      question.text.toLowerCase().includes(searchText)
    )).sort((a, b) => moment(b.question.postedOn, 'DD-MM-YYYY').toDate()
      - moment(a.question.postedOn, 'DD-MM-YYYY').toDate());
  }

  storeQnAsRef = (ref) => {
    if (ref) {
      this.qnasRef = ref;
    }
  }

  submitQuestion = (question) => {
    const { contentActions, location, QnAs } = this.props;
    const { pathname } = location;
    const self = this;
    contentActions.submitQuestion(question, pathname).then(() => {
      self.setState({
        isQuestionSubmitted: true,
        filteredQnAs: QnAs.content,
      });
    });
  }

  render() {
    const { searchText, isLoginModalOpen, isQuestionSubmitted } = this.state;
    const { QnAs } = this.props;
    const filteredQnAs = this.getFilteredQnAs();
    return (
      <section className="qnas" id="t-qnas">
        <div className="qnas__desc-title">
          <h2>{QnAs.title}</h2>
        </div>
        <div className="" ref={this.storeQnAsRef}>
          <Search searchText={searchText} onChange={this.onChange} />
          {
            (filteredQnAs.length || isQuestionSubmitted) ? null : (
              <div>
                {
                  QnAs.content.length ? (
                    <p className="qnas__no-answers">
                      We were unable to find any existing answers to your question.
                      Please submit your question to our community.
                    </p>
                  ) : (
                    <p className="qnas__no-answers">
                      Be the first one to ask us a question.
                    </p>
                  )
                }
                <Button
                  id="t-qnaSubmmit"
                  onClick={this.onSubmitQuestion}
                  disabled={searchText.length < 3}
                >
                  Submit Question
                </Button>
              </div>
            )
          }
          {
            isQuestionSubmitted ? (
              <p className="qnas__question__submit">
                Your question has been successfully submitted.
                It will be posted after verification.
              </p>
            ) : null
          }
          {
            isLoginModalOpen ? (
              <Modal
                isOpen
                onClose={this.onClose}
                closeIcon="close"
              >
                <AuthenticateUser
                  isModal
                  onLoginSuccess={this.onLoginSuccess}
                />
              </Modal>
            ) : null
          }
          <LoadMore
            childrenOnly={{ initial: 5, step: 2 }}
            loadMoreText="Load more questions"
            actionClassName="qnas__load-more-action"
          >
            {
              filteredQnAs.map((QnA) => (
                <div className="qnas__qna-container" key={QnA.question.text}>
                  <Question
                    question={QnA.question.text}
                    questionBy={QnA.question.author}
                    questionDate={QnA.question.postedOn}
                  />
                  <Answer answers={QnA.answers} />
                </div>
              ))
            }
          </LoadMore>
        </div>
      </section>
    );
  }
}

QuestionsAndAnswers.propTypes = {
  QnAs: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(QuestionsAndAnswers);
