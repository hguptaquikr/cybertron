import React from 'react';
import PropTypes from 'prop-types';

function Question({ question, questionBy, questionDate }) {
  return (
    <div className="qnas__question" key={question}>
      <img
        src="https://images.treebohotels.com/images/question_answer/question-icon.svg"
        className="qnas__icon"
        alt="Question"
      />
      <div>
        <p className="qnas__question__text" dangerouslySetInnerHTML={{ __html: question }} />
        <p className="qnas__question__by">Asked by {questionBy}, {questionDate}</p>
      </div>
    </div>
  );
}

Question.propTypes = {
  question: PropTypes.string.isRequired,
  questionBy: PropTypes.string.isRequired,
  questionDate: PropTypes.string.isRequired,
};

export default Question;
