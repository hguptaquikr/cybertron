import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

class QnASearch extends Component {
  state = {
    isFocused: false,
  }

  onFocus = () => this.setState({
    isFocused: true,
  })

  onBlur = () => this.setState({
    isFocused: false,
  })

  render() {
    const { isFocused } = this.state;
    const { searchText, onChange } = this.props;
    return (
      <div
        className={cx('qnas__search', {
          'qnas__search--focused': isFocused,
        })}
      >
        <input
          id="t-qnaSearch"
          className="qnas__search__input"
          type="text"
          value={searchText}
          placeholder="Have a question? Search/Ask here..."
          onChange={onChange}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        />
        <i className="icon-search" />
      </div>
    );
  }
}

QnASearch.propTypes = {
  searchText: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default QnASearch;
