import React from 'react';
import PropTypes from 'prop-types';

function Answer({ answers }) {
  return (
    <div>
      {
        answers.map((answer) => (
          <div className="qnas__answer" key={answer.text}>
            <img
              src="https://images.treebohotels.com/images/question_answer/answer-icon.svg"
              className="qnas__icon"
              alt="Answer"
            />
            <div>
              <p className="qnas__answer__text" dangerouslySetInnerHTML={{ __html: answer.text }} />
              <p className="qnas__answer__by">
                {
                  answer.isVerified ? (
                    <img
                      src="https://images.treebohotels.com/images/question_answer/verified-icon.svg"
                      className="qnas__verified-icon"
                      alt="Verified"
                    />
                  ) : null
                }
                Response by {answer.author}, {answer.postedOn}
              </p>
            </div>
          </div>
        ))
      }
    </div>
  );
}

Answer.propTypes = {
  answers: PropTypes.array.isRequired,
};

export default Answer;
