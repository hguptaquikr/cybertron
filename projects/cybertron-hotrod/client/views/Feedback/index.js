import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

const Feedback = Loadable({
  loader: () => {
    importCss('Feedback');
    return import('./Feedback' /* webpackChunkName: 'Feedback' */);
  },
  loading: () => null,
});

export default Feedback;
