import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import qs from 'query-string';
import Flex from 'leaf-ui/cjs/Flex/web';
import Text from 'leaf-ui/cjs/Text/web';
import TextInput from 'leaf-ui/cjs/TextInput/web';
import Space from 'leaf-ui/cjs/Space/web';
import Button from 'leaf-ui/cjs/Button/web';
import Card from 'leaf-ui/cjs/Card/web';
import Size from 'leaf-ui/cjs/Size/web';
import View from 'leaf-ui/cjs/View/web';
import * as contentActionCreators from '../../services/content/contentDuck';
import Building from './Building/Building';
import ArrowDown from './ArrowDown/ArrowDown';
import FeedbackFooter from './FeedbackFooter/FeedbackFooter';
import analyticsService from '../../services/analytics/analyticsService';

class Feedback extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    promises.push(dispatch(contentActionCreators.getFeedbackTags(match.params.feedbackId)));
    return Promise.all(promises);
  }

  state = {
    showInput: false,
    feedbackTag: true,
  };

  componentDidMount() {
    const {
      contentActions,
      match,
      location,
      route,
    } = this.props;
    const query = qs.parse(location.search);
    contentActions.getSeoContent(route, match.params);
    contentActions.getFeedbackTags(match.params.feedbackId)
      .then((feedbackData) => {
        contentActions.submitFeedback(this.makeFeedbackData(feedbackData, query));
      });
  }

  makeFeedbackData = (feedback, query) => (
    {
      ...feedback,
      source: query.source,
      feedback_flow: {
        ...feedback.feedback_flow,
        value: query.expression,
      },
    }
  )

  updateTag = (tag) => () => {
    const { feedbackTag } = this.state;
    const {
      contentActions,
      content: { feedback },
    } = this.props;
    this.setState({ feedbackTag: !feedbackTag });
    if (tag.type === 'text') {
      this.setState({ showInput: feedbackTag });
    } else {
      contentActions.updateFeedbackTags(tag.name, feedbackTag);
    }

    const analyticsProperty = {
      device: 'Desktop',
      source: feedback.source,
      booking: feedback.booking,
      user: feedback.user,
      tagSelected: tag.name,
      rating: feedback.feedback_flow.value,
    };
    analyticsService.feedbackDetail('Click on Tag', analyticsProperty);
  }

  updateValue = () => (e) => {
    const {
      contentActions,
      content: { feedback },
    } = this.props;
    contentActions.updateFeedbackTags('Others', e.target.value);
    const analyticsProperty = {
      device: 'Desktop',
      source: feedback.source,
      booking: feedback.booking,
      user: feedback.user,
      OtherComment: e.target.value,
      rating: feedback.feedback_flow.value,
    };
    analyticsService.feedbackDetail('Click on Others', analyticsProperty);
  }

  submitFeedback = () => {
    const {
      history,
      content: { feedback },
      contentActions,
      location,
      match,
    } = this.props;
    const query = qs.parse(location.search);
    contentActions.submitFeedback(this.makeFeedbackData(feedback, query))
      .then(() => {
        if (query.expression === 'terrible' || query.expression === 'veryBad') {
          history.push(`/feedback/${feedback.id}/thanks/`);
        } else if (query.expression === 'avg' || query.expression === 'good') {
          history.push({
            pathname: `/tripadvisor/reviews-${feedback.booking.hotel_id}/`,
            search: qs.stringify({
              flow: query.expression,
              feedbackId: match.params.feedbackId,
            }),
          });
        }
      });

    const analyticsProperty = {
      device: 'Desktop',
      source: feedback.source,
      booking: feedback.booking,
      user: feedback.user,
      value: feedback.current.sections.tag_section.values,
      rating: feedback.feedback_flow.value,
    };
    analyticsService.feedbackDetail('Click on Submit', analyticsProperty);
  }

  render() {
    const { showInput } = this.state;
    const {
      location,
      content: { feedback },
    } = this.props;

    const query = qs.parse(location.search);
    const enableSubmitBtn = feedback.current.sections.tag_section.values.some((tags) =>
      (tags.body.value.length || tags.body.value === true));

    return (
      <div className="container">
        {
          feedback.state === 'EXPIRED' || feedback.state === 'COMPLETED' ?
          (
            <Space margin={[2, 'auto']}>
              <Card backgroundColor="white">
                <Space padding={[10, 0, 8]}>
                  <Size
                    width="auto"
                    height="50%"
                  >
                    <Flex
                      flexDirection="row"
                      justifyContent="center"
                    >
                      <View>
                        <img src="http://images.treebohotels.com/images/Email/oops.svg" alt="Thank You" />
                      </View>
                    </Flex>
                  </Size>
                </Space>
                <Space margin={[0, 'auto', 10]}>
                  <Size width="50%">
                    <Flex justifyContent="center">
                      <View>
                        <Flex
                          justifyContent="center"
                          alignItems="center"
                        >
                          <View>
                            <Text size="m">This link is no longer available, but we would love to hear from you.</Text>
                            <Text size="m">Write to us at feedback@treebohotels.com</Text>
                          </View>
                        </Flex>
                        <Space margin={[5, 'auto']}>
                          <Size width="44%">
                            <a href="mailto:guestfeedback@treebohotels.com?Subject=Feedback" target="_top">
                              <Button >WRITE YOUR FEEDBACK</Button>
                            </a>
                          </Size>
                        </Space>
                      </View>
                    </Flex>
                  </Size>
                </Space>
                <Building />
              </Card>
            </Space>
          ) : (
            <Space margin={[2, 'auto']}>
              <Card backgroundColor="greyLight">
                <Space
                  margin={[0, 'auto']}
                  padding={[5, 0]}
                >
                  <Size width="40%">
                    <Flex
                      flexDirection="row"
                      justifyContent="space-around"
                    >
                      <View>
                        <Flex>
                          <View>
                            <img src={`http://images.treebohotels.com/images/Email/${query.expression}.png`} alt="Awesome Experience" />
                            <Space margin={[1, 'auto']}>
                              <Link to={`/feedback/${feedback.id}/change/?source=${query.source}`}>
                                <Text size="s" color="blue">Change</Text>
                              </Link>
                            </Space>
                          </View>
                        </Flex>
                        <Space margin={[0, 0, 0, 4]}>
                          <Flex justifyContent="center">
                            <View>
                              <Text
                                size="m"
                                color="greyDarker"
                              >
                              Hi {feedback.user.name},
                              </Text>
                              {
                                query.expression === 'terrible' || query.expression === 'veryBad' ? (
                                  <Text
                                    size="xl"
                                    color="greyDarker"
                                  >
                                    We are very sorry to hear about your poor experience with us.
                                  </Text>
                                ) : (
                                  <Text
                                    size="xl"
                                    color="greyDarker"
                                  >
                                    We are sorry we couldn’t deliver our best experience.
                                  </Text>
                                )
                              }
                            </View>
                          </Flex>
                        </Space>
                      </View>
                    </Flex>
                  </Size>
                </Space>
                <Card backgroundColor="white" style={{ position: 'relative' }}>
                  <ArrowDown />
                  <Space padding={[7, 0, 2]}>
                    <Flex
                      flexDirection="row"
                      justifyContent="space-around"
                    >
                      <View>
                        {
                          query.expression === 'terrible' || query.expression === 'veryBad' ? (
                            <Text
                              size="xxxl"
                              weight="bold"
                              color="greyDarker"
                            >
                              Please tell us what went wrong?
                            </Text>
                          ) : (
                            <Text
                              size="xxxl"
                              weight="bold"
                              color="greyDarker"
                            >
                              Please tell us areas of improvement?
                            </Text>
                          )
                        }
                      </View>
                    </Flex>
                  </Space>
                  <Space margin={[3, 'auto']}>
                    <Size width="44%">
                      <Flex
                        flexDirection="row"
                        justifyContent="space-around"
                        flexWrap="wrap"
                      >
                        <View>
                          {
                          Object.values(feedback.current.sections.tag_section.values).map((tag) => (
                            <Space
                              key={tag.name}
                              margin={[0, 0, 2, 0]}
                            >
                              <Button
                                color={tag.body.value === true || showInput ? 'lagoon' : 'greyDark'}
                                kind={tag.body.value === true || (tag.type === 'text' && showInput) ? 'filled' : 'outlined'}
                                onClick={this.updateTag(tag)}
                              >
                                {tag.name}
                              </Button>
                            </Space>
                          ))
                        }
                        </View>
                      </Flex>
                    </Size>
                  </Space>
                  {
                    showInput ? (
                      <Flex
                        flexDirection="row"
                        justifyContent="space-around"
                      >
                        <View>
                          <Space margin={[3, 'auto']}>
                            <Size width="40%">
                              <TextInput
                                name="Others"
                                type="text"
                                block
                                label="Type here about the issues you have faced"
                                onChange={this.updateValue()}
                              />
                            </Size>
                          </Space>
                        </View>
                      </Flex>
                    ) : null
                  }
                  <Flex
                    flexDirection="row"
                    justifyContent="space-around"
                  >
                    <View>
                      <Space margin={[3, 'auto']}>
                        <Size width="20%">
                          <Button
                            block
                            disabled={!enableSubmitBtn}
                            onClick={this.submitFeedback}
                          >
                          SUBMIT
                          </Button>
                        </Size>
                      </Space>
                    </View>
                  </Flex>
                  <Building />
                </Card>
              </Card>
            </Space>
          )
        }
        <FeedbackFooter />
      </div>
    );
  }
}

Feedback.propTypes = {
  history: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.object,
  route: PropTypes.object,
  content: PropTypes.object,
  contentActions: PropTypes.object,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Feedback);
