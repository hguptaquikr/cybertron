import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

const FeedbackThanks = Loadable({
  loader: () => {
    importCss('FeedbackThanks');
    return import('./FeedbackThanks' /* webpackChunkName: 'FeedbackThanks' */);
  },
  loading: () => null,
});

export default FeedbackThanks;
