import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Flex from 'leaf-ui/cjs/Flex/web';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import Card from 'leaf-ui/cjs/Card/web';
import View from 'leaf-ui/cjs/View/web';
import Size from 'leaf-ui/cjs/Size/web';
import * as contentActionCreators from '../../../services/content/contentDuck';
import Building from '../../Feedback/Building/Building';
import FeedbackFooter from '../FeedbackFooter/FeedbackFooter';

class FeedbackThanks extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];
    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));
    return Promise.all(promises);
  }

  componentDidMount() {
    const {
      contentActions,
      route,
      match,
    } = this.props;
    contentActions.getFeedbackTags(match.params.feedbackId)
      .then((feedbackData) => {
        const completeFeedback = {
          ...feedbackData,
          state: 'COMPLETED',
        };
        contentActions.submitFeedback(completeFeedback);
      });
    contentActions.getSeoContent(route, match.params);
  }

  render() {
    const {
      content: { feedback },
    } = this.props;
    return (
      <View className="container">
        <Space margin={[2, 'auto']}>
          <Card backgroundColor="white">
            <Space padding={[16, 0, 4]}>
              <Size height="50%">
                <Flex
                  flexDirection="row"
                  justifyContent="center"
                >
                  <View>
                    <img src="http://images.treebohotels.com/images/Email/thanks.png" alt="Thank You" />
                  </View>
                </Flex>
              </Size>
            </Space>
            <Space margin={[0, 'auto', 16]}>
              <Size width="60%">
                <Flex justifyContent="center">
                  <View>
                    <Space margin={[0, 0, 4]}>
                      <Flex
                        flexDirection="row"
                        justifyContent="center"
                      >
                        <View>
                          <Text
                            size="xl"
                            color="greyDarker"
                          >
                            Thank you {feedback.user.name}!
                          </Text>
                        </View>
                      </Flex>
                    </Space>
                    <Flex
                      flexDirection="row"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <View>
                        <Text
                          size="m"
                          color="greyDark"
                          style={{ textAlign: 'center', lineHeight: 1.5 }}
                        >
                          We are very sorry to hear that you had a poor experience with us.
                          This is definitely not the experience we strive to deliver.
                          Thank you for taking your time out to share your detailed feedback,
                          it goes a long way in helping us get better.
                        </Text>
                      </View>
                    </Flex>
                    <Space margin={[2, 0]}>
                      <Flex
                        flexDirection="row"
                        justifyContent="center"
                        alignItems="center"
                      >
                        <View>
                          <Text
                            size="m"
                            color="greyDark"
                            weight="bold"
                          >
                            - Sidharth Gupta,&nbsp;
                          </Text>
                          <Text size="xs" color="greyDark"> Co-founder @ Treebo Hotels</Text>
                        </View>
                      </Flex>
                    </Space>
                  </View>
                </Flex>
              </Size>
            </Space>
            <Building />
          </Card>
        </Space>
        <FeedbackFooter />
      </View>
    );
  }
}

FeedbackThanks.propTypes = {
  content: PropTypes.object,
  contentActions: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FeedbackThanks);
