import React, { Component } from 'react';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import qs from 'query-string';
import Flex from 'leaf-ui/cjs/Flex/web';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import Card from 'leaf-ui/cjs/Card/web';
import View from 'leaf-ui/cjs/View/web';
import Size from 'leaf-ui/cjs/Size/web';
import * as contentActionCreators from '../../../services/content/contentDuck';
import Building from '../../Feedback/Building/Building';
import ArrowDown from '../../Feedback/ArrowDown/ArrowDown';
import FeedbackFooter from '../FeedbackFooter/FeedbackFooter';
import analyticsService from '../../../services/analytics/analyticsService';

class FeedbackChange extends Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match }) => {
    const promises = [];

    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));

    promises.push(dispatch(contentActionCreators.getFeedbackTags(match.params.feedbackId)));

    return Promise.all(promises);
  }

  componentDidMount() {
    const {
      contentActions,
      match,
      route,
    } = this.props;
    contentActions.getSeoContent(route, match.params);
    contentActions.getFeedbackTags(match.params.feedbackId);
  }

  submitFeedback = () => {
    const {
      content: { feedback },
      contentActions,
      location,
    } = this.props;

    const query = qs.parse(location.search);
    const updatedFeedback = {
      ...feedback,
      source: query.source,
    };
    contentActions.submitFeedback(updatedFeedback);
    const analyticsProperty = {
      device: 'Desktop',
      source: feedback.source,
      booking: feedback.booking,
      user: feedback.user,
      rating: feedback.feedback_flow.value,
    };
    analyticsService.feedbackDetail('Click on rating', analyticsProperty);
  }

  render() {
    const {
      content: { feedback },
      match,
      location,
    } = this.props;

    const query = qs.parse(location.search);

    return (
      <View className="container">
        <Space margin={[2, 'auto']}>
          <Card backgroundColor="greyLight">
            <Space
              margin={[0, 'auto']}
              padding={[8, 0]}
            >
              <Size width="36%">
                <Flex justifyContent="center">
                  <View>
                    <Flex
                      flexDirection="column"
                      justifyContent="center"
                    >
                      <View>
                        <Text
                          size="s"
                          color="greyDarker"
                        >
                        Hi {feedback.user.name},
                        </Text>
                      </View>
                    </Flex>
                    <Flex justifyContent="center">
                      <View>
                        <Text
                          size="xxl"
                          color="greyDarker"
                          style={{ textAlign: 'center' }}
                        >
                        How was your recent stay with us, at {feedback.booking.hotel_name}?
                        </Text>
                      </View>
                    </Flex>
                  </View>
                </Flex>
              </Size>
            </Space>
            <Card
              backgroundColor="white"
              style={{ position: 'relative' }}
            >
              <ArrowDown />
              <Space
                padding={[7, 0, 4]}
                margin={[0, 'auto']}
              >
                <Size width="50%">
                  <Flex
                    flexDirection="row"
                    justifyContent="space-around"
                  >
                    <View>
                      {
                      ['terrible', 'veryBad', 'avg', 'good', 'amazing'].map((expression) => (
                        <View key={expression}>
                          {
                            expression === 'amazing' ? (
                              <Link
                                to={{
                                  pathname: `/tripadvisor/reviews-${feedback.booking.hotel_id}/`,
                                  search: qs.stringify({
                                    flow: 'amazing',
                                    source: query.source,
                                    feedbackId: feedback.id,
                                    taWidgetUrl: decodeURIComponent(feedback.ta_url),
                                  }),
                                }}
                                onClick={this.submitFeedback}
                              >
                                <img
                                  src={`http://images.treebohotels.com/images/Email/${expression}.png`}
                                  alt="Awesome Experience"
                                />
                              </Link>
                            ) : (
                              <Link
                                to={`/feedback/${match.params.feedbackId}/?source=${query.source}&expression=${expression}`}
                                onClick={this.submitFeedback}
                              >
                                <img
                                  src={`http://images.treebohotels.com/images/Email/${expression}.png`}
                                  alt="Awesome Experience"
                                />
                              </Link>
                            )
                          }
                        </View>
                      ))
                    }
                    </View>
                  </Flex>
                </Size>
              </Space>
              <img
                src="http://images.treebohotels.com/images/Email/hr.png"
                alt="hr"
              />
              <Space margin={[5, 0]} >
                <Flex
                  flexDirection="row"
                  justifyContent="center"
                  alignItems="center"
                >
                  <View>
                    <Text>YOUR STAY DETAILS</Text>
                  </View>
                </Flex>
              </Space>
              <Space margin={[2, 'auto', 0]}>
                <Size width="40%">
                  <Flex justifyContent="center">
                    <View>
                      <Text
                        weight="bold"
                        size="m"
                        color="greyDarker"
                      >
                        {feedback.booking.hotel_name}
                      </Text>
                      <Text
                        size="m"
                        color="greyDark"
                      >
                        {feedback.booking.hotel_address}
                      </Text>
                      <hr />
                    </View>
                  </Flex>
                </Size>
              </Space>
              <Space margin={[2, 'auto', 0]}>
                <Size width="40%">
                  <Flex
                    flexDirection="row"
                    justifyContent="space-between"
                  >
                    <View>
                      <Text
                        size="m"
                        color="greyDark"
                      >
                        {feedback.booking.from_date} - {feedback.booking.to_date}
                      </Text>
                      <Text
                        size="m"
                        color="greyDark"
                      >
                        {
                          feedback.booking.no_of_room > 1 ? (
                            `${feedback.booking.no_of_room} Rooms`
                          ) : (
                            `${feedback.booking.no_of_room} Room`
                          )
                        }
                      </Text>
                    </View>
                  </Flex>
                </Size>
              </Space>
              <Building />
            </Card>
          </Card>
        </Space>
        <FeedbackFooter />
      </View>
    );
  }
}

FeedbackChange.propTypes = {
  content: PropTypes.object,
  match: PropTypes.object,
  location: PropTypes.object,
  route: PropTypes.object,
  contentActions: PropTypes.object,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(FeedbackChange);
