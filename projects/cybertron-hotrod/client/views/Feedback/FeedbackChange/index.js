import Loadable from 'react-loadable';
import importCss from '../../../services/importCss';

const FeedbackChange = Loadable({
  loader: () => {
    importCss('FeedbackChange');
    return import('./FeedbackChange' /* webpackChunkName: 'FeedbackChange' */);
  },
  loading: () => null,
});

export default FeedbackChange;
