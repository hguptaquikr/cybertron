import styled from 'styled-components';

const ArrowDown = styled.div`
  right: 50%;
  position: absolute;
  border-left: 12px solid transparent;
  border-right: 12px solid transparent;
  border-top: 12px solid;
  border-top-color: ${(p) => p.theme.color.greyLight};
`;

export default ArrowDown;
