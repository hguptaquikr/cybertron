import React from 'react';
import Flex from 'leaf-ui/cjs/Flex/web';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import View from 'leaf-ui/cjs/View/web';

const FeedbackFooter = () => (
  <View className="container">
    <Space margin={[0, 'auto', 2]}>
      <Flex
        flexDirection="row"
        justifyContent="space-between"
      >
        <View>
          <img src="http://images.treebohotels.com/images/treebo-logo-black.svg" alt="Thank You" style={{ width: ' 10rem', height: '4rem' }} />
          <Flex
            flexDirection="row"
            alignItems="center"
          >
            <View>
              <Text size="s">Contact us: +91 9322-800-100   |    E-Mail:
                <span style={{ color: '#0eb550' }}> feedback@treebohotels.com</span>
              </Text>
            </View>
          </Flex>
        </View>
      </Flex>
    </Space>
  </View>
);

export default FeedbackFooter;
