import styled from 'styled-components';

const Building = styled.div`
  background-image: url('http://images.treebohotels.com/images/Email/building.png');
  height: 8rem;
  background-repeat: repeat no-repeat;
  margin-top: 4rem;
`;

export default Building;
