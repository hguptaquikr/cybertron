import React from 'react';
import PropTypes from 'prop-types';
import Pay from '../Pay/Pay';
import config from '../../../config';

class Payment extends React.Component {
  state = {
    error: '',
  };

  onErrorPay = (error) => {
    this.setState({ error });
  };

  render() {
    const {
      payment: {
        amount_to_send: amountToSend,
        order_id: orderId,
        gatewayOrderId,
        email,
        contact,
        offers,
        bookingApiError,
        payNowEnabled,
        isPrepaidOnly,
        isPayAtHotelEnabled,
      },
      itineraryActions,
      confirmBooking,
      selectedRatePlan,
      hidePaymentOption,
      refreshBookingId,
      onPay,
      payNow,
      ratePlans,
      payAtHotel,
    } = this.props;
    const { error } = this.state;
    const paymentGatewayConfig = {
      key: config.razorpayKey,
      image: '//s3-ap-southeast-1.amazonaws.com/treebo/static/files/new_treebo_gateway_logo.png',
    };

    return (
      <div>
        <h2 className="checkout__section-title checkout__section-title--active">
          Payment options
        </h2>
        { error && <p className="error checkout__pay-error" id="t-payError">{error}</p> }
        <div className="checkout__payment">
          <Pay
            orderId={orderId}
            gatewayOrderId={gatewayOrderId}
            amountToSend={amountToSend}
            user={{ email, contact }}
            paymentGatewayConfig={paymentGatewayConfig}
            onPay={onPay}
            payNow={payNow}
            onSuccessPay={confirmBooking}
            onErrorPay={this.onErrorPay}
            offers={offers}
            bookingApiError={bookingApiError}
            payNowEnabled={payNowEnabled}
            hidePaymentOption={hidePaymentOption}
            isPayAtHotelEnabled={isPayAtHotelEnabled}
            payAtHotel={payAtHotel}
            selectedRatePlan={selectedRatePlan}
            itineraryActions={itineraryActions}
            isPrepaidOnly={isPrepaidOnly}
            refreshBookingId={refreshBookingId}
            ratePlans={ratePlans}
          />
        </div>
      </div>
    );
  }
}

Payment.propTypes = {
  payment: PropTypes.object.isRequired,
  confirmBooking: PropTypes.func.isRequired,
  refreshBookingId: PropTypes.func.isRequired,
  itineraryActions: PropTypes.object.isRequired,
  hidePaymentOption: PropTypes.func.isRequired,
  payAtHotel: PropTypes.func,
  onPay: PropTypes.func,
  payNow: PropTypes.func,
  ratePlans: PropTypes.object,
  selectedRatePlan: PropTypes.string,
};

export default Payment;
