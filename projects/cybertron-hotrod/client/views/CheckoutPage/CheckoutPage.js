import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import qs from 'query-string';
import { itineraryActionCreators } from '../../services/checkout/checkoutDuck';
import * as otpActionCreators from '../../services/otp/otpDuck';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as userActionCreators from '../../services/user/userDuck';
import * as walletActionCreators from '../../services/wallet/walletDuck';
import Checkout from './Checkout';
import Cart from '../Cart/Cart';
import analyticsService from '../../services/analytics/analyticsService';
import searchService from '../../services/search/searchService';
import * as abService from '../../services/ab/abService';
import { constants } from '../../services/utils';
import './checkoutPage.css';

class ItineraryPage extends React.Component {
  static componentWillServerRender = ({ store: { dispatch }, match, route }) =>
    dispatch(contentActionCreators.getSeoContent(route, match.params))

  componentDidMount() {
    const {
      searchActions,
      itineraryActions,
      contentActions,
      location,
      match,
      route,
    } = this.props;

    const query = qs.parse(location.search);
    searchActions.setSearchState({ ...query, ...match.params });
    contentActions.getSeoContent(route, match.params);
    this.getCartContent(query.rateplan);
    itineraryActions.getOfferContent();
  }

  componentWillReceiveProps(nextProps) {
    const {
      user,
      location,
    } = this.props;
    const query = qs.parse(location.search);
    if ((!user.isAuthenticated && nextProps.user.isAuthenticated) ||
      (user.isAuthenticated && !nextProps.user.isAuthenticated)
    ) {
      this.getCartContent(query.rateplan);
    }
  }

  getCartContent = (ratePlan, isContinue = false) => {
    const { itineraryActions, location } = this.props;
    itineraryActions.getCartContent(qs.parse(location.search).bid, ratePlan)
      .then((response) => {
        const selectedRatePlanTag = response.price.ratePlans[ratePlan].tag;
        const newSummary = {
          summary: {
            title: 'VIEW PLAN',
            subtitle: `You have selected a ${selectedRatePlanTag} plan.${selectedRatePlanTag === 'Non-Refundable' ? 'Online Payment Only.' : ''}`,
          },
        };
        if (Object.keys(response.price.ratePlans).length === 1) {
          itineraryActions.skipRatePlanStep(newSummary);
        }
        if (isContinue) {
          itineraryActions.continueRatePLan({ ratePlan, newSummary });
        }
        itineraryActions.getCoupons(response.bid);
        this.captureAnalytics();
        return response;
      })
      .then((res) => this.replaceItineraryUrl(res, ratePlan));
  }

  replaceItineraryUrl = (response, ratePlan) => {
    const { history, location } = this.props;
    if (history.location.pathname === '/itinerary/') {
      history.replace({
        pathname: location.pathname,
        search: qs.stringify({
          ...qs.parse(location.search),
          bid: response.bid,
          rateplan: ratePlan,
        }),
      });
    }
  }

  captureAnalytics = () => {
    const {
      search: {
        datePicker: {
          range,
        },
        occupancy: {
          rooms,
        },
      },
      itinerary,
      location,
    } = this.props;
    const { checkIn, checkOut, numberOfNights, abwDays } =
    searchService.formattedDatePicker(range, constants.dateFormat.query);
    const analyticsProperties = {
      hotelId: itinerary.cart.hotel.id || '',
      hotelName: itinerary.cart.hotel.name || '',
      pageUrl: location.pathname,
      checkin: checkIn,
      checkout: checkOut,
      lengthOfStay: numberOfNights.count,
      abwDays,
      roomConfig: rooms.map((room) => `${room.adults}-${room.kids || 0}`).join(','),
      ...abService.impressionProperties(['checkoutCtaDesktop', 'checkoutCouponDesktop', 'checkoutDetailsCtaDesktop']),
    };
    analyticsService.itineraryContentViewed(analyticsProperties);
  }

  render() {
    const {
      itinerary: { cart, ...checkout },
      otp,
      wallet,
      itineraryActions,
      otpActions,
      location,
      userActions,
      walletActions,
      search,
    } = this.props;

    const query = qs.parse(location.search);

    return (
      <div className="container itinerary-page">
        <div className="row">
          <div className="col-8">
            <Checkout
              selectedRoomType={query.roomtype}
              selectedRatePlan={query.rateplan}
              getCartContent={this.getCartContent}
              price={cart.price}
              checkout={checkout}
              itineraryActions={itineraryActions}
              otpActions={otpActions}
              otp={otp}
              authAction={userActions}
              hotelId={query.hotel_id}
              hotel={cart.hotel}
              search={search}
            />
          </div>
          <div className="col-4">
            <Cart
              cart={cart}
              payment={checkout.payment}
              wallet={wallet}
              walletActions={walletActions}
              selectedRatePlan={query.rateplan}
              itineraryActions={itineraryActions}
            />
          </div>
        </div>
      </div>
    );
  }
}

ItineraryPage.propTypes = {
  itinerary: PropTypes.object.isRequired,
  itineraryActions: PropTypes.object.isRequired,
  otpActions: PropTypes.object.isRequired,
  otp: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  wallet: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  itinerary: state.itinerary,
  search: state.search,
  otp: state.otp,
  wallet: state.wallet,
});

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
  itineraryActions: bindActionCreators(itineraryActionCreators, dispatch),
  otpActions: bindActionCreators(otpActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(ItineraryPage);
