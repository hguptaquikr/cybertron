import React from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import isEmpty from 'lodash/isEmpty';
import FormsyInput from '../../components/Formsy/FormsyInput';
import NewYearRateTag from '../../components/NewYearRateTag/NewYearRateTag';
import ABButton from '../../components/ABButton/ABButton';
import analyticsService from '../../services/analytics/analyticsService';
import * as hotelService from '../../services/hotel/hotelService';

class Details extends React.Component {
  state = {
    showGstin: this.props.details.hasGstin,
  }
  onContinue = () => {
  }

  toggleGstin = () => {
    analyticsService.gstinClicked();
    this.setState({ showGstin: !this.state.showGstin });
  }

  render() {
    const { showGstin } = this.state;
    const {
      details: {
        name,
        mobile,
        email,
        gstin,
        companyName,
        companyAddress,
      },
      continueDetails,
      detailsConfirmation,
      ratePlans = {},
      selectedRatePlan,
      hotelId,
      hotel: { policies },
    } = this.props;

    const {
      sellingPrice,
      totalPrice,
    } = ratePlans[selectedRatePlan] || {};
    const isCoupleFriendly = !isEmpty(policies) ? hotelService.isCoupleFriendly(policies) : false;
    const shouldConfirmBooking = !sellingPrice && totalPrice;
    const onContinueDetails = shouldConfirmBooking ? detailsConfirmation : continueDetails;

    return (
      <div>
        <h2 className="checkout__section-title checkout__section-title--active">
          Enter Details of Primary Traveller
        </h2>
        <p className="checkout__section-subtitle">Who is the main traveller?</p>
        <Formsy className="checkout__form" onValidSubmit={onContinueDetails}>
          <FormsyInput
            type="text"
            name="name"
            label="Name"
            value={name}
            required
          />
          <FormsyInput
            type="number"
            name="mobile"
            validations="isLength:10"
            validationError="Please enter a valid mobile number"
            label="Mobile Number"
            value={mobile}
            required
          />
          <FormsyInput
            type="text"
            name="email"
            validations="isEmail"
            validationError="Please enter a valid email"
            label="Email"
            value={email}
            required
          />
          <div className="checkout__form__gstin">
            <label htmlFor="GSTIN">
              <input
                id="gstin"
                type="checkbox"
                checked={showGstin}
                onChange={this.toggleGstin}
              />
               Corporate User? Use GSTIN for this booking (Optional)
            </label>
          </div>
          {
            showGstin ? (
              <div>
                <FormsyInput
                  type="text"
                  name="gstin"
                  validations={{
                    matchRegexp: /^[\d]{2}[A-Za-z]{5}[\d]{4}[A-Za-z]{1}[1-9A-Za-z]{1}Z[0-9A-Za-z]{1}$/,
                  }}
                  validationError="Please enter a valid identification number"
                  label="GST Identification Number"
                  value={gstin}
                  required
                />
                <FormsyInput
                  type="text"
                  name="companyName"
                  label="Company Name"
                  value={companyName}
                  required
                />
                <FormsyInput
                  type="text"
                  name="companyAddress"
                  label="Company Address"
                  value={companyAddress}
                  required
                />
              </div>
            ) : null
          }
          <ABButton
            id="t-itineraryDetailsCTA"
            type="submit"
            experiment="checkoutDetailsCtaDesktop"
            className="checkout__action"
            block
            render={(text) => shouldConfirmBooking ? 'CONTINUE TO PAYMENT' : (text || 'CONTINUE')}
          />
        </Formsy>
        <p className="checkout__msg-header">Important Notice</p>
        <NewYearRateTag hotelId={hotelId} />
        {
          isCoupleFriendly ? (
            <p className="checkout__msg">
              {hotelService.getCoupleFriendlyPolicyText(policies)}
            </p>
          ) : (
            <p className="checkout__msg">
            Our hotels reserve the right of admission to
            ensure safety and comfort of guests. This may
            include cases such as local residents, unmarried
            and unrelated couples among others.
              <a
                className="text-primary"
                href="/faq/#right-to-admission"
                target="_blank"
                rel="noopener noreferrer"
              >&nbsp;Know more
              </a>
            </p>
          )
        }
      </div>
    );
  }
}

Details.propTypes = {
  continueDetails: PropTypes.func.isRequired,
  detailsConfirmation: PropTypes.func.isRequired,
  details: PropTypes.object.isRequired,
  ratePlans: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.string.isRequired,
  hotelId: PropTypes.number.isRequired,
  hotel: PropTypes.object.isRequired,
};

export default Details;
