import React from 'react';
import PropTypes from 'prop-types';
import Formsy from 'formsy-react';
import FormsyInput from '../../components/Formsy/FormsyInput';

class Login extends React.Component {
  state = {
    existingUser: false,
  };

  handleFieldChange = (e) => {
    this.setState({ existingUser: e.currentTarget.checked });
  };

  render() {
    const { continueLogin, login: { email }, errorMsg } = this.props;
    const { existingUser } = this.state;
    return (
      <div>
        <h2 className="checkout__section-title checkout__section-title--active">
          Your Email Address
        </h2>
        <p className="checkout__section-subtitle">
          Where do you want the booking confirmation.
        </p>
        <Formsy className="checkout__form" onValidSubmit={continueLogin}>
          <FormsyInput
            type="text"
            name="email"
            validations="isEmail"
            validationError="Please enter a valid email"
            label="Email"
            value={email}
            required
          />
          {
            existingUser &&
              <FormsyInput
                type="password"
                name="password"
                label="Password"
                required
              />
          }
          {
            errorMsg && (
              <p className="text-error"><strong>{errorMsg}</strong></p>
            )
          }
          <label htmlFor="existing-user">
            <input
              id="existing-user"
              type="checkbox"
              value={existingUser}
              onChange={this.handleFieldChange}
            />
            I have a Treebo account
          </label>
          <button
            type="submit"
            name="button"
            className="checkout__action button"
          >Continue
          </button>
        </Formsy>
      </div>
    );
  }
}

Login.propTypes = {
  continueLogin: PropTypes.func.isRequired,
  login: PropTypes.object.isRequired,
  errorMsg: PropTypes.string,
};

export default Login;
