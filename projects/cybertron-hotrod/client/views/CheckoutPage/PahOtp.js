import React from 'react';
import PropTypes from 'prop-types';
import Modal from '../../components/Modal/Modal';
import Otp from '../../components/Otp/Otp';
import analyticsService from '../../services/analytics/analyticsService';

class PahOtp extends React.Component {
  state = {
    mobile: this.props.mobile,
  };

  onEditMobile = () => {
    const { mobile } = this.state;
    analyticsService.editMobileClicked(mobile, 'PAH');
    this.setState({
      mobile: '',
    });
  };

  onNumberSubmit = ({ mobile }) => {
    analyticsService.onOtpContinueClicked(mobile, 'PAH');
  }

  verifyOtp = ({ otp }, reset, invalidate) => {
    const { mobile } = this.state;
    const { otpActions: { onVerifyOTP }, onSuccess } = this.props;
    analyticsService.verifyOtpClicked(mobile, 'PAH');
    onVerifyOTP(mobile, otp)
      .then((res) => {
        if (res.error) {
          invalidate({ otp: res.payload.errors[0].message });
        } else if (!res.is_valid) {
          invalidate({ otp: 'Session Expired, Resend OTP.' });
        } else {
          this.setState({
            mobile: '',
          });
          analyticsService.verifyOtpStatus(true, 'PAH');
          onSuccess();
        }
      })
      .catch(({ msg } = {}) => {
        analyticsService.verifyOtpStatus(false, 'PAH');
        invalidate({ otp: msg });
      });
  };

  resendOtp = () => {
    const { mobile } = this.state;
    const { otpActions } = this.props;
    otpActions.sendOtp(mobile);
    analyticsService.resendOtpClicked(mobile, 'PAH');
  };

  sendOtp = ({ mobile }, reset, invalidate) => {
    this.props.otpActions.sendOtp(mobile)
      .then(() => {
        this.setState({ mobile });
      })
      .catch(({ msg } = {}) => {
        invalidate({ mobile: msg });
      });
  };

  render() {
    const { mobile } = this.state;
    const { onReject, onSuccess } = this.props;
    return (
      <Modal
        isOpen
        onClose={onReject}
        closeOnEsc={false}
        closeOnBackdropClick={false}
      >
        <div className="pah-otp">
          <Otp
            onEditMobile={this.onEditMobile}
            onSuccess={onSuccess}
            onReject={onReject}
            onNumberSubmit={this.onNumberSubmit}
            mobile={mobile}
            onResend={this.resendOtp}
            sendOtp={this.sendOtp}
            onVerifyOtp={this.verifyOtp}
          />
          {
            onReject ? (
              <div className="row--center">
                <button
                  id="t-skipPah"
                  type="button"
                  className="otp-screen__verify-btn button button--secondary"
                  onClick={onReject}
                >
                  SKIP, I WILL PAY NOW
                </button>
              </div>
            ) : null
          }
        </div>
      </Modal>
    );
  }
}

PahOtp.propTypes = {
  otpActions: PropTypes.object.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onReject: PropTypes.func,
  mobile: PropTypes.string.isRequired,
};

export default PahOtp;
