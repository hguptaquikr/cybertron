import React from 'react';
import cx from 'classnames';

class Special extends React.Component {
  handleFieldChange = (e) => {
    this.props.updateSpecialRequests({
      [e.target.name]: e.target.value,
    });
  };

  render() {
    const {
      special: {
        check_in_time: checkInTime,
        check_out_time: checkOutTime,
        message,
        offers,
        payAtHotelEnabled,
        payNowEnabled,
        isPrepaidOnly,
        bookingApiError,
      },
      payNow,
      payAtHotel,
    } = this.props;

    const filteredOffers = offers.filter((offer) => offer.text);
    const payAtHotelClass = cx('checkout__action button', {
      'checkout__action--secondary': payNowEnabled,
      'button--secondary': payNowEnabled,
    });

    return (
      <div>
        <h2 className="checkout__section-title checkout__section-title--active">
          Mention Special Requests
        </h2>
        <p className="checkout__section-subtitle">
          Arriving early, Need something special.
          Let us know here, we will try our best to get it done.
        </p>
        <div className="row checkout__special-cont">
          <div className="col-5">
            <div className="checkout__special-time">
              <div>
                <div className="checkout__msg">
                  Check In
                </div>
                <div className="select-box checkout__select">
                  <select
                    className="select"
                    name="check_in_time"
                    value={checkInTime}
                    onChange={this.handleFieldChange}
                  >
                    <option value="12pm">12pm</option>
                    <option value="10am">10am</option>
                    <option value="11am">11am</option>
                  </select>
                </div>
              </div>
              <div>
                <div className="checkout__msg">
                  Check Out
                </div>
                <div className="select-box checkout__select">
                  <select
                    className="select"
                    name="check_out_time"
                    value={checkOutTime}
                    onChange={this.handleFieldChange}
                  >
                    <option value="11am">11am</option>
                    <option value="12pm">12pm</option>
                    <option value="1pm">01pm</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div className="col checkout__special-info">
            <p className="checkout__msg checkout__special-infotext">
              Early check-in (between 10am and 12pm) and late check-out (upto 1pm)
              can be accommodated at no extra charges, but will be subject to availability
              &nbsp;
              <a
                className="text-primary"
                href="/faq/#checkin-checkout-policy"
                target="_blank"
                rel="noopener noreferrer"
              >Know more
              </a>
            </p>
          </div>
        </div>

        <div className="checkout__special-text">
          <textarea
            className="checkout__special-textarea"
            name="message"
            rows="4"
            cols="75"
            value={message}
            placeholder="Other requests. Mention it here"
            onChange={this.handleFieldChange}
          />
        </div>

        <div className="itinerary-view__action">
          {
            payNowEnabled ? (
              <button
                onClick={payNow}
                className="checkout__action button"
              >Pay Now
              </button>
            ) : (null)
          }
          {
            (payAtHotelEnabled && !isPrepaidOnly) ? (
              <button
                onClick={payAtHotel}
                className={payAtHotelClass}
              >Pay at hotel
              </button>
            ) : (null)
          }
        </div>
        {
          payNowEnabled && filteredOffers.length ? (
            <div>
              <p className="checkout__msg-header">Special offer on Pay Now</p>
              <ul className="checkout__msg checkout__msg--offers">
                {filteredOffers.map(({ text }) => <li key={text}>{text}</li>)}
              </ul>
            </div>
          ) : (null)
        }
        {
          bookingApiError ? (
            <div className="text-error">{bookingApiError}</div>
          ) : (null)
        }
      </div>
    );
  }
}

Special.propTypes = {
  updateSpecialRequests: React.PropTypes.func.isRequired,
  payNow: React.PropTypes.func.isRequired,
  payAtHotel: React.PropTypes.func.isRequired,
  special: React.PropTypes.object.isRequired,
};

export default Special;
