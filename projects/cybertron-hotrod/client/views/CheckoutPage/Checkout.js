import React from 'react';
import PropTypes from 'prop-types';
import cookies from 'js-cookie';
import { withRouter } from 'react-router-dom';
import PahOtp from './PahOtp';
import CheckoutSection from './CheckoutSection';
import Details from './Details';
import Payment from './Payment';
import RatePlan from './RatePlan';
import analyticsService from '../../services/analytics/analyticsService';
import searchService from '../../services/search/searchService';
import { constants } from '../../services/utils';

class Checkout extends React.Component {
  state = {
    otpEnable: false,
  };

  onPayAtHotel = () => {
    const { otp: { isOtpEnabled } } = this.props;
    if (isOtpEnabled) {
      this.otpStatus();
    } else {
      this.payAtHotel();
    }
  };

  detailsConfirmation = (data) => {
    const {
      itineraryActions: {
        detailsConfirmation,
        payNowPayment,
      },
      selectedRatePlan,
    } = this.props;
    detailsConfirmation(data)
      .then(() => payNowPayment(selectedRatePlan, 'razorpay')
        .then(() => this.confirmBooking('razorpay')));
  };

  continueDetails = (data) => {
    const { itineraryActions: { continueDetails, applyCoupon } } = this.props;
    const isReferral = cookies.get('avclk');
    if (isReferral) {
      continueDetails(data).then((res) => {
        if (!res.is_booking_requested) {
          applyCoupon(res.coupon);
        }
      });
    } else {
      continueDetails(data);
    }
  };

  sendAnalytics = (ratePlan) => {
    const {
      search: {
        datePicker: {
          range,
        },
      },
      selectedRoomType,
      selectedRatePlan,
      price: { ratePlans },
    } = this.props;

    const { abwDays } =
    searchService.formattedDatePicker(range, constants.dateFormat.query);
    const priceDifference = Math.abs(
      Math.round(
        ratePlans[ratePlan].sellingPrice - ratePlans[selectedRatePlan].sellingPrice,
      ),
    );

    analyticsService.ratePlanSelected({
      page: 'Itinerary page',
      newRoomType: selectedRoomType,
      oldRoomType: selectedRoomType,
      newRatePlan: ratePlan,
      oldRatePlan: selectedRatePlan,
      abwDays,
      priceDifference,
    });
  }

  redirectToBooking = () => {
    const {
      history,
      checkout: {
        payment: { order_id: orderId },
      },
    } = this.props;
    history.push({
      pathname: `/confirmation/${orderId}/`,
    });
  };

  otpStatus = () => {
    const {
      checkout: { details: { mobile, email, name } },
      otpActions: { checkOtpVerified, sendOtp },
    } = this.props;

    checkOtpVerified(mobile, email, name)
      .then((res) => {
        if (res.is_verified) {
          this.payAtHotel();
        } else {
          sendOtp(mobile);
          this.setState({ otpEnable: true });
        }
      });
  };

  refreshBookingId = async () => {
    const {
      checkout: {
        payment: { order_id: orderId },
      },
      itineraryActions: {
        getCartContent,
      },
      selectedRatePlan,
    } = this.props;
    if (orderId) await getCartContent(null, selectedRatePlan);
  }

  payAtHotel = async () => {
    const {
      checkout: {
        payment: { amount_to_send: amountToSend },
      },
      itineraryActions: { payAtHotel },
      selectedRatePlan,
    } = this.props;
    analyticsService.paymentModeSelected('pay_at_hotel', amountToSend, selectedRatePlan);
    await this.refreshBookingId();
    payAtHotel(selectedRatePlan).then(this.redirectToBooking);
  };

  payNow = () => {
    const {
      checkout: {
        payment: { amount_to_send: amountToSend },
      },
      itineraryActions: { payNow },
    } = this.props;

    if (amountToSend) {
      payNow();
    } else {
      this.payAtHotel();
    }
  };

  otpSuccess = () => {
    this.setState({ otpEnable: false });
    this.payAtHotel();
  };

  otpReject = () => {
    this.setState({ otpEnable: false });
  };

  confirmBooking = (gateway, id, signature) => {
    const { itineraryActions: { confirmBooking }, selectedRatePlan } = this.props;
    return confirmBooking({
      gateway,
      id,
      signature,
      selectedRatePlan,
    }).then(this.redirectToBooking);
  };

  render() {
    const {
      checkout: { ratePlans, details, payment },
      itineraryActions: {
        togglePanel,
        payNowPayment,
        payNow,
        hidePaymentOption,
      },
      selectedRoomType,
      selectedRatePlan,
      getCartContent,
      price,
      otpActions,
      itineraryActions,
      hotelId,
      hotel,
    } = this.props;
    return (
      <div className="checkout">
        <ul>
          <li>
            <CheckoutSection togglePanel={togglePanel} data={ratePlans} index={1}>
              <RatePlan
                sendAnalytics={this.sendAnalytics}
                getCartContent={getCartContent}
                selectedRatePlan={selectedRatePlan}
                selectedRoomType={selectedRoomType}
                ratePlans={price.ratePlans}
              />
            </CheckoutSection>
          </li>
          <li>
            <CheckoutSection togglePanel={togglePanel} data={details} index={2}>
              <Details
                details={details}
                detailsConfirmation={this.detailsConfirmation}
                continueDetails={this.continueDetails}
                ratePlans={price.ratePlans}
                selectedRatePlan={selectedRatePlan}
                hotelId={hotelId}
                hotel={hotel}
              />
            </CheckoutSection>
          </li>
          <li>
            <CheckoutSection togglePanel={togglePanel} data={payment} index={3}>
              <Payment
                payment={payment}
                payNow={payNow}
                itineraryActions={itineraryActions}
                refreshBookingId={this.refreshBookingId}
                selectedRatePlan={selectedRatePlan}
                hidePaymentOption={hidePaymentOption}
                onPay={payNowPayment}
                payAtHotel={this.onPayAtHotel}
                confirmBooking={this.confirmBooking}
                ratePlans={price.ratePlans}
              />
            </CheckoutSection>
          </li>
        </ul>
        {
          this.state.otpEnable ?
            <PahOtp
              onSuccess={this.otpSuccess}
              onReject={this.otpReject}
              otpActions={otpActions}
              mobile={details.mobile}
            />
            : null
        }
      </div>
    );
  }
}

Checkout.propTypes = {
  checkout: PropTypes.object.isRequired,
  hotelId: PropTypes.number.isRequired,
  itineraryActions: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  otpActions: PropTypes.object.isRequired,
  otp: PropTypes.object.isRequired,
  selectedRoomType: PropTypes.string,
  selectedRatePlan: PropTypes.string,
  getCartContent: PropTypes.func,
  price: PropTypes.object,
  hotel: PropTypes.object,
  search: PropTypes.object.isRequired,
};

export default withRouter(Checkout);
