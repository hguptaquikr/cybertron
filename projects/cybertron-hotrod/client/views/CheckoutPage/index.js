import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

const CheckoutPage = Loadable({
  loader: () => {
    importCss('CheckoutPage');
    return import('./CheckoutPage' /* webpackChunkName: 'CheckoutPage' */);
  },
  loading: () => null,
});

export default CheckoutPage;
