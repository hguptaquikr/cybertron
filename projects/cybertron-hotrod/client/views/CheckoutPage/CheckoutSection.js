import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const CheckoutSection = ({
  data,
  index,
  children,
  togglePanel,
}) => {
  if (!data || !data.isVisible) {
    return null;
  }

  const {
    summary: { title, subtitle },
    isOpen,
    isEditable,
    section,
  } = data;

  return (
    <div className="checkout__section">
      <div className="checkout__index">
        {
          isEditable ? (
            <input type="checkbox" defaultChecked disabled />
          ) : (
            <span
              className={cx('checkout__badge', {
                'checkout__badge--active': isOpen,
              })}
            >{index}
            </span>
          )
        }
      </div>
      {
        isOpen ? (
          children
        ) : (
          <div className="checkout__summary">
            <div>
              <h2
                className={cx('subheading-3 checkout__section-title', {
                  'checkout__section-title--completed': isEditable,
                })}
              >{title}
              </h2>
              <p className="checkout__section-subtitle">{subtitle}</p>
            </div>
            {
              isEditable && (
                <button
                  className="button button--tertiary checkout__edit"
                  onClick={() => togglePanel(section)}
                >Edit
                </button>
              )
            }
          </div>
        )
      }
    </div>
  );
};

CheckoutSection.propTypes = {
  data: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired,
  togglePanel: PropTypes.func.isRequired,
};

export default CheckoutSection;
