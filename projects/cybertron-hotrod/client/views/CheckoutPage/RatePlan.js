import React from 'react';
import PropTypes from 'prop-types';
import RatePlans from '../../components/RatePlans/RatePlans';
import ABButton from '../../components/ABButton/ABButton';
import priceService from '../../services/price/priceService';

class RatePlan extends React.Component {
  state = {
    selectedRatePlan: this.props.selectedRatePlan,
  }

  changeRatePlan = (plan) => {
    const { getCartContent, sendAnalytics } = this.props;
    this.setState({
      selectedRatePlan: plan,
    });
    sendAnalytics(plan);
    getCartContent(plan);
  }

  render() {
    const { selectedRatePlan } = this.state;

    const {
      getCartContent,
      selectedRoomType,
      ratePlans = {},
    } = this.props;

    const ratePlansViewOrder = priceService.sortedRatePlans(ratePlans);

    return (
      <div className="checkout__rate-plan">
        <div className="col-12">
          <h2 className="checkout__section-title checkout__section-title--active">
            SELECT PLAN
          </h2>
          {
            ratePlans ? (
              ratePlansViewOrder.map((plan) => (
                ratePlans[plan] ? (
                  <RatePlans
                    key={plan}
                    plan={plan}
                    rateType={ratePlans[plan].type}
                    changeRatePlan={this.changeRatePlan}
                    roomType={selectedRoomType}
                    isSelected={selectedRatePlan === plan}
                    selectedRatePlan={selectedRatePlan}
                    sellingPrice={ratePlans[plan].sellingPrice}
                    strikedPrice={ratePlans[plan].strikedPrice}
                    discountPercentage={ratePlans[plan].discountPercentage}
                    selectionType="radio"
                  />
                ) : null
              ))
            ) : null
          }
        </div>
        <ABButton
          id="t-itineraryRatePlanCTA"
          onClick={() => getCartContent(selectedRatePlan, true)}
          className="checkout__rate-plan__continue"
          experiment="checkoutCtaDesktop"
          render={(text) => text || 'CONTINUE'}
        />
      </div>
    );
  }
}

RatePlan.propTypes = {
  sendAnalytics: PropTypes.func,
  getCartContent: PropTypes.func.isRequired,
  selectedRatePlan: PropTypes.string,
  selectedRoomType: PropTypes.string,
  ratePlans: PropTypes.object,
};

export default RatePlan;
