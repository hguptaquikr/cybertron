import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

const NotFoundPage = Loadable({
  loader: () => {
    importCss('NotFoundPage');
    return import('./NotFoundPage' /* webpackChunkName: 'NotFoundPage' */);
  },
  loading: () => null,
});

export default NotFoundPage;
