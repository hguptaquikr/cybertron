import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import * as contentActionCreators from '../../services/content/contentDuck';
import image404 from './404.png';
import './notFoundPage.css';

class NotFoundPage extends React.Component {
  static componentWillServerRender = ({ store: { dispatch, getState }, route, match }) => {
    const promises = [];

    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));

    if (isEmpty(getState().cities)) {
      promises.push(dispatch(contentActionCreators.getLandingContent()));
    }

    return Promise.all(promises);
  }

  componentDidMount() {
    const { cities, contentActions, route, match } = this.props;
    contentActions.getSeoContent(route, match.params);
    if (isEmpty(cities)) contentActions.getLandingContent();
  }

  render() {
    const { cities } = this.props;
    return (
      <div className="unavailable">
        <div className="flex-column container unavailable__container">
          <img src={image404} alt="404 page" className="unavailable__images" />
          <div className="unavailable__title">It looks like you are lost..</div>
          <div className="unavailable__info">
            The page you are looking for doesn&apos;t exist, here are some helpful links instead
          </div>
          <div className="unavailable__city-links flex-row">
            <span className="unavailable__city-links__title">Hotels in:</span>
            {
              cities.map((city) => (
                <Link
                  key={city.name}
                  className="unavailable__city-links__item"
                  to={`${city.url}/`}
                >{city.name}
                </Link>
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

NotFoundPage.propTypes = {
  contentActions: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  cities: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  cities: state.content.cities,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
)(NotFoundPage);
