import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

const CitiesPage = Loadable({
  loader: () => {
    importCss('CitiesPage');
    return import('./CitiesPage' /* webpackChunkName: 'CitiesPage' */);
  },
  loading: () => null,
});

export default CitiesPage;
