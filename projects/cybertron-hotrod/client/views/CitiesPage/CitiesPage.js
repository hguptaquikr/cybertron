import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import sortBy from 'lodash/sortBy';
import * as contentActionCreators from '../../services/content/contentDuck';
import analyticsService from '../../services/analytics/analyticsService';
import SearchWidget from '../../views/SearchWidget/SearchWidget';
import './citiesPage.css';

class CitiesPage extends React.Component {
  static componentWillServerRender = ({ store: { dispatch, getState }, route, match }) => {
    const promises = [];

    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));

    if (isEmpty(getState().content.cities)) {
      promises.push(dispatch(contentActionCreators.getLandingContent()));
    }

    return Promise.all(promises);
  }

  componentDidMount() {
    const { cities, contentActions, route, match } = this.props;

    contentActions.getSeoContent(route, match.params);
    if (isEmpty(cities)) contentActions.getLandingContent();
  }

  getMaxHeight = () => {
    const { cities } = this.props;
    const cityListItemHeight = 5;
    const noOfColumns = 3;
    const noOfCities = cities.length;
    const noOfCitiesPerColumn = Math.ceil(noOfCities / noOfColumns);
    return `${noOfCitiesPerColumn * cityListItemHeight}rem`;
  }

  render() {
    const { cities } = this.props;
    const sortedCities = sortBy(cities, 'name');
    return (
      <div className="container cities-page">
        <SearchWidget className="cities-page__search" />
        <div className="cities-page__container">
          <h1 className="heading-1 cities-page__title">
            WE ARE NOW IN {cities.length} CITIES ALL OVER INDIA &amp; MORE ARE UPCOMING
          </h1>
          <ul style={{ height: this.getMaxHeight() }} className="cities-page__list">
            {
              sortedCities.map((city) => (
                <li key={city.name} className="text-1 cities-page__list-item">
                  <Link
                    id="t-singleCity"
                    onClick={() => analyticsService.cityClicked(city.name)}
                    to={`${city.url}/`}
                  >
                    {city.name}
                  </Link>
                </li>
              ))
            }
          </ul>
        </div>
      </div>
    );
  }
}

CitiesPage.propTypes = {
  contentActions: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  cities: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  cities: state.content.cities,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CitiesPage);
