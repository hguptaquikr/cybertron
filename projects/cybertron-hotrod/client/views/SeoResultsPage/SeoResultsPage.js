import isEmpty from 'lodash/isEmpty';
import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Sticky from 'react-stickynode';
import qs from 'query-string';
import * as searchActionCreators from '../../services/search/searchDuck';
import * as contentActionCreators from '../../services/content/contentDuck';
import * as filterActionCreators from '../../services/filter/filterDuck';
import * as hotelActionCreators from '../../services/hotel/hotelDuck';
import * as priceActionCreators from '../../services/price/priceDuck';
import * as walletActionCreators from '../../services/wallet/walletDuck';
import { getTotalGuests, safeKeys, constants } from '../../services/utils';
import HotelDetailsPage from '../HotelDetailsPage';
import CheckoutPage from '../CheckoutPage';
import AboutPlace from './AboutPlace';
import authService from '../../services/auth/authService';
import WeatherWidget from './WeatherWidget';
import SeoFilters from './SeoFilters';
import SeoHeader from './SeoHeader';
import ResultItem from '../ResultsPage/ResultItem';
import ResultsEmpty from '../ResultsPage/ResultsEmpty';
import ResultsLoading from '../ResultsPage/ResultsLoading';
import Blogs from '../../components/Blogs/Blogs';
import Seo from '../../components/Seo/Seo';
import SeoLinksFooter from '../../components/Seo/SeoLinksFooter';
import Sort from '../ResultsPage/Sort';
// import WalletBanner from '../../components/WalletBanner/WalletBanner';
// import AutoApplyCouponBanner from '../../components/AutoApplyCouponBanner/AutoApplyCouponBanner';
import QualityGuaranteeBanner from '../../components/QualityGuaranteeBanner/QualityGuaranteeBanner';
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';
import CustomGoogleMap from '../../components/CustomGoogleMap/CustomGoogleMap';
import ErrorBoundary from '../../components/ErrorBoundary/ErrorBoundary';
import SeoReviews from './SeoReviews';
import QuestionsAndAnswers from '../../views/QuestionsAndAnswers/QuestionsAndAnswers';
import analyticsService from '../../services/analytics/analyticsService';
import filterService from '../../services/filter/filterService';
import searchService from '../../services/search/searchService';
import * as hotelService from '../../services/hotel/hotelService';
import * as abService from '../../services/ab/abService';
import './seoResultsPage.css';

class SeoResultsPage extends React.Component {
  static componentWillServerRender = ({ store: { dispatch }, route, match, req }) => {
    const promises = [];

    dispatch(searchActionCreators.setSearchState({ ...req.query, ...match.params }));

    promises.push(dispatch(contentActionCreators.getSeoContent(route, match.params)));

    promises.push(
      dispatch(hotelActionCreators.getHotelResults(req.query, match.params))
        .then(() => dispatch(hotelActionCreators.addDistanceToResults()))
        .then(() => dispatch(filterActionCreators.refineResults())),
    );

    return Promise.all(promises);
  }

  componentDidMount() {
    const {
      searchActions,
      location,
      match,
      walletActions,
      contentActions,
      route,
    } = this.props;
    searchActions.setSearchState({ ...qs.parse(location.search), ...match.params });
    contentActions.getSeoContent(route, match.params);
    this.getHotelResults();
    this.getGeoLocation();
    if (authService.isLoggedIn()) {
      walletActions.getWalletBalance();
    }
    HotelDetailsPage.preload();
    CheckoutPage.preload();
  }

  componentWillReceiveProps(nextProps) {
    const { props } = this;
    const { contentActions, searchActions } = this.props;

    const propsLocationQuery = qs.parse(props.location.search);
    const nextPropsLocationQuery = qs.parse(nextProps.location.search);

    if (props.location.pathname !== nextProps.location.pathname) {
      if (
        props.match.params.city !== nextProps.match.params.city
        || props.match.params.locality !== nextProps.match.params.locality
        || props.match.params.landmark !== nextProps.match.params.landmark
        || props.match.params.category !== nextProps.match.params.category
        || (props.user.isAuthenticated && !nextProps.user.isAuthenticated)
        || (
          props.location.pathname === nextProps.location.pathname
            && (
              propsLocationQuery.checkin !== nextPropsLocationQuery.checkin
              || propsLocationQuery.checkout !== nextPropsLocationQuery.checkout
              || propsLocationQuery.roomconfig !== nextPropsLocationQuery.roomconfig
            )
        )
      ) {
        searchActions.setSearchState({
          ...qs.parse(nextProps.location.search), ...nextProps.match.params,
        });
        contentActions.getSeoContent(nextProps.route, nextProps.match.params);
        this.getHotelResults(nextProps);
        this.getGeoLocation();
      }
    }
  }

  componentWillUnmount() {
    const { filterActions } = this.props;
    filterActions.clearResults();
    filterActions.clearAllFilters();
  }

  onLoginSuccess = () => {
    const {
      hotel,
    } = this.props;
    const hotelIds = Object.keys(hotel.byId);
    this.getSearchPrices(hotelIds);
  }

  getGeoLocation = async () => {
    const { match, location, history } = this.props;
    if (match.params.landmark === 'me') {
      const res = await searchService.getIpLocation();
      filterService.sortByNearMe({ location, history, city: res.city });
    }
  }

  getHotelResults = (props = this.props) => {
    const { location, hotelActions, filterActions, match } = props;
    hotelActions.getHotelResults(qs.parse(location.search), match.params)
      .then((res) => {
        hotelActions.addDistanceToResults();
        if (res.ids.length) this.getSearchPrices(res.ids);
        filterActions.clearAllFilters();
        filterActions.constructLocalitiesFilter();
      });
  }

  getSearchPrices = (hotelIds) => {
    const {
      search: { datePicker, occupancy },
      priceActions,
      filterActions,
      location,
      match,
    } = this.props;
    if (datePicker.range.start && datePicker.range.end && occupancy.rooms.length) {
      priceActions.getSearchPrices(hotelIds, location.query, match.params)
        .then(() => {
          const isMemberDiscountApplied = this.isMemberDiscountApplied();
          if (isMemberDiscountApplied) analyticsService.memberDiscountApplied('Search Result Page');
          filterActions.refineResults();
          filterActions.constructPriceRangesFilter();
          this.captureAnalytics(qs.parse(location.search));
        });
    }
  }

  checkPricing = () => {
    const { searchActions, hotel } = this.props;
    const hotelIds = Object.keys(hotel.byId);
    if (searchActions.validateSearch()) {
      this.getSearchPrices(hotelIds);
    }
  }

  captureAnalytics = () => {
    const {
      match,
      search: {
        occupancy: { rooms },
        datePicker: {
          range,
        },
        searchInput: {
          location,
        },
      },
      hotel: {
        ids,
      },
    } = this.props;
    const { checkIn, checkOut, numberOfNights, abwDays } =
    searchService.formattedDatePicker(range, constants.dateFormat.query);
    analyticsService.listContentViewed({
      flow: 'browse',
      destination: match.params.city || location.label,
      checkin: checkIn,
      checkout: checkOut,
      lengthOfStay: numberOfNights.count,
      abwDays,
      ...getTotalGuests(rooms),
      rooms: rooms.length,
      resultCount: ids.length,
      ...abService.impressionProperties(['findPageCtaDesktop', 'quickBookCTADesktop']),
    });
  }

  isMemberDiscountApplied = () => {
    const { price: { byRoomTypeId: prices } } = this.props;
    return Object.values(prices).some((price) => price.memberDiscount.isApplied);
  }

  isCoupleFriendlyHotelAvailable = (hotels, refinedHotelIds) =>
    !!refinedHotelIds.length && refinedHotelIds.some((hotelId) => {
      const hotel = hotels.byId[hotelId];
      return hotel && hotelService.isCoupleFriendly(hotel.policies);
    });

  storeScrollRef = (scrollRef) => {
    if (scrollRef !== null) this.scrollRef = scrollRef;
  }

  render() {
    const {
      content: { seo },
      match,
      location,
      search,
      hotelActions,
      filterActions,
      filter: { refinedHotelIds, sort, filters },
      price,
      hotel,
      // wallet,
      route,
      $roomType,
    } = this.props;

    const isCoupleFriendlyHotelAvailable =
      this.isCoupleFriendlyHotelAvailable(hotel, refinedHotelIds);
    const zoomIn = (route.name === 'city' || route.name === 'category') ? 10 : 15;
    const analyticsBookClicked = (args, index) => (
      hotelActions.bookNowClicked(args, {
        searchRank: index + 1,
        destination: search.searchInput.location.label,
        resultCount: refinedHotelIds.length,
      })
    );

    const analyticsSetHotelDetails = (prefillHotel, type, hotelData, index) => (
      analyticsService.hotelViewDetailsClicked(prefillHotel, {
        searchRank: index + 1,
        destination: search.searchInput.location.label,
        resultCount: refinedHotelIds.length,
        type,
      })
    );

    let resultsJsx;
    if (hotel.isHotelResultsLoading) {
      resultsJsx = [0, 1, 2, 3, 4].map((key) => <ResultsLoading key={key} />);
    } else if (refinedHotelIds.length || price.isPricesLoading) {
      resultsJsx = refinedHotelIds.map((hotelId, index) =>
        (<ResultItem
          key={hotelId}
          hotel={hotel.byId[hotelId]}
          prices={price.byRoomTypeId}
          search={search}
          setHotelDetails={
            (...args) => analyticsSetHotelDetails(...args, hotel.byId[hotelId], index)
          }
          bookNowClicked={(args) => analyticsBookClicked(args, index)}
          isPricesLoading={price.isPricesLoading}
          checkPricing={this.checkPricing}
          location={location}
          params={match.params}
          $roomType={$roomType}
          shouldSetParams={false}
        />));
    } else {
      resultsJsx = <ResultsEmpty isTrulyEmpty={!hotel.ids.length} />;
    }

    return (
      <div className="seo-results-page">
        {
          !hotel.isHotelResultsLoading && !refinedHotelIds.length && match.params.landmark !== 'me' ? (
            <Helmet meta={[{ name: 'robots', content: 'noindex, follow' }]} />
          ) : null
       }
        <SeoHeader
          search={search}
          location={location}
          params={match.params}
          cityImage={seo.content.cityImageUrl}
          minPrice={seo.content.minPrice.toString()}
        />
        <div className="container seo-results-page__breadcrumb">
          <Breadcrumbs />
        </div>
        <div className="container seo-results-page__body" id="resultsPageBody">
          <div className="row seo-results-page__bodyRow">
            <div className="col-9 ">
              {/*
                {
                  wallet.byType.TP.isApplied ? (
                    <WalletBanner
                      walletAmount={wallet.byType.TP.usableBalance}
                      src="seo-srp"
                    />
                  ) : null
                }
                {
                  !price.isPricesLoading ? (
                    <AutoApplyCouponBanner />
                  ) : null
                }
              */}

              <div className="seo-results-page__quality-guarantee-banner">
                <QualityGuaranteeBanner
                  page={match.params.city ? 'Hotels In City' : 'Search'}
                />
              </div>
              <meta itemScope itemType="http://schema.org/WebPage" />
              <meta itemProp="name" content={route.name} />
              <div
                ref={this.storeScrollRef}
                className="seo-results-page__results r-list"
              >
                <Seo index={match.params.landmark !== 'me'}>
                  {resultsJsx}
                </Seo>
              </div>
              {
                safeKeys(hotel.byId).length === 1 ? (
                  <div className="subheading-2 seo-results-page__single-result-text">
                    That&apos;s all folks,<br />
                    There&apos;s only one and it&apos;s the best option available in this location.
                  </div>
                ) : (null)
              }
            </div>
            <div className="col-3">
              <Sticky top={16} bottomBoundary="#resultsPageBody">
                <Sort
                  sort={sort}
                  filters={filters}
                  filterActions={filterActions}
                />
                <SeoFilters
                  route={route}
                  noOfResults={hotel.ids.length}
                  filters={filters}
                  filterActions={filterActions}
                  scrollRef={this.scrollRef}
                  filterClicked={analyticsService.filterClicked}
                  links={seo.content.links}
                  nearPlaces={seo.content.leftNav}
                  isCoupleFriendlyHotelAvailable={isCoupleFriendlyHotelAvailable}
                />
              </Sticky>
            </div>
          </div>
        </div>

        <div className="seo-results-page__seo-content">
          <ErrorBoundary render={() => null}>
            {
              !isEmpty(seo.content.description) ? (
                <AboutPlace description={seo.content.description} />
              ) : (null)
            }
          </ErrorBoundary>
          <ErrorBoundary render={() => null}>
            <section className="rp-seocontent">
              <div className="container seo-results-page__weather-map-section">
                {
                  !isEmpty(seo.content.weather) ? (
                    <WeatherWidget weatherWidget={seo.content.weather} />
                  ) : (null)
                }
                {
                  !isEmpty(seo.content.coordinates) ? (
                    <CustomGoogleMap placeCoordinate={seo.content.coordinates} zoom={zoomIn} />
                  ) : (null)
                }
              </div>
            </section>
          </ErrorBoundary>
          <ErrorBoundary render={() => null}>
            {
              !isEmpty(seo.content.blogs) ? (
                <Blogs blogs={seo.content.blogs} />
              ) : (null)
            }
          </ErrorBoundary>
          <ErrorBoundary render={() => null}>
            {
              !isEmpty(seo.content.reviews) ? (
                <SeoReviews
                  location={location}
                  search={search}
                  hotels={hotel.byId}
                  prices={price.byRoomTypeId}
                  topReviews={seo.content.reviews}
                />
              ) : (null)
            }
          </ErrorBoundary>
          <ErrorBoundary render={() => null}>
            {
              !isEmpty(seo.content.qna) ? (
                <div className="container">
                  <QuestionsAndAnswers QnAs={seo.content.qna} />
                </div>
              ) : (null)
            }
          </ErrorBoundary>
          <ErrorBoundary render={() => null}>
            {
              !isEmpty(seo.content.links) ? (
                <SeoLinksFooter links={seo.content.links} />
              ) : (null)
            }
          </ErrorBoundary>
        </div>
      </div>
    );
  }
}

SeoResultsPage.propTypes = {
  search: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  priceActions: PropTypes.object.isRequired,
  hotelActions: PropTypes.object.isRequired,
  filterActions: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  // wallet: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired, // eslint-disable-line
  price: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  $roomType: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  wallet: state.wallet,
  search: state.search,
  price: state.price,
  filter: state.filter,
  content: state.content,
  hotel: state.hotel,
  $roomType: state.$roomType,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  priceActions: bindActionCreators(priceActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  filterActions: bindActionCreators(filterActionCreators, dispatch),
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(SeoResultsPage);
