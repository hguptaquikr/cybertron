import React from 'react';
import PropTypes from 'prop-types';
import startCase from 'lodash/startCase';

const WeatherWidget = ({
  weatherWidget: {
    title,
    content,
  },
}) => (
  <div className="weather-widget" id="t-weatherWidget">
    <div className="weather-widget__title">
      {startCase(title)}
    </div>
    <div className="weather-widget__list">
      {
        content.map((item) => (
          <div className="weather-widget__item">
            <p className="text-2 weather-widget__temp">
              {item.avgTemp}°c
            </p>
            <img className="weather-widget__image" src={item.icon} alt="Temp" />
            <p>
              {item.date}
            </p>
          </div>
        ))
      }
    </div>
  </div>
);
WeatherWidget.propTypes = {
  weatherWidget: PropTypes.object.isRequired,
};

export default WeatherWidget;
