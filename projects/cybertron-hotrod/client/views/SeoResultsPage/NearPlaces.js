import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import contentService from '../../services/content/contentService';

class NearPlaces extends React.Component {
  state = {
    places: this.props.nearPlaces.content,
  }

  componentWillReceiveProps(nextProps) {
    const { nearPlaces } = this.props;
    if (nearPlaces.title !== nextProps.nearPlaces.title) {
      this.setState({ places: nextProps.nearPlaces.content });
    }
  }

  searchPlace = (e) => {
    const { value } = e.target;
    const places = this.props.nearPlaces.content;
    const filterPlace = places.filter((item) =>
      item.label.split(',')[0].toLowerCase().indexOf(value.toLowerCase()) > -1);
    this.setState({ places: filterPlace });
  }

  render() {
    const { nearPlaces: { key, title } } = this.props;
    const { places } = this.state;

    let placeholderText = 'Select locality or landmark';
    if (key === 'hotel_types') {
      placeholderText = 'Select hotel types';
    }
    if (key === 'popular_amenities') {
      placeholderText = 'Select amenities';
    }

    return (
      <div className="near-places">
        <div className="subheading-3">{title}</div>
        <div className="near-places__search">
          <i className="icon-location near-places__icon" />
          <input
            type="text"
            className="near-places__search-input"
            placeholder={placeholderText}
            onChange={this.searchPlace}
          />
        </div>
        <div className="near-places__list">
          <ul>
            {
              places.map((place) => {
                const label = place.label.split(',')[0];
                return (
                  <li className="text-2 near-places__list-item">
                    <Link
                      id="t-nearPlaces"
                      key={label}
                      to={contentService.getSeoPathname(place)}
                    >
                      {label}
                    </Link>
                  </li>
                );
              })
            }
          </ul>
        </div>
      </div>
    );
  }
}

NearPlaces.propTypes = {
  nearPlaces: PropTypes.object.isRequired,
};

export default NearPlaces;
