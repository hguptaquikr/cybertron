/* eslint-disable react/no-danger */
import React from 'react';
import PropTypes from 'prop-types';
import ShowMore from '../../components/ShowMore/ShowMore';

const HtmlDescription = ({ content }) => (
  <div dangerouslySetInnerHTML={{ __html: content }} /> // eslint-disable-line
);

HtmlDescription.propTypes = {
  content: PropTypes.string.isRequired,
};

class AboutPlace extends React.Component {
  state = {}

  render() {
    const {
      description: {
        title,
        content,
      },
    } = this.props;

    return (
      <section className="rp-seocontent" id="t-aboutPlace">
        <div className="container rp-seocontent__desc-title">
          {title}
        </div>
        <div className="container">
          <ShowMore
            actionClassName="rp-seocontent__read-more-less"
            initialHeight={160}
            showMoreText="READ MORE"
            showLessText="READ LESS"
          >
            <HtmlDescription content={content} />
          </ShowMore>
        </div>
      </section>
    );
  }
}

AboutPlace.propTypes = {
  description: PropTypes.object.isRequired,
};

export default AboutPlace;
