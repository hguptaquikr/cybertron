import React from 'react';
import PropTypes from 'prop-types';
import Sticky from 'react-stickynode';
import isEmpty from 'lodash/isEmpty';
import startCase from 'lodash/startCase';
import SearchWidget from '../../views/SearchWidget/SearchWidget';

const SeoHeader = ({ search, location, params, cityImage, minPrice }) => {
  const {
    searchInput,
    datePicker: { range },
  } = search;

  let resultCountText = '';
  let resultHeader = '';

  const priceText = minPrice ? ` - Prices start from ₹${minPrice}` : '';
  resultHeader = startCase(location.pathname);
  resultCountText = `List of ${resultHeader} with tariff ${priceText}`;
  const cityImageUrl = `${cityImage}?fm=pjpg&auto=compress`;
  const cityName = params.city ? params.city : '';

  return (
    <div className="seo-results-page__banner-body">
      <img src={cityImageUrl} className="seo-results-page__banner" alt={cityName} />
      <div className="container seo-results-page__query">
        <div className="seo-results-page__info">
          <div className="seo-results-page__query-container">
            {
              resultHeader ? (
                <h1 className="seo-results-page__header">{resultHeader}</h1>
              ) : (null)
            }
            <div className="seo-results-page__query-container__title">
              {resultCountText}
            </div>
          </div>
        </div>
      </div>
      <div className="seo-results-page__search">
        <Sticky activeClass="seo-search-widget--sticky">
          <div className="seo-search-widget">
            <SearchWidget
              className="seo-results-page__search-widget"
              isSearchDisabled={
                range.start
                && range.end
                && !isEmpty(searchInput.location)
              }
            />
          </div>
        </Sticky>
      </div>
    </div>
  );
};

SeoHeader.propTypes = {
  search: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
  cityImage: PropTypes.string.isRequired,
  minPrice: PropTypes.string.isRequired,
};

export default SeoHeader;
