import React from 'react';
import PropTypes from 'prop-types';

const AssuredEssentials = ({
  amenities,
}) => (
  <div className="assured-amenities">
    <div className="assured-amenities__title text-1">ASSURED ESSENTIALS</div>
    <p className="assured-amenities__subtitle text-2">Guaranteed at all our hotels</p>
    <ul className="assured-amenities__amenities assured-amenities__list">
      {
        amenities.map(({ key, css_class: cssClass, name }) => (
          <li key={key} className="assured-amenities__item">
            <i className={`assured-amenities__icon icon-${cssClass}`} />
            {name}
          </li>
        ))
      }
    </ul>
  </div>
);

AssuredEssentials.propTypes = {
  amenities: PropTypes.array.isRequired,
};

export default AssuredEssentials;
