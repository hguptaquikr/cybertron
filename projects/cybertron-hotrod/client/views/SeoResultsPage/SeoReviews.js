import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Slider from '../../components/Slider/Slider';
import reviewService from '../../services/review/reviewService';
import * as hotelService from '../../services/hotel/hotelService';

class SeoReviews extends Component {
  getHotelDetailsTarget = (hotel) =>
    hotel ? hotelService.getHotelDetailsTarget({
      hotelId: hotel.id,
      hotelName: hotel.name,
      area: hotel.address,
    }).pathname : '/'

  findHotel = (hotels, hotel) =>
    Object.values(hotels).find((singleHotel) => singleHotel.id === hotel.id);

  renderReview = (review) => {
    const { hotels } = this.props;
    const textLength = review.text.length;
    const hotel = this.findHotel(hotels, review.hotel);
    const hotelDetailsTarget = this.getHotelDetailsTarget(hotel);
    const showMoreTextEnabled = textLength > 300;
    const lessText = showMoreTextEnabled ? `“${review.text.slice(0, 300)}“` : `“${review.text}“`;
    const moreText = showMoreTextEnabled ? ` ${review.text.slice(300, textLength)}` : '';
    return (
      <div className="seo-reviews__review">
        <span className="text-1 seo-reviews__review__hotel-link"> Review for </span>
        <Link
          className="text-link"
          to={hotelDetailsTarget}
        >
          {review.hotel.name}
        </Link>
        <div className="text-1 seo-reviews__review__title">{review.title}</div>
        <div className="seo-reviews__review-desc">
          {lessText}
          <span className="hide">{moreText}</span>
        </div>
        <div className="seo-reviews__detail">
          <img src={review.ratingImage} alt="review" className="seo-reviews__ta-image" />
          <span className="seo-reviews__user">-{review.user.name}, {review.publishDate}</span>
        </div>
      </div>
    );
  }

  render() {
    const {
      topReviews: {
        title,
        reviewsList,
      },
    } = this.props;
    const reviewsToDisplay = reviewService.getJCR(reviewsList);
    return (
      <section className="seo-reviews">
        <div className="container seo-reviews__desc-title">
          <h2>{title}</h2>
        </div>
        <div className="container">
          <div className="seo-reviews__review-section">
            {
              reviewsToDisplay.length > 2 ? (
                <Slider
                  dots={false}
                  infinite={false}
                  lazyLoad={false}
                  speed={500}
                  slidesToShow={2}
                  slidesToScroll={1}
                  className="container"
                  arrowClass="slider__arrow--large seo-reviews__arrows"
                >
                  {reviewsToDisplay.map(this.renderReview)}
                </Slider>
              ) : (
                reviewsToDisplay.map(this.renderReview)
              )
            }
          </div>
        </div>
      </section>
    );
  }
}

SeoReviews.propTypes = {
  topReviews: PropTypes.object.isRequired,
  hotels: PropTypes.object.isRequired,
};

export default SeoReviews;
