import Loadable from 'react-loadable';
import importCss from '../../services/importCss';

const SeoResultsPage = Loadable({
  loader: () => {
    importCss('SeoResultsPage');
    return import('./SeoResultsPage' /* webpackChunkName: 'SeoResultsPage' */);
  },
  loading: () => null,
});

export default SeoResultsPage;
