import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import qs from 'query-string';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import kebabCase from 'lodash/kebabCase';
import * as searchActionCreators from '../../services/search/searchDuck';
import SearchInput from '../../components/SearchInput/SearchInput';
import DatePicker from '../../components/DatePicker/DatePicker';
import Occupancy from '../../components/Occupancy/Occupancy';
import ABButton from '../../components/ABButton/ABButton';
import filterService from '../../services/filter/filterService';
import searchService from '../../services/search/searchService';
import analyticsService from '../../services/analytics/analyticsService';
import * as routeService from '../../services/route/routeService';
import './searchWidget.css';

class SearchWidget extends React.Component {
  state = {
    wasSearchButtonClicked: false,
    isSearchDisabled: this.props.isSearchDisabled,
  }

  onSearchClick = () => {
    const {
      search: {
        datePicker: {
          range,
        },
        searchInput,
      },
      onSearchClick,
      searchActions,
    } = this.props;

    this.setState({ wasSearchButtonClicked: true }, () => {
      if (!searchActions.validateSearch(this.searchInput)) return;
      if (searchInput.location.hotel_id) this.handleHotelSearch();
      else if (searchInput.location.type === 'near_me') this.handleNearMeSearch();
      else this.handleSearch();

      if (onSearchClick) onSearchClick();
      analyticsService.searchTreebo();
    });

    if (range.start && range.end && !isEmpty(searchInput.location)) {
      this.setState({ isSearchDisabled: true });
    }
  }

  onSearchInputBlur = () => {
    this.setState({ wasSearchButtonClicked: false });
  }

  storeSearchInputRef = (autosuggest) => {
    if (autosuggest !== null) this.searchInput = autosuggest.input;
  }

  handleHotelSearch = () => {
    const { searchActions } = this.props;
    searchActions.checkHotelAvailability()
      .then((response) => (
        response.available ? this.showHotelDetails() : this.showNearbyHotels()
      ));
  }

  handleNearMeSearch = async () => {
    const {
      search: {
        searchInput,
        datePicker,
        occupancy,
      },
      location,
      history,
    } = this.props;
    const res = await searchService.getIpLocation();
    searchInput.location.area.city = res.city;
    searchInput.location.label = res.city;
    history.push({
      pathname: '/search/',
      search: qs.stringify({
        q: searchInput.location.label,
        landmark: searchInput.location.area.landmark,
        locality: searchInput.location.area.locality,
        city: searchInput.location.area.city,
        state: searchInput.location.area.state,
        ...routeService.makeCoordinatesQuery(searchInput.location.coordinates),
        ...routeService.makeHotelIdQuery(searchInput.location.hotel_id),
        ...routeService.makeDateRangeQuery(datePicker.range.start, datePicker.range.end),
        ...routeService.makeRoomConfigQuery(occupancy.rooms),
      }),
    });
    filterService.sortByNearMe({ location, history, city: res.city });
  }

  showHotelDetails = () => {
    const { search: { searchInput, datePicker, occupancy }, history } = this.props;
    const city = kebabCase(searchInput.location.area.city);
    const [hotelName, hotelLocality] = searchInput.location.label.split(',');
    const hotelSlug = kebabCase(`${hotelName} ${hotelLocality}`);
    if (city) searchInput.location.area.state = undefined;
    history.push({
      pathname: `/hotels-in-${city}/${hotelSlug}-${searchInput.location.hotel_id}/`,
      search: qs.stringify({
        q: searchInput.location.label,
        landmark: searchInput.location.area.landmark,
        locality: searchInput.location.area.locality,
        city: searchInput.location.area.city,
        state: searchInput.location.area.state,
        ...routeService.makeCoordinatesQuery(searchInput.location.coordinates),
        ...routeService.makeHotelIdQuery(searchInput.location.hotel_id),
        ...routeService.makeDateRangeQuery(datePicker.range.start, datePicker.range.end),
        ...routeService.makeRoomConfigQuery(occupancy.rooms),
      }),
    });
  }

  showNearbyHotels = () => {
    this.handleSearch();
  }

  handleSearch = () => {
    const { search: { searchInput, datePicker, occupancy }, history } = this.props;
    history.push({
      pathname: '/search/',
      search: qs.stringify({
        q: searchInput.location.label,
        landmark: searchInput.location.area.landmark,
        locality: searchInput.location.area.locality,
        city: searchInput.location.area.city,
        state: searchInput.location.area.state,
        ...routeService.makeCoordinatesQuery(searchInput.location.coordinates),
        ...routeService.makeHotelIdQuery(searchInput.location.hotel_id),
        ...routeService.makeDateRangeQuery(datePicker.range.start, datePicker.range.end),
        ...routeService.makeRoomConfigQuery(occupancy.rooms),
      }),
    });
  }

  toggleSearchButton = () => {
    this.setState(() => ({
      isSearchDisabled: false,
    }));
  }

  render() {
    const { wasSearchButtonClicked, isSearchDisabled } = this.state;
    const { className, searchActions, search } = this.props;
    const searchClasses = `container search-widget ${className}`;

    return (
      <div className={searchClasses} >
        <div className="row">
          <SearchInput
            searchInput={search.searchInput}
            storeSearchInputRef={this.storeSearchInputRef}
            getSearchSuggestions={searchActions.getSearchSuggestions}
            clearSearchSuggestions={searchActions.clearSearchSuggestions}
            searchLocationChange={searchActions.searchLocationChange}
            getLocationDetails={searchActions.getLocationDetails}
            wasSearchButtonClicked={wasSearchButtonClicked}
            onBlur={this.onSearchInputBlur}
            toggleSearchButton={this.toggleSearchButton}
          />
          <div className="col-3">
            <DatePicker
              datePicker={search.datePicker}
              setDatePickerVisibility={searchActions.setDatePickerVisibility}
              dateRangeChange={searchActions.dateRangeChange}
              toggleSearchButton={this.toggleSearchButton}
            />
          </div>
          <div className="col-3" onClick={this.toggleSearchButton}>
            <Occupancy
              occupancy={search.occupancy}
              occupancyVisibilty={searchActions.occupancyVisibilty}
              occupancyCustomRoomsVisibilty={searchActions.occupancyCustomRoomsVisibilty}
              occupancyRoomsChange={searchActions.occupancyRoomsChange}
              occupancyAdultsChange={searchActions.occupancyAdultsChange}
              occupancyKidsChange={searchActions.occupancyKidsChange}
              occupancyAddRoom={searchActions.occupancyAddRoom}
              occupancyRemoveRoom={searchActions.occupancyRemoveRoom}
            />
          </div>
          <div className="col search-button">
            <ABButton
              id="t-searchWidgetCTA"
              onClick={this.onSearchClick}
              block
              experiment="findPageCtaDesktop"
              disabled={isSearchDisabled}
              render={(text) => text || 'SEARCH'}
            />
          </div>
        </div>
      </div>
    );
  }
}

SearchWidget.propTypes = {
  className: PropTypes.string,
  history: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  onSearchClick: PropTypes.func,
  isSearchDisabled: PropTypes.bool,
};

SearchWidget.defaultProps = {
  className: '',
  isSearchDisabled: false,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(SearchWidget);
