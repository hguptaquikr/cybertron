import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import cx from 'classnames';
import AuthenticateUser from '../Authentication/AuthenticateUser/AuthenticateUser';
import Modal from '../../components/Modal/Modal';
import Loader from '../../components/Loader/Loader';
import * as walletActionCreators from '../../services/wallet/walletDuck';
import analyticsService from '../../services/analytics/analyticsService';
import './specialDeals.css';

class SpecialDeals extends Component {
  static showSpecialDealsPrompt = true;

  state = {
    isLoginModalOpen: false,
    isMemberRatesModalOpen: false,
  }

  componentDidMount() {
    const { src } = this.props;
    analyticsService.bannerViewed(src, 'MP');
  }

  onClose = () => {
    this.setVisibility(false);
  }

  onCloseIcon = () => {
    SpecialDeals.showSpecialDealsPrompt = false;
    this.forceUpdate();
  }

  onLoginSuccess = () => {
    const { onLoginSuccess, walletActions } = this.props;
    this.setState({ isMemberRatesModalOpen: true });
    setTimeout(() => {
      this.setVisibility(false);
      this.setState({ isMemberRatesModalOpen: false });
      onLoginSuccess();
      walletActions.getWalletBalance();
    }, 2000);
  }

  setVisibility = (visibility) => {
    this.setState({ isLoginModalOpen: visibility });
  }

  render() {
    const { isLoginModalOpen, isMemberRatesModalOpen } = this.state;
    const { className, user } = this.props;
    return SpecialDeals.showSpecialDealsPrompt ? (
      <div className={cx('sd', className)}>
        {
          !user.isAuthenticated ? (
            <div className="row row--middle row--between sd__cont">
              <div className="col col-8 sd__title">
                <span className="subheading-1 sd__title__text">Get discounted Member Rates.</span>
              </div>
              <div className="col col-4 text--right">
                <button
                  className="button sd__unlock"
                  onClick={() => this.setVisibility(true)}
                >
                  Unlock
                </button>
              </div>
            </div>
          ) : (
            <div className="row row--middle row--between sd__cont">
              <div className="col col-8 sd__title">
                <span className="subheading-1 sd__title__text">Special deals applied on Price.</span>
              </div>
              <i className="icon-cross sd__close" onClick={this.onCloseIcon} />
            </div>
          )
        }
        {
          isLoginModalOpen ? (
            <Modal
              isOpen={isLoginModalOpen}
              onClose={this.onClose}
              closeIcon="close"
            >
              <AuthenticateUser
                isModal
                onLoginSuccess={this.onLoginSuccess}
              />
            </Modal>
          ) : null
        }
        {
          isMemberRatesModalOpen ? (
            <Loader img="https://images.treebohotels.com/images/hotrod/group-17.svg" text="Applying member rates…" />
          ) : null
        }
      </div>
    ) : null;
  }
}

SpecialDeals.propTypes = {
  className: PropTypes.string,
  onLoginSuccess: PropTypes.func,
  user: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
  src: PropTypes.string,
};

const mapStateToProps = (state) => ({
  user: state.user,
  wallet: state.wallet,
});

const mapDispatchToProps = (dispatch) => ({
  walletActions: bindActionCreators(walletActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(SpecialDeals);
