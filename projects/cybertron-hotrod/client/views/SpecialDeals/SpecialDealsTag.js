import React from 'react';
import './specialDealsTag.css';

const SpecialDealsTag = () => (
  <div className="row text-2 sd-tag">
    <img
      src="https://images.treebohotels.com/images/hotrod/group-17.svg"
      className="sd-tag__img"
      alt="special"
    />
    <span className="sd-tag__text">Member Rate</span>
  </div>
);

export default SpecialDealsTag;
