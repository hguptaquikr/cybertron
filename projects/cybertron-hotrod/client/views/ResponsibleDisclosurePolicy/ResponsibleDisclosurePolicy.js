import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import Text from 'leaf-ui/cjs/Text/web';
import Space from 'leaf-ui/cjs/Space/web';
import Link from 'leaf-ui/cjs/Link/web';
import List from 'leaf-ui/cjs/List/web';
import Flex from 'leaf-ui/cjs/Flex/web';
import View from 'leaf-ui/cjs/View/web';
import Card from 'leaf-ui/cjs/Card/web';
import * as contentActionCreators from '../../services/content/contentDuck';

class ResponsibleDisclosurePolicy extends Component {
  componentDidMount() {
    const { contentActions } = this.props;
    contentActions.getPolicyContributorsContent();
  }

  render() {
    const { content: { policyContributors } } = this.props;
    return (
      <View className="container">
        <Card backgroundColor="white">
          <Space padding={[6, 4, 0, 6]}>
            <Flex>
              <View>
                <Text
                  component="p"
                  size="xxl"
                  color="greyDark"
                >
                Policy
                </Text>
                <Text component="p">
                We have built Treebo from the ground up with
                security as our top priority. Even so, we believe
                that all technology contains bugs and the public plays a crucial role in
                identifying these bugs. If you believe you have found a security bug
                in our systems, we will gladly work with you to resolve the issue
                and ensure you are recognized for discovering the bug.
                </Text>
                <Text component="p">
                Treebo will engage with security researchers when
                vulnerabilities are reported to us in accordance with
                this Responsible Disclosure Policy. We will validate,
                respond to and fix reported vulnerabilities in accordance
                with our commitment to security and privacy. We would not
                take legal action against or suspend or terminate access to the Services
                for those who discover and report security vulnerabilities.
                Treebo reserves all of its legal rights in the event of any
                noncompliance to the Responsible Disclosure Policy.
                </Text>
              </View>
            </Flex>
          </Space>
          <Space padding={[2, 4, 0, 6]}>
            <Flex>
              <View>
                <Text
                  component="p"
                  size="l"
                  color="greyDark"
                >
                In Scope
                </Text>
                <Link href="https://www.treebo.com">https://www.treebo.com</Link>
              </View>
            </Flex>
          </Space>
          <Space padding={[2, 4, 0, 6]}>
            <Flex>
              <View>
                <Text component="p" size="xl" color="greyDark">Scope Exclusions</Text>
                <Text>
              The following categories of reports are considered out of scope
              for our program and will not be rewarded:
                </Text>
                <List type="unordered" color="greyDark">
                  <List.Item>
                  Spamming other users with automated emails or notifications
                  (e.g. abusing the forgot password form).
                  </List.Item>
                  <List.Item>
                  Findings derived primarily from social engineering (e.g. phishing, vishing).
                  </List.Item>
                  <List.Item>
                  Reports relating to insufficient rate limiting on our APIs.
                  </List.Item>
                  <List.Item>
                  Any services hosted by 3rd party providers and services.
                  </List.Item>
                  <List.Item>
                  Flaws affecting the users of out-of-date web browsers and plugins.
                  </List.Item>
                  <List.Item>
                  Network level Denial of Service (DoS/DDoS) vulnerabilities.
                  </List.Item>
                  <List.Item>
                  Attacks requiring physical access to a user’s device and similar
                  incidents such as office access (e.g. open doors, tailgating).
                  </List.Item>
                  <List.Item>
                  Invalid or missing SPF (Sender Policy Framework) records.
                  </List.Item>
                  <List.Item>
                  Bypass of URL malware detection.
                  </List.Item>
                </List>
              </View>
            </Flex>
          </Space>
          <Space padding={[2, 4, 0, 6]}>
            <Flex>
              <View>
                <Text
                  component="p"
                  size="xl"
                  color="greyDark"
                >
                Things we do not want to receive
                </Text>
                <List type="unordered">
                  <List.Item>
                  Personally identifiable information (PII)
                  </List.Item>
                  <List.Item>
                  Credit card holder data
                  </List.Item>
                </List>
              </View>
            </Flex>
          </Space>
          <Space padding={[2, 4, 0, 6]}>
            <Flex>
              <View>
                <Text
                  component="p"
                  size="xl"
                  color="greyDark"
                >
              Reporting
                </Text>
                <Text component="p">
                The details of any suspected vulnerabilities should be shared with the
                Treebo Security Team by sending an email to engg.security@treebohotels.com.
                Please do not publicly disclose these details without obtaining an express
                written consent from Treebo. In reporting any suspected vulnerabilities,
                please include the following information:
                </Text>
                <List type="unordered">
                  <List.Item>
                  Vulnerability details with sufficient information to allow us to
                  efficiently reproduce your steps.
                  </List.Item>
                  <List.Item>
                  Your valid email address.
                  </List.Item>
                  <List.Item>
                  Your name as it should be displayed on this page, if you would like it to be.
                  </List.Item>
                  <List.Item>
                Your Twitter handle or website as it should be displayed on our contributors’ page.
                  </List.Item>
                </List>
              </View>
            </Flex>
          </Space>
          <Space padding={[2, 4, 0, 6]}>
            <Flex>
              <View>
                <Text
                  component="p"
                  size="xl"
                  color="greyDark"
                >
                Our Commitment
                </Text>
                <Text component="p">
                If you identify a verified security vulnerability in compliance
                with this Responsible Disclosure Policy, Treebo commits to:
                </Text>
                <List type="unordered">
                  <List.Item>
                Promptly acknowledge receipt of your vulnerability report.
                  </List.Item>
                  <List.Item>
                Provide an estimated timetable for resolution of the reported vulnerability.
                  </List.Item>
                  <List.Item>
                Notify you when the reported vulnerability is fixed.
                  </List.Item>
                  <List.Item>
                Publicly acknowledge your responsible disclosure.
                  </List.Item>
                </List>
              </View>
            </Flex>
          </Space>
          <Space padding={[2, 4, 0, 6]}>
            <Flex>
              <View>
                <Text
                  component="p"
                  size="l"
                  color="greyDark"
                >
                Compensation Requests
                </Text>
                <Text component="p">
                Requests for monetary compensation in connection with any identified or
                alleged vulnerability will be deemed non-compliant with
                this Responsible Disclosure Policy.
                </Text>
              </View>
            </Flex>
          </Space>
          <Space padding={[2, 4, 4, 6]}>
            <Flex>
              <View>
                <Text
                  component="p"
                  size="l"
                  color="greyDark"
                >
                  Contributors
                </Text>
                <List type="unordered">
                  {
                    policyContributors.map((contributor) => (
                      <List.Item>
                        {contributor.name}
                        <a href={`https://twitter.com/${contributor.twitterHandler}/`} target="_blank">
                          &nbsp; {!isEmpty(contributor.twitterHandler) ? '-' : '' } {contributor.twitterHandler}
                        </a>
                      </List.Item>
                    ))
                  }
                </List>
              </View>
            </Flex>
          </Space>
        </Card>
      </View>
    );
  }
}

ResponsibleDisclosurePolicy.propTypes = {
  contentActions: PropTypes.object,
  content: PropTypes.object,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(ResponsibleDisclosurePolicy);
