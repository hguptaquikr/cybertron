import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import 'react-dates/initialize';
import cookies from 'js-cookie';
import makeCreateStore from './services/store/makeCreateStore';
import * as abService from './services/ab/abService';
import authService from './services/auth/authService';
import * as uiActionCreators from './services/ui/uiDuck';
import analyticsService from './services/analytics/analyticsService';
import ScrollToTop from './components/ScrollToTop/ScrollToTop';
import * as routeService from './services/route/routeService';
import routes from './services/route/routes';

authService.verifyDomain(window.location.origin);
const history = createBrowserHistory();
const store = makeCreateStore({ history })(window.__INITIAL_STATE__);
abService.init(cookies.getJSON('ab_user'), cookies.getJSON('ab_experiments'));
analyticsService.init(store.getState, history);
store.dispatch(uiActionCreators.setUtmParams(window.location.search));

ReactDOM.hydrate(
  <Provider store={store}>
    <Router history={history}>
      <ScrollToTop>
        {routeService.renderRoutes(routes)}
      </ScrollToTop>
    </Router>
  </Provider>,
  document.getElementById('root'),
);
