import logger from 'cybertron-utils/logger';
import config from '../../../config';
import * as redisKey from '../../../client/services/redis/redisKey';
import * as redisClient from '../../../client/services/redis/redisClient';

export const getCacheDuration = (route, req) => {
  const { cache } = route;
  let duration = 0;
  if (typeof cache === 'number') {
    duration = cache;
  } else if (typeof cache === 'object') {
    for (let i = 0; i < cache.length; i += 1) {
      const c = cache[i];
      if (c.path === '*') {
        duration = c.duration || 0;
      }
      if (c.path === req.path) {
        duration = c.duration || 0;
        break;
      }
    }
  }
  return duration;
};

const cacheMiddleware = async (req, res, next) => {
  try {
    const data = await redisClient.get(
      redisKey.getRenderKey(config.hostUrl + req.path),
    );
    if (data != null) {
      return res.send(data);
    }
  } catch (err) {
    logger.warn(err);
  }
  return next();
};

export default cacheMiddleware;
