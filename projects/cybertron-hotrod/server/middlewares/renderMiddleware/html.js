/* eslint-disable max-len, import/no-unresolved */
import { assets, scripts } from './fragments';
import assetsManifest from '../../build/client/assetsManifest.json';
import * as packageJson from '../../../package.json';

export default (app, scStyles, head, initialState, route, chunks, req) => `
  <!doctype html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="dns-prefetch" href="//static.treebohotels.com">
      <link rel="dns-prefetch" href="//images.treebohotels.com">
      <link rel="preload" as="script" href="${assets.webpackManifest.js}">
      <link rel="preload" as="script" href="${assets.vendor.js}">
      <link rel="preload" as="script" href="${assets.main.js}">
      ${chunks.reduce((preloads, chunk) => `${preloads}<link rel="preload" as="script" href="${assets[chunk].js}">`, '')}
      ${route.name !== 'landing' ? '' : '<link rel="preload" as="image" href="//images.treebohotels.com/images/hotrod/homepage-desktop-banner-11-4-18.png?fm=pjpg&auto=compress">'}
      ${__LOCAL__ ? '' : `<style>${assets.vendor.styles}</style>`}
      ${__LOCAL__ ? '' : `<style>${assets.main.styles}</style>`}
      ${__LOCAL__ ? '' : chunks.reduce((styles, chunk) => `${styles}<style id="${chunk}.css">${assets[chunk].styles}</style>`, '')}
      ${__LOCAL__ ? '' : scStyles}
      ${__LOCAL__ ? '' : '<link rel="manifest" href="/manifest.json">'}
      <link rel="shortcut icon" href="//images.treebohotels.com/images/favicon.ico?v=2">
      ${head.title.toString()}
      ${head.meta.toString()}
      ${head.link.toString()}
      ${head.script.toString()}
    </head>
    <body data-version="${packageJson.version}">
      <div id="root">${app}</div>
      <script>window.__INITIAL_STATE__ = ${JSON.stringify(initialState)}</script>
      <script>window.__ASSETS_MANIFEST__ = ${JSON.stringify(assetsManifest)}</script>
      <script>${scripts.analytics(req.ip)}</script>
      <script src="${assets.webpackManifest.js}"></script>
      <script src="${assets.vendor.js}"></script>
      <script src="${assets.main.js}"></script>
      ${chunks.reduce((scripties, chunk) => `${scripties}<script src="${assets[chunk].js}"></script>`, '')}
      ${__LOCAL__ ? '' : `<script>${scripts.serviceWorker}</script>`}
    </body>
  </html>
`;
