import React from 'react';
import Helmet from 'react-helmet';
import { matchRoutes } from 'react-router-config';
import { renderToString } from 'react-dom/server';
import { Router } from 'react-router-dom';
import createMemoryHistory from 'history/createMemoryHistory';
import Loadable from 'react-loadable';
import { Provider } from 'react-redux';
import { ServerStyleSheet } from 'styled-components';
import logger from 'cybertron-utils/logger';
import { getCacheDuration } from '../cacheMiddleware/cacheMiddleware';
import makeCreateStore from '../../../client/services/store/makeCreateStore';
import routes from '../../../client/services/route/routes';
import * as routeService from '../../../client/services/route/routeService';
import * as redisClient from '../../../client/services/redis/redisClient';
import * as redisKey from '../../../client/services/redis/redisKey';
import execComponentWillServerRender from './execComponentWillServerRender';
import config from '../../../config';
import html from './html';
import * as packageJson from '../../../package.json';

const APP_SSR = process.env.APP_SSR === 'true';

export default async (req, res) => {
  logger.info('[renderMiddleware] req.originalUrl', req.originalUrl);
  const history = createMemoryHistory();
  history.replace(req.originalUrl);
  const branches = matchRoutes(routes, req.originalUrl.split('?')[0]);
  const branch = branches[branches.length - 1];
  const sheet = new ServerStyleSheet();
  const store = makeCreateStore({ history })();
  const chunks = [];

  if (APP_SSR) {
    await execComponentWillServerRender(
      branches,
      { store, route: branch.route, history, req, res },
    );
  }

  const app = APP_SSR ? renderToString(sheet.collectStyles(
    <Loadable.Capture report={(name) => chunks.push(name.replace(/.*\//, ''))}>
      <Provider store={store}>
        <Router history={history}>
          {routeService.renderRoutes(routes)}
        </Router>
      </Provider>
    </Loadable.Capture>,
  )) : '';

  const currentUrl = history.location.pathname + history.location.search;
  logger.info(`[renderMiddleware] currentUrl: ${currentUrl}`);
  logger.info(`[renderMiddleware] req.originalUrl: ${req.originalUrl}`);
  if (currentUrl !== req.originalUrl) {
    logger.info('[renderMiddleware] redirecting');
    return res.redirect(currentUrl);
  }

  const page = html(
    app,
    sheet.getStyleTags(),
    Helmet.renderStatic(),
    store.getState(),
    branch.route,
    chunks,
    req,
  );

  res.send(page);

  // Caching Logic
  const duration = getCacheDuration(branch.route, req);
  if (duration) {
    redisClient.set(
      redisKey.getRenderKey(config.hostUrl + req.path),
      `${page}<!--${packageJson.name}|${packageJson.version}|${config.hostUrl}${req.originalUrl}|${new Date().toISOString()}-->`,
      duration,
    ).catch((err) => {
      logger.warn(err);
    });
  }

  return true;
};
