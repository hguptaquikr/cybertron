import express from 'express';
import proxy from 'http-proxy-middleware';
import config from '../../../config';

const router = express.Router();

router.use('/v1/feedbacks/', proxy({
  target: config.feedbackUrl,
  pathRewrite: { '^/api/web': '/api' },
  changeOrigin: true,
  onProxyReq(proxyReq) {
    proxyReq.setHeader('x-auth-token', '');
  },
}));

router.use(proxy({
  target: config.webUrl,
  pathRewrite: { '^/api/web': '/api' },
  changeOrigin: true,
}));

export default router;
