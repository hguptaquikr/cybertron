import authService from '../../../client/services/auth/authService';

export default async (req, res, next) => {
  authService.unsetAuthData();
  const authTokenCookie = req.cookies.token;
  const authToken = authTokenCookie ? authTokenCookie.split(' ')[1] : null;
  const userProfile = req.cookies.authenticated_user;
  authService.setAuthToken(authToken);
  authService.setUser(userProfile);
  authService.userIp = req.ip;
  next();
};
