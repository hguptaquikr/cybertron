/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"
#import <CodePush/CodePush.h>
#import <React/RCTPushNotificationManager.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <BugsnagReactNative/BugsnagReactNative.h>
#import <dlfcn.h>
#import <Analytics/SEGAnalytics.h>
#import <SEGAppsFlyerIntegrationFactory.h>
#import <CleverTapReact/CleverTapReactManager.h>
#import <AppsFlyerLib/AppsFlyerTracker.h>
#import <Segment-CleverTap/SEGCleverTapIntegrationFactory.h>
#import <UserNotifications/UserNotifications.h>
#import <CleverTapSDK/CleverTap.h>
#import <React/RCTLinkingManager.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  // Razorpay
  [BugsnagReactNative start];
  dlopen("Razorpay.framework/Razorpay", RTLD_LAZY | RTLD_GLOBAL);

  NSURL *jsCodeLocation;

  // Segment and Appsflyer
  [SEGAnalytics debug:YES];
  [CleverTap setDebugLevel:CleverTapLogDebug];
  NSString *segmentAPIKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"SEGMENT_API_KEY"];
  SEGAnalyticsConfiguration *configuration = [SEGAnalyticsConfiguration configurationWithWriteKey:segmentAPIKey];
  configuration.trackApplicationLifecycleEvents = YES; // Enable this to record certain application events automatically!
  configuration.recordScreenViews = YES; // Enable this to record screen views automatically!
  [configuration use:[SEGAppsFlyerIntegrationFactory instance]];
  [SEGAnalytics setupWithConfiguration:configuration];
  // Segment and Clevertap
  [configuration use:[SEGCleverTapIntegrationFactory instance]];
  [SEGAnalytics setupWithConfiguration:configuration];

  // register for push
  if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_9_x_Max) {
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge)
                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                            if (granted) {
                              dispatch_async(dispatch_get_main_queue(), ^(void) {
                                [[UIApplication sharedApplication] registerForRemoteNotifications];
                              });
                            }
                          }];

  }
  else {
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeAlert | UIUserNotificationTypeSound) categories:nil];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
  }

#ifdef DEBUG
    jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index.ios" fallbackResource:nil];
#else
    jsCodeLocation = [CodePush bundleURL];
#endif

  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"starscream"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
   // notify AppsFlyerTracker
  #ifdef DEBUG
    NSLog(@"device token %@", deviceToken);
  #endif
  [[AppsFlyerTracker sharedTracker] registerUninstall:deviceToken];
  [[SEGAnalytics sharedAnalytics] registeredForRemoteNotificationsWithDeviceToken:deviceToken];
  [RCTPushNotificationManager didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

// Required to register for notifications
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
  [RCTPushNotificationManager didRegisterUserNotificationSettings:notificationSettings];
  [application registerForRemoteNotifications];
}

// Required for the notification event. You must call the completion handler after handling the remote notification.
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
  [[SEGAnalytics sharedAnalytics] receivedRemoteNotification:userInfo];
  completionHandler(UIBackgroundFetchResultNoData);
//  [RCTPushNotificationManager didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}
// Required for the registrationError event.
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
  #ifdef DEBUG
    NSLog(@"device token error %@", error);
  #endif
  [RCTPushNotificationManager didFailToRegisterForRemoteNotificationsWithError:error];
}
// Required for the localNotification event.
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
  [RCTPushNotificationManager didReceiveLocalNotification:notification];
}

- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
  [[SEGAnalytics sharedAnalytics] receivedRemoteNotification:userInfo];
}

// As of iOS 8 and above
- (void) application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier
forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler
{
  if (completionHandler) completionHandler();
}

- (void) application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier
forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler
{
  [[SEGAnalytics sharedAnalytics] handleActionWithIdentifier:identifier forRemoteNotification:userInfo];
  if (completionHandler) completionHandler();
}

- (BOOL) application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
  return [RCTLinkingManager application:application openURL:url
                      sourceApplication:sourceApplication annotation:annotation];
}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary *)options {
  #ifdef DEBUG
    NSLog(@"open url %@", [url absoluteString]);
  #endif
  [self handleUrl:url];
  return YES;
}

- (void)handleUrl:(NSURL *)url {

  UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Deep Link" message:[url absoluteString] preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleDestructive handler:nil];
  [alert addAction:cancelAction];
  [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
}

- (void)openURL:(NSURL*)url options:(NSDictionary<NSString *, id> *)options
completionHandler:(void (^ __nullable)(BOOL success))completion {
  if (completion) {
    completion(YES);
  }
}

@end
