//
//  SegmentManager.h
//  starscream
//
//  Created by Abhishek Nair on 22/08/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#ifndef SegmentManager_h
#define SegmentManager_h

#import <React/RCTBridgeModule.h>

@interface SegmentManager : NSObject <RCTBridgeModule>
@end

#endif /* SegmentManager_h */
