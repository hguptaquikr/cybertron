//
//  SegmentManager.m
//  starscream
//
//  Created by Abhishek Nair on 22/08/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Analytics/SEGAnalytics.h>
#import <React/RCTLog.h>
#import "SegmentManager.h"

@implementation SegmentManager
RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(identify:(NSString *) userID  traits:(NSDictionary *) data)
{
  RCTLogInfo(@"Identify: %@ Props: %@", userID, data);
  [[SEGAnalytics sharedAnalytics] identify:userID traits:data];
}

RCT_EXPORT_METHOD(trackEvent:(NSString *) eventName properties:(NSDictionary *) data)
{
  RCTLogInfo(@"Track event: %@ Props: %@", eventName, data);
  [[SEGAnalytics sharedAnalytics] track:eventName properties:data];
}

RCT_EXPORT_METHOD(screenView:(NSString *) screenName  properties:(NSDictionary *) data)
{
  RCTLogInfo(@"ScreenView: %@ Props: %@", screenName, data);
  [[SEGAnalytics sharedAnalytics] screen:screenName properties:data];
}

@end
