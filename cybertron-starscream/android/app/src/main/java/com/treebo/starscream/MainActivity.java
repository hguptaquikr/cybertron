package com.treebo.starscream;

import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.react.CleverTapModule;
import com.facebook.react.ReactActivity;
import com.facebook.react.modules.network.OkHttpClientProvider;

import okhttp3.OkHttpClient;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "starscream";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        CleverTapModule.setInitialUri(getIntent().getData());
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            CleverTapAPI.createNotificationChannel(getApplicationContext(),
                    "ch_promo", "Promotions", "Treebo Promotions",
                    NotificationManager.IMPORTANCE_DEFAULT, true);
            CleverTapAPI.createNotificationChannel(getApplicationContext(),
                    "ch_trb", "Booking Information", "Booking Information",
                    NotificationManager.IMPORTANCE_MAX, true);
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            rebuildOkHtttp();
        }
    }

    private void rebuildOkHtttp() {
        OkHttpClient currentClient = OkHttpClientProvider.getOkHttpClient();
        OkHttpClient replacementClient = OkHttpCertPin.extend(currentClient);
        OkHttpClientProvider.replaceOkHttpClient(replacementClient);
    }
}
