package com.treebo.starscream.nativebridge;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class PhoneCallModule extends ReactContextBaseJavaModule {

    ReactContext reactContext;
    private String number = "9322800100";

    public PhoneCallModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return PhoneCallModule.class.getSimpleName();
    }

    @ReactMethod
    public void immediateCall() {
        if (ActivityCompat.checkSelfPermission(this.reactContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        this.number = Uri.encode(this.number);
        String url = "tel:" + this.number;
        Intent i = new Intent(Intent.ACTION_CALL, Uri.parse(url));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.reactContext.startActivity(i);
    }
}