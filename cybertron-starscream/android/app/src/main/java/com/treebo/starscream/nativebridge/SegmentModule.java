package com.treebo.starscream.nativebridge;

import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;
import com.segment.analytics.Traits;
import com.treebo.starscream.Utils;

/**
 * Created by abhisheknair on 09/10/17.
 */

public class SegmentModule extends ReactContextBaseJavaModule {
    public SegmentModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return SegmentModule.class.getSimpleName();
    }

    @ReactMethod
    public void identify(String userID, ReadableMap data) {
        Traits userTraits = new Traits();
        userTraits.putAll(Utils.recursivelyDeconstructReadableMap(data));
        Analytics.with(getReactApplicationContext()).identify(userID, userTraits, null);
    }

    @ReactMethod
    public void trackEvent(String eventName, ReadableMap data) {
        Properties properties = new Properties();
        properties.putAll(Utils.recursivelyDeconstructReadableMap(data));
        Log.d(SegmentModule.class.getSimpleName(), properties.toString());
        Log.d(SegmentModule.class.getSimpleName(), Utils.recursivelyDeconstructReadableMap(data).toString());
        Analytics.with(getReactApplicationContext()).track(eventName, properties);
    }

    @ReactMethod
    public void screenView(String screenName, ReadableMap data) {
        Properties properties = new Properties();
        properties.putAll(Utils.recursivelyDeconstructReadableMap(data));
        Analytics.with(getReactApplicationContext()).screen(screenName, properties);
    }
}
