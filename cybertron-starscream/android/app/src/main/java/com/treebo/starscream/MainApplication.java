package com.treebo.starscream;

import android.app.Application;

import com.airbnb.android.react.lottie.LottiePackage;
import com.appsflyer.reactnative.RNAppsFlyerPackage;
import com.bugsnag.BugsnagReactNative;
import com.clevertap.android.sdk.ActivityLifecycleCallback;
import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.react.CleverTapPackage;
import com.facebook.react.ReactApplication;
import com.showlocationservicesdialogbox.LocationServicesDialogBoxPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.horcrux.svg.SvgPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.gnet.bottomsheet.RNBottomSheetPackage;
import com.microsoft.codepush.react.CodePush;
import com.oblador.shimmer.RNShimmerPackage;
import com.razorpay.rn.RazorpayPackage;
import com.react.rnspinkit.RNSpinkitPackage;
import com.segment.analytics.Analytics;
import com.treebo.starscream.nativebridge.StarscreamReactPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

        @Override
        protected String getJSBundleFile() {
            return CodePush.getJSBundleFile();
        }

        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.asList(
                    new MainReactPackage(),
                    new LocationServicesDialogBoxPackage(),
                    new LinearGradientPackage(),
                    new SvgPackage(),
                    new RNBottomSheetPackage(),
                    new RNShimmerPackage(),
                    new LottiePackage(),
                    new CodePush(BuildConfig.CODEPUSH_KEY, getApplicationContext(), BuildConfig.DEBUG),
                    new CleverTapPackage(),
                    BugsnagReactNative.getPackage(),
                    new RazorpayPackage(),
                    new RNSpinkitPackage(),
                    new RNAppsFlyerPackage(MainApplication.this),
                    new StarscreamReactPackage()
            );
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        CleverTapAPI.changeCredentials(BuildConfig.CLEVERTAP_ACCOUNT_ID, BuildConfig.CLEVERTAP_ACCOUNT_TOKEN);
        ActivityLifecycleCallback.register(this);
        super.onCreate();
        BugsnagReactNative.startWithApiKey(this, BuildConfig.BUGSNAG_API_KEY);
        SoLoader.init(this, /* native exopackage */ false);
        Analytics analytics = new Analytics.Builder(this, BuildConfig.SEGMENT_API_KEY)
                .logLevel(BuildConfig.DEBUG ? Analytics.LogLevel.VERBOSE : Analytics.LogLevel.NONE)
                .build();
        Analytics.setSingletonInstance(analytics);

        // Safely call Analytics.with(context) from anywhere within your app!
        Analytics.with(this).track("Android Application Started");
    }
}
