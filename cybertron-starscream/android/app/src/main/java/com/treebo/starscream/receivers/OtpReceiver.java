package com.treebo.starscream.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by aa on 07/10/17.
 */

public class OtpReceiver extends BroadcastReceiver {

    private WeakReference<OtpListener> mOtpListenerWeakReference;

    public static final String TAG = OtpReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0, length = pdusObj == null ? 0 : pdusObj.length; i < length; i++) {
                    SmsMessage currentMessage = getIncomingMessage(pdusObj[i], bundle);

                    String sender = currentMessage.getDisplayOriginatingAddress();
                    //You must check here if the sender is your provider and not another one with same text.

                    String message = currentMessage.getMessageBody();
                    Pattern pattern = Pattern.compile("\\d{6}");
                    Matcher matcher = pattern.matcher(message);

                    String otp = null;
                    if (matcher.find()) {
                        otp = matcher.group();
                    }

                    //Pass on otp to our listener.
                    if (!TextUtils.isEmpty(otp)) {
                        OtpListener otpListener = mOtpListenerWeakReference == null ? null : mOtpListenerWeakReference.get();
                        if (otpListener != null) {
                            otpListener.onGetOtp(otp);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception smsReceiver" + e);
        }
    }

    private SmsMessage getIncomingMessage(Object aObject, Bundle bundle) {
        SmsMessage currentSMS;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String format = bundle.getString("format");
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject, format);
        } else {
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject);
        }
        return currentSMS;
    }

    public void setOtpListener(OtpListener otpListener) {
        mOtpListenerWeakReference = new WeakReference<>(otpListener);
    }

    public void removeOtpListener() {
        if (mOtpListenerWeakReference != null) {
            mOtpListenerWeakReference.clear();
        }
    }

    //Interface to send opt to listeners
    public interface OtpListener {
        void onGetOtp(String otp);
    }
}
