package com.treebo.starscream.nativebridge;

import android.content.IntentFilter;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.treebo.starscream.receivers.OtpReceiver;

/**
 * Created by aa on 01/10/17.
 */

public class OtpListenerModule extends ReactContextBaseJavaModule {

    private OtpReceiver mOtpReceiver;
    private boolean mIsOtpReceiverRegistered = false;
    private OtpReceiver.OtpListener mOtpListener;

    public OtpListenerModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return OtpListenerModule.class.getSimpleName();
    }

    @ReactMethod
    public void autoReadOtp(final Promise promise) {
        unRegisterReadOtpReceiver();
        mOtpListener = new OtpReceiver.OtpListener() {
            @Override
            public void onGetOtp(String otp) {
                promise.resolve(otp);
            }
        };
        registerReadOtpReceiver(mOtpListener);
    }

    public void registerReadOtpReceiver(OtpReceiver.OtpListener otpListener) {
        if (!mIsOtpReceiverRegistered) {
            mOtpReceiver = new OtpReceiver();
            mOtpReceiver.setOtpListener(otpListener);
            getReactApplicationContext()
                    .registerReceiver(mOtpReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
            mIsOtpReceiverRegistered = true;
        }
    }

    public void unRegisterReadOtpReceiver() {
        if (mOtpReceiver != null && mIsOtpReceiverRegistered) {
            try {
                getReactApplicationContext().unregisterReceiver(mOtpReceiver);
            } catch (IllegalArgumentException exception) {
                exception.printStackTrace();
            }
            mIsOtpReceiverRegistered = false;
            mOtpReceiver.removeOtpListener();
        }
        mOtpListener = null;
        mOtpReceiver = null;
    }
}
