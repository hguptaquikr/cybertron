/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Card, Flex, Spacer, Divider, theme } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';
import * as searchActionCreators from '../../search/searchDuck';
import searchService from '../../search/searchService';
import Modal from '../../components/Modal/Modal';
import DatePicker from '../../search/SearchWidget/DatePicker';
import RoomConfig from '../../search/SearchWidget/RoomConfig';
import analyticsService from '../../analytics/analyticsService';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import TouchableComponent from '../../components/Touchable/TouchableComponent';

const DateAndGuestDetails = (props) => {
  const {
    searchActions,
    searchActions: { setSearchModal },
    search: {
      searchModal,
      datePicker,
      roomConfig,
      dummyRoomConfig,
    },
    isDisabled,
    eventProps,
  } = props;
  const { checkIn, checkOut } = searchService.formattedDatePicker(datePicker.range);
  const formattedRoomConfig = searchService.formattedRoomConfig(roomConfig.rooms);
  const rooms = formattedRoomConfig.rooms.text;
  const adults = formattedRoomConfig.adults.text;
  const kids = formattedRoomConfig.kids.text;

  return (
    <Spacer margin={[0, 0, 1, 0]}>
      <Card>
        <Spacer padding={[0, 2]}>
          <TouchableComponent
            onPress={isDisabled ? null : () => {
              analyticsService.segmentTrackEvent('Modify Dates Clicked', eventProps);
              setSearchModal('datePicker');
            }}
            disabled={isDisabled}
            testID="booking-date-clickable-container"
            accessibilityLabel="booking-date-clickable-container"
          >
            <Spacer padding={[2, 0]}>
              <Flex flexDirection="row" alignItems="center">
                <Flex flex={0.5}>
                  <Text size="m" color="greyDarker" weight="normal">Dates</Text>
                </Flex>
                <Flex flexDirection="row" flex={1.25} justifyContent="flex-start" alignItems="center">
                  <Text
                    size="s"
                    color={isDisabled ? 'greyDark' : 'blue'}
                    weight="medium"
                    testID="checkin-date-text"
                    accessibilityLabel="checkin-date-text"
                  >
                    {checkIn}
                  </Text>
                  {
                    !isDisabled ? (
                      getIcon('booking_date_icon', theme.color.blue)
                    ) : getIcon('booking_date_icon_inactive', theme.color.greyDark)
                  }
                  <Text
                    size="s"
                    color={isDisabled ? 'greyDark' : 'blue'}
                    weight="medium"
                    testID="checkout-date-text"
                    accessibilityLabel="checkout-date-text"
                  >
                    {checkOut}
                  </Text>
                </Flex>
                {
                  !isDisabled ? (
                    <Flex flex={0.5} justifyContent="flex-end" alignItems="flex-end">
                      {getIcon('arrow-next')}
                    </Flex>
                  ) :
                    <Flex flex={0.5}>
                      <Spacer padding={[1.5, 0]} />
                    </Flex>
                }
              </Flex>
            </Spacer>
          </TouchableComponent>
          <Divider />
          <TouchableComponent
            onPress={isDisabled ? null : () => {
              analyticsService.segmentTrackEvent('Modify Rooms Clicked', eventProps);
              setSearchModal('roomConfig');
            }}
            testID="room-config-modal"
            disabled={isDisabled}
            accessibilityLabel="room-config-modal"
          >
            <Spacer padding={[2, 0]}>
              <Flex flexDirection="row" alignItems="center">
                <Flex flex={0.5}>
                  <Text size="m" color="greyDarker" weight="normal">Guests</Text>
                </Flex>
                <Flex flexDirection="row" flex={1.25} justifyContent="flex-start" alignItems="center">
                  <Text
                    size="s"
                    color={isDisabled ? 'greyDark' : 'blue'}
                    weight="medium"
                    testID="room-config-text"
                    accessibilityLabel="room-config-text"
                  >
                    {rooms}, {adults} & {kids}
                  </Text>
                </Flex>
                {
                  !isDisabled ? (
                    <Flex flex={0.5} justifyContent="flex-end" alignItems="flex-end">
                      {getIcon('arrow-next')}
                    </Flex>
                  ) :
                    <Flex flex={0.5}>
                      <Spacer padding={[1.5, 0]} />
                    </Flex>
                }
              </Flex>
            </Spacer>
          </TouchableComponent>
          {
        !isDisabled ?
          <Modal
            title="Select Room"
            isOpen={searchModal === 'datePicker' || searchModal === 'roomConfig'}
            onClose={() => {
              if (searchModal === 'roomConfig') {
                searchActions.resetRoomConfig();
              }
              searchActions.setSearchModal(false);
            }}
            headerStyles={{
              borderBottomWidth: 0,
              paddingHorizontal: 32,
            }}
            headerBackgroundColor={theme.color.teal}
            iconProps={{ name: 'close', tintColor: theme.color.white }}
          >
            {
              searchModal === 'datePicker' ? (
                <DatePicker
                  datePicker={datePicker}
                  searchActions={searchActions}
                />
              ) : null
            }
            {
              searchModal === 'roomConfig' ? (
                <RoomConfig
                  roomConfig={dummyRoomConfig}
                  searchActions={searchActions}
                />
              ) : null
            }
          </Modal>
          : null
      }
        </Spacer>
      </Card>
    </Spacer>
  );
};

DateAndGuestDetails.propTypes = {
  searchActions: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  isDisabled: PropTypes.bool,
  eventProps: PropTypes.object,
};

DateAndGuestDetails.defaultProps = {
  isDisabled: false,
};

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
});

const mapStateToProps = (state, ownProps) => ({
  hotel: state.hotel.results[ownProps.hotelId] || state.hotel.results[0],
  search: state.search,
});

export default connect(mapStateToProps, mapDispatchToProps)(DateAndGuestDetails);
