import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import { Spacer, Card, Flex } from 'leaf-ui/native';
import { connect } from 'react-redux';
import Text from '../../components/NewLeaf/Text/Text';
import color from '../../theme/color';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import TouchableComponent from '../../components/Touchable/TouchableComponent';
import hotelService from '../hotelService';

class NeedToKnowPolicy extends React.Component {
  state = {
    footerExpanded: false,
  };

  render() {
    const { hotel, triggerScroll } = this.props;
    const { footerExpanded } = this.state;
    const needToKnowPolicies = hotelService.getNeedToKnowPolicies(get(hotel, 'policies', []));

    return (
      !isEmpty(needToKnowPolicies) ? (
        <Spacer padding={[0, 0, 1, 0]}>
          <Card
            testID="need-to-know-section"
            accessibilityLabel="need-to-know-section"
          >
            <Spacer padding={[3, 2, 2, 2]}>
              <Text size="m" weight="bold">Need To Know</Text>
              <Spacer margin={[3, 0, 0, 0]}>
                {
                  needToKnowPolicies.slice(0, 2).reverse().map((policy) => (
                    <Spacer margin={[0, 0, 1, 0]} key={policy.title}>
                      <Flex flexDirection="row">
                        <Text size="s" weight="normal">&#x2022;</Text>
                        <Spacer margin={[0, 0, 0, 0.5]}>
                          <Text
                            size="s"
                            weight="normal"
                            key={policy.description}
                            testID={`${policy.title}-description`}
                            accessibilityLabel={`${policy.title}-description`}
                          >
                            {policy.description}
                          </Text>
                        </Spacer>
                      </Flex>
                    </Spacer>
                  ))
                }
              </Spacer>
              {
              needToKnowPolicies.length > 2 ? (
                <Spacer>
                  {
                    footerExpanded ? (
                      needToKnowPolicies.slice(2).map((policy) => (
                        <Spacer padding={[0, 0, 1, 0]} key={policy.title}>
                          <Flex flexDirection="row" >
                            <Text size="s" weight="normal">&#x2022;</Text>
                            <Spacer margin={[0, 0, 0, 0.5]}>
                              <Text
                                size="s"
                                weight="normal"
                                key={policy.description}
                                testID={`${policy.title}-description`}
                                accessibilityLabel={`${policy.title}-description`}
                              >
                                {policy.description}
                              </Text>
                            </Spacer>
                          </Flex>
                        </Spacer>
                      ))
                    ) : null
                  }
                  <Spacer margin={[1, 0, 2, 0]}>
                    <TouchableComponent
                      onPress={() => {
                        this.setState({ footerExpanded: !this.state.footerExpanded }, () => {
                          if (this.state.footerExpanded && triggerScroll) triggerScroll();
                        });
                      }}
                      testID="view-more-button"
                      accessibilityLabel="view-more-button"
                    >
                      <Flex flexDirection="row" alignItems="center">
                        <Text
                          size="s"
                          weight="medium"
                          color="blue"
                          testID="button-text"
                          accessibilityLabel="button-text"
                        >
                          {footerExpanded ? 'View Less' : 'View More'}
                        </Text>
                        {getIcon(footerExpanded ? 'arrow-up' : 'arrow-down', color.blue)}
                      </Flex>
                    </TouchableComponent>
                  </Spacer>
                </Spacer>
              ) : null
            }
            </Spacer>
          </Card>
        </Spacer>
      ) : null
    );
  }
}

NeedToKnowPolicy.propTypes = {
  hotel: PropTypes.object.isRequired,
  triggerScroll: PropTypes.func,
};

const mapStateToProps = (state, ownProps) => ({
  hotel: state.hotel.results[ownProps.hotelId] || state.hotel.results[0],
});

export default connect(mapStateToProps)(NeedToKnowPolicy);
