/* eslint-disable global-require,no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import styled from 'styled-components/native';
import { StatusBar, StyleSheet, Platform, Dimensions, View, Image } from 'react-native';
import { Divider } from 'leaf-ui/native';
import LinearGradient from 'react-native-linear-gradient';
import ImageCarousel from 'react-native-image-carousel';
import ImageLoad from '../../components/ImageLoad/ImageLoad';
import color from '../../theme/color';
import Text from '../../components/NewLeaf/Text/Text';
import { capitalizeFirstLetter, imageResolutions } from '../../utils/utils';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const HotelImage = styled(ImageLoad)`
  aspectRatio: 1.6;
  width: 100%;
`;

const ParentContainer = styled.View`
  flex: 1;
  justifyContent: center;
`;
const FooterText = styled.View`
  marginBottom: 24;
  alignItems: center;
`;

const HeaderViewStyle = styled.View`
  paddingBottom: 8;
  paddingTop: 8;
  paddingRight: 14;
  paddingLeft: 14;
  borderRadius: 15;
  overflow: hidden;
  borderWidth: 1;
  position: absolute;
  top: ${Platform.OS === 'ios' ? 24 : 16};
  justifyContent: center;
  alignItems: center;
  backgroundColor: ${color.secondary};
  opacity: 0.5;
`;

const CloseButton = styled.TouchableOpacity`
  height: 40;
  width: 40;
  top: 24;
  left: 12;
  position: absolute;
  justifyContent: center;
  alignItems: center;
  zIndex: 10;
`;

const RoomTypeHeader = styled.View`
  alignItems: center;
  marginTop: 40;
`;

const ImageTagContainer = styled.View`
  alignSelf: center;
  backgroundColor: ${color.blackOpacity40};
  paddingHorizontal: 14;
  paddingVertical: 8;
  position: absolute;
  top: 32;
  zIndex: 10;
  borderRadius: 100;

`;

const ImageTagText = styled.Text`
  color: ${color.white};
  fontWeight: 400;
`;

const styles = {
  fullScreen: {
    ...StyleSheet.absoluteFillObject,
  },
};

class ImageGallery extends React.Component {
  state = {
    currentVisibleItemIndex: 0,
  };

  componentDidMount() {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('light-content');
    } else {
      StatusBar.setBackgroundColor(color.black);
    }
  }

  onIndexChange = (index) => {
    this.setState({ currentVisibleItemIndex: index });
  };

  getRoomCategory = (item) => {
    const category = capitalizeFirstLetter(get(item, 'category', ''));
    if (!category) {
      return '';
    } else if (category.includes('Common')) {
      return category;
    }
    return `Room Type: ${capitalizeFirstLetter(category)}`;
  }

  captureImageCarousel = (imageCarousel) => {
    this.imageCarousel = imageCarousel;
  };

  handleHeaderPress = () => this.imageCarousel.close();

  renderHeader = () => {
    const item = this.props.data[this.state.currentVisibleItemIndex];
    return (
      <View style={{ borderWidth: 1 }}>
        <CloseButton onPress={this.handleHeaderPress} >
          {getIcon('close', color.white)}
        </CloseButton>
        <RoomTypeHeader>
          {
            !!item &&
            <Text size="m" color="white" weight="bold">
              {this.getRoomCategory(item)}
            </Text>
          }
        </RoomTypeHeader>
      </View>
    );
  };

  renderFooter = () => (
    <FooterText>
      <Text size="s" weight="normal" color="white">
        {`${this.state.currentVisibleItemIndex + 1}/${this.props.data.length}`}
      </Text>
    </FooterText>);

  renderImage = (index) => {
    const { url } = this.props.data[index];
    const imageUrl = `${url.includes('https') ? url : `https:${url}`}?${imageResolutions.highres}`;
    return (
      <Image
        style={styles.fullScreen}
        resizeMode="contain"
        cache="force-cache"
        source={{ uri: imageUrl }}
      />
    );
  };

  render() {
    const {
      data,
      isVerticalListOnDisplay,
      onCloseClick,
    } = this.props;
    return (
      <ParentContainer>
        <ImageCarousel
          ref={this.captureImageCarousel}
          horizontal={false}
          renderContent={this.renderImage}
          renderHeader={this.renderHeader}
          renderFooter={this.renderFooter}
          onIdxChange={this.onIndexChange}
        >
          {
            data.map((item) => {
              const imageUrl = `${item.url}?${imageResolutions.highres}`;
              return (
                <View key={imageUrl}>
                  {
                    isVerticalListOnDisplay && !!item.header && !!item.category && !!item &&
                    <ImageTagContainer>
                      <ImageTagText>
                        {this.getRoomCategory(item)}
                      </ImageTagText>
                    </ImageTagContainer>
                  }
                  <HotelImage
                    source={{ uri: imageUrl }}
                    resizeMode="cover"
                  >
                    {
                      isVerticalListOnDisplay && !!item.header && !!item.category &&
                      <HeaderViewStyle>
                        <Text
                          size="s"
                          weight="normal"
                          color="white"
                        >
                          {get(item, 'category', '').includes('common') ?
                            'Common Area' :
                            `Room Type: ${capitalizeFirstLetter(get(item, 'category', ''))}`}
                        </Text>
                      </HeaderViewStyle>
                    }
                  </HotelImage>
                  <Divider color="black" />
                </View>
              );
            })}
        </ImageCarousel>
        <LinearGradient
          colors={[
            'rgba(0,0,0,0.7)',
            'rgba(0,0,0,0)',
          ]}
          style={{
            height: 140,
            width: Dimensions.get('window').width,
            position: 'absolute',
            top: 0,
          }}
        />
        {
          isVerticalListOnDisplay &&
          <CloseButton onPress={onCloseClick}>
            {getIcon('close', color.white)}
          </CloseButton>
        }
      </ParentContainer>
    );
  }
}

ImageGallery.propTypes = {
  data: PropTypes.array.isRequired,
  isVerticalListOnDisplay: PropTypes.bool,
  onCloseClick: PropTypes.func,
};

export default ImageGallery;
