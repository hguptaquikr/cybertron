import React from 'react';
import { Spacer, Card, Flex, theme } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const getAmenity = (amenity) => (
  <Flex alignItems="center" key={amenity.name}>
    {getIcon(amenity.iconName, theme.color.white, 16, 16)}
    <Spacer margin={[1, 0, 0, 0]}>
      <Text
        size="s"
        color="white"
        testID="assured-amenity-name-text"
        accessibilityLabel="assured-amenity-name-text"
      >
        {amenity.name === 'AC' ? 'AC*' : amenity.name}
      </Text>
    </Spacer>
  </Flex>
);

class AssuredEssentials extends React.Component {
  assuredAmenities = [
    { name: 'Wi-Fi', iconName: 'free-wifi' },
    { name: 'Breakfast', iconName: 'free-breakfast' },
    { name: 'AC', iconName: 'ac-room' },
    { name: 'TV', iconName: 'tv' },
    { name: 'Toiletries', iconName: 'complimentary-toiletries' },
  ];

  render() {
    const amenities = this.assuredAmenities;

    return (
      <Spacer margin={[1, 1, 1, 1]}>
        <Card color="lagoon">
          <Spacer margin={[1.5, 0, 0, 1.5]}>
            <Text size="l" color="white">
            Guaranteed at all our hotels
            </Text>
          </Spacer>
          <Spacer margin={[2]}>
            <Flex justifyContent="space-between" flexDirection="row">
              {amenities.map(getAmenity)}
            </Flex>
          </Spacer>
          <Spacer margin={[0, 0, 2, 2]}>
            <Text color="lagoonLight" size="s">
              *AC not available in hill stations
            </Text>
          </Spacer>
        </Card>
      </Spacer>
    );
  }
}

export default AssuredEssentials;
