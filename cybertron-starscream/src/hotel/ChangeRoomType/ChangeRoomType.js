/* eslint-disable global-require, no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';
import { css } from 'styled-components/native';
import { connect } from 'react-redux';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import Shimmer from 'react-native-shimmer';
import { Spacer, Button, Flex, Card, theme } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';
import Price from '../../components/NewLeaf/Price/Price';
import Modal from '../../components/Modal/Modal';
import Carousel from '../../components/Carousel/Carousel';
import RatePlanTypeView from '../../price/RatePlanTypeView/RatePlanTypeView';
import { roomSortOrder } from '../../hotel/hotelService';
import priceService from '../../price/priceService';
import { capitalizeFirstLetter, omitKeys, imageResolutions } from '../../utils/utils';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import Content from '../../components/Content/Content';
import analyticsService from '../../analytics/analyticsService';
import searchService from '../../search/searchService';
import CheckBox from '../../components/CheckBox/CheckBox';
import TouchableComponent from '../../components/Touchable/TouchableComponent';

const carouselImageCss = css`
  width: 128;
  aspectRatio: 1.6;
  justifyContent: center;
  alignItems: center;
  marginRight: 2;
`;

class ChangeRoomType extends React.Component {
  state = {
    isChangeRoomModalOpen: false,
    isExpandMoreClicked: this.props.isExpanded,
    selectedRoom: this.props.selectedRoom,
  };

  onSaveButtonPress = () => {
    this.props.onSave(this.state.selectedRoom);
    this._closeChangeRoomModal();
  };

  getAvailableRoomsLength = () => {
    let length = 0;
    const { price } = this.props;
    Object.keys(price).map((key) => {
      const { isAvailable, ratePlans } = price[key];
      if (!isEmpty(ratePlans) && isAvailable) length += 1;
      return 0;
    });
    return length;
  };

  _getModalFooter = () => (
    <Card elevation={1}>
      <Spacer padding={[1]}>
        <Button block onPress={this.onSaveButtonPress}>
          <Text size="m" weight="medium" family="medium" color="white">Save</Text>
        </Button>
      </Spacer>
    </Card>
  );

  _renderHeader = (section) => {
    if (section.display) {
      return (
        <Spacer>
          <Spacer padding={[0, 0, 0, 2]}>
            <Text
              size="l"
              weight="bold"
              color="greyDarker"
              testID="room-type-text"
              accessibilityLabel="room-type-text"
            >
              {section.title}
            </Text>
          </Spacer>
          <Spacer margin={[3, 0, 0, 0]}>
            <Carousel
              imageCss={carouselImageCss}
              style={{ paddingLeft: 16 }}
              data={section.images.map(({ url }) => (
                { url: `${url}?${imageResolutions.lowres}` }
              ))}
              lazy={3}
            />
          </Spacer>
        </Spacer>
      );
    }
    return null;
  };

  _getCheapestRoomType = () => {
    const { price } = this.props;
    const unavailableRoomTypes = Object.keys(price)
      .filter((key) => price[key].isAvailable === false || isEmpty(price[key].ratePlans));
    return priceService.getCheapestRoomType(omitKeys(price, unavailableRoomTypes));
  };

  _getCostliestRoomType = () => {
    const { price } = this.props;
    const unavailableRoomTypes = Object.keys(price)
      .filter((key) => price[key].isAvailable === false);
    return priceService.getCostliestRoomType(omitKeys(price, unavailableRoomTypes));
  };

  _renderFooter = (section, dataArrayLength) => {
    const { isExpandMoreClicked } = this.state;
    if (dataArrayLength > 1) {
      if ((get(section, 'title', '').toLowerCase() === this._getCheapestRoomType()) && !isExpandMoreClicked) {
        return (
          <TouchableComponent
            onPress={this._viewMoreRoomsClicked}
            testID="show-bigger-rooms-button"
            accessibilityLabel="show-bigger-rooms-button"
          >
            <Spacer margin={[2]}>
              <Flex flexDirection="row" alignItems="center">
                <Text size="m" weight="normal" color="blue">
                View bigger rooms
                </Text>
                {getIcon('arrow-down', theme.color.blue)}
              </Flex>
            </Spacer>
          </TouchableComponent>
        );
      } else if (get(section, 'title', '').toLowerCase() === this._getCostliestRoomType()
        && isExpandMoreClicked) {
        return (
          <TouchableComponent
            onPress={this._viewLessRoomsClicked}
            testID="hide-bigger-rooms-button"
            accessibilityLabel="hide-bigger-rooms-button"
          >
            <Spacer margin={[4, 5]}>
              <Flex flexDirection="row" alignItems="center">
                <Text size="s" weight="normal" color="blue">
                Hide bigger rooms
                </Text>
                {getIcon('arrow-up', theme.color.blue)}
              </Flex>
            </Spacer>
          </TouchableComponent>
        );
      }
    }
    return null;
  };

  _changeRoomSelected = (roomType, ratePlan, memberPricing) => () => {
    this.setState(
      {
        selectedRoom: {
          type: roomType,
          price: ratePlan.price,
          plan: ratePlan.ratePlanName,
          memberDiscount: memberPricing,
          isAvailable: true,
        },
      },
    );
  };

  _renderItem = (roomType, ratePlan, memberDiscount) => {
    const { selectedRoom } = this.state;
    const isSelected = ratePlan
      && (get(selectedRoom, 'type', '').toLowerCase() === roomType.toLowerCase())
      && (selectedRoom.price.code === ratePlan.price.code)
      && (selectedRoom.price.totalPrice === ratePlan.price.totalPrice);

    return (
      <Spacer margin={[4, 2, 0, 2]}>
        <TouchableComponent
          onPress={this._changeRoomSelected(roomType, ratePlan, memberDiscount)}
          testID="change-room-type-button"
          accessibilityLabel="change-room-type-button"
        >
          <Flex flexDirection="row">
            <CheckBox
              onPress={this._changeRoomSelected(roomType, ratePlan, memberDiscount)}
              checked={isSelected}
              color={theme.color.green}
            />
            <Flex flex={1} flexWrap="wrap">
              <Spacer margin={[0, 0, 1, 1]}>
                {
                  ratePlan.price &&
                  <Flex flexDirection="row" alignItems="flex-end">
                    <Price
                      price={ratePlan.price.totalPrice}
                      bold
                      round
                      color="greyDarker"
                      size="l"
                      type="regular"
                      testID="room-price-text"
                      accessibilityLabel="room-price-text"
                    />
                    <Spacer padding={[0, 0, 0, 1]}>
                      <RatePlanTypeView isNRP={ratePlan.ratePlanName === 'NRP'} />
                    </Spacer>
                  </Flex>
                }
              </Spacer>
              <Spacer margin={[0, 0, 1, 1]}>
                {
                  ratePlan.ratePlanName === 'NRP' ?
                    <Text size="s" weight="normal" color="greyDark">
                    Tax Inclusive. Prepaid only. This booking is not refundable.
                    </Text> :
                    <Text size="s" weight="normal" color="greyDark">
                    Tax Inclusive. Pay now or later at hotel.{' '}
                    Zero cancellation fee within 24 hours of checkin.
                    </Text>
                  }
              </Spacer>
            </Flex>
          </Flex>
        </TouchableComponent>
      </Spacer>
    );
  };

  _formChangeRoomModalContent = () => {
    const data = this.transformRoomObjectToArray();
    return (
      <Content
        testID="change-rate-plan-modal"
        accessibilityLabel="change-rate-plan-modal"
      >
        {
          data.map((section) => {
            if (section.isAvailable) {
              const sectionData = section.data;
              return (
                section.display &&
                <Card color="white" key={section.title}>
                  <Spacer padding={[2]} />
                  {this._renderHeader(section)}
                  {
                    sectionData.map((ratePlan) => (
                      <Spacer key={ratePlan.ratePlanName}>
                        {this._renderItem(section.title, ratePlan, section.memberDiscount)}
                      </Spacer>
                    ))
                  }
                  {this._renderFooter(section, data.length)}
                </Card>
              );
            }
            return null;
          })
        }
      </Content>
    );
  };

  transformRoomObjectToArray = () => {
    const { isExpandMoreClicked } = this.state;
    const { price, hotel } = this.props;
    const data = Object.keys(price).map((key) => {
      const { ratePlans = {}, isAvailable, memberDiscount } = price[key];

      return {
        sortOrder: roomSortOrder(key),
        title: capitalizeFirstLetter(key),
        data: Object.keys(ratePlans).map((ratePlanKey) => ({
          ratePlanName: ratePlanKey,
          price: ratePlans[ratePlanKey],
        })),
        images: get(hotel, 'images', []).filter((item) => get(item, 'category', '') === key),
        memberDiscount,
        isAvailable,
        display: (key === this._getCheapestRoomType() || isExpandMoreClicked)
              && !isEmpty(ratePlans),
      };
    });

    return data.sort((a, b) => (a.sortOrder - b.sortOrder));
  };

  _openChangeRoomTypeModal = () => {
    this.props.onClick();
    this.setState({ isChangeRoomModalOpen: true });
  };

  _viewMoreRoomsClicked = () => {
    const {
      search: {
        datePicker,
        roomConfig,
      },
    } = this.props;
    const { checkIn, checkOut } = searchService.formattedDatePicker(datePicker.range);
    const formattedRoomConfig = searchService.formattedRoomConfig(roomConfig.rooms);
    const eventprops = {
      checkin: checkIn,
      checkout: checkOut,
      adults: formattedRoomConfig.adults.count,
      kids: formattedRoomConfig.kids.count,
      rooms: formattedRoomConfig.rooms.count,
    };
    analyticsService.segmentTrackEvent('View Bigger Rooms Clicked', eventprops);
    this.setState({ isExpandMoreClicked: true });
  };

  _viewLessRoomsClicked = () => {
    this.setState({ isExpandMoreClicked: false });
  };

  _closeChangeRoomModal = () => {
    analyticsService.segmentScreenViewed('Select Room Type Modal Viewed');
    this.setState({ isChangeRoomModalOpen: false });
  };

  render() {
    const { isRoomPricesLoading, selectedRoom } = this.props;
    const hasBiggerRooms = this._getCostliestRoomType() !== selectedRoom.type;

    return (
      <Card>
        <Spacer padding={[2]}>
          {
          isRoomPricesLoading ?
            (
              <Shimmer
                animationOpacity={0.8}
                duration={500}
              >
                <Text size="s" weight="medium" color="greyDarker">
                    Fetching other room prices...
                </Text>
              </Shimmer>
            ) :
            this.getAvailableRoomsLength() > 1 ?
              (
                <Flex flexDirection="row">
                  <Text
                    size="s"
                    testID="view-other-rooms-text"
                    accessibilityLabel="view-other-rooms-text"
                  >
                    {hasBiggerRooms ? 'Bigger Rooms Available.  ' : 'Other Rooms Available.  '}
                  </Text>
                  <TouchableComponent
                    onPress={this._openChangeRoomTypeModal}
                    testID="change-room-button"
                    accessibilityLabel="change-room-button"
                  >
                    <Text
                      size="s"
                      weight="medium"
                      color="blue"
                      testID="view-other-rooms-text"
                      accessibilityLabel="view-other-rooms-text"
                    >
                      View Other Room Plans
                    </Text>
                  </TouchableComponent>
                </Flex>
              ) : (
                <Text
                  size="s"
                  weight="medium"
                  color="greyDarker"
                  testID="no-rooms-available-text"
                  accessibilityLabel="no-rooms-available-text"
                >
                  No other rooms available
                </Text>
              )
        }
        </Spacer>
        <Modal
          icon="cross"
          header={<Text size="l" color="greyDarker" weight="medium">Select Room</Text>}
          isOpen={this.state.isChangeRoomModalOpen}
          onClose={this._closeChangeRoomModal}
          footer={this._getModalFooter()}
          iosBarStyle="dark-content"
        >
          {this._formChangeRoomModalContent()}
        </Modal>
      </Card>
    );
  }
}

ChangeRoomType.propTypes = {
  onSave: PropTypes.func.isRequired,
  price: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
  search: PropTypes.object.isRequired,
  isExpanded: PropTypes.bool.isRequired,
  selectedRoom: PropTypes.object.isRequired,
  isRoomPricesLoading: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  isRoomPricesLoading: state.price.isRoomPricesLoading,
  search: state.search,
});

export default connect(mapStateToProps)(ChangeRoomType);
