import { handle } from 'redux-pack';
import recycleState from 'redux-recycle';
import startCase from 'lodash/startCase';
import merge from 'lodash/merge';
import get from 'lodash/get';
import { omit } from 'lodash';
import hotelService from './hotelService';
import { safeKeys } from '../utils/utils';
import { initialState as filterInitialState } from '../search/filterDuck';

const ADD_DISTANCE_TO_RESULTS = 'ADD_DISTANCE_TO_RESULTS';
const GET_LANDING_CONTENT = 'GET_LANDING_CONTENT';
const GET_CHECKOUT_DETAILS = 'GET_CHECKOUT_DETAILS';
const GET_CONFIRMATION_DETAILS = 'GET_CONFIRMATION_DETAILS';
const GET_HOTEL_RESULTS = 'GET_HOTEL_RESULTS';
const GET_HOTEL_REVIEWS = 'GET_HOTEL_REVIEWS';
const GET_HOTEL_DETAILS = 'GET_HOTEL_DETAILS';
const GET_NEARBY_HOTEL_RESULTS = 'GET_NEARBY_HOTEL_RESULTS';
const GET_SEARCH_PRICES = 'GET_SEARCH_PRICES';
const RESET_HOTEL_STATE = 'RESET_HOTEL_STATE';

const initialState = {
  results: {
    0: {
      id: 0,
      name: '',
      isActive: true,
      coordinates: {},
      address: {},
      rooms: [],
      description: '',
      amenities: [],
      accessibilities: [],
      trilights: [],
      images: [],
      reviews: {},
      policies: [],
      rank: {},
    },
  },
  city: '',
  isHotelResultsLoading: true,
  isHotelDetailsLoading: true,
};

export default recycleState(
  (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
      case ADD_DISTANCE_TO_RESULTS:
        return {
          ...state,
          results: {
            ...state.results,
            ...payload.results,
          },
        };

      case GET_HOTEL_RESULTS:
        return handle(state, action, {
          start: () => ({
            results: payload.results,
            isHotelResultsLoading: true,
            city: payload.city,
          }),
          success: (s) => ({
            ...s,
            results: {
              ...s.results,
              ...payload.results,
            },
          }),
          finish: (s) => ({ ...s, isHotelResultsLoading: false }),
        });

      case GET_SEARCH_PRICES:
        return handle(state, action, {
          success: (s) => ({
            ...s,
            results: merge({}, s.results, payload.hotels.results),
          }),
          finish: (s) => ({ ...s, isHotelResultsLoading: false }),
        });

      case GET_NEARBY_HOTEL_RESULTS:
        return handle(state, action, {
          success: (s) => ({
            ...s,
            results: {
              ...s.results,
              ...payload.results,
            },
          }),
        });

      case GET_HOTEL_DETAILS:
        return handle(state, action, {
          start: (s) => ({ ...s, isHotelDetailsLoading: true }),
          success: (s) => ({
            ...s,
            results: {
              ...s.results,
              [payload.hotel.id]: merge({}, s.results[payload.hotel.id], payload.hotel),
            },
          }),
          finish: (s) => ({ ...s, isHotelDetailsLoading: false }),
        });

      case GET_CONFIRMATION_DETAILS:
        return handle(state, action, {
          success: (s) => ({
            ...s,
            results: {
              ...s.results,
              [payload.booking.hotelId]: {
                ...s.results[payload.booking.hotelId],
                ...payload.hotel,
              },
            },
          }),
        });

      case GET_CHECKOUT_DETAILS:
        return handle(state, action, {
          success: (s) => ({
            ...s,
            results: {
              ...s.results,
              [payload.checkout.hotelId]: merge(
                {},
                s.results[payload.checkout.hotelId],
                payload.hotel,
              ),
            },
          }),
        });

      case GET_HOTEL_REVIEWS:
        return handle(state, action, {
          success: (s) => ({
            ...s,
            results: {
              ...s.results,
              [payload.hotelId]: {
                ...s.results[payload.hotelId],
                reviews: payload.reviews,
              },
            },
          }),
        });

      case RESET_HOTEL_STATE:
        return payload;

      default:
        return state;
    }
  }, [
    GET_LANDING_CONTENT,
  ],
);

export const getHotelResults = (query, params) => (dispatch, getState, { api }) => {
  const place = get(getState(), 'search.searchInput.place', {});
  const area = get(place, 'area', {});
  const searchResultsQuery = {
    state: startCase(area.state),
    city: startCase(area.city || get(params, 'city', '')),
    locality: startCase(area.locality),
    landmark: startCase(area.landmark),
    category: startCase(area.category),
    distance_cap: !!(area.locality || area.landmark),
  };

  const lat = get(place, 'coordinates.lat') || get(query, 'lat');
  const lng = get(place, 'coordinates.lng') || get(query, 'lng');

  const payload = {
    results: initialState.results,
    sort: lat && lng ? {
      by: 'distance',
      list: {
        rating: 'Rating',
        priceLToH: 'Price - Low to High',
        priceHToL: 'Price - High to Low',
        distance: 'Distance',
      },
      button: {
        priceLToH: 'PRICE(L - H)',
        priceHToL: 'PRICE(H - L)',
        rating: 'RATING',
        distance: 'DISTANCE',
      },
      coordinates: {
        lat,
        lng,
      },
    } : filterInitialState.sort,
    city: searchResultsQuery.city,
  };

  return dispatch({
    type: GET_HOTEL_RESULTS,
    payload,
    promise: api.get('/v3/search/', searchResultsQuery)
      .then(hotelService.transformHotelResultsApi),
  });
};

export const getNearbyHotelResults = (hotelId) => (dispatch, getState, { api }) => {
  const nearbyHotelResultsQuery = {
    hotel_id: hotelId,
    nearby: true,
  };
  return dispatch({
    type: GET_NEARBY_HOTEL_RESULTS,
    promise: api.get('/v3/search/', nearbyHotelResultsQuery)
      .then(hotelService.transformHotelResultsApi)
      .then((payload) => {
        const hotelResults = payload.results;
        return ({
          ...payload,
          results: omit(hotelResults, hotelId),
        });
      }),
  });
};

export const getHotelDetails = (hotelId) => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_HOTEL_DETAILS,
    payload: { hotelId },
    promise: api.get(`/v3/hotels/${hotelId}/details/`)
      .then(hotelService.transformHotelDetailsApi),
  });

export const getHotelReviews = (hotelId) => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_HOTEL_REVIEWS,
    promise: api.get('/v1/reviews/tripadvisor/hotel_review_data/', { hotel_id: hotelId })
      .then(hotelService.transformHotelReviewsApi),
  });

export const resetHotelState = () => ({
  type: RESET_HOTEL_STATE,
  payload: initialState,
});

export const addDistanceToResults = () => (dispatch, getState) => {
  const {
    filter: { sort: { coordinates } },
    hotel,
  } = getState();

  return dispatch({
    type: ADD_DISTANCE_TO_RESULTS,
    payload: {
      results: safeKeys(hotel.results).reduce((obj, hotelId) => ({
        ...obj,
        [hotelId]: {
          ...hotel.results[hotelId],
          distance: hotelService.getDistanceBetween(
            coordinates,
            hotel.results[hotelId].coordinates,
          ),
        },
      }), {}),
    },
  });
};
