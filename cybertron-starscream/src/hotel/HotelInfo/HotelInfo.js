import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Linking } from 'react-native';
import { Spacer, Flex, Tag, Card } from 'leaf-ui/native';
import get from 'lodash/get';
import Text from '../../components/NewLeaf/Text/Text';
import TouchableComponent from '../../components/Touchable/TouchableComponent';
import SVG from '../../components/SVG/SVG';
import analyticsService from '../../analytics/analyticsService';
import ReviewsModal from '../ReviewsModal/ReviewsModal';

class HotelInfo extends React.Component {
  state = {
    isReviewsModalOpen: false,
  };

  constructAddress = () => {
    const { address } = this.props.hotel;
    return address ?
      `${get(address, 'locality', '')}, ${address.city}.` : '';
  };

  _openMaps = (coordinates) => {
    const eventProps = {
      hotel_name: this.props.hotel.name,
      address: this.props.hotel.address,
    };
    analyticsService.segmentTrackEvent('View Location Clicked', eventProps);
    const mapsUrl = `https://maps.google.com/?q=${get(coordinates, 'lat', 0)},${get(coordinates, 'lng', 0)}`;
    Linking.openURL(mapsUrl);
  };

  _openReviewsModal = () => {
    this.setState({ isReviewsModalOpen: true });
  }

  _closeReviewsModal = () => {
    this.setState({ isReviewsModalOpen: false });
  }

  policies = this.props.hotel.policies.map((x) => {
    switch (x.policy_type) {
      case 'is_couple_friendly': {
        return (
          <Spacer margin={[2, 0, -1, 2]} key="is_couple_friendly">
            <Tag color="lagoon" size="small" shape="bluntEdged" kind="outlined">
              COUPLE FRIENDLY
            </Tag>
          </Spacer>
        );
      }
      default: return null;
    }
  });

  render() {
    const { hotel, isRoomAvailable, totalPrice, selectedRoom } = this.props;
    const hotelReviews = get(hotel, 'reviews', {});

    return (
      <Card>
        {this.policies}
        <Spacer padding={[2.5, 2, 0, 2]}>
          <Text size="xl" weight="medium" >
            {hotel.name}
          </Text>
        </Spacer>
        {
          hotelReviews && get(hotelReviews, 'noOfReviews', 0) > 0 &&
            <Spacer margin={[1, 2, 0, 2]}>
              <Flex flexDirection="row" alignItems="center">
                <TouchableComponent onPress={this._openReviewsModal}>
                  <SVG
                    style={{ width: 78, height: 30 }}
                    source={get(hotelReviews, 'overallRating.ratingImage', '')}
                  />
                </TouchableComponent>
                <TouchableComponent onPress={this._openReviewsModal}>
                  <Spacer padding={[0, 0, 0, 1]}>
                    <Text size="s" color="blue" weight="medium">
                      {`${get(hotelReviews, 'noOfReviews', 0)} Reviews`}
                    </Text>
                  </Spacer>
                </TouchableComponent>
              </Flex>
            </Spacer>
        }
        <Spacer padding={[1, 2, 2, 2]}>
          <Flex flexDirection="row" alignItems="center">
            <Text
              size="s"
              weight="normal"
              testID="hotel-address-text"
              accessibilityLabel="hotel-address-text"
            >
              {this.constructAddress(hotel.address)}
            </Text>
            <Spacer padding={[0, 0, 0, 1]}>
              <TouchableComponent
                onPress={() => this._openMaps(hotel.coordinates)}
                testID="open-maps-button"
                accessibilityLabel="open-maps-button"
              >
                <Text size="s" weight="medium" color="blue">
                  View in Maps
                </Text>
              </TouchableComponent>
            </Spacer>
          </Flex>
        </Spacer>
        <ReviewsModal
          hotelId={hotel.id}
          isReviewsModalOpen={this.state.isReviewsModalOpen}
          closeModal={this._closeReviewsModal}
          isRoomAvailable={isRoomAvailable}
          totalPrice={totalPrice}
          selectedRoom={selectedRoom}
        />
      </Card>
    );
  }
}

HotelInfo.propTypes = {
  hotel: PropTypes.object,
  selectedRoom: PropTypes.object.isRequired,
  isRoomAvailable: PropTypes.bool.isRequired,
  totalPrice: PropTypes.number,
};

const mapStateToProps = (state, ownProps) => ({
  hotel: state.hotel.results[ownProps.hotelId] || state.hotel.results[0],
});

export default connect(mapStateToProps)(HotelInfo);
