import React from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  Dimensions,
  Share,
  Platform,
  StatusBar,
  TouchableWithoutFeedback,
  BackHandler,
  PanResponder,
  Keyboard,
  Linking,
  KeyboardAvoidingView,
  View,
} from 'react-native';
import styled from 'styled-components/native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-native';
import { Spacer, Card, Flex, theme } from 'leaf-ui/native';
import Shimmer from 'react-native-shimmer';
import moment from 'moment';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import get from 'lodash/get';
import Header from '../../components/Header/Header';
import * as hotelActionCreators from '../../hotel/hotelDuck';
import * as priceActionCreators from '../../price/priceDuck';
import * as searchActionCreators from '../../search/searchDuck';
import * as toastActionCreators from '../../toast/toastDuck';
import * as checkoutActionCreators from '../../checkout/checkoutDuck';
import * as couponActionCreators from '../../coupon/couponDuck';
import priceService from '../../price/priceService';
import searchService from '../../search/searchService';
import { impressionProperties } from '../../ab/abService';
import { omitKeys } from '../../utils/utils';
import HotelInfo from '../HotelInfo/HotelInfo';
import Text from '../../components/NewLeaf/Text/Text';
import AboutHotelAndPolicies from '../AboutHotelAndPolicies/AboutHotelAndPolicies';
import PriceInformation from '../PriceInformation/PriceInformation';
import ChangeRoomType from '../ChangeRoomType/ChangeRoomType';
import BookButton from '../BookButton/BookButton';
import ImageGallery from '../ImageGallery/ImageGallery';
import config from '../../config';
import DateAndGuestDetails from '../DateAndGuestDetails/DateAndGuestDetails';
import RoomSoldOutInformation from '../RoomSoldOutInformation/RoomSoldOutInformation';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import analyticsService from '../../analytics/analyticsService';
import PrimaryTravellerCard from '../PrimaryTraveller/PrimaryTravellerCard';
import NeedToKnowPolicy from '../NeedToKnowPolicy/NeedToKnowPolicy';
import Coupon from '../../coupon/Coupon/Coupon';
import BrandCampaignAccordion from '../../brandCampaign/BrandCampaignAccordion';
import { checkAndRequestCallPermission } from '../../permissions/androidPermissionService';
import { immediateCall } from '../../nativeModules';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;
const EMPTY_SPACE_HEIGHT = 300;

const contentContainerStyle = {
  width: SCREEN_WIDTH,
  height: Platform.OS === 'android' ? SCREEN_HEIGHT - 24 : SCREEN_HEIGHT,
};

const EmptySpace = styled.View`
  height: ${EMPTY_SPACE_HEIGHT};
  width: 100%;
`;

const BackButtonContainer = styled.TouchableOpacity`
  height: 40;
  width: 40;
  top: 16;
  left: 12;
  alignItems: center;
  justifyContent: center;
  position: absolute;
  zIndex: 1;
`;
const CallusContainer = styled.TouchableOpacity`
  alignItems: center;
  justifyContent: center;
  position: absolute;
  height: 40;
  width: 40;
  top: 16;
  right: 16;
  zIndex: 1;
  `;

export const swipeDirections = {
  SWIPE_UP: 'SWIPE_UP',
  SWIPE_DOWN: 'SWIPE_DOWN',
  SWIPE_LEFT: 'SWIPE_LEFT',
  SWIPE_RIGHT: 'SWIPE_RIGHT',
};

const swipeConfig = {
  velocityThreshold: 0.3,
  directionalOffsetThreshold: 80,
};

class HotelDetailsPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedRoom: {},
      isFooterCTAVisible: true,
      isOtherRoomsClicked: false,
      scrollY: new Animated.Value(0),
      headerY: new Animated.Value(-65),
      scrollViewY: new Animated.Value(SCREEN_HEIGHT),
      isImageGalleryOnDisplay: false,
      showBackButton: true,
      isHeaderShowing: false,
      isScrolling: false,
      checkoutFailed: false,
      bookButtonPosition: 0,
    };

    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('light-content');
    } else {
      StatusBar.setBackgroundColor(theme.color.black);
      StatusBar.setBarStyle('light-content');
    }
    const responderEnd = this._handlePanResponderEnd.bind(this);
    this._panResponder = PanResponder.create({
      onPanResponderRelease: responderEnd,
      onPanResponderTerminate: responderEnd,
    });
    this.currentScrollPosition = 0;
    this.ptLayout = {};
    this.isKeyboardOpen = false;
  }

  async componentDidMount() {
    const {
      params,
      location,
      searchActions,
      hotelActions,
    } = this.props;
    const currentHotelId = Number(params.hotelId.split('-').pop());

    this.pageInit.initTime = moment();
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this.keyboardDidShow());
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => this.keyboardDidHide());

    this.hideImageGallery();
    searchActions.setSearchState({ ...location.query, ...params });

    hotelActions.getHotelReviews(currentHotelId);
    await hotelActions.getHotelDetails(currentHotelId);
    analyticsService.segmentScreenViewed('Hotel Detail Page Viewed', {
      ...this.getCommonEventProps(),
      ...impressionProperties(['singlePageHD']),
    });
    await this.getRoomPrices(currentHotelId);
  }

  componentWillReceiveProps(nextProps) {
    const nextHotelId = Number(nextProps.params.hotelId.split('-').pop());
    const oldRoomConfig = searchService.formattedRoomConfig(this.props.search.roomConfig.rooms);
    const newRoomConfig = searchService.formattedRoomConfig(nextProps.search.roomConfig.rooms);
    const oldDate = searchService.formattedDatePicker(this.props.search.datePicker.range);
    const newDate = searchService.formattedDatePicker(nextProps.search.datePicker.range);

    if (!isEqual(oldRoomConfig, newRoomConfig)) {
      this.getRoomPrices(nextHotelId);
      analyticsService.segmentTrackEvent('Modify Rooms Clicked', this.getCommonEventProps());
    } else if (!isEqual(oldDate, newDate)) {
      this.getRoomPrices(nextHotelId);
      analyticsService.segmentTrackEvent('Modify Dates Clicked', this.getCommonEventProps());
    }
  }

  async shouldComponentUpdate(nextProps) {
    const { hotelActions, params, history, location } = nextProps;
    if (history && location && location.state === 'reload') {
      const hotelId = Number(params.hotelId.split('-').pop());
      hotelActions.getHotelReviews(hotelId);
      await hotelActions.getHotelDetails(hotelId);
      await this.getRoomPrices(hotelId);
      history.replace({ ...location, state: null });
    }
    return true;
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
    this.keyboardDidShowListener.remove();
  }

  onBackPress = () => {
    const { history, searchActions: { resetSearchModal } } = this.props;
    const { isImageGalleryOnDisplay } = this.state;
    if (isImageGalleryOnDisplay) {
      this.hideImageGallery();
    } else {
      resetSearchModal();
      history.goBack();
    }
    return true;
  };

  getCommonEventProps = () => {
    const {
      hotel,
      search: {
        datePicker,
        roomConfig,
      },
      location,
    } = this.props;
    const { checkIn, checkOut } = searchService.formattedDatePicker(datePicker.range);
    const formattedRoomConfig = searchService.formattedRoomConfig(roomConfig.rooms);
    return {
      hotel_id: hotel.id,
      af_content_id: hotel.id,
      hotel_name: hotel.name,
      checkin: checkIn,
      checkout: checkOut,
      adults: formattedRoomConfig.adults.count,
      kids: formattedRoomConfig.kids.count,
      rooms: formattedRoomConfig.rooms.count,
      price: this.state.selectedRoom.price,
      q: location.query,
      address: hotel.address,
      special_deal_tag_show: this.isMemberDiscountAvailable(),
    };
  };

  getCommonEventPropsBookNow = () => {
    const {
      hotel,
      search: { roomConfig, datePicker },
      price,
      coupon,
    } = this.props;
    const { selectedRoom: { plan, type } } = this.state;
    const {
      checkIn,
      checkOut,
      numberOfNights,
    } = searchService.formattedDatePicker(datePicker.range);
    const formattedRoomConfig = searchService.formattedRoomConfig(roomConfig.rooms);
    const ratePlan = get(price[type], `ratePlans.${plan}`, {});

    return {
      hotel_id: hotel.id,
      af_content_id: hotel.id,
      hotel_name: hotel.name,
      price: get(ratePlan, 'sellingPrice'),
      total_price: get(ratePlan, 'totalPrice'),
      is_using_treebo_points: get(ratePlan, 'isUsingTreeboPoints'),
      treebo_points: get(ratePlan, 'totalUsableTreeboPoints'),
      coupon,
      checkin: checkIn,
      checkout: checkOut,
      numberOfNights,
      adults: formattedRoomConfig.adults.count,
      kids: formattedRoomConfig.kids.count,
      rooms: formattedRoomConfig.rooms.count,
    };
  }

  getCommonEventPropsCheckout = (ratePlan) => {
    const {
      location,
      hotel,
      search: { roomConfig, datePicker },
    } = this.props;
    const { checkIn, checkOut } = searchService.formattedDatePicker(datePicker.range);
    const formattedRoomConfig = searchService.formattedRoomConfig(roomConfig.rooms);
    return {
      hotel_id: hotel.id,
      af_content_id: hotel.id,
      hotel_name: hotel.name,
      checkin: checkIn,
      checkout: checkOut,
      adults: formattedRoomConfig.adults.count,
      kids: formattedRoomConfig.kids.count,
      rooms: formattedRoomConfig.rooms.count,
      price: ratePlan,
      q: location.query,
    };
  }

  getPageHeader = () => {
    const { hotel } = this.props;
    return (
      <Flex alignItems="center">
        <Text
          size="m"
          weight="normal"
          numberOfLines={1}
          testID="header-title"
          accessibilityLabel="header-title"
        >
          {hotel.name}
        </Text>
        <Text
          size="xs"
          weight="normal"
          color="greyDark"
          testID="header-subtitle"
          accessibilityLabel="header-subtitle"
        >
          {get(hotel, 'address.city', '')}
        </Text>
      </Flex>
    );
  };

  getHeaderRightIcon = () => (
    getIcon('share')
  );

  getRoomPrices = async (hotelId) => {
    const { priceActions } = this.props;

    await priceActions.getRoomPrices(hotelId);

    if (!this.pageInit.hasInteractivityEventBeenSent) {
      const loadingTime = moment().diff(this.pageInit.initTime, 'milliseconds');
      analyticsService.segmentTrackEvent('HD Page Price Load Time',
        { time: loadingTime })
        .then(this.pageInit.hasInteractivityEventBeenSent = true);
    }

    await this.getSelectedRoom();
    await this.getCheckoutDetails(hotelId);
  };

  getCheapestAvailableRoomType = () => {
    const { price } = this.props;
    let unavailableRoomTypes = [];
    if (Object.keys(price).length > 1) {
      unavailableRoomTypes = Object.keys(price)
        .filter((roomType) =>
          !price[roomType].isAvailable || isEmpty(price[roomType].ratePlans));
    }
    // Else condition ensures the prices on
    // SRP are shown first while the other room prices are loading
    return unavailableRoomTypes.length < 4 ?
      priceService.getCheapestRoomType(omitKeys(price, unavailableRoomTypes)) : undefined;
  };

  getSelectedRoom = () => {
    const { price, searchActions } = this.props;
    const { selectedRoom } = this.state;
    if (!isEmpty(selectedRoom)) {
      const typeOfRoom = selectedRoom.type.toLowerCase();
      const ratePlan = selectedRoom.plan;
      this.setState({
        selectedRoom: {
          ...selectedRoom,
          memberDiscount: get(price[typeOfRoom], 'memberDiscount', {}),
          price: price[typeOfRoom].ratePlans[ratePlan],
          isAvailable: get(price[typeOfRoom], 'isAvailable', false),
        },
      });
      searchActions.changeRoomType(typeOfRoom);
    } else {
      const roomType = this.getCheapestAvailableRoomType();
      const cheapestRatePlan = roomType && price[roomType].ratePlans
        ? priceService.getCheapestRatePlan(price[roomType].ratePlans) : undefined;
      const roomPrice = cheapestRatePlan ? price[roomType].ratePlans[cheapestRatePlan] : undefined;
      if (roomType) {
        searchActions.changeRoomType(roomType.toLowerCase());
        this.setState({
          selectedRoom: {
            type: roomType.toLowerCase(),
            memberDiscount: get(price[roomType], 'memberDiscount', {}),
            plan: cheapestRatePlan,
            price: roomPrice,
            isAvailable: get(price[roomType], 'isAvailable', false),
          },
        });
      }
    }
  };

  getCheckoutDetails = (currentHotelId) => {
    const {
      checkoutActions,
      couponActions,
      toastActions: { showToast },
      booking,
      price,
      checkout,
    } = this.props;

    const { selectedRoom: { type, plan } } = this.state;
    if (!price[type]) {
      return false;
    }

    const isUsingTreeboPoints = get(price[type], `ratePlans.${plan}.isUsingTreeboPoints`, false);

    return checkoutActions.getCheckoutDetails(
      checkout.hotelId === currentHotelId ? booking.bid : null,
      plan,
      isUsingTreeboPoints,
    ).then(({ payload }) => {
      if (payload.errors) {
        showToast('Uh oh, the room is no longer available!');
        this.setState({ checkoutFailed: true });
      } else {
        const newBid = payload.booking.bid;
        this.setState({ checkoutFailed: false });
        couponActions.getCoupons(newBid);
      }
    });
  }

  getDetailsAndPrices = (hotelId) => {
    const { hotelActions } = this.props;
    hotelActions.getHotelDetails(hotelId).then(() => {
      this.getRoomPrices(hotelId);
    });
    hotelActions.getHotelReviews(hotelId);
  };

  setBookButtonPosition = () => {
    const { y } = this.ptLayout;
    const position = y;
    if (position !== this.state.bookButtonPosition) {
      this.setState({ bookButtonPosition: position > 0 ? position : 0 });
    }
  }

  handleCalliOS = () => {
    const url = `tel:${config.customerCareNumber}`;
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        return Linking.openURL(url).catch(() => null);
      }
      return Promise.reject();
    });
  }

  handleCallAndroid = async () => {
    const isPermissionGranted = await checkAndRequestCallPermission();
    if (isPermissionGranted) {
      immediateCall();
    }
  }

  handleCallus = () => {
    analyticsService.segmentTrackEvent('Call Widget Clicked', { source: 'HotelDetailsPage' });
    if (Platform.OS === 'ios') {
      this.handleCalliOS();
    } else {
      this.handleCallAndroid();
    }
  }

  keyboardDidShow() {
    this.isKeyboardOpen = true;
  }

  keyboardDidHide() {
    this.isKeyboardOpen = false;
  }

  _isValidHorizontalSwipe(gestureState) {
    const { vx, dy } = gestureState;
    const { velocityThreshold, directionalOffsetThreshold } = swipeConfig;
    return this.isValidSwipe(vx, velocityThreshold, dy, directionalOffsetThreshold);
  }

  _isValidVerticalSwipe(gestureState) {
    const { vy, dx } = gestureState;
    const { velocityThreshold, directionalOffsetThreshold } = swipeConfig;
    return this.isValidSwipe(vy, velocityThreshold, dx, directionalOffsetThreshold);
  }

  isValidSwipe = (velocity, velocityThreshold, directionalOffset, directionalOffsetThreshold) => (
    Math.abs(velocity) > velocityThreshold
    && Math.abs(directionalOffset) < directionalOffsetThreshold
  );

  isHotelSoldOut = () => {
    const { price } = this.props;
    let unavailableRoomTypes = [];
    if (Object.keys(price).length >= 1) {
      unavailableRoomTypes = Object.keys(price)
        .filter((roomType) =>
          !price[roomType].isAvailable || price[roomType].ratePlans.length === 0);
    }
    return (Object.keys(price).length === unavailableRoomTypes.length);
  };

  _getSwipeDirection(gestureState) {
    const { SWIPE_LEFT, SWIPE_RIGHT, SWIPE_UP, SWIPE_DOWN } = swipeDirections;
    const { dx, dy } = gestureState;
    if (this._isValidHorizontalSwipe(gestureState)) {
      return (dx > 0)
        ? SWIPE_RIGHT
        : SWIPE_LEFT;
    } else if (this._isValidVerticalSwipe(gestureState)) {
      return (dy > 0)
        ? SWIPE_DOWN
        : SWIPE_UP;
    }
    return null;
  }

  _triggerSwipeHandlers(swipeDirection) {
    const { SWIPE_LEFT, SWIPE_RIGHT, SWIPE_UP, SWIPE_DOWN } = swipeDirections;
    switch (swipeDirection) {
      case SWIPE_LEFT:
        break;
      case SWIPE_RIGHT:
        break;
      case SWIPE_UP:
        break;
      case SWIPE_DOWN:
        if (this.currentScrollPosition <= 0) {
          this.showImageGallery();
        }
        break;
      default:
        break;
    }
  }

  _handlePanResponderEnd(evt, gestureState) {
    const swipeDirection = this._getSwipeDirection(gestureState);
    this._triggerSwipeHandlers(swipeDirection);
  }

  triggerScroll = () => {
    if (this.state.isImageGalleryOnDisplay) {
      this.hideImageGallery();
      if (this.content && this.content._component) {
        this.content._component.scrollToEnd();
      }
    } else if (this.content && this.content._component) {
      this.content._component.scrollToEnd();
    }
  }

  triggerScrollToBca = () => {
    if (this.content && this.content._component && this.currentScrollPosition < this.bcaPosition) {
      this.content._component.scrollTo({ x: 0, y: this.bcaPosition });
    }
  }

  triggerScrollToNeedToKnow = () => {
    if (this.content && this.content._component && this.currentScrollPosition < this.ntkPosition) {
      this.content._component.scrollTo({ x: 0, y: this.ntkPosition });
    }
  }

  pageInit = {
    initTime: 0,
    hasInteractivityEventBeenSent: false,
  };

  changeRoomType = (selectedRoomObject) => {
    const {
      history,
      location,
      hotel,
      searchActions,
    } = this.props;
    const oldRoom = this.state.selectedRoom;
    const newRoom = selectedRoomObject;

    this.setState({
      selectedRoom: {
        ...newRoom,
        type: newRoom.type.toLowerCase(),
      },
      isOtherRoomsClicked: (oldRoom.type !== newRoom.type),
    }, () => {
      searchActions.changeRoomType(newRoom.type.toLowerCase());
      history.replace({
        pathname: location.pathname,
        query: { ...location.query, roomtype: newRoom.type },
      });
      if ((oldRoom.type !== newRoom.type) || (oldRoom.plan !== newRoom.plan)) {
        this.getCheckoutDetails(hotel.id);
        analyticsService.segmentTrackEvent('Rate Plan Changed', {
          newRoomType: newRoom.type,
          oldRoomType: oldRoom.type,
          roomTypeChanged: oldRoom.type !== newRoom.type,
          ratePlanChanged: oldRoom.plan !== newRoom.plan,
          oldRoom,
          newRoom,
          ...this.getCommonEventProps(),
        });
      }
    });
  };

  viewOtherPlansClicked = () => {
    analyticsService.segmentTrackEvent('View Other Plans Clicked', this.getCommonEventProps());
  };

  isMemberDiscountAvailable = () => {
    const { price } = this.props;
    return Object.keys(price)
      .some((roomType) => get(price[roomType], 'memberDiscount.isAvailable'));
  };

  isScrolling = () => {
    this.setBookButtonPosition();
    if (!this.state.isScrolling) {
      this.setState({ isScrolling: true });
    }
  }

  isNotScrolling = () => {
    if (this.state.isScrolling) {
      this.setState({ isScrolling: false });
    }
  }

  handleBookButtonPress = (e) => {
    const { bookButtonPosition } = this.state;
    analyticsService.segmentTrackEvent('Book Now Clicked');
    if (this.currentScrollPosition <= bookButtonPosition) {
      this.triggerScroll();
    } else {
      this.submitBookButton(e);
    }
  }

  handleScrollBeginDrag = () => {
    this.isScrolling();
  }

  handleScrollEndDrag = () => {
    this.isNotScrolling();
    if (this.isKeyboardOpen && this.currentScrollPosition < this.ntkPosition) {
      Keyboard.dismiss();
    }
  }

  handleMomentumScrollBegin = () => {
    this.isScrolling();
  }

  handleMomentumScrollEnd = () => {
    this.isNotScrolling();
    if (this.isKeyboardOpen && this.currentScrollPosition < this.ntkPosition) {
      Keyboard.dismiss();
    }
  }

  handleFooterCTA(position) {
    const { bookButtonPosition } = this.state;
    if (position > bookButtonPosition && this.state.isFooterCTAVisible) {
      this.setState({ isFooterCTAVisible: false });
    } else if (position < bookButtonPosition && !this.state.isFooterCTAVisible) {
      this.setState({ isFooterCTAVisible: true });
    }
  }

  handleHeader(position) {
    if (position > EMPTY_SPACE_HEIGHT && !this.state.isHeaderShowing) {
      this.setState({ isHeaderShowing: true });
      Animated.timing(this.state.headerY, {
        toValue: 0,
        duration: 200,
        useNativeDriver: true,
      }).start();
    } else if (position < EMPTY_SPACE_HEIGHT && this.state.isHeaderShowing) {
      this.setState({ isHeaderShowing: false });
      Animated.timing(this.state.headerY, {
        toValue: -65,
        duration: 200,
        useNativeDriver: true,
      }).start();
    }
  }

  handleBackButtonState(position) {
    if (position > EMPTY_SPACE_HEIGHT - 60 && this.state.showBackButton) {
      this.setState({ showBackButton: false });
    }
    if (position < EMPTY_SPACE_HEIGHT - 60 && !this.state.showBackButton) {
      this.setState({ showBackButton: true });
    }
  }

  shareHotelDetails = () => {
    const {
      hotel,
      location: { pathname },
    } = this.props;
    const { selectedRoom } = this.state;
    analyticsService.segmentTrackEvent('Page Shared', this.getCommonEventProps());
    const selectedRoomPrice = get(selectedRoom.price, 'sellingPrice', 0);
    const url = `${config.baseUrl}${pathname}`;
    const content = {
      message: `Hi! Checkout ${hotel.name}, Total Price - ${selectedRoomPrice}.
      Click on the link ${url} or call ${config.customerCareNumber} for more details!`,
      url,
      title: `Checkout ${hotel.name}`,
      dialogTitle: `Checkout ${hotel.name}`,
    };
    Share.share(content);
  };

  showImageGallery = () => {
    if (this.isKeyboardOpen) return;
    this.setState({ isImageGalleryOnDisplay: true });
    Animated.timing(this.state.scrollViewY, {
      toValue: SCREEN_HEIGHT,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };

  hideImageGallery = () => {
    this.setState({ isImageGalleryOnDisplay: false });
    Animated.timing(this.state.scrollViewY, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };

  render() {
    const {
      hotel,
      history,
      checkout,
      isRoomPricesLoading,
      isHotelDetailsLoading,
      price,
      coupon: { isCouponApplyLoading },
    } = this.props;

    const {
      headerY,
      scrollViewY,
      isImageGalleryOnDisplay,
      isOtherRoomsClicked,
      showBackButton,
      selectedRoom,
      selectedRoom: { type, plan },
      isHeaderShowing,
      isScrolling,
    } = this.state;

    const { color } = theme;

    const totalPrice = get(price[type], `ratePlans.${plan}.totalPrice`, 0);
    const sellingPrice = get(price[type], `ratePlans.${plan}.sellingPrice`, 0);
    const isRoomAvailable = get(selectedRoom, 'isAvailable', false) && !this.isHotelSoldOut();

    const headerContainerStyle = {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      zIndex: 1,
      transform: [{ translateY: headerY }],
    };

    const scrollViewStyle = {
      backgroundColor: color.transparent,
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      transform: [{ translateY: scrollViewY }],
    };

    const getImageData = () => get(hotel, 'images', []).map((element, index, array) => {
      const newImage = element;
      if (index === 0 || newImage.category !== array[index - 1].category) {
        newImage.header = true;
      }
      return newImage;
    });

    const opacity = this.state.scrollY.interpolate({
      inputRange: [0, (2 * EMPTY_SPACE_HEIGHT)],
      outputRange: [1, 0],
    });

    const handleOnScroll = Animated.event(
      [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
      {
        listener: ({ nativeEvent: { contentOffset: { y } } }) => {
          this.currentScrollPosition = y;
          this.isScrolling();
          this.handleBackButtonState(y);
          this.handleHeader(y);
        },
        useNativeDriver: true,
      },
    );

    return (
      <KeyboardAvoidingView
        behavior="position"
        keyboardVerticalOffset={Platform.OS === 'android' ? 24 : 0}
        contentContainerStyle={contentContainerStyle}
      >
        <Animated.View
          style={{
            opacity,
            flex: 1,
          }}
        >
          <ImageGallery
            data={getImageData(hotel)}
            isVerticalListOnDisplay={isImageGalleryOnDisplay}
            onCloseClick={this.hideImageGallery}
            onBackPress={this.onBackPress}
          />
        </Animated.View>
        {
          !isImageGalleryOnDisplay && showBackButton &&
            <BackButtonContainer
              opacity={0.5}
              onPress={() => {
                this.props.couponActions.removeCoupon(true);
                history.goBack();
              }}
            >
              {getIcon('back', color.white)}
            </BackButtonContainer>
        }
        {
          !isImageGalleryOnDisplay && showBackButton &&
            <CallusContainer
              hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
              onPress={this.handleCallus}
            >
              {getIcon('call', color.white)}
            </CallusContainer>
        }
        <Animated.View style={headerContainerStyle} >
          <Header
            middle={this.getPageHeader()}
            iconProps={{ name: 'back', tintColor: color.greyDarker }}
            right={this.getHeaderRightIcon()}
            onLeftPress={() => history.goBack()}
            onRightPress={this.shareHotelDetails}
            backgroundColor={color.white}
            androidStatusBarColor={isHeaderShowing ? color.white : color.black}
            style={{ borderBottomColor: color.greyLighter, borderBottomWidth: 1 }}
            iosBarStyle={isHeaderShowing ? 'dark-content' : 'light-content'}
            noShadow
          />
        </Animated.View>
        <Animated.ScrollView
          ref={(c) => { this.content = c; }}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled"
          style={scrollViewStyle}
          scrollEventThrottle={16}
          onScroll={handleOnScroll}
          onScrollBeginDrag={this.handleScrollBeginDrag}
          onScrollEndDrag={this.handleScrollEndDrag}
          onMomentumScrollBegin={this.handleMomentumScrollBegin}
          onMomentumScrollEnd={this.handleMomentumScrollEnd}
          onContentSizeChange={this.setBookButtonPosition}
          removeClippedSubviews={false}
          {...this._panResponder.panHandlers}
        >
          <TouchableWithoutFeedback onPress={this.showImageGallery}>
            <EmptySpace />
          </TouchableWithoutFeedback>
          <Card color="greyLighter" >
            <Flex flexGrow={1}>
              <HotelInfo
                hotelId={hotel.id}
                isRoomAvailable={isRoomAvailable}
                totalPrice={totalPrice}
                selectedRoom={selectedRoom}
              />
              {
                !isRoomAvailable && !isRoomPricesLoading && !isHotelDetailsLoading &&
                <RoomSoldOutInformation hotelId={hotel.id} />
              }
              <BrandCampaignAccordion
                history={history}
                triggerScroll={this.triggerScrollToBca}
                callingPage="Hotel Detail"
              />
              <Spacer onLayout={(e) => { this.bcaPosition = e.nativeEvent.layout.y; }} />
              <DateAndGuestDetails eventProps={this.getCommonEventProps()} />
              {
                (isRoomPricesLoading || isCouponApplyLoading) &&
                <Spacer margin={[0, 0, 1, 0]}>
                  <Card>
                    <Spacer padding={[2]}>
                      <Shimmer
                        animationOpacity={0.8}
                        duration={500}
                      >
                        <Text size="l" weight="medium" color="greyDarker">
                          Loading Price...
                        </Text>
                      </Shimmer>
                    </Spacer>
                  </Card>
                </Spacer>
              }
              {
                isRoomAvailable && !isRoomPricesLoading && !isCouponApplyLoading &&
                <PriceInformation
                  hotelId={hotel.id}
                  onChange={this.changeRoomType}
                  selectedRoom={selectedRoom}
                  isRoomAvailable={isRoomAvailable}
                  totalPrice={totalPrice}
                />
              }
              {
                isRoomAvailable &&
                <Spacer margin={[1, 0, 0, 0]}>
                  <ChangeRoomType
                    selectedRoom={selectedRoom}
                    price={price}
                    hotel={hotel}
                    onSave={this.changeRoomType}
                    onClick={this.viewOtherPlansClicked}
                    isExpanded={isOtherRoomsClicked}
                  />
                </Spacer>
              }
              {
                isRoomAvailable &&
                <Coupon
                  hotelId={hotel.id}
                  room={selectedRoom}
                  isDisabled={checkout.isLoading}
                />
              }
              <NeedToKnowPolicy
                hotelId={hotel.id}
                triggerScroll={this.triggerScrollToNeedToKnow}
              />
              <Spacer onLayout={(e) => { this.ntkPosition = e.nativeEvent.layout.y; }} />
              {
                isRoomAvailable &&
                <View
                  onLayout={(e) => { this.ptLayout = e.nativeEvent.layout; }}
                >
                  <PrimaryTravellerCard
                    hotelId={hotel.id}
                    sellingPrice={sellingPrice}
                    totalPrice={totalPrice}
                    selectedRoom={selectedRoom}
                    setPress={(x) => { this.submitBookButton = x; }}
                  />
                </View>

              }
              <AboutHotelAndPolicies
                hotelId={hotel.id}
                totalPrice={totalPrice}
                selectedRoom={selectedRoom}
                isRoomAvailable={isRoomAvailable}
              />
              <View height={8} />
              <Card height={70} />
            </Flex>
          </Card>
        </Animated.ScrollView>
        <BookButton
          isScrolling={isScrolling}
          price={totalPrice}
          isLoading={isRoomPricesLoading || checkout.isLoading || isHotelDetailsLoading}
          onBookButtonPress={this.handleBookButtonPress}
          isAvailable={isRoomAvailable && !this.state.checkoutFailed}
        />
      </KeyboardAvoidingView>
    );
  }
}

HotelDetailsPage.propTypes = {
  hotel: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
  coupon: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  booking: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  checkout: PropTypes.object.isRequired,
  hotelActions: PropTypes.object.isRequired,
  priceActions: PropTypes.object.isRequired,
  toastActions: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  couponActions: PropTypes.object.isRequired,
  checkoutActions: PropTypes.object.isRequired,
  isRoomPricesLoading: PropTypes.bool.isRequired,
  isHotelDetailsLoading: PropTypes.bool,
};

const mapDispatchToProps = (dispatch) => ({
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  priceActions: bindActionCreators(priceActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
  checkoutActions: bindActionCreators(checkoutActionCreators, dispatch),
  couponActions: bindActionCreators(couponActionCreators, dispatch),
});

const mapStateToProps = (state, props) => {
  const { match } = props;
  const hotelId = Number(match.params.hotelId.split('-').pop());
  return ({
    hotel: state.hotel.results[hotelId] || state.hotel.results[0],
    price: state.price.results[hotelId] || state.price.results[0],
    isRoomPricesLoading: state.price.isRoomPricesLoading,
    isHotelDetailsLoading: state.hotel.isHotelDetailsLoading,
    search: state.search,
    params: match.params,
    checkout: state.checkout,
    booking: state.booking,
    coupon: state.coupon,
  });
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(HotelDetailsPage));
