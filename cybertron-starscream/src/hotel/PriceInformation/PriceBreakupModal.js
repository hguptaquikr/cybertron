import { ScrollView } from 'react-native';
import { Spacer, Flex, Card, Divider } from 'leaf-ui/native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import React from 'react';
import PropTypes from 'prop-types';
import Price from '../../components/NewLeaf/Price/Price';
import Text from '../../components/NewLeaf/Text/Text';
import CheckBox from '../../components/NewLeaf/CheckBox/CheckBox';
import Modal from '../../components/Modal/Modal';
import BookButton from '../BookButton/BookButton';
import RatePlanTypeView from '../../price/RatePlanTypeView/RatePlanTypeView';
import * as walletActionCreators from '../../wallet/walletDuck';
import PrimaryTravellerCard from '../PrimaryTraveller/PrimaryTravellerCard';

class PriceBreakupModal extends React.Component {
  state = {
    isUsingTreeboPoints: false,
    canUseTreeboPoints: false,
    isShowingPrimaryTraveller: false,
    isScrolling: false,
  };

  async getInitialState() {
    const { wallet, price, selectedRoom } = this.props;
    const selectedRoomPrice = price[selectedRoom.type.toLowerCase()];
    const { ratePlans } = selectedRoomPrice;
    const roomPrice = ratePlans[selectedRoom.plan];

    this.setState({
      isUsingTreeboPoints: roomPrice.isUsingTreeboPoints,
      canUseTreeboPoints: wallet.wallets.TP.spendableAmount > 0,
    });
  }

  componentDidMount() {
    this.getInitialState();
  }

  isScrolling = () => {
    if (!this.state.isScrolling) {
      this.setState({ isScrolling: true });
    }
  }

  isNotScrolling = () => {
    if (this.state.isScrolling) {
      this.setState({ isScrolling: false });
    }
  }

  toggleTreeboPoints = () => {
    const {
      booking: { bid, hotelId },
      walletActions,
    } = this.props;

    if (this.state.isUsingTreeboPoints) {
      walletActions.removeWallet(bid, hotelId);
      this.setState({ isUsingTreeboPoints: false });
    } else {
      walletActions.applyWallet(bid, hotelId);
      this.setState({ isUsingTreeboPoints: true });
    }
  }

  openPrimaryTraveller = () => {
    this.setState({ isShowingPrimaryTraveller: true });
  }

  closePrimaryTraveller = () => {
    this.setState({ isShowingPrimaryTraveller: false });
  }

  render() {
    const {
      isShowingPrimaryTraveller,
      canUseTreeboPoints,
      isScrolling,
      isUsingTreeboPoints,
    } = this.state;

    const {
      isModalOpen,
      closeModal,
      nightsCount,
      isRoomAvailable,
      selectedRoom,
      price,
      booking,
    } = this.props;

    const selectedRoomPrice = price[selectedRoom.type.toLowerCase()];
    const { ratePlans } = selectedRoomPrice;
    const roomPrice = ratePlans[selectedRoom.plan];

    const {
      basePrice,
      roomDiscount,
      couponApplied,
      couponCode,
      couponDiscount,
      tax,
      totalPrice,
      treeboPointsUsed,
      totalUsableTreeboPoints,
      code,
      discountPercentage,
      sellingPrice,
    } = roomPrice;

    const roomDiscountPercentage = couponApplied ?
      ((roomDiscount / basePrice) * 100).toFixed(2) :
      discountPercentage;

    return (
      <Modal
        isOpen={isModalOpen}
        onClose={isShowingPrimaryTraveller ? this.closePrimaryTraveller : closeModal}
        iosBarStyle="dark-content"
      >
        {!isShowingPrimaryTraveller ?
          <ScrollView
            onScrollBeginDrag={this.isScrolling}
            onScrollEndDrag={this.isNotScrolling}
            onMomentumScrollBegin={this.isScrolling}
            onMomentumScrollEnd={this.isNotScrolling}
            bounces={false}
            alwaysBounceVertical={false}
            showsVerticalScrollIndicator={false}
          >
            <Spacer margin={[5, 2, 0, 2]}>
              <Text weight="bold" size="l">Payment Breakup</Text>
            </Spacer>
            {
              canUseTreeboPoints &&
              <Spacer margin={[3, 2, 1, 2]}>
                <Card color="greyLighter">
                  <Spacer padding={[2]}>
                    <CheckBox
                      onPress={this.toggleTreeboPoints}
                      checked={isUsingTreeboPoints}
                      label={`Pay ₹${totalUsableTreeboPoints} using Treebo Rewards`}
                    />
                  </Spacer>
                </Card>
              </Spacer>
            }
            <Spacer margin={[4, 2, 0, 2]}>
              <Flex flexDirection="row" justifyContent="space-between">
                <Text>Room Tariff</Text>
                <Price
                  price={basePrice}
                  size="m"
                  roundUpTo={2}
                  weight="medium"
                />
              </Flex>
            </Spacer>
            {
              roomDiscountPercentage !== 0 && roomDiscount !== 0 && roomDiscount &&
              <Spacer margin={[3, 2, 0, 2]}>
                <Flex flexDirection="row" justifyContent="space-between">
                  <Text>
                    Room Discount {`${roomDiscountPercentage}%`}
                  </Text>
                  <Price
                    price={roomDiscount}
                    size="m"
                    negative
                    color="green"
                    weight="medium"
                    roundUpTo={2}
                  />
                </Flex>
              </Spacer>
            }
            {
              couponApplied &&
              <Spacer margin={[3, 2, 0, 2]}>
                <Flex flexDirection="row" justifyContent="space-between">
                  <Text>{`"${couponCode}" Applied`}</Text>
                  <Price
                    price={couponDiscount}
                    size="m"
                    negative
                    color="green"
                    weight="medium"
                    roundUpTo={2}
                  />
                </Flex>
              </Spacer>
            }
            <Spacer margin={[3, 2, 0, 2]}>
              <Flex flexDirection="row" justifyContent="space-between">
                <Text>Taxes</Text>
                <Price
                  price={tax}
                  size="m"
                  weight="medium"
                  roundUpTo={2}
                />
              </Flex>
            </Spacer>
            <Spacer margin={[3, 2, 0, 2]}>
              <Divider />
            </Spacer>
            <Spacer margin={[2, 2, 0, 2]}>
              <Flex flexDirection="row" justifyContent="space-between">
                <Text>Total Tariff</Text>
                <Price
                  price={sellingPrice}
                  size="m"
                  weight="medium"
                  roundUpTo={2}
                />
              </Flex>
            </Spacer>
            <Spacer margin={[2, 2, 0, 2]}>
              <Divider />
            </Spacer>
            {
              treeboPointsUsed !== 0 &&
              <Spacer>
                <Spacer margin={[2, 2, 0, 2]}>
                  <Flex flexDirection="row" justifyContent="space-between">
                    <Text>Treebo Points Applied</Text>
                    <Price
                      price={treeboPointsUsed}
                      size="m"
                      color="green"
                      negative
                      weight="medium"
                      roundUpTo={2}
                    />
                  </Flex>
                </Spacer>
                <Spacer margin={[2, 2, 0, 2]}>
                  <Divider />
                </Spacer>
              </Spacer>
            }
            <Spacer margin={[2.5, 2, 0, 2]}>
              <Flex flexDirection="row" justifyContent="space-between" alignItems="center">
                <Text>{`Total Payable for ${nightsCount} night${nightsCount > 1 ? 's' : ''}`}</Text>
                <Price
                  price={totalPrice}
                  size="xl"
                  weight="medium"
                  roundUpTo={2}
                />
              </Flex>
            </Spacer>
            <Spacer margin={[1, 2, 0, 2]}>
              <Flex flexDirection="row" justifyContent="space-between" alignItems="center">
                <Text color="grey">Tax Inclusive</Text>
                <RatePlanTypeView isNRP={code === 'NRP'} />
              </Flex>
            </Spacer>
          </ScrollView>
          :
          <Spacer padding={[2, 0, 0, 0]}>
            <PrimaryTravellerCard
              hotelId={booking.hotelId}
              sellingPrice={sellingPrice}
              totalPrice={totalPrice}
              selectedRoom={selectedRoom}
              isModal
              closeModals={() => {
                this.closePrimaryTraveller();
                closeModal();
              }}
            />
          </Spacer>
        }
        {
          isRoomAvailable && !isShowingPrimaryTraveller &&
          <BookButton
            isScrolling={isScrolling}
            price={totalPrice}
            onBookButtonPress={this.openPrimaryTraveller}
            isAvailable
          />
        }
      </Modal>
    );
  }
}

PriceBreakupModal.propTypes = {
  nightsCount: PropTypes.number.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  isRoomAvailable: PropTypes.bool.isRequired,
  wallet: PropTypes.object.isRequired,
  booking: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  selectedRoom: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  walletActions: bindActionCreators(walletActionCreators, dispatch),
});

const mapStateToProps = (state, ownProps) => ({
  price: state.price.results[ownProps.hotelId] || state.price.results[0],
  wallet: state.wallet,
  booking: state.booking,
});

export default connect(mapStateToProps, mapDispatchToProps)(PriceBreakupModal);
