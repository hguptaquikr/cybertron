import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { connect } from 'react-redux';
import { Spacer, Flex, Card, Divider } from 'leaf-ui/native';
import Shimmer from 'react-native-shimmer';
import Price from '../../components/NewLeaf/Price/Price';
import Text from '../../components/NewLeaf/Text/Text';
import color from '../../theme/color';
import CheckBox from '../../components/NewLeaf/CheckBox/CheckBox';
import TouchableComponent from '../../components/Touchable/TouchableComponent';
import { pluralize } from '../../utils/utils';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import RatePlanTypeView from '../../price/RatePlanTypeView/RatePlanTypeView';
import priceService from '../../price/priceService';
import searchService from '../../search/searchService';
import PriceBreakupModal from './PriceBreakupModal';

class PriceInformation extends React.Component {
  state = {
    isPriceBreakupModalOpen: false,
    isRefundable: false,
  };

  componentWillReceiveProps(nextProps) {
    const {
      selectedRoom: { plan },
    } = nextProps;

    if (plan === 'EP') {
      this.setState({ isRefundable: true });
    } else if (plan === 'NRP') {
      this.setState({ isRefundable: false });
    }
  }

  openPriceBreakupModal = () => {
    this.setState({ isPriceBreakupModalOpen: true });
  }

  closePriceBreakupModal = () => {
    this.setState({ isPriceBreakupModalOpen: false });
  }

  _toggleRefundable = () => {
    const {
      selectedRoom,
      price,
    } = this.props;
    const currentRoomType = selectedRoom.type.toLowerCase();
    const { ratePlans } = price[currentRoomType];

    this.setState({ isRefundable: !this.state.isRefundable }, () => {
      if (this.state.isRefundable) {
        this.props.onChange({
          ...selectedRoom,
          plan: ratePlans.EP.code,
          price: ratePlans.EP,
        });
      } else {
        this.props.onChange({
          ...selectedRoom,
          plan: ratePlans.NRP.code,
          price: ratePlans.NRP,
        });
      }
    });
  }

  render() {
    const {
      selectedRoom,
      price,
      search,
      totalPrice,
      isRoomAvailable,
      hotelId,
      checkout,
    } = this.props;

    const { datePicker: { range } } = search;
    const { numberOfNights } = searchService.formattedDatePicker(range);
    const nightsCount = numberOfNights.count || 0;

    const selectedRoomPrice = price[selectedRoom.type.toLowerCase()];
    const { memberDiscount, ratePlans } = selectedRoomPrice;
    const roomPrice = ratePlans[selectedRoom.plan];
    const isTreeboPointsApplied = get(roomPrice, 'isUsingTreeboPoints', false);
    const treeboPointsDeducted = get(roomPrice, 'treeboPointsUsed', 0);

    let isEPAvailable = false;
    let diffPrice = 0;
    if ('EP' in ratePlans && 'NRP' in ratePlans) {
      isEPAvailable = true;
      diffPrice = priceService.getRatePlanDiff(ratePlans);
    }

    return (
      <Spacer>
        <Card>
          <Spacer margin={[2, 2, 1.5, 2]}>
            <Flex flexDirection="row">
              <Text size="s" color="greyDarker">
              Total payable for {pluralize(nightsCount, 'night', 's', true)}.
              </Text>
              <Spacer margin={[0, 0.5]}>
                <TouchableComponent
                  onPress={this.openPriceBreakupModal}
                  disabled={checkout.isLoading}
                >
                  {checkout.isLoading ?
                    <Shimmer animationOpacity={0.8} duration={500} >
                      <Text size="s" weight="medium" color="greyDark">
                        View Breakup
                      </Text>
                    </Shimmer>
                    :
                    <Text size="s" weight="medium" color="blue" >
                        View Breakup
                    </Text>
                  }
                </TouchableComponent>
              </Spacer>
            </Flex>
          </Spacer>
          <Spacer margin={[0, 2]}>
            <Flex flexDirection="row" >
              <Price
                price={roomPrice.totalPrice}
                weight="medium"
                round
                color="greyDarker"
                size="xl"
                type="regular"
              />
              <Spacer margin={[1.5, 0, 0, 1]}>
                <RatePlanTypeView isNRP={selectedRoom.plan === 'NRP'} />
              </Spacer>
            </Flex>
          </Spacer>
          <Spacer margin={[1, 2, 2, 2]}>
            <Text size="s" color="grey">
            Price inclusive of all taxes
            </Text>
          </Spacer>
          {
            isEPAvailable &&
            <Spacer>
              <Spacer margin={[0, 2, 2, 2]}>
                <Divider />
              </Spacer>
              <Spacer margin={[0, 2, 2, 2]}>
                <Flex flexDirection="row" >
                  <CheckBox
                    onPress={this._toggleRefundable}
                    checked={this.state.isRefundable}
                    label={`Refundable plan for an additional ₹${diffPrice}`}
                  />
                </Flex>
              </Spacer>
            </Spacer>
          }
          <Flex flexDirection="row">
            {
            get(memberDiscount, 'isApplied', false) &&
              <Spacer
                padding={[0, 1, 2, 2]}
                testID="special-deal-view"
                accessibilityLabel="special-deal-view"
              >
                <Flex flexDirection="row" alignItems="center">
                  {getIcon('special_deal', color.warmGrey)}
                  <Spacer padding={[0, 0, 0, 0.5]}>
                    <Text size="xs" color="greyDark">
                      Member Rate
                    </Text>
                  </Spacer>
                </Flex>
              </Spacer>
            }
            {
            isTreeboPointsApplied && treeboPointsDeducted > 0 &&
              <Spacer margin={[0, 2, 2, 1]}>
                <Flex flexDirection="row" alignItems="center">
                  {getIcon('menu-rewards', color.warmGrey)}
                  <Spacer padding={[0, 0, 0, 0.5]}>
                    <Flex flexDirection="row">
                      <Price
                        price={treeboPointsDeducted}
                        size="xs"
                        color="greyDark"
                        testID="wallet-amount-text"
                        accessibilityLabel="wallet-amount-text"
                      />
                      <Text
                        size="xs"
                        color="greyDark"
                      >
                        {' '}Treebo Points applied.
                      </Text>
                    </Flex>
                  </Spacer>
                </Flex>
              </Spacer>
            }
          </Flex>
        </Card>
        <PriceBreakupModal
          isModalOpen={this.state.isPriceBreakupModalOpen}
          closeModal={this.closePriceBreakupModal}
          hotelId={hotelId}
          nightsCount={nightsCount}
          totalPrice={totalPrice}
          isRoomAvailable={isRoomAvailable}
          selectedRoom={selectedRoom}
        />
      </Spacer>
    );
  }
}

PriceInformation.propTypes = {
  hotelId: PropTypes.number.isRequired,
  selectedRoom: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  isRoomAvailable: PropTypes.bool.isRequired,
  totalPrice: PropTypes.number.isRequired,
  search: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  checkout: PropTypes.object.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  price: state.price.results[ownProps.hotelId],
  search: state.search,
  checkout: state.checkout,
});

export default connect(mapStateToProps)(PriceInformation);
