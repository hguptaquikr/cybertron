import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { Spacer } from 'leaf-ui/native';
import SearchResultsItem from '../../search/SearchResultsItem/SearchResultsItem';
import Text from '../../components/NewLeaf/Text/Text';
import color from '../../theme/color';
import List from '../../components/List/List';
import searchService from '../../search/searchService';

const NearbyHotelsContainer = styled.View``;

const isHotelAvailable = (hotelList) => (
  !isEmpty(hotelList) && Object.values(hotelList).some((value) => value)
);

const NearbyHotels = ({ nearby, search, history }) => {
  const hotels = get(nearby, 'hotels', {});
  const prices = get(nearby, 'prices', {});
  const availability = get(nearby, 'availability', {});
  const { datePicker: { range } } = search;
  const { numberOfNights } = searchService.formattedDatePicker(range);
  const nightsCount = numberOfNights.count || 0;
  const dataStore = Object.keys(hotels).map((hotelID) => ({
    key: hotelID,
    hotel: hotels[hotelID],
    price: prices[hotelID],
  })).filter((hotel) => availability[hotel.key]);
  if (isHotelAvailable(availability)) {
    return (
      <NearbyHotelsContainer>
        <Spacer padding={[3, 0, 3, 2]} style={{ backgroundColor: color.white }}>
          <Text size="l" weight="bold">Treebos Nearby</Text>
        </Spacer>
        <List
          keyExtractor={(item) => item.key}
          data={dataStore.length > 4 ? dataStore.slice(0, 4) : dataStore}
          renderItem={({ item }) => (
            <SearchResultsItem
              key={item.key}
              hotel={item.hotel}
              price={item.price}
              search={search}
              history={history}
              numberOfNights={nightsCount}
              available={availability[item.key]}
            />
          )}
        />
      </NearbyHotelsContainer>
    );
  }
  return null;
};

NearbyHotels.propTypes = {
  nearby: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default NearbyHotels;
