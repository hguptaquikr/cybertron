import React from 'react';
import PropTypes from 'prop-types';
import { Spacer, Card, Flex } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';
import HighDemandIcon from './HighDemandIcon';
import PeopleAreViewingIcon from './PeopleAreViewingIcon';
import PriceDropIcon from './PriceDropIcon';

const getIcon = (trigger) => {
  switch (trigger) {
    case 'most-viewed': return <PeopleAreViewingIcon />;
    case 'price-drop': return <PriceDropIcon />;
    default: return <HighDemandIcon />;
  }
};

const getText = (trigger) => {
  switch (trigger) {
    case 'most-viewed': return 'Most viewed Treebo today';
    case 'price-drop': return 'Price Dropped';
    default: return 'High Demand';
  }
};

const PsychTriggerCard = ({ trigger }) => (
  <Spacer margin={[1, 0, 0, 0]}>
    <Card>
      <Spacer padding={[2, 2, 2, 2]}>
        <Flex flexDirection="row" alignItems="center">
          {getIcon(trigger)}
          <Spacer padding={[1, 1, 1, 1]}>
            <Text size="s">{getText(trigger)}</Text>
          </Spacer>
        </Flex>
      </Spacer>
    </Card>
  </Spacer>
);

PsychTriggerCard.propTypes = {
  trigger: PropTypes.string,
};

export default PsychTriggerCard;
