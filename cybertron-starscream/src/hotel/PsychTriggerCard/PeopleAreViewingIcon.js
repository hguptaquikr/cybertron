import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const PeopleAreViewing = ({ size }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="people-are-viewing" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Combined-Shape"
        d="M17,10.5 C19.8,10.5 22.1,12.4 22.1,14.7 L22.1,18.5 L16.1,18.5 L16.1,14.7 C16.1,13.1 15.3,11.6 14,10.5 L17,10.5 Z M14.1,14.7 L14.1,18.5 L2.1,18.5 L2.1,14.7 C2.1,12.4 4,10.5 6.4,10.5 L9.8,10.5 C10.4,10.5 11,10.6 11.6,10.9 C13.1,11.6 14.1,13 14.1,14.7 Z M8.1,8.5 C6.5,8.5 5.1,7.2 5.1,5.5 C5.1,3.8 6.4,2.5 8.1,2.5 C9.8,2.5 11.1,3.8 11.1,5.5 C11.1,7.2 9.8,8.5 8.1,8.5 Z M16.1,8.5 C14.5,8.5 13.1,7.2 13.1,5.5 C13.1,3.8 14.4,2.5 16.1,2.5 C17.8,2.5 19.1,3.8 19.1,5.5 C19.1,7.2 17.8,8.5 16.1,8.5 Z M21.1,20.5 C21.7,20.5 22.1,20.9 22.1,21.5 C22.1,22.1 21.7,22.5 21.1,22.5 L3.1,22.5 C2.5,22.5 2.1,22.1 2.1,21.5 C2.1,20.9 2.5,20.5 3.1,20.5 L21.1,20.5 Z"
        fill="#FAA31A"
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

PeopleAreViewing.propTypes = {
  size: PropTypes.number,
};

PeopleAreViewing.defaultProps = {
  size: 24,
};

export default PeopleAreViewing;
