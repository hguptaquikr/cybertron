import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Polygon,
  Defs,
} from 'react-native-svg';

const HighDemand = ({ size }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="high-demand" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Polygon
        id="Shape"
        fill="#FAA31A"
        fillRule="nonzero"
        points="19 9 12.7 9 17 2 10 2 5 12 10 12 6 22"
      />
    </G>
  </Svg>
);

HighDemand.propTypes = {
  size: PropTypes.number,
};

HighDemand.defaultProps = {
  size: 24,
};

export default HighDemand;
