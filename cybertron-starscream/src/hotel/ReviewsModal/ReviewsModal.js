import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { ScrollView, Linking, View } from 'react-native';
import { Spacer, Flex, theme, Button } from 'leaf-ui/native';
import get from 'lodash/get';
import ReadMore from '../ReadMore/ReadMore';
import Modal from '../../components/Modal/Modal';
import Text from '../../components/NewLeaf/Text/Text';
import List from '../../components/List/List';
import { capitalizeFirstLetter } from '../../utils/utils';
import SVG from '../../components/SVG/SVG';
import BookButton from '../BookButton/BookButton';
import PrimaryTravellerCard from '../PrimaryTraveller/PrimaryTravellerCard';

const RatingContainer = styled.View`
  background-color: ${theme.color.greyLight};
  height: 4px;
  border-radius: 4;
  flex-direction: row;
`;

const RatingFill = styled.View`
  background-color: ${theme.color.green};
  height: 4px;
  border-radius: 4;
`;

const RatingBar = ({ percentage }) => (
  <RatingContainer>
    <RatingFill flex={percentage} />
  </RatingContainer>
);

RatingBar.propTypes = {
  percentage: PropTypes.number,
};

class ReviewsModal extends Component {
  state = {
    isShowingPrimaryTraveller: false,
    isScrolling: false,
  };

  isScrolling = () => {
    this.setState({ isScrolling: true });
  }

  isNotScrolling = () => {
    this.setState({ isScrolling: false });
  }

  openPrimaryTraveller = () => {
    this.setState({ isShowingPrimaryTraveller: true });
  }

  closePrimaryTraveller = () => {
    this.setState({ isShowingPrimaryTraveller: false });
  }

  _renderTruncatedFooter = (handlePress) => (
    <Text color="blue" onPress={handlePress}>
    Read more
    </Text>
  );

  _renderRevealedFooter = (handlePress) => (
    <Text color="blue" onPress={handlePress}>
    Show less
    </Text>
  );

  writeReview = () => {
    const { reviews: { writeReviewsUrl } } = this.props.hotel;
    Linking.openURL(writeReviewsUrl);
  };

  render() {
    const {
      isShowingPrimaryTraveller,
      isScrolling,
    } = this.state;

    const {
      isReviewsModalOpen,
      closeModal,
      hotel: { reviews },
      totalPrice,
      isRoomAvailable,
      selectedRoom,
      price,
      hotelId,
    } = this.props;

    const overallRating = get(reviews, 'overallRating', {});
    const subRatings = get(overallRating, 'subRatings', []);
    const reviewsToShow = get(reviews, 'userReviews', []).slice(0, 4);

    let selectedRoomPrice;
    let plans;
    let roomPrice;

    const SubRatings = subRatings ? subRatings.map(({ level, percent, noOfReviews }) => (
      <Spacer padding={[0, 0, 2, 0]} key={level}>
        <Flex display="flex" flexDirection="row" alignItems="center">
          <Flex flex={0.3}>
            <Text size="s" color="greyDarker">{level}</Text>
          </Flex>
          <Flex flex={0.4}>
            <RatingBar percentage={percent / 100} />
          </Flex>
          <Flex flex={0.3}>
            <Spacer padding={[0, 0, 0, 4]}>
              <Text size="s" color="greyDark">{noOfReviews}</Text>
            </Spacer>
          </Flex>
        </Flex>
      </Spacer>
    )) : null;

    if (isRoomAvailable) {
      selectedRoomPrice = isRoomAvailable ? price[selectedRoom.type.toLowerCase()] : null;
      plans = selectedRoomPrice.ratePlans;
      roomPrice = plans[selectedRoom.plan];
    }

    return (
      <Modal
        isOpen={isReviewsModalOpen}
        onClose={isShowingPrimaryTraveller ? this.closePrimaryTraveller : closeModal}
        iosBarStyle="dark-content"
      >
        {
      !isShowingPrimaryTraveller ?
        <ScrollView
          onScrollBeginDrag={this.isScrolling}
          onScrollEndDrag={this.isNotScrolling}
          onMomentumScrollBegin={this.isScrolling}
          onMomentumScrollEnd={this.isNotScrolling}
        >
          <Spacer margin={[0, 2]}>
            <Spacer margin={[3, 0]}>
              <Text weight="bold" size="l">Overall Ratings</Text>
            </Spacer>
            {SubRatings}
            {reviewsToShow.length > 0 &&
              <View>
                <Spacer margin={[4, 0, 0, 0]}>
                  <Text weight="bold" size="l">User Reviews</Text>
                </Spacer>
                <Flex flexDirection="row" style={{ height: 24 }} alignItems="center">
                  <Text size="s" color="greyDarker">In partnership with</Text>
                  <Spacer padding={[0, 0, 0, 1]} />
                  <SVG
                    style={{ width: 90, height: 16, bottom: 2 }}
                    source="https://developer-tripadvisor.s3.amazonaws.com/uploads/tripadvisor-horz-rgb_newcolor_(1)_(1).svg"
                  />
                </Flex>
                <List
                  keyExtractor={(item, i) => i}
                  scrollEnabled={false}
                  data={reviewsToShow}
                  renderItem={({ item }) => (
                    <Spacer padding={[3, 0, 0, 0]}>
                      <Text
                        size="l"
                        weight="medium"
                        color="greyDarker"
                        testID="review-title-text"
                        accessibilityLabel="review-title-text"
                      >
                        {capitalizeFirstLetter(item.title)}
                      </Text>
                      <Flex flexDirection="row" alignItems="center">
                        <SVG
                          style={{ width: 70, height: 24, bottom: 2 }}
                          source={get(item, 'ratingImage', '')}
                        />
                        <Spacer padding={[0, 1]}>
                          <Text size="s" weight="normal" color="grey">
                            {`${get(item, 'user.name', 'User')}, ${get(item, 'publishDate', 'Date Unknown')}`}
                          </Text>
                        </Spacer>
                      </Flex>
                      <Spacer padding={[1, 0]}>
                        <ReadMore
                          numberOfLines={3}
                          renderTruncatedFooter={this._renderTruncatedFooter}
                          renderRevealedFooter={this._renderRevealedFooter}
                        >
                          <Text>
                            {get(item, 'description', '')}
                          </Text>
                        </ReadMore>
                      </Spacer>
                    </Spacer>
                  )}
                />
                <Spacer padding={[3, 0, 0, 0]} />
              </View>
            }
          </Spacer>
          <Spacer padding={[1]}>
            <Button block kind="outlined" onPress={this.writeReview}>
              <Text weight="medium" color="green">
                WRITE A REVIEW
              </Text>
            </Button>
          </Spacer>
        </ScrollView>
      :
        <Spacer padding={[2, 0, 0, 0]}>
          <PrimaryTravellerCard
            hotelId={hotelId}
            sellingPrice={get(roomPrice, 'sellingPrice', 0)}
            totalPrice={totalPrice}
            selectedRoom={selectedRoom}
            isModal
            closeModals={() => {
              this.closePrimaryTraveller();
              closeModal();
            }}
          />
        </Spacer>
      }
        {
        isRoomAvailable && !isShowingPrimaryTraveller &&
        <BookButton
          isScrolling={isScrolling}
          price={totalPrice}
          onBookButtonPress={this.openPrimaryTraveller}
          isAvailable
        />
      }
      </Modal>
    );
  }
}

ReviewsModal.propTypes = {
  hotelId: PropTypes.number.isRequired,
  isReviewsModalOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  hotel: PropTypes.object.isRequired,
  selectedRoom: PropTypes.object.isRequired,
  isRoomAvailable: PropTypes.bool.isRequired,
  totalPrice: PropTypes.number,
  price: PropTypes.object,
};

const mapStateToProps = (state, ownProps) => ({
  hotel: state.hotel.results[ownProps.hotelId] || state.hotel.results[0],
  price: state.price.results[ownProps.hotelId] || state.price.results[0],
});

export default connect(mapStateToProps)(ReviewsModal);
