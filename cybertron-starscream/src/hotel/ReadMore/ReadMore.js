/* eslint-disable react/no-did-mount-set-state */
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  button: {
    color: '#888',
    marginTop: 5,
  },
});

const measureHeightAsync = (component) => (
  new Promise((resolve) => {
    component.measure((x, y, w, h) => {
      resolve(h);
    });
  })
);

const nextFrameAsync = () => (
  new Promise((resolve) => requestAnimationFrame(() => resolve()))
);

class ReadMore extends React.Component {
  state = {
    measured: false,
    shouldShowReadMore: false,
    showAllText: false,
  };

  async componentDidMount() {
    await nextFrameAsync();

    // Get the height of the text with no restriction on number of lines
    const fullHeight = await measureHeightAsync(this._text);
    this.setState({ measured: true });
    await nextFrameAsync();

    // Get the height of the text now that number of lines has been set
    const limitedHeight = await measureHeightAsync(this._text);

    if (fullHeight > limitedHeight) {
      this.setState({ shouldShowReadMore: true });
    }
  }

  _handlePressReadMore = () => {
    this.setState({ showAllText: true });
  };

  _handlePressReadLess = () => {
    this.setState({ showAllText: false });
  };

  _maybeRenderReadMore() {
    const {
      shouldShowReadMore,
      showAllText,
    } = this.state;

    if (shouldShowReadMore && !showAllText) {
      if (this.props.renderTruncatedFooter) {
        return this.props.renderTruncatedFooter(this._handlePressReadMore);
      }

      return (
        <Text style={styles.button} onPress={this._handlePressReadMore}>
          More
        </Text>
      );
    } else if (shouldShowReadMore && showAllText) {
      if (this.props.renderRevealedFooter) {
        return this.props.renderRevealedFooter(this._handlePressReadLess);
      }

      return (
        <Text style={styles.button} onPress={this._handlePressReadMore}>
          Less
        </Text>
      );
    }
    return (
      <View />
    );
  }

  render() {
    const {
      measured,
      showAllText,
    } = this.state;

    const {
      numberOfLines,
      style,
    } = this.props;

    return (
      <View style={style}>
        <Text
          numberOfLines={measured && !showAllText ? numberOfLines : 0}
          ref={(text) => {
            this._text = text;
          }}
        >
          {this.props.children}
        </Text>

        {this._maybeRenderReadMore()}
      </View>
    );
  }
}

ReadMore.propTypes = {
  numberOfLines: PropTypes.number,
  children: PropTypes.object,
  renderRevealedFooter: PropTypes.func,
  renderTruncatedFooter: PropTypes.func,
  style: PropTypes.object,
};

export default ReadMore;
