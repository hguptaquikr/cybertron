import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-native';
import { ScrollView, Dimensions, View } from 'react-native';
import { Spacer, Card, Form } from 'leaf-ui/native';
import get from 'lodash/get';
import styled from 'styled-components';
import moment from 'moment';
import * as checkoutActionCreators from '../../checkout/checkoutDuck';
import * as searchActionCreators from '../../search/searchDuck';
import Text from '../../components/NewLeaf/Text/Text';
import TextInput from '../../components/NewLeaf/TextInput/TextInput';
import SlideInView from '../../components/SlideInView/SlideInView';
import CheckBox from '../../components/NewLeaf/CheckBox/CheckBox';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import {
  mobileRegex,
  emailRegex,
  gstnRegex,
} from '../../services/inputValidationService';
import BookButton from '../BookButton/BookButton';
import analyticsService from '../../analytics/analyticsService';
import apiService from '../../api/apiService';
import urlService from '../../services/urlService';
import searchService from '../../search/searchService';

const ClearButton = styled.TouchableOpacity`
  width: 48;
  height: 48;
  alignItems: center;
  justifyContent: center;
  zIndex: 2;
`;

class PrimaryTravellerCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isGstEnabled: false,
      isScrolling: false,
    };
    this.isSubmitting = false;
  }

  componentWillMount() {
    this.pageInit.initTime = moment();
    const {
      search: {
        datePicker,
        searchInput,
        roomConfig,
      },
      searchActions,
      hotel,
      selectedRoom,
    } = this.props;
    const existingSearchQuery = {
      ...apiService.getSearchQuery({
        hotelId: hotel.id,
        range: datePicker.range,
        rooms: roomConfig.rooms,
        roomType: roomConfig.roomType.toLowerCase(),
        ratePlan: selectedRoom.plan,
      }),
    };
    searchActions.setSearchState({
      ...urlService.constructUrlQuery({
        place: searchInput.place,
        range: datePicker.range,
        rooms: roomConfig.rooms,
        roomType: selectedRoom.type.toLowerCase(),
      }),
      ratePlan: selectedRoom.plan,
      hotel_id: hotel.id,
      checkin: existingSearchQuery.checkin,
      checkout: existingSearchQuery.checkout,
      roomconfig: existingSearchQuery.roomconfig,
    });
  }

  getInitialFormValues = () => {
    const loggedInUser = get(this.props.user, 'profile', {});
    const bookingUser = get(this.props.booking, 'guest', {});
    if (__DEV__) {
      return {
        name: 'Test' || bookingUser.name || loggedInUser.name,
        mobile: '8792103416' || bookingUser.mobile || loggedInUser.mobile,
        email: 'test@treebohotels.com' || bookingUser.email || loggedInUser.email || '',
        orgName: bookingUser.organizationName,
        orgTaxCode: bookingUser.organizationTaxcode,
        orgAddress: bookingUser.organizationAddress,
      };
    }
    return {
      name: bookingUser.name || loggedInUser.name,
      mobile: bookingUser.mobile || loggedInUser.mobile,
      email: bookingUser.email || loggedInUser.email || '',
      orgName: bookingUser.organizationName,
      orgTaxCode: bookingUser.organizationTaxcode,
      orgAddress: bookingUser.organizationAddress,
    };
  };

  getCommonEventPropsBookNow = () => {
    const {
      hotel,
      search: { roomConfig, datePicker },
      price,
      coupon,
      selectedRoom: { plan, type },
    } = this.props;
    const {
      checkIn,
      checkOut,
      numberOfNights,
    } = searchService.formattedDatePicker(datePicker.range);
    const formattedRoomConfig = searchService.formattedRoomConfig(roomConfig.rooms);
    const ratePlan = get(price[type], `ratePlans.${plan}`, {});

    return {
      hotel_id: hotel.id,
      af_content_id: hotel.id,
      hotel_name: hotel.name,
      price: get(ratePlan, 'sellingPrice'),
      total_price: get(ratePlan, 'totalPrice'),
      is_using_treebo_points: get(ratePlan, 'isUsingTreeboPoints'),
      treebo_points: get(ratePlan, 'totalUsableTreeboPoints'),
      coupon,
      checkin: checkIn,
      checkout: checkOut,
      numberOfNights,
      adults: formattedRoomConfig.adults.count,
      kids: formattedRoomConfig.kids.count,
      rooms: formattedRoomConfig.rooms.count,
    };
  }

  pageInit = {
    initTime: 0,
    hasInteractivityEventBeenSent: false,
  };

  isScrolling = () => {
    this.setState({ isScrolling: true });
  }

  isNotScrolling = () => {
    this.setState({ isScrolling: false });
  }

  handleBookButton = (e) => {
    analyticsService.segmentTrackEvent('Book Now Clicked', this.getCommonEventPropsBookNow());
    if (!this.isSubmitting) {
      this.formProps.handleSubmit(e);
    }
  }

  handlePayment = (details) => {
    const {
      history,
      search,
      hotel,
      selectedRoom,
      sellingPrice,
      totalPrice,
      checkoutActions,
      booking,
      checkout,
    } = this.props;

    checkoutActions.continueDetails(details);
    history.push({
      pathname: '/payment/',
      query: {
        hotel,
        history,
        price: {
          sellingPrice,
          totalPrice,
        },
        search,
        ratePlan: checkout.selectedRatePlan,
        user: details,
        booking,
        bid: booking.bid,
        isPayAtHotelEnabled: checkout.isPayAtHotelEnabled,
        memberDiscount: selectedRoom.memberDiscount,
      },
    });
  };

  render() {
    const { sellingPrice, totalPrice, isModal, checkout } = this.props;
    const { isGstEnabled } = this.state;

    const validationSchema = Form.validation.object().shape({
      name: Form.validation.string().required('Guest name is required'),
      mobile: Form.validation.string().required('Mobile Number is required')
        .matches(mobileRegex, { message: 'Please enter a valid contact number' }),
      email: Form.validation.string().required('Email is required')
        .matches(emailRegex, { message: 'Please enter a valid email address' }),
      orgName: isGstEnabled ? (
        Form.validation.string().required('Company name is required')
      ) : (
        Form.validation.string()
      ),
      orgTaxCode: isGstEnabled ? (
        Form.validation.string().required('Please enter a valid GSTIN')
          .matches(gstnRegex, { message: 'Please enter a valid GSTIN' })
      ) : (
        Form.validation.string().matches(gstnRegex, { message: 'Please enter a valid GSTIN' })
      ),
      orgAddress: isGstEnabled ? (
        Form.validation.string().required('Company address is required')
      ) : (
        Form.validation.string()
      ),
    });

    return (
      <Form
        initialValues={this.getInitialFormValues()}
        validationSchema={validationSchema}
        onSubmit={(values, actions) => {
          const { closeModals } = this.props;
          if (closeModals) {
            closeModals();
          }
          this.isSubmitting = true;
          actions.setSubmitting(true);
          analyticsService.segmentTrackEvent('Traveller Info Added', {
            name: values.name,
            mobile: values.mobile,
            email: values.email,
            organizationName: values.orgName,
            organizationTaxcode: values.orgTaxCode,
            organizationAddress: values.orgAddress,
          });
          this.handlePayment({
            name: values.name,
            mobile: values.mobile,
            email: values.email,
            organizationName: values.orgName,
            organizationTaxcode: values.orgTaxCode,
            organizationAddress: values.orgAddress,
          });
          actions.setSubmitting(false);
          this.isSubmitting = false;
        }}
        render={(formProps) => {
          this.formProps = formProps;
          return (
            <Card
              marginBottom={isModal ? 0 : 8}
              style={{ height: isModal ? Dimensions.get('window').height - 94 : 'auto' }}
            >
              <ScrollView
                scrollEnabled={isModal}
                keyboardShouldPersistTaps="handled"
                onScrollBeginDrag={this.isScrolling}
                onScrollEndDrag={this.isNotScrolling}
                onMomentumScrollBegin={this.isScrolling}
                onMomentumScrollEnd={this.isNotScrolling}
                bounces={false}
                alwaysBounceVertical={false}
                showsVerticalScrollIndicator={false}
              >
                <SlideInView disabled={!isModal}>
                  <Spacer padding={[2, 0, 0, 2]}>
                    <Text size="m" weight="bold">Primary Traveller</Text>
                  </Spacer>
                  <Spacer padding={[1, 2, 2, 2]}>
                    <TextInput
                      name="name"
                      label="Primary Guest Name"
                      error="Please enter a valid guest name"
                      rightIcon={{
                        component: (
                          <ClearButton onPress={() => formProps.setFieldValue('name', '')}>
                            {getIcon('reset-input', 'grey', 24, 16)}
                          </ClearButton>
                        ),
                        showOnlyOnFocus: true,
                      }}
                      autoCorrect={false}
                      keyboardType="default"
                      returnKeyType="next"
                      onSubmitEditing={() => { this._mobileRef.focus(); }}
                      testID="enter-name-field"
                      accessibilityLabel="enter-name-field"
                    />
                    <TextInput
                      name="mobile"
                      innerRef={(ref) => { this._mobileRef = ref; }}
                      label="Mobile Number"
                      error="Please enter a valid Mobile number"
                      keyboardType="numeric"
                      maxLength={10}
                      rightIcon={{
                        component: (
                          <ClearButton onPress={() => formProps.setFieldValue('mobile', '')}>
                            {getIcon('reset-input', 'grey', 24, 16)}
                          </ClearButton>
                        ),
                        showOnlyOnFocus: true,
                      }}
                      autoCorrect={false}
                      returnKeyType="next"
                      onSubmitEditing={() => { this._emailRef.focus(); }}
                      testID="enter-phone-field"
                      accessibilityLabel="enter-phone-field"
                    />
                    <TextInput
                      name="email"
                      label="Email Id"
                      error="Please enter a valid email address"
                      innerRef={(ref) => { this._emailRef = ref; }}
                      keyboardType="email-address"
                      autoCapitalize="none"
                      rightIcon={{
                        component: (
                          <ClearButton onPress={() => formProps.setFieldValue('email', '')}>
                            {getIcon('reset-input', 'grey', 24, 16)}
                          </ClearButton>
                        ),
                        showOnlyOnFocus: true,
                      }}
                      autoCorrect={false}
                      returnKeyType="go"
                      onSubmitEditing={(e) => {
                        analyticsService.segmentTrackEvent('Book Now Clicked', this.getCommonEventPropsBookNow());
                        formProps.handleSubmit(e);
                      }}
                      testID="enter-email-address-field"
                      accessibilityLabel="enter-email-address-field"
                    />
                    <Spacer padding={[2, 0, 0, 0]}>
                      <CheckBox
                        onPress={() => {
                          formProps.setFieldValue('orgName', '');
                          formProps.setFieldValue('orgTaxCode', '');
                          formProps.setFieldValue('orgAddress', '');
                          this.setState({ isGstEnabled: !isGstEnabled });
                        }}
                        checked={isGstEnabled}
                        label="Use GSTIN for this booking (Optional)"
                      />
                      {
                        isGstEnabled &&
                        <Spacer padding={[2, 0, 0, 0]}>
                          <TextInput
                            name="orgTaxCode"
                            label="GST Identification No."
                            error="Please enter a valid GSTIN"
                            rightIcon={{
                              component: (
                                <ClearButton onPress={() => formProps.setFieldValue('orgTaxCode', '')}>
                                  {getIcon('reset-input', 'grey', 24, 16)}
                                </ClearButton>
                              ),
                              showOnlyOnFocus: true,
                            }}
                            autoCorrect={false}
                            keyboardType="default"
                            autoCapitalize="characters"
                            testID="enter-gst-identification-number-field"
                            accessibilityLabel="enter-gst-identification-number-field"
                          />
                          <TextInput
                            label="Company Name"
                            name="orgName"
                            error="Please enter a valid company name"
                            rightIcon={{
                              component: (
                                <ClearButton onPress={() => formProps.setFieldValue('orgName', '')}>
                                  {getIcon('reset-input', 'grey', 24, 16)}
                                </ClearButton>
                              ),
                              showOnlyOnFocus: true,
                            }}
                            autoCorrect={false}
                            keyboardType="default"
                            testID="enter-gst-company-name-field"
                            accessibilityLabel="enter-gst-company-name-field"
                          />
                          <TextInput
                            name="orgAddress"
                            label="Company Address"
                            error="Please enter a valid address"
                            rightIcon={{
                              component: (
                                <ClearButton onPress={() => formProps.setFieldValue('orgAddress', '')}>
                                  {getIcon('reset-input', 'grey', 24, 16)}
                                </ClearButton>
                              ),
                              showOnlyOnFocus: true,
                            }}
                            autoCorrect={false}
                            keyboardType="default"
                            autoCapitalize="words"
                            testID="enter-gst-company-address-field"
                            accessibilityLabel="enter-gst-company-address-field"
                          />
                        </Spacer>
                      }
                    </Spacer>
                  </Spacer>
                </SlideInView>
                <View
                  onLayout={() => {
                    if (this.props.setPress) {
                      this.props.setPress(this.handleBookButton);
                    }
                  }}
                />
              </ScrollView>
              {
                sellingPrice > 0 && isModal &&
                <BookButton
                  isScrolling={this.state.isScrolling}
                  disabled={checkout.isLoading}
                  price={totalPrice}
                  onBookButtonPress={this.handleBookButton}
                  isAvailable
                />
              }
            </Card>
          );
        }}
      />
    );
  }
}

PrimaryTravellerCard.propTypes = {
  sellingPrice: PropTypes.number,
  totalPrice: PropTypes.number,
  hotel: PropTypes.object,
  search: PropTypes.object,
  checkoutActions: PropTypes.object,
  searchActions: PropTypes.object,
  history: PropTypes.object,
  price: PropTypes.object,
  coupon: PropTypes.object,
  selectedRoom: PropTypes.object,
  user: PropTypes.object,
  booking: PropTypes.object,
  checkout: PropTypes.object,
  isModal: PropTypes.bool,
  closeModals: PropTypes.func,
  setPress: PropTypes.func,
};

PrimaryTravellerCard.defaultProps = {
  isModal: false,
};

const mapStateToProps = (state, ownProps) => ({
  hotel: state.hotel.results[ownProps.hotelId] || state.hotel.results[0],
  price: state.price.results[ownProps.hotelId] || state.price.results[0],
  checkout: state.checkout,
  coupon: state.coupon,
  search: state.search,
  user: state.user,
  booking: state.booking,
  wallet: state.wallet,
});

const mapDispatchToProps = (dispatch) => ({
  checkoutActions: bindActionCreators(checkoutActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(PrimaryTravellerCard));
