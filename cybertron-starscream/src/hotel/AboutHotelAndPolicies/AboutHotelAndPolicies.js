import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import kebabCase from 'lodash/kebabCase';
import get from 'lodash/get';
import { Spacer, Divider, Card, Flex } from 'leaf-ui/native';
import Modal from '../../components/Modal/Modal';
import Text from '../../components/NewLeaf/Text/Text';
import ReadMore from '../ReadMore/ReadMore';
import color from '../../theme/color';
import { getIcon, getSvgIconCode } from '../../utils/ComponentUtils/ComponentUtils';
import Content from '../../components/Content/Content';
import analyticsService from '../../analytics/analyticsService';
import TouchableComponent from '../../components/Touchable/TouchableComponent';
import BookButton from '../BookButton/BookButton';
import PrimaryTravellerCard from '../PrimaryTraveller/PrimaryTravellerCard';

const View = styled.View``;

class AboutHotelAndPolicies extends React.Component {
  state = {
    showPolicyModal: false,
    isShowingPrimaryTraveller: false,
    showAboutHotelModal: false,
    isScrolling: false,
  };

  isScrolling = () => {
    this.setState({ isScrolling: true });
  }

  isNotScrolling = () => {
    this.setState({ isScrolling: false });
  }

  _openHotelPoliciesModal = () => {
    this.setState({ showPolicyModal: true, showAboutHotelModal: false });
  };

  _formContentForPoliciesModal = () => {
    const {
      hotel: {
        policies,
      },
    } = this.props;

    return (
      <Content
        testID="hotel-policies-modal"
        accessibilityLabel="hotel-policies-modal"
        bounces={false}
        alwaysBounceVertical={false}
        onScrollBeginDrag={this.isScrolling}
        onScrollEndDrag={this.isNotScrolling}
        onMomentumScrollBegin={this.isScrolling}
        onMomentumScrollEnd={this.isNotScrolling}
      >
        <Spacer margin={[4, 0, 3, 2]}>
          <Text
            size="l"
            color="greyDarker"
            weight="bold"
          >
           Policies
          </Text>
        </Spacer>
        {
          policies && policies.map((policy) => (
            policy.policy_type === 'standard_check_in_time' ||
            policy.policy_type === 'standard_check_out_time' ||
            policy.policy_type === 'check_in_time' ||
            policy.policy_type === 'check_out_time' ?
              <View key={policy.title}>
                <Spacer padding={[0, 0, 2, 2]}>
                  <Flex flexDirection="row">
                    <Text
                      size="m"
                      color="greyDarker"
                      testID={`${policy.title}-text`}
                      accessibilityLabel={`${policy.title}-text`}
                    >
                      {`${policy.title}: `}
                    </Text>
                    <Text
                      size="m"
                      color="greyDarker"
                      testID={`${policy.title}-description`}
                      accessibilityLabel={`${policy.title}-description`}
                    >
                      {policy.description}
                    </Text>
                  </Flex>
                </Spacer>
              </View>
            : null
          ))
        }
        {
          policies && policies.map((policy) => (
            policy.policy_type === 'standard_check_in_time' ||
            policy.policy_type === 'standard_check_out_time' ||
            policy.policy_type === 'check_in_time' ||
            policy.policy_type === 'check_out_time' ?
            null :
            <View key={policy.title}>
              <Spacer padding={[6, 2, 0, 2]}>
                <Text
                  size="m"
                  weight="bold"
                  color="greyDarker"
                  testID={`${policy.title}-text`}
                  accessibilityLabel={`${policy.title}-text`}
                >
                  {`${policy.title}: `}
                </Text>
                <Spacer padding={[3, 0, 0, 0]}>
                  <Text
                    size="m"
                    color="greyDarker"
                    testID={`${policy.title}-description`}
                    accessibilityLabel={`${policy.title}-description`}
                  >
                    {policy.description}
                  </Text>
                </Spacer>
              </Spacer>
            </View>
          ))
        }
        <Spacer padding={[2]} />
      </Content>
    );
  };

  _openAboutHotelModal = () => {
    this.setState({ showAboutHotelModal: true, showPolicyModal: false });
  };

  _renderTruncatedFooter = (handlePress) => (
    <Text
      size="s"
      weight="normal"
      color="blue"
      onPress={handlePress}
    >
      Read more
    </Text>
  );

  _renderRevealedFooter = (handlePress) => (
    <Text
      size="s"
      weight="normal"
      color="blue"
      onPress={handlePress}
    >
      Show less
    </Text>
  );

  _getTreeboHighlights = () => {
    const { trilights } = this.props.hotel;
    const arr = [];
    if (trilights) {
      trilights.forEach((trilight) => (
        arr.push(
          <View key={trilight}>
            <Spacer padding={[0, 2, 2, 2]}>
              <Text
                size="s"
                weight="normal"
                color="greyDarker"
                testID="treebo-highlights-text"
                accessibilityLabel="treebo-highlights-text"
              >
                {trilight}
              </Text>
            </Spacer>
          </View>,
        )
      ));
    }
    return arr;
  };

  _formContentForAboutHotelModal = () => {
    const {
      amenities,
      accessibilities,
      trilights,
      description,
    } = this.props.hotel;

    return (
      <Content
        testID="about-hotel-modal"
        accessibilityLabel="about-hotel-modal"
        bounces={false}
        alwaysBounceVertical={false}
        onScrollBeginDrag={this.isScrolling}
        onScrollEndDrag={this.isNotScrolling}
        onMomentumScrollBegin={this.isScrolling}
        onMomentumScrollEnd={this.isNotScrolling}
      >
        <View>
          {
            !isEmpty(amenities) ? (
              <Spacer margin={[4, 0, 3, 2]}>
                <Text
                  size="l"
                  color="greyDarker"
                  weight="bold"
                  testID="hotel-amenities-section-text"
                  accessibilityLabel="hotel-amenities-section-text"
                >
                  Hotel Amenities
                </Text>
              </Spacer>
            ) : null
          }
          {
            amenities && amenities.map((amenity) => (
              !isEmpty(amenity.name) && getSvgIconCode(kebabCase(amenity.name)) ? (
                <View key={amenity.name}>
                  <Spacer padding={[0, 0, 2, 2]}>
                    <Flex
                      flexDirection="row"
                      alignItems="center"
                    >
                      {getIcon(kebabCase(amenity.name), color.warmGrey)}
                      <Spacer margin={[0, 0, 0, 1]}>
                        <Text
                          size="s"
                          color="greyDarker"
                          weight="normal"
                          testID="amenities-text"
                          accessibilityLabel="amenities-text"
                        >
                          {amenity.name.replace('_', ' ')}
                        </Text>
                      </Spacer>
                    </Flex>
                  </Spacer>
                </View>
              ) : null
            ))
          }
          {
            !isEmpty(accessibilities) ? (
              <Spacer margin={[!isEmpty(amenities) ? 4 : 7.5, 0, 3, 2]}>
                <Text
                  size="l"
                  color="greyDarker"
                  weight="bold"
                  testID="hotel-accessibilities-section-text"
                  accessibilityLabel="hotel-accessibilities-section-text"
                >
                  Hotel Accessibilities
                </Text>
              </Spacer>
            ) : null
          }
          {
            accessibilities && accessibilities.map((accessibility) => (
              <View key={accessibility.name}>
                <Spacer padding={[0, 0, 2, 2]}>
                  <Flex flexDirection="row">
                    <Text
                      size="s"
                      color="greyDarker"
                      weight="normal"
                      testID="accessibilities-text"
                      accessibilityLabel="accessibilities-text"
                    >
                      {`${accessibility.name} - `}
                    </Text>
                    <Text
                      size="s"
                      color="greyDarker"
                      weight="normal"
                      testID="accessibilities-distance-text"
                      accessibilityLabel="accessibilities-distance-text"
                    >
                      {`${accessibility.distance} kms away`}
                    </Text>
                  </Flex>
                </Spacer>
              </View>
            ))
          }
          {
            !isEmpty(trilights) ? (
              <Spacer margin={[4, 0, 3, 2]}>
                <Text
                  size="l"
                  color="greyDarker"
                  weight="bold"
                  testID="treebo-highlights-section-text"
                  accessibilityLabel="treebo-highlights-section-text"
                >
                  Treebo Highlights
                </Text>
              </Spacer>
            ) : null
          }
          <Spacer padding={[0]}>
            {this._getTreeboHighlights()}
          </Spacer>
          {
            !isEmpty(description) ? (
              <Spacer margin={[4, 0, 2, 2]}>
                <Text
                  size="l"
                  color="greyDarker"
                  weight="bold"
                  testID="hotel-description-section-text"
                  accessibilityLabel="hotel-description-section-text"
                >
                  Description
                </Text>
              </Spacer>
            ) : null
          }
          <Spacer padding={[0, 2]}>
            <ReadMore
              numberOfLines={5}
              renderTruncatedFooter={this._renderTruncatedFooter}
              renderRevealedFooter={this._renderRevealedFooter}
            >
              <Text
                size="s"
                color="greyDarker"
                weight="normal"
                testID="hotel-description-text"
                accessibilityLabel="hotel-description-text"
              >
                {description}
              </Text>
            </ReadMore>
          </Spacer>
        </View>
      </Content>
    );
  };

  closeModal = () => {
    analyticsService.segmentScreenViewed('About Hotel Page Viewed');
    this.setState({ showAboutHotelModal: false, showPolicyModal: false });
  };

  openPrimaryTraveller = () => {
    this.setState({ isShowingPrimaryTraveller: true });
  }

  closePrimaryTraveller = () => {
    this.setState({ isShowingPrimaryTraveller: false });
  }

  render() {
    const {
      showAboutHotelModal,
      showPolicyModal,
      isShowingPrimaryTraveller,
      isScrolling,
    } = this.state;

    const {
      isRoomAvailable,
      totalPrice,
      selectedRoom,
      booking,
      price,
    } = this.props;

    let selectedRoomPrice;
    let plans;
    let roomPrice;

    if (isRoomAvailable) {
      selectedRoomPrice = isRoomAvailable ? price[selectedRoom.type.toLowerCase()] : null;
      plans = selectedRoomPrice.ratePlans;
      roomPrice = plans[selectedRoom.plan];
    }

    return (
      <Card>
        <TouchableComponent
          onPress={this._openAboutHotelModal}
          testID="about-hotel-button"
          accessibilityLabel="about-hotel-button"
        >
          <Spacer padding={[2]}>
            <Flex
              flexDirection="row"
              justifyContent="space-between"
              alignItems="center"
            >
              <Text
                size="m"
                color="greyDarker"
                weight="normal"
              >
                About Hotel
              </Text>
              {getIcon('arrow-next', color.warmGrey)}
              <Modal
                icon="cross"
                isOpen={showAboutHotelModal}
                onClose={isShowingPrimaryTraveller ? this.closePrimaryTraveller : this.closeModal}
                iosBarStyle="dark-content"
              >
                {
                  !isShowingPrimaryTraveller ?
                  this._formContentForAboutHotelModal()
                  :
                  <Spacer padding={[2, 0, 0, 0]}>
                    <PrimaryTravellerCard
                      hotelId={booking.hotelId}
                      sellingPrice={get(roomPrice, 'sellingPrice', 0)}
                      totalPrice={totalPrice}
                      selectedRoom={selectedRoom}
                      isModal
                      closeModals={() => {
                        this.closePrimaryTraveller();
                        this.closeModal();
                      }}
                    />
                  </Spacer>
                }
                {
                  isRoomAvailable && !isShowingPrimaryTraveller &&
                    <BookButton
                      isScrolling={isScrolling}
                      price={totalPrice}
                      onBookButtonPress={this.openPrimaryTraveller}
                      isAvailable
                    />
                }
              </Modal>
            </Flex>
          </Spacer>
        </TouchableComponent>
        <Divider />
        <TouchableComponent
          onPress={this._openHotelPoliciesModal}
          testID="hotel-policies-button"
          accessibilityLabel="hotel-policies-button"
        >
          <Spacer padding={[2]}>
            <Flex
              flexDirection="row"
              justifyContent="space-between"
              alignItems="center"
            >
              <Text
                size="m"
                color="greyDarker"
                weight="normal"
              >
                Policies
              </Text>
              {getIcon('arrow-next', color.warmGrey)}
              <Modal
                icon="cross"
                isOpen={showPolicyModal}
                onClose={isShowingPrimaryTraveller ? this.closePrimaryTraveller : this.closeModal}
                iosBarStyle="dark-content"
              >
                {
                  !isShowingPrimaryTraveller ?
                  this._formContentForPoliciesModal()
                  :
                  <Spacer padding={[2, 0, 0, 0]}>
                    <PrimaryTravellerCard
                      hotelId={booking.hotelId}
                      sellingPrice={get(roomPrice, 'sellingPrice', 0)}
                      totalPrice={totalPrice}
                      selectedRoom={selectedRoom}
                      isModal
                      closeModals={() => {
                        this.closePrimaryTraveller();
                        this.closeModal();
                      }}
                    />
                  </Spacer> }
                {
                  isRoomAvailable && !isShowingPrimaryTraveller &&
                    <BookButton
                      isScrolling={isScrolling}
                      price={totalPrice}
                      onBookButtonPress={this.openPrimaryTraveller}
                      isAvailable
                    />
                }
              </Modal>
            </Flex>
          </Spacer>
        </TouchableComponent>
      </Card>
    );
  }
}

AboutHotelAndPolicies.propTypes = {
  hotel: PropTypes.object.isRequired,
  isRoomAvailable: PropTypes.bool.isRequired,
  totalPrice: PropTypes.number,
  booking: PropTypes.object.isRequired,
  price: PropTypes.object,
  selectedRoom: PropTypes.object.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  price: state.price.results[ownProps.hotelId] || state.price.results[0],
  hotel: state.hotel.results[ownProps.hotelId] || state.hotel.results[0],
  booking: state.booking,
});

export default connect(mapStateToProps)(AboutHotelAndPolicies);
