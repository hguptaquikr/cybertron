import React from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator } from 'react-native';
import { Button, Card, Spacer, Divider } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';
import priceService from '../../price/priceService';

const BookButton = ({
  price,
  isAvailable,
  disabled,
  onBookButtonPress,
  onLayout,
  isScrolling,
  isLoading,
}) => (
  <Card onLayout={onLayout} >
    {isScrolling && <Divider />}
    <Spacer padding={[1]}>
      <Button
        block
        disabled={!isAvailable || disabled || isLoading}
        onPress={onBookButtonPress}
        testID="book-button"
        accessibilityLabel="book-button"
      >
        {isLoading ?
          <ActivityIndicator color="white" />
          :
          <Text size="m" weight="medium" color="white">
            {
              isAvailable
                ? `BOOK NOW FOR ₹${priceService.formatPrice(price, 0)}`
                : 'CHECK AVAILABLE DATES'
            }
          </Text>
        }
      </Button>
    </Spacer>
  </Card>
);

BookButton.propTypes = {
  price: PropTypes.number,
  isAvailable: PropTypes.bool,
  disabled: PropTypes.bool,
  onBookButtonPress: PropTypes.func.isRequired,
  onLayout: PropTypes.func,
  isScrolling: PropTypes.bool,
  isLoading: PropTypes.bool,
};

BookButton.defaultProps = {
  isScrolling: false,
};

export default BookButton;
