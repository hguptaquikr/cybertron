// import Hashids from 'hashids';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';

const sortRoomType = (data) => {
  const roomTypePriority = ['common', 'acacia', 'oak', 'maple', 'mahogany'];
  const byRoomTypePriority = (a, b) =>
    roomTypePriority.indexOf(a.type || a) - roomTypePriority.indexOf(b.type || b);
  return data.sort(byRoomTypePriority);
};

const httpUrlFallback = (url = '') => url.replace('http:', 'https:');

export const roomSortOrder = (roomType) => {
  const roomTypePriority = ['common', 'acacia', 'oak', 'maple', 'mahogany'];
  return roomTypePriority.indexOf(roomType);
};

export default {
  transformHotelResultsApi({ data }) {
    return {
      results: data.result.reduce((obj, result) => ({
        ...obj,
        [result.id]: {
          id: result.id,
          name: result.hotelName,
          coordinates: result.coordinates,
          address: result.area,
          rooms: [],
          description: '',
          amenities: result.amenities,
          accessibilities: [],
          trilights: [],
          images: result.images,
          policies: result.hotel_policies,
          reviews: {
            isTaEnabled: get(result, 'ta_review.ta_enabled', ''),
            overallRating: {
              rating: get(result, 'ta_review.rating', ''),
              ratingImage: !isEmpty(get(result, 'ta_review.image', '')) ? httpUrlFallback(result.ta_review.image) : '',
            },
            amenitiesRatings: [],
            userReviews: [],
          },
          rank: {},
        },
      }), {}),
      filters: {
        distanceCap: get(data, 'filters.distance_cap', false),
      },
      sort: {
        by: get(data, 'sort.by', '') === 'recommended' ? 'recommended' : 'distance',
        coordinates: get(data, 'sort.coordinates', {}),
        list: get(data, 'sort.by') === 'recommended' ? { recommended: 'Recommended' } : null,
        button: get(data, 'sort.by') === 'recommended' ? { recommended: 'RECOMMENDED' } : null,
      },
    };
  },

  transformHotelDetailsApi({ data }) {
    return {
      hotel: {
        id: data.id,
        name: data.name,
        isActive: data.is_active,
        coordinates: data.coordinates,
        address: data.address,
        rooms: sortRoomType(data.rooms),
        description: data.description,
        amenities: data.facilities,
        accessibilities: data.accessibilities,
        trilights: data.trilights,
        policies: data.hotel_policies,
        images: sortRoomType(Object.keys(data.images))
          .reduce((arr, roomType) => {
            const imgArr = data.images[roomType].map((img) => ({ ...img, category: roomType }));
            return [...arr, ...imgArr];
          }, []),
      },
    };
  },

  transformHotelReviewsApi({ request_data: requestData, data: { ta_reviews: reviews } }) {
    const ratingMap = {
      5: 'Excellent',
      4: 'Very Good',
      3: 'Good',
      2: 'Average',
      1: 'Poor',
    };

    const overallRating = reviews.overall_rating || { rating_percent: [] };
    const overallRanking = reviews.overall_ranking || { rank: 0 };
    const amenitiesRatings = reviews.subratings || [];
    const topReviews = reviews.top_reviews || [];

    return {
      hotelId: requestData.hotel_id,
      reviews: {
        noOfReviews: reviews.num_reviews,
        overallRanking: {
          rankString: overallRanking.rank_string,
          rank: overallRanking.rank,
          rankOutOf: overallRanking.rank_out_of,
        },
        overallRating: {
          ratingImage: httpUrlFallback(overallRating.image),
          rating: overallRating.rating,
          subRatings: overallRating.rating_percent.map((percent) => ({
            percent: percent.percent,
            level: ratingMap[percent.rating],
            noOfReviews: percent.review_str,
          })),
        },
        amenitiesRatings: amenitiesRatings.map((amenitiesRating) => ({
          name: amenitiesRating.name,
          rating: amenitiesRating.rating,
          image: httpUrlFallback(amenitiesRating.image),
        })),
        userReviews: topReviews.map((topReview) => ({
          rating: topReview.review_detail.user_rating,
          ratingImage: httpUrlFallback(topReview.review_detail.image),
          title: topReview.review_detail.title,
          description: topReview.review_detail.text,
          publishDate: topReview.review_detail.review_date,
          user: {
            image: httpUrlFallback(topReview.user.image_url),
            name: topReview.user.user_name,
          },
        })),
        readReviewsUrl: reviews.read_reviews_url,
        writeReviewsUrl: reviews.write_review_url,
        isTaEnabled: reviews.ta_enabled,
      },
    };
  },

  transformHotelAvailabilityApi({ data }) {
    const availableRooms = Object.keys(data.availability)
      .map((room) => ({
        type: room.toLowerCase(),
        count: data.availability[room],
      }));

    return {
      rooms: sortRoomType(availableRooms),
      available: data.available,
    };
  },

  constructAddress({ street, locality = '', city, pincode = '' }) {
    return street ? `${street.replace(locality, '')} ${locality}, ${city} ${pincode}` : null;
  },

  parseHotelId(nextState, replace) {
    // const hashids = new Hashids('bqoksIlEnWtsL9lmaiZD', 8);
    const { location: { pathname, query }, params: { hotelId } } = nextState;
    if (!Number.isFinite(+hotelId)) {
      const decodedHotelId = 127;
      // hashids.decode(hotelId)[0];
      replace({
        pathname: pathname.replace(hotelId, decodedHotelId),
        query,
      });
    }
  },

  getNeedToKnowPolicies(policies) {
    return policies.filter((policy) => policy.display_in_need_to_know);
  },

  getDistanceBetween(start, end, accuracy = 1, precision = 0) {
    /* eslint-disable no-param-reassign, no-mixed-operators, no-plusplus, max-len */
    const startLat = get(start, 'lat', 0);
    const startLng = get(start, 'lng', 0);
    const endLat = get(end, 'lat', 0);
    const endLng = get(end, 'lng', 0);
    accuracy = Math.floor(accuracy);
    precision = Math.floor(precision);
    const toRad = Math.PI / 180;
    const a = 6378137;
    const b = 6356752.314245;
    const f = 1 / 298.257223563; // WGS-84 ellipsoid params
    const L = (endLng - startLng) * toRad;
    let cosSigma;
    let sigma;
    let sinAlpha;
    let cosSqAlpha;
    let cos2SigmaM;
    let sinSigma;
    const U1 = Math.atan((1 - f) * Math.tan(parseFloat(startLat) * toRad));
    const U2 = Math.atan((1 - f) * Math.tan(parseFloat(endLat) * toRad));
    const sinU1 = Math.sin(U1);
    const cosU1 = Math.cos(U1);
    const sinU2 = Math.sin(U2);
    const cosU2 = Math.cos(U2);
    let lambda = L;
    let lambdaP;
    let iterLimit = 100;
    do {
      const sinLambda = Math.sin(lambda);
      const cosLambda = Math.cos(lambda);
      sinSigma = Math.sqrt(cosU2 * sinLambda * (cosU2 * sinLambda) + (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
      if (sinSigma === 0) return 0; // co-incident points
      cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
      sigma = Math.atan2(sinSigma, cosSigma);
      sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
      cosSqAlpha = 1 - sinAlpha * sinAlpha;
      cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
      if (Number.isNaN(cos2SigmaM)) cos2SigmaM = 0; // equatorial line: cosSqAlpha=0 (§6)
      const C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
      lambdaP = lambda;
      lambda = L + (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));
    } while (Math.abs(lambda - lambdaP) > 1e-12 && --iterLimit > 0);
    if (iterLimit === 0) return NaN; // formula failed to converge
    const uSq = cosSqAlpha * (a * a - b * b) / (b * b);
    const A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
    const B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
    const deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
    const distance = (b * A * (sigma - deltaSigma)).toFixed(precision); // round to 1mm precision
    return Math.round(distance * (10 ** precision) / accuracy) * accuracy / (10 ** precision);
    /* eslint-enable no-param-reassign, no-mixed-operators, no-plusplus, max-len */
  },

  isCoupleFriendly(hotel) {
    return get(hotel, 'policies', []).some((policy) => policy.policy_type === 'is_couple_friendly');
  },

  isAnyCoupleFriendlyHotel(hotels = []) {
    return Object.keys(hotels).some((hotelId) => get(hotels[hotelId], 'policies', [])
      .some((policy) => policy.policy_type === 'is_couple_friendly'));
  },
};
