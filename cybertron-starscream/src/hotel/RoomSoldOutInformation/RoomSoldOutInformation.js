import React from 'react';
import { Flex, Card, Spacer } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';

const RoomSoldOutInformation = () => (
  <Spacer margin={[1, 0, 0, 0]}>
    <Card
      testID="room-sold-out-view"
      accessibilityLabel="room-sold-out-view"
    >
      <Spacer padding={[3, 2]}>
        <Flex flex={1}>
          <Text size="xxl" color="red" weight="semibold">Sold Out</Text>
        </Flex>
      </Spacer>
    </Card>
  </Spacer>
);

export default RoomSoldOutInformation;
