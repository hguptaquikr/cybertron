import React from 'react';
import { Switch, Route } from 'react-router-native';
// import ABExperiment from './ab/ABExperiment';
import LandingPage from './home/LandingPage/LandingPage';
import SearchResultsPage from './search/SearchResultsPage/SearchResultsPage';
import HotelDetailsPage from './hotel/HotelDetailsPage/HotelDetailsPage';
import CitiesPage from './city/CitiesPage/CitiesPage';
import LoginPage from './auth/LoginPage/LoginPage';
import ConfirmationPage from './checkout/ConfirmationPage/ConfirmationPage';
import Payment from './payment/Payment/Payment';
import BookingHistoryListPage from './booking/BookingHistoryList/BookingHistoryListPage';
import BookingHistoryDetailsPage from './booking/BookingHistoryDetails/BookingHistoryDetailsPage';
import BookingHistoryCancellationPage from './booking/BookingHistoryCancellation/BookingHistoryCancellationPage';
import WalletHistoryPage from './wallet/WalletHistory/WalletHistoryPage';
import IntroPage from './wallet/IntroPage/IntroPage';
import FAQsPage from './help/FAQsPage/FAQsPage';
import BrandCampaignPage from './brandCampaign/BrandCampaignPage';
import BrowserPage from './browser/BrowserPage';

// const HotelDetailsPageVariant = () => (
//   <ABExperiment
//     oldVariant={HotelDetailsPageOld}
//     newVariant={HotelDetailsPageNew}
//     experimentName="singlePageHD"
//   />
// );

const Routes = () => (
  <Switch>
    <Route exact path="/" component={LandingPage} />
    <Route path="/login/" component={LoginPage} />
    <Route path="/account/booking-history/:bookingId/" component={BookingHistoryDetailsPage} />
    <Route path="/account/booking-history/" component={BookingHistoryListPage} />
    <Route path="/c-:cancellationHash" component={BookingHistoryCancellationPage} />
    <Route path="/search/" component={SearchResultsPage} />
    <Route path="/hotels-in-:city/:hotelSlug-:hotelId(\w+)/" component={HotelDetailsPage} />
    <Route path="/hotels-in-:city/" component={SearchResultsPage} />
    <Route path="/:category-in-:city/" component={SearchResultsPage} />
    <Route path="/payment/" component={Payment} />
    <Route path="/confirmation/:bookingId/" component={ConfirmationPage} />
    <Route path="/cities/" component={CitiesPage} />
    <Route path="/wallet/wallet-history/" component={WalletHistoryPage} />
    <Route path="/wallet/intro/" component={IntroPage} />
    <Route path="/faq/" component={FAQsPage} />
    <Route path="/perfect-stay/" component={BrandCampaignPage} />
    <Route path="/f/" component={BrowserPage} />
    <Route path="/feedback/" component={BrowserPage} />
    <Route path="/tripadvisor/" component={BrowserPage} />
    <Route component={LandingPage} />
  </Switch>
);

export default Routes;
