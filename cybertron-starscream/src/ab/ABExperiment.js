import React from 'react';
import PropTypes from 'prop-types';
import StoreHouse from '../services/storeHouse';

export default class ABExperiment extends React.Component {
  state = { Component: null };

  componentDidMount() {
    const {
      experimentName,
      oldVariant,
      newVariant,
    } = this.props;

    // Check if the Component is not loaded already
    if (!this.state.Component) {
      StoreHouse.get('ab_experiments')
        .then((experiments) => {
          if (experiments[experimentName] && experiments[experimentName].variant === 'new') {
            this.setState({ Component: newVariant });
          } else {
            this.setState({ Component: oldVariant });
          }
        }).catch(() => {
          this.setState({ Component: oldVariant });
        });
    }
  }
  render() {
    const { Component } = this.state;
    if (Component) {
      return <Component />;
    }
    return null;
  }
}

ABExperiment.propTypes = {
  experimentName: PropTypes.string.isRequired,
  oldVariant: PropTypes.func.isRequired,
  newVariant: PropTypes.func.isRequired,
};
