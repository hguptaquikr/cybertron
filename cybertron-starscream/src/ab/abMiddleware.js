import api from '../api/api';
import * as abService from './abService';
import storeHouse from '../services/storeHouse';

export default async () => {
  abService.init();
  if (__DEV__) {
    storeHouse.delete(['ab_user', 'ab_experiments']);
  }
  try {
    const abUser = await storeHouse.get('ab_user');
    const currentTimestamp = new Date().getTime();
    let abTimestamp = (abUser && abUser.timestamp) || currentTimestamp;

    if (currentTimestamp - abTimestamp >= 2592000000) {
      abTimestamp = currentTimestamp;
    }

    const abUserId = abUser ? abUser.id : '';
    const ab = await api.get('/web/v1/ab/assign_all/', { abUserId }).then(abService.transformAbApi);

    abService.init(ab.user, ab.experiments);
    storeHouse.save('ab_user', {
      ...ab.user,
      timestamp: abTimestamp,
    });
    storeHouse.save('ab_experiments', {
      ...ab.experiments,
    });
  } catch (err) {
    console.warn(err);
  }
};
