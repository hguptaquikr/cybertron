import local from './local';
import development from './development';
import production from './production';

const env = process.env.STARSCREAM_ENV || (process.env.NODE_ENV === 'production' ? 'production' : 'local');

const config = {
  local,
  development,
  production,
};

export default config[env];
