import { Platform } from 'react-native';

export default {
  analyticsBlacklistedIps: [],
  apiUrl: 'https://growth.treebo.be/api',
  appleAppId: '1281202038',
  appsflyerKey: '5wZegg8yZhDicmtSv4PTve',
  baseUrl: 'https://growth.treebo.be',
  dateFormat: { query: 'YYYY-MM-DD', view: 'ddd, D MMM' },
  customerCareNumber: '+91 9322800100',
  facebookAppId: '920145944706004',
  googleAuthId: '639434118840-6v74lggr27nigdtvag387nkntd8scco1.apps.googleusercontent.com',
  googleMapsApiKey: Platform.OS === 'android'
    ? 'AIzaSyA3moSg_kGJIx9S9XnhAIjl8N_LyC1i4_c'
    : 'AIzaSyClxJTRvJ5E8lB0fwcdxPI69fcdBUl1NXQ',
  ipApiKey: 'hMhZQeoVYBKa3Bl',
  razorpayConfig: {
    key: 'rzp_test_upYNslfmrDV2tg',
    image: '//images.treebohotels.com/images/payment/payment-logo.png',
  },
  segmentAPIKeyWeb: 'b7knpKDJCZcd14nhK9xw3c2o9hhOhmvM',
  segmentAPIKeyAndroid: 'ikNqMVL2X5U3ZNOXLUGVUvhGpqcFvlGt',
  storeLink: Platform.OS === 'android'
    ? 'market://details?id=com.treebo.starscream'
    : 'itms-apps://itunes.apple.com/us/app/id1281202038?mt=8',
};
