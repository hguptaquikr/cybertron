import { Platform } from 'react-native';

export default {
  transformLandingContentApi({ data }) {
    return {
      offers: data[`app_offers_list_${Platform.OS.toLowerCase()}`],
      popularCities: data.app_popular_cities,
      whyTreebo: data.app_promises_list.map(({ image_url: imageUrl, ...otherPromises }) => ({
        ...otherPromises,
        imageUrl,
      })),
      promotionSection: {
        promotionImageUrl: data.app_promotion_section.promotion_image_url,
        navigateTo: data.app_promotion_section.navigate_to,
        isInAppUrl: data.app_promotion_section.is_in_app_url,
        aspectRatio: data.app_promotion_section.aspect_ratio,
      },
    };
  },

  transformCitiesListApi({ data }) {
    return data.sort((city1, city2) => {
      if (city1.name < city2.name) return -1;
      if (city1.name > city2.name) return 1;
      return 0;
    });
  },

  transformCancellationPolicyApi({ data }) {
    return {
      cancellationPolicy: data.cancellation_policy,
    };
  },

  transformLoginPageHeader({ data }) {
    return {
      loginPageHeader: data.login_page_header,
    };
  },

  transformSpecialRateHotels({ data }) {
    return {
      specialRateHotels: data.special_rate_hotels,
    };
  },

  transformFeedbackContent({ data }) {
    return {
      rateAndReview: {
        heading: data.rate_and_review.heading,
        message: data.rate_and_review.message,
        maxRating: data.rate_and_review.max_rating,
        thresholdForReasons: data.rate_and_review.threshold_for_reasons,
        goodRatingMessageAndroid: data.rate_and_review.good_rating_message_android,
        goodRatingMessageIos: data.rate_and_review.good_rating_message_ios,
        reasonsTitle: data.rate_and_review.reasons_title,
        reasons: data.rate_and_review.reasons,
      },
    };
  },
};
