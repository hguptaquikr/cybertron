const SHOW_NO_INTERNET_CONNECTION = 'SHOW_NO_INTERNET_CONNECTION';

const initialState = {
  show: false,
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SHOW_NO_INTERNET_CONNECTION:
      return {
        ...state,
        show: payload,
      };

    default:
      return state;
  }
};

export const noInternetConnection = (show) => ({
  type: SHOW_NO_INTERNET_CONNECTION,
  payload: show,
});
