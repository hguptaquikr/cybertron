import { handle } from 'redux-pack';
import { Platform } from 'react-native';
import contentService from './contentService';

const GET_SEO_CONTENT = 'GET_SEO_CONTENT';
const GET_LANDING_CONTENT = 'GET_LANDING_CONTENT';
const GET_CITIES_LIST = 'GET_CITIES_LIST';
const GET_CANCELLATION_POLICY = 'GET_CANCELLATION_POLICY';
const GET_LOGIN_PAGE_HEADER = 'GET_LOGIN_PAGE_HEADER';
const GET_FEEDBACK_CONTENT = 'GET_FEEDBACK_CONTENT';
const GET_SPECIAL_RATE_HOTELS = 'GET_SPECIAL_RATE_HOTELS';

const initialState = {
  policies: [],
  offers: [],
  popularCities: [],
  whyTreebo: [],
  promotionSection: {
    promotionImageUrl: '',
    navigateTo: '',
    isInAppUrl: '',
    aspectRatio: '',
  },
  cities: [],
  cancellationPolicy: {
    title: '',
    description: '',
  },
  loginPageHeader: [],
  rateAndReview: {
    heading: 'We\'d love to hear from you',
    message: 'How was your booking experience for',
    maxRating: 5,
    thresholdForReasons: 3,
    goodRatingMessageAndroid: 'Glad you had a very good experience! Please rate us on Play Store',
    goodRatingMessageIos: 'Glad you had a very good experience! Please rate us on App Store',
    reasonsTitle: 'How can we improve?',
    reasons: [
      'Price',
      'Availability',
      'Communication',
    ],
  },
  specialRateHotels: {},
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_SEO_CONTENT:
      return handle(state, action, {
        success: (s) => ({ ...s, seo: payload.data }),
      });

    case GET_LANDING_CONTENT:
      return handle(state, action, {
        success: (s) => ({ ...s, ...payload }),
      });

    case GET_CITIES_LIST:
      return handle(state, action, {
        success: (s) => ({ ...s, cities: payload }),
      });

    case GET_CANCELLATION_POLICY:
      return handle(state, action, {
        success: (s) => ({
          ...s,
          cancellationPolicy: payload.cancellationPolicy,
        }),
      });

    case GET_LOGIN_PAGE_HEADER:
      return handle(state, action, {
        success: (s) => ({
          ...s,
          loginPageHeader: payload.loginPageHeader,
        }),
      });

    case GET_FEEDBACK_CONTENT:
      return handle(state, action, {
        success: (s) => ({
          ...s,
          rateAndReview: payload.rateAndReview,
        }),
      });

    case GET_SPECIAL_RATE_HOTELS:
      return handle(state, action, {
        success: (s) => ({
          ...s,
          specialRateHotels: payload.specialRateHotels,
        }),
      });

    default:
      return state;
  }
};

export const getLandingContent = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_LANDING_CONTENT,
    promise: api.get('/v2/contents/', {
      keys: `app_promises_list,app_offers_list_${Platform.OS.toLowerCase()},app_popular_cities,app_promotion_section`,
    }).then(contentService.transformLandingContentApi),
  });

export const getCitiesList = (name) => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_CITIES_LIST,
    promise: api.get('/v2/cities/', { name })
      .then(contentService.transformCitiesListApi),
  });

export const getCancellationPolicy = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_CANCELLATION_POLICY,
    promise: api.get('/v2/contents/', {
      keys: 'cancellation_policy',
    }).then(contentService.transformCancellationPolicyApi),
  });

export const getLoginPageHeader = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_LOGIN_PAGE_HEADER,
    promise: api.get('/v2/contents/', {
      keys: 'login_page_header',
    }).then(contentService.transformLoginPageHeader),
  });

export const getSpecialRateHotels = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_SPECIAL_RATE_HOTELS,
    promise: api.get('/v2/contents/', {
      keys: 'special_rate_hotels',
    }).then(contentService.transformSpecialRateHotels),
  });

export const getFeedbackContent = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_FEEDBACK_CONTENT,
    promise: api.get('/v2/contents/', {
      keys: 'rate_and_review',
    }).then(contentService.transformFeedbackContent),
  });
