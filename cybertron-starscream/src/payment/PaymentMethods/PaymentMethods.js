import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Button } from 'leaf-ui/native';
import CardPay from '../CardPay/CardPay';
import color from '../../theme/color';
import Netbanking from '../Netbanking/Netbanking';
import Wallet from '../Wallet/Wallet';
import analyticsService from '../../analytics/analyticsService';
import priceService from '../../price/priceService';
import Text from '../../components/NewLeaf/Text/Text';

const View = styled.View``;

const RootContainer = styled.View`
  flex: 1;
`;

const BottomContainer = styled.View`
  backgroundColor: ${color.white};
  padding: 8px;
  width: 100%;
`;

class PaymentMethods extends Component {
  state = {
    selectedMethod: 'card',
  };

  onCheckBoxPress = (paymentType) => {
    const { totalPrice } = this.props;
    const properties = {
      type: paymentType,
      price: totalPrice,
    };
    analyticsService.segmentTrackEvent('Payment Type Selected', properties);
    this.setState({ selectedMethod: paymentType });
  };

  isSelected = (paymentType) => (this.state.selectedMethod === paymentType);

  showPaymentMethods = () => {
    this.setState((prevState) => ({ showPaymentMethods: !prevState.showPaymentMethods }));
  };

  render() {
    const {
      makePayment,
      totalPrice,
    } = this.props;
    return (
      <RootContainer>
        <View>
          <CardPay
            checked={this.isSelected('card')}
            onCheckBoxPress={() => this.onCheckBoxPress('card')}
            makePayment={makePayment}
            testID="debit-credit-card-button"
            accessibilityLabel="debit-credit-card-button"
          />
          <Netbanking
            checked={this.isSelected('netbanking')}
            onCheckBoxPress={() => this.onCheckBoxPress('netbanking')}
            makePayment={makePayment}
            testID="netbanking-button"
            accessibilityLabel="netbanking-button"
          />
          <Wallet
            checked={this.isSelected('wallet')}
            onCheckBoxPress={() => this.onCheckBoxPress('wallet')}
            makePayment={makePayment}
            testID="wallet-button"
            accessibilityLabel="wallet-button"
          />
          <BottomContainer>
            <Button
              block
              onPress={() => makePayment(this.state.selectedMethod)}
              testID="pay-button"
              accessibilityLabel="pay-button"
            >
              <Text size="m" weight="medium" family="medium" color="white">
                PAY &#x20b9;{priceService.formatPrice(totalPrice, 2)}
              </Text>
            </Button>
          </BottomContainer>
        </View>
      </RootContainer>
    );
  }
}

PaymentMethods.propTypes = {
  makePayment: PropTypes.func,
  totalPrice: PropTypes.number,
};

export default PaymentMethods;
