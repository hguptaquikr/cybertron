import React, { Component } from 'react';
import { Platform, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { bindActionCreators } from 'redux';
import { theme } from 'leaf-ui/native';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-native';
import RazorpayCheckout from 'react-native-razorpay';
import get from 'lodash/get';
import * as bookingActionCreators from '../../booking/bookingDuck';
import * as checkoutActionCreators from '../../checkout/checkoutDuck';
import * as searchActionCreators from '../../search/searchDuck';
import * as toastActionCreators from '../../toast/toastDuck';
import Container from '../../components/Container/Container';
import Header from '../../components/Header/Header';
import Text from '../../components/NewLeaf/Text/Text';
import PayAtHotel from '../PayAtHotel/PayAtHotel';
import color from '../../theme/color';
import Loader from '../../components/Loader/Loader';
import BookingDetailsSection from '../../checkout/BookingDetailsSection/BookingDetailsSection';
import PaymentMethods from '../PaymentMethods/PaymentMethods';
import Accordion from '../../components/Accordion/Accordion';
import priceService from '../../price/priceService';
import OtpModal from '../PayAtHotel/OtpModal';
import config from '../../config';
import analyticsService from '../../analytics/analyticsService';
import { actionProperties } from '../../ab/abService';
import SlideInView from '../../components/SlideInView/SlideInView';

const ScrollView = styled.ScrollView`
  backgroundColor: ${color.disabledGreyOpacity50};
`;

class Payment extends Component {
  state = {
    isRazorpayLoading: false,
    loadingMessage: 'Redirecting you to the payment gateway',
    showOtpModal: false,
    selectedAccordionIndex: 2,
  };

  componentWillMount() {
    const {
      price: { totalPrice },
      checkoutActions: { confirmBooking },
    } = this.props;
    if (totalPrice === 0) {
      this.setState(
        {
          isRazorpayLoading: true,
          loadingMessage: 'Confirming your booking!',
        },
      );
      this.initiateBooking(null)
        .then((payload) => {
          if (payload.error) {
            this.setState({ isRazorpayLoading: false });
          } else {
            confirmBooking(null, null)
              .then(this.redirectToBooking('pre-paid', payload.orderId, null));
          }
        });
    }
  }

  componentDidMount() {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('dark-content');
    } else {
      StatusBar.setBackgroundColor(color.black);
    }
    analyticsService.segmentScreenViewed('Payment Page Viewed');
  }

  onCheckBoxPress = (paymentType) => {
    this.setState({ selectedMethod: paymentType });
  };

  onProceedToPayAtHotel = () => {
    const { price: { sellingPrice } } = this.props;
    const properties = {
      type: 'pay at hotel',
      price: sellingPrice,
    };
    analyticsService.segmentTrackEvent('Payment Type Selected', properties);

    const {
      booking: { guest },
      checkout: { isOtpEnabled },
      checkoutActions: { isVerifiedNumber, sendCheckoutOtp },
    } = this.props;

    if (!isOtpEnabled) {
      this.setState(
        {
          isRazorpayLoading: true,
          loadingMessage: 'Confirming your booking!',
        },
      );
      this.checkRoomPlanAndConfirm();
    } else {
      isVerifiedNumber(guest.mobile).then(
        (result) => {
          if (result.payload.isOtpVerified) {
            this.setState(
              {
                isRazorpayLoading: true,
                loadingMessage: 'Confirming your booking!',
              },
            );
            this.checkRoomPlanAndConfirm();
          } else {
            this.setState(
              {
                isRazorpayLoading: false,
                loadingMessage: 'Verifying your number!',
              },
            );
            this.showOtpModal(true);
            sendCheckoutOtp(guest.mobile);
          }
        },
      );
    }
  };

  checkRoomPlanAndConfirm = () => {
    const {
      allRatePlans,
      ratePlan,
      booking,
      location,
      history,
      checkoutActions: { getCheckoutDetails },
      toastActions: { showToast },
      booking: { payment: { orderId } },
    } = this.props;

    const selectedRatePlan = ratePlan || priceService.getCheapestRatePlan(allRatePlans);
    let roomPrice = allRatePlans[selectedRatePlan];
    roomPrice = (roomPrice.code === 'NRP') ?
      priceService.getRatePlan(allRatePlans, 'EP') : roomPrice;
    const isRatePlanChanged = ratePlan !== roomPrice.code;
    // if order id exist then get new bid
    if (orderId) {
      getCheckoutDetails(null, roomPrice.code, roomPrice.isUsingTreeboPoints).then(
        (res) => {
          if (res.error) {
            showToast(res.error.payload.msg);
            this.setState({ isRazorpayLoading: false });
          } else {
            this.setState(
              {
                isRazorpayLoading: true,
                loadingMessage: 'Confirming your booking!',
              },
            );
            history.replace({
              query: {
                ...location.query,
                ratePlan: roomPrice.code,
                price: roomPrice,
              },
            });
            this.proceedToConfirmBooking(roomPrice.code);
          }
        },
      );
    } else if (isRatePlanChanged) {
      getCheckoutDetails(booking.bid, roomPrice.code).then(
        (res) => {
          if (res.error) {
            showToast(res.error.payload.msg);
            this.setState({ isRazorpayLoading: false });
          } else {
            this.setState(
              {
                isRazorpayLoading: true,
                loadingMessage: 'Confirming your booking!',
              },
            );
            history.replace({
              query: {
                ...location.query,
                ratePlan: roomPrice.code,
                price: roomPrice,
              },
            });
            this.proceedToConfirmBooking(roomPrice.code);
          }
        },
      );
    } else {
      this.proceedToConfirmBooking(ratePlan);
    }
  };

  handleSelectedAccordion = (key) => {
    this.setState(((prevState) => {
      if (prevState.selectedAccordionIndex !== key) {
        return { selectedAccordionIndex: key };
      }
      return { selectedAccordionIndex: (key % 3) + 1 };
    }));
  }

  showOtpModal = (visibility) => {
    this.setState({ showOtpModal: visibility });
  };

  proceedToConfirmBooking = (ratePlan) => {
    const {
      checkoutActions: { payAtHotel },
      bookingActions: { getConfirmationDetails },
      toastActions,
    } = this.props;
    this.showOtpModal(false);
    this.setState(
      {
        isRazorpayLoading: true,
        loadingMessage: 'Confirming your booking!',
      },
    );
    return payAtHotel(ratePlan).then(
      (res) => {
        if (res.error) {
          this.showSoldOutError(toastActions, res.payload.error.msg);
          this.setState({ isRazorpayLoading: false });
        } else {
          getConfirmationDetails(res.payload.orderId)
            .then(this.redirectToBooking('pay-at-hotel', res.payload.orderId, ''));
        }
      },
    );
  };

  invokeRazorPay = (method, amount, prefill, notes, orderId) => {
    const {
      checkoutActions: { confirmBooking },
      price: { sellingPrice },
      ratePlan,
      hotel,
    } = this.props;
    const options = {
      image: 'https://images.treebohotels.com/images/payment/payment-logo.png',
      currency: 'INR',
      key: config.razorpayConfig.key,
      amount,
      name: 'Treebo Hotels',
      prefill: {
        ...prefill,
        method,
      },
      notes,
      order_id: orderId,
      theme: { color: theme.color.teal },
    };

    RazorpayCheckout.open(options).then((data) => {
      this.setState({ loadingMessage: 'Confirming your booking!' });
      const propertiesToTrack = {
        amount_paid: sellingPrice,
        mode: 'pre-paid',
        order_id: orderId,
        razorpay_payment_id: data.razorpay_payment_id,
        rate_plan_type: ratePlan,
        hotel_id: hotel.id,
        af_content_id: hotel.id,
        hotel_name: hotel.name,
      };
      analyticsService.segmentTrackEvent('Payment Confirmed', propertiesToTrack);

      const razorPayData = data.razorpay_payment_id ? data : data.details;

      confirmBooking(razorPayData.razorpay_payment_id, razorPayData.razorpay_signature)
        .then(this.redirectToBooking('pre-paid', orderId, razorPayData.razorpay_payment_id));
    }).catch(() => {
      this.setState({
        isRazorpayLoading: false,
      });
    });
  };

  redirectToBooking = (mode, orderId, razorayPaymentId) => ({ payload, error }) => {
    const {
      price: { sellingPrice },
      hotel,
      ratePlan,
      history,
      search,
      price,
      user,
      booking,
      toastActions,
      memberDiscount,
    } = this.props;
    if (error) {
      this.showSoldOutError(toastActions, get(payload, 'error.msg', 'Confirmation Error'));
      this.setState({ isRazorpayLoading: false });
    } else {
      const propertiesToTrack = {
        amount_paid: sellingPrice,
        mode,
        order_id: orderId,
        razorpay_payment_id: razorayPaymentId,
        rate_plan_type: ratePlan,
        hotel_id: hotel.id,
        af_content_id: hotel.id,
        hotel_name: hotel.name,
      };
      analyticsService.segmentTrackEvent('Adwords Completed Order', {
        ...propertiesToTrack,
        ...actionProperties(['singlePageHD']),
      });

      history.push({
        pathname: `/confirmation/${payload.orderId}/`,
        query: {
          history,
          price,
          search,
          ratePlan: payload.ratePlan,
          user,
          booking,
          memberDiscount,
        },
      });
    }
  };

  isSelected = (paymentType) => (this.state.selectedMethod === paymentType);

  initiateBooking = async (paymentGateway) => {
    const {
      checkoutActions: { initiateBooking, getCheckoutDetails },
      booking: { payment: { orderId } },
      toastActions,
      ratePlan,
      price: { isUsingTreeboPoints },
    } = this.props;

    if (orderId) {
      await getCheckoutDetails(null, ratePlan, isUsingTreeboPoints);
    }

    const bookingOrderDetails = await initiateBooking(ratePlan, paymentGateway);
    if (bookingOrderDetails.error) {
      this.showSoldOutError(toastActions, bookingOrderDetails.payload.error.msg);
      return { error: true };
    }
    return bookingOrderDetails.payload;
  };

  showSoldOutError = (toastActions, errorMessage) => {
    toastActions.showToast(errorMessage);
  };

  makePayment = (method) => {
    this.setState({ isRazorpayLoading: true, loadingMessage: 'Redirecting you to the payment gateway' });
    const {
      price: { sellingPrice },
      ratePlan,
      booking: {
        bid,
        guest: {
          name,
          mobile,
          email,
        },
      },
    } = this.props;
    const amount = +(sellingPrice * 100).toFixed(2);

    const prefill = {
      name,
      contact: mobile,
      email,
    };

    const propertiesToTrack = {
      actual_total_cost: sellingPrice,
      bid,
      check_in_time: '12:00',
      check_out_time: '11:00',
      email,
      is_mobile: true,
      message: '',
      mobile,
      mode: 'pre-paid',
      name,
      rate_plan_type: ratePlan,
    };
    analyticsService.segmentTrackEvent('Payment Initiated', propertiesToTrack);

    this.initiateBooking()
      .then((payload) => {
        if (payload.error) {
          this.setState({ isRazorpayLoading: false });
        } else {
          const notes = { order_id: payload.orderId };
          this.invokeRazorPay(method, amount, prefill, notes, payload.gatewayOrderId);
        }
      });
  };

  render() {
    const {
      history,
      price,
      booking,
      booking: { isPrepaidOnly },
      checkoutActions: { updateCheckoutMobile, verifyCheckoutOtp, sendCheckoutOtp },
      hotel,
      search,
      ratePlan,
      searchActions,
      allRatePlans,
      memberDiscount,
      isPayAtHotelEnabled,
    } = this.props;

    const { selectedAccordionIndex } = this.state;

    const selectedRatePlan = ratePlan || priceService.getCheapestRatePlan(allRatePlans);
    const showPayAtHotel = !isPrepaidOnly && isPayAtHotelEnabled;

    return (
      this.state.isRazorpayLoading ?
        <Loader loadingText={this.state.loadingMessage} />
        :
        <Container>
          <Header
            iconProps={{ name: 'back' }}
            middle={
              <Text size="m" weight="normal">
                Payment Details
              </Text>
            }
            onLeftPress={history.goBack}
            backgroundColor={color.white}
            androidStatusBarColor={color.black}
            noShadow
          />
          <ScrollView>
            <Accordion
              title="View Booking Details"
              isSelected={selectedAccordionIndex === 1}
              onPress={() => this.handleSelectedAccordion(1)}
            >
              <BookingDetailsSection
                hotel={hotel}
                price={price}
                search={search}
                ratePlan={ratePlan}
                searchActions={searchActions}
              />
            </Accordion>
            <SlideInView>
              <Accordion
                title="Payment Methods"
                isSelected={selectedAccordionIndex === 2}
                onPress={() => this.handleSelectedAccordion(2)}
                testID="prepaid-payment-options-section"
                accessibilityLabel="prepaid-payment-options-section"
              >
                <PaymentMethods
                  makePayment={this.makePayment}
                  totalPrice={price.totalPrice}
                  memberDiscount={memberDiscount}
                />
              </Accordion>
            </SlideInView>
            <SlideInView>
              <Accordion
                title="Pay At Hotel"
                testID="pay-at-hotel-section"
                accessibilityLabel="pay-at-hotel-section"
                isSelected={selectedAccordionIndex === 3}
                onPress={() => this.handleSelectedAccordion(3)}
              >
                <PayAtHotel
                  showPayAtHotel={showPayAtHotel}
                  price={price}
                  onProceedToPayAtHotel={this.onProceedToPayAtHotel}
                  allRatePlans={allRatePlans}
                  selectedRatePlan={selectedRatePlan}
                  memberDiscount={memberDiscount}
                />
              </Accordion>
            </SlideInView>
          </ScrollView>
          <OtpModal
            isOpen={this.state.showOtpModal}
            onClose={() => this.showOtpModal(false)}
            onMobileChanged={updateCheckoutMobile}
            mobile={booking.guest.mobile}
            onVerifyOtp={verifyCheckoutOtp}
            onResendOtp={sendCheckoutOtp}
            otpSuccess={this.checkRoomPlanAndConfirm}
          />
        </Container>
    );
  }
}

Payment.propTypes = {
  checkoutActions: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  bookingActions: PropTypes.object.isRequired,
  checkout: PropTypes.object,
  price: PropTypes.object.isRequired,
  booking: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  ratePlan: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  allRatePlans: PropTypes.object.isRequired,
  toastActions: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  memberDiscount: PropTypes.object.isRequired,
  isPayAtHotelEnabled: PropTypes.bool.isRequired,
};

const mapStateToProps = (state, { location: { query } }) => ({
  checkout: state.checkout,
  booking: state.booking,
  hotel: query.hotel,
  price: query.price,
  search: state.search,
  ratePlan: query.ratePlan,
  user: query.user,
  allRatePlans: state.price.results[query.hotel.id][state.booking.room.type].ratePlans,
  memberDiscount: query.memberDiscount,
  isPayAtHotelEnabled: query.isPayAtHotelEnabled,
});

const mapDispatchToProps = (dispatch) => ({
  checkoutActions: bindActionCreators(checkoutActionCreators, dispatch),
  bookingActions: bindActionCreators(bookingActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(Payment));
