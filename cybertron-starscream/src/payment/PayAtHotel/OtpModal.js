import React from 'react';
import PropTypes from 'prop-types';
import Modal from '../../components/Modal/Modal';
import Otp from '../../auth/otp/Otp/Otp';

const OtpModal = ({
  isOpen,
  onClose,
  onMobileChanged,
  mobile,
  onVerifyOtp,
  onResendOtp,
  otpSuccess,
}) => (
  <Modal
    isOpen={isOpen}
    label="Otp"
    onClose={onClose}
    closeIcon="close"
    iosBarStyle="dark-content"
  >
    <Otp
      mobile={mobile}
      onVerifyOtp={onVerifyOtp}
      onResendOtp={onResendOtp}
      onMobileChanged={onMobileChanged}
      otpSuccess={otpSuccess}
      onClose={onClose}
    />
  </Modal>
);

OtpModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  mobile: PropTypes.string,
  onClose: PropTypes.func.isRequired,
  onVerifyOtp: PropTypes.func,
  onResendOtp: PropTypes.func,
  otpSuccess: PropTypes.func,
  onMobileChanged: PropTypes.func,
};

export default OtpModal;
