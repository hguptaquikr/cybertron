import React from 'react';
import PropTypes from 'prop-types';
import PayAtHotelModal from './PayAtHotelModal';

const PayAtHotel = ({
  onProceedToPayAtHotel,
  allRatePlans,
  selectedRatePlan,
  showPayAtHotel,
}) => (
  <PayAtHotelModal
    onProceedToPayAtHotel={onProceedToPayAtHotel}
    allRatePlans={allRatePlans}
    selectedRatePlan={selectedRatePlan}
    showPayAtHotel={showPayAtHotel}
  />
);

PayAtHotel.propTypes = {
  onProceedToPayAtHotel: PropTypes.func.isRequired,
  allRatePlans: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.string.isRequired,
  showPayAtHotel: PropTypes.bool.isRequired,
};

export default PayAtHotel;
