import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import debounce from 'lodash/debounce';
import get from 'lodash/get';
import { Spacer, Button } from 'leaf-ui/native';
import Content from '../../components/Content/Content';
import Text from '../../components/NewLeaf/Text/Text';
import Price from '../../components/NewLeaf/Price/Price';
import RatePlanTypeView from '../../price/RatePlanTypeView/RatePlanTypeView';
import color from '../../theme/color';
import priceService from '../../price/priceService';

const RootContainer = styled.View`
  paddingBottom: 8px;
  paddingHorizontal: 8px;
  backgroundColor: ${color.white};
`;

const BottomContainer = styled.View`
  padding: 8px;
  width: 100%;
`;

const SectionsView = styled.View`
  paddingHorizontal: 8px;
  paddingVertical: 16px;
`;

const Priceview = styled.View`
  flexDirection: row
`;

class PayAtHotelModal extends Component {
  state = {};

  render() {
    const {
      onProceedToPayAtHotel,
      allRatePlans,
      selectedRatePlan,
      showPayAtHotel,
    } = this.props;
    let roomPrice = get(allRatePlans, `${selectedRatePlan}`, {});
    const isNonRefundableRatePlan = (get(roomPrice, 'code') === 'NRP');
    const ratePlanDiff = isNonRefundableRatePlan ? priceService.getRatePlanDiff(allRatePlans) : 0;
    roomPrice = isNonRefundableRatePlan ?
      priceService.getRatePlan(allRatePlans, 'EP') : roomPrice;

    return (
      <RootContainer
        testID="pay-at-hotel-modal"
        accessibilityLabel="pay-at-hotel-modal"
      >
        {
          showPayAtHotel ? (
            <Content
              testID="pay-at-hotel-enabled-view"
              accessibilityLabel="pay-at-hotel-enabled-view"
            >
              {
                ratePlanDiff > 0 ? (
                  <SectionsView>
                    <Spacer padding={[0, 0, 1.5, 0]}>
                      <Text
                        size="m"
                        weight="normal"
                        testID="rate-plan-different-text"
                        accessibilityLabel="rate-plan-different-text"
                      >
                        Additional Charge of ₹{ratePlanDiff}
                      </Text>
                    </Spacer>
                    <Text size="s" weight="normal">
                      If you are unsure of your travel dates,  you can choose to pay at hotel for an
                      additional ₹{ratePlanDiff}. Zero cancellation fee within 24hours of
                      checkin.
                    </Text>
                  </SectionsView>
                ) : null
              }
              <SectionsView>
                <Spacer padding={[0, 0, 1.5, 0]}>
                  <Text size="m" weight="normal">
                    Secure your Rooms
                  </Text>
                </Spacer>
                <Text size="s" weight="normal">
                    You can secure your rooms by paying a part payment 2 days prior your check-in
                    dates.
                </Text>
              </SectionsView>
              <SectionsView>
                <Spacer padding={[0, 0, 1.5, 0]}>
                  <Text size="m" weight="normal">
                    Total Payable Amount
                  </Text>
                </Spacer>
                <Priceview>
                  <Price
                    price={get(roomPrice, 'totalPrice')}
                    size="l"
                    color="greyDarker"
                    type="regular"
                    bold
                  />
                  <Spacer padding={[0.5, 0, 0, 0.5]}>
                    <RatePlanTypeView isNRP={get(roomPrice, 'code') === 'NRP'} />
                  </Spacer>
                </Priceview>
                <Text size="xs" weight="normal" color="grey">
                  inc. of all taxes
                </Text>
              </SectionsView>
              <BottomContainer>
                <Button
                  block
                  onPress={debounce(onProceedToPayAtHotel, 500)}
                  testID="pay-at-hotel-proceed-button"
                  accessibilityLabel="pay-at-hotel-proceed-button"
                >
                  <Text size="m" weight="medium" family="medium" color="white">
                    PROCEED TO BOOK
                  </Text>
                </Button>
              </BottomContainer>
            </Content>
          ) : (
            <Content
              testID="pay-at-hotel-disabled-view"
              accessibilityLabel="pay-at-hotel-disabled-view"
            >
              <SectionsView>
                <Text size="m" weight="normal" color="greyDarker">
                  Pay At Hotel is unavailable due to high demand. Please select from one of the
                  other payment methods.
                </Text>
              </SectionsView>
              <BottomContainer>
                <Button
                  block
                  disabled
                  testID="pay-at-hotel-proceed-button"
                  accessibilityLabel="pay-at-hotel-proceed-button"
                >
                  <Text size="m" weight="medium" family="medium" color="white">
                    PROCEED TO BOOK
                  </Text>
                </Button>
              </BottomContainer>
            </Content>
          )
        }
      </RootContainer>
    );
  }
}

PayAtHotelModal.propTypes = {
  onProceedToPayAtHotel: PropTypes.func.isRequired,
  allRatePlans: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.string.isRequired,
  showPayAtHotel: PropTypes.bool.isRequired,
};

export default PayAtHotelModal;
