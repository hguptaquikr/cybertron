import React from 'react';
import PropTypes from 'prop-types';
import PaymentMethodCard from '../PaymentMethodCard/PaymentMethodCard';

const title = 'Pay by Debit / Credit / ATM';

const CardPay = ({ checked, onCheckBoxPress, ...restProps }) => (
  <PaymentMethodCard
    title={title}
    checked={checked}
    onPress={onCheckBoxPress}
    {...restProps}
  />
);

CardPay.propTypes = {
  checked: PropTypes.bool,
  onCheckBoxPress: PropTypes.func,
};

export default CardPay;
