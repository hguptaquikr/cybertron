import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Spacer, Flex } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';
import color from '../../theme/color';
import CheckBox from '../../components/CheckBox/CheckBox';

const View = styled.View``;

const TouchableOpacity = styled.TouchableOpacity`
  backgroundColor: ${color.white}
`;

const PaymentMethodCard = ({ title, checked, children, onPress, testID, accessibilityLabel }) => (
  <TouchableOpacity
    onPress={onPress}
    testID={testID}
    accessibilityLabel={accessibilityLabel}
  >
    <Spacer padding={[2, 1]}>
      <Flex
        flexDirection="row"
        alignItems="center"
      >
        <CheckBox
          onClick={onPress}
          checked={checked}
          color={color.primary}
        />
        <Spacer margin={[0, 0, 0, 1.5]}>
          <Text
            size="m"
            weight={checked ? 'bold' : 'normal'}
            color="greyDarker"
          >
            {title}
          </Text>
        </Spacer>
      </Flex>
      { checked ? (
        <View>
          {children}
        </View>
        ) : null }
    </Spacer>
  </TouchableOpacity>
);

PaymentMethodCard.propTypes = {
  title: PropTypes.string.isRequired,
  checked: PropTypes.bool,
  children: PropTypes.node,
  onPress: PropTypes.func.isRequired,
  testID: PropTypes.string,
  accessibilityLabel: PropTypes.string,
};

export default PaymentMethodCard;
