import React from 'react';
import PropTypes from 'prop-types';
import PaymentMethodCard from '../PaymentMethodCard/PaymentMethodCard';

const title = 'Mobikwik';

const Wallet = ({ checked, onCheckBoxPress, ...restProps }) => (
  <PaymentMethodCard title={title} checked={checked} onPress={onCheckBoxPress} {...restProps} />
);

Wallet.propTypes = {
  checked: PropTypes.bool,
  onCheckBoxPress: PropTypes.func,
};

export default Wallet;
