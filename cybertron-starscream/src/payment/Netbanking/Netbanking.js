import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PaymentMethodCard from '../PaymentMethodCard/PaymentMethodCard';

const title = 'Net banking';

class Netbanking extends Component {
  state = {};

  render() {
    const { checked, onCheckBoxPress, ...restProps } = this.props;
    return (
      <PaymentMethodCard title={title} checked={checked} onPress={onCheckBoxPress} {...restProps} />
    );
  }
}

Netbanking.propTypes = {
  checked: PropTypes.bool,
  onCheckBoxPress: PropTypes.func,
};

export default Netbanking;
