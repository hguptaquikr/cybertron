/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import Text from '../../../components/NewLeaf/Text/Text';
import Modal from '../../../components/Modal/Modal';
import List from '../../../components/List/List';
import { getIcon } from '../../../utils/ComponentUtils/ComponentUtils';

const RootContainer = styled.View`
  padding: 16px;
`;

const TouchableOpacity = styled.TouchableOpacity``;

const BankItemContainer = styled.View`
  flexDirection: row;
  justifyContent: space-between;
  padding: 16px;
`;

const BankItem = ({ name, onPress }) => (
  <TouchableOpacity onPress={onPress}>
    <BankItemContainer>
      <Text size="m" weight="normal">{name}</Text>
      {getIcon('arrow-next')}
    </BankItemContainer>
  </TouchableOpacity>
);

const buildDataStore = (banks, onPress) => (banks.map((bank) => ({ name: bank, onPress })));

const BankListModal = ({ banks, onPress, isOpen, onClose }) => (
  <Modal
    title="Netbanking"
    onClose={onClose}
    isOpen={isOpen}
  >
    <RootContainer>
      <List
        keyExtractor={(item, i) => i}
        data={buildDataStore(banks, onPress)}
        renderItem={({ item }) => (
          <BankItem name={item.name} />
          )}
      />
    </RootContainer>
  </Modal>

);

BankItem.propTypes = {
  name: PropTypes.string,
  onPress: PropTypes.func,
};

BankListModal.propTypes = {
  banks: PropTypes.array,
  onPress: PropTypes.func,
  isOpen: PropTypes.func,
  onClose: PropTypes.func,
};

export default BankListModal;
