import { Platform } from 'react-native';
import moment from 'moment';
import config from '../config';

export default {
  getDatePickerQuery(range) {
    return {
      checkin: range.start && range.start.format(config.dateFormat.query),
      checkout: range.end
        ? range.end.format(config.dateFormat.query)
        : moment(range.start).add(1, 'days').format(config.dateFormat.query),
    };
  },

  getRoomConfigQuery(rooms) {
    return {
      roomconfig: rooms
        .map((room) => `${room.adults}-${room.kids || 0}`)
        .join(','),
    };
  },

  getRoomTypeQuery(roomType) {
    return {
      roomtype: roomType,
    };
  },

  getRatePlanQuery(ratePlan) {
    return {
      rateplan: ratePlan,
    };
  },

  getApplyWalletQuery(value) {
    return {
      apply_wallet: value,
    };
  },

  getSearchQuery({ hotelId, range, rooms, roomType, ratePlan, applyWallet, sort }) {
    return {
      hotel_id: hotelId,
      ...this.getDatePickerQuery(range),
      ...this.getRoomConfigQuery(rooms),
      ...this.getRoomTypeQuery(roomType),
      ...this.getRatePlanQuery(ratePlan),
      ...this.getApplyWalletQuery(applyWallet),
      channel: Platform.OS === 'ios' ? 'ios' : 'android',
      sort,
    };
  },
};
