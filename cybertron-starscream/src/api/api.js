import { Platform } from 'react-native';
import queryString from 'query-string';
import config from '../config';
import userService from '../user/userService';

const fireRequest = async (method, url, data) => {
  const fullUrl = `${config.apiUrl}${url}`;
  let authToken = '';
  const value = await userService.getAuthToken();
  if (value) {
    authToken = value;
  }
  const options = {
    method,
    body: JSON.stringify(data),
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${authToken}`,
      'User-Agent': `starscream/treebo-${Platform.OS}`,
    },
  };
  const response = await fetch(fullUrl, options);
  const json = await response.json();
  await userService.refreshAuthToken(response.headers.get('x-auth-token'));
  return response.ok ? json : Promise.reject(json);
};

export default {
  get(url, query) {
    const qs = queryString.stringify(query, { arrayFormat: 'index' });
    return fireRequest('GET', `${url}?${qs}`);
  },

  post(url, data) {
    return fireRequest('POST', url, data);
  },

  put(url, data) {
    return fireRequest('PUT', url, data);
  },

  delete(url) {
    return fireRequest('DELETE', url);
  },
};
