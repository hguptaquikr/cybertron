import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';

export default {
  transformSearchPricesApi({ data: hotels }) {
    return {
      price: {
        results: Object.keys(hotels)
          .reduce((obj, hotelId) => ({
            ...obj,
            [hotelId]: {
              [hotels[hotelId].cheapest_room.room_type]: {
                couponCode: hotels[hotelId].coupon_code,
                isAvailable: !!+hotels[hotelId].cheapest_room.availability,
                memberDiscount: {
                  isApplied: hotels[hotelId].member_discount_applied,
                  isAvailable: hotels[hotelId].member_discount_available,
                },
                ratePlans: [hotels[hotelId].rate_plan].reduce((ratePlans, ratePlan) => ({
                  ...ratePlans,
                  [ratePlan.code]: {
                    code: ratePlan.code,
                    tag: ratePlan.tag,
                    type: ratePlan.refundable ? 'refundable' : 'non-refundable',
                    description: ratePlan.description,
                    basePrice: hotels[hotelId].base_price,
                    pretaxPrice: hotels[hotelId].pretax_price,
                    treeboPointsUsed: hotels[hotelId].wallet_deduction,
                    isUsingTreeboPoints: hotels[hotelId].wallet_applied,
                    totalUsableTreeboPoints: hotels[hotelId].wallet_deductable_amount,
                    sellingPrice: hotels[hotelId].sell_price,
                    totalPrice: hotels[hotelId].net_payable_amount,
                    tax: hotels[hotelId].tax,
                    discountPercentage: hotels[hotelId].total_discount_percent,
                  },
                }), {}),
              },
            },
          }), {}),
      },
      hotels: {
        results: Object.keys(hotels)
          .reduce((obj, hotelId) => ({
            ...obj,
            [hotelId]: {
              rank: {
                recommended: hotels[hotelId].sort_index,
              },
            },
          }), {}),
      },
      availability: Object.keys(hotels)
        .reduce((obj, hotelId) => ({
          ...obj,
          [hotelId]: !!+hotels[hotelId].cheapest_room.availability,
        }), {}),
    };
  },

  transformRoomPricesApi({ data: { total_price: prices, error } }) {
    if (error) {
      return {
        price: {},
      };
    }
    return {
      price: Object.keys(prices)
        .reduce((obj, roomType) => ({
          ...obj,
          [roomType]: {
            isAvailable: prices[roomType].availability,
            memberDiscount: {
              isApplied: prices[roomType].member_discount_applied,
              isAvailable: prices[roomType].member_discount_available,
            },
            ratePlans: prices[roomType].rate_plans.reduce((ratePlans, ratePlan) => ({
              ...ratePlans,
              [ratePlan.rate_plan.code]: {
                code: ratePlan.rate_plan.code,
                tag: ratePlan.rate_plan.tag,
                type: ratePlan.rate_plan.refundable ? 'refundable' : 'non-refundable',
                description: ratePlan.rate_plan.description,
                basePrice: ratePlan.price.pretax_price,
                isUsingTreeboPoints: ratePlan.price.wallet_applied,
                treeboPointsUsed: ratePlan.price.wallet_deduction,
                sellingPrice: ratePlan.price.sell_price,
                totalPrice: ratePlan.price.net_payable_amount,
                tax: ratePlan.price.tax,
                discountPercentage: ratePlan.price.total_discount_percent,
                roomDiscount: ratePlan.price.promo_discount,
              },
            }), {}),
          },
        }), {}),
    };
  },

  getCheapestRoomType(price) {
    return !isEmpty(price) ?
      Object.keys(price)
        .reduce(
          (roomTypeA, roomTypeB) => {
            const cheapestRoomTypeARatePlan = roomTypeA ?
              this.getCheapestRatePlan(price[roomTypeA].ratePlans) : undefined;
            const cheapestRoomTypeBRatePlan = roomTypeB ?
              this.getCheapestRatePlan(price[roomTypeB].ratePlans) : undefined;
            return (
              ((get(price, `${roomTypeA}.ratePlans.${cheapestRoomTypeARatePlan}.sellingPrice`)
                < get(price, `${roomTypeB}.ratePlans.${cheapestRoomTypeBRatePlan}.sellingPrice`))
                ? roomTypeA : roomTypeB
              ));
          },
        )
      : undefined;
  },

  getCheapestRatePlan(roomTypeRatePlans) {
    return roomTypeRatePlans
      && !isEmpty(roomTypeRatePlans)
      && Object.keys(roomTypeRatePlans).reduce((ratePlanA, ratePlanB) => {
        const sellingPriceA = get(roomTypeRatePlans, `${ratePlanA}.sellingPrice`);
        const sellingPriceB = get(roomTypeRatePlans, `${ratePlanB}.sellingPrice`);
        return sellingPriceA < sellingPriceB ? ratePlanA : ratePlanB;
      });
  },

  getCheapestRoom(price) {
    let roomDetails;
    if (isEmpty(price)) {
      roomDetails = ({
        price: {},
        plan: '',
        type: '',
      });
    } else {
      const cheapestRoomType = this.getCheapestRoomType(price);
      const cheapestRatePlan = this.getCheapestRatePlan(price[cheapestRoomType].ratePlans);
      const roomPrice = price[cheapestRoomType].ratePlans[cheapestRatePlan];
      roomDetails = ({
        roomPrice,
        plan: cheapestRatePlan,
        type: cheapestRoomType,
      });
    }
    const isAvailable = get(price, `${roomDetails.type}.ratePlans.${roomDetails.plan}.isAvailable`, false);
    return {
      isAvailable,
      ...roomDetails,
    };
  },

  getCostliestRoomType(price) {
    return !isEmpty(price) ?
      Object.keys(price)
        .reduce(
          (roomTypeA, roomTypeB) => {
            const cheapestRoomTypeARatePlan = roomTypeA ?
              this.getCheapestRatePlan(price[roomTypeA].ratePlans) : undefined;
            const cheapestRoomTypeBRatePlan = roomTypeB ?
              this.getCheapestRatePlan(price[roomTypeB].ratePlans) : undefined;
            return (
              ((get(price, `${roomTypeA}.ratePlans.${cheapestRoomTypeARatePlan}.sellingPrice`)
                > get(price, `${roomTypeB}.ratePlans.${cheapestRoomTypeBRatePlan}.sellingPrice`))
                ? roomTypeA : roomTypeB
              ));
          },
        )
      : undefined;
  },

  getCostliestRatePlan(roomTypeRatePlans) {
    return roomTypeRatePlans
      && !isEmpty(roomTypeRatePlans)
      && Object.keys(roomTypeRatePlans).reduce((ratePlanA, ratePlanB) => {
        const sellingPriceA = get(roomTypeRatePlans, `${ratePlanA}.sellingPrice`);
        const sellingPriceB = get(roomTypeRatePlans, `${ratePlanB}.sellingPrice`);
        return sellingPriceA > sellingPriceB ? ratePlanA : ratePlanB;
      });
  },

  isMemberDiscountAvailable(price) {
    return Object.keys(price).some((roomType) => (Object.keys(price[roomType].ratePlans).some((ratePlan) => get(price[roomType].ratePlans[ratePlan], 'memberDiscount.isAvailable'))));
  },

  isMemberDiscountApplied(price) {
    return Object.keys(price).some((roomType) => (Object.keys(price[roomType].ratePlans).some((ratePlan) => get(price[roomType].ratePlans[ratePlan], 'memberDiscount.isApplied'))));
  },

  formatPrice(price, roundUpTo = 0) {
    return price.toFixed(roundUpTo).toLocaleString('en-IN');
  },

  getRatePlan(ratePlans, tag) {
    return Object.values(ratePlans).find((ratePlan) => ratePlan.code === tag);
  },

  getAutoAppliedCoupon(prices) {
    const priceWithCouponCode = Object.values(prices)
      .find((price) => (Object.values(price)[0].couponCode));
    return priceWithCouponCode && Object.values(priceWithCouponCode)[0].couponCode;
  },
  getRatePlanDiff(ratePlans) {
    const refundableRatePlan = this.getRatePlan(ratePlans, 'EP');
    const nonRefundableRatePlan = this.getRatePlan(ratePlans, 'NRP');
    const refundableRatePlanPrice = get(refundableRatePlan, 'sellingPrice');
    const nonRefundableRatePlanPrice = get(nonRefundableRatePlan, 'sellingPrice');
    return Math.round(refundableRatePlanPrice - nonRefundableRatePlanPrice);
  },
};
