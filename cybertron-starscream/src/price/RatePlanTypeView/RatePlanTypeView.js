import React from 'react';
import PropTypes from 'prop-types';
import Text from '../../components/NewLeaf/Text/Text';

const RatePlanTypeView = ({ isNRP, ratePlanTag }) => {
  if (ratePlanTag) {
    return (
      <Text
        size="m"
        weight="normal"
        color={ratePlanTag !== 'Refundable' ? 'yellow' : 'primary'}
        testID="rate-plan-tag"
        accessibilityLabel="rate-plan-tag"
      >
        {ratePlanTag}
      </Text>
    );
  }
  return (
    <Text
      size="m"
      weight="normal"
      color={isNRP ? 'yellow' : 'primary'}
      testID="rate-plan-tag"
      accessibilityLabel="rate-plan-tag"
    >
      {isNRP ? 'Non-Refundable' : 'Refundable'}
    </Text>
  );
};

RatePlanTypeView.propTypes = {
  isNRP: PropTypes.bool,
  ratePlanTag: PropTypes.string,
};

RatePlanTypeView.defaultProps = {
  isNRP: false,
};

export default RatePlanTypeView;
