import { handle } from 'redux-pack';
import merge from 'lodash/merge';
import priceService from './priceService';
import apiService from '../api/apiService';

const GET_SEARCH_PRICES = 'GET_SEARCH_PRICES';
const GET_NEARBY_SEARCH_PRICES = 'GET_NEARBY_SEARCH_PRICES';
const GET_ROOM_PRICES = 'GET_ROOM_PRICES';
const GET_CHECKOUT_DETAILS = 'GET_CHECKOUT_DETAILS';
const GET_HOTEL_RESULTS = 'GET_HOTEL_RESULTS';
const APPLY_REMOVE_COUPON = 'APPLY_REMOVE_COUPON';
const APPLY_WALLET = 'APPLY_WALLET';
const REMOVE_WALLET = 'REMOVE_WALLET';

const initialState = {
  results: {
    0: {
      acacia: { isAvailable: true, ratePlans: {} },
      oak: { isAvailable: true, ratePlans: {} },
      maple: { isAvailable: true, ratePlans: {} },
      mahogany: { isAvailable: true, ratePlans: {} },
    },
  },
  availability: {
    0: false,
  },
  isPricesLoading: true,
  isRoomPricesLoading: true,
};

export default (state = initialState, action) => {
  const { type, payload, meta } = action;
  switch (type) {
    case GET_HOTEL_RESULTS:
      return handle(state, action, {
        finish: () => ({ ...initialState }),
      });

    case GET_SEARCH_PRICES:
      return handle(state, action, {
        start: (s) => ({
          ...s,
          results: payload.results,
          availability: payload.availability,
          isPricesLoading: true,
        }),
        success: (s) => ({
          ...s,
          results: {
            ...s.results,
            ...payload.price.results,
          },
          availability: {
            ...s.availability,
            ...payload.availability,
          },
        }),
        finish: (s) => ({ ...s, isPricesLoading: false }),
      });

    case GET_NEARBY_SEARCH_PRICES:
      return handle(state, action, {
        success: (s) => ({
          ...s,
          results: {
            ...s.results,
            ...payload.price.results,
          },
          availability: {
            ...s.availability,
            ...payload.availability,
          },
        }),
      });

    case GET_ROOM_PRICES:
      return handle(state, action, {
        start: (s) => ({ ...s, isRoomPricesLoading: true }),
        success: (s) => ({
          ...s,
          results: {
            ...s.results,
            [meta.startPayload.hotelId]: payload.price,
          },
        }),
        finish: (s) => ({ ...s, isRoomPricesLoading: false }),
      });

    case GET_CHECKOUT_DETAILS:
      return handle(state, action, {
        success: (s) => ({
          ...s,
          results: {
            ...s.results,
            [payload.checkout.hotelId]: {
              ...s.results[0],
              ...s.results[payload.checkout.hotelId],
              ...payload.price,
            },
          },
        }),
      });

    case APPLY_REMOVE_COUPON:
      return handle(state, action, {
        success: (s) => ({
          ...s,
          results: {
            ...s.results,
            [meta.startPayload.hotelId]: {
              ...merge(s.results[meta.startPayload.hotelId], payload.price),
            },
          },
        }),
      });

    case APPLY_WALLET:
    case REMOVE_WALLET:
      return handle(state, action, {
        success: (s) => ({
          ...s,
          results: {
            ...s.results,
            [meta.startPayload.hotelId]: {
              ...merge(s.results[meta.startPayload.hotelId], payload.price),
            },
          },
        }),
      });

    default:
      return state;
  }
};

export const getSearchPrices = (hotelIds) => (dispatch, getState, { api }) => {
  const { search, filter, ui } = getState();
  const searchPricesQuery = apiService.getSearchQuery({
    hotelId: hotelIds.filter((v) => +v).join(','),
    range: search.datePicker.range,
    rooms: search.roomConfig.rooms,
    applyWallet: 'True',
    sort: filter.sort.by,
  });

  return dispatch({
    type: GET_SEARCH_PRICES,
    promise: api.get('/v5/pricing/hotels/', { ...searchPricesQuery, ...ui.utmParams })
      .then(priceService.transformSearchPricesApi),
    payload: { results: initialState.results, availability: initialState.availability },
  });
};

export const getNearbySearchPrices = (hotelId, hotelIds) => (dispatch, getState, { api }) => {
  const { search, ui } = getState();
  const searchPricesQuery = apiService.getSearchQuery({
    hotelId: hotelIds.filter((hId) => hId !== hotelId).join(','),
    range: search.datePicker.range,
    rooms: search.roomConfig.rooms,
    applyWallet: 'True',
  });
  return dispatch({
    type: GET_NEARBY_SEARCH_PRICES,
    promise: api.get('/v5/pricing/hotels/', { ...searchPricesQuery, ...ui.utmParams })
      .then(priceService.transformSearchPricesApi),
  });
};

export const getRoomPrices = (hotelId) => (dispatch, getState, { api }) => {
  const { search: { datePicker: { range }, roomConfig: { rooms } }, ui } = getState();
  const roomPricesQuery = apiService.getSearchQuery({
    range,
    rooms,
    applyWallet: 'True',
  });
  return dispatch({
    type: GET_ROOM_PRICES,
    promise:
      api.get(`/v5/pricing/hotels/${hotelId}/room-prices/`, { ...roomPricesQuery, ...ui.utmParams })
        .then(priceService.transformRoomPricesApi),
    payload: { hotelId },
  });
};
