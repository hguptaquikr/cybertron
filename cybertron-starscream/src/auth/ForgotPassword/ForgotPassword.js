/* eslint-disable global-require */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Keyboard } from 'react-native';
import { connect } from 'react-redux';
import styled from 'styled-components/native';
import { bindActionCreators } from 'redux';
import { TextInput, Form, Button } from 'leaf-ui/native';
import * as userActionCreators from '../../user/userDuck';
import * as toastActionCreators from '../../toast/toastDuck';
import Text from '../../components/NewLeaf/Text/Text';
import Loader from '../../components/Loader/Loader';
import LoginPageLayout from '../LoginPage/LoginPageLayout';
import color from '../../theme/color';
import { emailRegex, isValidEmail } from '../../services/inputValidationService';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const Container = styled.View`
  flex: 1;
`;

const InputFieldsContainer = styled.View`
  flex: 1;
`;

const SuccessMessageContainer = styled.View`
  flex: 1;
  justifyContent: center;
  alignItems: center;
`;

const EmailSentImage = styled.Image`
  width: 128;
  height: 128;
  marginBottom: 40;
`;

const ResetLinkInfoContainer = styled.View`
  marginTop: 16;
  alignItems: center;
`;

const ResendEmailContainer = styled.View`
  paddingVertical: 24
  paddingHorizontal: 32;
  flexDirection: row;
  justifyContent: center;
`;

const ActionsContainer = styled.View``;

const Touchable = styled.TouchableOpacity`
  marginHorizontal: 4;
`;

const ClearButton = styled.TouchableOpacity`
  width: 48;
  height: 48;
  alignItems: center;
  justifyContent: center;
`;

const ErrorContainer = styled.View`
  paddingVertical: 16
  paddingHorizontal: 32;
  backgroundColor: ${color.lightGrey};
  flexDirection: row;
  justifyContent: center;
`;

const LoaderContainer = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
`;

class ForgotPassword extends Component {
  state = {
    showSuccessMsg: false,
    showErrorMsg: false,
    showSpinner: false,
  };

  onForgotPassword = (email) => {
    const { userActions: { forgotPassword } } = this.props;

    Keyboard.dismiss();
    this.setState({ showSpinner: true });
    return forgotPassword(email)
      .then((res) => {
        if (res.error) {
          this.showErrorMsg();
        } else {
          this.showSuccessMsg();
        }
      })
      .catch(() => this.showErrorMsg());
  };

  onEditEmail = () => {
    this.setState({
      showSpinner: false,
      showSuccessMsg: false,
      showErrorMsg: false,
    });
  };

  resendEmail = () => {
    const {
      user: { profile },
      userActions: { forgotPassword },
      toastActions: { showToast },
    } = this.props;

    this.setState({ showSpinner: true });
    forgotPassword(profile.email)
      .then((res) => {
        if (res.error) {
          this.showErrorMsg();
        } else {
          this.setState({ showSpinner: false });
          showToast('Email Sent');
        }
      })
      .catch(() => {
        this.showErrorMsg();
      });
  };

  showErrorMsg = () => {
    this.setState({
      showSpinner: false,
      showSuccessMsg: false,
      showErrorMsg: true,
    });
  };

  showSuccessMsg = () => {
    this.setState({
      showSpinner: false,
      showSuccessMsg: true,
      showErrorMsg: false,
    });
  };

  renderSuccessContainer = () => {
    const { showSpinner } = this.state;
    const { user: { profile }, onDoneResetPassword } = this.props;
    return (
      <Container>
        <SuccessMessageContainer>
          <EmailSentImage
            source={require('../login_email_sent.png')}
          />
          <Text color="greyDarker" weight="bold" size="l">
            Email Sent
          </Text>
          <ResetLinkInfoContainer>
            <Text size="s" color="greyDark" weight="normal">
              A link to reset your password has been sent to
            </Text>
            <Text size="s" color="greyDark" weight="normal">
              {`${profile.email} `}
              <Text
                size="s"
                color="blue"
                weight="medium"
                onPress={this.onEditEmail}
              >
                Edit
              </Text>
            </Text>
          </ResetLinkInfoContainer>
        </SuccessMessageContainer>
        <ActionsContainer>
          <ResendEmailContainer>
            <Text size="s" color="greyDark" weight="normal">
              Didn’t receive the email?
            </Text>
            <Touchable onPress={this.resendEmail}>
              <Text
                size="s"
                color="blue"
                weight="normal"
                family="medium"
              >
                Resend
              </Text>
            </Touchable>
          </ResendEmailContainer>
          <LoginPageLayout.ButtonSection>
            <Button
              block
              onPress={onDoneResetPassword}
            >
              <Text size="m" weight="medium" family="medium" color="white">
                DONE
              </Text>
            </Button>
          </LoginPageLayout.ButtonSection>
        </ActionsContainer>
        {
          showSpinner ? (
            <LoaderContainer>
              <Loader
                backgroundColor={color.transparent}
                loaderSize={50}
              />
            </LoaderContainer>
          ) : null
        }
      </Container>
    );
  };

  renderEmailInputContainer = () => {
    const { showSpinner } = this.state;
    const { showErrorMsg } = this.state;
    return (
      <Form
        validationSchema={
          Form.validation.object().shape({
            email: Form.validation.string().required()
              .matches(emailRegex, { message: 'Please enter a valid email address' }),
          })
        }
        onSubmit={(values, actions) => {
          actions.setSubmitting(true);
          this.onForgotPassword(values.email)
            .then(() => actions.setSubmitting(false));
        }}
        render={(formProps) => (
          <Container>
            <InputFieldsContainer>
              <LoginPageLayout.InputSection>
                <Text size="m" color="black" weight="normal">
                  Please enter your registered email to receive
                  a verification link to reset your password.
                </Text>
                <TextInput
                  name="email"
                  label="Email Id"
                  error="Please enter a valid email address"
                  keyboardType="email-address"
                  autoCapitalize="none"
                  rightIcon={{
                    component: (
                      <ClearButton onPress={() => formProps.setFieldValue('email', '')}>
                        {getIcon('reset-input', color.disabledGrey, 24, 16)}
                      </ClearButton>
                    ),
                    showOnlyOnFocus: true,
                  }}
                  autoCorrect={false}
                  testID="enter-email-address-field"
                  accessibilityLabel="enter-email-address-field"
                  autoFocus
                />
              </LoginPageLayout.InputSection>
            </InputFieldsContainer>
            <ActionsContainer>
              {
                showErrorMsg ? (
                  <ErrorContainer>
                    <Text size="s" color="greyDarker" weight="normal">
                      Error sending email. Try Again
                    </Text>
                  </ErrorContainer>
                ) : null
              }
              <LoginPageLayout.ButtonSection>
                <Button
                  block
                  disabled={!isValidEmail(formProps.values.email) || formProps.isSubmitting}
                  onPress={formProps.handleSubmit}
                >
                  <Text size="m" weight="medium" family="medium" color="white">
                    SEND EMAIL
                  </Text>
                </Button>
              </LoginPageLayout.ButtonSection>
            </ActionsContainer>
            {
              showSpinner ? (
                <LoaderContainer>
                  <Loader
                    backgroundColor={color.transparent}
                    loaderSize={50}
                  />
                </LoaderContainer>
              ) : null
            }
          </Container>
        )}
      />
    );
  };

  render() {
    const { showSuccessMsg } = this.state;
    if (showSuccessMsg) {
      return this.renderSuccessContainer();
    }
    return this.renderEmailInputContainer();
  }
}

ForgotPassword.propTypes = {
  user: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired,
  toastActions: PropTypes.object.isRequired,
  onDoneResetPassword: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
