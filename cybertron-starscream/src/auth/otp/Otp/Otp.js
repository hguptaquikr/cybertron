import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Keyboard } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import get from 'lodash/get';
import styled from 'styled-components/native';
import * as toastActionCreators from '../../../toast/toastDuck';
import Loader from '../../../components/Loader/Loader';
import OtpVerify from '../OtpVerify/OtpVerify';
import OtpEditNumber from '../OtpEditNumber/OtpEditNumber';
import color from '../../../theme/color';

const Container = styled.View`
  flex: 1;
`;

const LoaderContainer = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
`;

class Otp extends Component {
  state = {
    flow: 'EnterOtp',
    showSpinner: false,
  };

  onEditMobile = () => {
    this.setState({ flow: 'ChangeMobile' });
  };

  onMobileChanged = (mobile, invalidate) => {
    const {
      onMobileChanged,
      toastActions: { showToast },
    } = this.props;

    Keyboard.dismiss();
    this.setState({ showSpinner: true });
    return onMobileChanged(mobile)
      .then((res) => {
        this.setState({ showSpinner: false });
        if (res.error) {
          if (invalidate) {
            invalidate(res.payload.errors[0].message);
          } else {
            showToast(res.payload.errors[0].message);
          }
        } else {
          this.setState({ flow: 'EnterOtp' });
        }
      })
      .catch(() => {
        this.setState({ showSpinner: false });
        if (invalidate) {
          invalidate('Something went wrong. Please try again');
        } else {
          showToast('Something went wrong. Please try again');
        }
      });
  };

  onResendOtp = (invalidate) => () => {
    const {
      mobile,
      onResendOtp,
      toastActions: { showToast },
    } = this.props;

    this.setState({ showSpinner: true });
    onResendOtp(mobile)
      .then((res) => {
        this.setState({ showSpinner: false });
        if (res.error) {
          if (invalidate) {
            invalidate(res.payload.errors[0].message);
          } else {
            showToast(res.payload.errors[0].message);
          }
        } else {
          showToast(`OTP has been sent to ${mobile}`);
        }
      })
      .catch(() => {
        this.setState({ showSpinner: false });
        if (invalidate) {
          invalidate('Error in sending Otp, Try again.');
        } else {
          showToast('Error in sending Otp, Try again.');
        }
      });
  };

  onVerifyOtp = (otp, invalidate) => () => {
    const {
      onLoginSuccess,
      otpSuccess,
      mobile,
      onVerifyOtp,
      toastActions: { showToast },
    } = this.props;

    Keyboard.dismiss();
    this.setState({ showSpinner: true });
    return onVerifyOtp(mobile, otp)
      .then((res) => {
        this.setState({ showSpinner: false });
        if (res.error || !get(res.payload, 'data.is_valid', true)) {
          let errorMessage = 'OTP not verified';
          if (res.payload.errors) {
            errorMessage = res.payload.errors[0].message;
          }
          if (invalidate) {
            invalidate(errorMessage);
          } else {
            showToast(errorMessage);
          }
        } else {
          if (onLoginSuccess) {
            onLoginSuccess(res.payload.profile);
          }
          if (otpSuccess) {
            otpSuccess(res);
          }
        }
      })
      .catch(() => {
        this.setState({ showSpinner: false });
        if (invalidate) {
          invalidate('Something went wrong. Please try again');
        } else {
          showToast('Something went wrong. Please try again');
        }
      });
  };

  render() {
    const { flow, showSpinner } = this.state;
    const {
      mobile,
    } = this.props;

    return (
      <Container
        testID="otp-modal"
        accessibilityLabel="otp-modal"
      >
        {
          flow === 'EnterOtp' ? (
            <OtpVerify
              mobile={mobile}
              onEditMobile={this.onEditMobile}
              onResendOtp={this.onResendOtp}
              onVerifyOtp={this.onVerifyOtp}
            />
          ) : null
        }
        {
          flow === 'ChangeMobile' ? (
            <OtpEditNumber
              mobile={mobile}
              onMobileChanged={this.onMobileChanged}
            />
          ) : null
        }
        {
          showSpinner ? (
            <LoaderContainer>
              <Loader
                backgroundColor={color.transparent}
                loaderSize={50}
              />
            </LoaderContainer>
          ) : null
        }
      </Container>
    );
  }
}

Otp.propTypes = {
  mobile: PropTypes.string,
  toastActions: PropTypes.object.isRequired,
  otpSuccess: PropTypes.func,
  onLoginSuccess: PropTypes.func,
  onMobileChanged: PropTypes.func.isRequired,
  onResendOtp: PropTypes.func.isRequired,
  onVerifyOtp: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  toastActions: bindActionCreators(toastActionCreators, dispatch),
});

export default connect(null, mapDispatchToProps)(Otp);
