/* eslint-disable import/prefer-default-export */
import { requestSmsPermission } from '../../permissions/androidPermissionService';
import { autoReadOTP } from '../../nativeModules';

export const autoReadOtp = async () => {
  try {
    const granted = await requestSmsPermission();
    if (granted) {
      const otp = await autoReadOTP();
      return Promise.resolve(otp);
    }
    return Promise.reject('permission not granted');
  } catch (error) {
    return Promise.reject(error);
  }
};
