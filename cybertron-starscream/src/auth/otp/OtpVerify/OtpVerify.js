import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Platform } from 'react-native';
import { TextInput, Form, Button } from 'leaf-ui/native';
import Text from '../../../components/NewLeaf/Text/Text';
import LoginPageLayout from '../../../auth/LoginPage/LoginPageLayout';
import color from '../../../theme/color';
import { otpRegex, isValidOtp } from '../../../services/inputValidationService';
import { autoReadOtp } from '../readOtpService';
import { getIcon } from '../../../utils/ComponentUtils/ComponentUtils';

const Container = styled.View`
  flex: 1;
`;

const InputContainer = styled.View`
  flex: 1;
`;

const ActionsContainer = styled.View``;

const TitleContainer = styled.View`
  marginBottom: 36;
  flexDirection: row;
  alignItems: center;
`;

const ResendOtpContainer = styled.View`
  marginBottom: 4;
  paddingHorizontal: 16;
  flexDirection: row;
  alignItems: center;
`;

const Touchable = styled.TouchableOpacity`
  marginHorizontal: 4;
`;

const ClearButton = styled.TouchableOpacity`
  width: 48;
  height: 48;
  alignItems: center;
  justifyContent: center;
`;

class OtpVerify extends Component {
  state = {
    otp: '',
    verifyButtonText: 'VERIFY',
  };

  componentWillMount() {
    if (Platform.OS === 'android') {
      autoReadOtp().then((otp) => {
        this.setState({ otp, verifyButtonText: 'VERIFYING' });
        this.props.onVerifyOtp(otp, this.invalidate)();
      });
    }
  }

  clearOtpField = (setFieldValue) => () => {
    setFieldValue('otp', '');
    this.setState({ otp: '' });
  };

  invalidate = (setFieldError) => (errorMessage) => {
    setFieldError('otp', errorMessage);
    this.setState({ verifyButtonText: 'VERIFY' });
  };

  render() {
    const {
      mobile,
      onEditMobile,
      onResendOtp,
      onVerifyOtp,
    } = this.props;
    const { otp, verifyButtonText } = this.state;
    return (
      <Form
        initialValues={{ otp }}
        validationSchema={
          Form.validation.object().shape({
            otp: Form.validation.string().required('OTP is required').matches(otpRegex, { message: 'Please enter a valid OTP' }),
          })
        }
        onSubmit={(values, actions) => {
          actions.setSubmitting(true);
          this.setState({ verifyButtonText: 'VERIFYING' });
          onVerifyOtp(values.otp, this.invalidate(actions.setFieldError))()
            .then(() => actions.setSubmitting(false));
        }}
        render={(formProps) => (
          <Container>
            <InputContainer>
              <LoginPageLayout.InputSection>
                <TitleContainer>
                  <Text size="m" color="black" weight="normal">
                    OTP sent to +91 {mobile}.
                  </Text>
                  <Touchable onPress={onEditMobile}>
                    <Text size="s" color="blue" weight="normal">
                      Edit
                    </Text>
                  </Touchable>
                </TitleContainer>
                <TextInput
                  name="otp"
                  label="OTP"
                  error="Please enter a valid OTP"
                  keyboardType="numeric"
                  maxLength={6}
                  rightIcon={{
                    component: (
                      <ClearButton onPress={this.clearOtpField(formProps.setFieldValue)}>
                        {getIcon('reset-input', color.disabledGrey, 24, 16)}
                      </ClearButton>
                    ),
                    showOnlyOnFocus: true,
                  }}
                  autoCorrect={false}
                  testID="enter-otp-verify-field"
                  accessibilityLabel="enter-otp-verify-field"
                  autoFocus
                />
              </LoginPageLayout.InputSection>
            </InputContainer>
            <ActionsContainer>
              <ResendOtpContainer>
                <Text size="s" color="grey" weight="normal">
                  Haven’t received the code yet?
                </Text>
                <Touchable
                  onPress={onResendOtp(this.invalidate)}
                  testID="resend-button"
                  accessibilityLabel="resend-button"
                >
                  <Text size="s" color="blue" weight="normal">
                    Resend OTP
                  </Text>
                </Touchable>
              </ResendOtpContainer>
              <LoginPageLayout.ButtonSection>
                <Button
                  block
                  disabled={!isValidOtp(formProps.values.otp) || formProps.isSubmitting}
                  onPress={formProps.handleSubmit}
                  testID="verify-button"
                  accessibilityLabel="verify-button"
                >
                  <Text size="m" weight="medium" family="medium" color="white">
                    {verifyButtonText}
                  </Text>
                </Button>
              </LoginPageLayout.ButtonSection>
            </ActionsContainer>
          </Container>
        )}
      />
    );
  }
}

OtpVerify.propTypes = {
  mobile: PropTypes.string,
  onEditMobile: PropTypes.func.isRequired,
  onResendOtp: PropTypes.func.isRequired,
  onVerifyOtp: PropTypes.func.isRequired,
};

export default OtpVerify;
