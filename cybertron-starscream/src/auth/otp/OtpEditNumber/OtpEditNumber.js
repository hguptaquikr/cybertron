import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { TextInput, Form, Button } from 'leaf-ui/native';
import Text from '../../../components/NewLeaf/Text/Text';
import LoginPageLayout from '../../../auth/LoginPage/LoginPageLayout';
import { mobileRegex, isValidMobile } from '../../../services/inputValidationService';
import color from '../../../theme/color';
import { getIcon } from '../../../utils/ComponentUtils/ComponentUtils';

const Container = styled.View`
  flex: 1;
`;
const InputContainer = LoginPageLayout.InputSection.extend`
  flex: 1;
`;
const ClearButton = styled.TouchableOpacity`
  width: 48;
  height: 48;
  alignItems: center;
  justifyContent: center;
`;

class OtpEditNumber extends Component {
  invalidate = (setFieldError) => (errorMessage) => {
    setFieldError('mobile', errorMessage);
  };

  render() {
    const { onMobileChanged } = this.props;
    return (
      <Form
        initialValues={{
          mobile: this.props.mobile,
        }}
        validationSchema={
          Form.validation.object().shape({
            mobile: Form.validation.string()
              .required().matches(mobileRegex, { message: 'Please enter a valid mobile number' }),
          })
        }
        onSubmit={(values, actions) => {
          actions.setSubmitting(true);
          onMobileChanged(values.mobile, this.invalidate(actions.setFieldError))
            .then(() => actions.setSubmitting(false));
        }}
        render={(formProps) => (
          <Container>
            <InputContainer>
              <Text size="m" color="greyDarker" weight="bold">
                Enter Mobile Number
              </Text>
              <TextInput
                name="mobile"
                label="Mobile"
                error="Please enter a valid mobile number"
                keyboardType="numeric"
                maxLength={10}
                rightIcon={{
                  component: (
                    <ClearButton onPress={() => formProps.setFieldValue('mobile', '')}>
                      {getIcon('reset-input', color.disabledGrey, 24, 16)}
                    </ClearButton>
                  ),
                  showOnlyOnFocus: true,
                }}
                autoCorrect={false}
                testID="enter-phone-number-field"
                accessibilityLabel="enter-phone-number-field"
              />
            </InputContainer>
            <LoginPageLayout.ButtonSection>
              <Button
                block
                disabled={!isValidMobile(formProps.values.mobile) || formProps.isSubmitting}
                onPress={formProps.handleSubmit}
                testID="verify-button"
                accessibilityLabel="verify-button"
              >
                <Text size="m" weight="medium" family="medium" color="white">
                  VERIFY
                </Text>
              </Button>
            </LoginPageLayout.ButtonSection>
          </Container>
        )}
      />
    );
  }
}

OtpEditNumber.propTypes = {
  mobile: PropTypes.string,
  onMobileChanged: PropTypes.func.isRequired,
};

export default OtpEditNumber;
