import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Keyboard } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components/native';
import { TextInput, Form, Button } from 'leaf-ui/native';
import * as userActionCreators from '../../user/userDuck';
import Text from '../../components/NewLeaf/Text/Text';
import Loader from '../../components/Loader/Loader';
import LoginPageLayout from '../LoginPage/LoginPageLayout';
import color from '../../theme/color';
import { emailRegex, isValidEmail, isValidPassword } from '../../services/inputValidationService';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const Container = styled.View`
  flex: 1;
`;

const InputFieldsContainer = styled.View`
  flex: 1;
`;

const ActionsContainer = styled.View``;

const Touchable = styled.TouchableOpacity``;

const ClearButton = styled.TouchableOpacity`
  width: 48;
  height: 48;
  alignItems: center;
  justifyContent: center;
`;

const ErrorContainer = styled.View`
  paddingVertical: 16
  paddingHorizontal: 32;
  backgroundColor: ${color.lightGrey};
  flexDirection: row;
`;

const LoaderContainer = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
`;

class EmailLogin extends Component {
  state = {
    isCredentialsValid: true,
    errorMessage: '',
    showPassword: false,
    showSpinner: false,
  };

  onUserLogin = (email, password) => {
    const {
      onLoginSuccess,
      userActions: { emailLogin },
    } = this.props;

    const credentials = {
      email,
      password,
    };

    Keyboard.dismiss();
    this.setState({ showSpinner: true });
    return emailLogin(credentials)
      .then((res) => {
        this.setState({ showSpinner: false });
        if (res.error) {
          this.setState({
            isCredentialsValid: false,
            errorMessage: res.payload.errors[0].message,
          });
        } else {
          onLoginSuccess(res.payload.profile);
        }
      })
      .catch(() => this.setState({ isCredentialsValid: false, showSpinner: false }));
  };

  onShowMobileNumberLogin = () => {
    const { onShowMobileNumberLogin } = this.props;
    onShowMobileNumberLogin();
  };

  togglePasswordVisibility = () => {
    const { showPassword } = this.state;
    this.setState({ showPassword: !showPassword });
  };

  render() {
    const {
      showPassword,
      isCredentialsValid,
      errorMessage,
      showSpinner,
    } = this.state;
    const { onShowForgotPasswordScreen } = this.props;
    return (
      <Form
        validationSchema={
          Form.validation.object().shape({
            email: Form.validation.string().required()
              .matches(emailRegex, { message: 'Please enter a valid email address' }),
            password: Form.validation.string().required(),
          })
        }
        onSubmit={(values, actions) => {
          actions.setSubmitting(true);
          this.onUserLogin(values.email, values.password)
            .then(() => actions.setSubmitting(false));
        }}
        render={(formProps) => (
          <Container>
            <InputFieldsContainer>
              <LoginPageLayout.InputSection>
                <TextInput
                  name="email"
                  label="Email Id"
                  error="Please enter a valid email address"
                  keyboardType="email-address"
                  autoCapitalize="none"
                  rightIcon={{
                    component: (
                      <ClearButton onPress={() => formProps.setFieldValue('email', '')}>
                        {getIcon('reset-input', color.disabledGrey, 24, 16)}
                      </ClearButton>
                    ),
                    showOnlyOnFocus: true,
                  }}
                  autoCorrect={false}
                  testID="enter-email-address-field"
                  accessibilityLabel="enter-email-address-field"
                />
                <TextInput
                  name="password"
                  label="Password"
                  error="Please enter a valid password"
                  autoCapitalize="none"
                  secureTextEntry={!showPassword}
                  rightIcon={{
                    component: (
                      <ClearButton onPress={this.togglePasswordVisibility}>
                        {getIcon(showPassword ? 'eye-off' : 'eye', color.disabledGrey, 24, 24)}
                      </ClearButton>
                    ),
                    showOnlyOnFocus: false,
                  }}
                  autoCorrect={false}
                  testID="enter-password-field"
                  accessibilityLabel="enter-password-field"
                />
              </LoginPageLayout.InputSection>
              <LoginPageLayout.InputSection>
                <Touchable onPress={onShowForgotPasswordScreen}>
                  <Text color="blue" weight="medium" family="medium" size="s">
                    Forgot your password?
                  </Text>
                </Touchable>
              </LoginPageLayout.InputSection>
            </InputFieldsContainer>
            <ActionsContainer>
              {
                !isCredentialsValid ? (
                  <ErrorContainer>
                    <Text color="grey" weight="normal" size="s">
                      {errorMessage}
                    </Text>
                    <Touchable onPress={this.onShowMobileNumberLogin}>
                      <Text color="blue" weight="normal" size="s">
                        Continue with mobile no.
                      </Text>
                    </Touchable>
                  </ErrorContainer>
                ) : null
              }
              <LoginPageLayout.ButtonSection>
                <Button
                  block
                  disabled={
                    !isValidEmail(formProps.values.email)
                    || !isValidPassword(formProps.values.password)
                    || formProps.isSubmitting
                  }
                  onPress={formProps.handleSubmit}
                  testID="continue-button"
                  accessibilityLabel="continue-button"
                >
                  <Text weight="medium" family="medium" color="white" size="m">
                    CONTINUE
                  </Text>
                </Button>
              </LoginPageLayout.ButtonSection>
            </ActionsContainer>
            {
              showSpinner ? (
                <LoaderContainer>
                  <Loader
                    backgroundColor={color.transparent}
                    loaderSize={50}
                  />
                </LoaderContainer>
              ) : null
            }
          </Container>
        )}
      />
    );
  }
}

EmailLogin.propTypes = {
  userActions: PropTypes.object.isRequired,
  onLoginSuccess: PropTypes.func.isRequired,
  onShowForgotPasswordScreen: PropTypes.func.isRequired,
  onShowMobileNumberLogin: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
});

export default connect(null, mapDispatchToProps)(EmailLogin);
