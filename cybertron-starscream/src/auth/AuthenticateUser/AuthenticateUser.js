import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components/native';
import * as userActionCreators from '../../user/userDuck';
import MobileLogin from '../MobileLogin/MobileLogin';
import Otp from '../otp/Otp/Otp';
import EmailLogin from '../EmailLogin/EmailLogin';
import ForgotPassword from '../ForgotPassword/ForgotPassword';
import analyticsService from '../../analytics/analyticsService';

const Container = styled.View`
  flex: 1;
`;

class AuthenticateUser extends Component {
  state = {
    flow: 'Mobile',
  };

  onFlowChange = (flow) => () => {
    this.setState({ flow });
    const { onFlowChange } = this.props;
    if (onFlowChange) {
      onFlowChange(flow);
    }
  };

  onLoginSuccess = (profile) => {
    const { onLoginSuccess } = this.props;
    const { mobile, userId } = profile;
    const propertiesToTrack = { name: profile.name, email: profile.email, phone: `+91${mobile}`, type: this.state.flow };
    analyticsService.segmentTrackEvent('Login Success', propertiesToTrack);
    analyticsService.segmentIdentify(userId, propertiesToTrack);
    onLoginSuccess(profile);
  };

  render() {
    const { flow } = this.state;
    const {
      mobile,
      userActions: {
        updateLoginMobile,
        sendLoginOtp,
        verifyLoginOtp,
      },
    } = this.props;
    let template = null;
    if (flow === 'Mobile') {
      template = (
        <MobileLogin
          onLoginSuccess={this.onLoginSuccess}
          onSendOtp={this.onFlowChange('EnterOtp')}
          onShowEmailScreen={this.onFlowChange('Email')}
        />
      );
    } else if (flow === 'EnterOtp') {
      template = (
        <Otp
          mobile={mobile}
          onLoginSuccess={this.onLoginSuccess}
          onMobileChanged={updateLoginMobile}
          onResendOtp={sendLoginOtp}
          onVerifyOtp={verifyLoginOtp}
        />
      );
    } else if (flow === 'Email') {
      template = (
        <EmailLogin
          onLoginSuccess={this.onLoginSuccess}
          onShowMobileNumberLogin={this.onFlowChange('Mobile')}
          onShowForgotPasswordScreen={this.onFlowChange('ForgotPassword')}
        />
      );
    } else if (flow === 'ForgotPassword') {
      template = (
        <ForgotPassword onDoneResetPassword={this.onFlowChange('Email')} />
      );
    }
    // TODO: Add other flows later
    return (
      <Container>
        {template}
      </Container>
    );
  }
}

AuthenticateUser.propTypes = {
  mobile: PropTypes.string,
  onFlowChange: PropTypes.func,
  onLoginSuccess: PropTypes.func.isRequired,
  userActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  mobile: state.user && state.user.profile ? state.user.profile.mobile : '',
});

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthenticateUser);
