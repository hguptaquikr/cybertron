import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Keyboard } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components/native';
import { TextInput, Form, Button } from 'leaf-ui/native';
import * as userActionCreators from '../../user/userDuck';
import * as toastActionCreators from '../../toast/toastDuck';
import Text from '../../components/NewLeaf/Text/Text';
import Loader from '../../components/Loader/Loader';
import LoginPageLayout from '../LoginPage/LoginPageLayout';
import color from '../../theme/color';
import { mobileRegex, isValidMobile } from '../../services/inputValidationService';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const Container = styled.View`
  flex: 1;
`;

const EmailLoginContainer = styled.View`
  paddingVertical: 20;
  paddingHorizontal: 24;
  justifyContent: center;
  flexDirection: row;
`;

const Touchable = styled.TouchableOpacity`
  marginHorizontal: 4;
`;

const ClearButton = styled.TouchableOpacity`
  width: 48;
  height: 48;
  alignItems: center;
  justifyContent: center;
`;

const ContinueButtonContainer = LoginPageLayout.ButtonSection.extend`
  marginVertical: 16;
`;

const LoaderContainer = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
`;

class MobileLogin extends Component {
  state = { showSpinner: false };

  onShowEmailScreen =() => {
    const { onShowEmailScreen } = this.props;
    onShowEmailScreen();
  };

  sendOtp = (mobile) => {
    const {
      onSendOtp,
      userActions: { sendLoginOtp },
      toastActions: { showToast },
    } = this.props;

    Keyboard.dismiss();
    this.setState({ showSpinner: true });
    return sendLoginOtp(mobile)
      .then((res) => {
        this.setState({ showSpinner: false });
        if (res.error) {
          showToast('Error in sending Otp, Try again.');
        } else {
          onSendOtp();
        }
      })
      .catch(() => {
        this.setState({ showSpinner: false });
        showToast('Error in sending Otp, Try again.');
      });
  };

  render() {
    const { showSpinner } = this.state;
    return (
      <Container>
        <Form
          validationSchema={
            Form.validation.object().shape({
              mobile: Form.validation.string()
                .required('Mobile Number is required').matches(mobileRegex, { message: 'Please enter a valid mobile number' }),
            })
          }
          onSubmit={(values, actions) => {
            actions.setSubmitting(true);
            this.sendOtp(values.mobile)
              .then(() => actions.setSubmitting(false));
          }}
          render={(formProps) => (
            <Container>
              <LoginPageLayout.InputSection>
                <Text size="m" color="greyDark" family="medium" weight="bold">
                  Continue with your mobile number
                </Text>
                <TextInput
                  name="mobile"
                  label="Mobile"
                  error="Please enter a valid mobile number"
                  keyboardType="numeric"
                  maxLength={10}
                  rightIcon={{
                    component: (
                      <ClearButton onPress={() => formProps.setFieldValue('mobile', '')}>
                        {getIcon('reset-input', color.disabledGrey, 24, 16)}
                      </ClearButton>
                    ),
                    showOnlyOnFocus: true,
                  }}
                  autoCorrect={false}
                  testID="enter-phone-number-field"
                  accessibilityLabel="enter-phone-number-field"
                />
              </LoginPageLayout.InputSection>
              {
                formProps.values.mobile && formProps.values.mobile.length > 0 ? (
                  <ContinueButtonContainer>
                    <Button
                      block
                      disabled={!isValidMobile(formProps.values.mobile) || formProps.isSubmitting}
                      onPress={formProps.handleSubmit}
                      testID="confirm-button"
                      accessibilityLabel="confirm-button"
                    >
                      <Text size="m" weight="medium" family="medium" color="white">
                        CONFIRM
                      </Text>
                    </Button>
                  </ContinueButtonContainer>
                ) : null
              }
            </Container>
          )}
        />
        <EmailLoginContainer>
          <Text size="s" color="grey" weight="normal">
            Use your Email Address to
          </Text>
          <Touchable onPress={this.onShowEmailScreen}>
            <Text
              size="s"
              weight="medium"
              family="medium"
              color="blue"
              testID="email-signin-button"
              accessibilityLabel="email-signin-button"
            >
              Sign In
            </Text>
          </Touchable>
        </EmailLoginContainer>
        {
          showSpinner ? (
            <LoaderContainer>
              <Loader
                backgroundColor={color.transparent}
                loaderSize={50}
              />
            </LoaderContainer>
          ) : null
        }
      </Container>
    );
  }
}

MobileLogin.propTypes = {
  onSendOtp: PropTypes.func.isRequired,
  userActions: PropTypes.object.isRequired,
  toastActions: PropTypes.object.isRequired,
  onShowEmailScreen: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(MobileLogin);
