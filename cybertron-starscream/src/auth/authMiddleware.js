import userService from '../user/userService';
import { noInternetConnection } from '../content/noInternetConnectionDuck';

export default ({ dispatch }) => (next) => (action) => {
  if (action.error && action.payload.status === 401) {
    // TODO: redirect to login page
    return userService.unsetAuthData()
      .then(dispatch({ type: 'UNSET_USER_PROFILE' }));
  }
  if (action.error && action.payload.message === 'Network request failed') {
    return dispatch(noInternetConnection(true));
  }
  return next(action);
};
