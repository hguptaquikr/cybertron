/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import color from '../../theme/color';
import Text from '../../components/NewLeaf/Text/Text';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import WalletBalanceBanner from '../../wallet/WalletBalanceBanner/WalletBalanceBanner';
import TouchableComponent from '../../components/Touchable/TouchableComponent';

const BannerBackgroundView = styled.View`
  borderRadius: 2;
  marginBottom: 8;
  marginHorizontal: 8;
  padding: 8px;
  backgroundColor: ${({ backgroundColor = color.lightBlue }) => backgroundColor};
  flexDirection: row;
  alignItems: center;
  borderWidth: 0.5;
  borderColor: ${({ borderColor = color.darkBlue }) => borderColor};
`;

const BannerText = styled.View`
  flex: 1;
  marginLeft: 8;
  paddingVertical: ${({ padding = 0 }) => padding};
`;

const ButtonContainer = styled.View`
  paddingHorizontal: 12px;
  paddingVertical: 8px;
  alignItems: center;
  borderRadius: 3;
  backgroundColor: ${color.blue};
`;

const UnlockSpecialDealBanner = ({ onUnlockPress, style, isLoggedIn }) => (
  !isLoggedIn ? (
    <BannerBackgroundView
      style={style}
      testID="special-deal-banner"
      accessibilityLabel="special-deal-banner"
    >
      {getIcon('special_deal')}
      <BannerText>
        <Text size="s" color="blue" weight="medium">Get discounted member rates</Text>
      </BannerText>
      <TouchableComponent
        onPress={onUnlockPress}
        testID="login-button"
        accessibilityLabel="login-button"
      >
        <ButtonContainer>
          <Text size="s" color="white" weight="normal">
            LOGIN
          </Text>
        </ButtonContainer>
      </TouchableComponent>
    </BannerBackgroundView>
  ) : (
    <WalletBalanceBanner />
  )
);

UnlockSpecialDealBanner.propTypes = {
  style: PropTypes.array,
  onUnlockPress: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
};

export default UnlockSpecialDealBanner;
