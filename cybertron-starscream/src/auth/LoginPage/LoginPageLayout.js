import styled from 'styled-components/native';

export default {
  InputSection: styled.View`
    paddingHorizontal: 16;
    paddingTop: 24;
  `,
  ButtonSection: styled.View`
    paddingHorizontal: 16;
    paddingVertical: 8;
  `,
};
