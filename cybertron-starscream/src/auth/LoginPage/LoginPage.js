/* eslint-disable global-require */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StatusBar, Platform, Dimensions, BackHandler } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-native';
import { Spacer, theme } from 'leaf-ui/native';
import isEmpty from 'lodash/isEmpty';
import styled from 'styled-components/native';
import * as toastActionCreators from '../../toast/toastDuck';
import * as contentActionCreators from '../../content/contentDuck';
import * as walletActionCreators from '../../wallet/walletDuck';
import BackButtonHeader from '../../components/Header/BackButtonHeader';
import AuthenticateUser from '../AuthenticateUser/AuthenticateUser';
import ImageLoad from '../../components/ImageLoad/ImageLoad';
import Content from '../../components/Content/Content';
import List from '../../components/List/List';
import PagerDotIndicator from '../../components/PagerIndicator/PagerDotIndicator';
import Text from '../../components/NewLeaf/Text/Text';
import color from '../../theme/color';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const windowWidth = Dimensions.get('window').width;

const Container = styled.View`
  flex: 1;
`;

const Touchable = styled.TouchableOpacity`
  padding: 4px;
  marginLeft: 8;
  justifyContent: center;
  alignItems: center;
  width: 32;
  height: 32;
`;

const HeaderContainer = styled.View`
  paddingTop: 24;
  backgroundColor: ${theme.color.teal};
`;

const HeaderCarouselItemContainer = styled.View`
  width: ${windowWidth};
  alignItems: center;
  justifyContent: center;
`;

const HeaderImage = styled(ImageLoad)`
  borderRadius: 0;
  width: 100;
`;

const PageIndicatorContainer = styled.View`
  paddingVertical: 12;
  paddingHorizontal: 16;
  justifyContent: center;
  alignItems: center;
`;

class LoginPage extends Component {
  state = {
    flow: 'Mobile',
    currentHeaderPage: 1,
  };

  componentDidMount() {
    const { contentActions } = this.props;
    contentActions.getLoginPageHeader();
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  onBackPress = () => {
    const { history } = this.props;
    history.goBack();
    return true;
  };

  onFlowChange = (flow) => this.setState({ flow });

  onLoginSuccess = (profile) => {
    const { onLoginSuccess, history, toastActions, walletActions } = this.props;

    walletActions.getWalletBalance();
    if (onLoginSuccess) {
      onLoginSuccess(profile);
    } else {
      setTimeout(() => {
        const { firstName, email, phone } = profile;
        const toastLabel = firstName || email || phone;
        toastActions.showToast(`Logged in as ${toastLabel}`);
      }, 700);
      history.goBack();
    }
  };

  getHeaderView = () => {
    const { flow, currentHeaderPage } = this.state;
    const { onClose, history, loginPageHeader } = this.props;

    if (isEmpty(loginPageHeader) || flow !== 'Mobile') {
      if (Platform.OS === 'ios') {
        StatusBar.setBarStyle('dark-content');
      } else {
        StatusBar.setBackgroundColor(color.black);
      }
      return (
        <BackButtonHeader
          iconProps={{ name: 'close' }}
          onLeftPress={onClose}
        />
      );
    }

    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('light-content');
    } else {
      StatusBar.setBackgroundColor(color.statusBarBackground);
    }

    return (
      <HeaderContainer>
        <Touchable onPress={() => {
          if (onClose) {
            onClose();
          } else {
            history.goBack();
          }
        }}
        >
          {getIcon('close', color.white)}
        </Touchable>
        <List
          data={loginPageHeader}
          keyExtractor={(item) => item.image}
          renderItem={({ item }) => (
            <HeaderCarouselItemContainer key={item.image}>
              <HeaderImage
                source={{ uri: `${item.image}` }}
                isShowActivity={false}
                placeholderColor={theme.color.teal}
              />
              <Spacer margin={[1, 'auto', 1, 'auto']}>
                <Text size="m" color="white" weight="medium">{item.title}</Text>
              </Spacer>
            </HeaderCarouselItemContainer>
          )}
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          onScroll={(event: Object) => {
            const currentPage = Math.round((event.nativeEvent.contentOffset.x / windowWidth)) + 1;
            if (currentPage !== currentHeaderPage) {
              this.setState({ currentHeaderPage: currentPage });
            }
          }}
        />
        <PageIndicatorContainer>
          <PagerDotIndicator
            totalCount={loginPageHeader.length}
            currentPage={currentHeaderPage}
          />
        </PageIndicatorContainer>
      </HeaderContainer>
    );
  };

  render() {
    return (
      <Container>
        {this.getHeaderView()}
        <Content
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{ flexGrow: 1, backgroundColor: color.white }}
        >
          <AuthenticateUser
            onFlowChange={this.onFlowChange}
            onLoginSuccess={this.onLoginSuccess}
          />
        </Content>
      </Container>
    );
  }
}

LoginPage.propTypes = {
  onLoginSuccess: PropTypes.func,
  onClose: PropTypes.func,
  loginPageHeader: PropTypes.array.isRequired,
  history: PropTypes.object.isRequired,
  toastActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  loginPageHeader: state.content.loginPageHeader,
});

const mapDispatchToProps = (dispatch) => ({
  toastActions: bindActionCreators(toastActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginPage));
