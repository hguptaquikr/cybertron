import { Linking, Dimensions, PixelRatio } from 'react-native';
import isEmpty from 'lodash/isEmpty';
import config from '../config';

export const pluralize = (value, text, suffix = 's', prefixCount = false) => {
  const pluralText = +value > 1 ? `${text}${suffix}` : text;
  return prefixCount ? `${value} ${pluralText}` : pluralText;
};

// Capitalises the first character of a string
export const capitalizeFirstLetter = (string) => (
  string && string.length > 0 ? string.charAt(0).toUpperCase() + string.slice(1) : ''
);

export const slugify = (text = '') =>
  text.toString()
    .toLowerCase()
    .replace(/\s+/g, '-')
    .replace(/[^\w-]+/g, '')
    .replace(/--+/g, '-')
    .replace(/^-+/, '')
    .replace(/-+$/, '');

export const unslugify = (text) => text.replace(/-/g, ' ');

export const performanceMark = (name) => {
  if (window.performance && window.performance.mark) {
    window.performance.mark(name);
  }
};

// returns an array of keys, removing non useful keys like 0
export const safeKeys = (ob = {}) =>
  Object.keys(ob).filter((v) => v.length && v !== '0');

// returns a new object, removing the keysToOmit
export const omitKeys = (ob = {}, keysToOmit = []) => (
  !isEmpty(ob) ?
    Object.keys(ob).reduce((obj, key) => (
      keysToOmit.includes(key) ? obj : {
        ...obj,
        [key]: ob[key],
      }
    ), {}) : ob
);

export const browserDimensions = () => ({
  width:
  window.innerWidth
  || document.documentElement.clientWidth
  || document.body.clientWidth,
  height:
  window.innerHeight
  || document.documentElement.clientHeight
  || document.body.clientHeight,
});

export const formatDate = (moment, dateFormat = config.dateFormat.view) => (
  moment.format(dateFormat)
);

export const openMaps = (lat, lng) => () => {
  const mapsUrl = `https://maps.google.com/?q=${lat},${lng}`;
  Linking.openURL(mapsUrl);
};

const { width, height } = Dimensions.get('window');

const guidelineBaseWidth = 320;

const guidelineBaseHeight = 680;

export const horizontalScale = (size) => ((width / guidelineBaseWidth) * size);

export const verticalScale = (size) => ((height / guidelineBaseHeight) * size);

export const moderateScale = (size, factor = 0.5) => (
  size + ((horizontalScale(size) - size) * factor)
);

export const fontScale = (size) => (horizontalScale(size));

export const getDistanceWithUnit = (distance) => `${(distance / 1000).toFixed(1)} Km`;

const _125 = PixelRatio.getPixelSizeForLayoutSize(125);
const _200 = PixelRatio.getPixelSizeForLayoutSize(200);
const _320 = PixelRatio.getPixelSizeForLayoutSize(320);

export const imageResolutions = {
  lowres: `w=${_200}&h=${_125}&auto=compress&fm=pjpg`,
  highres: `w=${_320}&h=${_200}&auto=compress&fm=pjpg`,
};
