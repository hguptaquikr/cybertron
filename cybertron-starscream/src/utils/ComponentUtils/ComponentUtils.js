/* eslint-disable global-require,import/prefer-default-export */
import React from 'react';
import { Animated } from 'react-native';
import styled from 'styled-components/native';
import color from '../../theme/color';
import {
  AngleRight,
  AngleUp,
  BookingDateIcon,
  Close,
  LoyaltyBurned,
  LoyaltyEarned,
  LoyaltyExpired,
  Menu,
  RadioButtonActive,
} from '../../theme/icons';

const IconView = styled.View`
  width: ${({ size = 24 }) => size};
  height: ${({ size = 24 }) => size};
  justifyContent: center;
  alignItems: center;
`;

const Text = styled.Text`
  fontFamily: icomoon;
  transform: rotate(${({ rotationAngle = 0 }) => rotationAngle}deg);
  textAlign: center;
  fontSize: ${({ size = 24 }) => size};
  color: ${({ tintColor = color.charcoalGrey }) => tintColor};
`;

export const getSvgIconCode = (name) => {
  switch (name) {
    case 'close':
      return '\ue909';

    case 'reset-input':
      return '\ue90d';

    case 'back':
      return '\ue902';

    case 'menu':
      return '\ue919';

    case 'time':
      return '\ue908';

    case 'arrow-up':
    case 'arrow-down':
    case 'arrow-next':
      return '\ue90b';

    case 'eye':
    case 'eye-off':
      return '\ue923';

    case 'booking_date_icon':
    case 'booking_date_icon_inactive':
      return '\ue904';

    case 'coupon_code':
      return '\ue90a';

    case 'special_deal':
      return '\ue922';

    case 'sort':
      return '\ue921';

    case 'filter':
      return '\ue90c';

    case 'menu_contact_us':
    case 'call':
      return '\ue926';

    case 'share':
      return '\ue925';

    case 'manage-bookings':
      return '\ue916';

    case 'booking-confirmation':
      return '\ue903';

    case 'parking':
      return '\ue993';

    case 'elevator':
      return '\ue97c';

    case '24-hour-front-desk':
      return '\ue961';

    case '24-hours-power-supply':
      return '\ue962';

    case 'guest-laundry':
      return '\ue98b';

    case '24-hour-security':
      return '\ue960';

    case 'pet-friendly':
      return '\ue994';

    case 'complimentary-tea-coffee':
      return '\ue975';

    case 'cab-service':
      return '\ue971';

    case 'luggage-storage':
      return '\ue98e';

    case 'complimentary-water':
      return '\ue976';

    case 'banquet-party-hall':
      return '\ue96b';

    case 'coffee-table':
      return '\ue974';

    case 'cable-dth':
      return '\ue972';

    case 'board-room':
      return '\ue96e';

    case 'business-event-hosting':
      return '\ue970';

    case 'dining-table':
      return '\ue97a';

    case 'bar':
      return '\ue96c';

    case 'roof-top-restaurant':
      return '\ue985';

    case 'gym':
      return '\ue97f';

    case 'ironing-board':
      return '\ue988';

    case 'meeting-facility':
      return '\ue96e';

    case 'microwave':
      return '\ue98f';

    case 'shower-cabinet':
      return '\ue99a';

    case 'wake-up-service':
      return '\ue9a1';

    case 'restaurant-on-premises':
      return '\ue985';

    case 'room-service':
      return '\ue998';

    case 'easily-accessible-from-railway-station':
    case 'easily-accessible-from-airport':
      return '\ue983';

    case 'spa':
      return '\ue99c';

    case 'smoking-room-available':
      return '\ue99b';

    case 'swimming-pool':
      return '\ue99e';

    case 'safety-locker':
      return '\ue98d';

    case 'geyser':
      return '\ue97e';

    case 'wheel-chair':
      return '\ue97d';

    case 'kitchenett':
      return '\ue98a';

    case 'twin-bed':
      return '\ue9a0';

    case 'cupboards':
      return '\ue978';

    case 'bath-tub':
      return '\ue96d';

    case 'queen-bed':
      return '\ue996';

    case 'balcony':
      return '\ue96a';

    case 'fridge':
      return '\ue990';

    case 'king-bed':
      return '\ue989';

    case 'living-room':
      return '\ue98c';

    case 'audio-equipments':
      return '\ue969';

    case 'lcd-projector':
      return '\ue95f';

    case 'conference-hall':
      return '\ue977';

    case 'free-wifi':
      return '\ue968';

    case 'hot-water-kettle':
      return '\ue982';

    case 'complimentary-toiletries':
      return '\ue966';

    case 'tv':
      return '\ue967';

    case 'free-breakfast':
      return '\ue965';

    case 'card-payment-accepted':
      return '\ue973';

    case 'ac-room':
      return '\ue964';

    case 'star-fill':
      return '\ue91e';

    case 'star-stroke':
      return '\ue91f';

    case 'add-enabled':
    case 'add-disabled':
      return '\ue900';

    case 'remove-disabled':
    case 'remove-enabled':
      return '\ue920';

    case 'manager':
      return '\ue970';

    case 'menu-rewards':
      return '\ue912';

    case 'menu-faq':
      return '\ue915';

    case 'earn-treebo-points':
      return '\ue910';

    case 'image-placeholder':
      return '\ue90e';

    case 'radio-button-active':
      return '\ue91c';

    case 'radio-button-inactive':
      return '\ue91d';

    default:
      return '';
  }
};

const getIconText = (iconName, tintColor = '#4A4A4A', size = 24) => {
  switch (iconName) {
    case 'close':
      return (
        <Close color={JSON.stringify(tintColor)} size={size} />
      );

    case 'menu':
      return (
        <Menu color={JSON.stringify(tintColor)} size={size} />
      );

    case 'arrow-up':
      return (
        <AngleUp color={JSON.stringify(tintColor)} size={size} />
      );

    case 'arrow-next':
      return (
        <AngleRight color={JSON.stringify(tintColor)} size={size} />
      );

    case 'booking_date_icon':
      return (
        <BookingDateIcon color={JSON.stringify(tintColor)} size={size} />
      );

    case 'special_deal':
      return (
        <Text tintColor={tintColor || color.orangeYellow} size={size}>
          {getSvgIconCode(iconName)}
        </Text>
      );

    case 'radio-button-active':
      return (
        <RadioButtonActive color={JSON.stringify(tintColor || color.primary)} size={size} />
      );

    case 'points-credited':
      return (
        <LoyaltyBurned color={JSON.stringify(tintColor)} size={size} />
      );

    case 'points-debited':
      return (
        <LoyaltyEarned color={JSON.stringify(tintColor)} size={size} />
      );

    case 'points-expired':
      return (
        <LoyaltyExpired color={JSON.stringify(tintColor)} size={size} />
      );

    default:
      return (
        <Text tintColor={tintColor} size={size}>
          {getSvgIconCode(iconName)}
        </Text>
      );
  }
};

export const getIcon =
  (iconName, tintColor, boundingBoxSize = 24, size = 24) =>
    <IconView size={boundingBoxSize}>{getIconText(iconName, tintColor, size)}</IconView>;

export const getAnimatedIcon =
  (iconName, tintColor = color.charcoalGrey, size = 24, style = {}) => (
    <Animated.Text
      style={{ fontFamily: 'icomoon', fontSize: size, color: tintColor, ...style }}
    >
      {getSvgIconCode(iconName)}
    </Animated.Text>
  );
