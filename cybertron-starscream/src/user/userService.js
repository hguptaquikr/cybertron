import get from 'lodash/get';
import storeHouse from '../services/storeHouse';

const transformBookingObject = (booking) => ({
  id: booking.order_id,
  status: booking.status,
  amountPaid: booking.payment_detail.paid_amount,
  discount: booking.payment_detail.discount,
  partPayAmount: booking.partial_payment_amount,
  partPayLink: booking.partial_payment_link,
  isPayAtHotel: booking.paid_at_hotel,
  hotel: {
    id: booking.hotel_detail.hotel_id,
    name: booking.hotel_name,
    city: booking.hotel_detail.city,
    address: booking.hotel_detail.address,
    locality: booking.hotel_detail.locality,
    state: booking.hotel_detail.state,
    images: booking.hotel_detail.images,
    contact: booking.hotel_detail.contact_number,
    coordinates: {
      lat: booking.hotel_detail.latitude,
      lng: booking.hotel_detail.longitude,
    },
  },
  dates: {
    checkIn: booking.checkin_date,
    checkOut: booking.checkout_date,
    durationOfStay: booking.total_number_of_nights,
  },
  roomConfig: {
    adults: booking.room_config.no_of_adults,
    kids: booking.room_config.no_of_childs,
    roomCount: booking.room_config.room_count,
    roomType: booking.room_config.room_type,
  },
  price: {
    basePrice: booking.payment_detail.room_price,
    tax: booking.payment_detail.tax,
    totalPrice: booking.payment_detail.total,
    voucherPrice: booking.payment_detail.voucher_amount,
    memberDiscountPrice: booking.payment_detail.member_discount,
    pendingAmount: booking.payment_detail.pending_amount,
  },
  coupon: {
    code: booking.payment_detail.coupon_code,
    amount: booking.payment_detail.discount,
  },
  guest: {
    contact: booking.user_detail.contact_number,
    name: booking.user_detail.name,
    email: booking.user_detail.email,
  },
  cancellation: {
    hash: booking.cancel_hash,
  },
  ratePlan: {
    tag: get(booking, 'rate_plan_meta.tag', ''),
    description: get(booking, 'rate_plan_meta.description', ''),
  },
});

export default {
  isLoggedIn() {
    return storeHouse.get(['auth_token', 'user_profile'])
      .then((values) => values[0] && values[1]).catch(() => false);
  },

  getUserProfile() {
    return storeHouse.get('user_profile')
      .then((userProfile) => userProfile).catch(() => false);
  },

  getAuthToken() {
    return storeHouse.get('auth_token')
      .then((authToken) => authToken).catch(() => false);
  },

  refreshAuthToken(newAuthToken) {
    if (newAuthToken) {
      return this.setAuthToken(newAuthToken.split(' ')[1])
        .then(() => true).catch(() => false);
    }
    return Promise.resolve(false);
  },

  setAuthToken(token) {
    return storeHouse.save('auth_token', token);
  },

  setUserProfile(profileDetails) {
    return storeHouse.save('user_profile', profileDetails);
  },

  unsetAuthData() {
    return storeHouse.delete(['auth_token', 'user_profile'])
      .then(() => true).catch(() => false);
  },

  transformBookingHistoryApi({ data }) {
    const transformedData = {
      results: {},
      upcomingSortOrder: [],
      pastSortOrder: [],
    };

    transformedData.upcomingSortOrder = data.upcoming.map((bh) => {
      transformedData.results[bh.order_id] = {
        ...transformBookingObject(bh),
        isPastBooking: false,
      };
      return bh.order_id;
    });

    transformedData.pastSortOrder = data.previous.map((bh) => {
      transformedData.results[bh.order_id] = {
        ...transformBookingObject(bh),
        isPastBooking: true,
      };
      return bh.order_id;
    });

    return transformedData;
  },

  transformInitiateCancellationApi({ data }) {
    return {
      bookingId: data.booking_details.booking_id,
      isPayAtHotel: data.booking_details.paid_at_hotel,
      hotel: {
        name: data.booking_details.hotel_name,
        address: data.booking_details.hotel_address,
      },
      dates: {
        checkIn: data.booking_details.checkin_date,
        checkOut: data.booking_details.checkout_date,
      },
      guest: {
        name: data.booking_details.guest_name,
      },
      cancellation: {
        hash: data.booking_details.booking_hash,
        reasons: data.booking_details.reasons.map((reason) => ({
          label: reason.label,
          value: reason.value.toString(),
        })),
        status: data.booking_status.status.toUpperCase(),
        message: data.booking_status.message,
        date: data.cancelled_dateTime,
      },
    };
  },

  transformConfirmCancellationApi({ data }) {
    return {
      bookingId: data.bookingId,
      cancellation: {
        hash: data.cancellationHash,
        status: data.booking_status.status,
        message: data.booking_status.message,
      },
    };
  },

  transformLoginApi({ data }) {
    return {
      profile: {
        email: data.email,
        mobile: data.phone_number,
        firstName: data.first_name,
        lastName: data.last_name,
        name: `${data.first_name}${data.last_name ? ` ${data.last_name}` : ''}`,
        userId: data.id,
      },
      token: data.token,
    };
  },
};
