import { handle } from 'redux-pack';
import { REHYDRATE } from 'redux-persist/constants';
import merge from 'lodash/merge';
import userService from './userService';
import analyticsService from '../analytics/analyticsService';

const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
const RESET_PASSWORD = 'RESET_PASSWORD';
const SEND_LOGIN_OTP = 'SEND_LOGIN_OTP';
const SET_USER_PROFILE = 'SET_USER_PROFILE';
const UNSET_USER_PROFILE = 'UNSET_USER_PROFILE';
const UPDATE_LOGIN_MOBILE = 'UPDATE_LOGIN_MOBILE';
const EMAIL_LOGIN = 'EMAIL_LOGIN';
const USER_LOGOUT = 'USER_LOGOUT';
const VERIFY_OTP = 'VERIFY_OTP';
const GET_BOOKING_HISTORY = 'GET_BOOKING_HISTORY';
const INITIATE_CANCELLATION = 'INITIATE_CANCELLATION';
const CONFIRM_CANCELLATION = 'CONFIRM_CANCELLATION';

const initialState = {
  isAuthenticated: false,
  profile: {
    email: '',
    firstName: '',
    lastName: '',
    mobile: '',
    name: '',
    userId: '',
  },
  bookingHistory: {
    isBookingHistoryLoading: false,
    upcomingSortOrder: [],
    pastSortOrder: [],
    results: {
      0: {
        id: '',
        status: '',
        discount: '',
        amountPaid: '',
        partPayLink: '',
        partPayAmount: '',
        isPayAtHotel: '',
        booking: { status: '', type: '', statusInfo: '' },
        hotel: {
          name: '',
          address: '',
          images: [],
          coordinates: { lat: '', lng: '' },
          contact: '',
        },
        dates: { checkIn: '', checkOut: '', durationOfStay: '' },
        price: {
          basePrice: 0,
          tax: 0,
          totalPrice: 0,
          voucherPrice: 0,
          memberDiscountPrice: 0,
          pendingAmount: 0,
        },
        coupon: { code: '', amount: '' },
        roomConfig: {},
        guest: { contact: '', name: '', email: '' },
        cancellation: {
          hash: '',
          hashUrl: '',
          status: '',
          message: '',
          charges: '',
          reasons: [],
        },
        ratePlan: {
          tag: '',
          description: '',
        },
      },
    },
  },
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  const incoming = payload ? payload.user : null;

  switch (type) {
    case REHYDRATE:
      if (incoming) {
        return { ...state, ...incoming };
      }
      return state;

    case SET_USER_PROFILE:
      return {
        ...state,
        profile: {
          ...state.profile,
          ...payload.profile,
        },
        isAuthenticated: true,
      };

    case UNSET_USER_PROFILE:
      return {
        ...state,
        profile: {},
        isAuthenticated: false,
      };

    case SEND_LOGIN_OTP: return handle(state, action, {
      start: (s) => ({
        ...s,
        profile: {
          ...s.profile,
          mobile: payload.mobile,
        },
      }),
    });

    case VERIFY_OTP: return handle(state, action, {
      success: (s) => ({
        ...s,
        profile: {
          ...s.profile,
          ...payload.profile,
        },
        isAuthenticated: true,
        authToken: payload.token,
      }),
    });

    case UPDATE_LOGIN_MOBILE:
      return {
        ...state,
        profile: {
          ...state.profile,
          mobile: payload.mobile,
        },
      };

    case FORGOT_PASSWORD: return handle(state, action, {
      start: (s) => ({
        ...s,
        profile: {
          ...s.profile,
          email: payload.email,
        },
      }),
    });

    case EMAIL_LOGIN:
    case RESET_PASSWORD: return handle(state, action, {
      success: (s) => ({
        ...s,
        profile: {
          ...s.profile,
          ...payload.profile,
        },
        isAuthenticated: true,
        authToken: payload.token,
      }),
      failure: (s) => ({
        ...s,
        error: payload.error,
      }),
    });

    case USER_LOGOUT: return handle(state, action, {
      success: (s) => ({
        ...s,
        profile: {},
        isAuthenticated: false,
        bookingHistory: {
          results: {
            0: s.bookingHistory.results[0],
          },
          upcomingSortOrder: [],
          pastSortOrder: [],
        },
      }),
      failure: (s) => ({
        ...s,
        error: payload.error,
      }),
    });

    case GET_BOOKING_HISTORY: return handle(state, action, {
      start: (s) => ({
        ...s,
        bookingHistory: {
          ...s.bookingHistory,
          isBookingHistoryLoading: true,
        },
      }),
      success: (s) => ({
        ...s,
        bookingHistory: merge(s.bookingHistory, payload),
      }),
      finish: (s) => ({
        ...s,
        bookingHistory: {
          ...s.bookingHistory,
          isBookingHistoryLoading: false,
        },
      }),
    });

    case INITIATE_CANCELLATION: return handle(state, action, {
      success: (s) => ({
        ...s,
        bookingHistory: {
          ...s.bookingHistory,
          results: {
            ...s.bookingHistory.results,
            [payload.bookingId]: {
              ...merge(
                payload,
                s.bookingHistory.results[payload.bookingId],
              ),
            },
          },
        },
      }),
    });

    case CONFIRM_CANCELLATION: return handle(state, action, {
      success: (s) => ({
        ...s,
        bookingHistory: {
          ...s.bookingHistory,
          results: {
            ...s.bookingHistory.results,
            [payload.bookingId]: {
              ...s.bookingHistory.results[payload.bookingId],
              cancellation: {
                ...s.bookingHistory.results[payload.bookingId].cancellation,
                ...payload.cancellation,
              },
            },
          },
        },
      }),
    });

    default:
      return state;
  }
};

const getCancellationEventProps = (bookingDetails) => {
  const {
    id,
    amountPaid,
    partPayLink,
    partPayAmount,
    status,
    price = {},
    coupon = {},
    dates: {
      checkIn,
      checkOut,
    },
    hotel: {
      name,
    },
  } = bookingDetails;
  let bookingStatus = 'cancelled';
  if (partPayLink && status && status.toLowerCase() === 'reserved') {
    bookingStatus = 'temporary';
  } else if (status && status.toLowerCase() === 'reserved') {
    bookingStatus = 'confirmed';
  }

  return {
    order_id: id,
    checkin_date: checkIn,
    checkout_date: checkOut,
    hotel_name: name,
    price: price.totalPrice,
    payment_mode: price.pendingAmount > 0 ? 'PAH' : 'Prepaid', // TODO: check this logic
    booking_status: bookingStatus,
    amount_paid: amountPaid,
    amount_pending: price.pendingAmount,
    coupon: coupon.code,
    member_discount_perc: '', // TODO: get it from backend
    member_discount_amount: price.memberDiscountPrice,
    part_pay_pending: partPayLink && partPayAmount > 0,
  };
};

const onLoginSuccess = (authData) => {
  const { profile, token } = authData;
  userService.setUserProfile(profile);
  userService.setAuthToken(token);
};

export const setUserProfile = (profile) => ({
  type: SET_USER_PROFILE,
  payload: { profile },
});

export const unsetUserProfile = () => ({
  type: UNSET_USER_PROFILE,
});

export const emailLogin = (userData) => (dispatch, getState, { api }) =>
  dispatch({
    type: EMAIL_LOGIN,
    promise: api.post('/v2/auth/login/email/', userData)
      .then(userService.transformLoginApi),
    meta: {
      onSuccess: (authData) => {
        onLoginSuccess(authData);
      },
    },
  });

export const userLogout = () => (dispatch, getState, { api }) =>
  dispatch({
    type: USER_LOGOUT,
    promise: api.get('/v2/auth/logout/'),
    meta: {
      onSuccess: () => {
        userService.unsetAuthData();
      },
    },
  });

export const forgotPassword = (email) => (dispatch, getState, { api }) =>
  dispatch({
    type: FORGOT_PASSWORD,
    payload: { email },
    promise: api.post('/v2/auth/forgot-password/', { email }),
  });

export const resetPassword = (password, token) => (dispatch, getState, { api }) => {
  const resetPasswordData = {
    confirm: password,
    password,
    verification_code: token,
  };
  return dispatch({
    type: RESET_PASSWORD,
    promise: api.post('/v2/auth/reset-password/', resetPasswordData)
      .then(userService.transformLoginApi),
    meta: {
      onSuccess: (authData) => {
        onLoginSuccess(authData);
      },
    },
  });
};

export const sendLoginOtp = (mobile) => (dispatch, getState, { api }) =>
  dispatch({
    type: SEND_LOGIN_OTP,
    payload: { mobile },
    promise: api.post('/v2/auth/login/otp/', { phone_number: mobile }),
  });

export const verifyLoginOtp = (mobile, otp) => (dispatch, getState, { api }) => {
  const verifyOtpData = {
    phone_number: mobile,
    verification_code: otp,
  };
  return dispatch({
    type: VERIFY_OTP,
    promise: api.post('/v2/auth/login/otp/verify/', verifyOtpData)
      .then(userService.transformLoginApi),
    meta: {
      onSuccess: (authData) => {
        onLoginSuccess(authData);
      },
    },
  });
};

export const updateLoginMobile = (mobile) => (dispatch) => {
  dispatch({
    type: UPDATE_LOGIN_MOBILE,
    payload: { mobile },
  });
  return dispatch(sendLoginOtp(mobile));
};

export const checkIfUserAuthenticated = () => (dispatch) => {
  userService.isLoggedIn().then((isUserLoggedIn) => {
    if (isUserLoggedIn) {
      userService.getUserProfile().then((userProfile) => {
        dispatch(setUserProfile(userProfile));
      });
    } else {
      dispatch(unsetUserProfile());
    }
  });
};

export const getBookingHistory = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_BOOKING_HISTORY,
    promise: api.get('/v2/booking/booking-history/')
      .then(userService.transformBookingHistoryApi),
    meta: {
      onSuccess: (payload) => {
        const eventProps = {
          num_of_upcoming_bookings: payload.upcomingSortOrder.length,
          num_of_past_bookings: payload.pastSortOrder.length,
        };
        analyticsService.segmentTrackEvent('Booking History Page Viewed', eventProps);
      },
    },
  });

export const initiateCancellation = (cancellationHash) =>
  (dispatch, getState, { api }) => {
    const cancellationData = {
      booking_hash: cancellationHash,
    };
    return dispatch({
      type: INITIATE_CANCELLATION,
      promise: api.get('/v1/growth/bookingdata/', cancellationData)
        .then(userService.transformInitiateCancellationApi),
      meta: {
        onSuccess: () => {
          const state = getState();
          const bookingDetails = Object.values(state.user.bookingHistory.results)
            .find((booking) => booking.cancellation.hash === cancellationHash)
          || state.user.bookingHistory.results[0];
          analyticsService.segmentTrackEvent('Cancel Modal Viewed', getCancellationEventProps(bookingDetails));
        },
      },
    });
  };

export const confirmCancellation = (cancellationHash, bookingId, reason, message) =>
  (dispatch, getState, { api }) => {
    const state = getState();
    const bookingDetails = state.user.bookingHistory.results[bookingId]
      || state.user.bookingHistory.results[0];
    const eventProps = {
      ...getCancellationEventProps(bookingDetails),
      cancellation_reason: reason,
    };
    const cancellationData = {
      order_id: bookingId,
      booking_hash: cancellationHash,
      reason,
      message,
    };
    analyticsService.segmentTrackEvent('Proceed With Cancellation Clicked', eventProps);
    return dispatch({
      type: CONFIRM_CANCELLATION,
      promise: api.post('/v1/growth/submitcancel', cancellationData)
        .then((response) => ({
          data: {
            ...response.data,
            bookingId,
            cancellationHash,
          },
        }))
        .then(userService.transformConfirmCancellationApi),
      meta: {
        onSuccess: () => {
          analyticsService.segmentTrackEvent('Booking Successfully Cancelled', eventProps);
        },
      },
    });
  };
