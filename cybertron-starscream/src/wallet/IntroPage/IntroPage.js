/* eslint-disable global-require */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Platform, StatusBar, BackHandler } from 'react-native';
import { withRouter } from 'react-router-native';
import { Spacer, Button } from 'leaf-ui/native';
import color from '../../theme/color';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import Text from '../../components/NewLeaf/Text/Text';
import userService from '../../user/userService';
import ImageLoad from '../../components/ImageLoad/ImageLoad';
import storeHouse from '../../services/storeHouse';
import TouchableComponent from '../../components/Touchable/TouchableComponent';

const HeaderContainer = styled.View`
  paddingTop: 24;
  backgroundColor: ${color.charcoalGrey};
`;

const TouchableIcon = styled.View`
  marginLeft: 8;
  justifyContent: center;
  alignItems: center;
  padding: 4px;
  width: 32px;
  height: 32px;
`;

const BulletPointsView = styled.View`
  marginLeft: ${({ marginLeft = 0 }) => marginLeft};
  paddingVertical: 8;
`;

const RootView = styled.View`
  flex: 1;
`;

const PageContainer = styled.ScrollView`
  flexGrow: 1;
`;

const RowContainer = styled.View`
  flexDirection: row;
`;

const IntroducingTextBackground = styled.View`
  flex: 1;
  justifyContent: center;
  alignItems: center;
`;

const TitleIconTextContainer = styled.View`
  marginTop: 32;
  flexDirection: row;
`;

const DetailsTextContainer = styled.View`
  paddingTop: 36;
  paddingLeft: 16;
  paddingRight: 16;
`;

const TnCContainer = styled.View`
  flexDirection: row;
  flex: 1;
  justifyContent: center;
  alignItems: center;
  backgroundColor: ${color.veryLightGrey};
  marginTop: 40;
  paddingVertical: 16;
  marginHorizontal: 16;
  borderRadius: 2;
  borderWidth: 1;
  borderColor: ${color.disabledGreyOpacity10}
`;

const BannerImageContainer = styled.View`
  paddingTop: 24;
  paddingBottom: 44;
  alignItems: center;
`;

const DummyView = styled.View`
  width: 32;
`;

const ButtonContainer = styled.View`
  padding: 8px;
`;

const Image = styled.Image``;

class IntroPage extends Component {
  state = {
    isLoggedIn: 'None',
  };

  componentWillMount() {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('light-content');
    } else {
      StatusBar.setBackgroundColor(color.charcoalGrey);
    }
    userService.isLoggedIn().then((isLoggedId) => {
      if (isLoggedId) {
        this.setState({ isLoggedIn: true });
      } else {
        this.setState({ isLoggedIn: false });
      }
    }).catch(() => this.setState({ isLoggedIn: false }));
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  onBackPress = () => {
    const { history } = this.props;
    history.goBack();
    return true;
  };

  openTnCs = () => {
    const { history } = this.props;
    history.push('/faq/');
    // const url = `${config.baseUrl}/faq/`;
    // Linking.canOpenURL(url).then((supported) => {
    //   if (supported) {
    //     return Linking.openURL(url).catch(() => null);
    //   }
    //   return Promise.reject('Supporting app not present');
    // });
  };

  render() {
    const { history } = this.props;
    const { isLoggedIn } = this.state;
    return (
      <RootView>
        <PageContainer contentContainerStyle={{ paddingBottom: 16 }}>
          <HeaderContainer>
            <RowContainer>
              <TouchableComponent
                onPress={() => {
                  history.goBack();
                }}
                testID="close-button"
                accessibilityLabel="close-button"
              >
                <TouchableIcon>
                  {getIcon('close', color.white)}
                </TouchableIcon>
              </TouchableComponent>
              <IntroducingTextBackground>
                <Text size="m" style={{ color: color.whiteOpacity40 }}>
                  Introducing
                </Text>
              </IntroducingTextBackground>
              <DummyView />
            </RowContainer>
            <BannerImageContainer>
              <Image source={require('../treebo_rewards_logo_white.png')} />
            </BannerImageContainer>
          </HeaderContainer>
          <DetailsTextContainer>
            <Text size="l" weight="bold">
              Booking directly on Treebo pays!
            </Text>
            <TitleIconTextContainer>
              <ImageLoad source={{ uri: 'https://images.treebohotels.com/images/app_login_member_rate.png?w=320&h=200&auto=compress' }} />
              <BulletPointsView marginLeft={16}>
                <Spacer margin={[0, 0, 1, 0]}>
                  <Text size="m" weight="medium" family="medium">
                    Our Best Rates
                  </Text>
                </Spacer>
                <Text size="s" color="greyDark">
                  Members of the program get our best rates.
                </Text>
              </BulletPointsView>
            </TitleIconTextContainer>
            <TitleIconTextContainer>
              <ImageLoad source={{ uri: 'https://images.treebohotels.com/images/app_login_treebo_rewards.png?w=320&h=200&auto=compress' }} />
              <BulletPointsView marginLeft={16}>
                <Spacer margin={[0, 0, 1, 0]}>
                  <Text size="m" weight="medium" family="medium">
                    Earn Treebo Points
                  </Text>
                </Spacer>
                <Text size="s" color="greyDark">
                  Earn for every stay booked directly with us.
                </Text>
              </BulletPointsView>
            </TitleIconTextContainer>
            <Spacer margin={[5, 0, 4, 0]}>
              <Text size="l" weight="bold">
                How do Treebo Points Work?
              </Text>
            </Spacer>
            <Spacer margin={[0, 0, 1, 0]}>
              <Text size="m" weight="medium" family="medium">
                Book via Treebo
              </Text>
            </Spacer>
            <Text size="s" color="greyDark">
              Book a Treebo hotel, directly with us.
            </Text>
            <Spacer margin={[4, 0, 1, 0]}>
              <Text size="m" weight="medium" family="medium">
                Stay & Earn
              </Text>
            </Spacer>
            <Text size="s" color="greyDark">
              Treebo points get credited to your account on checkout.{'\n'}
              <Text
                size="s"
                family="medium"
                color="greyDarker"
                weight="bold"
              >
                1 Treebo Point = ₹ 1
              </Text>
            </Text>
            <Spacer margin={[4, 0, 1, 0]}>
              <Text size="m" weight="medium" family="medium">
              Use Treebo Points
              </Text>
            </Spacer>
            <Text size="s" color="greyDark">
              Treebo Points earned can be used on your
              future stays with us, booked directly on Treebo.
            </Text>
          </DetailsTextContainer>
          <TnCContainer>
            <Text size="s">
              Want more details?
            </Text>
            <TouchableComponent onPress={this.openTnCs}>
              <Text size="s" color="blue">
                View detailed T&Cs.
              </Text>
            </TouchableComponent>
          </TnCContainer>
        </PageContainer>
        <ButtonContainer>
          <Button
            block
            onPress={() => {
              if (isLoggedIn) {
                history.replace('/wallet/wallet-history/');
              } else {
                history.replace('/login/');
              }
              storeHouse.save('is_wallet_intro_shown', true);
            }}
            testID="treebo-points-button"
            accessibilityLabel="treebo-points-button"
          >
            <Text size="m" weight="medium" family="medium" color="white">
              {
                isLoggedIn
                  ? 'VIEW YOUR TREEBO POINTS'
                  : 'SIGN UP FOR REWARD POINTS'
              }
            </Text>
          </Button>
        </ButtonContainer>
      </RootView>
    );
  }
}

IntroPage.propTypes = {
  history: PropTypes.object,
};

export default withRouter(IntroPage);
