import React from 'react';
import styled from 'styled-components/native';
import Text from '../../components/NewLeaf/Text/Text';
import color from '../../theme/color';

const InfoContainer = styled.View`
  borderRadius: 2;
  borderWidth: 1;
  borderColor: ${color.orangeYellowOpacity70}
  backgroundColor: ${color.orangeYellowOpacity20};
  paddingHorizontal: 16;
  paddingVertical: 12;
`;

const TreeboPointInfo = () => (
  <InfoContainer>
    <Text size="s" weight="medium" family="medium">
      Treebo Points get credited to primary traveller post mobile verification during checkout.
    </Text>
  </InfoContainer>
);

export default TreeboPointInfo;
