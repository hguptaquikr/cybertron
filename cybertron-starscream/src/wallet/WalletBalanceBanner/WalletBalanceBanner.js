/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { connect } from 'react-redux';
import get from 'lodash/get';
import { Spacer } from 'leaf-ui/native';
import color from '../../theme/color';
import Text from '../../components/NewLeaf/Text/Text';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const BannerBackgroundView = styled.View`
  borderRadius: 2;
  marginBottom: 8;
  marginHorizontal: 8;
  paddingHorizontal: 12;
  backgroundColor: ${({ backgroundColor = color.lightBlue }) => backgroundColor};
  flexDirection: row;
  alignItems: center;
  borderWidth: 0.5;
  borderColor: ${({ borderColor = color.darkBlue }) => borderColor};
`;

const WalletBalanceBanner = ({ style, wallet: { wallets }, walletType }) => {
  const walletBalance = get(wallets, `${walletType}.totalBalance`, 0);
  return (
    <BannerBackgroundView
      backgroundColor={color.skinColor}
      borderColor={color.skinColorDark}
      style={style}
    >
      {getIcon(walletBalance === 0 ? 'earn-treebo-points' : 'menu-rewards', color.warmGrey)}
      <Spacer margin={[0, 0, 0, 1]} padding={[1.5, 0, 1.5, 0]}>
        <Text
          size="s"
          weight="medium"
          color="greyDarker"
          testID="wallet-banner-text"
          accessibilityLabel="wallet-banner-text"
        >
          {
          walletBalance === 0 ?
            'To earn Treebo Points on your stay - Book Now!' :
            `\u20b9${walletBalance} worth of Treebo Points pre-applied to price.`
          }
        </Text>
      </Spacer>
    </BannerBackgroundView>
  );
};

WalletBalanceBanner.propTypes = {
  style: PropTypes.array,
  wallet: PropTypes.object,
  walletType: PropTypes.string,
};

const mapStateToProps = (state) => ({
  userDetails: state.user.profile,
  wallet: state.wallet,
});

WalletBalanceBanner.defaultProps = {
  walletType: 'TP',
};

export default connect(
  mapStateToProps,
  null,
)(WalletBalanceBanner);
