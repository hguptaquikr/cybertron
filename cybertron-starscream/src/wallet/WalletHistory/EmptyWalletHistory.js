/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Button } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';

const EmptyHistoryContainer = styled.View`
  flex: 1;
  padding: 8px;
`;

const EmptyHistoryMessageContainer = styled.View`
  flex: 1;
  paddingTop: 48;
  paddingHorizontal: 40;
  alignItems: center;
`;

const EmptyStateImage = styled.Image``;

const emptyStateImageStyle = {
  width: 'auto',
  aspectRatio: 1.6,
  flex: 1,
};

const EmptyWalletHistory = ({ history }) => (
  <EmptyHistoryContainer>
    <EmptyHistoryMessageContainer>
      <EmptyStateImage
        style={emptyStateImageStyle}
        resizeMode="contain"
        resizeMethod="resize"
        source={require('../empty_state_no_treebo_points.png')}
      />
      <Text size="l" weight="bold" style={{ textAlign: 'center', marginTop: 36 }}>
        Earn Treebo Points
      </Text>
      <Text size="s" color="greyDark" style={{ textAlign: 'center', marginTop: 16, marginBottom: 16 }}>
        Book and stay with us to start earning Treebo Points!
      </Text>
    </EmptyHistoryMessageContainer>
    <Button
      block
      bold
      onPress={() => history.go(1 - history.length)}
    >
      <Text size="m" weight="medium" family="medium" color="white">
        BOOK NOW
      </Text>
    </Button>
  </EmptyHistoryContainer>
);

EmptyWalletHistory.propTypes = {
  history: PropTypes.object.isRequired,
};

export default EmptyWalletHistory;
