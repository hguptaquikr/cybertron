import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StatusBar, Platform, BackHandler } from 'react-native';
import { bindActionCreators } from 'redux';
import { theme } from 'leaf-ui/native';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-native';
import styled from 'styled-components/native';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import * as walletActionCreators from '../../wallet/walletDuck';
import BackButtonHeader from '../../components/Header/BackButtonHeader';
import Loader from '../../components/Loader/Loader';
import List from '../../components/List/List';
import Text from '../../components/NewLeaf/Text/Text';
import Price from '../../components/NewLeaf/Price/Price';
import EmptyWalletHistory from './EmptyWalletHistory';
import WalletHistoryListItem from './WalletHistoryListItem';
import TreePointInfo from '../TreeboPointInfo/TreeboPointInfo';
import color from '../../theme/color';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const View = styled.View``;

const FlexContainer = styled.View`
  flex: 1;
`;

const ListItemSeparator = styled.View`
  height: 1;
  background: ${color.tertiary};
  marginLeft: 56;
  marginRight: 16;
`;

const TreeboPointInfoContainer = styled.View`
  padding: 8px;
  backgroundColor: ${color.tertiary};
`;

const Row = styled.View`
  flexDirection: row;
  alignItems: center;
  justifyContent: space-between;
`;

const Touchable = styled.TouchableOpacity`
  padding: 4px;
  marginLeft: 8;
  justifyContent: center;
  alignItems: center;
  width: 32;
  height: 32;
`;

const ViewDetailsTochable = styled.TouchableOpacity`
  padding: 8px;
  marginRight: 8;
`;

const DarkHeaderContainer = styled.View`
  paddingTop: 24;
  backgroundColor: ${theme.color.teal};
`;

const LightHeaderContainer = styled.View`
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  zIndex: 5;
`;

const WalletBalanceContainer = styled.View`
  paddingTop: 20;
  paddingBottom: 32;
  paddingHorizontal: 16;
  backgroundColor: ${theme.color.teal};
  alignItems: center;
`;

class WalletHistory extends Component {
  state = {
    showLightHeader: false,
  };

  componentDidMount() {
    const { walletActions } = this.props;
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('dark-content');
    } else {
      StatusBar.setBackgroundColor(color.black);
    }
    walletActions.getWalletHistory();
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  onBackPress = () => {
    const { history } = this.props;
    history.goBack();
    return true;
  };

  getWalletBalanceView() {
    const { wallet: { wallets }, walletType } = this.props;
    return (
      <WalletBalanceContainer>
        <Price
          price={get(wallets, `${walletType}.totalBalance`, 0)}
          size="xxl"
          color="white"
          roundUpTo={2}
        />
        <Text
          size="m"
          style={{
            marginTop: 8,
            textAlign: 'center',
            color: color.whiteOpacity80,
          }}
        >
          worth of Treebo Points
        </Text>
      </WalletBalanceContainer>
    );
  }

  getDarkHeader = () => {
    const { history } = this.props;
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('light-content');
    } else {
      StatusBar.setBackgroundColor(color.statusBarBackground);
    }
    return (
      <DarkHeaderContainer>
        <Row>
          <Touchable onPress={history.goBack}>
            {getIcon('close', color.white)}
          </Touchable>
          <ViewDetailsTochable onPress={() => history.replace('/wallet/intro/')}>
            <Text size="s" color="white" family="medium" weight="medium">View Details</Text>
          </ViewDetailsTochable>
        </Row>
        {this.getWalletBalanceView()}
      </DarkHeaderContainer>
    );
  };

  getLightHeader = () => {
    const { showLightHeader } = this.state;
    const { wallet: { wallets }, walletType } = this.props;
    if (showLightHeader) {
      if (Platform.OS === 'ios') {
        StatusBar.setBarStyle('dark-content');
      } else {
        StatusBar.setBackgroundColor(color.black);
      }
      return (
        <LightHeaderContainer>
          <BackButtonHeader middle={`Treebo Points: \u20B9${get(wallets, `${walletType}.totalBalance`, 0)}`} />
        </LightHeaderContainer>
      );
    }
    return null;
  };

  renderList = () => {
    const { wallet: { walletHistory: { walletStatement } } } = this.props;
    return (
      <List
        listHeaderComponent={
          <View>
            {this.getDarkHeader()}
            <TreeboPointInfoContainer>
              <TreePointInfo />
            </TreeboPointInfoContainer>
          </View>
        }
        data={walletStatement}
        keyExtractor={(item, i) => `${item.particular}${item.customFieldOne}${i}`}
        itemSeparatorComponent={ListItemSeparator}
        renderItem={({ item }) => (
          <WalletHistoryListItem statement={item} />
        )}
        onScroll={(event: Object) => {
          const { showLightHeader } = this.state;
          if (event.nativeEvent.contentOffset.y <= 0
            && showLightHeader) {
            this.setState({ showLightHeader: false });
          } else if (event.nativeEvent.contentOffset.y > 0
            && !showLightHeader) {
            this.setState({ showLightHeader: true });
          }
        }}
      />
    );
  };

  render() {
    const {
      wallet: { walletHistory: { isWalletHistoryLoading, walletStatement } },
      history,
    } = this.props;

    if (isWalletHistoryLoading) {
      return (
        <FlexContainer>
          <BackButtonHeader middle="Wallet History" />
          <Loader />
        </FlexContainer>
      );
    }

    if (isEmpty(walletStatement)) {
      return (
        <FlexContainer>
          {this.getDarkHeader()}
          <EmptyWalletHistory history={history} />
        </FlexContainer>
      );
    }

    return (
      <FlexContainer>
        {this.renderList()}
        {this.getLightHeader()}
      </FlexContainer>
    );
  }
}

WalletHistory.propTypes = {
  wallet: PropTypes.object,
  walletType: PropTypes.string,
  walletActions: PropTypes.object.isRequired,
  history: PropTypes.object,
};

WalletHistory.defaultProps = {
  walletType: 'TP',
};

const mapStateToProps = (state) => ({
  wallet: state.wallet,
});

const mapDispatchToProps = (dispatch) => ({
  walletActions: bindActionCreators(walletActionCreators, dispatch),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(WalletHistory));
