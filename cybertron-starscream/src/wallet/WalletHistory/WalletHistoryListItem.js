import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import Text from '../../components/NewLeaf/Text/Text';
import Price from '../../components/NewLeaf/Price/Price';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import color from '../../theme/color';

const View = styled.View``;

const StatementContainer = styled.View`
  paddingHorizontal: 16;
  paddingTop: 20;
  paddingBottom: 12;
`;

const StatementParticulars = styled.View`
  flexDirection: row;
  alignItems: center;
`;

const StatementDetails = styled.View`
  marginTop: 4;
  marginLeft: 40;
`;

const ExpiryDateContainer = styled.View`
  flexDirection: row;
  alignItems: center;
  marginBottom: 4;
`;

const ExpiredTag = styled.View`
  paddingHorizontal: 4;
  paddingVertical: 2;
  borderRadius: 2;
  backgroundColor: ${color.warmGrey};
`;

const RelatedCreditContainer = styled.View`
  paddingLeft: 16;
  paddingRight: 10;
  paddingVertical: 12;
  marginTop: 8;
  backgroundColor: ${color.tertiary};
`;

const RelatedCreditParticular = styled.View`
  flexDirection: row;
  alignItems: center;
  marginBottom: 4;
`;

const iconName = {
  credit: 'points-credited',
  debit: 'points-debited',
  refund: 'points-credited',
};

const priceColor = {
  credit: 'green',
  debit: 'redDark',
  refund: 'green',
};

const getExpiryDateView = (expiryDate, status, light = false, isMedium = true) => {
  if (expiryDate) {
    if (status === 'expired') {
      return (
        <ExpiryDateContainer>
          <ExpiredTag>
            <Text size="s" weight="medium" family="medium" color="white">Expired</Text>
          </ExpiredTag>
          <Text size="s" color="grey">
            {` on ${expiryDate}`}
          </Text>
        </ExpiryDateContainer>
      );
    }

    return (
      <ExpiryDateContainer>
        <Text
          size="s"
          weight={isMedium ? 'medium' : 'normal'}
          color={light ? 'greyDark' : 'greyDarker'}
        >
          {`Expires on ${expiryDate}`}
        </Text>
      </ExpiryDateContainer>
    );
  }
  return null;
};

const renderRelatedCredits = (relatedCredits) => {
  if (!isEmpty(relatedCredits)) {
    return (
      <View>
        {
          relatedCredits.map((credit) => (
            <RelatedCreditContainer key={`${credit.particular}${credit.expiresOn}${credit.customFieldOne}`}>
              <RelatedCreditParticular>
                <Text
                  size="s"
                  weight="medium"
                  family="medium"
                  color={credit.status === 'expired' ? 'grey' : 'greyDark'}
                  style={{
                    marginRight: 16,
                    marginBottom: 4,
                    flex: 1,
                    textDecorationLine: credit.status === 'expired' ? 'line-through' : 'none',
                  }}
                >
                  {credit.particular}
                </Text>
                <Price
                  price={credit.amount}
                  color={credit.status === 'expired' ? 'grey' : 'greyDark'}
                  size="s"
                  showSign
                  type={credit.status === 'expired' ? 'striked' : 'regular'}
                />
              </RelatedCreditParticular>
              {getExpiryDateView(credit.expiresOn, credit.status, true, false)}
            </RelatedCreditContainer>
          ))
        }
      </View >
    );
  }
  return null;
};

const WalletHistoryListItem = ({ statement }) => {
  const status = get(statement, 'status', '');
  const isExpiredStatement = status === 'expired';
  return (
    <StatementContainer>
      <StatementParticulars>
        {getIcon(isExpiredStatement ? 'points-expired' : iconName[statement.type])}
        <Text
          size="s"
          style={{
            marginHorizontal: 16,
            flex: 1,
            textDecorationLine: status === 'expired' ? 'line-through' : 'none',
          }}
          weight="medium"
          family="medium"
          color={status === 'expired' ? 'grey' : 'greyDarker'}
        >
          {statement.particular}
        </Text>
        <Price
          size="s"
          price={statement.amount}
          color={isExpiredStatement ? 'grey' : priceColor[statement.type]}
          type={isExpiredStatement ? 'striked' : 'regular'}
          showSign
          negative={statement.type === 'debit'}
          roundUpTo={2}
        />
      </StatementParticulars>
      <StatementDetails>
        {
          statement.expiresOn ? getExpiryDateView(statement.expiresOn, status) : null
        }
        <Text size="s" color="grey" style={{ marginTop: 12 }}>{statement.customFieldOne}</Text>
        <Text size="s" color="grey" style={{ marginTop: 4, marginBottom: 8 }}>{statement.createdOn}</Text>
        {renderRelatedCredits(statement.relatedCredits)}
      </StatementDetails>
    </StatementContainer>
  );
};

WalletHistoryListItem.propTypes = {
  statement: PropTypes.object.isRequired,
};

export default WalletHistoryListItem;
