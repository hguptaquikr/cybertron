import { Platform } from 'react-native';
import { handle } from 'redux-pack';
import merge from 'lodash/merge';
import walletService from './walletService';

const GET_WALLET_BALANCE = 'GET_WALLET_BALANCE';
const GET_WALLET_HISTORY = 'GET_WALLET_HISTORY';
const APPLY_WALLET = 'APPLY_WALLET';
const REMOVE_WALLET = 'REMOVE_WALLET';
const platform = Platform.OS === 'ios' ? 'ios' : 'android';

const initialState = {
  wallets: {
    TP: {
      totalPoints: 0,
      totalBalance: 0,
      spendableAmount: 0,
    },
  },
  walletHistory: {
    isWalletHistoryLoading: false,
    walletStatement: [],
  },
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_WALLET_BALANCE:
      return handle(state, action, {
        success: (s) => ({
          ...s,
          wallets: merge(s.wallets, payload),
        }),
      });

    case GET_WALLET_HISTORY:
      return handle(state, action, {
        start: (s) => ({
          ...s,
          walletHistory: {
            ...s.walletHistory,
            isWalletHistoryLoading: true,
          },
        }),
        success: (s) => ({
          ...s,
          walletHistory: {
            ...s.walletHistory,
            walletStatement: payload.walletStatement,
          },
        }),
        finish: (s) => ({
          ...s,
          walletHistory: {
            ...s.walletHistory,
            isWalletHistoryLoading: false,
          },
        }),
      });

    default:
      return state;
  }
};

export const getWalletBalance = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_WALLET_BALANCE,
    promise: api.get('/v1/reward/balance/')
      .then(walletService.transformWalletBalanceApi),
  });

export const getWalletHistory = () => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_WALLET_HISTORY,
    promise: api.get('/v1/reward/statement/')
      .then((response) => walletService.transformWalletHistoryApi(response)),
  });

export const applyWallet = (bid, hotelId) => (dispatch, getState, { api }) => {
  const { booking: { room }, ui } = getState();
  return dispatch({
    type: APPLY_WALLET,
    payload: { hotelId },
    promise: api.post('/v5/checkout/wallet/apply/', { bid, ...ui.utmParams, channel: platform })
      .then((response) => walletService.transformApplyRemoveWallet(response, room)),
  });
};

export const removeWallet = (bid, hotelId) => (dispatch, getState, { api }) => {
  const { booking: { room }, ui } = getState();
  return dispatch({
    type: REMOVE_WALLET,
    payload: { hotelId },
    promise: api.post('/v5/checkout/wallet/remove/', { bid, ...ui.utmParams, channel: platform })
      .then((response) => walletService.transformApplyRemoveWallet(response, room)),
  });
};
