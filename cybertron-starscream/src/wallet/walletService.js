import isEmpty from 'lodash/isEmpty';
import moment from 'moment';
import { formatDate } from '../utils/utils';

export default {
  transformWalletBalanceApi({ data }) {
    return data.wallets.reduce((wallets, wallet) => (
      {
        ...wallets,
        [wallet.type]: {
          totalPoints: wallet.total_points,
          totalBalance: wallet.total_balance,
          spendableAmount: wallet.spendable_amount,
        },
      }
    ), {});
  },

  getStatementType(type) {
    switch (type) {
      case 'Cr':
        return 'credit';
      case 'Dr':
        return 'debit';
      case 'Rf':
        return 'refund';
      default:
        return '';
    }
  },
  transformStatement(statement) {
    return {
      particular: statement.particular,
      amount: +statement.amount,
      type: this.getStatementType(statement.type),
      isPerishable: statement.is_perishable,
      expiresOn: statement.expires_on ?
        formatDate(moment(statement.expires_on), 'DD MMM \'YY') : null,
      createdOn: formatDate(moment(statement.created_on), 'DD MMM, H:mm a'),
      status: statement.status.toLocaleLowerCase(),
      customFieldOne: statement.custom_field_one,
      customFieldTwo: statement.custom_field_two,
      relatedCredits: isEmpty(statement.related_credits) ? [] : (
        statement.related_credits.map((credit) => this.transformStatement(credit))
      ),
    };
  },
  transformWalletHistoryApi({ data }) {
    if (isEmpty(data.wallet_statement)) {
      return {
        walletStatement: [],
      };
    }

    return {
      walletStatement: data.wallet_statement.map((s) => this.transformStatement(s)),
    };
  },
  transformApplyRemoveWallet({ data }, selectedRoom) {
    return {
      price: {
        [selectedRoom.type.toLowerCase()]: {
          ratePlans: data.all_rate_plans.reduce((roomPrice, ratePlan) => ({
            ...roomPrice,
            [ratePlan.rate_plan.code]: {
              code: ratePlan.rate_plan.code,
              tag: ratePlan.rate_plan.tag,
              description: ratePlan.rate_plan.description,
              type: ratePlan.rate_plan.code === 'NRP' ? 'Non Refundable' : 'Refundable',
              sellingPrice: +ratePlan.price.sell_price,
              basePrice: +ratePlan.price.base_price,
              totalPrice: +ratePlan.price.net_payable_amount,
              pretaxPrice: +ratePlan.price.pretax_price,
              tax: +ratePlan.price.tax,
              discountAmount: +ratePlan.price.coupon_discount,
              discountPercentage: +ratePlan.price.total_discount_percent,
              roomDiscount: +ratePlan.price.promo_discount,
              treeboPointsUsed: +ratePlan.price.wallet_deduction,
              isUsingTreeboPoints: ratePlan.price.wallet_applied,
              totalUsableTreeboPoints: +ratePlan.price.wallet_deductable_amount,
            },
          }), {}),
          memberDiscount: {
            value: data.selected_rate_plan.price.member_discount,
            isApplied: data.member_discount_applied,
            isAvailable: data.member_discount_available,
          },
        },
      },
    };
  },
};
