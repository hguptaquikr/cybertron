import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StatusBar,
  Animated,
  Platform,
  BackHandler,
  View,
} from 'react-native';
import { Drawer } from 'native-base';
import { theme } from 'leaf-ui/native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-native';
import LottieView from 'lottie-react-native';
import isEmpty from 'lodash/isEmpty';
import moment from 'moment';
import SearchWidget from '../../search/SearchWidget/SearchWidget';
import PopularCities from '../../city/PopularCities/PopularCities';
import Promotion from './Promotion';
import Offers from './Offers';
import BrandCampaignCard from '../../brandCampaign/BrandCampaignCard';
import * as contentActionCreators from '../../content/contentDuck';
import * as walletActionCreators from '../../wallet/walletDuck';
import DrawerContent from './DrawerContent';
import storeHouse from '../../services/storeHouse';
import analyticsService from '../../analytics/analyticsService';

const OnboardingAnimation = require('../Onboarding.json');

const isPlatformIOS = Platform.OS === 'ios';

const landingContentStyle = {
  paddingBottom: 48,
  position: 'absolute',
  top: 0,
  bottom: 0,
};

const animationStyle = {
  flex: 1,
  transform: [{ scale: !isPlatformIOS ? 1.12 : 1 }],
};

class LandingPage extends Component {
  constructor(props) {
    super(props);

    this.searchWidgetHeight = 0;
    this.pageInit.initTime = moment();

    if (isPlatformIOS) {
      StatusBar.setBarStyle('light-content');
    } else {
      StatusBar.setBackgroundColor(theme.color.teal);
      StatusBar.setBarStyle('light-content');
    }
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
  }

  state = {
    headerHeight: 0,
    showOnBoarding: 'None',
    progress: new Animated.Value(0),
    scrollY: new Animated.Value(0),
  };

  componentDidMount() {
    this.handleShowOnboardingAnimation();
    this.getLandingContent();
    this.props.walletActions.getWalletBalance();
    analyticsService.segmentScreenViewed('Home Page Viewed');
  }

  shouldComponentUpdate(nextProps) {
    const { history, location } = nextProps;
    if (history && location && location.state === 'reload') {
      this.getLandingContent();
      history.replace({ ...location, state: null });
    }
    return true;
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  onScroll = Animated.event(
    [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
    {
      listener: ({ nativeEvent: { contentOffset: { y } } }) => {
        if (this.state.headerHeight !== 0) {
          if (y <= this.state.headerHeight - 64) {
            if (isPlatformIOS) {
              StatusBar.setBarStyle('light-content');
            } else {
              StatusBar.setBackgroundColor(theme.color.teal);
            }
          } else if (isPlatformIOS) {
            StatusBar.setBarStyle('dark-content');
          } else {
            StatusBar.setBackgroundColor(theme.color.black);
          }
        }
      },
    },
  );

  setShowOnBoarding = () => {
    storeHouse.save('show_on_boarding', false);
    this.setState({ showOnBoarding: false });
    analyticsService.segmentTrackEvent('Onboarding ended');
  };

  getLandingContent = () => {
    const { contentActions } = this.props;
    contentActions.getLandingContent();
    contentActions.getCitiesList();
    contentActions.getSpecialRateHotels();
  };

  handleShowOnboardingAnimation = () => {
    storeHouse.get('show_on_boarding').then((showOnBoarding) => {
      if (showOnBoarding) {
        this.setState({ showOnBoarding: true });
        analyticsService.segmentTrackEvent('Onboarding Started');
        Animated.timing(this.state.progress, {
          delay: 100,
          toValue: 1,
          duration: 7500,
        }).start(this.setShowOnBoarding);
      } else {
        this.setState({ showOnBoarding: false });
      }
    }).catch(() => this.setState({ showOnBoarding: false }));
  }

  handleBack = () => {
    const { history } = this.props;
    if (history.index !== 0) {
      history.go(-history.length);
    } else {
      BackHandler.exitApp();
    }
    return false;
  };

  pageInit = {
    initTime: 0,
    hasInteractivityEventBeenSent: false,
  };

  closeDrawer = () => {
    if (isPlatformIOS) {
      StatusBar.setBarStyle('light-content');
    }
    if (this.drawer) {
      this.drawer._root.close();
    }
  };

  closeWidget = () => {
    if (this.scrollViewRef && this.scrollViewRef._component) {
      this.scrollViewRef._component.scrollTo({ x: 0, y: this.searchWidgetHeight, animated: true });
    }
  }

  openDrawer = () => {
    if (isPlatformIOS) {
      StatusBar.setBarStyle('dark-content');
    }
    if (this.drawer) {
      this.drawer._root.open();
    }
    analyticsService.segmentTrackEvent('Menu Clicked');
  };

  openWidget = () => {
    if (this.scrollViewRef && this.scrollViewRef._component) {
      this.scrollViewRef._component.scrollTo({ x: 0, y: 0, animated: true });
    }
  }

  render() {
    const {
      content: {
        offers,
        popularCities,
        promotionSection: { promotionImageUrl, navigateTo, isInAppUrl, aspectRatio },
      },
      history,
    } = this.props;
    const { showOnBoarding, progress, scrollY, headerHeight } = this.state;

    if (showOnBoarding === 'None') {
      return null;
    }

    const animatedHeaderHeight = headerHeight === 0 ? 0 :
      scrollY.interpolate({
        inputRange: [0, headerHeight - 64, headerHeight],
        outputRange: [headerHeight, 64, 64],
      });

    const animatedShadowColor = headerHeight === 0 ? theme.color.transparent :
      scrollY.interpolate({
        inputRange: [0, headerHeight - 64, headerHeight - 64],
        outputRange: [theme.color.transparent, theme.color.transparent, theme.color.greyLighter],
      });

    if (showOnBoarding) {
      return (
        <LottieView
          style={animationStyle}
          source={OnboardingAnimation}
          imageAssetsFolder="json/Onboarding.json"
          progress={progress}
        />
      );
    }

    if (!this.pageInit.hasInteractivityEventBeenSent && !isEmpty(popularCities)) {
      const loadingTime = moment().diff(this.pageInit.initTime, 'milliseconds');
      analyticsService.segmentTrackEvent('Landing Page Interactivity Time',
        { time: loadingTime })
        .then(this.pageInit.hasInteractivityEventBeenSent = true);
    }

    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<DrawerContent history={history} closeDrawer={this.closeDrawer} />}
        onClose={this.closeDrawer}
      >
        <Animated.ScrollView
          showsVerticalScrollIndicator={false}
          automaticallyAdjustContentInsets={false}
          keyboardShouldPersistTaps="handled"
          style={landingContentStyle}
          contentContainerStyle={{ paddingTop: headerHeight }}
          scrollEventThrottle={16}
          onScroll={this.onScroll}
          ref={(componentRef) => { this.scrollViewRef = componentRef; }}
        >
          <View onLayout={(e) => { this.searchWidgetHeight = e.nativeEvent.layout.y - 80; }} />
          {
            !isEmpty(popularCities) &&
              <PopularCities
                cities={popularCities}
                history={history}
              />
          }
          {
            !isEmpty(popularCities) &&
              <BrandCampaignCard
                history={history}
                callingPage="Home"
              />
          }
          {
            !!promotionImageUrl &&
              <Promotion
                history={history}
                imageUrl={promotionImageUrl}
                navigateTo={navigateTo}
                isInAppUrl={isInAppUrl}
                aspectRatio={aspectRatio}
              />
          }
          {
            !isEmpty(offers) &&
              <Offers offers={offers} />
          }
        </Animated.ScrollView>
        {
          headerHeight === 0 ?
            <SearchWidget
              navigationIcon="menu"
              getHeight={(height) => this.setState({ headerHeight: height })}
            /> :
            <Animated.View
              removeClippedSubviews
              style={{
                height: animatedHeaderHeight,
                overflow: 'hidden',
                borderBottomWidth: 1,
                borderBottomColor: animatedShadowColor,
              }}
            >
              <SearchWidget
                navigationIcon="menu"
                onNavigationIconPress={this.openDrawer}
                onHeaderTitlePress={this.openWidget}
                getHeight={(height) => this.setState({ headerHeight: height })}
                scrollPostion={scrollY}
                widgetHeight={headerHeight}
              />
            </Animated.View>
        }
      </Drawer>
    );
  }
}

LandingPage.propTypes = {
  content: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  walletActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  walletActions: bindActionCreators(walletActionCreators, dispatch),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(LandingPage));
