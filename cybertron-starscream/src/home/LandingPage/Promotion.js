import React from 'react';
import PropTypes from 'prop-types';
import { Linking } from 'react-native';
import styled from 'styled-components/native';
import ImageLoad from '../../components/ImageLoad/ImageLoad';
import LandingPageLayout from './LandingPageLayout';

const PromotionImage = styled(ImageLoad)`
  marginHorizontal: 16;
  aspectRatio: ${({ aspectRatio }) => aspectRatio};
  borderRadius: 2;
`;

const View = styled.View``;

const onSectionPress = (navigateTo, isInAppUrl, history) => () => {
  if (navigateTo) {
    if (isInAppUrl) {
      history.push(navigateTo);
    } else {
      Linking.canOpenURL(navigateTo).then((supported) => {
        if (supported) {
          return Linking.openURL(navigateTo).catch(() => null);
        }
        return Promise.reject('Supporting app not present');
      });
    }
  }
};

const Promotion = ({ imageUrl, navigateTo, isInAppUrl, history, aspectRatio }) => {
  const Section = navigateTo ? LandingPageLayout.TouchableSection : LandingPageLayout.Section;
  return (
    <Section
      onPress={onSectionPress(navigateTo, isInAppUrl, history)}
      testID="promotion-view"
      accessibilityLabel="promotion-view"
    >
      <View>
        <PromotionImage
          source={{ uri: imageUrl }}
          aspectRatio={aspectRatio}
        />
      </View>
    </Section>
  );
};

Promotion.propTypes = {
  history: PropTypes.object.isRequired,
  imageUrl: PropTypes.string.isRequired,
  navigateTo: PropTypes.string,
  isInAppUrl: PropTypes.bool,
  aspectRatio: PropTypes.number,
};

Promotion.defaultProps = {
  navigateTo: '',
  isInAppUrl: false,
  aspectRatio: 2,
};

export default Promotion;
