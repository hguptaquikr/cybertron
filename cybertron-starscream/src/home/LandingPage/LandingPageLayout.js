import styled from 'styled-components/native';
import TouchableComponent from '../../components/Touchable/TouchableComponent';

export default {
  Section: styled.View`
    paddingVertical: 24;
  `,
  TouchableSection: TouchableComponent.extend`
    paddingVertical: 24;
  `,
  SectionTitle: styled.View`
    marginHorizontal: 16;
  `,
  SectionSubtitle: styled.View`
    marginHorizontal: 16;
    marginTop: 8;
  `,
};
