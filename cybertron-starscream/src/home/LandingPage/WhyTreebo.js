import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import ImageLoad from '../../components/ImageLoad/ImageLoad';
import Text from '../../components/NewLeaf/Text/Text';
import LandingPageLayout from './LandingPageLayout';
import { horizontalScale } from '../../utils/utils';

const WhyTreeboList = styled.View`
  flexDirection: row;
  justifyContent: space-between;
  alignItems: center;
  paddingHorizontal: 16;
  marginTop: ${({ showTitle }) => (showTitle ? 24 : 0)};
`;

const WhyTreeboItem = styled.View`
  justifyContent: center;
  alignItems: center;
  width: ${horizontalScale(72)};
`;

const WhyTreeboImage = styled(ImageLoad)`
  width: ${horizontalScale(72)};
`;

const WhyTreebo = ({ whyTreebo, showTitle }) => (
  <LandingPageLayout.Section>
    {
      showTitle ? (
        <LandingPageLayout.SectionTitle>
          <Text size="l" weight="bold" color="greyDarker" family="bold">
          Our Assurance
          </Text>
        </LandingPageLayout.SectionTitle>
      ) : null
    }
    <WhyTreeboList showTitle={showTitle}>
      {
        whyTreebo.map((promise) => (
          <WhyTreeboItem key={promise.title}>
            <WhyTreeboImage
              source={{ uri: promise.imageUrl }}
            />
            <Text size="s" color="greyDarker" weight="medium" family="medium">
              {promise.title}
            </Text>
          </WhyTreeboItem>
        ))
      }
    </WhyTreeboList>
  </LandingPageLayout.Section>
);

WhyTreebo.propTypes = {
  whyTreebo: PropTypes.array.isRequired,
  showTitle: PropTypes.bool,
};

WhyTreebo.defaultProps = {
  showTitle: true,
};

export default WhyTreebo;
