import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components/native';
import Carousel from '../../components/Carousel/Carousel';
import LandingPageLayout from './LandingPageLayout';
import Text from '../../components/NewLeaf/Text/Text';

const OffersCarousel = styled(Carousel)`
  paddingLeft: 16;
  marginTop: 24;
`;

const offersImageCss = css`
  width: 272;
  aspectRatio: 1.8;
`;

class Offers extends Component {
  getOffersCarouselDataSet = () => {
    const { offers } = this.props;
    return offers.map((offer) => ({
      url: offer.offerUrl,
      key: offer.description,
    }));
  };

  render() {
    return (
      <LandingPageLayout.Section>
        <LandingPageLayout.SectionTitle>
          <Text
            size="l"
            weight="bold"
            color="greyDarker"
          >
            Offers
          </Text>
        </LandingPageLayout.SectionTitle>
        <LandingPageLayout.SectionSubtitle>
          <Text
            size="s"
            weight="normal"
            color="greyDarker"
          >
            Pinky Promise, these are the best
          </Text>
        </LandingPageLayout.SectionSubtitle>
        <OffersCarousel
          data={this.getOffersCarouselDataSet()}
          imageCss={offersImageCss}
        />
      </LandingPageLayout.Section>
    );
  }
}

Offers.propTypes = {
  offers: PropTypes.array.isRequired,
};

export default Offers;
