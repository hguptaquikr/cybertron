import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Linking } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Spacer, Divider, Flex } from 'leaf-ui/native';
import styled from 'styled-components/native';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import * as userActionCreators from '../../user/userDuck';
import * as toastActionCreators from '../../toast/toastDuck';
import color from '../../theme/color';
import userService from '../../user/userService';
import Text from '../../components/NewLeaf/Text/Text';
import config from '../../config';
import analyticsService from '../../analytics/analyticsService';
import { capitalizeFirstLetter, horizontalScale } from '../../utils/utils';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import storeHouse from '../../services/storeHouse';
import local from '../../config/local';

const pkg = require('../../../package.json');
const ProfileImageSource = require('../sidebar_generic_profile_Illustration.png');
const LoginImageSource = require('../../auth/login_menu.png');

const View = styled.View``;

const TouchableOpacity = styled.TouchableOpacity`
  padding: 4px;
  marginTop: 8;
  justifyContent: center;
  alignItems: center;
  width: 32px;
  height: 32px;
`;

const RootView = styled.View`
  flex: 1;
`;

const LoggedOutBackgroundView = styled.View`
  paddingVertical: 16;
  paddingHorizontal: 8;
  backgroundColor: ${color.disabledGreyOpacity50};
`;

const LoggedInBackgroundView = styled.View`
  paddingVertical: 16;
  paddingHorizontal: 8;
  minHeight: 180;
  backgroundColor: ${color.white};
`;

const HorizontalView = styled.View`
  flexDirection: row;
`;

const HeaderTextView = styled.View`
  flex: 1;
  justifyContent: center;
  alignItems: center;
  marginTop: 12;
`;

const LoginBackgroundImage = styled.Image`
  marginTop: 24;
  justifyContent: center;
  alignItems: center;
  alignSelf: center;
`;

const ProfileImage = styled.Image`
  width: ${horizontalScale(48)};
  aspectRatio: 1;
  marginRight: 16;
`;

const ButtonContainer = styled.View`
  marginTop: 16;
  marginHorizontal: 8;
`;

const LoggedInContentView = styled.View`
  flexDirection: row;
  marginTop: 20;
  alignItems: center;
  paddingHorizontal: 8;
`;

const DrawerContentView = styled.View`
  flex: 1;
  paddingHorizontal: 16;
  backgroundColor: ${color.white};
`;

const ItemView = styled.TouchableOpacity`
  paddingVertical: 16;
  flexDirection: row;
  justifyContent: space-between;
  alignItems: center;
`;

const BottomContentView = styled.View`
  paddingHorizontal: 16;
  paddingVertical: 16;
  backgroundColor: ${color.white};
`;

const FlexRow = styled.View`
  flexDirection: row;
  flex: 1;
  paddingHorizontal: 24;
`;

const Tag = styled.View`
  paddingHorizontal: 4;
  paddingVertical: 2;
  marginLeft: 12;
  borderRadius: 100;
  minWidth: 35;
  backgroundColor: ${({ backgroundColor }) => backgroundColor || color.blue};
  alignItems: center;
  justifyContent: center;
`;

class DrawerContent extends Component {
  state = {
    isLoggedIn: 'None',
  };

  componentWillMount() {
    userService.isLoggedIn().then((isLoggedId) => {
      if (isLoggedId) {
        this.setState({ isLoggedIn: true });
      } else {
        this.setState({ isLoggedIn: false });
      }
    }).catch(() => this.setState({ isLoggedIn: false }));
  }

  onLogoutClick = () => {
    const { userActions: { userLogout }, toastActions: { showToast } } = this.props;
    userLogout().then((res) => {
      if (res.error) {
        showToast('Something went wrong, Try again.');
      } else {
        showToast('Logged out successfully');
        this.setState({ isLoggedIn: false });
      }
    });
  };

  openTreeboRewards = () => {
    const { history } = this.props;
    const { isLoggedIn } = this.state;
    storeHouse.get('is_wallet_intro_shown')
      .then((isIntroShown) => {
        if (isLoggedIn && isIntroShown) {
          history.push('/wallet/wallet-history/');
        } else {
          history.push('/wallet/intro/');
        }
      });
  };

  openContactUs = () => {
    const eventProps = { page: 'Home' };
    analyticsService.segmentTrackEvent('Contact Us Clicked', eventProps);
    const url = `tel:${config.customerCareNumber}`;
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        return Linking.openURL(url).catch(() => null);
      }
      return Promise.reject('Supporting app not present');
    });
  };

  openFaqs = () => {
    const { history } = this.props;
    history.push('/faq/');
    // const url = `${config.baseUrl}/faq/`;
    // Linking.canOpenURL(url).then((supported) => {
    //   if (supported) {
    //     return Linking.openURL(url).catch(() => null);
    //   }
    //   return Promise.reject('Supporting app not present');
    // });
  };

  openLogin = () => {
    const { history } = this.props;
    const eventProps = { page: 'Home' };
    analyticsService.segmentTrackEvent('Login Clicked', eventProps);
    history.push('/login/');
  };

  openBookingHistory = () => {
    const { history } = this.props;
    const eventProps = { page: 'Home' };
    analyticsService.segmentTrackEvent('View Booking History Clicked', eventProps);
    history.push('/account/booking-history/');
  };

  render() {
    const { userDetails, wallet: { wallets }, walletType, closeDrawer } = this.props;
    const { isLoggedIn } = this.state;

    // if isLoggedin state is not fetched then don't render anything
    if (isLoggedIn === 'None') {
      return null;
    }

    return (
      <RootView>
        {
          isLoggedIn ?
            (
              <LoggedInBackgroundView>
                <TouchableOpacity
                  onPress={closeDrawer}
                  testID="icon-menu-close"
                  accessibilityLabel="icon-menu-close"
                >
                  <Spacer padding={[2]}>
                    {getIcon('close')}
                  </Spacer>
                </TouchableOpacity>
                <LoggedInContentView>
                  <ProfileImage source={ProfileImageSource} />
                  <View>
                    {
                      userDetails.firstName && (
                        <Text
                          size="l"
                          color="greyDarker"
                          weight="bold"
                          family="medium"
                        >
                          {capitalizeFirstLetter(userDetails.firstName)}
                        </Text>
                      )
                    }
                    <Text
                      size="s"
                      color="greyDarker"
                      weight="normal"
                    >
                      {userDetails.mobile}
                    </Text>
                  </View>
                </LoggedInContentView>
              </LoggedInBackgroundView>
            )
            : (
              <LoggedOutBackgroundView>
                <HorizontalView>
                  <TouchableOpacity
                    onPress={closeDrawer}
                    testID="icon-menu-close"
                    accessibilityLabel="icon-menu-close"
                  >
                    <Spacer padding={[2]}>
                      {getIcon('close')}
                    </Spacer>
                  </TouchableOpacity>
                  <HeaderTextView>
                    <Text
                      size="s"
                      color="greyDarker"
                      weight="normal"
                    >
                      Manage your bookings
                    </Text>
                    <Text
                      size="s"
                      color="greyDarker"
                      weight="medium"
                    >
                      anytime anywhere
                    </Text>
                  </HeaderTextView>
                </HorizontalView>
                <LoginBackgroundImage source={LoginImageSource} />
                <ButtonContainer>
                  <Button
                    block
                    onPress={this.openLogin}
                    testID="login-button"
                    accessibilityLabel="login-button"
                  >
                    <Text
                      size="s"
                      weight="medium"
                      color="white"
                    >
                      LOGIN
                    </Text>
                  </Button>
                </ButtonContainer>
              </LoggedOutBackgroundView>
            )
        }
        <DrawerContentView>
          {
            isLoggedIn ? (
              <View>
                <ItemView onPress={this.openBookingHistory} testID="booking-history" accessibilityLabel="booking-history">
                  {getIcon('manage-bookings', color.warmGrey)}
                  <FlexRow>
                    <Text
                      size="m"
                      color="greyDarker"
                      weight="normal"
                    >
                      Booking History
                    </Text>
                  </FlexRow>
                  {getIcon('arrow-next', color.warmGrey)}
                </ItemView>
                <Divider />
              </View>
            ) : null
          }
          <ItemView onPress={this.openTreeboRewards} testID="treebo-rewards" accessibilityLabel="treebo-rewards">
            {getIcon('menu-rewards', color.warmGrey)}
            <FlexRow>
              <Text
                size="m"
                color="greyDarker"
              >
                Treebo Rewards
              </Text>
              {
                isLoggedIn && !isEmpty(wallets) ? (
                  <Tag backgroundColor={color.blue}>
                    <Text
                      size="xxs"
                      weight="medium"
                      color="white"
                    >
                      &#x20b9;{get(wallets, `${walletType}.totalBalance`, 0)}
                    </Text>
                  </Tag>
                ) : (
                  <Tag backgroundColor={color.orangeYellow}>
                    <Text
                      size="xxs"
                      weight="medium"
                      color="white"
                    >
                      NEW
                    </Text>
                  </Tag>
                )
              }
            </FlexRow>
            {getIcon('arrow-next', color.warmGrey)}
          </ItemView>
          <Divider />
          <ItemView onPress={this.openContactUs} testID="contact-us" accessibilityLabel="contact-us">
            {getIcon('menu_contact_us', color.warmGrey)}
            <FlexRow>
              <Text
                size="m"
                color="greyDarker"
                weight="normal"
              >
                Contact Us
              </Text>
            </FlexRow>
            {getIcon('arrow-next', color.warmGrey)}
          </ItemView>
          <Divider />
          <ItemView onPress={this.openFaqs} testID="open-faqs" accessibilityLabel="open-faqs">
            {getIcon('menu-faq', color.warmGrey)}
            <FlexRow>
              <Text
                size="m"
                color="greyDarker"
                weight="normal"
              >
                FAQs
              </Text>
            </FlexRow>
            {getIcon('arrow-next', color.warmGrey)}
          </ItemView>
        </DrawerContentView>
        <BottomContentView>
          {
            __DEV__ ?
              <Spacer padding={[1, 0]}>
                <Text
                  color="grey"
                  size="xs"
                  weight="semibold"
                >
                Development Build - v{pkg.version}
                </Text>
                <Text
                  color="grey"
                  size="xs"
                  weight="semibold"
                >
                  {local.apiUrl}
                </Text>
              </Spacer>
            :
              <Flex alignSelf="center">
                <Spacer padding={[1, 0]}>
                  <Text
                    color="grey"
                    size="xs"
                    weight="semibold"
                  >
                    v{pkg.version}
                  </Text>
                </Spacer>
              </Flex>
          }
          {isLoggedIn &&
            <Button
              block
              onPress={this.onLogoutClick}
              testID="logout-button"
              accessibilityLabel="logout-button"
            >
              <Text
                size="m"
                weight="medium"
                color="white"
              >
                LOGOUT
              </Text>
            </Button>
          }
        </BottomContentView>
      </RootView>
    );
  }
}

DrawerContent.propTypes = {
  history: PropTypes.object.isRequired,
  userDetails: PropTypes.object,
  wallet: PropTypes.object,
  walletType: PropTypes.string,
  userActions: PropTypes.object.isRequired,
  toastActions: PropTypes.object.isRequired,
  closeDrawer: PropTypes.func,
};

DrawerContent.defaultProps = {
  walletType: 'TP',
};

const mapStateToProps = (state) => ({
  userDetails: state.user.profile,
  wallet: state.wallet,
});

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DrawerContent);
