import { Platform } from 'react-native';
import appsFlyer from 'react-native-appsflyer';
import config from '../config';
import storeHouse from '../services/storeHouse';
import { SegmentAnalyticsManager } from '../nativeModules';

const pkg = require('../../package.json');

const isPlatformAndroid = Platform.OS === 'android';

const appMeta = {
  channel: isPlatformAndroid ? 'Android App' : 'iOS App',
  appVersion: pkg.version,
};

export default {

  initService() {
    appsFlyer.onInstallConversionData(
      (responseObject) => {
        storeHouse.save('appsflyer_data', responseObject.data);
      },
    );

    const options = {
      devKey: config.appsflyerKey,
      appId: config.appleAppId,
      isDebug: __DEV__,
    };

    appsFlyer.initSdk(options, async (success) => {
      storeHouse.save('appsflyer_init_result', success);
    }, (error) => {
      storeHouse.save('appsflyer_init_result', error);
    });

    if (isPlatformAndroid && typeof appsFlyer.setCollectIMEI === 'function') {
      appsFlyer.setCollectIMEI(false, () => {});
    }
  },

  async segmentTrackEvent(eventName, props, trackNetworkSpeed = false) {
    storeHouse.get('appsflyer_data')
      .then((data) => {
        let propsObject = {
          ...appMeta,
          ...data,
          ...props,
        };
        if (trackNetworkSpeed) {
          propsObject = {
            ...propsObject,
            network: '',
          };
        }
        SegmentAnalyticsManager.trackEvent(eventName, propsObject);
        if (isPlatformAndroid) {
          appsFlyer.trackEvent(eventName, propsObject, () => { }, () => { });
          if (__DEV__) {
            console.log('🛤', eventName, JSON.stringify(propsObject, ':', 4));
          }
        }
      });
  },

  async segmentIdentify(userID, props) {
    storeHouse.get('appsflyer_data')
      .then((data) => {
        const propsObject = {
          ...appMeta,
          ...data,
          ...props,
        };
        SegmentAnalyticsManager.identify(userID.toString(), propsObject);
        appsFlyer.setCustomerUserId(userID.toString(), () => { });
      });
  },

  async segmentScreenViewed(eventName, props) {
    storeHouse.get('appsflyer_data')
      .then((data) => {
        const propsObject = {
          ...appMeta,
          ...data,
          ...props,
        };
        SegmentAnalyticsManager.trackEvent(eventName, propsObject);
        SegmentAnalyticsManager.screenView(eventName, propsObject);
        if (__DEV__ && isPlatformAndroid) {
          console.log('🛤', eventName, JSON.stringify(propsObject, ':', 4));
        }
      });
  },
};
