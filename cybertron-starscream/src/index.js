/* eslint-disable no-undef */
import React, { Component } from 'react';
import codePush from 'react-native-code-push';
import { Provider } from 'react-redux';
import { NativeRouter as Router } from 'react-router-native';
import Routes from './routes';
import createStore from './store/createStore';
import Wrapper from './bootstrap/Wrapper/Wrapper';
import analyticsService from './analytics/analyticsService';
import abMiddleware from './ab/abMiddleware';

class App extends Component {
  constructor() {
    super();
    const store = createStore()(this.onRehydrate);
    this.state = {
      store,
      rehydrated: false,
    };
    analyticsService.initService();
    abMiddleware();
    if (__DEV__) {
      // eslint-disable-next-line
      console.disableYellowBox = true;
    }
  }

  onRehydrate = () => {
    this.setState({ rehydrated: true });
  };

  render() {
    const { rehydrated, store } = this.state;
    return rehydrated ? (
      <Provider store={store}>
        <Router>
          <Wrapper>
            <Routes />
          </Wrapper>
        </Router>
      </Provider>
    ) : null;
  }
}

const CodePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.ON_NEXT_RESUME,
};

export default codePush(CodePushOptions)(App);
