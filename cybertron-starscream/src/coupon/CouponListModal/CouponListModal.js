import React, { Component } from 'react';
import { ActivityIndicator, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Form, Button, Card, Spacer } from 'leaf-ui/native';
import Modal from '../../components/Modal/Modal';
import CouponListItem from '../CouponListItem/CouponListItem';
import Text from '../../components/NewLeaf/Text/Text';
import TextInput from '../../components/NewLeaf/TextInput/TextInput';
import List from '../../components/List/List';

const RootContainer = styled.View`
  backgroundColor: white;
  flex: 1;
`;

const InputContainer = styled.View`
  padding: 16px;
`;

const CouponList = styled(List)`
  padding: 16px;
`;

class CouponListModal extends Component {
  state = {
    appliedCouponCode: this.props.coupon.appliedCouponCode,
    isCouponApplying: false,
  };

  componentWillReceiveProps(nextProps) {
    const { props } = this;
    if (props.coupon.appliedCouponCode !== nextProps.coupon.appliedCouponCode) {
      this.setState({ appliedCouponCode: nextProps.coupon.appliedCouponCode });
    }
  }

  onCouponClick = async (formProps, couponCode, e) => {
    formProps.setFieldTouched('couponCode', true);
    await formProps.setFieldValue('couponCode', couponCode);
    formProps.handleSubmit(e);
    this.setState({ appliedCouponCode: couponCode });
  };

  onApplyCoupon = (couponCode, formActions) => {
    const { checkoutActions, ratePlan, bid, isUsingTreeboPoints } = this.props;
    if (bid) {
      return checkoutActions.getCheckoutDetails.call(this, null, ratePlan, isUsingTreeboPoints)
        .then(({ payload }) => {
          if (!payload.isError) {
            this.applyCoupon(couponCode, formActions);
          }
        });
    }
    return this.applyCoupon(couponCode, formActions);
  };

  applyCoupon = (couponCode, formActions) => {
    const { couponActions, isUsingTreeboPoints } = this.props;
    this.setState({ isCouponApplying: true });
    return couponActions.applyCoupon(couponCode, isUsingTreeboPoints).then((res) => {
      if (res.error) {
        formActions.setFieldError('couponCode', res.error.msg || 'Invalid Coupon Code');
      }
      this.setState({ isCouponApplying: false });
    }).catch(() => {
      formActions.setFieldError('couponCode', 'Invalid Coupon Code');
      this.setState({ isCouponApplying: false });
    });
  };

  renderCouponListItem = (formProps) => ({ item }) => (<CouponListItem
    coupon={item}
    key={item.code}
    onCouponClick={(e) => this.onCouponClick(formProps, item.code, e)}
    appliedCouponCode={this.state.appliedCouponCode}
  />);

  render() {
    const {
      coupon: {
        results,
        sortedCouponCodes,
        showCouponModal,
        isCouponResultsLoading,
      },
      couponActions,
    } = this.props;
    const { isCouponApplying } = this.state;
    const coupons = sortedCouponCodes.map((sortedCouponCode) => results[sortedCouponCode]);
    return (
      <Modal
        middle="Apply Coupon"
        isOpen={showCouponModal}
        onClose={() => {
          this.setState({ appliedCouponCode: this.props.coupon.appliedCouponCode });
          couponActions.showCouponModal(false);
        }}
        iosBarStyle="dark-content"
      >
        <Form
          initialValues={{
            couponCode: this.props.coupon.appliedCouponCode,
          }}
          validationSchema={
            Form.validation.object().shape({
              couponCode: Form.validation.string().required('Please enter a valid coupon code'),
            })
          }
          onSubmit={(values, actions) => {
            actions.setSubmitting(true);
            this.onApplyCoupon(values.couponCode, actions);
          }}
          render={(formProps) => (
            <RootContainer
              testID="coupon-modal"
              accessibilityLabel="coupon-modal"
            >
              <InputContainer>
                <TextInput
                  innerRef={(ref) => { this._couponInputRef = ref; }}
                  name="couponCode"
                  label="Enter Coupon Code"
                  autoCapitalize="none"
                  autoCorrect={false}
                  testID="enter-coupon-code-field"
                  accessibilityLabel="enter-coupon-code-field"
                  error="Please enter a valid coupon code"
                  showPlaceholder
                />
              </InputContainer>
              {
                isCouponResultsLoading ?
                  <ScrollView>
                    <ActivityIndicator />
                  </ScrollView>
                :
                  <CouponList
                    data={coupons}
                    renderItem={this.renderCouponListItem(formProps)}
                    keyExtractor={(item) => item.code}
                  />
              }
              <Card elevation={1}>
                <Spacer padding={[1]}>
                  <Button
                    block
                    disabled={
                    !formProps.values.couponCode || formProps.values.couponCode.length === 0
                  }
                    onPress={formProps.handleSubmit}
                    testID="apply-coupon-button"
                    accessibilityLabel="apply-coupon-button"
                  >
                    <Text size="m" weight="medium" family="medium" color="white">
                      { !isCouponApplying ? 'APPLY' : 'APPLYING...' }
                    </Text>
                  </Button>
                </Spacer>
              </Card>
            </RootContainer>
          )}
        />
      </Modal>
    );
  }
}

CouponListModal.propTypes = {
  coupon: PropTypes.object.isRequired,
  couponActions: PropTypes.object.isRequired,
  isUsingTreeboPoints: PropTypes.bool.isRequired,
  ratePlan: PropTypes.string.isRequired,
  bid: PropTypes.string,
  checkoutActions: PropTypes.object.isRequired,
};

CouponListModal.defaultProps = {
  bid: '',
};

export default CouponListModal;
