import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import { Spacer, Flex, Card } from 'leaf-ui/native';
import TouchableComponent from '../../components/Touchable/TouchableComponent';
import Price from '../../components/NewLeaf/Price/Price';
import Text from '../../components/NewLeaf/Text/Text';
import color from '../../theme/color';
import CouponListModal from '../CouponListModal/CouponListModal';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import analyticsService from '../../analytics/analyticsService';
import * as couponActionCreators from '../../coupon/couponDuck';
import * as checkoutActionCreators from '../../checkout/checkoutDuck';

const removeCoupon = (couponActions, checkoutActions, room) => {
  couponActions.removeCoupon(room.price.isUsingTreeboPoints);
};

const getCouponDiscount = (price, selectedRoom) => {
  const selectedRoomPrice = price[selectedRoom.type.toLowerCase()];
  const { ratePlans } = selectedRoomPrice;
  const roomPrice = ratePlans[selectedRoom.plan];
  return get(roomPrice, 'couponDiscount', 0);
};

const Coupon = ({
  coupon,
  couponActions,
  checkoutActions,
  hotel,
  isDisabled,
  room,
  booking,
  price,
}) => (
  <Spacer padding={[1]}>
    {
      <Card>
        <Spacer padding={[2]}>
          <Flex flexDirection="row" alignItems="center">
            <Spacer padding={[0, 0, 0.5, 0]}>
              {getIcon('coupon_code', color.warmGrey, 24, 24)}
            </Spacer>
            <Spacer margin={[0, 0, 0, 2]}>
              <Flex flexDirection="row">
                <Spacer margin={[0, 0.5, 0, 0]}>
                  {
                    isEmpty(coupon.appliedCouponCode) ?
                      <Text
                        size="s"
                        color="greyDarker"
                        weight="normal"
                        applied={!isEmpty(coupon.appliedCouponCode)}
                        testID="coupon-applied-text"
                        accessibilityLabel="coupon-applied-text"
                      >
                        View Available Coupons.
                      </Text> :
                      <Flex flexDirection="row">
                        <Text
                          size="s"
                          color="greyDarker"
                          weight="medium"
                        >
                          {coupon.appliedCouponCode}
                        </Text>
                        <Text
                          size="s"
                          color="greyDarker"
                          weight="normal"
                        >
                          {' '}Applied.
                        </Text>
                      </Flex>
                  }
                </Spacer>
                <TouchableComponent
                  onPress={
                    isDisabled ? null : () => {
                      if (isEmpty(coupon.appliedCouponCode)) {
                        analyticsService.segmentTrackEvent('Add Coupon Link Clicked');
                        couponActions.showCouponModal(true);
                      } else {
                        removeCoupon(couponActions, checkoutActions, room);
                      }
                  }}
                  testID="apply-remove-coupon-button"
                  accessibilityLabel="apply-remove-coupon-button"
                >
                  <Text
                    size="s"
                    weight="medium"
                    color={isDisabled ? 'greyDark' : 'blue'}
                    testID="apply-remove-coupon-text"
                    accessibilityLabel="apply-remove-coupon-text"
                  >
                    {
                      isEmpty(coupon.appliedCouponCode)
                        ? 'Apply here' : 'Remove'
                    }
                  </Text>
                </TouchableComponent>
              </Flex>
              {
                !isEmpty(coupon.appliedCouponCode) &&
                <Flex flexDirection="row">
                  <Text size="s" color="grey">
                    You save
                  </Text>
                  <Price
                    price={getCouponDiscount(price, room)}
                    size="s"
                    color="grey"
                    weight="normal"
                    roundUpTo={0}
                  />
                </Flex>
              }
            </Spacer>
          </Flex>
        </Spacer>
      </Card>
      }
    <CouponListModal
      coupon={coupon}
      couponActions={couponActions}
      hotel={hotel}
      isUsingTreeboPoints={room.price.isUsingTreeboPoints}
      checkoutActions={checkoutActions}
      ratePlan={room.plan}
      bid={booking.id}
    />
  </Spacer>
);

Coupon.propTypes = {
  coupon: PropTypes.object.isRequired,
  couponActions: PropTypes.object.isRequired,
  checkoutActions: PropTypes.object.isRequired,
  hotel: PropTypes.object,
  price: PropTypes.object,
  isDisabled: PropTypes.bool.isRequired,
  room: PropTypes.object,
  booking: PropTypes.object,
};

const mapDispatchToProps = (dispatch) => ({
  couponActions: bindActionCreators(couponActionCreators, dispatch),
  checkoutActions: bindActionCreators(checkoutActionCreators, dispatch),
});

const mapStateToProps = (state, ownProps) => ({
  hotel: state.hotel.results[ownProps.hotelId] || state.hotel.results[0],
  price: state.price.results[ownProps.hotelId] || state.price.results[0],
  search: state.search,
  content: state.content,
  coupon: state.coupon,
  booking: state.booking,
});

export default connect(mapStateToProps, mapDispatchToProps)(Coupon);
