export default {
  transformCouponResultsApi({ data }) {
    return {
      results: data.reduce((obj, coupon) => ({
        ...obj,
        [coupon.code]: {
          code: coupon.code,
          amount: coupon.coupon_amount,
          tagline: coupon.tagline,
          description: coupon.description,
          expiry: coupon.expiry,
        },
      }), {}),
      sortedCouponCodes: data.map((coupon) => coupon.code),
    };
  },

  transformCouponApplyApi({ data }, { room }, couponCode = '') {
    return {
      coupon: {
        results: {
          [couponCode]: {
            description: data.coupon_desc,
          },
        },
        appliedCouponCode: couponCode,
      },
      booking: {
        isPrepaidOnly: 'is_prepaid' in data ? data.is_prepaid : false,
      },
      price: {
        [room.type]: {
          ratePlans: data.all_rate_plans.reduce((result, ratePlan) => ({
            ...result,
            [ratePlan.rate_plan.code]: {
              code: ratePlan.rate_plan.code,
              tag: ratePlan.rate_plan.tag,
              description: ratePlan.rate_plan.description,
              type: ratePlan.rate_plan.code === 'NRP' ? 'non-refundable' : 'refundable',
              sellingPrice: +ratePlan.price.sell_price,
              basePrice: +ratePlan.price.base_price,
              totalPrice: +ratePlan.price.net_payable_amount,
              pretaxPrice: +ratePlan.price.pretax_price,
              tax: +ratePlan.price.tax,
              discountAmount: +ratePlan.price.coupon_discount,
              discountPercentage: +ratePlan.price.total_discount_percent,
              roomDiscount: +ratePlan.price.promo_discount,
              treeboPointsUsed: +ratePlan.price.wallet_deduction,
              isUsingTreeboPoints: ratePlan.price.wallet_applied,
              totalUsableTreeboPoints: +ratePlan.price.wallet_deductable_amount,
              couponCode: ratePlan.price.coupon_code,
              couponDiscount: ratePlan.price.coupon_discount,
              couponApplied: ratePlan.price.coupon_applied,
            },
          }), {}),
          memberDiscount: {
            value: data.selected_rate_plan.price.member_discount,
            isApplied: data.member_discount_applied,
            isAvailable: data.member_discount_available,
          },
        },
      },
    };
  },
};
