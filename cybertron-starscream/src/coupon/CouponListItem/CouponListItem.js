/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import moment from 'moment';
import isEmpty from 'lodash/isEmpty';
import Text from '../../components/NewLeaf/Text/Text';
import Price from '../../components/NewLeaf/Price/Price';
import color from '../../theme/color';
import CheckBox from '../../components/CheckBox/CheckBox';
import { formatDate, capitalizeFirstLetter } from '../../utils/utils';

const RootContainer = styled.View`
  flexDirection: row;
  alignItems: center;
`;

const TextContainer = styled.View`
  paddingLeft: 42;
  paddingTop: 16;
  paddingBottom: 10;
`;

const CouponCodeContainer = styled.View`
  borderColor: ${color.primary};
  borderStyle: dashed;
  borderWidth: 1;
  paddingTop: 10;
  paddingBottom: 10;
  paddingLeft: 25;
  paddingRight: 25;
  marginLeft: 12;
  backgroundColor: ${({ isChecked }) => (isChecked ? color.couponGreen : color.white)};
`;

const ItemView = styled.View`
  marginVertical: 16px;
`;

const TouchableOpacity = styled.TouchableOpacity`
  flexDirection: row;
  alignItems: center;
`;

const FlexWrapColumn = styled.View`
  flexDirection: column;
  flexWrap: wrap;
  marginHorizontal: 8;
`;

const CouponCode = ({ code, isChecked }) => (
  <CouponCodeContainer isChecked={isChecked}>
    <Text
      size="s"
      color="green"
      weight="bold"
      testID="coupon-name-text"
      accessibilityLabel="coupon-name-text"
    >
      {capitalizeFirstLetter(code)}
    </Text>
  </CouponCodeContainer>
);

const CouponListItem = ({ coupon, onCouponClick, appliedCouponCode }) => (
  <ItemView>
    <RootContainer>
      <TouchableOpacity
        onPress={(e) => onCouponClick(e)}
        testID="coupon-button"
        accessibilityLabel="coupon-button"
      >
        <CheckBox
          checked={coupon.code === appliedCouponCode}
          color={color.primary}
          onPress={(e) => onCouponClick(e)}
        />
        <CouponCode code={coupon.code} isChecked={coupon.code === appliedCouponCode} />
      </TouchableOpacity>
      <FlexWrapColumn>
        <Text size="s" color="greyDarker" weight="bold">Max discount upto</Text>
        <Price
          price={coupon.amount}
          color="greyDarker"
          size="s"
          type="regular"
          bold
          testID="coupon-amount-text"
          accessibilityLabel="coupon-amount-text"
        />
      </FlexWrapColumn>
    </RootContainer>
    <TextContainer>
      {
        !isEmpty(coupon.expiry) ? (
          <Text
            size="xs"
            color="greyDark"
            weight="normal"
            testID="coupon-valid-till-text"
            accessibilityLabel="coupon-valid-till-text"
          >
            Valid till {formatDate(moment(coupon.expiry))}
          </Text>
        ) : null
      }
      {
        !isEmpty(coupon.tagline) ? (
          <Text
            size="xs"
            color="greyDark"
            weight="normal"
            testID="coupon-tagline-text"
            accessibilityLabel="coupon-tagline-text"
          >
            {coupon.tagline}
          </Text>
        ) : null
      }
      {
        !isEmpty(coupon.description) ? (
          <Text
            size="xs"
            color="greyDark"
            weight="normal"
            testID="coupon-description-text"
            accessibilityLabel="coupon-description-text"
          >
            {coupon.description}
          </Text>
        ) : null
      }
    </TextContainer>
  </ItemView>
);

CouponCode.propTypes = {
  code: PropTypes.string,
  isChecked: PropTypes.bool,
};

CouponListItem.propTypes = {
  coupon: PropTypes.object,
  onCouponClick: PropTypes.func,
  appliedCouponCode: PropTypes.string,
};

export default CouponListItem;
