import { handle } from 'redux-pack';
import { Platform } from 'react-native';

import couponService from './couponService';
import analyticsService from '../analytics/analyticsService';
import searchService from '../search/searchService';

const SHOW_COUPON_MODAL = 'SHOW_COUPON_MODAL';
const GET_COUPONS = 'GET_COUPONS';
const APPLY_REMOVE_COUPON = 'APPLY_REMOVE_COUPON';
const GET_CHECKOUT_DETAILS = 'GET_CHECKOUT_DETAILS';

const initialState = {
  results: {
    '': {
      code: '',
      amount: '',
      tagline: '',
      description: '',
      expiry: '',
    },
  },
  sortedCouponCodes: [],
  appliedCouponCode: '',
  showCouponModal: false,
  isCouponApplyLoading: false,
  isCouponResultsLoading: false,
  error: '',
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SHOW_COUPON_MODAL:
      return {
        ...state,
        showCouponModal: payload.visibility,
        error: '',
      };

    case GET_COUPONS: return handle(state, action, {
      start: (s) => ({
        ...s,
        results: payload.results,
        sortedCouponCodes: payload.sortedCouponCodes,
        isCouponResultsLoading: true,
      }),
      success: (s) => ({
        ...s,
        results: {
          ...s.results,
          ...payload.results,
        },
        sortedCouponCodes: payload.sortedCouponCodes,
      }),
      finish: (s) => ({ ...s, isCouponResultsLoading: false }),
    });

    case APPLY_REMOVE_COUPON: return handle(state, action, {
      start: (s) => ({ ...s, isCouponApplyLoading: true }),
      success: (s) => ({
        ...s,
        results: {
          ...s.results,
          [payload.coupon.appliedCouponCode]: {
            ...s.results[payload.coupon.appliedCouponCode],
            ...payload.coupon.results[payload.coupon.appliedCouponCode],
          },
        },
        appliedCouponCode: payload.coupon.appliedCouponCode,
        showCouponModal: false,
        isCouponApplyLoading: false,
      }),
      failure: (s) => ({ ...s, error: payload.error.msg, isCouponApplyLoading: false }),
      finish: (s) => ({ ...s, isCouponApplyLoading: false }),
    });

    case GET_CHECKOUT_DETAILS: return handle(state, action, {
      start: (s) => ({ ...s, appliedCouponCode: '' }),
      success: (s) => ({
        ...s,
        results: {
          ...s.results,
          [payload.coupon.appliedCouponCode]: {
            ...s.results[payload.coupon.appliedCouponCode],
            ...payload.coupon.results[payload.coupon.appliedCouponCode],
          },
        },
        appliedCouponCode: payload.coupon.appliedCouponCode,
      }),
    });
    default:
      return state;
  }
};

export const showCouponModal = (visibility) => ({
  type: SHOW_COUPON_MODAL,
  payload: { visibility },
});

export const getCoupons = (bid) => (dispatch, getState, { api }) => dispatch({
  type: GET_COUPONS,
  payload: {
    results: initialState.results,
    sortedCouponCodes: initialState.sortedCouponCodes,
  },
  promise: api.get('/v2/discount/featured/', {
    bid,
    channel: Platform.OS === 'ios' ? 'ios' : 'android',
  }).then(couponService.transformCouponResultsApi),
});

export const applyCoupon = (couponCode, applyWallet) => (dispatch, getState, { api }) => {
  const { booking, search: { datePicker, roomConfig }, ui } = getState();
  const { checkIn, checkOut } = searchService.formattedDatePicker(datePicker.range);
  const formattedRoomConfig = searchService.formattedRoomConfig(roomConfig.rooms);
  const applyCouponQuery = {
    bid: booking.bid,
    couponcode: couponCode,
    channel: Platform.OS === 'ios' ? 'ios' : 'android',
    apply_wallet: applyWallet ? 'True' : 'False',
    ...ui.utmParams,
  };

  return dispatch({
    type: APPLY_REMOVE_COUPON,
    payload: { hotelId: booking.hotelId },
    promise: api.post('/v5/checkout/coupon/', applyCouponQuery)
      .then((res) => couponService.transformCouponApplyApi(res, booking, couponCode)),
    meta: {
      onSuccess: () => analyticsService.segmentTrackEvent('Coupon Applied',
        {
          couponCode,
          hotel_id: booking.hotelId,
          af_content_id: booking.hotelId,
          checkin: checkIn,
          checkout: checkOut,
          adults: formattedRoomConfig.adults.count,
          kids: formattedRoomConfig.kids.count,
          rooms: formattedRoomConfig.rooms.count,
        }),
    },
  });
};

export const removeCoupon = (applyWallet) => (dispatch, getState, { api }) => {
  const { booking, ui } = getState();
  const removeCouponQuery = {
    bid: booking.bid,
    channel: Platform.OS === 'ios' ? 'ios' : 'android',
    apply_wallet: applyWallet ? 'True' : 'False',
    ...ui.utmParams,
  };

  return dispatch({
    type: APPLY_REMOVE_COUPON,
    payload: { hotelId: booking.hotelId },
    promise: api.post('/v5/checkout/coupon/remove/', removeCouponQuery)
      .then((res) => couponService.transformCouponApplyApi(res, booking)),
  });
};
