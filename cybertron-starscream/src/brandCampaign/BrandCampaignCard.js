import React from 'react';
import { Spacer, theme } from 'leaf-ui/native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Image, Dimensions } from 'react-native';
import Text from '../components/NewLeaf/Text/Text';
import { getIcon } from '../utils/ComponentUtils/ComponentUtils';
import TouchableComponent from '../components/Touchable/TouchableComponent';
import analyticsService from '../analytics/analyticsService';

const SCREEN_WIDTH = Dimensions.get('window').width;
const IMAGE_WIDTH = SCREEN_WIDTH * 0.6;
const IMAGE_HEIGHT = IMAGE_WIDTH * 0.57;

const QualityGuaranteeImage = require('./quality-guarantee.png');

const imageStyle = { height: IMAGE_HEIGHT, width: IMAGE_WIDTH, marginTop: 16 };

const ListItemArray = [
  { icon: 'queen-bed', text: 'Comfortable Rooms' },
  { icon: 'manager', text: 'Best-in-class Service' },
  { icon: 'free-breakfast', text: 'Wholesome Breakfast' },
  { icon: 'tv', text: 'Assured Room Amenities' },
];

const Container = styled.View`
  margin: 16px;
`;

const TitleContainer = styled.View`
  justify-content: center;
  align-items: center;
  background-color: ${theme.color.lagoon};
  border-top-start-radius: 2;
  border-top-end-radius: 2;
  padding: 16px;
`;

const ContentContainer = styled.View`
  border-bottom-start-radius: 2;
  border-bottom-end-radius: 2;
  background-color: ${theme.color.white};
  justify-content: center;
  padding-top: 16px;
  padding-horizontal: 8px;
  border-left-width: 1;
  border-right-width: 1;
  border-bottom-width: 1;
  border-color: #dbdbdb;
`;

const ListItemContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

const KnowMoreContainer = styled.View`
  padding-top: 12px;
  padding-bottom: 24px;
  padding-left: 16px;
`;

const List = ListItemArray.map((x) => (
  <ListItemContainer key={x.text}>
    {getIcon(x.icon, theme.color.greyDarker, 48, 16)}
    <Text color="greyDarker" weight="medium">{x.text}</Text>
  </ListItemContainer>));

class BrandCampaignCard extends React.Component {
  openBrandPage = () => {
    const { history } = this.props;
    analyticsService.segmentTrackEvent(
      'Perfect Stay Know More Clicked',
      { page: this.props.callingPage },
    );
    if (history) { history.push('/perfect-stay/'); }
  };

  render() {
    return (
      <Container>
        <TitleContainer>
          <Text color="white" size="l" weight="bold">
            Perfect Stay or Don{"'"}t Pay
          </Text>
          <Spacer padding={[1, 0, 0, 0]}>
            <Text color="greyLighter" size="s">100% money-back promise</Text>
          </Spacer>
          <Image style={imageStyle} source={QualityGuaranteeImage} />
        </TitleContainer>
        <ContentContainer>
          {List}
          <KnowMoreContainer>
            <TouchableComponent onPress={this.openBrandPage}>
              <Text color="blue" weight="medium">Know more</Text>
            </TouchableComponent>
          </KnowMoreContainer>
        </ContentContainer>
      </Container>
    );
  }
}

BrandCampaignCard.propTypes = {
  history: PropTypes.object.isRequired,
  callingPage: PropTypes.string.isRequired,
};

export default BrandCampaignCard;
