import React from 'react';
import { Spacer, theme } from 'leaf-ui/native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import Text from '../components/NewLeaf/Text/Text';
import TouchableComponent from '../components/Touchable/TouchableComponent';
import { getIcon } from '../utils/ComponentUtils/ComponentUtils';
import analyticsService from '../analytics/analyticsService';

const ListItemArray = [
  { icon: 'queen-bed', text: 'Comfortable Rooms' },
  { icon: 'manager', text: 'Best-in-class Service' },
  { icon: 'free-breakfast', text: 'Wholesome Breakfast' },
  { icon: 'tv', text: 'Assured Room Amenities' },
];

const TouchableWithoutFeedback = styled.TouchableWithoutFeedback``;

const Container = styled.View`
  padding: 8px;
  background-color: ${theme.color.greyLighter};
`;

const TitleContainer = styled.View`
  flex-direction: row;
  justify-content: center;
  border-top-start-radius: 2;
  border-top-end-radius: 2;
  background-color: ${theme.color.lagoon};
`;

const TitleTextContainer = styled.View`
  margin: 16px;
  flex: 1;
`;

const FAIcon = styled.View`
  margin: 16px;
`;

const ContentContainer = styled.View`
  border-radius: 2;
  background-color: ${theme.color.white};
  justify-content: center;
  border-bottom-start-radius: 2;
  border-bottom-end-radius: 2;
  padding-top: 16px;
  padding-horizontal: 8px;
`;

const ListItemContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

const KnowMoreContainer = styled.View`
  padding-top: 12px;
  padding-bottom: 24px;
  padding-left: 16px;
`;

const List = ListItemArray.map((x) => (
  <ListItemContainer key={x.text}>
    {getIcon(x.icon, theme.color.greyDarker, 48, 16)}
    <Text color="greyDarker" weight="medium">{x.text}</Text>
  </ListItemContainer>));

export default class BrandCampaignAccordion extends React.Component {
  state = { expanded: false };

  toggle = () => {
    this.setState({
      expanded: !this.state.expanded,
    }, () => {
      if (this.state.expanded) {
        analyticsService.segmentTrackEvent(
          'Perfect Stay Card Expanded',
          { page: this.props.callingPage },
        );
        this.props.triggerScroll();
      }
    });
  }

  openBrandPage = () => {
    const { history } = this.props;
    analyticsService.segmentTrackEvent(
      'Perfect Stay Know More Clicked',
      { page: this.props.callingPage },
    );
    if (history) { history.push('/perfect-stay/'); }
  }

  render() {
    const { expanded } = this.state;
    const titleContainerStyle = {
      borderBottomLeftRadius: expanded ? 0 : 2,
      borderBottomRightRadius: expanded ? 0 : 2,
    };

    return (
      <Container>
        <TouchableWithoutFeedback onPress={this.toggle} >
          <TitleContainer style={titleContainerStyle} >
            <TitleTextContainer>
              <Text color="white" size="l" weight="bold">
                {'Perfect Stay or Don\'t Pay'}
              </Text>
              <Spacer padding={[1, 0, 0, 0]}>
                <Text color="greyLighter" size="s">100% money-back promise</Text>
              </Spacer>
            </TitleTextContainer>
            <FAIcon>{getIcon(expanded ? 'arrow-up' : 'arrow-down', 'white')}</FAIcon>
          </TitleContainer>
        </TouchableWithoutFeedback>
        {
          this.state.expanded &&
          <ContentContainer>
            {List}
            <KnowMoreContainer>
              <TouchableComponent onPress={this.openBrandPage}>
                <Text color="blue" weight="medium">Know more</Text>
              </TouchableComponent>
            </KnowMoreContainer>
          </ContentContainer>
        }
      </Container>
    );
  }
}

BrandCampaignAccordion.propTypes = {
  history: PropTypes.object.isRequired,
  triggerScroll: PropTypes.func,
  callingPage: PropTypes.string.isRequired,
};

BrandCampaignAccordion.defaultProps = {
  triggerScroll: () => null,
};
