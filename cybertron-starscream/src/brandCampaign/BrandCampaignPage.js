import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Platform, StatusBar, BackHandler, Image, TouchableWithoutFeedback, View, Linking } from 'react-native';
import { withRouter } from 'react-router-native';
import { Spacer, Button, theme, Card, Divider } from 'leaf-ui/native';
import { getIcon } from '../utils/ComponentUtils/ComponentUtils';
import Text from '../components/NewLeaf/Text/Text';
import config from '../config';
import analyticsService from '../analytics/analyticsService';

const IfWeCantImage = require('./ifWeCant.png');
const LetUsKnowImage = require('./letUsKnow.png');
const NotHavingPerfectStayImage = require('./notHavingPerfectStay.png');

const HeaderContainer = styled.View`
  padding-top: ${Platform.OS === 'ios' ? '32px' : '24px'};
  background-color: ${theme.color.lagoon};
  padding-left: 16px;
`;

const HeaderTextContainer = styled.View`
  padding-top: 44px;
  padding-bottom: 32px;
`;

const CallButtonContainer = styled.View`
  margin-horizontal: 16px;
`;

const RootView = styled.View`
  flex: 1;
  background-color: #f1f1f1
`;

const PageContainer = styled.ScrollView`
  flexGrow: 1;
  paddingBottom: 8px;
`;

const ListItemContainer = styled.View`
  padding-left: 16px;
  flex-direction: row;
  align-items: center;
  padding-bottom: 32px;
`;

const ListItemTextContainer = styled.View`
  margin-left: 16px;
`;

const ListTextHeading = styled.Text`
  color: ${theme.color.greyDarker};
  font-weight: 600;
`;

const ListTextSubheading = styled.Text`
  color: ${theme.color.greyDark};
  font-size: 14;
  font-weight: normal;
  padding-top: 8px;
`;

const WhatAllContainer = styled.View`
  background-color: #f1f1f1;
  margin-horizontal: 16px;
  margin-top: 32px;
  padding-top: 24px;
  padding-bottom: 12px;
  border-width: 1;
  border-color: #dbdbdb;
  border-radius: 2;
`;

const WhatAllListHeading = styled.Text`
  color: #4a4a4a;
  font-size: 16;
  font-weight: 600;
  padding-left: 16px;
  padding-bottom: 32px;
`;

const WhatAllListItemContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: 8px;
  height: 16;
`;

const WhatAboutContainer = styled.View`
  background-color: #f1f1f1;
  margin: 16px;
  padding-horizontal: 16px;
  padding-vertical: 24px;
  border-width: 1;
  border-color: #dbdbdb;
  border-radius: 2;
`;

const TermsContainer = styled.View`
  background-color: #fff;
  padding-horizontal: 16px;
  padding-vertical: 24px;
`;

const Seperator = styled.View`
  flex: 1;
  height: 8px;
  background-color: #f1f1f1
`;

const CloseButton = styled.TouchableOpacity`
  height: 40;
  width: 40;
  align-items: center;
  justify-content: center;
  position: absolute;
  margin-left: 4px;
  margin-top: 24px;
  z-index: 10;
`;

const callNow = () => {
  const url = `tel:${config.customerCareNumber}`;
  Linking.canOpenURL(url).then((supported) => {
    if (supported) {
      analyticsService.segmentTrackEvent('Perfect Stay CTA Clicked', { name: 'Call Now' });
      return Linking.openURL(url).catch(() => null);
    }
    return Promise.reject('Supporting app not present');
  });
};

const handlePhone = () => {
  const url = `tel:${config.customerCareNumber}`;
  Linking.canOpenURL(url).then((supported) => {
    if (supported) {
      analyticsService.segmentTrackEvent('Perfect Stay CTA Clicked', { name: 'Phone' });
      return Linking.openURL(url).catch(() => null);
    }
    return Promise.reject('Supporting app not present');
  });
};

const emailTreebo = () => {
  const url = 'mailto:hello@treebohotels.com?subject=Perfect Stay or Don\'t Pay';
  Linking.canOpenURL(url).then((supported) => {
    if (supported) {
      analyticsService.segmentTrackEvent('Perfect Stay CTA Clicked', { name: 'Email' });
      return Linking.openURL(url).catch(() => null);
    }
    return Promise.reject('Supporting app not present');
  });
};

const Header = ({ history }) => (
  <HeaderContainer>
    <CloseButton onPress={history.goBack}>
      {getIcon('close', theme.color.white)}
    </CloseButton>
    <HeaderTextContainer>
      <Text color="white" weight="bold" size="l">
        Perfect Stay or Don{"'"}t Pay
      </Text>
      <Spacer padding={[1, 0, 0, 0]}>
        <Text color="white" size="s">100% money-back promise</Text>
      </Spacer>
    </HeaderTextContainer>
  </HeaderContainer>
);

Header.propTypes = {
  history: PropTypes.object,
};

const HowToClaim = () => (
  <Card>
    <Spacer padding={[4, 2]}>
      <Text color="greyDarker" weight="bold" size="l">
        Need help with your stay?
      </Text>
    </Spacer>
    <ListItemContainer>
      <Image
        style={{ width: 56, height: 56 }}
        resizeMode="contain"
        source={LetUsKnowImage}
      />
      <ListItemTextContainer>
        <ListTextHeading>
          Let us know
        </ListTextHeading>
        <ListTextSubheading>
          {'Call us at '}
          <Text size="s" color="blue" weight="medium" onPress={() => handlePhone()}>
            {config.customerCareNumber}
          </Text>
        </ListTextSubheading>
      </ListItemTextContainer>
    </ListItemContainer>
    <ListItemContainer>
      <Image
        style={{ width: 56, height: 56 }}
        resizeMode="contain"
        source={NotHavingPerfectStayImage}
      />
      <ListItemTextContainer>
        <ListTextHeading>
          Give us 30 min
        </ListTextHeading>
        <ListTextSubheading>
          We will resolve the issue
        </ListTextSubheading>
      </ListItemTextContainer>
    </ListItemContainer>
    <ListItemContainer>
      <Image
        style={{ width: 56, height: 56 }}
        resizeMode="contain"
        source={IfWeCantImage}
      />
      <ListItemTextContainer>
        <ListTextHeading>
          And if we can{"'"}t
        </ListTextHeading>
        <ListTextSubheading>
          100% money-back upto ₹1200
        </ListTextSubheading>
      </ListItemTextContainer>
    </ListItemContainer>
    <CallButtonContainer>
      <Button block kind="outlined" onPress={() => callNow()} >
        <Text weight="medium" color="green">
          CALL NOW
        </Text>
      </Button>
    </CallButtonContainer>
  </Card>
);

const WhatAll = () => (
  <Card>
    <WhatAllContainer>
      <WhatAllListHeading>
        What does this cover?
      </WhatAllListHeading>

      <WhatAllListItemContainer>
        {getIcon('queen-bed', theme.color.greyDarker, 48, 16)}
        <Text color="greyDarker" weight="medium">Comfortable Rooms</Text>
      </WhatAllListItemContainer>
      <Spacer padding={[0, 0, 3, 6]}>
        <Text color="greyDark">Fresh linen & clean washroom</Text>
      </Spacer>

      <WhatAllListItemContainer>
        {getIcon('manager', theme.color.greyDarker, 48, 16)}
        <Text color="greyDarker" weight="medium">Best-in-class Service</Text>
      </WhatAllListItemContainer>
      <Spacer padding={[0, 0, 3, 6]}>
        <Text color="greyDark">Well trained staff and 24x7 support</Text>
      </Spacer>

      <WhatAllListItemContainer>
        {getIcon('free-breakfast', theme.color.greyDarker, 48, 16)}
        <Text color="greyDarker" weight="medium">Wholesome Breakfast</Text>
      </WhatAllListItemContainer>
      <Spacer padding={[0, 0, 3, 6]}>
        <Text color="greyDark">Minimum of 1 main course, tea/coffee, juice/cut fruits</Text>
      </Spacer>

      <WhatAllListItemContainer>
        {getIcon('ac-room', theme.color.greyDarker, 48, 16)}
        <Text color="greyDarker" weight="medium">Assured Room Amenities</Text>
      </WhatAllListItemContainer>
      <Spacer padding={[0, 0, 1, 6]}>
        <Text color="greyDark">DTH or Cable TV</Text>
      </Spacer>
      <Spacer padding={[0, 0, 1, 6]}>
        <Text color="greyDark">WiFi with min speed of 1 Mbps*</Text>
      </Spacer>
      <Spacer padding={[0, 0, 1, 6]}>
        <Text color="greyDark">AC*</Text>
      </Spacer>

      <Spacer padding={[3, 0, 0, 2]}>
        <Text color="grey">*Not applicable in hill stations</Text>
      </Spacer>

    </WhatAllContainer>
  </Card>
);

const WhatAbout = () => (
  <Card>
    <WhatAboutContainer>
      <Text color="greyDarker" weight="medium">
        Other issues?
      </Text>
      <Spacer padding={[2, 0]}>
        <Text color="greyDark">
          We are here to to resolve any issue you face while staying at a Treebo.
        </Text>
      </Spacer>
      <Spacer padding={[1.5, 0, 0, 0]}>
        <Text color="greyDarker" weight="medium">
          Let us know
        </Text>
      </Spacer>
      <Spacer padding={[1.5, 0, 0, 0]}>
        <Text color="blue" weight="medium" onPress={() => handlePhone()}>
          {config.customerCareNumber}
        </Text>
      </Spacer>
      <Spacer padding={[1.5, 0, 0, 0]}>
        <Text color="blue" weight="medium" onPress={() => emailTreebo()}>
          hello@treebohotels.com
        </Text>
      </Spacer>
    </WhatAboutContainer>
  </Card>
);

const TCTitleContainer = styled.View`
  background-color: ${theme.color.white};
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const FAIcon = styled.View`
  margin-horizontal: 16px;
  align-items: center;
`;

const TCTextContainer = styled.View`
  margin-top: 24px;
`;

const TCTextRow = styled.View`
  flex-direction: row;
  margin-bottom: 24px;
`;

const TermsAndConditions = ({ expanded, icon, toggle }) => (
  <TermsContainer>
    <TouchableWithoutFeedback onPress={toggle}>
      <TCTitleContainer>
        <Text color="greyDarker" weight="medium">
          Terms and Conditions
        </Text>
        <FAIcon>{icon}</FAIcon>
      </TCTitleContainer>
    </TouchableWithoutFeedback>
    {
      expanded &&
      <TCTextContainer>
        <TCTextRow>
          <Text color="greyDark">{'\u2022'}</Text>
          <Spacer padding={[0, 0, 0, 1]}>
            <Text color="greyDark">
              {'Upon receipt of a complaint from the customer against the '}
              {'guaranteed services as stipulated in the offer page, '}
              {'Treebo will pay a maximum refund amount of ₹1200/- '}
              {'(Rupees One Thousand and Two Hundred Only) per booking.'}
            </Text>
          </Spacer>
        </TCTextRow>
        <TCTextRow>
          <Text color="greyDark">{'\u2022'}</Text>
          <Spacer padding={[0, 0, 0, 1]}>
            <Text color="greyDark">
              {'Complaints shall be raised during customer’s stay at Treebo Hotels only.'}
              {'Any complaints raised post check-out from Treebo Hotels shall'}
              {'be entertained at Treebo’s discretion.'}
            </Text>
          </Spacer>
        </TCTextRow>
        <TCTextRow>
          <Text color="greyDark">{'\u2022'}</Text>
          <Spacer padding={[0, 0, 0, 1]}>
            <Text color="greyDark">
              {'Complaints can be raised either by calling Treebo customer care or through an email at '}
              <Text color="blue" onPress={() => emailTreebo()}>
                hello@treebohotels.com
              </Text>
            .
            </Text>
          </Spacer>
        </TCTextRow>
        <TCTextRow>
          <Text color="greyDark">{'\u2022'}</Text>
          <Spacer padding={[0, 0, 0, 1]}>
            <Text color="greyDark">
              {'This offer shall not be applicable in the event Treebo rectifies any complaint '}
              {'received by the customer within 30 minutes from the receipt of such complaint.'}
            </Text>
          </Spacer>
        </TCTextRow>
        <TCTextRow>
          <Text color="greyDark">{'\u2022'}</Text>
          <Spacer padding={[0, 0, 0, 1]}>
            <Text color="greyDark">
            Treebo reserves the right to rescind this offer at its sole discretion.
            </Text>
          </Spacer>
        </TCTextRow>
        <TCTextRow>
          <Text color="greyDark">{'\u2022'}</Text>
          <Spacer padding={[0, 0, 0, 1]}>
            <Text color="greyDark">
            Any complaint raised against the hotel policies shall not be covered under this offer.
            </Text>
          </Spacer>
        </TCTextRow>
        <TCTextRow>
          <Text color="greyDark">{'\u2022'}</Text>
          <Spacer padding={[0, 0, 0, 1]}>
            <Text color="greyDark">
              {'Terms of Service and Privacy Policy as mentioned at '}
              <Text color="blue" onPress={() => Linking.openURL('https://www.treebo.com/')}>
                {'https://www.treebo.com '}
              </Text>
              shall be applicable.
            </Text>
          </Spacer>
        </TCTextRow>
      </TCTextContainer>
    }
  </TermsContainer>
);

TermsAndConditions.propTypes = {
  expanded: PropTypes.bool,
  icon: PropTypes.object,
  toggle: PropTypes.func,
};

class BrandCampaignPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      isScrolling: false,
    };
    this.tcPosition = 0;
  }

  componentWillMount() {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('light-content');
    } else {
      StatusBar.setBackgroundColor(theme.color.lagoon);
    }
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentDidMount() {
    analyticsService.segmentTrackEvent('Perfect Stay Page Viewed');
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  onBackPress = () => {
    const { history } = this.props;
    history.goBack();
    return true;
  };

  setScrollViewRef = (ref) => {
    this._scrollView = ref;
  }

  isScrolling = () => {
    this.setState({ isScrolling: true });
  }

  isNotScrolling = () => {
    this.setState({ isScrolling: false });
  }

  toggleTC = () => {
    this.setState((prevState) => ({
      expanded: !prevState.expanded,
    }),
    () => {
      if (this.state.expanded && this._scrollView) {
        setTimeout(() => this._scrollView.scrollTo({ x: 0, y: this.tcPosition, animated: true }));
      } else if (this.state.expanded) {
        setTimeout(() => this._scrollView.scrollToEnd({ animated: true }));
      }
    });
  };

  bookNow = () => {
    analyticsService.segmentTrackEvent('Perfect Stay CTA Clicked', { name: 'Book Now' });
    this.props.history.goBack();
  };

  render() {
    const { history } = this.props;
    let icon = getIcon('arrow-down');
    if (this.state.expanded) {
      icon = getIcon('arrow-up');
    }

    return (
      <RootView>
        <PageContainer
          innerRef={this.setScrollViewRef}
          scrollEventThrottle={16}
          onScroll={(e) => {
            if (Platform.OS === 'ios') {
              if (e.nativeEvent.contentOffset.y > 159.5) {
                StatusBar.setBarStyle('dark-content');
              } else {
                StatusBar.setBarStyle('light-content');
              }
            }
          }}
          onScrollBeginDrag={this.isScrolling}
          onScrollEndDrag={this.isNotScrolling}
          onMomentumScrollBegin={this.isScrolling}
          onMomentumScrollEnd={this.isNotScrolling}
        >
          <Header history={history} />
          <HowToClaim />
          <WhatAll />
          <WhatAbout />
          <Seperator />
          <View onLayout={(e) => { this.tcPosition = e.nativeEvent.layout.y; }} />
          <TermsAndConditions
            icon={icon}
            expanded={this.state.expanded}
            toggle={this.toggleTC}
          />
        </PageContainer>
        <Card>
          {this.state.isScrolling && <Divider />}
          <Spacer padding={[1]}>
            <Button block onPress={this.bookNow} >
              <Text weight="medium" color="white">
              BOOK NOW
              </Text>
            </Button>
          </Spacer>
        </Card>
      </RootView>
    );
  }
}

BrandCampaignPage.propTypes = {
  history: PropTypes.object,
};

export default withRouter(BrandCampaignPage);
