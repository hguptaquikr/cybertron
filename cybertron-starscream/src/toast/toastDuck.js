const SHOW_TOAST = 'SHOW_TOAST';

const initialState = {
  label: '',
  timeout: 3000,
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SHOW_TOAST:
      return {
        ...state,
        label: payload.label,
      };

    default:
      return state;
  }
};

export const showToast = (label) => ({
  type: SHOW_TOAST,
  payload: { label },
});
