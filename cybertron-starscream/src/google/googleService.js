import { Places } from 'google-places-web';
import config from '../config';

export default {
  initHandlers() {
    Places.apiKey = config.googleMapsApiKey;
    this.placesHandler = Places;
  },

  constructPlace(result) {
    const googleAddressPieceMap = {
      city: 'locality',
      state: 'administrative_area_level_1',
      country: 'country',
    };

    const area = result.address_components.reduce((obj, addressComponent) => {
      Object.keys(googleAddressPieceMap)
        .forEach((addressPiece) => {
          obj[addressPiece] = // eslint-disable-line
            addressComponent.types.includes(googleAddressPieceMap[addressPiece])
              ? addressComponent.long_name : obj[addressPiece];
        });
      return obj;
    }, {});

    const coordinates = {
      lat: result.geometry.location.lat,
      lng: result.geometry.location.lng,
    };

    return { area, coordinates };
  },
};
