import isEqual from 'lodash/isEqual';
import { Platform } from 'react-native';
import moment from 'moment';
import config from '../config';
import { pluralize } from '../utils/utils';
import { requestLocationPermission, checkAndroidLocationServices } from '../permissions/androidPermissionService';
import analyticsService from '../analytics/analyticsService';

const isPlatformIOS = Platform.OS === 'ios';

export default {
  transformAutocompleteApi({ data }) {
    return data.map((result) => ({
      q: result.label,
      type: result.type,
      area: result.area,
      hotelId: result.hotel_id,
      hotelCount: result.hotel_count,
      coordinates: result.coordinates,
      googlePlaceId: result.place_id,
    }));
  },

  formattedRoomConfig(rooms) {
    const { adults, kids } = rooms.reduce((obj, room) => ({
      adults: obj.adults + +room.adults,
      kids: obj.kids + (+room.kids ? +room.kids : 0),
    }), { adults: 0, kids: 0 });
    const adultsText = `${adults} ${pluralize(adults, 'Adult')}`;
    const kidsText = `${kids} ${pluralize(kids, 'Kid')}`;
    const roomsText = `${rooms.length} ${pluralize(rooms.length, 'Room')}`;

    return {
      adults: {
        count: adults,
        text: adultsText,
      },
      kids: {
        count: kids,
        text: kidsText,
      },
      rooms: {
        count: rooms.length,
        text: roomsText,
      },
      text: [
        adultsText,
        kids ? kidsText : null,
        roomsText,
      ].filter((v) => v).join(', '),
    };
  },

  formattedDatePicker({ start, end }, dateFormat = config.dateFormat.view) {
    const checkIn = start ? start.format(dateFormat) : '';
    const checkOut = end ? end.format(dateFormat) : '';
    const numberOfNights = start && end ? end.diff(start, 'days') : '';
    return {
      checkIn,
      checkOut,
      numberOfNights: {
        count: numberOfNights,
        text: `${numberOfNights} ${pluralize(numberOfNights, 'Day')}`,
      },
    };
  },

  didSearchChange(current, next) {
    const { datePicker: { range: currentDate }, searchInput: { place: currentPlace } } = current;
    const { datePicker: { range: nextDate }, searchInput: { place: nextPlace } } = next;

    const didDateChange = (nextDate.start && nextDate.end) &&
      !(nextDate.start.isSame(currentDate.start, 'day') && nextDate.end.isSame(currentDate.end, 'day'));
    const didRoomConfigChange = !isEqual(current.roomConfig, next.roomConfig);
    const didSearchQueryChange = !isEqual(currentPlace.q, nextPlace.q);

    return didDateChange || didRoomConfigChange || didSearchQueryChange;
  },

  areDatesInBetween(checkIn, checkout, eventStart, eventEnd) {
    const start = moment(eventStart, 'DD-MM-YYYY');
    const end = moment(eventEnd, 'DD-MM-YYYY');
    return start.isBetween(checkIn, checkout, 'days', []) && end.isBetween(checkIn, checkout, 'days', []);
  },

  async getGeoLocation() {
    const isPermissionGranted = await requestLocationPermission();
    const isAndroidLocationServicesEnabled = await checkAndroidLocationServices();

    const isLocationAvailable = navigator.geolocation &&
      (isPermissionGranted || isAndroidLocationServicesEnabled || isPlatformIOS);

    if (isPermissionGranted) {
      analyticsService.segmentTrackEvent('Location Permission Granted');
    } else {
      analyticsService.segmentTrackEvent('Location Permission Rejected');
    }

    return new Promise(((resolve, reject) => {
      if (isLocationAvailable) {
        navigator.geolocation.getCurrentPosition(
          ({ coords: { latitude, longitude } }) => {
            analyticsService.segmentTrackEvent('Get Location Successful', {
              coordinates: { lat: latitude, lng: longitude },
            });
            resolve({ lat: latitude, lng: longitude });
          },
          (error) => {
            analyticsService.segmentTrackEvent('Get Location Failed', error);
            alert(`${error.message} Please try again.`);
            reject(false);
          },
          { timeout: 10000 },
        );
      } else {
        alert('Could not access location. Please enable location access.');
        reject(false);
      }
    }));
  },

  async getCurrentCity() {
    const response = await fetch(`https://pro.ip-api.com/json?key=${config.ipApiKey}`);
    const json = await response.json();
    if (response.ok) {
      return json.city;
    }
    return '';
  },
};
