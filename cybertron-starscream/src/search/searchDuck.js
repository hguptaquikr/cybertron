import { handle } from 'redux-pack';
import isEmpty from 'lodash/isEmpty';
import startCase from 'lodash/startCase';
import get from 'lodash/get';
import moment from 'moment';
import urlService from '../services/urlService';
import apiService from '../api/apiService';
import searchService from '../search/searchService';
import hotelService from '../hotel/hotelService';
import googleService from '../google/googleService';

const SET_SEARCH_MODAL = 'SET_SEARCH_MODAL';
const GET_SEARCH_SUGGESTIONS = 'GET_SEARCH_SUGGESTIONS';
const SEARCH_LOCATION_CHANGE = 'SEARCH_LOCATION_CHANGE';
const GET_LOCATION_DETAILS = 'GET_LOCATION_DETAILS';
const DATE_RANGE_CHANGE = 'DATE_RANGE_CHANGE';
const ADD_ROOM_CONFIG = 'ADD_ROOM_CONFIG';
const ADD_GUEST_TYPE = 'ADD_GUEST_TYPE';
const SUBTRACT_GUEST_TYPE = 'SUBTRACT_GUEST_TYPE';
const SAVE_ROOM_CONFIG = 'SAVE_ROOM_CONFIG';
const DISMISS_ROOM_CONFIG_CHANGES = 'DISMISS_ROOM_CONFIG_CHANGES';
const SET_UP_DUMMY_ROOM_CONFIG = 'SET_UP_DUMMY_ROOM_CONFIG';
const REMOVE_ROOM_CONFIG = 'REMOVE_ROOM_CONFIG';
const CHECK_HOTEL_AVAILABILITY = 'CHECK_HOTEL_AVAILABILITY';
const SET_SEARCH_STATE = 'SET_SEARCH_STATE';
const SET_SEARCH_CITY = 'SET_SEARCH_CITY';
const SET_SEARCH_CATEGORY = 'SET_SEARCH_CATEGORY';
const RESET_SEARCH_STATE = 'RESET_SEARCH_STATE';
const SET_NEW_SEARCH_STATE = 'SET_NEW_SEARCH_STATE';
const CHANGE_ROOM_TYPE = 'CHANGE_ROOM_TYPE';

const initialState = {
  searchModal: false,
  searchInput: {
    isLoading: false,
    suggestions: [],
    place: {
      coordinates: {},
      q: 'Near Me',
      type: 'near-me',
    },
  },
  datePicker: {
    range: {
      start: moment(),
      end: moment().add(1, 'days'),
    },
    highlight: '',
  },
  roomConfig: {
    rooms: [
      { adults: 1, kids: 0 },
    ],
    roomType: '',
  },
  dummyRoomConfig: {
    rooms: [
      { adults: 1, kids: 0 },
    ],
    roomType: '',
  },
  newSearch: false,
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_SEARCH_MODAL:
      return {
        ...state,
        searchModal: payload.searchModal,
        datePicker: {
          ...state.datePicker,
          highlight: payload.type,
        },
      };

    case GET_SEARCH_SUGGESTIONS:
      return handle(state, action, {
        start: (s) => ({
          ...s,
          searchInput: {
            ...s.searchInput,
            isLoading: true,
          },
        }),
        success: (s) => ({
          ...s,
          searchInput: {
            ...s.searchInput,
            isLoading: false,
            suggestions: payload,
          },
        }),
        failure: (s) => ({
          ...s,
          searchInput: {
            ...s.searchInput,
            isLoading: false,
            error: payload.error,
          },
        }),
      });

    case SEARCH_LOCATION_CHANGE:
      return {
        ...state,
        searchInput: {
          ...state.searchInput,
          place: payload.place,
        },
      };

    case GET_LOCATION_DETAILS:
      return handle(state, action, {
        success: (s) => ({
          ...s,
          searchInput: {
            ...s.searchInput,
            place: {
              ...s.searchInput.place,
              ...payload.place,
            },
          },
        }),
      });

    case DATE_RANGE_CHANGE:
      return {
        ...state,
        datePicker: {
          ...state.datePicker,
          range: payload.range,
        },
      };

    case ADD_ROOM_CONFIG:
      return {
        ...state,
        dummyRoomConfig: {
          ...state.dummyRoomConfig,
          rooms: [
            ...state.dummyRoomConfig.rooms,
            { adults: 1, kids: 0 },
          ],
        },
      };

    case ADD_GUEST_TYPE:
      return {
        ...state,
        dummyRoomConfig: {
          ...state.dummyRoomConfig,
          rooms: state.dummyRoomConfig.rooms.map((room, roomNumber) => (
            roomNumber === payload.roomNumber ? {
              ...room,
              [payload.guestType]: +room[payload.guestType] + 1 > 8
                ? 8 : +room[payload.guestType] + 1,
            } : room
          )),
        },
      };

    case SUBTRACT_GUEST_TYPE:
      return {
        ...state,
        dummyRoomConfig: {
          ...state.dummyRoomConfig,
          rooms: state.dummyRoomConfig.rooms.map((room, roomNumber) => {
            if (roomNumber === payload.roomNumber) {
              let newGuestCount = +room[payload.guestType] - 1;
              if (payload.guestType === 'adults' && newGuestCount < 1) newGuestCount = 1;
              return {
                ...room,
                [payload.guestType]: newGuestCount < 0
                  ? 0 : newGuestCount,
              };
            }
            return room;
          }),
        },
      };

    case REMOVE_ROOM_CONFIG:
      return {
        ...state,
        dummyRoomConfig: {
          ...state.dummyRoomConfig,
          rooms: state.dummyRoomConfig.rooms.filter((room, index) => payload.roomNumber !== index),
        },
      };

    case SAVE_ROOM_CONFIG:
      return {
        ...state,
        roomConfig: {
          ...state.dummyRoomConfig,
        },
      };

    case DISMISS_ROOM_CONFIG_CHANGES:
      return {
        ...state,
        dummyRoomConfig: {
          rooms: [
            { adults: 1, kids: 0 },
          ],
          roomType: '',
        },
      };

    case SET_UP_DUMMY_ROOM_CONFIG:
      return {
        ...state,
        dummyRoomConfig: {
          ...state.roomConfig,
        },
      };

    case SET_SEARCH_STATE:
      return {
        ...state,
        searchInput: {
          ...state.searchInput,
          place: payload.place || state.searchInput.place,
        },
        datePicker: {
          ...state.datePicker,
          range: payload.range || state.datePicker.range,
        },
        roomConfig: {
          ...state.roomConfig,
          rooms: payload.rooms || state.roomConfig.rooms,
          roomType: payload.roomType || state.roomConfig.roomType,
        },
      };

    case SET_SEARCH_CITY:
      return {
        ...state,
        searchInput: {
          ...state.searchInput,
          place: {
            ...get(state, 'searchInput.place', {}),
            q: startCase(payload),
            area: {
              ...get(state, 'searchInput.place.area', {}),
              city: payload,
            },
          },
        },
      };

    case SET_SEARCH_CATEGORY:
      return {
        ...state,
        searchInput: {
          ...state.searchInput,
          place: {
            ...get(state, 'searchInput.place', {}),
            area: {
              ...get(state, 'searchInput.place.area', {}),
              category: payload,
            },
          },
        },
      };

    case SET_NEW_SEARCH_STATE:
      return {
        ...state,
        newSearch: payload,
      };

    case RESET_SEARCH_STATE:
      return {
        ...state,
        [state.searchModal]: payload[state.searchModal],
      };

    case CHANGE_ROOM_TYPE:
      return {
        ...state,
        roomConfig: {
          ...state.roomConfig,
          roomType: payload.roomType,
        },
      };

    default:
      return state;
  }
};

export const setSearchModal = (searchModal, type) => ({
  type: SET_SEARCH_MODAL,
  payload: {
    searchModal,
    type,
  },
});

export const getSearchSuggestions = (input) => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_SEARCH_SUGGESTIONS,
    promise: api.get('/v1/search/autocomplete/', { search_string: input })
      .then((res) => {
        if ('near me'.includes(input.toLowerCase())) {
          res.data = [
            {
              label: 'Near Me',
              type: 'near-me',
              area: { country: 'India' },
            },
            ...res.data,
          ];
        }
        return res;
      })
      .then(searchService.transformAutocompleteApi),
  });

export const searchLocationChange = (place) => (dispatch) => {
  dispatch({
    type: SEARCH_LOCATION_CHANGE,
    payload: { place },
  });
  dispatch(setSearchModal(false));
};

export const resetSearchModal = () => ({
  type: RESET_SEARCH_STATE,
  payload: initialState,
});

export const getLocationDetails = (place) => (dispatch) => {
  const getLocationDetailsPromise = googleService.placesHandler.details({
    placeid: place.googlePlaceId,
  }).then((result) => ({
    place: googleService.constructPlace(result),
  }));

  dispatch({
    type: GET_LOCATION_DETAILS,
    promise: getLocationDetailsPromise,
  });
};

export const dateRangeChange = (range) => (dispatch) => {
  dispatch({
    type: DATE_RANGE_CHANGE,
    payload: { range },
  });
  if (range.start && range.end) {
    dispatch(setSearchModal(false));
  }
};

export const addRoomConfig = () => ({
  type: ADD_ROOM_CONFIG,
});

export const addGuestType = (roomNumber, guestType) => ({
  type: ADD_GUEST_TYPE,
  payload: {
    roomNumber,
    guestType,
  },
});

export const subtractGuestType = (roomNumber, guestType) => ({
  type: SUBTRACT_GUEST_TYPE,
  payload: {
    roomNumber,
    guestType,
  },
});

export const removeRoomConfig = (roomNumber) => ({
  type: REMOVE_ROOM_CONFIG,
  payload: { roomNumber },
});

export const saveRoomConfig = () => ({
  type: SAVE_ROOM_CONFIG,
});

export const changeRoomType = (roomType) => ({
  type: CHANGE_ROOM_TYPE,
  payload: { roomType },
});

export const resetRoomConfig = () => ({
  type: DISMISS_ROOM_CONFIG_CHANGES,
});

export const setUpDummyRoomConfig = () => ({
  type: SET_UP_DUMMY_ROOM_CONFIG,
});

export const isSearchWidgetValid = () => (dispatch, getState) => {
  const { search: { searchInput, datePicker, roomConfig } } = getState();
  if (!searchInput.place.q) {
    dispatch(setSearchModal('searchInput'));
    return false;
  } else if (!datePicker.range.start || !datePicker.range.end) {
    dispatch(setSearchModal('datePicker'));
    return false;
  } else if (isEmpty(roomConfig.rooms)) {
    dispatch(setSearchModal('roomConfig'));
    return false;
  }
  return true;
};

export const checkHotelAvailability = () => (dispatch, getState, { api }) => {
  const { searchInput, datePicker, roomConfig } = getState().search;
  const { hotelId } = searchInput.place;
  const availabilityQuery = apiService.getSearchQuery({
    range: datePicker.range,
    rooms: roomConfig.rooms,
  });

  return dispatch({
    type: CHECK_HOTEL_AVAILABILITY,
    promise: api.get(`/v2/hotels/${hotelId}/availability/`, availabilityQuery)
      .then(hotelService.transformHotelAvailabilityApi),
  });
};

export const setSearchState = (urlQuery) => ({
  type: SET_SEARCH_STATE,
  payload: urlService.deconstructUrlQuery(urlQuery),
});

export const setSearchCity = (city) => ({
  type: SET_SEARCH_CITY,
  payload: city || '',
});

export const setSearchCategory = (category) => ({
  type: SET_SEARCH_CATEGORY,
  payload: category || '',
});

export const setNewSearch = (val) => ({
  type: SET_NEW_SEARCH_STATE,
  payload: val,
});
