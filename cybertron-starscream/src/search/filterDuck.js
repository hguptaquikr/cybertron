import { handle } from 'redux-pack';
import groupBy from 'lodash/groupBy';
import get from 'lodash/get';
import merge from 'lodash/merge';
import hotelService from '../hotel/hotelService';
import priceService from '../price/priceService';
import { safeKeys } from '../utils/utils';

const SORT_CHANGE = 'SORT_CHANGE';
const CLEAR_ALL_FILTERS = 'CLEAR_ALL_FILTERS';
const CONSTRUCT_LOCALITIES_FILTER = 'CONSTRUCT_LOCALITIES_FILTER';
const CONSTRUCT_PRICE_RANGES_FILTER = 'CONSTRUCT_PRICE_RANGES_FILTER';
const GET_AMENITIES_FILTER = 'GET_AMENITIES_FILTER';
const FILTERS_CHECKBOX_CHANGE = 'FILTERS_CHECKBOX_CHANGE';
const SET_REFINED_HOTEL_IDS = 'SET_REFINED_HOTEL_IDS';
const GET_HOTEL_RESULTS = 'GET_HOTEL_RESULTS';

export const initialState = {
  filters: {
    coupleFriendly: [{ name: ' View All ', checked: false }],
    priceRanges: [],
    localities: [],
    amenities: [],
    showAvailableOnly: [{ checked: false }],
    distanceCap: null,
  },
  sort: {
    by: 'priceLToH',
    list: {
      rating: 'Rating',
      priceLToH: 'Price - Low to High',
      priceHToL: 'Price - High to Low',
      distance: 'Distance',
    },
    button: {
      priceLToH: 'PRICE(L - H)',
      priceHToL: 'PRICE(H - L)',
      rating: 'RATING',
      distance: 'DISTANCE',
    },
    coordinates: { lat: '', lng: '' },
  },
  refinedHotelIds: [],
};

export default (state = initialState, action) => {
  const { type, payload, meta } = action;
  switch (type) {
    case SORT_CHANGE:
      return {
        ...state,
        sort: {
          ...state.sort,
          by: payload.sortBy,
        },
      };

    case CLEAR_ALL_FILTERS:
      return {
        ...state,
        filters: payload.clearedFilters,
      };

    case CONSTRUCT_LOCALITIES_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          localities: payload.localities,
        },
      };

    case CONSTRUCT_PRICE_RANGES_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          priceRanges: payload.priceRanges,
        },
      };

    case GET_AMENITIES_FILTER: return handle(state, action, {
      success: (s) => ({
        ...s,
        filters: {
          ...state.filters,
          amenities: payload.data.map((amenity) => ({ ...amenity, checked: false })),
        },
      }),
    });

    case FILTERS_CHECKBOX_CHANGE: {
      const { i: index, filterKey } = payload;
      return {
        ...state,
        filters: {
          ...state.filters,
          [filterKey]: [
            ...state.filters[filterKey].slice(0, index),
            {
              ...state.filters[filterKey][index],
              checked: payload.checked,
            },
            ...state.filters[filterKey].slice(index + 1),
          ],
        },
      };
    }

    case SET_REFINED_HOTEL_IDS:
      return {
        ...state,
        refinedHotelIds: payload.refinedHotelIds,
      };

    case GET_HOTEL_RESULTS:
      return handle(state, action, {
        start: (s) => ({
          ...s,
          sort: payload.sort,
          refinedHotelIds: [],
        }),
        success: (s) => ({
          ...s,
          filters: {
            ...state.filters,
            ...payload.filters,
          },
          sort: meta.startPayload.sort.by === 'distance' ?
            merge({}, state.sort, meta.startPayload.sort)
            : merge({}, state.sort, payload.sort),
          refinedHotelIds: safeKeys(payload.results),
        }),
      });

    default:
      return state;
  }
};

export const constructPriceRangesFilter = () => (dispatch, getState) => {
  const { price: { results: prices } } = getState();
  const sortedPrices = safeKeys(prices)
    .map((hotelId) => {
      const cheapestRoom = priceService.getCheapestRoom(prices[hotelId]);
      return get(prices[hotelId][cheapestRoom.type].ratePlans[cheapestRoom.plan], 'sellingPrice');
    })
    .sort((a, b) => (a - b > 0 ? 1 : -1));

  const minPrice = sortedPrices[0];
  const maxPrice = sortedPrices[sortedPrices.length - 1];
  const incPrice = Math.round((maxPrice - minPrice) / 3);
  let currentPrice = Math.round(minPrice);
  const priceRanges = [0, 1, 2].map(() => {
    const priceRange = {
      start: currentPrice,
      end: currentPrice + incPrice,
      name: `&#x20b9;${currentPrice} - &#x20b9;${currentPrice + incPrice}`,
      checked: false,
    };
    currentPrice += incPrice;
    return priceRange;
  });

  dispatch({
    type: CONSTRUCT_PRICE_RANGES_FILTER,
    payload: { priceRanges },
  });
};

export const constructLocalitiesFilter = () => (dispatch, getState) => {
  const { hotel: { results: hotels } } = getState();
  const allLocalities = safeKeys(hotels).map((hotelId) => get(hotels[hotelId], 'address.locality', {}));
  const distinctLocalities = [...new Set(allLocalities)];
  const localities = distinctLocalities.sort().map((locality) => ({
    name: locality,
    checked: false,
  }));

  dispatch({
    type: CONSTRUCT_LOCALITIES_FILTER,
    payload: { localities },
  });
};

export const getAmenitiesFilter = () => (dispatch, getState, { api }) => {
  dispatch({
    type: GET_AMENITIES_FILTER,
    promise: api.get('/v1/facilities/'),
  });
};

const _filterResults = (hotelIds, getState) => {
  const {
    filter: { filters },
    hotel: { results: hotels },
    price: { results: prices },
  } = getState();
  const hasPrices = !(safeKeys(prices).length === 0 && safeKeys(hotels).length === 0);
  const shouldFilterCoupleFriendly = filters.coupleFriendly.length > 0
    && filters.coupleFriendly[0].checked;
  const checkedPriceRanges = filters.priceRanges.filter((priceRange) => priceRange.checked);
  const checkedLocalities = filters.localities.filter((locality) => locality.checked);
  const checkedAmenities = filters.amenities.filter((amenity) => amenity.checked);
  const shouldFilterWithinDistanceCap = !!filters.distanceCap;
  const shouldFilterAvailableOnly = filters.showAvailableOnly[0].checked && hasPrices;
  const shouldFilterPriceRanges = checkedPriceRanges.length && hasPrices;
  const shouldFilterLocalities = checkedLocalities.length;
  const shouldFilterAmenities = checkedAmenities.length;

  const withinDistanceCap = (hotel) => hotel.distance <= filters.distanceCap;
  const isAvailable = (price) => price.isAvailable;
  const inPriceRange = (priceRanges, price) =>
    priceRanges.some((priceRange) =>
      Math.round(price.sellingPrice) >= Math.round(priceRange.start)
      && Math.round(price.sellingPrice) <= Math.round(priceRange.end));
  const inLocalities = (localities, hotel) =>
    localities.some((locality) => get(hotel, 'address.locality', '') === locality.name);
  const hasAmenities = (amenities, hotel) =>
    amenities.every((amenity) => hotel.amenities.includes(amenity.id));

  return hasPrices && hotelIds.filter((hotelId) => {
    const hotel = hotels[hotelId];
    const cheapestRoomType = priceService.getCheapestRoomType(prices[hotelId]);
    const ratePlansObject = get(prices, `[${hotelId}][${cheapestRoomType}].ratePlans`, {});
    const cheapestRatePlan = priceService.getCheapestRatePlan(ratePlansObject);
    const price = get(ratePlansObject, `${cheapestRatePlan}`, {});
    let keepHotel = true;
    if (shouldFilterCoupleFriendly) {
      keepHotel = keepHotel && hotelService.isCoupleFriendly(hotel);
    }
    if (shouldFilterWithinDistanceCap) {
      keepHotel = keepHotel && withinDistanceCap(hotel);
    }
    if (shouldFilterAvailableOnly) {
      keepHotel = keepHotel && isAvailable(price);
    }
    if (shouldFilterPriceRanges) {
      keepHotel = keepHotel && inPriceRange(checkedPriceRanges, price);
    }
    if (shouldFilterLocalities) {
      keepHotel = keepHotel && inLocalities(checkedLocalities, hotel);
    }
    if (shouldFilterAmenities) {
      keepHotel = keepHotel && hasAmenities(checkedAmenities, hotel);
    }
    return keepHotel;
  });
};

const _comparePrices = (priceA, priceB) => priceA - priceB;

const _sortResults = (hotelIds, getState) => {
  const {
    filter: { sort },
    search: { searchInput },
    hotel: { results: hotels },
    price: { results: prices, availability },
  } = getState();

  // break if no results to sort
  if (!hotelIds.length) return false;

  // sort by
  let sortedHotelIds = [];
  const hasPrices = !(safeKeys(prices).length === 0 && safeKeys(hotels).length === 0);
  if ((sort.by === 'priceLToH' || sort.by === 'priceHToL') && hasPrices) {
    sortedHotelIds = hotelIds.sort((hotelIdA, hotelIdB) => {
      if (!availability[hotelIdA]) { return 1; }
      if (!availability[hotelIdB]) { return -1; }
      const cheapestRoomA = priceService.getCheapestRoom(prices[hotelIdA]);
      const cheapestRoomB = priceService.getCheapestRoom(prices[hotelIdB]);
      return sort.by === 'priceLToH' ?
        _comparePrices(prices[hotelIdA][cheapestRoomA.type]
          .ratePlans[cheapestRoomA.plan].sellingPrice,
        prices[hotelIdB][cheapestRoomB.type].ratePlans[cheapestRoomB.plan].sellingPrice) :
        _comparePrices(prices[hotelIdB][cheapestRoomB.type]
          .ratePlans[cheapestRoomB.plan].sellingPrice,
        prices[hotelIdA][cheapestRoomA.type].ratePlans[cheapestRoomA.plan].sellingPrice);
    });
  } else if (sort.by === 'distance') {
    sortedHotelIds = hotelIds.sort((a, b) => {
      if (!availability[a]) { return 1; }
      if (!availability[b]) { return -1; }
      return (hotels[a].distance - hotels[b].distance);
    });
  } else if (sort.by === 'rating') {
    sortedHotelIds = hotelIds.sort((a, b) => {
      if ((hotels[a].reviews.isTaEnabled && hotels[a].reviews.overallRating.rating) &&
        ((hotels[b].reviews.isTaEnabled && hotels[b].reviews.overallRating.rating))) {
        return (hotels[b].reviews.overallRating.rating - hotels[a].reviews.overallRating.rating);
      } else if (hotels[a].reviews.isTaEnabled && hotels[a].reviews.overallRating.rating) {
        return -1;
      } else if ((hotels[b].reviews.isTaEnabled && hotels[b].reviews.overallRating.rating)) {
        return 1;
      }
      return 0;
    });
  } else if (sort.by === 'recommended') {
    sortedHotelIds = hotelIds.sort((a, b) => {
      if (hotels[a].rank && hotels[b].rank) {
        return hotels[a].rank.recommended - hotels[b].rank.recommended;
      }
      return 0;
    });
  }

  // break if no results after sort
  if (!sortedHotelIds.length) return false;

  // group by availability
  if (hasPrices) {
    const groupedResults = groupBy(
      sortedHotelIds,
      (hotelId) => {
        const cheapestRoomType = priceService.getCheapestRoomType(prices[hotelId]);
        return !!get(prices, `[${hotelId}][${cheapestRoomType}].isAvailable`, '');
      },
    );
    sortedHotelIds = [];
    sortedHotelIds = groupedResults.true
      ? sortedHotelIds.concat(groupedResults.true) : sortedHotelIds;
    sortedHotelIds = groupedResults.false
      ? sortedHotelIds.concat(groupedResults.false) : sortedHotelIds;
  }

  // move searched soldout hotel to top
  if (searchInput.place.hotelId) {
    const placeHotelId = searchInput.place.hotelId;
    sortedHotelIds = sortedHotelIds.reduce((arr, hotelId) => {
      if (+hotelId === +placeHotelId) arr.unshift(placeHotelId);
      else arr.push(hotelId);
      return arr;
    }, []);
  }

  return sortedHotelIds;
};

export const refineResults = () => (dispatch, getState) => {
  const { hotel: { results: hotels } } = getState();

  const hotelIds = safeKeys(hotels);
  const filteredHotelIds = _filterResults(hotelIds, getState);
  const sortedHotelIds = _sortResults(filteredHotelIds, getState);
  dispatch({
    type: SET_REFINED_HOTEL_IDS,
    payload: { refinedHotelIds: sortedHotelIds || [] },
  });
};

export const filtersCheckboxChange = (filterKey, i, checked) => (dispatch) => {
  dispatch({
    type: FILTERS_CHECKBOX_CHANGE,
    payload: {
      filterKey,
      i,
      checked,
    },
  });
  dispatch(refineResults());
};

export const sortBy = (parameter) => (dispatch) => {
  dispatch({
    type: SORT_CHANGE,
    payload: { sortBy: parameter },
  });
  dispatch(refineResults());
};

export const clearAllFilters = () => (dispatch, getState) => {
  const { filter: { filters } } = getState();
  const clearedFilters = { ...filters };
  Object.keys(clearedFilters)
    .filter((filterKey) => filterKey !== 'distanceCap')
    .forEach((filterKey) => {
      clearedFilters[filterKey].forEach((filter) => {
        filter.checked = false; // eslint-disable-line
      });
    });

  dispatch({
    type: CLEAR_ALL_FILTERS,
    payload: { clearedFilters },
  });
  dispatch(refineResults());
};
