import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import Header from '../../components/Header/Header';
import color from '../../theme/color';

const View = styled.View``;

const SearchResultsHeader = ({ title, children, onBackPress }) => (
  <View>
    <Header
      middle={title}
      iconProps={{ name: 'back' }}
      onLeftPress={onBackPress}
      backgroundColor={color.white}
      androidStatusBarColor={color.black}
      noShadow
    />
    { children }
  </View>
);

SearchResultsHeader.propTypes = {
  title: PropTypes.node.isRequired,
  children: PropTypes.node,
  onBackPress: PropTypes.func.isRequired,
};

export default SearchResultsHeader;
