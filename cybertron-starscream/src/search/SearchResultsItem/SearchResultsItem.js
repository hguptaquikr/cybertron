/* eslint-disable global-require, no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components/native';
import Shimmer from 'react-native-shimmer';
import isEmpty from 'lodash/isEmpty';
import kebabCase from 'lodash/kebabCase';
import get from 'lodash/get';
import { Tag, Spacer, Button } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';
import SVG from '../../components/SVG/SVG';
import Price from '../../components/NewLeaf/Price/Price';
import priceService from '../../price/priceService';
import Carousel from '../../components/Carousel/Carousel';
import color from '../../theme/color';
import urlService from '../../services/urlService';
import analyticsService from '../../analytics/analyticsService';
import { pluralize, imageResolutions, getDistanceWithUnit } from '../../utils/utils';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import TouchableComponent from '../../components/Touchable/TouchableComponent';
import hotelService from '../../hotel/hotelService';

const View = styled.View``;

const TagContainer = styled.View`
  justifyContent: center;
  alignItems: flex-start;
`;

const ParentContainer = styled.View`
  paddingBottom: 16;
  marginBottom: 8;
  background-color: ${color.white};
  borderBottomWidth: 1;
  borderBottomColor: ${color.disabledGreyOpacity60};
`;

const TopContainer = styled.View`
  flexDirection: row;
  justifyContent: space-between;
  alignItems: flex-start;
  paddingTop: 22px;
`;

const SoldOutContainer = styled.View`
  backgroundColor: ${color.blackOpacity70};
  height: 25px;
  width: 105px;
  justifyContent: center;
  alignItems: center;
`;

const TAContainer = styled.View`
  flexDirection: row;
  marginTop: -6px;
`;

const Location = styled.View`
  marginTop: 4px;
`;

const PriceContainer = styled.View`
  marginTop: 16px;
  flexDirection: row;
  alignItems: flex-end;
`;

const ConditionContainer = styled.View`
  marginTop: 4px;
  marginBottom: 16px;
  flexDirection: row;
  alignItems: center;
`;

const DotView = styled.View`
  backgroundColor: ${color.disabledGrey};
  marginLeft: 6;
  width: 4;
  height: 4;
  borderRadius: 2;
`;

const SpecialDealsContainer = styled.View`
  flexDirection: row;
  marginLeft: 12;
  alignItems: center;
`;

const SpecialDealsText = styled.View`
  marginLeft: 2;
`;

const HotelDetailsContainer = styled.View`
  flex: 1;
`;

const InfoContainerChild = styled.View`
  paddingHorizontal: 16;
`;

const LoadingView = styled.View`
  backgroundColor: ${color.lightGrey};
`;

const carouselImageCss = css`
  width: 205;
  aspectRatio: 1.6;
  justifyContent: center;
  alignItems: center;
  marginRight: 2;
  borderRadius: 0;
`;

const goToHotelDetailsPage = (search, hotel, history) => {
  requestAnimationFrame(() => {
    const { datePicker, roomConfig, searchInput: { place } } = search;
    const city = kebabCase(get(place, 'area.city') || get(hotel, 'address.city'));
    const hotelLocality = get(hotel, 'address.locality', '');
    const hotelSlug = kebabCase(`${hotel.name} ${hotelLocality}`);
    analyticsService.segmentTrackEvent('Hotel Clicked', {
      hotel_id: hotel.id,
      hotel_name: hotel.name,
      af_content_id: hotel.id,
    });
    history.push({
      pathname: `/hotels-in-${city}/${hotelSlug}-${hotel.id}/`,
      query: urlService.constructUrlQuery({
        place,
        range: datePicker.range,
        rooms: roomConfig.rooms,
      }),
    });
  });
};

const SearchResultsItem = ({
  hotel,
  price,
  history,
  search,
  available,
  numberOfNights,
  isPricesLoading,
  animate,
}) => {
  const reviews = get(hotel, 'reviews', {});
  const { roomPrice } = priceService.getCheapestRoom(price);
  const isHotelAvailable = available;
  const isSoldOut = !isEmpty(roomPrice) && !isHotelAvailable;
  const isUndefined = !price && !isPricesLoading;
  const hotelImages = get(hotel, 'images', []);
  const hotelImagesUrl = !isEmpty(hotelImages) && hotelImages.map((hotelImage) => ({
    url: `${hotelImage.url}?${imageResolutions.lowres}`,
  }));
  const distance = get(hotel, 'distance');
  const hotelDistance = distance && getDistanceWithUnit(distance);

  return (
    <ParentContainer>
      <Carousel
        data={hotelImagesUrl || []}
        imageCss={carouselImageCss}
        onItemPress={() => goToHotelDetailsPage(search, hotel, history, isHotelAvailable)}
        disabled={isSoldOut}
        animate={!isEmpty(roomPrice) && animate}
      />
      <TouchableComponent
        onPress={() => goToHotelDetailsPage(search, hotel, history, isHotelAvailable)}
        testID="search-result-item"
        accessibilityLabel="search-result-item"
      >
        <InfoContainerChild>
          <TopContainer>
            <HotelDetailsContainer>
              <Text
                size="s"
                weight="medium"
                color="greyDarker"
                family="medium"
                testID="hotel-name-text"
                accessibilityLabel="hotel-name-text"
              >
                {hotel.name}
              </Text>
              <Location>
                <Text size="s" weight="normal" color="greyDark">
                  {get(hotel, 'address.locality', '')}
                </Text>
              </Location>
              {
                hotelService.isCoupleFriendly(hotel) ? (
                  <TagContainer>
                    <Spacer margin={[2, 0, 0, 0]}>
                      <Tag color="lagoon" kind="outlined" shape="capsular">Couple Friendly</Tag>
                    </Spacer>
                  </TagContainer>
                ) : null
              }
            </HotelDetailsContainer>
            {
              isSoldOut || isUndefined ? (
                <SoldOutContainer>
                  <Text
                    size="s"
                    weight="normal"
                    color="white"
                    testID="sold-out-text"
                    accessibilityLabel="sold-out-text"
                  >
                    SOLD OUT
                  </Text>
                </SoldOutContainer>
              ) : (
                <TAContainer>
                  {
                      !isEmpty(reviews) && get(reviews, 'isTaEnabled', false)
                        && reviews.overallRating.ratingImage ? (
                          <View
                            testID="trip-advisor-rating-view"
                            accessibilityLabel="trip-advisor-rating-view"
                          >
                            <SVG
                              style={{ width: 80, height: 22 }}
                              source={reviews.overallRating.ratingImage}
                            />
                          </View>
                        ) : null
                    }
                </TAContainer>
                )
            }
          </TopContainer>
          {
            isPricesLoading ? (
              <View
                testID="hotel-price-loading-view"
                accessibilityLabel="hotel-price-loading-view"
              >
                <Shimmer
                  animationOpacity={1}
                  duration={500}
                  style={{ marginTop: 16, marginBottom: 12, height: 24, width: 80 }}
                >
                  <LoadingView />
                </Shimmer>
                <Shimmer
                  animationOpacity={0.8}
                  duration={500}
                  style={{ height: 16, width: 152 }}
                >
                  <LoadingView />
                </Shimmer>
                <Shimmer
                  animationOpacity={0.8}
                  duration={500}
                  style={{ marginTop: 16, height: 48, width: '100%', borderRadius: 2, alignSelf: 'center' }}
                >
                  <LoadingView />
                </Shimmer>
              </View>
            ) : (
                !isEmpty(roomPrice) && isHotelAvailable ? (
                  <View>
                    <PriceContainer>
                      <Price
                        testID="hotel-price-text"
                        accessibilityLabel="hotel-price-text"
                        price={roomPrice.totalPrice}
                        size="l"
                        color="greyDarker"
                        type="regular"
                        bold
                        round
                      />
                      {
                        get(roomPrice, 'memberDiscount.isApplied', false) ? (
                          <SpecialDealsContainer
                            testID="special-deal-view"
                            accessibilityLabel="special-deal-view"
                          >
                            {getIcon('special_deal')}
                            <SpecialDealsText>
                              <Text size="s" color="blueDark" weight="bold" family="normal">Member Rate</Text>
                            </SpecialDealsText>
                          </SpecialDealsContainer>
                        ) : null
                      }
                    </PriceContainer>
                    <ConditionContainer>
                      <Text size="xs" weight="normal" color="greyDark">Tax inclusive</Text>
                      <DotView />
                      <Spacer margin={[0, 0, 0, 0.75]}>
                        <Text size="xs" weight="normal" color="greyDark">
                          Price for {pluralize(numberOfNights, 'night', 's', true)}
                        </Text>
                      </Spacer>
                      {!!hotelDistance && <DotView />}
                      {
                        !!hotelDistance &&
                        <Spacer margin={[0, 0, 0, 0.75]}>
                          <Text size="xs" weight="normal" color="greyDark">
                            Distance: {hotelDistance}
                          </Text>
                        </Spacer>
                      }
                    </ConditionContainer>
                    <Button
                      block
                      onPress={() => {
                        requestAnimationFrame(() => {
                          goToHotelDetailsPage(search, hotel, history, isHotelAvailable);
                        });
                      }}
                    >
                      <Text size="m" weight="medium" family="medium" color="white">
                        BOOK
                      </Text>
                    </Button>
                  </View>
                ) : null
              )
          }
        </InfoContainerChild>
      </TouchableComponent>
    </ParentContainer>
  );
};

SearchResultsItem.propTypes = {
  hotel: PropTypes.object.isRequired,
  price: PropTypes.object,
  history: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  available: PropTypes.bool,
  numberOfNights: PropTypes.number.isRequired,
  isPricesLoading: PropTypes.bool,
  animate: PropTypes.bool,
};

export default SearchResultsItem;
