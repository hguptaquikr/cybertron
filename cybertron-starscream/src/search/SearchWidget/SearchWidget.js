import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Platform, InteractionManager, Animated, ActivityIndicator, Linking } from 'react-native';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { theme } from 'leaf-ui/native';
import get from 'lodash/get';
import styled from 'styled-components/native';
import config from '../../config';
import * as searchActionCreators from '../../search/searchDuck';
import color from '../../theme/color';
import SearchHOC from '../../components/hocs/SearchHOC/SearchHOC';
import Text from '../../components/NewLeaf/Text/Text';
import TouchableComponent from '../../components/Touchable/TouchableComponent';
import Modal from '../../components/Modal/Modal';
import searchService from '../../search/searchService';
import SearchInput from './SearchInput';
import DatePicker from './DatePicker';
import RoomConfig from './RoomConfig';
import analyticsService from '../../analytics/analyticsService';
import { getAnimatedIcon, getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import { capitalizeFirstLetter } from '../../utils/utils';
import { checkAndRequestCallPermission } from '../../permissions/androidPermissionService';
import { immediateCall } from '../../nativeModules';

const SearchComponents = styled.View`
  padding: 8px;
  marginBottom: 8;
`;

const CallusContainer = styled.TouchableOpacity`
  justifyContent: center;
  alignItems: center;
  marginTop: ${Platform.OS === 'android' ? 8 : 16};
  marginBottom: 20;
  width: 32;
  height: 32;`;

const HeaderRow = styled.View`
  flexDirection: row;
  alignItems: center;
  justifyContent: space-between;
  zIndex: 1;
`;

const HeaderContentContainer = styled.View`
  flex: 1;
  justifyContent: center;
  alignItems: center;
  marginRight: 32;
`;

const SearchDisplayItemContainer = styled.TouchableOpacity`
  borderBottomWidth: 1;
  borderBottomColor: ${color.whiteOpacity30};
`;

const CheckInContainer = styled(SearchDisplayItemContainer)`
  marginRight: 20;
  flex: 1;
`;

const CheckOutContainer = styled(SearchDisplayItemContainer)`
  marginLeft: 20;
  flex: 1;
`;

const SearchDisplayItem = styled.View`
  paddingVertical: 8;
`;

const DatePickerContainer = styled(SearchComponents)`
  flexDirection: row;
  justifyContent: space-between;
`;

const ResetSearch = styled.TouchableOpacity``;

const NavigationIcon = styled.TouchableOpacity`
  justifyContent: center;
  alignItems: center;
  marginTop: ${Platform.OS === 'android' ? 8 : 16};
  marginBottom: 20;
  width: 32;
  height: 32;
`;

const TitleIconView = styled.View`
  marginBottom: ${Platform.OS === 'android' ? 8 : 0};
`;

const SearchButton = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;
  backgroundColor: ${theme.color.white};
  border-radius: ${theme.px(10)};
  padding: ${theme.px([2, 3])};
  align-self: stretch;
`;

const modalHeaderStyle = {
  borderBottomWidth: 0,
  paddingLeft: 4,
  paddingRight: 100,
};

const widgetHeaderStyle = {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  paddingBottom: 4,
};

class SearchWidget extends Component {
  state = {
    isClearAllActive: false,
  };

  onSearch = () => {
    const {
      search,
      searchActions,
      performSearch,
    } = this.props;
    analyticsService.segmentTrackEvent('Search Treebo');
    if (searchActions.isSearchWidgetValid()) {
      performSearch(search, searchActions);
      searchActions.setNewSearch(true);
    }
  };

  getHeader = () => {
    const {
      search: { searchModal, searchInput: { isLoading } },
      searchActions: { resetSearchModal, resetRoomConfig },
    } = this.props;

    return (
      <ResetSearch
        onPress={() => {
          analyticsService.segmentTrackEvent('Widget Reset');
          const { allowSearchReset } = this.props;
          if (searchModal === 'roomConfig') {
            resetRoomConfig();
          } else if (allowSearchReset) {
            resetSearchModal();
          }
          this.setState({ isClearAllActive: true });
          setTimeout(() => this.setState({ isClearAllActive: false }), 10);
        }}
        testID="reset-button"
        accessibilityLabel="reset-button"
        disabled={isLoading}
      >
        {isLoading ?
          <ActivityIndicator color="white" />
        :
          <Text size="s" color="white">
            {(searchModal === 'searchInput') ? 'Clear All' : 'Reset'}
          </Text>
        }
      </ResetSearch>
    );
  };

  handleCalliOS = () => {
    const url = `tel:${config.customerCareNumber}`;
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        return Linking.openURL(url).catch(() => null);
      }
      return Promise.reject();
    });
  }

  handleCallAndroid = async () => {
    const isPermissionGranted = await checkAndRequestCallPermission();
    if (isPermissionGranted) {
      immediateCall();
    }
  }

  handleCallus = () => {
    const showSearchButton = get(this.props, 'showSearchButton', true);
    analyticsService.segmentTrackEvent('Call Widget Clicked', { source: showSearchButton ? 'LandingPage' : 'SearchResultsPage' });
    if (Platform.OS === 'ios') {
      this.handleCalliOS();
    } else {
      this.handleCallAndroid();
    }
  }

  render() {
    const {
      searchActions,
      searchActions: { setSearchModal, resetSearchModal },
      search: {
        searchModal,
        searchInput,
        datePicker,
        roomConfig,
        dummyRoomConfig,
      },
      onNavigationIconPress,
      navigationIcon,
      onHeaderTitlePress,
      getHeight,
      scrollPostion,
      widgetHeight,
      showSearchButton,
      scrollFactor,
      allowSearchReset,
    } = this.props;
    const datePickerValue = searchService.formattedDatePicker(datePicker.range);
    const roomConfigValue = searchService.formattedRoomConfig(roomConfig.rooms);

    const widgetBackground = !scrollPostion ? theme.color.teal :
      scrollPostion.interpolate({
        inputRange: [0, widgetHeight - 120, widgetHeight - 64],
        outputRange: [theme.color.teal, theme.color.teal, color.white],
        extrapolate: 'clamp',
      });

    const searchButtonOpacity = !scrollPostion ? 1 :
      scrollPostion.interpolate({
        inputRange: [0, 80 + scrollFactor],
        outputRange: [1, 0],
        extrapolate: 'clamp',
      });

    const searchButtonScale = !scrollPostion ? 1 :
      searchButtonOpacity.interpolate({
        inputRange: [0, 0.8, 1],
        outputRange: [0.5, 1, 1],
        extrapolate: 'clamp',
      });

    const searchButtonTranslation = !scrollPostion ? 0 :
      searchButtonOpacity.interpolate({
        inputRange: [0, 1],
        outputRange: [-40, 0],
      });

    const guestsContainerOpacity = !scrollPostion ? 1 :
      scrollPostion.interpolate({
        inputRange: [0, 80 + scrollFactor, 160 + scrollFactor],
        outputRange: [1, 1, 0],
        extrapolate: 'clamp',
      });

    const guestsContainerTranslation = !scrollPostion ? 0 :
      guestsContainerOpacity.interpolate({
        inputRange: [0, 1],
        outputRange: [-40, 0],
      });

    const datesContainerOpacity = !scrollPostion ? 1 :
      scrollPostion.interpolate({
        inputRange: [0, 160 + scrollFactor, 240 + scrollFactor],
        outputRange: [1, 1, 0],
        extrapolate: 'clamp',
      });

    const datesContainerTranslation = !scrollPostion ? 0 :
      datesContainerOpacity.interpolate({
        inputRange: [0, 1],
        outputRange: [-40, 0],
      });

    const searchContainerOpacity = !scrollPostion ? 1 :
      scrollPostion.interpolate({
        inputRange: [0, 240 + scrollFactor, 300 + scrollFactor],
        outputRange: [1, 1, 0],
        extrapolate: 'clamp',
      });

    const searchContainerTranslation = !scrollPostion ? 0 :
      searchContainerOpacity.interpolate({
        inputRange: [0, 1],
        outputRange: [-40, 0],
      });

    const navIconTint = !scrollPostion ? color.white :
      scrollPostion.interpolate({
        inputRange: [0, widgetHeight - 120, widgetHeight - 64],
        outputRange: [color.white, color.white, color.charcoalGrey],
        extrapolate: 'clamp',
      });

    const headerTitleOpacity = !scrollPostion ? 0 :
      scrollPostion.interpolate({
        inputRange: [0, widgetHeight - 120, widgetHeight - 64],
        outputRange: [0, 0, 1],
        extrapolate: 'clamp',
      });

    const callUsIconTint = !scrollPostion ? color.white :
      scrollPostion.interpolate({
        inputRange: [0, widgetHeight - 120, widgetHeight - 64],
        outputRange: [color.white, color.white, color.warmGrey],
        extrapolate: 'clamp',
      });

    return (
      <Animated.View
        style={{
          backgroundColor: widgetBackground,
          paddingHorizontal: 16,
          paddingVertical: 8,
        }}
        onLayout={(event) => {
          const { height } = event.nativeEvent.layout;
          if (getHeight) {
            getHeight(height);
          }
        }}
      >
        <HeaderRow>
          <NavigationIcon
            testID="icon-sw-nav"
            accessibilityLabel="icon-sw-nav"
            onPress={onNavigationIconPress}
          >
            {getAnimatedIcon(navigationIcon, navIconTint)}
          </NavigationIcon>
          <HeaderContentContainer>
            <TouchableComponent
              onPress={onHeaderTitlePress}
              testID="search-page-header"
              accessibilityLabel="search-page-header"
            >
              <Animated.View
                style={{ ...widgetHeaderStyle, opacity: headerTitleOpacity }}
              >
                <Text
                  size="m"
                  color="greyDarker"
                  weight="medium"
                  testID="destination-text"
                  accessibilityLabel="destination-text"
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  style={{
                    marginLeft: 16,
                    marginBottom: Platform.OS === 'android' ? 8 : 0,
                  }}
                >
                  {capitalizeFirstLetter(searchInput.place.q) || 'Near Me'}
                </Text>
                <TitleIconView>
                  {getIcon('arrow-down')}
                </TitleIconView>
              </Animated.View>
            </TouchableComponent>
          </HeaderContentContainer>
          <CallusContainer
            onPress={this.handleCallus}
          >
            {getAnimatedIcon('call', callUsIconTint)}
          </CallusContainer>

        </HeaderRow>
        <Animated.View
          style={{
            opacity: searchContainerOpacity,
            transform: [
              { translateY: searchContainerTranslation },
              { perspective: 1000 },
            ],
          }}
        >
          <SearchComponents>
            <SearchDisplayItemContainer
              testID="enter-destination-field"
              accessibilityLabel="enter-destination-field"
              onPress={() => {
                analyticsService.segmentScreenViewed('Destination Viewed');
                setSearchModal('searchInput');
              }}
            >
              <Text size="xs" color="tealLight">Destination</Text>
              <SearchDisplayItem>
                <Text size="l" color="white" weight="bold">
                  {searchInput.place.q || 'Near Me'}
                </Text>
              </SearchDisplayItem>
            </SearchDisplayItemContainer>
          </SearchComponents>
        </Animated.View>
        <Animated.View
          style={{
            opacity: datesContainerOpacity,
            transform: [
              { translateY: datesContainerTranslation },
              { perspective: 1000 },
            ],
          }}
        >
          <DatePickerContainer>
            <CheckInContainer
              testID="checkin-date-field"
              accessibilityLabel="checkin-date-field"
              onPress={() => {
                InteractionManager.runAfterInteractions(() => {
                  analyticsService.segmentScreenViewed('Date Picker Viewed');
                  setSearchModal('datePicker');
                });
              }}
            >
              <Text size="xs" color="tealLight">Check In</Text>
              <SearchDisplayItem>
                <Text size="l" color="white" weight="bold">{datePickerValue.checkIn || 'Check In'}</Text>
              </SearchDisplayItem>
            </CheckInContainer>
            <CheckOutContainer
              testID="checkout-date-field"
              accessibilityLabel="checkout-date-field"
              onPress={() => {
                InteractionManager.runAfterInteractions(() => {
                  analyticsService.segmentScreenViewed('Date Picker Viewed');
                  setSearchModal('datePicker');
                });
              }}
            >
              <Text size="xs" color="tealLight">Check Out</Text>
              <SearchDisplayItem>
                <Text size="l" color="white" weight="bold">{datePickerValue.checkOut || 'Check Out'}</Text>
              </SearchDisplayItem>
            </CheckOutContainer>
          </DatePickerContainer>
        </Animated.View>
        <Animated.View
          style={{
            opacity: guestsContainerOpacity,
            transform: [
              { translateY: guestsContainerTranslation },
              { perspective: 1000 },
            ],
          }}
        >
          <SearchComponents>
            <SearchDisplayItemContainer
              testID="room-configuration-field"
              accessibilityLabel="room-configuration-field"
              onPress={() => {
                analyticsService.segmentScreenViewed('Room Config Viewed');
                setSearchModal('roomConfig');
              }}
            >
              <Text size="xs" color="tealLight">Guests</Text>
              <SearchDisplayItem>
                <Text size="l" color="white" weight="bold">{roomConfigValue.text}</Text>
              </SearchDisplayItem>
            </SearchDisplayItemContainer>
          </SearchComponents>
        </Animated.View>
        {
          showSearchButton &&
            <Animated.View
              style={{
                opacity: searchButtonOpacity,
                transform: [
                  { translateY: searchButtonTranslation },
                  { scale: searchButtonScale },
                  { perspective: 1000 },
                ],
                marginTop: 34,
                marginBottom: 16,
              }}
            >
              <SearchButton
                onPress={this.onSearch}
                testID="search-button"
                accessibilityLabel="search-button"
              >
                <Text size="m" weight="semibold">
                  Search
                </Text>
              </SearchButton>
            </Animated.View>
        }

        <Modal
          title="Select Room"
          isOpen={!!searchModal}
          onClose={() => {
            if (searchModal === 'roomConfig') {
              searchActions.resetRoomConfig();
            }
            searchActions.setSearchModal(false);
          }}
          right={this.getHeader()}
          onRightPress={resetSearchModal}
          headerStyles={modalHeaderStyle}
          iconProps={{ name: 'close', tintColor: color.white }}
          headerBackgroundColor={theme.color.teal}
        >
          {
            searchModal === 'searchInput' &&
              <SearchInput
                searchInput={searchInput}
                searchActions={searchActions}
                clearAllClicked={this.state.isClearAllActive}
                allowSearchReset={allowSearchReset}
              />
          }
          {
            searchModal === 'datePicker' &&
              <DatePicker
                datePicker={datePicker}
                searchActions={searchActions}
              />
          }
          {
            searchModal === 'roomConfig' &&
              <RoomConfig
                roomConfig={dummyRoomConfig}
                searchActions={searchActions}
              />
          }
        </Modal>
      </Animated.View>
    );
  }
}

SearchWidget.propTypes = {
  searchActions: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  performSearch: PropTypes.func.isRequired,
  onNavigationIconPress: PropTypes.func,
  navigationIcon: PropTypes.string,
  onHeaderTitlePress: PropTypes.func,
  getHeight: PropTypes.func,
  scrollPostion: PropTypes.object,
  widgetHeight: PropTypes.number,
  showSearchButton: PropTypes.bool,
  scrollFactor: PropTypes.number,
  allowSearchReset: PropTypes.bool,
};

SearchWidget.defaultProps = {
  navigationIcon: 'back',
  onHeaderTitlePress: () => { },
  showSearchButton: true,
  scrollFactor: 0,
  allowSearchReset: true,
};

const mapStateToProps = (state) => ({
  search: state.search,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  SearchHOC,
)(SearchWidget);
