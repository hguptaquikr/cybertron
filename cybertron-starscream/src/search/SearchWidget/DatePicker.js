import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dimensions, Platform, StatusBar } from 'react-native';
import styled from 'styled-components/native';
import { Spacer, theme } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';
import DateRangePicker from '../../components/DatePicker/DateRangePicker';
import searchService from '../../search/searchService';
import { pluralize } from '../../utils/utils';
import analyticsService from '../../analytics/analyticsService';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const windowDimensions = Dimensions.get('window');

const DatePickerContainer = styled.View`
  height: ${windowDimensions.height};
`;

const View = styled.View``;

const DateHeader = styled.View`
  backgroundColor: ${theme.color.teal};
  paddingBottom: 28px;
  paddingHorizontal: 16px;
  flexDirection: row;
  justifyContent: space-between;
`;

const DateHeaderItems = styled.View`
  alignItems: center;
  justifyContent: flex-end;
`;

class DatePicker extends Component {
  componentWillMount() {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('light-content');
    } else {
      StatusBar.setBackgroundColor(theme.color.tealDark);
    }
  }

  onDatesChange = ({ startDate, endDate }) => {
    const { searchActions } = this.props;
    const props = {
      checkinDate: startDate,
      checkoutDate: endDate,
    };
    analyticsService.segmentTrackEvent('Dates Selected', props);
    searchActions.dateRangeChange({
      start: startDate,
      end: endDate,
    });
  };

  render() {
    const { datePicker: { range } } = this.props;
    const { checkIn, checkOut, numberOfNights } = searchService.formattedDatePicker(range);
    const nightsCount = numberOfNights.count || 0;

    return (
      <DatePickerContainer
        testID="date-picker-modal"
        accessibilityLabel="date-picker-modal"
      >
        <DateHeader>
          <View>
            <Spacer padding={[1, 0, 1, 0]}>
              <Text size="xs" weight="normal" color="tealLight">{!checkIn || 'Check In'}</Text>
            </Spacer>
            <Text
              size="l"
              color="white"
              weight="bold"
              testID="checkin-date-text"
              accessibilityLabel="checkin-date-text"
            >
              {checkIn || 'Check In'}
            </Text>
          </View>

          <DateHeaderItems>
            {getIcon('time', theme.color.tealLighter)}
            <Text
              color="tealLight"
              size="m"
              testID="stay-duration-text"
              accessibilityLabel="stay-duration-text"
            >
              {pluralize(nightsCount, 'night', 's', true)}
            </Text>
          </DateHeaderItems>

          <View>
            <Spacer padding={[1, 0, 1, 0]}>
              <Text size="xs" weight="normal" color="tealLight">{!checkOut || 'Check Out'}</Text>
            </Spacer>
            <Text
              size="l"
              color="white"
              weight="bold"
              testID="checkout-date-text"
              accessibilityLabel="checkout-date-text"
            >
              {checkOut || 'Check Out'}
            </Text>
          </View>
        </DateHeader>

        <DateRangePicker
          startDate={range.start}
          endDate={range.end}
          onDatesChange={this.onDatesChange}
        />
      </DatePickerContainer>
    );
  }
}

DatePicker.propTypes = {
  datePicker: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
};

export default DatePicker;
