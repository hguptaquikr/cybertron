import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dimensions, Platform, StatusBar } from 'react-native';
import { theme } from 'leaf-ui/native';
import styled from 'styled-components/native';
import get from 'lodash/get';
import debounce from 'lodash/debounce';
import Text, { fontFamilies } from '../../components/NewLeaf/Text/Text';
import List from '../../components/List/List';
import { pluralize, capitalizeFirstLetter } from '../../utils/utils';
import color from '../../theme/color';
import analyticsService from '../../analytics/analyticsService';

const windowDimensions = Dimensions.get('window');

const NearbyIcon = require('../nearbyIcon.png');

const SearchInputContainer = styled.View`
  height: ${windowDimensions.height};
`;

const View = styled.View``;

const SearchTextInput = styled.TextInput`
  height: 60;
  background: ${theme.color.teal};
  color: ${color.white};
  fontWeight: 700;
  paddingHorizontal: 16px;
  fontFamily: ${fontFamilies.bold};
  fontSize: 18;
`;

const SuggestionItem = styled.TouchableOpacity`
  borderBottomWidth: 1;
  borderBottomColor: ${color.disabledGreyOpacity50};
  paddingVertical: 16px;
  paddingRight: 60px;
  justifyContent: space-between;
  flexDirection: row;
  marginHorizontal: 16px;
`;

const SuggestionTextView = styled.View`
  position: absolute;
  right: 0;
  top: 16px;
`;

const NearbyIconContainer = styled.Image`
  height: 24;
  width: 24;
`;

function removeDuplicates(myArr, prop) {
  return myArr.filter((obj, pos, arr) => (
    arr.map((mapObj) => mapObj[prop]).indexOf(obj[prop]) === pos));
}

class SearchInput extends Component {
  state = {
    text: this.props.searchInput.place.q || ' ',
  };

  componentWillMount() {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('light-content');
    } else {
      StatusBar.setBackgroundColor(color.statusBarBackground);
    }
  }

  componentDidMount() {
    this.getSuggestions();
    this.onFocus();
  }

  componentWillReceiveProps(nextProps) {
    const { searchInput } = this.props;
    if (nextProps.searchInput.place.q !== searchInput.place.q) {
      this.setState({ text: nextProps.searchInput.place.q || '' }, () => this.getSuggestions());
    } else if (nextProps.clearAllClicked) {
      this.setState({ text: '' }, () => this.getSuggestions());
      this.onFocus();
    }
  }

  onFocus = () => {
    if (get(this.inputRef, 'value')) {
      setTimeout(() => {
        this.inputRef.focus();
        this.inputRef.select();
      }, 50);
    }
  };

  onSuggestionClicked = (suggestion) => () => {
    const { searchActions } = this.props;
    searchActions.searchLocationChange(suggestion);
    const props = {
      q: suggestion.q,
      city: get(suggestion, 'area.city', ''),
      landmark: get(suggestion, 'area.landmark_slug', ''),
      entity: suggestion,
    };
    analyticsService.segmentTrackEvent('Destination Selected', props);
    if (suggestion.type === 'google_api') searchActions.getLocationDetails(suggestion);
  };

  getSuggestions = debounce(() => {
    const { searchActions } = this.props;
    searchActions.getSearchSuggestions(this.state.text.trim());
  }, 100);

  storeInputRef = (input) => {
    this.inputRef = input;
  }

  changeText = (text) => {
    this.setState({ text }, this.getSuggestions);
  }

  renderSuggestion = (suggestion) => {
    const { q, type, hotelCount } = suggestion.item;

    return (
      <SuggestionItem
        key={suggestion.index}
        onPress={this.onSuggestionClicked(suggestion.item)}
        testID="search-suggestion-item"
        accessibilityLabel="search-suggestion-item"
      >
        <Text size="m" color="greyDarker" numberOfLines={1}>{q}</Text>
        {
          (type !== 'google_api' && type !== 'near-me') &&
            <SuggestionTextView>
              <Text size="s">
                { hotelCount ? `${hotelCount} ${pluralize(hotelCount, 'hotel')}` : capitalizeFirstLetter(type) }
              </Text>
            </SuggestionTextView>
        }
        { type === 'near-me' &&
        <SuggestionTextView>
          <NearbyIconContainer source={NearbyIcon} />
        </SuggestionTextView>
        }
      </SuggestionItem>
    );
  };

  render() {
    const { text } = this.state;
    let { searchInput: { suggestions } } = this.props;

    suggestions = removeDuplicates(suggestions, 'q');
    return (
      <SearchInputContainer>
        <View>
          <SearchTextInput
            ref={this.storeInputRef}
            value={text}
            autoFocus
            autoCapitalize="words"
            autoCorrect={false}
            defaultValue=" "
            placeholder="Search Destination"
            placeholderTextColor={theme.color.teal}
            selectionColor={color.white}
            onChangeText={this.changeText}
            returnKeyType="done"
            underlineColorAndroid={color.transparent}
            testID="destination-input-field"
            accessibilityLabel="destination-input-field"
          />
        </View>
        <List
          keyExtractor={(item) => item.q + item.googlePlaceId}
          data={suggestions}
          renderItem={this.renderSuggestion}
          alwaysBounceVertical={false}
        />
      </SearchInputContainer>
    );
  }
}

SearchInput.propTypes = {
  searchInput: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  clearAllClicked: PropTypes.bool,
};

export default SearchInput;
