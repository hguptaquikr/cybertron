import React, { Component } from 'react';
import { Platform, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Spacer, Button, theme } from 'leaf-ui/native';
import color from '../../theme/color';
import Text from '../../components/NewLeaf/Text/Text';
import List from '../../components/List/List';
import searchService from '../../search/searchService';
import analyticsService from '../../analytics/analyticsService';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const RoomConfigContainer = styled.View`
  flex: 1;
`;

const AddRemoveIcon = styled.TouchableOpacity`
  width: 32;
  height: 32;
  padding: 4px;
  alignItems: center;
  justifyContent: center;
`;

const RoomConfigDetails = styled.View`
  backgroundColor: ${theme.color.teal};
  paddingHorizontal: 16px;
  paddingBottom: 28px;
`;

const EditRoomConfigList = styled(List)`
  paddingTop: 32;
  paddingHorizontal: 16;
  paddingBottom: 16;
`;

const Room = styled.View`
  marginBottom: 40px;
`;

const RoomConfigValue = styled.View`
  marginVertical: 8px;
`;

const RoomHeader = styled.View`
  flexDirection: row;
  justifyContent: space-between;
  alignItems: flex-end;
  marginBottom: 16;
`;

const RoomItem = styled.View`
  flexDirection: row;
  justifyContent: space-between;
  alignItems: center;
  paddingVertical: 8px;
  marginTop: 8;
`;

const RoomItemTitle = styled.View`
  flex: 1;
`;

const RemoveRoom = styled.TouchableOpacity``;

const Footer = styled.View`
  padding: 8px;
`;

const ButtonContainer = styled.View`
  marginVertical: 8;
`;

class RoomConfig extends Component {
  componentWillMount() {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('light-content');
    } else {
      StatusBar.setBackgroundColor(color.statusBarBackground);
    }
  }

  componentDidMount() {
    const { searchActions } = this.props;
    searchActions.setUpDummyRoomConfig();
  }

  storeRef = (c) => {
    this.content = c;
  };

  renderRoom = ({ item: room, index: roomNumber }) => {
    const { adults, kids } = room;
    const {
      searchActions: {
        addGuestType,
        subtractGuestType,
        removeRoomConfig,
      },
    } = this.props;
    return (
      <Room key={roomNumber}>
        <RoomHeader>
          <Text size="l" weight="bold">Room {roomNumber + 1}</Text>
          {
            roomNumber > 0 ? (
              <RemoveRoom
                onPress={() => removeRoomConfig(roomNumber)}
                testID="remove-room-button"
                accessibilityLabel="remove-room-button"
              >
                <Text size="s" color="blue" weight="medium" family="medium">Remove</Text>
              </RemoveRoom>
            ) : null
          }
        </RoomHeader>
        <RoomItem>
          <RoomItemTitle>
            <Text size="m" weight="normal">No. of adults</Text>
          </RoomItemTitle>
          <AddRemoveIcon
            onPress={() => subtractGuestType(roomNumber, 'adults')}
            disabled={adults <= 1}
            testID="subtract-adult-count-button"
            accessibilityLabel="subtract-adult-count-button"
          >
            {getIcon(adults <= 1 ? 'remove-disabled' : 'remove-enabled')}
          </AddRemoveIcon>
          <Spacer margin={[0, 4, 0, 4]}>
            <Text size="l" weight="bold">{adults}</Text>
          </Spacer>
          <AddRemoveIcon
            onPress={() => addGuestType(roomNumber, 'adults')}
            testID="add-adult-count-button"
            accessibilityLabel="add-adult-count-button"
          >
            {getIcon(adults === 8 ? 'add-disabled' : 'add-enabled')}
          </AddRemoveIcon>
        </RoomItem>
        <RoomItem>
          <RoomItemTitle>
            <Text size="m" weight="normal">No. of kids*</Text>
            <Text size="xs" weight="normal">Ages 2-8</Text>
          </RoomItemTitle>
          <AddRemoveIcon
            onPress={() => subtractGuestType(roomNumber, 'kids')}
            disabled={kids <= 0}
            testID="subtract-kid-count-button"
            accessibilityLabel="subtract-kid-count-button"
          >
            {getIcon(kids <= 0 ? 'remove-disabled' : 'remove-enabled')}
          </AddRemoveIcon>
          <Spacer margin={[0, 4, 0, 4]}>
            <Text size="l" weight="bold">{kids}</Text>
          </Spacer>
          <AddRemoveIcon
            onPress={() => addGuestType(roomNumber, 'kids')}
            testID="add-kid-count-button"
            accessibilityLabel="add-kid-count-button"
          >
            {getIcon('add-enabled')}
          </AddRemoveIcon>
        </RoomItem>
      </Room>
    );
  };

  render() {
    const { roomConfig: { rooms }, searchActions } = this.props;

    const formattedRoomConfig = searchService.formattedRoomConfig(rooms);

    return (
      <RoomConfigContainer
        testID="room-config-modal"
        accessibilityLabel="room-config-modal"
      >
        <RoomConfigDetails>
          <RoomConfigValue>
            <Text size="m" weight="normal" color="tealLight">Add Guests</Text>
          </RoomConfigValue>
          <Text
            size="l"
            color="white"
            weight="bold"
            testID="room-config-text"
            accessibilityLabel="room-config-text"
          >
            {formattedRoomConfig.text}
          </Text>
        </RoomConfigDetails>

        <EditRoomConfigList
          data={rooms}
          keyExtractor={(items, index) => index}
          renderItem={this.renderRoom}
          getRef={this.storeRef}
          listFooterComponent={
            <Spacer margin={[0, 0, 2, 0]}>
              <Text size="xs">
                *One infant under the age of 2 allowed / room at no additional charge
              </Text>
            </Spacer>
          }
        />

        <Footer>
          <ButtonContainer>
            <Button
              kind="outlined"
              block
              onPress={() => {
                searchActions.addRoomConfig();
                setTimeout(() => {
                  if (this.content) {
                    this.content.scrollToEnd();
                  }
                }, 0);
              }}
              testID="add-room-button"
              accessibilityLabel="add-room-button"
            >
              <Text size="m" weight="medium" family="medium" color="green">
                ADD ANOTHER ROOM
              </Text>
            </Button>
          </ButtonContainer>
          <Button
            block
            testID="save-button"
            accessibilityLabel="save-button"
            onPress={() => {
              const props = {
                roomConfig: rooms,
                adults: formattedRoomConfig.adults.count,
                kids: formattedRoomConfig.kids.count,
                rooms: formattedRoomConfig.rooms.count,
              };
              analyticsService.segmentTrackEvent('Room Config selected', props);
              searchActions.saveRoomConfig();
              searchActions.setSearchModal(false);
            }}
          >
            <Text size="m" weight="medium" family="medium" color="white">
              SAVE
            </Text>
          </Button>
        </Footer>
      </RoomConfigContainer>
    );
  }
}

RoomConfig.propTypes = {
  roomConfig: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
};

export default RoomConfig;
