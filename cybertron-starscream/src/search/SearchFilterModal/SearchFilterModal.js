/* eslint-disable global-require */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components/native';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import { Tag, Button } from 'leaf-ui/native';
import * as filterActionCreators from '../../search/filterDuck';
import Text from '../../components/NewLeaf/Text/Text';
import Modal from '../../components/Modal/Modal';
import color from '../../theme/color';
import CheckBox from '../../components/CheckBox/CheckBox';
import Content from '../../components/Content/Content';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import TouchableComponent from '../../components/Touchable/TouchableComponent';
import analyticsService from '../../analytics/analyticsService';

const SectionList = styled.SectionList``;

const View = styled.View``;

const RootContainer = styled.View`
  backgroundColor: ${color.white};
`;

const ListItemContainer = styled.View`
  flexDirection: row;
  justifyContent: space-between;
  padding: 16px;
`;

const BottomContainer = styled.View`
  backgroundColor: ${color.white};
  padding: 8px;
  width: 100%;
`;

const SectionHeader = styled.View`
  flexDirection: row;
  paddingLeft: 16px;
  marginTop: 45;
`;

const FooterContainer = styled.View`
  flexDirection: row;
  marginTop: 10;
  paddingLeft: 16px;
  marginBottom: 16;
  alignItems: center;
`;

const AssuredAmenitiesContainer = styled.View`
  padding: 16px;
  width: auto;
  aspectRatio: 2;
`;

const Image = styled.Image``;

const TagContainer = styled.View`
  left: 12;
`;

class SearchFilterModal extends Component {
  state = {
    showMore: {
      priceRanges: false,
      localities: false,
      amenities: false,
    },
  };

  componentDidMount() {
    const { filter: { filters }, filterActions } = this.props;
    if (isEmpty(filters.amenities)) {
      filterActions.getAmenitiesFilter();
    }
  }

  onFilterItemPress = (item) => {
    const { filter: { filters } } = this.props;
    const { index, section } = item;
    const status = !filters[item.section.key][item.index].checked;
    analyticsService.segmentTrackEvent('Filter Applied', { type: get(item, 'section.title', '') });
    this.filtersCheckboxChange(section.key, index, status);
  };

  filterItemsToShow = (arr, key) => (arr.length > 3 && this.state.showMore[key] ?
    arr : arr.slice(0, 3));

  buildCoupleFriendlySection = (coupleFriendly) => {
    const coupleFriendlyOptions = this.filterItemsToShow(coupleFriendly, 'coupleFriendly');
    return ({
      data: coupleFriendlyOptions.map((item, index) => ({
        label: `${item.name}`,
        key: index,
      })),
      title: 'Couple Friendly Treebo',
      footer: 'coupleFriendly',
      key: 'coupleFriendly',
      renderItem: (item) => this.renderItem(item, 'coupleFriendly'),
    });
  };

  buildPriceDataSection = (price) => {
    const priceItemsToShow = this.filterItemsToShow(price, 'priceRanges');
    return ({
      data: priceItemsToShow.map((item, index) => ({
        label: `${item.start} - ${item.end}`,
        key: index,
      })),
      title: 'Price',
      footer: 'prices',
      key: 'priceRanges',
      renderItem: (item) => this.renderItem(item, 'priceRanges'),
    });
  };

  buildLocalityDataSection = (locality, index) => {
    const localityItemsToShow = this.filterItemsToShow(locality, 'localities');
    return ({
      data: localityItemsToShow.map((item) => ({
        label: `${item.name}`,
        key: index,
      })),
      title: 'Locality',
      footer: 'localities',
      key: 'localities',
      renderItem: (item) => this.renderItem(item, 'localities'),
    });
  };

  buildAmenityDataSection = (amenity, index) => {
    const amenityItemsToShow = this.filterItemsToShow(amenity, 'amenities');
    return ({
      data: amenityItemsToShow.map((item) => ({
        label: `${item.name}`,
        key: index,
      })),
      title: 'Amenities',
      footer: 'amenities',
      key: 'amenities',
      renderItem: (item) => (this.renderItem(item, 'amenities')),
    });
  };

  buildDataSection = (filters, isCoupleFriendlyHotelAvailable) => {
    const data = [];
    if (isCoupleFriendlyHotelAvailable) {
      data.push(this.buildCoupleFriendlySection(filters.coupleFriendly));
    }
    data.push(this.buildPriceDataSection(filters.priceRanges));
    data.push(this.buildLocalityDataSection(filters.localities));
    data.push(this.buildAmenityDataSection(filters.amenities));
    return data;
  };

  applySort = (sortItem) => () => {
    const { filterActions } = this.props;
    filterActions.sortBy(sortItem);
  };

  filtersCheckboxChange = (filterKey, i, checked) => {
    const { filterActions } = this.props;
    filterActions.filtersCheckboxChange(filterKey, i, checked);
  };

  clearAllFilters = () => {
    const { filterActions } = this.props;
    filterActions.clearAllFilters();
  };

  toggleShowMore = (filterKey) => {
    this.setState((prevState) => ({
      showMore: {
        ...prevState.showMore,
        [filterKey]: !prevState.showMore[filterKey],
      },
    }));
  };

  renderHeader = (headerItem) => (
    <View>
      <SectionHeader
        testID={`title-${headerItem.title}`}
        accessibilityLabel={`title-${headerItem.title}`}
      >
        <Text size="l" weight="bold">{headerItem.title}</Text>
        {
          headerItem.title === 'Couple Friendly Treebo' ? (
            <TagContainer>
              <Tag color="yellow" size="small" kind="filled">NEW</Tag>
            </TagContainer>
          ) : null
        }
      </SectionHeader>
      {
        headerItem.title === 'Amenities' ? (
          <AssuredAmenitiesContainer>
            <Image
              style={{ width: null, height: null, flex: 1 }}
              resizeMode="cover"
              source={require('../../hotel/assured_amenities_filter.png')}
            />
          </AssuredAmenitiesContainer>
        ) : null
      }
    </View>
  );

  renderItem = (item) => {
    const { filter: { filters } } = this.props;
    return (
      <TouchableComponent
        onPress={() => this.onFilterItemPress(item)}
        testID={`item-${item.index}`}
        accessibilityLabel={`item-${item.index}`}
      >
        <ListItemContainer>
          <Text size="m" weight="normal" color="greyDarker">{item.item.label}</Text>
          <CheckBox
            onPress={
              () => this.onFilterItemPress(item)
            }
            isChecked={filters[item.section.key][item.index].checked}
            color={
              filters[item.section.key][item.index].checked ? color.primary : color.charcoalGrey
            }
            square
          />
        </ListItemContainer>
      </TouchableComponent>
    );
  };

  renderFooter = (footerItem, filters) => {
    if (filters && filters[footerItem.key].length > 3) {
      return (
        <TouchableComponent
          onPress={() => (this.toggleShowMore(footerItem.key))}
          testID={`footer-${footerItem.footer.toLocaleLowerCase()}`}
          accessibilityLabel={`footer-${footerItem.footer.toLocaleLowerCase()}`}
        >
          <FooterContainer>
            <Text size="s" color="blue" weight="bold">
              View {this.state.showMore[footerItem.key] ? 'less' : 'more'} {footerItem.footer}
            </Text>
            {getIcon(this.state.showMore[footerItem.key] ? 'arrow-up' : 'arrow-down', color.blue)}
          </FooterContainer>
        </TouchableComponent>
      );
    }
    return null;
  };

  render() {
    const {
      filter: { filters, refinedHotelIds },
      isOpen,
      onClose,
      isCoupleFriendlyHotelAvailable,
    } = this.props;
    const noOfRefinedResults = refinedHotelIds.length;
    return (
      <Modal
        footer={
          <BottomContainer>
            <Button
              block
              onPress={onClose}
              disabled={noOfRefinedResults === 0}
              testID="apply-filter-button"
              accessibilityLabel="apply-filter-button"
            >
              <Text size="m" weight="medium" family="medium" color="white">
                {`VIEW ${noOfRefinedResults} ${noOfRefinedResults > 1 ? 'TREEBOS' : 'TREEBO'}`}
              </Text>
            </Button>
          </BottomContainer>
        }
        header="Filter"
        right={<Text size="s" weight="normal" color="blue">Clear All</Text>}
        isOpen={isOpen}
        onClose={noOfRefinedResults > 0 ? onClose : null}
        onRightPress={this.clearAllFilters}
        iosBarStyle="dark-content"
      >
        <Content>
          <RootContainer>
            <SectionList
              renderSectionHeader={({ section }) => this.renderHeader(section)}
              renderSectionFooter={({ section }) => this.renderFooter(section, filters)}
              sections={
                this.buildDataSection(filters, isCoupleFriendlyHotelAvailable)
              }
              testID="filters-list"
              accessibilityLabel="filters-list"
            />
          </RootContainer>
        </Content>
      </Modal>
    );
  }
}

SearchFilterModal.propTypes = {
  filter: PropTypes.object.isRequired,
  filterActions: PropTypes.object.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  isCoupleFriendlyHotelAvailable: PropTypes.bool,
};

SearchFilterModal.defaultProps = {
  isCoupleFriendlyHotelAvailable: false,
};

const mapStateToProps = (state) => ({
  hotel: state.hotel,
  filter: state.filter,
});

const mapDispatchToProps = (dispatch) => ({
  filterActions: bindActionCreators(filterActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchFilterModal);
