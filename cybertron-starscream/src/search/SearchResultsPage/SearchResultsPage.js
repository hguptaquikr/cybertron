import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  InteractionManager,
  Platform,
  StatusBar,
  Animated,
  BackHandler,
  Dimensions,
} from 'react-native';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-native';
import { theme } from 'leaf-ui/native';
import styled from 'styled-components/native';
import isEmpty from 'lodash/isEmpty';
import qs from 'query-string';
import get from 'lodash/get';
import * as filterActionCreators from '../../search/filterDuck';
import * as searchActionCreators from '../../search/searchDuck';
import * as hotelActionCreators from '../../hotel/hotelDuck';
import * as toastActionCreators from '../../toast/toastDuck';
import * as priceActionCreators from '../../price/priceDuck';
import Text from '../../components/NewLeaf/Text/Text';
import List from '../../components/List/List';
import Container from '../../components/Container/Container';
import Root from '../../components/Root/Root';
import SearchResultsFooter from '../SearchResultsFooter/SearchResultsFooter';
import SearchResultsItem from '../SearchResultsItem/SearchResultsItem';
import { safeKeys, pluralize } from '../../utils/utils';
import Loader from '../../components/Loader/Loader';
import PopularCity from '../../components/PopularCity/PopularCity';
import SearchWidget from '../SearchWidget/SearchWidget';
import LoginPage from '../../auth/LoginPage/LoginPage';
import Modal from '../../components/Modal/Modal';
import analyticsService from '../../analytics/analyticsService';
import searchService from '../../search/searchService';
import priceService from '../../price/priceService';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import storeHouse from '../../services/storeHouse';
import hotelService from '../../hotel/hotelService';
import BrandCampaignAccordion from '../../brandCampaign/BrandCampaignAccordion';

const majorVersionIOS = parseInt(Platform.Version, 10);
const contentInset = Platform.OS === 'ios' && majorVersionIOS < 11 ? 0 : -20;

const View = styled.View``;

const TouchableOpacity = styled.TouchableOpacity`
  paddingLeft: 4;
`;

const Message = styled.View`
  padding: 16px;
`;

const PopularCitiesHeading = styled.View`
  marginVertical: 24;
`;

const PopularCitiesRow = styled.View`
  flexDirection: row;
  flex: 1;
  justifyContent: space-between;
  marginBottom: 24;
`;

const NoHotelsContentView = styled.View`
  flex: 1;
  padding: 16px;
`;

const HeaderTitleContainer = styled.View`
  flexDirection: row;
  justifyContent: center;
  alignItems: center;
`;

const searchResultsStyle = {
  position: 'absolute',
  top: 0,
  bottom: 0,
  width: Dimensions.get('window').width,
};

const HeaderTitle = ({ title, up, onPress }) => (
  <TouchableOpacity onPress={onPress}>
    <HeaderTitleContainer>
      <Text
        size="m"
        weight="normal"
        numberOfLines={1}
      >
        {title}
      </Text>
      {getIcon(up ? 'arrow-up' : 'arrow-down')}
    </HeaderTitleContainer>
  </TouchableOpacity>
);

class SearchResultsPage extends Component {
  state = {
    hasPageLoaded: false,
    showLoginModal: false,
    headerHeight: 0,
    scrollY: new Animated.Value(0),
    footerTranslateY: new Animated.Value(0),
    shouldAnimateCarousel: false,
  };

  async componentDidMount() {
    const { hotel: { results } } = this.props;
    const isHotelResultsNotPresent = Object.keys(results).length === 1;

    this.pageInit.initTime = moment();

    if (isHotelResultsNotPresent) {
      await this.checkIfNearMe(this.props, { shouldTrackAnalytics: false });
      await this.getHotelResults();
    } else {
      this.getSearchPrices(results);
    }

    this.animateCarousel();
    analyticsService.segmentScreenViewed('Search Page Viewed', this.getCommonEventProps());
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  async componentWillReceiveProps(nextProps) {
    if (searchService.didSearchChange(this.props.search, nextProps.search)) {
      this.scrollHeaderUp();
      await this.checkIfNearMe(nextProps, { shouldTrackAnalytics: true });
      await this.getHotelResults(nextProps);
    }
  }

  componentWillUnmount() {
    const { filterActions } = this.props;
    InteractionManager.runAfterInteractions(filterActions.clearAllFilters);
    BackHandler.removeEventListener('hardwareBackPress');
  }

  onBackPress = () => {
    const {
      history,
      hotelActions: { resetHotelState },
    } = this.props;

    history.go(1 - history.length);
    resetHotelState();
    return true;
  };

  onLoginSuccess = () => {
    const {
      hotel,
      toastActions: { showToast },
      priceActions,
      filterActions,
    } = this.props;
    this.setState({ showLoginModal: false });
    const hotelIds = Object.keys(hotel.results);
    showToast('Applying Member Discount...');
    priceActions.getSearchPrices(hotelIds)
      .then(() => {
        filterActions.refineResults();
        filterActions.constructPriceRangesFilter();
        showToast('Member Discount Applied!.');
      });
  };

  onPress(city) {
    const { history, searchActions } = this.props;
    const { name, slug } = city;
    const props = {
      name: name.toLowerCase(),
      page: 'city page',
    };
    const place = {
      q: name,
      area: {
        city: name,
      },
    };
    analyticsService.segmentTrackEvent('City Clicked', props);
    searchActions.searchLocationChange(place);
    history.push(`/hotels-in-${slug}/`);
  }

  onUnlockSpecialDealPress = () => {
    this.setState({ showLoginModal: true });
  };

  onScroll = Animated.event(
    [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
    {
      listener: ({ nativeEvent: { contentOffset: { y } } }) => {
        if (this.state.headerHeight !== 0) {
          if (y <= this.state.headerHeight - 64) {
            if (Platform.OS === 'ios') {
              StatusBar.setBarStyle('light-content');
            } else {
              StatusBar.setBackgroundColor(theme.color.teal);
            }
          } else if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content');
          } else {
            StatusBar.setBackgroundColor(theme.color.black);
          }
        }
      },
    },
  );

  getCommonEventProps = () => {
    const {
      hotel: { results: hotels },
      location,
      search: {
        datePicker,
        roomConfig,
      },
    } = this.props;
    const { checkIn, checkOut } = searchService.formattedDatePicker(datePicker.range);
    const formattedRoomConfig = searchService.formattedRoomConfig(roomConfig.rooms);
    const totalHotels = safeKeys(hotels).length;
    return {
      num_results: totalHotels,
      checkin: checkIn,
      checkout: checkOut,
      adults: formattedRoomConfig.adults.count,
      kids: formattedRoomConfig.kids.count,
      rooms: formattedRoomConfig.rooms.count,
      q: location.query,
    };
  };

  getHotelResults = (props = this.props) => {
    const {
      hotelActions,
      searchActions,
      search: { searchInput: { place } },
      params,
      location,
    } = props;

    if (isEmpty(get(place, 'area.category', '')) && !isEmpty(params.category)) {
      searchActions.setSearchCategory(params.category);
    }

    hotelActions.getHotelResults(qs.parse(location.search), get(place, 'area.city') || params.city)
      .then((res) => {
        hotelActions.addDistanceToResults();
        this.getSearchPrices(get(res, 'payload.results', {}));
      });
  };

  getSearchPrices = (hotels) => {
    const {
      filterActions,
      priceActions,
      searchActions,
      hotelActions,
    } = this.props;
    const hotelIds = Object.keys(hotels);
    priceActions.getSearchPrices(hotelIds)
      .then((res) => {
        hotelActions.addDistanceToResults();
        InteractionManager.runAfterInteractions(() => {
          // show coupon toast
          this.showAutoAppliedCouponToast(res);
          filterActions.clearAllFilters();
          filterActions.refineResults();
          filterActions.constructPriceRangesFilter();
          filterActions.constructLocalitiesFilter();
          searchActions.setNewSearch(false);
        });
      });
  };

  getSearchEntityName = () => {
    const {
      search: { searchInput: { place, place: { hotelId } } },
      params,
    } = this.props;
    if (hotelId) {
      return `around ${place.q}.`; // If you've searched for a hotel that's sold out
    } else if (place.q === 'Near Me') {
      return 'near you.';
    }
    return `in ${(place.q || params.city)}.`;
  };

  getContentHeader = (availableHotels) => (
    <View>
      <BrandCampaignAccordion
        history={this.props.history}
        callingPage="Search"
      />
      {this.renderSoldOutMessage(availableHotels)}
    </View>
  );

  animateCarousel() {
    storeHouse.get('carousel_animation_shown')
      .then((carouselAnimationShown) => {
        if (!carouselAnimationShown) {
          this.setState({ shouldAnimateCarousel: true });
          storeHouse.save('carousel_animation_shown', true);
        }
      });
  }

  checkIfNearMe = async (props = this.props, options) => {
    const { shouldTrackAnalytics } = options;

    if (props.search.searchInput.place.q === 'Near Me') {
      const { searchActions } = this.props;
      const city = await searchService.getCurrentCity();
      const coordinates = await searchService.getGeoLocation();

      const place = {
        q: 'Near Me',
        area: { city },
        type: 'landmark',
        coordinates,
      };

      if (shouldTrackAnalytics) {
        analyticsService.segmentTrackEvent('Near Me Clicked', {
          source: 'SRP Search Widget',
        });
      }

      await searchActions.searchLocationChange(place);
    }
  }

  showAutoAppliedCouponToast = ({ payload = { price: {} } }) => {
    const { toastActions } = this.props;
    const couponCode = priceService.getAutoAppliedCoupon(get(payload, 'price.results', {}));
    if (couponCode) toastActions.showToast(`${couponCode} applied`);
  }

  pageInit = {
    initTime: 0,
    hasPageInitEventBeenSent: false,
    hasInteractivityEventBeenSent: false,
  };

  scrollHeaderUp = () => {
    setTimeout(() => {
      if (this.listViewRef && this.state.headerHeight > 0) {
        this.listViewRef.scrollToOffset({
          offset: this.state.headerHeight - contentInset - 83,
          animated: true,
        });
      } else {
        setTimeout(this.scrollHeaderUp, 1000);
      }
    }, 10);
    if (!this.state.hasPageLoaded) {
      this.setState({ hasPageLoaded: true });
      if (Platform.OS === 'ios') {
        StatusBar.setBarStyle('dark-content');
      } else {
        StatusBar.setBackgroundColor(theme.color.teal);
      }
    }
  };

  isMemberDiscountAvailable = () => {
    const { price: { results } } = this.props;
    return Object.keys(results).some((hotelID) =>
      Object.keys(results[hotelID]).some((roomType) =>
        get(results, `[${hotelID}].${roomType}.memberDiscount.isAvailable`, false)));
  };

  availableHotels = (hotels) => (
    Object.keys(hotels).filter((item) => hotels[item])
  );

  openWidget = () => {
    if (this.scrollViewRef && this.scrollViewRef._component) {
      this.scrollViewRef._component.scrollTo({ x: 0, y: 0, animated: true });
    } else if (this.listViewRef) {
      this.listViewRef.scrollToOffset({ offset: 0, animated: true });
    }
  }

  contentContainerStyle = (dataStore) => ({
    paddingTop: this.state.headerHeight,
    paddingBottom: dataStore.length <= 1 ? 320 : 50,
    backgroundColor: theme.color.greyLighter,
  });

  renderPopularCities = (cities) => (
    cities.map((city) => (
      <PopularCity
        key={city.name}
        city={city}
        hotelCount={city.noOfHotels}
        small
        page="search page"
        onPress={() => this.onPress(city)}
      />
    ))
  );

  renderSoldOutMessage = (availableHotels) => {
    const {
      search: { datePicker: { range } },
      hotel: { results: hotels },
      price: { isPricesLoading, results: prices, availability },
    } = this.props;
    const { numberOfNights } = searchService.formattedDatePicker(range);
    const nightsCount = numberOfNights.count || 0;
    const noOfTotalResults = safeKeys(hotels).length;
    if (isPricesLoading || noOfTotalResults === 0) {
      return null;
    }
    if (!availableHotels.length) {
      return (
        <View
          testID="all-hotels-sold-out-view"
          accessibilityLabel="all-hotels-sold-out-view"
        >
          <Message>
            <Text
              size="l"
              weight="bold"
            >
              All {noOfTotalResults > 1 ? (
                pluralize(noOfTotalResults, 'Treebo', 's', true)
              ) : 'Treebo'} {this.getSearchEntityName()} are sold out for these dates
            </Text>
          </Message>
        </View>
      );
    }
    const soldOutHotels = noOfTotalResults - availableHotels.length;
    if (availableHotels.length === 1 && availableHotels[0] !== '0') {
      const availableHotel = availableHotels[0];
      return (
        <View
          testID="all-hotels-except-1-sold-out-view"
          accessibilityLabel="all-hotels-except-1-sold-out-view"
        >
          <SearchResultsItem
            hotel={hotels[availableHotel]}
            price={prices[availableHotel]}
            history={this.props.history}
            search={this.props.search}
            available={availability[availableHotel]}
            numberOfNights={nightsCount}
            isPricesLoading={isPricesLoading}
          />
          {
            soldOutHotels > 0 &&
            <Message>
              <Text
                size="l"
                weight="bold"
              >
                {pluralize(soldOutHotels, 'other Treebo', 's', true)} {this.getSearchEntityName()} are sold out for
                these dates
              </Text>
            </Message>
          }
        </View>
      );
    }
    return null;
  };

  renderResults = (availableHotels) => {
    const { headerHeight, shouldAnimateCarousel } = this.state;
    const {
      hotel: { results: hotels, isHotelResultsLoading },
      price: { isPricesLoading, results: prices, availability },
      filter: { refinedHotelIds },
      search,
      history,
      content: { popularCities },
    } = this.props;
    const { datePicker: { range } } = search;
    const { numberOfNights } = searchService.formattedDatePicker(range);
    const nightsCount = numberOfNights.count || 0;

    if (refinedHotelIds.length) {
      if (!this.pageInit.hasPageInitEventBeenSent && !isHotelResultsLoading) {
        const loadingTime = moment().diff(this.pageInit.initTime, 'milliseconds');
        analyticsService.segmentTrackEvent('SRP Loader Display Time',
          { ...this.getCommonEventProps(), time: loadingTime })
          .then(this.pageInit.hasPageInitEventBeenSent = true);
      }
      if (this.pageInit.hasPageInitEventBeenSent
        && !this.pageInit.hasInteractivityEventBeenSent && !isPricesLoading) {
        const loadingTime = moment().diff(this.pageInit.initTime, 'milliseconds');
        analyticsService.segmentTrackEvent('SRP Interactivity Time',
          { ...this.getCommonEventProps(), time: loadingTime })
          .then(this.pageInit.hasInteractivityEventBeenSent = true);
      }
      const dataStore = refinedHotelIds
        .filter((hotelId) => !(availableHotels.length === 1 && hotelId === availableHotels[0]))
        .map((hotelId) => ({
          key: hotelId,
          hotel: hotels[hotelId],
          price: prices[hotelId],
        }));

      return (
        <List
          data={dataStore}
          lazy={dataStore.length > 5 ? 5 : 0}
          getRef={(componentRef) => {
            this.listViewRef = componentRef;
            if (!this.state.hasPageLoaded) {
              this.scrollHeaderUp();
            }
          }}
          keyExtractor={(item) => item.key}
          testID="search-result-list"
          accessibilityLabel="search-result-list"
          renderItem={({ item }) => (
            <SearchResultsItem
              key={item.key}
              hotel={item.hotel}
              price={item.price}
              history={history}
              search={search}
              available={availability[item.key]}
              numberOfNights={nightsCount}
              isPricesLoading={isPricesLoading}
              animate={shouldAnimateCarousel}
            />
          )}
          listHeaderComponent={this.getContentHeader(availableHotels)}
          contentInset={contentInset}
          style={searchResultsStyle}
          contentContainerStyle={this.contentContainerStyle(dataStore)}
          scrollEventThrottle={16}
          onScroll={this.onScroll}
        />
      );
    } else if (!isHotelResultsLoading) {
      return (
        <Animated.ScrollView
          contentInset={{ top: contentInset }}
          style={{ flex: 1, backgroundColor: theme.color.white, ...searchResultsStyle }}
          contentContainerStyle={{ paddingTop: headerHeight, paddingBottom: 140 }}
          scrollEventThrottle={16}
          bounces={false}
          ref={(componentRef) => { this.scrollViewRef = componentRef; }}
          onScroll={this.onScroll}
        >
          {this.getContentHeader(availableHotels)}
          <NoHotelsContentView
            testID="no-treebos-in-city-view"
            accessibilityLabel="no-treebos-in-city-view"
          >
            <Text
              size="l"
              weight="bold"
            >
              Oops!! We currently don&#39;t have any Treebos {this.getSearchEntityName()}
            </Text>
            {
              !isEmpty(popularCities) &&
              <View>
                <PopularCitiesHeading>
                  <Text
                    size="l"
                    color="greyDarker"
                    weight="bold"
                  >
                    Other Popular Cities
                  </Text>
                </PopularCitiesHeading>
                <PopularCitiesRow>
                  {this.renderPopularCities(popularCities.slice(0, 4))}
                </PopularCitiesRow>
                <PopularCitiesRow>
                  {this.renderPopularCities(popularCities.slice(4))}
                </PopularCitiesRow>
              </View>
            }
          </NoHotelsContentView>
        </Animated.ScrollView>
      );
    }
    return null;
  };

  render() {
    const {
      hotel: { isHotelResultsLoading, results },
      price: { availability },
    } = this.props;

    const { showLoginModal, headerHeight, scrollY, footerTranslateY } = this.state;
    const availableHotels = this.availableHotels(availability);

    const animatedHeaderHeight = headerHeight === 0 ? 0 :
      scrollY.interpolate({
        inputRange: [0, headerHeight - 64, headerHeight],
        outputRange: [headerHeight, 64, 64],
      });

    const animatedShadowColor = headerHeight === 0 ? theme.color.transparent :
      scrollY.interpolate({
        inputRange: [0, headerHeight - 64, headerHeight - 64],
        outputRange: [theme.color.transparent, theme.color.transparent, theme.color.greyLighter],
      });

    const footerStyle = {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      transform: [{
        translateY: footerTranslateY,
      }],
    };

    const widgetStyle = {
      height: animatedHeaderHeight,
      overflow: 'hidden',
      zIndex: 10,
      borderBottomColor: animatedShadowColor,
      borderBottomWidth: 1,
    };

    return (
      <Root>
        <Container>
          {!isHotelResultsLoading && this.renderResults(availableHotels)}
          {
            headerHeight === 0 ?
              <SearchWidget
                navigationIcon="back"
                getHeight={(height) => this.setState({ headerHeight: height })}
                showSearchButton={false}
                allowSearchReset={false}
              /> :
              <Animated.View
                removeClippedSubviews
                style={widgetStyle}
              >
                <SearchWidget
                  navigationIcon="back"
                  onNavigationIconPress={this.onBackPress}
                  onHeaderTitlePress={this.openWidget}
                  scrollPostion={scrollY}
                  widgetHeight={headerHeight}
                  showSearchButton={false}
                  scrollFactor={-80}
                  allowSearchReset={false}
                />
              </Animated.View>
          }
          {isHotelResultsLoading && <Loader />}
          {
            availableHotels.length > 2 && !isHotelResultsLoading &&
            <Animated.View style={footerStyle} >
              <SearchResultsFooter
                isCoupleFriendlyHotelAvailable={hotelService.isAnyCoupleFriendlyHotel(results)}
              />
            </Animated.View>
          }
          <Modal
            isOpen={showLoginModal}
            onClose={() => this.setState({ showLoginModal: false })}
            showHeader={false}
          >
            <LoginPage
              onLoginSuccess={this.onLoginSuccess}
              onClose={() => this.setState({ showLoginModal: false })}
            />
          </Modal>
        </Container>
      </Root>
    );
  }
}

HeaderTitle.propTypes = {
  title: PropTypes.string.isRequired,
  up: PropTypes.bool.isRequired,
  onPress: PropTypes.func.isRequired,
};

SearchResultsPage.propTypes = {
  hotel: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
  hotelActions: PropTypes.object.isRequired,
  toastActions: PropTypes.object.isRequired,
  filterActions: PropTypes.object.isRequired,
  priceActions: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
};

const mapStateToProps = (state, { match: { params }, location }) => ({
  hotel: state.hotel,
  price: state.price,
  filter: state.filter,
  search: state.search,
  content: state.content,
  location,
  params,
});

const mapDispatchToProps = (dispatch) => ({
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
  filterActions: bindActionCreators(filterActionCreators, dispatch),
  priceActions: bindActionCreators(priceActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchResultsPage));
