import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import get from 'lodash/get';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Tag, Divider, Spacer } from 'leaf-ui/native';
import * as filterActionCreators from '../../search/filterDuck';
import * as hotelActionCreators from '../../hotel/hotelDuck';
import Text from '../../components/NewLeaf/Text/Text';
import TouchableComponent from '../../components/Touchable/TouchableComponent';
import Footer from '../../components/Footer/Footer';
import FooterTab from '../../components/FooterTab/FooterTab';
import SearchFilterModal from '../SearchFilterModal/SearchFilterModal';
import color from '../../theme/color';
import showBottomSheet from '../../components/ActionSheet/ActionSheet';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import analyticsService from '../../analytics/analyticsService';
import searchService from '../../search/searchService';

const View = styled.View``;

const Touchable = styled(TouchableComponent)`
  flex: 1;
  backgroundColor: ${color.white};
`;

const FooterItemContainer = styled.View`
  flex: 1;
  flexDirection: row;
  alignItems: center;
  justifyContent: center;
  backgroundColor: ${color.white};
`;

const FooterText = styled.View`
  marginLeft: 8;
`;

const CountView = styled.View`
  backgroundColor: ${color.primary};
  justifyContent: center;
  alignItems: center;
  width: 16;
  height: 16;
  borderRadius: 8;
  marginLeft: 4;
`;

class SearchResultsFooter extends Component {
  state = {
    showModal: false,
  };

  componentDidMount() {
    this.currentCity = searchService.getCurrentCity();
  }

  getFilterCount = () => {
    const {
      filter: {
        filters: {
          coupleFriendly,
          priceRanges,
          localities,
          amenities,
        },
      },
    } = this.props;

    let count = 0;
    count += coupleFriendly.reduce((c, priceRange) => {
      if (priceRange.checked) {
        return c + 1;
      }
      return c;
    }, 0);
    count += priceRanges.reduce((c, priceRange) => {
      if (priceRange.checked) {
        return c + 1;
      }
      return c;
    }, 0);
    count += localities.reduce((c, priceRange) => {
      if (priceRange.checked) {
        return c + 1;
      }
      return c;
    }, 0);
    count += amenities.reduce((c, priceRange) => {
      if (priceRange.checked) {
        return c + 1;
      }
      return c;
    }, 0);
    return count;
  };

  applySort = (sortItem) => {
    const { filterActions } = this.props;
    filterActions.sortBy(sortItem);
  };

  bottomSheetItemOnPress = (buttonIndex, type) => {
    const { filter: { sort } } = this.props;
    const sortKeys = Object.keys(sort.list);
    if (buttonIndex < sortKeys.length && type !== 'Cancel') {
      this.applySort(sortKeys[buttonIndex]);
    }
  };

  showSheet = () => {
    analyticsService.segmentTrackEvent('Sort Clicked');
    const { filter: { sort } } = this.props;

    const sortList = Object.keys(sort.list)
      .filter((sortKey) => !(sort.list.recommended && sortKey === 'distance'))
      .map((sortKey) => sort.list[sortKey])
      .concat(['Cancel']);

    showBottomSheet(
      sortList,
      sortList.length - 1,
      'Sort By',
      (i) => this.bottomSheetItemOnPress(i, sortList[i]),
    );
  };

  showRefineResultsModal = (status) => {
    analyticsService.segmentTrackEvent('Filter Clicked');
    this.setState({ showModal: status });
  };

  render() {
    const { filter: { sort }, isCoupleFriendlyHotelAvailable } = this.props;
    const count = this.getFilterCount();
    const button = get(sort, 'button');
    const sortBy = get(sort, 'by');

    if (!button) {
      return null;
    }

    return (
      <View>
        <Footer>
          <FooterTab>
            <Touchable
              onPress={this.showSheet}
              testID="sort-button"
              accessibilityLabel="sort-button"
            >
              <FooterItemContainer>
                {getIcon('sort')}
                <FooterText>
                  <Text size="s" color="greyDarker" weight="semibold">
                    {get(button, sortBy)}
                  </Text>
                </FooterText>
              </FooterItemContainer>
            </Touchable>
            <Divider vertical />
            <Touchable
              onPress={() => this.showRefineResultsModal(true)}
              testID="filter-button"
              accessibilityLabel="filter-button"
            >
              <FooterItemContainer>
                {getIcon('filter')}
                <FooterText>
                  <Text size="s" weight="semibold" color="greyDarker">FILTER</Text>
                </FooterText>
                {
                  count > 0 &&
                    <CountView>
                      <Text size="xxs" weight="semibold" color="white">{count}</Text>
                    </CountView>
                }
                {
                  isCoupleFriendlyHotelAvailable && count === 0 &&
                    <Spacer padding={[0, 0, 0, 1]}>
                      <Tag color="yellow" size="small" kind="filled">NEW</Tag>
                    </Spacer>
                }
              </FooterItemContainer>
            </Touchable>
          </FooterTab>
        </Footer>
        <SearchFilterModal
          onClose={() => this.showRefineResultsModal(false)}
          isOpen={this.state.showModal}
          isCoupleFriendlyHotelAvailable={isCoupleFriendlyHotelAvailable}
        />
      </View>
    );
  }
}

SearchResultsFooter.propTypes = {
  filter: PropTypes.object.isRequired,
  filterActions: PropTypes.object.isRequired,
  isCoupleFriendlyHotelAvailable: PropTypes.bool,
};

SearchResultsFooter.defaultProps = {
  isCoupleFriendlyHotelAvailable: false,
};

const mapStateToProps = (state) => ({
  hotel: state.hotel,
  filter: state.filter,
});

const mapDispatchToProps = (dispatch) => ({
  filterActions: bindActionCreators(filterActionCreators, dispatch),
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchResultsFooter);
