const SET_UTM_PARAMS = 'SET_UTM_PARAMS';

const initialState = {
  utmParams: {
    utm_source: 'directTwo',
    utm_medium: '',
    utm_campaign: '',
  },
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_UTM_PARAMS:
      return {
        ...state,
        utmParams: payload.utmParams,
      };
    default:
      return state;
  }
};

export const setSearchParameters = (search) => (dispatch) => {
  const query = search;
  query.utm_source = search.utm_source;
  dispatch({
    type: SET_UTM_PARAMS,
    payload: {
      utmParams: {
        utm_source: query.utm_source,
        utm_medium: query.utm_medium,
        utm_campaign: query.utm_campaign,
      },
    },
  });
};
