import { createStore as reduxCreateStore, compose, applyMiddleware } from 'redux';
// eslint-disable-next-line
import { composeWithDevTools } from 'redux-devtools-extension';
import { default as reduxThunk } from 'redux-thunk';
import { middleware as reduxPack } from 'redux-pack';
import { persistStore, createTransform } from 'redux-persist';
import { REHYDRATE } from 'redux-persist/constants';
import createActionBuffer from 'redux-action-buffer';
import { AsyncStorage } from 'react-native';
// eslint-disable-next-line import/no-extraneous-dependencies
import Reactotron from 'reactotron-react-native';
import authMiddleware from '../auth/authMiddleware';
import api from '../api/api';
import rootReducer from './rootReducer';

const middlewares = [
  reduxThunk.withExtraArgument({ api }),
  reduxPack,
  createActionBuffer(REHYDRATE), // break buffer after rehydrate step
  authMiddleware,
];

const storeEnhancers = [
  applyMiddleware(...middlewares),
];

const userTransform = createTransform(
  // transform state coming from redux on its way to being serialized and stored
  (inboundState) => ({
    profile: inboundState.profile,
    authToken: inboundState.authToken,
    isAuthenticated: inboundState.isAuthenticated,
  }),
  // transform state coming from storage, on its way to be rehydrated into redux
  (outboundState) => ({
    profile: outboundState.profile,
    authToken: outboundState.authToken,
    isAuthenticated: outboundState.isAuthenticated,
  }),
  // configuration options
  { whitelist: ['user'] },
);

export default (initialState) => (onComplete) => {
  const createStore = Reactotron ?
    Reactotron.createStore :
    reduxCreateStore;
  const store = createStore(
    rootReducer,
    initialState,
    composeWithDevTools(
      compose(...storeEnhancers),
    ),
  );
  persistStore(store,
    {
      storage: AsyncStorage,
      transforms: [userTransform],
      whitelist: ['user'],
    },
    onComplete);
  return store;
};
