import { combineReducers } from 'redux';

import uiReducer from '../search/uiDuck';
import bookingReducer from '../booking/bookingDuck';
import checkoutReducer from '../checkout/checkoutDuck';
import contentReducer from '../content/contentDuck';
import filterReducer from '../search/filterDuck';
import hotelReducer from '../hotel/hotelDuck';
import priceReducer from '../price/priceDuck';
import searchReducer from '../search/searchDuck';
import toastReducer from '../toast/toastDuck';
import userReducer from '../user/userDuck';
import couponReducer from '../coupon/couponDuck';
import noInternetConnectionReducer from '../content/noInternetConnectionDuck';
import rateAndReviewReducer from '../feedback/rateAndReviewDuck';
import walletReducer from '../wallet/walletDuck';

export default combineReducers({
  content: contentReducer,
  filter: filterReducer,
  hotel: hotelReducer,
  price: priceReducer,
  search: searchReducer,
  toast: toastReducer,
  user: userReducer,
  checkout: checkoutReducer,
  coupon: couponReducer,
  booking: bookingReducer,
  noInternetConnection: noInternetConnectionReducer,
  rateAndReview: rateAndReviewReducer,
  wallet: walletReducer,
  ui: uiReducer,
});
