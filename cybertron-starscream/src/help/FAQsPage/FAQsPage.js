import React from 'react';
import styled from 'styled-components/native';
import BackButtonHeader from '../../components/Header/BackButtonHeader';
import config from '../../config';

const Container = styled.View`
  flex: 1;
`;

const HeaderContainer = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
`;

const WebView = styled.WebView``;

export default () => (
  <Container>
    <WebView source={{ uri: `${config.baseUrl}/faq/` }} />
    <HeaderContainer><BackButtonHeader middle="FAQs" /></HeaderContainer>
  </Container>
);
