import React from 'react';
import PropTypes from 'prop-types';
import { Item as NBItem } from 'native-base';

const Item = ({ children, ...restProps }) => (
  <NBItem {...restProps}>
    {children}
  </NBItem>);

Item.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Item;
