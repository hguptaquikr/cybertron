import React from 'react';
import PropTypes from 'prop-types';
import { CardItem as NBCardItem } from 'native-base';

const CardItem = ({ children, ...restProps }) => (
  <NBCardItem {...restProps}>
    {children}
  </NBCardItem>);

CardItem.propTypes = {
  children: PropTypes.node.isRequired,
};

export default CardItem;
