/* eslint-disable import/no-unresolved,global-require,no-nested-ternary */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { Animated, View, Image as RNImage } from 'react-native';
import { theme } from 'leaf-ui/native';
import styled from 'styled-components/native';

const Image = styled(RNImage).attrs({
  width: 'auto',
  aspectRatio: 1,
  borderRadius: 1,
})`
  width: ${({ width }) => width};
  aspectRatio: ${({ aspectRatio }) => aspectRatio};
  borderRadius: ${({ borderRadius }) => borderRadius};
  justifyContent: center;
  alignItems: center;
  backgroundColor: ${'transparent'};
`;

const AnimatedImage = Animated.createAnimatedComponent(Image);

const Placeholder = styled(View).attrs({
  width: 'auto',
  aspectRatio: 1,
  borderRadius: 1,
})`
  width: ${({ width }) => width};
  borderRadius: ${({ borderRadius }) => borderRadius};
  position: absolute;
`;

const AnimatedPlaceholder = Animated.createAnimatedComponent(Placeholder);

class ImageLoad extends Component {
  constructor(props) {
    super(props);
    this.state = {
      placeholderOpacity: new Animated.Value(1),
    };
  }

  onLoadEnd = () => {
    Animated.timing(this.state.placeholderOpacity, {
      toValue: 0,
      duration: 500,
      useNativeDriver: true,
    }).start();
  }

  render() {
    const { source, style, resizeMode, placeholderColor } = this.props;
    const { placeholderOpacity } = this.state;
    const imageUrl = get(source, 'uri', '');

    const placeholderStyle = [
      style,
      {
        opacity: placeholderOpacity,
        backgroundColor: placeholderColor || theme.color.greyLight,
      },
    ];

    return (
      <View>
        <AnimatedImage
          style={style}
          cache="force-cache"
          onLoadEnd={this.onLoadEnd}
          source={{ uri: imageUrl.includes('https') ? imageUrl : `https:${imageUrl}` }}
          resizeMode={resizeMode}
        />
        <AnimatedPlaceholder style={placeholderStyle} />
      </View>
    );
  }
}

ImageLoad.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  source: PropTypes.object.isRequired,
  resizeMode: PropTypes.string,
  placeholderColor: PropTypes.string,
};

export default ImageLoad;
