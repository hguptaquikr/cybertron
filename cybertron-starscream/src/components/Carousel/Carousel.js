import React, { Component } from 'react';
import { Platform } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import isEqual from 'lodash/isEqual';
import List from '../../components/List/List';
import ImageLoad from '../ImageLoad/ImageLoad';

const CarouselImage = styled(ImageLoad)`
  borderRadius: 2;
  marginRight: 8;
  ${({ imageCss }) => imageCss};
`;

const TouchableWithoutFeedback = styled.TouchableWithoutFeedback``;

const View = styled.View``;

class Carousel extends Component {
  shouldComponentUpdate(nextProps) {
    if (nextProps.animate && !this.props.animate) {
      this.animateCarousel();
    }
    return !isEqual(nextProps.data, this.props.data) && nextProps.disabled !== this.props.disabled;
  }

  animateCarousel = () => {
    if (this.listRef) {
      setTimeout(() => {
        if (this.listRef) {
          this.listRef.scrollToOffset({ offset: 50, animated: true });
        }
        setTimeout(() => {
          if (this.listRef) {
            this.listRef.scrollToOffset({ offset: 0, animated: true });
          }
        }, 1000);
      }, 1000);
    }
  };

  renderImage = (url) => {
    const { imageCss, disabled, onItemPress } = this.props;
    const imageUrl = disabled ? `${url}${url.includes('?') ? '' : '?'}&mono=868282` : url;

    const image = (
      <CarouselImage
        key={url}
        source={{ uri: imageUrl }}
        imageCss={imageCss}
      />
    );
    if (onItemPress) {
      return (
        <TouchableWithoutFeedback onPress={onItemPress}>
          <View>
            {image}
          </View>
        </TouchableWithoutFeedback>
      );
    }
    return image;
  };

  render() {
    const {
      style,
      data,
      showsHorizontalScrollIndicator,
      lazy,
      testID,
      accessibilityLabel,
    } = this.props;
    return (
      <List
        lazy={lazy}
        horizontal
        data={data}
        showsHorizontalScrollIndicator={showsHorizontalScrollIndicator}
        style={style}
        keyExtractor={(item, i) => i}
        renderItem={({ item: { url } }) => this.renderImage(url)}
        getRef={(ref) => { this.listRef = ref; }}
        testID={testID}
        accessibilityLabel={accessibilityLabel}
      />
    );
  }
}

Carousel.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  data: PropTypes.array.isRequired,
  imageCss: PropTypes.array,
  showsHorizontalScrollIndicator: PropTypes.bool,
  disabled: PropTypes.bool,
  onItemPress: PropTypes.func,
  lazy: PropTypes.number,
  animate: PropTypes.bool,
  testID: PropTypes.string,
  accessibilityLabel: PropTypes.string,
};

Carousel.defaultProps = {
  showsHorizontalScrollIndicator: false,
  disabled: false,
  lazy: Platform.OS === 'android' ? 10 : 3,
  animate: false,
  testID: 'image-carousel',
  accessibilityLabel: 'image-carousel',
};

export default Carousel;
