import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Item, Label, Input as NBInput } from 'native-base';
import Text, { fontFamilies, fontWeights } from '../../components/NewLeaf/Text/Text';
import color from '../../theme/color';
import { isValidField } from '../../services/inputValidationService';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const styles = {
  labelStyle: {
    fontSize: 12,
    paddingBottom: 0,
    paddingTop: 0,
    paddingLeft: 0,
    paddingRight: 0,
  },
  inputStyle: {
    color: color.secondary,
    fontSize: 16,
    fontFamily: fontFamilies.regular.toString(),
    fontWeight: fontWeights.regular.toString(),
    borderBottomColor: color.transparent,
    borderBottomWidth: 0,
    paddingBottom: 0,
    paddingTop: 0,
    paddingLeft: 0,
    paddingRight: 0,
  },
};

const Container = styled.View``;

const InputContainer = styled.View`
  flexDirection: row;
  alignItems: center;
  justifyContent: center;
  borderWidth: ${({ boxed }) => (boxed ? 1 : 0)};
  borderRadius: ${({ boxed }) => (boxed ? 2 : 0)};
  borderColor: ${({ errorMessage }) => (errorMessage ? color.wildWatermelon : color.disabledGrey)}
  borderColor: ${({ focused }) => (focused ? color.primary : color.disabledGrey)}
`;

const LeftIconContainer = styled.TouchableOpacity`
  position: absolute;
  left: 0;
  bottom: 4;
  padding: 8px;
`;

const RightIconContainer = styled.TouchableOpacity`
  position: absolute;
  right: 0;
  bottom: 4;
  padding: 8px;
`;

class Input extends Component {
  state = {
    text: this.props.value,
    errorMessage: '',
    isFocused: false,
  };

  componentWillReceiveProps(nextProps) {
    const { text } = this.state;
    if (nextProps.value !== text) {
      this.setState({ text: nextProps.value });
    }
  }

  onChangeText = (text) => {
    const { onChangeText } = this.props;
    this.setState({ text, errorMessage: '' });
    onChangeText(text);
  };

  onFocus = () => {
    const { onChangeFocus } = this.props;
    this.setState({ isFocused: true });
    if (onChangeFocus) {
      onChangeFocus(true);
    }
  };

  onBlur = () => {
    this.setState({ isFocused: false });
    const { text } = this.state;
    const { validation, validationError, required, onChangeFocus } = this.props;
    const errorMessage = isValidField(text)(validation, validationError, required);
    if (errorMessage) {
      this.setState({ errorMessage });
    }
    if (onChangeFocus) {
      onChangeFocus(false);
    }
  };

  getLeftIcon = () => {
    const { leftIcon, onLeftIconPress, leftIconTint } = this.props;
    if (!leftIcon) {
      return null;
    }
    const icon = (
      getIcon(leftIcon, leftIconTint, 24, 12)
    );
    if (onLeftIconPress) {
      return (
        <LeftIconContainer onPress={onLeftIconPress}>
          {icon}
        </LeftIconContainer>
      );
    }
    return icon;
  };

  getRightIcon = () => {
    const { rightIcon, onRightIconPress, rightIconTint } = this.props;
    if (!rightIcon) {
      return null;
    }
    const icon = (
      getIcon(rightIcon, rightIconTint, 24, 12)
    );
    if (onRightIconPress) {
      return (
        <RightIconContainer onPress={onRightIconPress}>
          {icon}
        </RightIconContainer>
      );
    }
    return icon;
  };

  renderError = (errorMessage) => {
    if (errorMessage) {
      return (
        <Text
          size="xs"
          color={color.wildWatermelon}
          testID="input-field-error-text"
          accessibilityLabel="input-field-error-text"
          style={{ marginTop: 8 }}
        >
          {errorMessage}
        </Text>
      );
    }
    return null;
  };

  render() {
    const {
      label, placeholder, secureTextEntry, returnKeyLabel, keyboardType,
      autoFocus, maxLength, multiline, leftIcon, rightIcon,
      numberOfLines, autoCapitalize, boxed, forcedError,
      underlineColorAndroid, testID, accessibilityLabel,
    } = this.props;
    const { errorMessage, isFocused } = this.state;
    let { labelStyle, inputStyle } = styles;
    let itemStyle = {
      flex: 1,
      borderBottomWidth: 1,
      borderBottomColor: color.disabledGrey,
      marginLeft: 0,
    };

    if (isFocused) {
      labelStyle = { ...labelStyle, color: color.primary };
      itemStyle = { ...itemStyle, borderBottomColor: color.primary };
    } else {
      labelStyle = { ...labelStyle, color: color.disabledGrey };
    }

    if (errorMessage || forcedError) {
      labelStyle = { ...labelStyle, color: color.wildWatermelon };
    }

    if (boxed) {
      itemStyle = {
        ...itemStyle,
        borderBottomColor: color.transparent,
        paddingLeft: 16,
        paddingRight: 16,
      };
      inputStyle = {
        ...inputStyle,
        borderBottomColor: color.transparent,
        margin: 0,
        height: 40,
        lineHeight: 10,
        fontSize: 14,
        color: color.warmGrey,
        top: 0,
      };
    }

    if (leftIcon) {
      labelStyle = {
        ...labelStyle,
        paddingLeft: 12,
      };
      inputStyle = {
        ...inputStyle,
        paddingLeft: 12,
      };
    }

    if (rightIcon) {
      labelStyle = {
        ...labelStyle,
        paddingRight: 12,
      };
      inputStyle = {
        ...inputStyle,
        paddingRight: 12,
      };
    }

    return (
      <Container>
        <InputContainer
          boxed={boxed}
          errorMessage={errorMessage || forcedError}
        >
          {
            this.getLeftIcon()
          }
          <Item
            style={itemStyle}
            floatingLabel
            error={!!errorMessage || !!forcedError}
          >
            {
              label ? (
                <Label style={labelStyle}>
                  {label}
                </Label>
              ) : null
            }
            <NBInput
              placeholder={placeholder}
              value={this.state.text}
              maxLength={maxLength}
              multiline={multiline}
              numberOfLines={numberOfLines}
              secureTextEntry={secureTextEntry}
              returnKeyLabel={returnKeyLabel}
              keyboardType={keyboardType}
              autoFocus={autoFocus}
              autoCorrect={false}
              autoCapitalize={autoCapitalize}
              selectionColor={color.primary}
              placeholderTextColor={color.disabledGrey}
              onChangeText={this.onChangeText}
              style={inputStyle}
              onBlur={this.onBlur}
              onFocus={this.onFocus}
              underlineColorAndroid={underlineColorAndroid}
              testID={testID}
              accessibilityLabel={accessibilityLabel}
            />
          </Item>
          {
            this.getRightIcon()
          }
        </InputContainer>
        {this.renderError(errorMessage || forcedError)}
      </Container>
    );
  }
}

Input.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChangeText: PropTypes.func.isRequired,
  onChangeFocus: PropTypes.func,
  secureTextEntry: PropTypes.bool,
  returnKeyLabel: PropTypes.string,
  keyboardType: PropTypes.string,
  autoFocus: PropTypes.bool,
  autoCapitalize: PropTypes.string,
  maxLength: PropTypes.number,
  multiline: PropTypes.bool,
  numberOfLines: PropTypes.number,
  leftIcon: PropTypes.string,
  leftIconTint: PropTypes.string,
  onLeftIconPress: PropTypes.func,
  rightIcon: PropTypes.string,
  rightIconTint: PropTypes.string,
  onRightIconPress: PropTypes.func,
  validation: PropTypes.oneOf([
    'isEmail',
    'isMobileNumber',
    'isOtp',
    'isPassword',
    'isName',
    'isGSTNumber',
  ]),
  validationError: PropTypes.string,
  forcedError: PropTypes.string,
  required: PropTypes.bool,
  boxed: PropTypes.bool,
  underlineColorAndroid: PropTypes.string,
  testID: PropTypes.string,
  accessibilityLabel: PropTypes.string,
};

Input.defaultProps = {
  keyboardType: 'default',
  maxLength: 100,
  multiline: false,
  numberOfLines: 1,
  autoCapitalize: 'words',
  leftIconTint: color.warmGrey,
  rightIconTint: color.warmGrey,
  validationError: 'Invalid Input',
  required: false,
  boxed: false,
};

export default Input;
