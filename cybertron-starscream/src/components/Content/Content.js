import React from 'react';
import PropTypes from 'prop-types';
import { Content as NBContent } from 'native-base';

const Content = ({
  children,
  getRef,
  showsVerticalScrollIndicator,
  alwaysBounceVertical,
  alwaysBounceHorizontal,
  keyboardShouldPersistTaps,
  ...restProps
}) => (
  <NBContent
    ref={getRef}
    showsVerticalScrollIndicator={showsVerticalScrollIndicator}
    alwaysBounceVertical={alwaysBounceVertical}
    alwaysBounceHorizontal={alwaysBounceHorizontal}
    keyboardShouldPersistTaps={keyboardShouldPersistTaps}
    {...restProps}
  >
    {children}
  </NBContent>
);

Content.propTypes = {
  children: PropTypes.node.isRequired,
  showsVerticalScrollIndicator: PropTypes.bool,
  alwaysBounceVertical: PropTypes.bool,
  alwaysBounceHorizontal: PropTypes.bool,
  getRef: PropTypes.func,
  keyboardShouldPersistTaps: PropTypes.string,
};

Content.defaultProps = {
  showsVerticalScrollIndicator: false,
  alwaysBounceVertical: false,
  alwaysBounceHorizontal: false,
  keyboardShouldPersistTaps: 'handled',
};

export default Content;
