import React from 'react';
import PropTypes from 'prop-types';
import { FooterTab as NBFooterTab } from 'native-base';

const FooterTab = ({ children, ...restprops }) => (
  <NBFooterTab {...restprops}>
    {children}
  </NBFooterTab>
);

FooterTab.propTypes = {
  children: PropTypes.node.isRequired,
};

export default FooterTab;
