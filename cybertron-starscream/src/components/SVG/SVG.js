import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SVGImage from 'react-native-svg-image';
import { View } from 'react-native';

class SVG extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.source !== this.props.source;
  }

  render() {
    const { source, style } = this.props;
    return (
      <View pointerEvents="none" >
        <SVGImage
          showWebviewLoader={false}
          renderLoading={() => null}
          scrollEnabled={false}
          style={style}
          source={{ uri: source.includes('https') ? source : `https:${source}` }}
        />
      </View>
    );
  }
}

SVG.propTypes = {
  source: PropTypes.string.isRequired,
  style: PropTypes.object,
};

export default SVG;
