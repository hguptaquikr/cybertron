import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import color from '../../theme/color';

const dotSizeMap = {
  small: 4,
  medium: 6,
  large: 8,
};

const Container = styled.View`
  flexDirection: row;
  justifyContent: space-around;
`;

const CircleView = styled.View`
  width: ${({ dotSize }) => dotSizeMap[dotSize]};
  height: ${({ dotSize }) => dotSizeMap[dotSize]};
  borderRadius: ${({ dotSize }) => dotSizeMap[dotSize] / 2};
  borderColor: ${({ fillColor }) => fillColor};
  borderWidth: ${({ isFilled }) => (isFilled ? 0 : 1)};
  backgroundColor: ${({ isFilled, fillColor }) => (isFilled ? fillColor : color.transparent)};
  marginHorizontal: 4;
`;

const generateDots = (totalCount, currentPage, dotSize, fillColor) => {
  const dots = [];
  for (let i = 0; i < totalCount; i += 1) {
    dots.push(
      <CircleView
        key={i}
        dotSize={dotSize}
        fillColor={fillColor}
        isFilled={i !== currentPage - 1}
      />,
    );
  }
  return dots;
};

const PagerDotIndicator = ({
  totalCount,
  currentPage,
  dotSize,
  fillColor,
}) => (
  <Container>
    {generateDots(totalCount, currentPage, dotSize, fillColor)}
  </Container>
);

PagerDotIndicator.propTypes = {
  totalCount: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  dotSize: PropTypes.oneOf(['small', 'medium', 'large']),
  fillColor: PropTypes.string,
};

PagerDotIndicator.defaultProps = {
  dotSize: 'medium',
  fillColor: color.whiteOpacity70,
};

export default PagerDotIndicator;
