/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import { default as RNCheckBox } from 'react-native-check-box';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const checkBoxStyle = {
  outerStyle: {
    paddingLeft: 8,
  },
};

const CheckBox = ({ square, checked, color, onPress, ...restProps }) => (
  <RNCheckBox
    style={checkBoxStyle.outerStyle}
    isChecked={checked}
    checkBoxColor={color}
    onClick={onPress}
    checkedImage={
      !square ?
        getIcon('radio-button-active')
        : null
    }
    unCheckedImage={
      !square ?
        getIcon('radio-button-inactive')
      : null
    }
    testID="checkbox"
    accessibilityLabel="checkbox"
    {...restProps}
  />
);

CheckBox.propTypes = {
  square: PropTypes.bool,
  checked: PropTypes.bool,
  color: PropTypes.string,
  onPress: PropTypes.func,
};

CheckBox.defaultProps = {
  square: false,
  onPress: () => {},
};

export default CheckBox;
