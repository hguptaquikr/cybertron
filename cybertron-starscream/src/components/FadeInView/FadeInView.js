import React from 'react';
import PropTypes from 'prop-types';
import { Animated } from 'react-native';

class FadeInView extends React.Component {
  state = {
    fadeAnim: new Animated.Value(0),
  }

  componentDidMount() {
    Animated.timing(
      this.state.fadeAnim,
      {
        toValue: 100,
        delay: 300,
        duration: 500,
        seNativeDriver: true,
      },
    ).start();
  }

  render() {
    const { fadeAnim } = this.state;

    return (
      <Animated.View
        style={{
          ...this.props.style,
          opacity: fadeAnim,
        }}
      >
        {this.props.children}
      </Animated.View>
    );
  }
}

FadeInView.propTypes = {
  style: PropTypes.object,
  children: PropTypes.object,
};

export default FadeInView;
