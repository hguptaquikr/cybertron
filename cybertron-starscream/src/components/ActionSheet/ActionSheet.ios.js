import { ActionSheetIOS } from 'react-native';

const showBottomSheet = (options, cancelButtonIndex, title, onPress) => {
  ActionSheetIOS.showActionSheetWithOptions(
    {
      options,
      cancelButtonIndex,
      title,
    },
    onPress,
  );
};

export default showBottomSheet;
