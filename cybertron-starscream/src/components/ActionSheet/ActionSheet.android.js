import RNBottomSheet from 'react-native-bottom-sheet';

const showBottomSheet = (options, cancelButtonIndex, title, onPress) => {
  RNBottomSheet.showBottomSheetWithOptions(
    {
      options,
      title,
      cancelButtonIndex,
    },
    onPress,
  );
};

export default showBottomSheet;
