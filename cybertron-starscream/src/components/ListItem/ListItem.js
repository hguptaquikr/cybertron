import React from 'react';
import PropTypes from 'prop-types';
import { ListItem as NBListItem } from 'native-base';

const ListItem = ({ children, ...restProps }) => (
  <NBListItem {...restProps}>
    {children}
  </NBListItem>
);

ListItem.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ListItem;
