import React from 'react';
import PropTypes from 'prop-types';

import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const CheckBoxActive = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="check-box-active" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M19,3 L5,3 C3.9,3 3,3.9 3,5 L3,19 C3,20.1 3.9,21 5,21 L19,21 C20.1,21 21,20.1 21,19 L21,5 C21,3.9 20.1,3 19,3 Z M6.3,11.3 C6.7,10.9 7.3,10.9 7.7,11.3 L10,13.6 L16.3,8.2 C16.7,7.8 17.3,7.9 17.7,8.3 C18.1,8.7 18,9.3 17.6,9.7 L10.6,15.7 C10.4,15.9 10.2,15.9 10,15.9 C9.8,15.9 9.5,15.8 9.3,15.6 L6.3,12.6 C5.9,12.4 5.9,11.7 6.3,11.3 Z"
        fill={color}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

CheckBoxActive.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

CheckBoxActive.defaultProps = {
  color: '#4A4A4A',
  size: 24,
};

export default CheckBoxActive;
