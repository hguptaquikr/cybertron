import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { theme } from 'leaf-ui/native';
import Text, { fontFamilies } from '../Text/Text';

const fontSize = {
  xs: 12,
  s: 14,
  m: 16,
  l: 20,
  xl: 28,
  xxl: 44,
};

const types = {
  regular: 'none',
  striked: 'line-through',
};

const Amount = styled.Text`
  ${(props) => props.size ? `font-size: ${fontSize[props.size]};` : ''}
  ${(props) => props.color ? `color: ${props.theme.color[props.color]};` : ''}
  ${(props) => props.bold ? `font-weight: ${props.theme.fontWeight.bold};` : `font-weight: ${props.theme.fontWeight.normal}`}
  ${(props) => props.type ? `text-decoration-line: ${types[props.type]};` : ''}
  ${(props) => props.size ? `font-family: ${fontFamilies.medium};` : ''}
  ${(props) => props.weight && !props.bold ? `font-weight: ${theme.fontWeight[props.weight]};` : ''}
`;

const Price = ({
  price,
  type,
  roundUpTo,
  size,
  bold,
  weight,
  color: pColor,
  negative,
  previewStyle,
  showSign,
  testID,
  accessibilityLabel,
}) => {
  if (price === undefined) {
    return (<Text size="s" preview={previewStyle}>&#x20b9;</Text>);
  }
  const positiveSign = showSign ? '+' : null;

  return (
    <Amount
      bold={bold}
      weight={weight}
      color={pColor}
      size={size}
      type={type}
      testID={testID}
      accessibilityLabel={accessibilityLabel}
    >
      { negative ? '-' : positiveSign} &#x20b9; {price.toFixed(roundUpTo).toLocaleString('en-IN')}
    </Amount>
  );
};

Price.propTypes = {
  price: PropTypes.number,
  bold: PropTypes.bool,
  weight: PropTypes.oneOf(['normal', 'medium', 'semibold', 'bold']),
  negative: PropTypes.bool,
  color: PropTypes.oneOf(['greyDarker', 'greyDark', 'grey', 'green', 'white', 'redDark']),
  size: PropTypes.oneOf(['xs', 's', 'm', 'l', 'xl', 'xxl']),
  type: PropTypes.oneOf(['regular', 'striked']),
  previewStyle: PropTypes.bool,
  showSign: PropTypes.bool,
  roundUpTo: PropTypes.number,
  testID: PropTypes.string,
  accessibilityLabel: PropTypes.string,
};

Price.defaultProps = {
  size: 'm',
  type: 'regular',
  color: 'greyDarker',
  bold: false,
  weight: 'normal',
  negative: false,
  showSign: false,
  roundUpTo: 0,
  testID: 'price-text',
  accessibilityLabel: 'price-text',
};

export default Price;
