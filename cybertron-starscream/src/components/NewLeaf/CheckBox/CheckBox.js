import React from 'react';
import PropTypes from 'prop-types';
import { theme, Spacer, Flex } from 'leaf-ui/native';
import CheckBoxActive from '../Icons/CheckBoxActive';
import CheckBoxInactive from '../Icons/CheckBoxInactive';
import CheckBoxInactiveFilled from '../Icons/CheckBoxInactiveFilled';
import TouchableComponent from '../../Touchable/TouchableComponent';
import Text from '../Text/Text';

class CheckBox extends React.Component {
  _handleOnPress = () => {
    this.props.onPress();
  }

  render() {
    const { label, checked, disabled } = this.props;

    return (
      <Flex flexDirection="row" alignItems="center">
        {!disabled ?
          <TouchableComponent onPress={this._handleOnPress}>
            <Flex flexDirection="row" alignItems="center">
              {checked
              ? <CheckBoxActive color={theme.color.green} />
              : <CheckBoxInactive color={theme.color.greyDarker} />
            }
              <Spacer margin={[0, 0, 0, 1]}>
                <Text size="s" color="greyDarker">
                  {label}
                </Text>
              </Spacer>
            </Flex>
          </TouchableComponent>
        :
          <Flex flexDirection="row" alignItems="center">
            {checked
              ? <CheckBoxActive color={theme.color.greyLight} />
              : <CheckBoxInactiveFilled color={theme.color.greyLight} />
            }
            <Spacer margin={[0, 0, 0, 1]}>
              <Text size="s" color="greyDarker">
                {label}
              </Text>
            </Spacer>
          </Flex>
      }
      </Flex>
    );
  }
}

CheckBox.propTypes = {
  checked: PropTypes.bool.isRequired,
  disabled: PropTypes.bool,
  label: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

CheckBox.defaultProps = {
  disabled: false,
};

export default CheckBox;
