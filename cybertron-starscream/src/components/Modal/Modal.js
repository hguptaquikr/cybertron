import React from 'react';
import PropTypes from 'prop-types';
import {
  Modal as RNModal,
} from 'react-native';
import styled from 'styled-components/native';
import Container from '../Container/Container';
import Header from '../Header/Header';
import Content from '../Content/Content';
import color from '../../theme/color';
import Text from '../NewLeaf/Text/Text';

const View = styled.View``;

const Modal = ({
  title,
  right,
  onClose,
  onRightPress,
  children,
  footer,
  headerStyles,
  footerStyles,
  header,
  noHeaderShadow,
  isOpen,
  androidStatusBarColor,
  iosBarStyle,
  headerBackgroundColor,
  iconProps,
  showHeader,
  ...restProps
}) => (
  <RNModal
    transparent={false}
    visible={isOpen}
    onRequestClose={onClose}
    {...restProps}
  >
    <Container>
      {
        showHeader ? (
          <View>
            <Header
              title={<Text size="m">{title}</Text>}
              right={right}
              iconProps={{ name: 'close', ...iconProps }}
              middle={header}
              onRightPress={onRightPress}
              onLeftPress={onClose}
              noShadow={noHeaderShadow}
              style={{ ...headerStyles, borderBottomColor: 'white' }}
              androidStatusBarColor={androidStatusBarColor}
              iosBarStyle={iosBarStyle}
              backgroundColor={headerBackgroundColor}
            />
          </View>
        ) : null
      }

      <Content
        contentContainerStyle={{ flex: 1 }}
        alwaysBounceVertical={false}
        keyboardShouldPersistTaps="handled"
      >
        {children}
      </Content>
      {
        footer ?
          <View>
            {footer}
          </View>
          : null
      }
    </Container>
  </RNModal>
);

Modal.propTypes = {
  title: PropTypes.string,
  right: PropTypes.object,
  onClose: PropTypes.func,
  onRightPress: PropTypes.func,
  footer: PropTypes.node,
  children: PropTypes.node.isRequired,
  header: PropTypes.node,
  iconProps: PropTypes.object,
  headerStyles: PropTypes.object,
  footerStyles: PropTypes.object,
  isOpen: PropTypes.bool,
  noHeaderShadow: PropTypes.bool,
  androidStatusBarColor: PropTypes.string,
  iosBarStyle: PropTypes.string,
  headerBackgroundColor: PropTypes.string,
  showHeader: PropTypes.bool,
  animationType: PropTypes.string,
  reset: PropTypes.string,
  backgroundColor: PropTypes.string,
};

Modal.defaultProps = {
  title: '',
  reset: '',
  androidStatusBarColor: color.white,
  iosBarStyle: 'light-content',
  backgroundColor: color.white,
  noHeaderShadow: true,
  iconProps: {},
  showHeader: true,
  animationType: 'slide',
};

export default Modal;
