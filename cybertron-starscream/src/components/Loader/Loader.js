import React from 'react';
import Spinner from 'react-native-spinkit';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import styled from 'styled-components/native';
import { theme } from 'leaf-ui/native';
import color from '../../theme/color';
import Text from '../../components/NewLeaf/Text/Text';

const LoaderView = styled.View`
  flex: 1;
  justifyContent: center;
  alignItems: center;
  backgroundColor: ${({ backgroundColor }) => backgroundColor};
`;

const Loader = ({
  loaderType,
  loaderColor,
  loaderSize,
  loadingText,
  backgroundColor,
}) => (
  <LoaderView backgroundColor={backgroundColor}>
    <Spinner
      type={loaderType}
      color={loaderColor}
      size={loaderSize}
      isVisible
    />
    {
      !isEmpty(loadingText) &&
        <Text size="m" color="greyDarker" style={{ marginTop: 24 }}>
          {loadingText}
        </Text>
    }
  </LoaderView>
);

Loader.propTypes = {
  loaderType: PropTypes.string,
  loaderColor: PropTypes.string,
  loaderSize: PropTypes.number,
  loadingText: PropTypes.string,
  backgroundColor: PropTypes.string,
};

Loader.defaultProps = {
  loaderType: 'ThreeBounce',
  loaderColor: theme.color.teal,
  loaderSize: 60,
  loadingText: '',
  backgroundColor: color.white,
};

export default Loader;
