import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components/native';
import { Spacer } from 'leaf-ui/native';
import CheckBox from '../CheckBox/CheckBox';
import Text from '../NewLeaf/Text/Text';
import color from '../../theme/color';

const Container = styled.View``;

const Option = styled.View`
  paddingVertical: 12;
  flexDirection: row;
`;

const defaultContainerStyle = css`
  paddingHorizontal: 16;
  paddingVertical: 12;
`;

class RadioGroup extends Component {
  state = { value: '' };

  onChange = (text) => () => {
    const { setValue } = this.props;
    this.setState({ value: text });
    setValue(text);
  };

  render() {
    const { options, style } = this.props;
    return (
      <Container style={style}>
        {
          options.map(({ value, label }) => (
            <Option key={value}>
              <CheckBox
                onPress={this.onChange(value)}
                color={color.primary}
                checked={value === this.state.value}
              />
              <Spacer margin={[0, 0, 0, 3]}>
                <Text
                  size="m"
                  weight={value === this.state.value ? 'medium' : 'normal'}
                  color="greyDarker"
                >
                  {label}
                </Text>
              </Spacer>
            </Option>
          ))
        }
      </Container>
    );
  }
}

RadioGroup.propTypes = {
  style: PropTypes.array,
  options: PropTypes.array,
  setValue: PropTypes.func.isRequired,
};

RadioGroup.defaultProps = {
  style: defaultContainerStyle,
  options: [],
};

export default RadioGroup;
