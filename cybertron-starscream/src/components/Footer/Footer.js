import React from 'react';
import PropTypes from 'prop-types';
import { Footer as NBFooter } from 'native-base';

const Footer = ({ children }) => (
  <NBFooter>
    {children}
  </NBFooter>
);

Footer.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Footer;
