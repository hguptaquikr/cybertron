import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import ImageLoad from '../ImageLoad/ImageLoad';
import Text from '../NewLeaf/Text/Text';
import color from '../../theme/color';

const EmptyView = styled.View``;

const Container = styled.View`
  backgroundColor: ${color.teal};
  paddingHorizontal: 16;
  paddingVertical: 20;
  borderRadius: 2;
`;

const Row = styled.View`
  flexDirection: row;
  alignItems: center;
  justifyContent: space-between;
`;

const LoginInfoContainer = styled.View`
  marginRight: 24;
`;

const LoginInfoImage = styled(ImageLoad)`
  borderRadius: 0;
  aspectRatio: 1.6;
  flex: 1;
`;

const WhyLoginCard = ({ style }) => (
  <Container style={style}>
    <Text size="m" family="bold" weight="bold" color="white">Login to Treebo</Text>
    <Row>
      <LoginInfoContainer>
        <Text size="s" family="normal" weight="normal" style={{ color: color.whiteOpacity70, marginTop: 8 }}>
          &#x2022; View your Treebo Points
        </Text>
        <Text size="s" family="normal" weight="normal" style={{ color: color.whiteOpacity70, marginTop: 8 }}>
          &#x2022; Manage your Bookings on the go
        </Text>
      </LoginInfoContainer>
      <EmptyView>
        <LoginInfoImage
          source={{
            uri: 'https://images.treebohotels.com/images/app_login_info_icon.png?w=320&h=200&auto=compress&fit=crop',
          }}
        />
      </EmptyView>
    </Row>
  </Container>
);

WhyLoginCard.propTypes = {
  style: PropTypes.array,
};

export default WhyLoginCard;
