import React from 'react';
import { withRouter } from 'react-router-native';
import Header from './Header';
import color from '../../theme/color';

const BackButtonHeader = ({
  history,
  onLeftPress,
  ...restProps
}) => (
  <Header
    {...restProps}
    onLeftPress={onLeftPress || history.goBack}
    backgroundColor={color.white}
    androidStatusBarColor={color.black}
    noShadow
  />
);

BackButtonHeader.propTypes = Header.propTypes;
BackButtonHeader.defaultProps = {
  iconProps: { name: 'back', style: { paddingRight: 8 } },
};

export default withRouter(BackButtonHeader);
