/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import {
  Header as NBHeader,
} from 'native-base';
import styled from 'styled-components/native';
import Text from '../NewLeaf/Text/Text';
import color from '../../theme/color';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const Touchable = styled.TouchableOpacity`
  justifyContent: center;
  alignItems: center;
  alignSelf: center;
  paddingVertical: 4px;
  paddingHorizontal: 12px;
  minWidth: 32;
  height: 32;
`;

const EmptyRightView = styled.View`
  width: 32px;
  height: 32px;
`;

const Middle = styled.View`
  justifyContent: center;
  alignItems: center;
  flex: 1;
  marginHorizontal: 4;
`;

const getLeftIcon = (iconProps, onLeftPress) => (
  <Touchable
    onPress={onLeftPress}
    testID="icon-header-left"
    accessibilityLabel="icon-header-left"
  >
    {getIcon(iconProps.name, iconProps.tintColor)}
  </Touchable>
);

const Header = ({
  middle,
  right,
  onRightPress,
  onLeftPress,
  iconProps,
  style,
  noShadow,
  androidStatusBarColor,
  backgroundColor,
  iosBarStyle,
}) => (
  <NBHeader
    style={{ ...style, paddingLeft: 0, paddingRight: 0 }}
    noShadow={noShadow}
    androidStatusBarColor={androidStatusBarColor}
    backgroundColor={backgroundColor}
    iosBarStyle={iosBarStyle}
  >
    {
      getLeftIcon(iconProps, onLeftPress)
    }
    <Middle
      testID="header-middle-view"
      accessibilityLabel="header-middle-view"
    >
      {
        typeof middle === 'string' ? (
          <Text
            size="m"
            family="medium"
            weight="medium"
            testID="header-middle-text"
            accessibilityLabel="header-middle-text"
          >
            {middle}
          </Text>
        ) : middle
      }
    </Middle>
    {
      right ? (
        <Touchable
          onPress={onRightPress}
          testID="icon-header-right"
          accessibilityLabel="icon-header-right"
        >
          {right}
        </Touchable>
      ) : <EmptyRightView />
    }
  </NBHeader>
);

Header.propTypes = {
  middle: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  right: PropTypes.node,
  style: PropTypes.object,
  iconProps: PropTypes.object,
  onRightPress: PropTypes.func,
  onLeftPress: PropTypes.func,
  noShadow: PropTypes.bool,
  backgroundColor: PropTypes.string,
  androidStatusBarColor: PropTypes.string,
  iosBarStyle: PropTypes.string,
};

Header.defaultPros = {
  backgroundColor: color.white,
  noHeaderShadow: true,
};

export default Header;
