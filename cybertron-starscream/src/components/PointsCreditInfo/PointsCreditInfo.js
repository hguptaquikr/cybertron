import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-native';
import styled from 'styled-components/native';
import Text from '../NewLeaf/Text/Text';
import color from '../../theme/color';

const Container = styled.View`
  paddingHorizontal: 12;
  paddingVertical: 8;
  borderRadius: 2;
  borderWidth: 1;
  borderColor: ${color.orangeYellowOpacity70};
  backgroundColor: ${color.orangeYellowOpacity20};
`;

const PointsCreditInfo = ({ phoneNumber, history, style }) => (
  <Container style={style}>
    <Text
      size="s"
      testID="treebo-points-text"
      accessibilityLabel="treebo-points-text"
    >
      {`Treebo Points will get credited to ${phoneNumber} post checkout. `}
      <Text
        size="s"
        color="blue"
        onPress={() => { history.push('/wallet/intro/'); }} // TODO: Take to FAQ
      >
        View T&C
      </Text>
    </Text>
  </Container>
);

PointsCreditInfo.propTypes = {
  phoneNumber: PropTypes.string.isRequired,
  history: PropTypes.object,
  style: PropTypes.array,
};

export default withRouter(PointsCreditInfo);
