import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Flex, Spacer, Card } from 'leaf-ui/native';
import Text from '../NewLeaf/Text/Text';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const TouchableWithoutFeedback = styled.TouchableWithoutFeedback`
`;

const Accordion = ({
  title,
  children,
  testID,
  accessibilityLabel,
  onPress,
  isSelected,
}) => (
  <Spacer margin={[1, 0, 0, 0]}>
    <Card>
      <TouchableWithoutFeedback
        onPress={onPress}
        testID={testID}
        accessibilityLabel={accessibilityLabel}
      >
        <Spacer padding={[2]}>
          <Flex
            flexDirection="row"
            justifyContent="space-between"
            alignItems="center"
          >
            <Text
              size="m"
              color="greyDarker"
            >
              {title}
            </Text>
            {isSelected ? getIcon('arrow-up') : getIcon('arrow-down')}
          </Flex>
        </Spacer>
      </TouchableWithoutFeedback>
      { isSelected && children }
    </Card>
  </Spacer>
);

Accordion.propTypes = {
  title: PropTypes.string,
  children: PropTypes.object,
  isSelected: PropTypes.bool,
  onPress: PropTypes.func,
  testID: PropTypes.string,
  accessibilityLabel: PropTypes.string,
};

Accordion.defaultProps = {
  testID: 'toggle-component',
  accessibilityLabel: 'toggle-component',
};

export default Accordion;
