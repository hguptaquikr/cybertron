import { Platform } from 'react-native';
import styled from 'styled-components/native';

const TouchableComponent =
  Platform.OS === 'ios' ? styled.TouchableOpacity`` : styled.TouchableNativeFeedback``;

export default TouchableComponent;
