import React from 'react';
import { SectionList as RNSectionList } from 'react-native';
import PropTypes from 'prop-types';

const SectionList = ({
  sections,
  style,
  renderItem,
  renderSectionHeader,
  keyExtractor,
  onRefresh,
  refreshing,
  testID,
  accessibilityLabel,
}) => (
  <RNSectionList
    sections={sections}
    contentContainerStyle={style}
    renderItem={renderItem}
    renderSectionHeader={renderSectionHeader}
    keyExtractor={keyExtractor}
    onRefresh={onRefresh}
    refreshing={refreshing}
    showsVerticalScrollIndicator={false}
    testID={testID}
    accessibilityLabel={accessibilityLabel}
  />
);

SectionList.propTypes = {
  style: PropTypes.array,
  sections: PropTypes.array,
  renderItem: PropTypes.func,
  renderSectionHeader: PropTypes.func,
  keyExtractor: PropTypes.func,
  onRefresh: PropTypes.func,
  refreshing: PropTypes.bool,
  testID: PropTypes.string,
  accessibilityLabel: PropTypes.string,
};

SectionList.defaultProps = {
  sections: [],
  renderItem: () => {},
  renderSectionHeader: () => {},
  testID: 'filters-list',
  accessibilityLabel: 'filters-list',
};

export default SectionList;
