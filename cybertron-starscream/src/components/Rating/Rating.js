import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import TouchableComponent from '../../components/Touchable/TouchableComponent';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const Container = styled.View`
  flexDirection: row;
  justifyContent: center;
`;

const IconView = styled.View`
  width: 32;
  height: 32;
  padding: 4px;
  justifyContent: center;
  alignItems: center;
  marginHorizontal: 6;
`;

class Rating extends Component {
  state = {
    rating: this.props.rating,
  };

  onRatingChanged = (rating) => {
    const { onRatingChange } = this.props;
    this.setState({ rating });
    onRatingChange(rating);
  };

  render() {
    const { maxRating, filledIconName, strokedIconName } = this.props;
    const { rating } = this.state;
    const icons = [];
    for (let i = 0; i < maxRating; i += 1) {
      icons.push(
        <TouchableComponent
          key={i}
          onPress={() => this.onRatingChanged(i + 1)}
        >
          <IconView>
            {
              getIcon(i < rating ? filledIconName : strokedIconName)
            }
          </IconView>
        </TouchableComponent>,
      );
    }

    return (
      <Container>
        {icons}
      </Container>
    );
  }
}

Rating.propTypes = {
  rating: PropTypes.number,
  maxRating: PropTypes.number,
  filledIconName: PropTypes.string,
  strokedIconName: PropTypes.string,
  onRatingChange: PropTypes.func,
};

Rating.defaultProps = {
  rating: 0,
  maxRating: 5,
  filledIconName: 'star-fill',
  strokedIconName: 'star-stroke',
};

export default Rating;
