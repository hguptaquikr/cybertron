import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { LayoutAnimation, Platform } from 'react-native';
import { connect } from 'react-redux';
import styled from 'styled-components/native';
import color from '../../theme/color';
import Text from '../NewLeaf/Text/Text';

const Container = styled.View`
  width: 100%;
  height: ${Platform.OS === 'android' ? 50 : 40};
  justifyContent: center;
  alignItems: center;
  position: absolute;
  bottom: ${({ visible }) => (visible ? 10 : -40)};
  zIndex: 10;
`;

const TextContainer = styled.View`
  minWidth: 60%;
  maxWidth: 80%;
  flex: 1;
  borderRadius: ${Platform.OS === 'android' ? 25 : 20};
  justifyContent: center;
  alignItems: center;
  backgroundColor: ${color.blackOpacity90};
  opacity: ${({ visible }) => (visible ? 1.0 : 0.0)};
  paddingVertical: 12;
  paddingHorizontal: 12;
`;

const AnimationConfig = {
  duration: 200,
  create: {
    type: LayoutAnimation.Types.linear,
    property: LayoutAnimation.Properties.opacity,
  },
  update: {
    type: 'easeInEaseOut',
  },
};

class Toast extends Component {
  constructor(props) {
    super(props);
    LayoutAnimation.configureNext(AnimationConfig);
  }

  state = {
    label: '',
    visible: false,
  };

  componentWillReceiveProps(nextProps) {
    this.showToast(nextProps.toast);
  }

  showToast = (toast) => {
    const { label, timeout } = toast;
    this.hideToast();
    LayoutAnimation.configureNext(AnimationConfig);
    this.setState({ label, visible: true });
    this.toastTimer = setTimeout(() => this.hideToast(), timeout);
  };

  hideToast = () => {
    const { label } = this.state;
    if (label) {
      clearTimeout(this.toastTimer);
      this.toastTimer = null;
      LayoutAnimation.configureNext(AnimationConfig);
      this.setState({ visible: false });
    }
  };

  render() {
    const { label, visible } = this.state;
    return (
      <Container visible={visible}>
        <TextContainer visible={visible}>
          <Text color="white">
            {label}
          </Text>
        </TextContainer>
      </Container>
    );
  }
}

Toast.propTypes = {
  toast: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  toast: state.toast,
});

export default connect(
  mapStateToProps,
)(Toast);
