/* eslint-disable global-require */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { LayoutAnimation } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components/native';
import { withRouter } from 'react-router-native';
import { Spacer } from 'leaf-ui/native';
import * as noInternetActionCreators from '../../content/noInternetConnectionDuck';
import Text from '../../components/NewLeaf/Text/Text';
import color from '../../theme/color';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import analyticsService from '../../analytics/analyticsService';

const Container = styled.View`
  width: 100%;
  padding: 8px;
  borderTopWidth: 1;
  borderTopColor: ${color.lightGrey};
  backgroundColor: ${({ visible }) => (visible ? color.white : color.transparent)};
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
`;

const Touchable = styled.TouchableOpacity`
  justifyContent: center;
  alignItems: center;
  padding: 4px;
  width: 32;
  height: 32;
`;

const AnimationConfig = {
  duration: 200,
  create: {
    type: LayoutAnimation.Types.linear,
    property: LayoutAnimation.Properties.opacity,
  },
  update: {
    type: 'easeInEaseOut',
  },
};

class NoInternetConnection extends Component {
  constructor(props) {
    super(props);
    LayoutAnimation.configureNext(AnimationConfig);
  }

  state = {
    visible: false,
  };

  componentWillReceiveProps(nextProps) {
    const { location } = nextProps;
    if (nextProps.show && location && location.pathname) {
      analyticsService.segmentTrackEvent('No Internet Screen Displayed', { pathname: location.pathname });
    }
    this.setState({ visible: nextProps.show });
  }

  onClosePress = () => {
    const { noInternetActions: { noInternetConnection } } = this.props;
    LayoutAnimation.configureNext(AnimationConfig);
    noInternetConnection(false);
  };

  render() {
    const { location, history } = this.props;
    const { visible } = this.state;
    if (!visible) {
      return null;
    }
    return (
      <Container visible={visible}>
        <Touchable onPress={this.onClosePress}>
          {getIcon('close')}
        </Touchable>
        <Spacer margin={[2, 1, 1, 1]}>
          <Text size="l" weight="bold">No Internet</Text>
        </Spacer>
        <Spacer margin={[1]}>
          <Text
            size="s"
            color="greyDark"
          >
            Looks like you don’t have an active internet connection.
          </Text>
          <Text
            size="s"
            color="blue"
            weight="medium"
            onPress={() => {
                  analyticsService.segmentTrackEvent('Try Again Clicked', { pathname: location.pathname });
                  history.replace({ ...location, state: 'reload' });
                  this.onClosePress();
                }}
          >
              Try Again
          </Text>
        </Spacer>
      </Container>
    );
  }
}

NoInternetConnection.propTypes = {
  show: PropTypes.bool.isRequired,
  noInternetActions: PropTypes.object.isRequired,
  history: PropTypes.object,
  location: PropTypes.object,
};

const mapStateToProps = (state) => ({
  show: state.noInternetConnection.show,
});

const mapDispatchToProps = (dispatch) => ({
  noInternetActions: bindActionCreators(noInternetActionCreators, dispatch),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(NoInternetConnection));
