import React, { Component } from 'react';
import { View, FlatList, Platform } from 'react-native';
import PropTypes from 'prop-types';

const footerStyle = {
  flex: 1,
  paddingTop: 16,
  paddingBottom: 16,
};

class List extends Component {
  constructor(props) {
    super(props);
    const { lazy, data } = this.props;
    this.state = {
      data: lazy ? data.slice(0, lazy) : data,
      showFooter: !!lazy,
    };
  }

  componentWillReceiveProps(nextProps) {
    const { showFooter, data: localData } = this.state;
    const { data: rootData } = nextProps;

    this.setState({
      data: showFooter ? rootData.slice(0, localData.length) : rootData,
    });
  }

  onMomentumScrollBegin = () => {
    this.onEndReachedCalledDuringMomentum = false;
  };

  onEndReached = () => {
    if (!this.onEndReachedCalledDuringMomentum) {
      this.onEndReachedCalledDuringMomentum = true;
      this.loadNewData();
    }
  };

  loadNewData = () => {
    const { showFooter, data: localData } = this.state;
    const { data: rootData, step, lazy } = this.props;

    if (showFooter) {
      const newLength = localData.length + (step || lazy);

      this.setState({
        data: rootData.slice(0, newLength),
        showFooter: rootData.length > newLength,
      });
    }
  };

  listFooterComponent = () => {
    const { showFooter } = this.state;
    const { listFooterComponent } = this.props;
    if (showFooter || listFooterComponent) {
      return (
        <View style={footerStyle}>
          {listFooterComponent}
        </View>
      );
    }
    return null;
  };

  render() {
    const { showFooter, data } = this.state;
    const {
      renderItem,
      horizontal,
      showsHorizontalScrollIndicator,
      itemSeparatorComponent,
      style,
      contentContainerStyle,
      keyExtractor,
      keyboardShouldPersistTaps,
      listHeaderComponent,
      scrollEnabled,
      getRef,
      pagingEnabled,
      onScroll,
      bounces,
      contentInset,
      testID,
      accessibilityLabel,
    } = this.props;

    return (
      <FlatList
        data={data}
        horizontal={horizontal}
        extraData={{ showFooter }}
        onEndReached={this.onEndReached}
        onEndReachedThreshold={Platform.OS === 'ios' ? 0.3 : 0.5}
        onMomentumScrollBegin={this.onMomentumScrollBegin}
        showsHorizontalScrollIndicator={showsHorizontalScrollIndicator}
        style={style}
        contentContainerStyle={contentContainerStyle}
        ItemSeparatorComponent={itemSeparatorComponent}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        keyboardShouldPersistTaps={keyboardShouldPersistTaps}
        ListFooterComponent={this.listFooterComponent}
        ListHeaderComponent={listHeaderComponent}
        removeClippedSubviews
        ref={getRef}
        pagingEnabled={pagingEnabled}
        onScroll={onScroll}
        scrollEnabled={scrollEnabled}
        scrollEventThrottle={16}
        bounces={bounces}
        automaticallyAdjustContentInsets={false}
        contentInset={{ top: contentInset }}
        testID={testID}
        accessibilityLabel={accessibilityLabel}
      />
    );
  }
}

List.propTypes = {
  data: PropTypes.array.isRequired,
  renderItem: PropTypes.func.isRequired,
  horizontal: PropTypes.bool,
  lazy: PropTypes.number,
  step: PropTypes.number,
  showsHorizontalScrollIndicator: PropTypes.bool,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  contentContainerStyle: PropTypes.object,
  listFooterComponent: PropTypes.node,
  listHeaderComponent: PropTypes.node,
  itemSeparatorComponent: PropTypes.func,
  keyExtractor: PropTypes.func,
  keyboardShouldPersistTaps: PropTypes.string,
  getRef: PropTypes.func,
  pagingEnabled: PropTypes.bool,
  onScroll: PropTypes.func,
  scrollEnabled: PropTypes.bool,
  bounces: PropTypes.bool,
  contentInset: PropTypes.number,
  testID: PropTypes.string,
  accessibilityLabel: PropTypes.string,
};

List.defaultProps = {
  horizontal: false,
  showsHorizontalScrollIndicator: false,
  keyboardShouldPersistTaps: 'handled',
  pagingEnabled: false,
  bounces: true,
  scrollEnabled: true,
  contentInset: 0,
};

export default List;
