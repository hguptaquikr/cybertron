import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dimensions } from 'react-native';
import moment from 'moment';
import Calendar from 'react-native-day-picker';
import color from '../../theme/color';

const windowDimensions = Dimensions.get('window');

class RodimusCalendar extends Calendar {
  componentWillReceiveProps({ selectFrom, selectTo, monthsCount, startDate }) {
    this.selectFrom = selectFrom;
    this.selectTo = selectTo;
    this.months = this.generateMonths(monthsCount, startDate);

    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(this.months),
    });
  }

  // Overriding the library method
  generateMonths(count, startDate) {
    const months = [...Array(count)];
    const monthIterator = startDate;
    const { isFutureDate, startFromMonday } = this.props;

    const startUTC = Date.UTC(startDate.getYear(), startDate.getMonth(), startDate.getDate());

    const formatDates = (day) => {
      const dateUTC = Date.UTC(day.getYear(), day.getMonth(), day.getDate());
      return {
        date: day,
        status: this.getStatus(day, this.selectFrom, this.selectTo),
        disabled: day.getMonth() !== monthIterator.getMonth()
        || ((isFutureDate) ? startUTC > dateUTC : startUTC < dateUTC),
      };
    };

    return months.map(() => {
      const month = this.getDates(monthIterator, startFromMonday).map(formatDates);

      if (isFutureDate) {
        monthIterator.setMonth(monthIterator.getMonth() + 1, 1);
      } else {
        monthIterator.setMonth(monthIterator.getMonth() - 1, 1);
      }

      return month;
    });
  }
}

class DateRangePicker extends Component {
  onSelectionChange = (date, prevDate) => {
    const { onDatesChange } = this.props;
    const selectedDate = moment(date);
    const selectedPrevDate = prevDate && moment(prevDate);

    const dates = prevDate && selectedDate.isAfter(selectedPrevDate, 'day') ? {
      startDate: selectedPrevDate,
      endDate: selectedDate,
    } : { startDate: selectedDate };

    if (selectedDate && selectedPrevDate) {
      onDatesChange(dates);
    }
  }

  render() {
    const { startDate, endDate } = this.props;
    const calendarFirstDay = new Date();

    return (
      <RodimusCalendar
        isFutureDate
        startDate={calendarFirstDay}
        selectFrom={startDate && startDate.toDate()}
        selectTo={endDate && endDate.toDate()}
        onSelectionChange={this.onSelectionChange}
        width={windowDimensions.width - 2}
        dayCommonTextColor={color.charcoalGrey}
        dayDisabledTextColor={color.disabledGrey}
        daySelectedBackColor={color.primary}
        daySelectedTextColor={color.white}
        dayInRangeBackColor={color.primary}
        dayInRangeTextColor={color.white}
        headerSepColor={color.disabledGreyOpacity50}
        monthTextColor={color.charcoalGrey}
        monthsLocale={['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']}
        weekDaysLocale={['S', 'M', 'T', 'W', 'T', 'F', 'S']}
        testID="calendar"
        accessibilityLabel="calendar"
      />
    );
  }
}

DateRangePicker.propTypes = {
  onDatesChange: PropTypes.func,
  startDate: PropTypes.object,
  endDate: PropTypes.object,
};

export default DateRangePicker;
