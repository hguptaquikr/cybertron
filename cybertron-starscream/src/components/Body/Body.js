import React from 'react';
import PropTypes from 'prop-types';
import { Body as NBBody } from 'native-base';

const Body = ({ children }) => (
  <NBBody>
    {children}
  </NBBody>
);

Body.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Body;
