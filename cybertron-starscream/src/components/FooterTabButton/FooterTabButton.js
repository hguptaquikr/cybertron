import React from 'react';
import PropTypes from 'prop-types';
import { Button as NBButton } from 'native-base';

const FooterTabButton = ({ children }) => (
  <NBButton vertical={false}>
    {children}
  </NBButton>
);

FooterTabButton.propTypes = {
  children: PropTypes.node.isRequired,
};

export default FooterTabButton;
