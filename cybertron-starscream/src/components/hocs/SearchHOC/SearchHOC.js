import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-native';
import kebabCase from 'lodash/kebabCase';
import urlService from '../../../services/urlService';
import analyticsService from '../../../analytics/analyticsService';

const SearchHOC = (WrappedComponent) => withRouter(
  class extends Component {
    static propTypes = {
      search: PropTypes.object.isRequired,
      location: PropTypes.object.isRequired,
      history: PropTypes.object.isRequired,
      searchActions: PropTypes.object.isRequired,
    }

    componentDidMount() {
      this.initialSearchState = this.props.search;
    }

    performSearch = (search, searchActions, place) => {
      const { location: { pathname } } = this.props;
      this.search = search;
      this.searchActions = searchActions;
      this.place = place || search.searchInput.place;

      if (
        pathname === 'itinerary'
        && this.initialSearchState.searchInput.place.q === this.place.q
      ) {
        this.handleCheckoutSearch();
      } else if (search.searchInput.place.hotelId) {
        this.handleHotelSearch();
      } else if (this.place.q === 'Near Me') {
        this.handleNearMeSearch();
      } else {
        this.handleSearch();
      }
    }

    handleCheckoutSearch = () => {
      this.searchActions.checkHotelAvailability()
        .then((res) => {
          if (res.payload.available) {
            this.showCheckoutDetails(res.payload.rooms);
          } else {
            this.showNearbyHotels();
          }
        });
    }

    handleHotelSearch = () => {
      this.searchActions.checkHotelAvailability()
        .then((res) => {
          if (res.payload.available) {
            this.showHotelDetails();
          } else {
            this.showNearbyHotels();
          }
        });
    }

    handleNearMeSearch = () => {
      const { props: { searchActions, history } } = this;
      const query = {
        q: 'Near Me',
        type: 'near-me',
      };

      searchActions.searchLocationChange(query);
      analyticsService.segmentTrackEvent('Near Me Clicked', { source: 'Landing Page Search Widget' });
      history.push('/search/');
    }

    showCheckoutDetails = (availableRooms) => {
      const { props: { history }, search } = this;
      const room = availableRooms.find(({ type }) => type === search.roomConfig.roomType)
        || availableRooms[0];
      history.push(urlService.getCheckoutUrl(search, room.type));
    }

    showHotelDetails = () => {
      const { search: { datePicker, roomConfig }, place, props: { history } } = this;
      const city = kebabCase(place.area.city);
      const [hotelName, hotelLocality] = place.q.split(',');
      const hotelSlug = kebabCase(`${hotelName} ${hotelLocality}`);
      history.push({
        pathname: `/hotels-in-${city}/${hotelSlug}-${place.hotelId}/`,
        query: urlService.constructUrlQuery({
          place,
          range: datePicker.range,
          rooms: roomConfig.rooms,
        }),
      });
    }

    showNearbyHotels = () => this.handleSearch();

    handleSearch = () => {
      const {
        place,
        props: { history },
        search: { datePicker, roomConfig },
      } = this;

      history.push({
        pathname: '/search/',
        query: urlService.constructUrlQuery({
          place,
          range: datePicker.range,
          rooms: roomConfig.rooms,
        }),
      });
    }

    render() {
      const additionalProps = {
        performSearch: this.performSearch,
      };

      return (
        <WrappedComponent {...this.props} {...additionalProps} />
      );
    }
  },
);

export default SearchHOC;
