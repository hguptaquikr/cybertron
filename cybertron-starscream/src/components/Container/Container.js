import React from 'react';
import PropTypes from 'prop-types';
import { Container as NBContainer } from 'native-base';

const Container = ({ children, ...restProps }) => (
  <NBContainer {...restProps}>
    {children}
  </NBContainer>);

Container.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Container;
