import React from 'react';
import PropTypes from 'prop-types';
import { View, Animated, Easing } from 'react-native';

class SlideInView extends React.Component {
  state = {
    x: new Animated.Value(300),
  };

  componentDidMount() {
    Animated.spring(this.state.x, {
      toValue: 0,
      duration: 3000,
      easing: Easing.in(Easing.ease),
      velocity: 1,
      bounciness: 0,
      useNativeDriver: true,
    }).start();
  }

  render() {
    const { disabled } = this.props;
    const { x } = this.state;
    return (
      <View>
        <Animated.View
          style={
            disabled ?
            {
              ...this.props.style,
            } :
            {
              ...this.props.style,
              transform: [
                {
                  translateX: x,
                },
              ],
            }
          }
        >
          {this.props.children}
        </Animated.View>
      </View>
    );
  }
}

SlideInView.propTypes = {
  style: PropTypes.object,
  disabled: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

SlideInView.defaultProps = {
  disabled: false,
};

export default SlideInView;
