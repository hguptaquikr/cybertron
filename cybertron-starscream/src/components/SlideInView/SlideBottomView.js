import React from 'react';
import PropTypes from 'prop-types';
import { Animated, Easing, Dimensions } from 'react-native';

const windowHeight = Dimensions.get('window').height;

class SlideBottomView extends React.Component {
  state = {
    positionY: new Animated.Value(windowHeight),
    slideIn: true,
  };

  componentDidMount() {
    this.animate();
  }

  animate = () => {
    Animated.timing(this.state.positionY, {
      toValue: this.state.slideIn ? 0 : windowHeight,
      duration: 3000,
      delay: 1000,
      easing: Easing.in(Easing.ease),
      velocity: 1,
      useNativeDriver: true,
    }).start(() => {
      this.setState((prevState) => ({
        ...prevState,
        slideIn: !prevState.slideIn,
      }));
    });
  };

  render() {
    const { positionY } = this.state;
    const { children } = this.props;
    return children(
      positionY,
      this.animate,
    );
  }
}

SlideBottomView.propTypes = {
  children: PropTypes.func.isRequired,
};

export default SlideBottomView;
