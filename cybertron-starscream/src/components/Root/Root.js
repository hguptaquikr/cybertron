import React from 'react';
import PropTypes from 'prop-types';
import { Root as NBRoot } from 'native-base';

const Root = ({ children, ...restProps }) => (
  <NBRoot {...restProps}>
    {children}
  </NBRoot>);

Root.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Root;
