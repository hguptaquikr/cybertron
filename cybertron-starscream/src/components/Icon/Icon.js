import React from 'react';
import PropTypes from 'prop-types';
import { Icon as NBIcon } from 'native-base';

const Icon = ({ name, iconTint, size, style }) => (
  <NBIcon
    name={name}
    style={{ fontSize: size, color: iconTint, ...style }}
  />
);

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  iconTint: PropTypes.string,
  size: PropTypes.number,
  style: PropTypes.object,
};

Icon.defaultProps = {
  style: {},
};

export default Icon;
