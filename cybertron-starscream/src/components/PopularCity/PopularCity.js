import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import debounce from 'lodash/debounce';
import ImageLoad from '../../components/ImageLoad/ImageLoad';
import Text from '../../components/NewLeaf/Text/Text';
import { imageResolutions } from '../../utils/utils';

const PopularCityContainer = styled.View`
  justifyContent: center;
  alignItems: center;
`;

const TouchableContainer = styled.TouchableOpacity``;

const Image = styled(ImageLoad)`
  width: ${({ small }) => (small ? 56 : 72)};
  borderRadius: ${({ small }) => (small ? 28 : 36)}
`;

const CityName = styled.View`
  marginTop: 12;
`;

const PopularCity = ({
  city,
  small,
  onPress,
  page,
}) => (
  <TouchableContainer
    onPress={debounce(() => onPress(city, page), 500)}
    testID="popular-city-button"
    accessibilityLabel="popular-city-button"
  >
    <PopularCityContainer>
      <Image
        source={{ uri: `${city.image}?${imageResolutions.highres}` }}
        small={small}
        isShowActivity={false}
      />
      {city.name &&
        <CityName>
          <Text size="s" weight="medium">{city.name}</Text>
        </CityName>
      }
    </PopularCityContainer>
  </TouchableContainer>
);

PopularCity.propTypes = {
  city: PropTypes.object.isRequired,
  small: PropTypes.bool,
  onPress: PropTypes.func,
  page: PropTypes.string.isRequired,
};

PopularCity.defaultProps = {
  small: false,
};

export default PopularCity;
