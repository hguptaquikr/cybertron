/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import Text from '../../components/NewLeaf/Text/Text';
import color from '../../theme/color';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const TapSectionContainer = styled.TouchableOpacity`
  backgroundColor: ${color.white};
  flex: 1;
  flexDirection: row;
  paddingVertical: 12;
  paddingHorizontal: 16;
  alignItems: center;
`;

const TapSection = ({ title, children, onPress }) => (
  <TapSectionContainer
    onPress={onPress}
    underlayColor={color.warmGrey}
  >
    <Text
      size="m"
      style={{
        color: color.charcoalGrey,
        flex: 3,
        justifyContent: 'center',
      }}
    >
      {title}
    </Text>
    {getIcon('arrow-next')}
    {children}
  </TapSectionContainer>
);

TapSection.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  children: PropTypes.node,
};

export default TapSection;
