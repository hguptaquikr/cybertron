import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Polygon,
  Defs,
} from 'react-native-svg';

const ArrowFilledDown = ({ color, size }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="arrow-filled-down" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Polygon
        id="Shape"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
        points="12 15 8 9 16 9"
      />
    </G>
  </Svg>
);

ArrowFilledDown.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default ArrowFilledDown;
