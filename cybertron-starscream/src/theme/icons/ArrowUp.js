import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const ArrowUp = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="arrow-up" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M12.9875,18.8875 C12.9875,19.4875 12.5875,19.9875 11.9875,19.9875 C11.3875,19.9875 10.9875,19.5875 10.9875,18.9875 L10.9875,7.3875 L5.6875,12.6875 C5.2875,13.0875 4.6875,13.0875 4.2875,12.6875 C4.0875,12.4875 3.9875,12.1875 3.9875,11.9875 C3.9875,11.7875 4.0875,11.4875 4.2875,11.2875 L11.2875,4.2875 C11.3875,4.1875 11.4875,4.1875 11.5875,4.0875 C11.8875,3.9875 12.1875,3.9875 12.3875,4.0875 C12.4875,4.0875 12.5875,4.1875 12.6875,4.2875 L19.6875,11.2875 C20.0875,11.6875 20.0875,12.2875 19.6875,12.6875 C19.2875,13.0875 18.6875,13.0875 18.2875,12.6875 L12.9875,7.3875 L12.9875,18.8875 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

ArrowUp.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default ArrowUp;
