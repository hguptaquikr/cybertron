import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const RadioButtonInactive = ({ color, size }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="radio-button-inactive" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M12,22 C6.5,22 2,17.5 2,12 C2,6.5 6.5,2 12,2 C17.5,2 22,6.5 22,12 C22,17.5 17.5,22 12,22 Z M12,4 C7.6,4 4,7.6 4,12 C4,16.4 7.6,20 12,20 C16.4,20 20,16.4 20,12 C20,7.6 16.4,4 12,4 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

RadioButtonInactive.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default RadioButtonInactive;
