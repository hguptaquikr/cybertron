import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const BookingConfirmed = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="booking-confirmed" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Combined-Shape"
        d="M12.6,2.1 C18.1,2.1 22.6,6.6 22.6,12.1 C22.6,17.6 18.1,22.1 12.6,22.1 C7.1,22.1 2.6,17.6 2.6,12.1 C2.6,6.6 7.1,2.1 12.6,2.1 Z M12.6,20.1 C17,20.1 20.6,16.5 20.6,12.1 C20.6,7.7 17,4.1 12.6,4.1 C8.2,4.1 4.6,7.7 4.6,12.1 C4.6,16.5 8.2,20.1 12.6,20.1 Z M18.4,8.5 C18.7,8.9 18.7,9.5 18.4,10 L11.4,16 C11.2,16.1 10.9,16.2 10.7,16.2 C10.5,16.2 10.2,16.1 10,15.9 L7,12.9 C6.6,12.5 6.6,11.9 7,11.5 C7.4,11.1 8,11.1 8.4,11.5 L10.7,13.8 L17,8.4 C17.4,8 18,8.1 18.4,8.5 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

BookingConfirmed.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default BookingConfirmed;
