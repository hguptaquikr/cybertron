import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const Close = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="close" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M13.4,12 L17.7,7.7 C18.1,7.3 18.1,6.7 17.7,6.3 C17.3,5.9 16.7,5.9 16.3,6.3 L12,10.6 L7.7,6.3 C7.3,5.9 6.7,5.9 6.3,6.3 C5.9,6.7 5.9,7.3 6.3,7.7 L10.6,12 L6.3,16.3 C5.9,16.7 5.9,17.3 6.3,17.7 C6.5,17.9 6.8,18 7,18 C7.2,18 7.5,17.9 7.7,17.7 L12,13.4 L16.3,17.7 C16.5,17.9 16.8,18 17,18 C17.2,18 17.5,17.9 17.7,17.7 C18.1,17.3 18.1,16.7 17.7,16.3 L13.4,12 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

Close.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default Close;
