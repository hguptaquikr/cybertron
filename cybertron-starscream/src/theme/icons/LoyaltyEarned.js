import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const LoyaltyEarned = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="loyalty-earned" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Combined-Shape"
        d="M19,3.7 C20.1,3.7 21.1,4.7 21.1,5.8 L21.1,19.7 C21.1,20.8 20.2,21.7 19.1,21.7 L16.1,21.7 C15.5,21.7 15.1,21.3 15.1,20.7 C15.1,20.1 15.5,19.7 16.1,19.7 L19.1,19.7 L19.1,5.7 L5.1,5.7 L5.1,8.7 C5.1,9.3 4.7,9.7 4.1,9.7 C3.5,9.7 3.1,9.3 3.1,8.7 L3.1,5.7 C3.1,4.6 4,3.7 5.1,3.7 L19,3.7 Z M11.4,12 C11.7,11.6 12.4,11.6 12.8,12 C13.2,12.4 13.2,13 12.8,13.4 L6.5,19.7 L11.1,19.7 C11.7,19.7 12.1,20.1 12.1,20.7 C12.1,21.3 11.7,21.7 11.1,21.7 L5.1,21.7 C4,21.7 3.1,20.8 3.1,19.7 L3.1,13.7 C3.1,13.1 3.5,12.7 4.1,12.7 C4.7,12.7 5.1,13.1 5.1,13.7 L5.1,18.3 L11.4,12 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

LoyaltyEarned.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default LoyaltyEarned;
