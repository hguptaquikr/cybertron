import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const AngleUp = ({ color, size }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="angle-up" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M12,8 C12.3,8 12.5,8.1 12.7,8.3 L18.7,14.3 C19.1,14.7 19.1,15.3 18.7,15.7 C18.3,16.1 17.7,16.1 17.3,15.7 L12,10.4 L6.7,15.7 C6.3,16.1 5.7,16.1 5.3,15.7 C4.9,15.3 4.9,14.7 5.3,14.3 L11.3,8.3 C11.5,8.1 11.8,8 12,8 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

AngleUp.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default AngleUp;
