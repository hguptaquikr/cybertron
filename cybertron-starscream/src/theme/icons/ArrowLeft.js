import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const ArrowLeft = ({ color, size }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="arrow-left" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M18.875,11 L7.375,11 L12.675,5.7 C13.075,5.3 13.075,4.7 12.675,4.3 C12.275,3.9 11.675,3.9 11.275,4.3 L4.275,11.3 C4.175,11.4 4.075,11.5 4.075,11.6 C3.975,11.8 3.975,12.1 4.075,12.4 C4.175,12.5 4.175,12.6 4.275,12.7 L11.275,19.7 C11.475,19.9 11.775,20 11.975,20 C12.175,20 12.475,19.9 12.675,19.7 C13.075,19.3 13.075,18.7 12.675,18.3 L7.375,13 L18.975,13 C19.575,13 19.975,12.6 19.975,12 C19.975,11.4 19.475,11 18.875,11 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

ArrowLeft.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default ArrowLeft;
