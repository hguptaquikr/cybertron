import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const ArrowRight = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="arrow-right" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M5.1,13 C4.5,13 4,12.6 4,12 C4,11.4 4.4,11 5,11 L16.6,11 L11.3,5.7 C10.9,5.3 10.9,4.7 11.3,4.3 C11.5,4.1 11.8,4 12,4 C12.2,4 12.5,4.1 12.7,4.3 L19.7,11.3 C19.8,11.4 19.8,11.5 19.9,11.6 C20,11.9 20,12.2 19.9,12.4 C19.9,12.5 19.8,12.6 19.7,12.7 L12.7,19.7 C12.3,20.1 11.7,20.1 11.3,19.7 C10.9,19.3 10.9,18.7 11.3,18.3 L16.6,13 L5.1,13 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

ArrowRight.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default ArrowRight;
