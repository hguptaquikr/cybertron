import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const RadioButtonActive = ({ color, size }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="radio-button-active" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M12,2 C6.5,2 2,6.5 2,12 C2,17.5 6.5,22 12,22 C17.5,22 22,17.5 22,12 C22,6.5 17.5,2 12,2 Z M17.6,9.8 L10.6,15.8 C10.4,16 10.2,16 10,16 C9.8,16 9.5,15.9 9.3,15.7 L6.3,12.7 C5.9,12.3 5.9,11.7 6.3,11.3 C6.7,10.9 7.3,10.9 7.7,11.3 L10,13.6 L16.3,8.2 C16.7,7.8 17.3,7.9 17.7,8.3 C18.1,8.8 18.1,9.4 17.6,9.8 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

RadioButtonActive.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default RadioButtonActive;
