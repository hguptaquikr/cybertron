import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const Clock = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="clock" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Combined-Shape"
        d="M12.6,4.9 C17,4.9 20.6,8.5 20.6,12.9 C20.6,17.3 17,20.9 12.6,20.9 C8.2,20.9 4.6,17.3 4.6,12.9 C4.6,8.5 8.2,4.9 12.6,4.9 Z M12.6,18.9 C15.9,18.9 18.6,16.2 18.6,12.9 C18.6,9.6 15.9,6.9 12.6,6.9 C9.3,6.9 6.6,9.6 6.6,12.9 C6.6,16.2 9.3,18.9 12.6,18.9 Z M13.6,12.5 L15.3,14.2 C15.7,14.6 15.7,15.2 15.3,15.6 C15.1,15.8 14.8,15.9 14.6,15.9 C14.4,15.9 14.1,15.8 13.9,15.6 L11.9,13.6 C11.8,13.5 11.8,13.4 11.7,13.3 C11.6,13.2 11.6,13 11.6,12.9 L11.6,9.9 C11.6,9.3 12,8.9 12.6,8.9 C13.2,8.9 13.6,9.3 13.6,9.9 L13.6,12.5 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

Clock.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default Clock;
