import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const Filter = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="filter" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Combined-Shape"
        d="M19,15.2 C20.2,15.6 21,16.7 21,18 C21,19.7 19.7,21 18,21 C16.3,21 15,19.7 15,18 C15,16.7 15.8,15.6 17,15.2 L17,4 C17,3.4 17.4,3 18,3 C18.6,3 19,3.4 19,4 L19,15.2 Z M18,19 C18.6,19 19,18.6 19,18 C19,17.4 18.6,17 18,17 C17.4,17 17,17.4 17,18 C17,18.6 17.4,19 18,19 Z M9,6 C9,7.3 8.2,8.4 7,8.8 L7,20 C7,20.6 6.6,21 6,21 C5.4,21 5,20.6 5,20 L5,8.8 C3.8,8.4 3,7.3 3,6 C3,4.3 4.3,3 6,3 C7.7,3 9,4.3 9,6 Z M6,7 C6.6,7 7,6.6 7,6 C7,5.4 6.6,5 6,5 C5.4,5 5,5.4 5,6 C5,6.6 5.4,7 6,7 Z M15,12 C15,13.3 14.2,14.4 13,14.8 L13,20 C13,20.6 12.6,21 12,21 C11.4,21 11,20.6 11,20 L11,14.8 C9.8,14.4 9,13.3 9,12 C9,10.7 9.8,9.6 11,9.2 L11,4 C11,3.4 11.4,3 12,3 C12.6,3 13,3.4 13,4 L13,9.2 C14.2,9.6 15,10.7 15,12 Z M12,13 C12.6,13 13,12.6 13,12 C13,11.4 12.6,11 12,11 C11.4,11 11,11.4 11,12 C11,12.6 11.4,13 12,13 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

Filter.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default Filter;
