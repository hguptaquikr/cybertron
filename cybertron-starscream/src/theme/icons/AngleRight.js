import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const AngleRight = ({ color, size }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="angle-right" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M16,12 C16,12.3 15.9,12.5 15.7,12.7 L9.7,18.7 C9.3,19.1 8.7,19.1 8.3,18.7 C7.9,18.3 7.9,17.7 8.3,17.3 L13.6,12 L8.3,6.7 C7.9,6.3 7.9,5.7 8.3,5.3 C8.7,4.9 9.3,4.9 9.7,5.3 L15.7,11.3 C15.9,11.5 16,11.8 16,12 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

AngleRight.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default AngleRight;
