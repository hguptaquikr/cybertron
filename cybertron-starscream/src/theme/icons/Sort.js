import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const Sort = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="sort" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Combined-Shape"
        d="M18.9,7.7 C19.2,8.1 19.2,8.8 18.9,9.1 C18.7,9.3 18.4,9.4 18.2,9.4 C18,9.4 17.7,9.3 17.5,9.1 L16.2,7.8 L16.2,19.4 C16.2,20 15.8,20.4 15.2,20.4 C14.6,20.4 14.2,20 14.2,19.4 L14.2,5.4 C14.2,5.3 14.2,5.1 14.3,5 C14.4,4.8 14.6,4.6 14.8,4.5 C15.1,4.4 15.4,4.4 15.6,4.5 C15.7,4.5 15.8,4.6 15.9,4.7 L18.9,7.7 Z M9.2,4.4 C9.7,4.4 10.2,4.9 10.2,5.4 L10.2,19.4 C10.2,19.5 10.2,19.7 10.1,19.8 C10,20 9.8,20.2 9.6,20.3 C9.5,20.4 9.3,20.4 9.2,20.4 C9.1,20.4 8.9,20.4 8.8,20.3 C8.7,20.3 8.6,20.2 8.5,20.1 L5.5,17.1 C5.1,16.7 5.1,16.1 5.5,15.7 C5.9,15.3 6.5,15.3 6.9,15.7 L8.2,17 L8.2,5.4 C8.2,4.8 8.6,4.4 9.2,4.4 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

Sort.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default Sort;
