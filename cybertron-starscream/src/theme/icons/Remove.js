import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const Remove = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="remove" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Combined-Shape"
        d="M12.5,22.4 C7,22.4 2.5,17.9 2.5,12.4 C2.5,6.9 7,2.4 12.5,2.4 C18,2.4 22.5,6.9 22.5,12.4 C22.5,17.9 18,22.4 12.5,22.4 Z M12.5,4.4 C8.1,4.4 4.5,8 4.5,12.4 C4.5,16.8 8.1,20.4 12.5,20.4 C16.9,20.4 20.5,16.8 20.5,12.4 C20.5,8 16.9,4.4 12.5,4.4 Z M16.5,13.4 L8.5,13.4 C7.9,13.4 7.5,13 7.5,12.4 C7.5,11.8 7.9,11.4 8.5,11.4 L16.5,11.4 C17.1,11.4 17.5,11.8 17.5,12.4 C17.5,13 17,13.4 16.5,13.4 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

Remove.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default Remove;
