import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const ArrowDown = ({ color, size }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="arrow-down" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M10.9875,5.1125 C10.9875,4.5125 11.3875,4.0125 11.9875,4.0125 C12.5875,4.0125 12.9875,4.4125 12.9875,5.0125 L12.9875,16.6125 L18.2875,11.3125 C18.6875,10.9125 19.2875,10.9125 19.6875,11.3125 C19.8875,11.5125 19.9875,11.8125 19.9875,12.0125 C19.9875,12.2125 19.8875,12.5125 19.6875,12.7125 L12.6875,19.7125 C12.5875,19.8125 12.4875,19.8125 12.3875,19.9125 C12.0875,20.0125 11.7875,20.0125 11.5875,19.9125 C11.4875,19.9125 11.3875,19.8125 11.2875,19.7125 L4.2875,12.7125 C3.8875,12.3125 3.8875,11.7125 4.2875,11.3125 C4.6875,10.9125 5.2875,10.9125 5.6875,11.3125 L10.9875,16.6125 L10.9875,5.1125 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

ArrowDown.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default ArrowDown;
