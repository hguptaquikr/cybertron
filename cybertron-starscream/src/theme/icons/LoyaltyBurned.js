import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const LoyaltyBurned = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="loyalty-burned" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Combined-Shape"
        d="M5.2,21.7 C4.1,21.7 3.1,20.8 3.1,19.6 L3.1,5.7 C3.1,4.6 4,3.7 5.1,3.7 L8.1,3.7 C8.7,3.7 9.1,4.1 9.1,4.7 C9.1,5.3 8.7,5.7 8.1,5.7 L5.1,5.7 L5.1,19.7 L19.1,19.7 L19.1,16.7 C19.1,16.1 19.5,15.7 20.1,15.7 C20.7,15.7 21.1,16.1 21.1,16.7 L21.1,19.7 C21.1,20.8 20.2,21.7 19.1,21.7 L5.2,21.7 Z M12.9,13.4 C12.5,13.8 11.9,13.8 11.5,13.4 C11.1,13 11.1,12.4 11.5,12 L17.8,5.7 L13.2,5.7 C12.6,5.7 12.2,5.3 12.2,4.7 C12.2,4.1 12.6,3.7 13.2,3.7 L19.2,3.7 C20.3,3.7 21.2,4.6 21.2,5.7 L21.2,11.7 C21.2,12.3 20.8,12.7 20.2,12.7 C19.6,12.7 19.2,12.3 19.2,11.7 L19.2,7.1 L12.9,13.4 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

LoyaltyBurned.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default LoyaltyBurned;
