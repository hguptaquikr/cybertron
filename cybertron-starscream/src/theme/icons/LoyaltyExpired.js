import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const LoyaltyExpired = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="loyalty-expired" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Combined-Shape"
        d="M19.6,13.2 C20.2,13.2 20.6,13.7 20.5,14.1 L20.5,19.1 C20.5,20.2 19.6,21.1 18.5,21.1 L4.6,21.1 C3.5,21.1 2.6,20.2 2.6,19.1 L2.6,5.2 C2.6,4.1 3.5,3.2 4.6,3.2 L9.6,3.2 C10.2,3.2 10.6,3.6 10.6,4.2 C10.6,4.8 10.2,5.2 9.6,5.2 L4.6,5.2 L4.6,19.2 L18.6,19.2 L18.6,14.2 C18.6,13.6 19,13.2 19.6,13.2 Z M19,6.2 L21.3,8.5 C21.7,8.9 21.7,9.5 21.3,9.9 C21.1,10.1 20.8,10.2 20.6,10.2 C20.4,10.2 20.1,10.1 19.9,9.9 L17.6,7.6 L15.3,9.9 C15.1,10.1 14.8,10.2 14.6,10.2 C14.4,10.2 14.1,10.1 13.9,9.9 C13.5,9.5 13.5,8.9 13.9,8.5 L16.2,6.2 L13.9,3.9 C13.5,3.5 13.5,2.9 13.9,2.5 C14.3,2.1 14.9,2.1 15.3,2.5 L17.6,4.8 L19.9,2.5 C20.3,2.1 20.9,2.1 21.3,2.5 C21.7,2.9 21.7,3.5 21.3,3.9 L19,6.2 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

LoyaltyExpired.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default LoyaltyExpired;
