import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const Share = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="share" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M16,15.1 C15.7,15.1 15.3,15.2 15.1,15.3 L10,12.2 C10,12.2 10,12.2 10,12.1 C10,12 10,12.1 10,12 L15.1,8.8 C15.4,8.9 15.7,9 16,9 C17.1,9 18,8.1 18,7 C18,5.9 17.1,5 16,5 C14.9,5 14,5.9 14,7 C14,7 14,7 14,7.1 L8.9,10.3 C8.6,10.2 8.3,10.1 8,10.1 C6.9,10.1 6,11 6,12.1 C6,13.2 6.9,14.1 8,14.1 C8.3,14.1 8.7,14 8.9,13.9 L14,17.1 C14,17.1 14,17.1 14,17.2 C14,18.3 14.9,19.2 16,19.2 C17.1,19.2 18,18.3 18,17.2 C18,16.1 17.1,15.1 16,15.1 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

Share.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default Share;
