import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const BookingDateIcon = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="booking-date-icon" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M16,13 L8,13 C7.4,13 7,12.6 7,12 C7,11.4 7.4,11 8,11 L16,11 C16.6,11 17,11.4 17,12 C17,12.6 16.6,13 16,13 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

BookingDateIcon.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default BookingDateIcon;
