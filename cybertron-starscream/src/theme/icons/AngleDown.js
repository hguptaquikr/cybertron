import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const AngleDown = ({ color, size }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="angle-down" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M12,16 C11.7,16 11.5,15.9 11.3,15.7 L5.3,9.7 C4.9,9.3 4.9,8.7 5.3,8.3 C5.7,7.9 6.3,7.9 6.7,8.3 L12,13.6 L17.3,8.3 C17.7,7.9 18.3,7.9 18.7,8.3 C19.1,8.7 19.1,9.3 18.7,9.7 L12.7,15.7 C12.5,15.9 12.2,16 12,16 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

AngleDown.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default AngleDown;
