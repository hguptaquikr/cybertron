import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const Calendar = ({ size, color }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="calendar" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Combined-Shape"
        d="M19.4,6.9 C20.5,6.9 21.5,7.9 21.5,9 L21.5,19.8 C21.5,21 20.5,21.9 19.4,21.9 L5.6,21.9 C4.4,21.9 3.5,20.9 3.5,19.8 L3.5,9 C3.5,7.8 4.5,6.9 5.6,6.9 L6.5,6.9 L6.5,4.9 C6.5,4.3 6.9,3.9 7.5,3.9 C8.1,3.9 8.5,4.3 8.5,4.9 L8.5,6.9 L16.5,6.9 L16.5,4.9 C16.5,4.3 16.9,3.9 17.5,3.9 C18.1,3.9 18.5,4.3 18.5,4.9 L18.5,6.9 L19.4,6.9 Z M19.5,19.9 L19.5,12 L5.5,12 L5.5,19.9 C5.5,20 5.6,20 5.6,20 L19.5,19.9 C19.4,19.9 19.5,19.9 19.5,19.9 Z M8.5,15.9 C7.9,15.9 7.5,15.5 7.5,14.9 C7.5,14.3 7.9,13.9 8.5,13.9 L11.5,13.9 C12.1,13.9 12.5,14.3 12.5,14.9 C12.5,15.5 12.1,15.9 11.5,15.9 L8.5,15.9 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

Calendar.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default Calendar;
