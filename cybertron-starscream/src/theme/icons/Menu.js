import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const MenuIcon = ({ color, size }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="menu" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Combined-Shape"
        d="M19.2156863,8.46788991 L5.78431373,8.46788991 C5.39215686,8.46788991 5,8.19266055 5,7.73394495 C5,7.27522936 5.29411765,7 5.78431373,7 L19.2156863,7 C19.6078431,7 20,7.27522936 20,7.73394495 C20,8.19266055 19.6078431,8.46788991 19.2156863,8.46788991 Z M15.4901961,12.7798165 L5.78431373,12.7798165 C5.39215686,12.7798165 5,12.5045872 5,12.0458716 C5,11.587156 5.29411765,11.3119266 5.78431373,11.3119266 L15.4901961,11.3119266 C15.8823529,11.3119266 16.2745098,11.587156 16.2745098,12.0458716 C16.2745098,12.5045872 15.8823529,12.7798165 15.4901961,12.7798165 Z M19.2156863,17 L5.78431373,17 C5.39215686,17 5,16.7247706 5,16.266055 C5,15.8073394 5.29411765,15.5321101 5.78431373,15.5321101 L19.2156863,15.5321101 C19.6078431,15.5321101 20,15.8073394 20,16.266055 C20,16.7247706 19.6078431,17 19.2156863,17 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

MenuIcon.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default MenuIcon;
