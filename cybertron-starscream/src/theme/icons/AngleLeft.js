import React from 'react';
import PropTypes from 'prop-types';
import {
  Svg,
  G,
  Path,
  Defs,
} from 'react-native-svg';

const AngleLeft = ({ color, size }) => (
  <Svg height={size} width={size} version="1.1" viewBox={`0 0 ${size} ${size}`}>
    <Defs />
    <G id="angle-left" fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <Path
        id="Shape"
        d="M8,12 C8,11.7 8.1,11.5 8.3,11.3 L14.3,5.3 C14.7,4.9 15.3,4.9 15.7,5.3 C16.1,5.7 16.1,6.3 15.7,6.7 L10.4,12 L15.7,17.3 C16.1,17.7 16.1,18.3 15.7,18.7 C15.3,19.1 14.7,19.1 14.3,18.7 L8.3,12.7 C8.1,12.5 8,12.2 8,12 Z"
        fill={color.replace(/"/g, '')}
        fillRule="nonzero"
      />
    </G>
  </Svg>
);

AngleLeft.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
};

export default AngleLeft;
