import { Platform, NativeModules } from 'react-native';

export function immediateCall() {
  return NativeModules.PhoneCallModule.immediateCall();
}

export const SegmentAnalyticsManager = Platform.OS === 'ios'
  ? NativeModules.SegmentManager
  : NativeModules.SegmentModule;

export function autoReadOTP() {
  return NativeModules.OtpListenerModule.autoReadOtp();
}
