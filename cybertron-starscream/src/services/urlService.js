import moment from 'moment';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import kebabCase from 'lodash/kebabCase';
import startCase from 'lodash/startCase';
import apiService from '../api/apiService';

export default {
  constructUrlQuery({ place, range, rooms, roomType }) {
    return {
      hotel_id: place.hotelId,
      q: place.q,
      landmark: get(place, 'area.landmark'),
      locality: get(place, 'area.locality'),
      city: get(place, 'area.city'),
      state: get(place, 'area.state'),
      ...place.coordinates,
      ...apiService.getDatePickerQuery(range),
      ...apiService.getRoomConfigQuery(rooms),
      ...apiService.getRoomTypeQuery(roomType),
    };
  },

  deconstructUrlQuery(combinedQuery) {
    const search = {};
    search.place = {
      q: combinedQuery.q
      || startCase(combinedQuery.landmark)
      || startCase(combinedQuery.locality)
      || startCase(combinedQuery.city),
      area: {
        landmark: startCase(combinedQuery.landmark),
        locality: startCase(combinedQuery.locality),
        city: startCase(combinedQuery.city),
        state: startCase(combinedQuery.state),
        country: startCase(combinedQuery.country || 'India'),
      },
      coordinates: {
        lat: combinedQuery.lat,
        lng: combinedQuery.lng,
      },
      hotelId: combinedQuery.hotelId || combinedQuery.hotel_id,
    };

    const today = moment();
    let checkin = moment(combinedQuery.checkin);
    let checkout = moment(combinedQuery.checkout);
    checkin = (!isEmpty(combinedQuery.checkin) && checkin.diff(today, 'days') >= 0) ? checkin : today;
    checkout = (!isEmpty(combinedQuery.checkout) && checkout.diff(checkin, 'days') >= 1)
      ? checkout : moment(checkin).add(1, 'days');
    search.range = {
      start: checkin,
      end: checkout,
    };

    if (combinedQuery.roomconfig) {
      search.rooms = combinedQuery.roomconfig
        .split(',')
        .map((room) => {
          const [adults, kids] = room.split('-');
          return {
            adults: Number(adults),
            kids: Number(kids),
          };
        });
    }

    search.roomType = combinedQuery.roomtype;

    return search;
  },

  getHotelDetailsUrl(hotel, roomType, location) {
    const city = kebabCase(hotel.address.city);
    const hotelSlug = kebabCase(`${hotel.name} ${get(hotel, 'address.locality', '')}`);
    return {
      pathname: `/hotels-in-${city}/${hotelSlug}-${hotel.id}/`,
      query: {
        ...location.query,
        q: `${hotel.name}, ${get(hotel, 'address.locality', '')}`,
        hotel_id: hotel.id,
        roomtype: roomType && roomType.toLowerCase(),
      },
    };
  },

  getCheckoutUrl(search, roomType, hotel) {
    const { searchInput, datePicker, roomConfig } = search;
    return {
      pathname: '/itinerary/',
      query: this.constructUrlQuery({
        place: {
          ...searchInput.place,
          q: hotel ? `${hotel.name}, ${get(hotel, 'address.locality', '')}` : searchInput.place.q,
          hotelId: searchInput.place.hotelId || hotel.id,
        },
        range: datePicker.range,
        rooms: roomConfig.rooms,
        roomType,
      }),
    };
  },
};
