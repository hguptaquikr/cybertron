/**
 * @overview A minimalistic wrapper around React Native's AsyncStorage.
 * @license MIT
 */
import merge from 'lodash/merge';
import { AsyncStorage } from 'react-native';

export default {
  /**
   * Get a one or more value for a key or array of keys from AsyncStorage
   * @param {String|Array} key A key or array of keys
   * @return {Promise}
   */
  get(key) {
    if (!Array.isArray(key)) {
      try {
        return AsyncStorage.getItem(key).then((value) => JSON.parse(value));
      } catch (error) {
        return Promise.reject(error);
      }
    }
    try {
      return AsyncStorage.multiGet(key).then((values) => (
        // return value[1] because both key and value are returned as array.
        values.map((value) => JSON.parse(value[1]))
      ));
    } catch (error) {
      return Promise.reject(error);
    }
  },

  /**
   * Save a key value pair or a series of key value pairs to AsyncStorage.
   * @param  {String|Array} key The key or an array of key/value pairs
   * @param  {Any} value The value to save
   * @return {Promise}
   */
  save(key, value) {
    if (!Array.isArray(key)) {
      try {
        return AsyncStorage.setItem(key, JSON.stringify(value));
      } catch (error) {
        return Promise.reject(error);
      }
    } else {
      try {
        const pairs = key.map((pair) => [pair[0], JSON.stringify(pair[1])]);
        return AsyncStorage.multiSet(pairs);
      } catch (error) {
        return Promise.reject(error);
      }
    }
  },

  /**
   * Updates the value in the store for a given key in AsyncStorage.
   * If the value is a string it will be replaced.
   * If the value is an object it will be deep merged.
   * @param  {String} key The key
   * @param  {Value} value The value to update with
   * @return {Promise}
   */
  update(key, value) {
    return this.get(key).then((item) => {
      const val = typeof value === 'string' ? value : merge({}, item, value);
      return this.save(key, JSON.stringify(val));
    });
  },

  /**
   * Delete the value for a given key in AsyncStorage.
   * @param  {String|Array} key The key or an array of keys to be deleted
   * @return {Promise}
   */
  delete(key) {
    if (Array.isArray(key)) {
      return AsyncStorage.multiRemove(key);
    }
    return AsyncStorage.removeItem(key);
  },

  /**
   * Erases all AsyncStorage for all clients, libraries, etc.
   * You probably don't want to call this; use removeItem or multiRemove
   * @return {Promise}
   */
  clear() {
    return AsyncStorage.clear();
  },

  /**
   * Get all keys in AsyncStorage.
   * @return {Promise} A promise which when it resolves gets passed the saved keys in AsyncStorage.
   */
  keys() {
    return AsyncStorage.getAllKeys();
  },

  /**
   * Push a value onto an array stored in AsyncStorage by key
   * or create a new array in AsyncStorage for a key if it's not yet defined.
   * @param {String} key They key
   * @param {Any} value The value to push onto the array
   * @return {Promise}
   */
  push(key, value) {
    return this.get(key).then((currentValue) => {
      if (currentValue === null) {
        // if there is no current value populate it with the new value
        return this.save(key, [value]);
      }
      if (Array.isArray(currentValue)) {
        return this.save(key, [...currentValue, value]);
      }
      return Promise.reject(
        `Existing value for key "${key}" must be of type null or Array, received ${typeof currentValue}.`,
      );
    });
  },
};
