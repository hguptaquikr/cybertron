export const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const mobileRegex = /^[0]?[789]\d{9}$/;

export const otpRegex = /\d{6}/;

export const gstnRegex = /^[\d]{2}[A-Z]{5}[\d]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/;

export const isValidMobile = (mobile) => {
  const re = /^[0]?[789]\d{9}$/;
  return re.test(mobile);
};

export const isValidOtp = (otp) => (
  otp && !Number.isNaN(otp) && otp.length === 6
);

export const isValidEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

export const isValidPassword = (password) => (
  password && password.length > 0
);

export const isValidName = (name) => (
  name && name.length > 0
);

export const isValidGSTNumber = (gstNo) => {
  const regEx = /^[\d]{2}[A-Z]{5}[\d]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/;
  return regEx.test(gstNo);
};

export const isValidField = (value) => (validation, errorMessage, required = false) => {
  if (required && !(value && value.length > 0)) {
    return errorMessage;
  }
  switch (validation) {
    case 'isEmail':
      if (isValidEmail(value)) {
        return null;
      }
      return errorMessage;

    case 'isMobileNumber':
      if (isValidMobile(value)) {
        return null;
      }
      return errorMessage;

    case 'isOtp':
      if (isValidOtp(value)) {
        return null;
      }
      return errorMessage;

    case 'isPassword':
      if (isValidPassword(value)) {
        return null;
      }
      return errorMessage;

    case 'isName':
      if (isValidName(value)) {
        return null;
      }
      return errorMessage;

    case 'isGSTNumber':
      if (isValidGSTNumber(value)) {
        return null;
      }
      return errorMessage;

    default:
      break;
  }
  return null;
};
