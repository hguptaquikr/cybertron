import React, { Component } from 'react';
import { Platform, Linking } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import get from 'lodash/get';
import { TextInput, Form, Button } from 'leaf-ui/native';
import * as rateAndReviewActionCreators from '../rateAndReviewDuck';
import * as contentActionCreators from '../../content/contentDuck';
import Modal from '../../components/Modal/Modal';
import Text from '../../components/NewLeaf/Text/Text';
import Rating from '../../components/Rating/Rating';
import TouchableComponent from '../../components/Touchable/TouchableComponent';
import Content from '../../components/Content/Content';
import color from '../../theme/color';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import analyticsService from '../../analytics/analyticsService';
import storeHouse from '../../services/storeHouse';
import config from '../../config';

const View = styled.View``;

const TransparentContainer = styled.View`
  backgroundColor: ${color.blackOpacity40};
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

const ClickableContainer = styled.TouchableOpacity`
  flex: 1;
`;

const MiniSheetContainer = styled.View`
  padding: 8px;
  borderTopWidth: 1;
  borderTopColor: ${color.lightGrey};
  backgroundColor: ${color.white};
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  zIndex: 10;
`;

const MiniSheetHeader = styled.View`
  flexDirection: row;
  margin: 4px;
  alignItems: center;
`;

const RatingContainer = styled.View`
  marginTop: 16;
  marginHorizontal: 8;
  marginBottom: 24;
`;

const Touchable = styled.TouchableWithoutFeedback`
  justifyContent: center;
  alignItems: center;
  padding: 4px;
  width: 32;
  height: 32;
`;

const ModalContentContainer = styled.View`
  paddingHorizontal: 8;
  paddingTop: 8;
  flex: 1;
`;

const ModalContentSeparator = styled.View`
  width: auto;
  height: 1;
  backgroundColor: ${color.lightGrey};
  margin: 8px;
`;

const FlexContainer = styled.View`
  flex: 1;
`;

const ReasonsContainer = styled.View`
  flexDirection: row;
  flexWrap: wrap;
  paddingHorizontal: 8;
  paddingBottom: 8;
  justifyContent: space-around;
`;

const OtherReasonContainer = styled.View`
  paddingHorizontal: 16;
  marginBottom: 20;
`;

const ButtonContainer = styled.View`
  marginBottom: 8;
`;

const ReasonView = styled.View`
  paddingHorizontal: 16;
  paddingVertical: 4;
  backgroundColor: ${({ isSelected }) => (isSelected ? color.charcoalGrey : color.white)}
  borderRadius: 2;
  borderColor: ${color.warmGrey}
  borderWidth: 1;
  justifyContent: center;
  alignItems: center;
`;

class InAppFeedback extends Component {
  state = {
    rating: 0,
    reason: '',
  };

  componentDidMount() {
    const {
      contentActions: { getFeedbackContent },
      rateAndReviewActions: { setRatingMiniSheetVisible },
    } = this.props;

    getFeedbackContent();
    storeHouse.get(['latest_hotel_booked', 'latest_booking_date', 'latest_booking', 'is_feedback_submitted'])
      .then((values) => {
        const hotel = values[0];
        const latestBookingDate = values[1];
        const latestBooking = values[2];
        const isFeedbackSubmitted = values[3];
        if (hotel && !isFeedbackSubmitted) {
          setTimeout(
            () => setRatingMiniSheetVisible(true, hotel, latestBookingDate, latestBooking),
            2000,
          );
        }
      }).catch(() => false);
  }

  onRatingChange = (rating) => {
    const {
      isRatingMiniSheetVisible,
      rateAndReviewActions: { setRatingMiniSheetVisible, setRatingModalVisible },
      latestHotelBooked,
      latestBookingDate,
      latestBooking,
    } = this.props;
    if (isRatingMiniSheetVisible) {
      setRatingMiniSheetVisible(false, latestHotelBooked, latestBookingDate, latestBooking);
      setRatingModalVisible(true, latestHotelBooked, latestBookingDate, latestBooking);
    }
    this.setState({ rating });
  };

  getRatingView = () => {
    const {
      rateAndReview: { message, maxRating },
      latestHotelBooked,
      latestBookingDate,
    } = this.props;
    const { rating } = this.state;
    let hotelName = 'your latest booking';
    if (latestHotelBooked) {
      hotelName = latestHotelBooked.name;
      const city = get(latestHotelBooked, 'address.city', '');
      if (city) {
        hotelName = `${hotelName}, ${city}`;
      }
    }
    return (
      <View>
        <Text
          size="s"
          weight="medium"
          color="greyDarker"
        >
          {`${message} ${hotelName} ?`}
        </Text>
        {
          latestBookingDate ? (
            <Text
              size="xs"
              weight="normal"
              color="greyDark"
            >
              {`Booked on ${moment(latestBookingDate).date()}th ${moment(latestBookingDate).format('MMMM')}`}
            </Text>
          ) : null
        }
        <RatingContainer>
          <Rating
            rating={rating}
            maxRating={maxRating}
            onRatingChange={this.onRatingChange}
          />
        </RatingContainer>
      </View>
    );
  };

  isGoodRating = () => {
    const { rateAndReview: { thresholdForReasons } } = this.props;
    const { rating } = this.state;
    return rating > thresholdForReasons;
  };

  closeMiniSheet = () => {
    const { rateAndReviewActions: { setRatingMiniSheetVisible } } = this.props;
    storeHouse.save('is_feedback_submitted', true);
    setRatingMiniSheetVisible(false);
  };

  postFeedback = (otherReason) => {
    const { rating, reason } = this.state;
    const {
      latestBooking,
      rateAndReviewActions: { setRatingMiniSheetVisible, setRatingModalVisible },
    } = this.props;
    const orderId = get(latestBooking, 'payment.orderId', '');
    if (this.isGoodRating()) {
      analyticsService.segmentTrackEvent('Feedback Submitted', { rating, order_id: orderId });
    } else {
      analyticsService.segmentTrackEvent(
        'Feedback Submitted',
        {
          rating,
          reason,
          other_reason: otherReason,
          order_id: orderId,
        },
      );
    }
    storeHouse.save('is_feedback_submitted', true);
    setRatingMiniSheetVisible(false);
    setRatingModalVisible(false);
  };

  openAppStore = () => {
    const { rateAndReviewActions: { setRatingModalVisible } } = this.props;
    const url = config.storeLink;
    if (url) {
      Linking.canOpenURL(url).then((supported) => {
        if (supported) {
          Linking.openURL(url).catch(() => null);
        }
      });
    }
    setRatingModalVisible(false);
  };

  render() {
    const {
      rateAndReview: {
        heading,
        goodRatingMessageAndroid,
        goodRatingMessageIos,
        reasonsTitle,
        reasons,
      },
      rateAndReviewActions,
      isRatingMiniSheetVisible,
      isRatingModalVisible,
    } = this.props;
    const { reason } = this.state;
    const isGoodRating = this.isGoodRating();

    if (isRatingMiniSheetVisible) {
      return (
        <TransparentContainer>
          <ClickableContainer onPress={this.closeMiniSheet} />
          <MiniSheetContainer>
            <MiniSheetHeader>
              <Touchable onPress={this.closeMiniSheet}>
                {getIcon('close')}
              </Touchable>
              <Text
                size="m"
                weight="medium"
                color="greyDarker"
              >
                {heading}
              </Text>
            </MiniSheetHeader>
            {this.getRatingView()}
          </MiniSheetContainer>
        </TransparentContainer>
      );
    }
    if (isRatingModalVisible) {
      return (
        <Modal
          icon="cross"
          header={heading}
          isOpen={isRatingModalVisible}
          onClose={() => {
            storeHouse.save('is_feedback_submitted', true);
            rateAndReviewActions.setRatingModalVisible(false);
          }}
          iosBarStyle="dark-content"
        >
          <Form
            onSubmit={(values, actions) => {
              actions.setSubmitting(true);
              this.postFeedback(values.otherReason);
              actions.setSubmitting(false);
            }}
            render={(formProps) => (
              <ModalContentContainer>
                <Content>
                  {this.getRatingView()}
                  <ModalContentSeparator />
                  {
                    isGoodRating ? (
                      <FlexContainer>
                        <Text
                          size="s"
                          color="greyDarker"
                          weight="normal"
                        >
                          {Platform.OS === 'android' ? goodRatingMessageAndroid : goodRatingMessageIos}
                        </Text>
                        <ReasonsContainer />
                      </FlexContainer>
                    ) : (
                      <FlexContainer>
                        <Text
                          size="s"
                          color="greyDarker"
                          weight="normal"
                        >
                          {reasonsTitle}
                        </Text>
                        <ReasonsContainer>
                          {
                              reasons.map((value) => (
                                <TouchableComponent
                                  key={value}
                                  onPress={() => this.setState({ reason: value })}
                                >
                                  <ReasonView isSelected={value === reason}>
                                    <Text
                                      size="xs"
                                      weight="normal"
                                      color={value === reason ? 'white' : 'greyDark'}
                                    >
                                      {value}
                                    </Text>
                                  </ReasonView>
                                </TouchableComponent>
                              ))
                            }
                        </ReasonsContainer>
                        <OtherReasonContainer>
                          <TextInput
                            name="otherReason"
                            label="Other reason"
                            autoCapitalize="sentences"
                            autoCorrect={false}
                            showPlaceholder
                          />
                        </OtherReasonContainer>
                      </FlexContainer>
                      )
                  }
                </Content>
                {
                  isGoodRating ? (
                    <View>
                      <ButtonContainer>
                        <Button
                          block
                          kind="outlined"
                          style={{
                            borderColor: color.primary,
                            borderRadius: 2,
                          }}
                          onPress={formProps.handleSubmit}
                        >
                          <Text
                            size="s"
                            weight="medium"
                            color="primary"
                          >
                            DONE
                          </Text>
                        </Button>
                      </ButtonContainer>
                      <ButtonContainer>
                        <Button
                          block
                          onPress={this.openAppStore}
                        >
                          <Text
                            size="s"
                            weight="medium"
                            color="white"
                          >
                            {`RATE US ON ${Platform.OS === 'android' ? 'PLAY' : 'APP'} STORE`}
                          </Text>
                        </Button>
                      </ButtonContainer>
                    </View>
                  ) : (
                    <ButtonContainer>
                      <Button
                        block
                        onPress={formProps.handleSubmit}
                      >
                        <Text
                          size="m"
                          weight="medium"
                          color="white"
                        >
                          DONE
                        </Text>
                      </Button>
                    </ButtonContainer>
                    )
                }
              </ModalContentContainer>
            )}
          />
        </Modal>
      );
    }
    return null;
  }
}

InAppFeedback.propTypes = {
  rateAndReview: PropTypes.object.isRequired,
  rateAndReviewActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  isRatingMiniSheetVisible: PropTypes.bool.isRequired,
  isRatingModalVisible: PropTypes.bool.isRequired,
  latestHotelBooked: PropTypes.object,
  latestBookingDate: PropTypes.string,
  latestBooking: PropTypes.object,
};

const mapStateToProps = (state) => ({
  rateAndReview: state.content.rateAndReview,
  isRatingMiniSheetVisible: state.rateAndReview.isRatingMiniSheetVisible,
  isRatingModalVisible: state.rateAndReview.isRatingModalVisible,
  latestHotelBooked: state.rateAndReview.latestHotelBooked,
  latestBookingDate: state.rateAndReview.latestBookingDate,
  latestBooking: state.rateAndReview.latestBooking,
});

const mapDispatchToProps = (dispatch) => ({
  rateAndReviewActions: bindActionCreators(rateAndReviewActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(InAppFeedback);
