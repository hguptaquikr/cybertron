const SET_RATING_MINI_SHEET_VISIBILITY = 'SET_RATING_MINI_SHEET_VISIBILITY';
const SET_RATING_MODAL_VISIBILITY = 'SET_RATING_MODAL_VISIBILITY';

const initialState = {
  isRatingMiniSheetVisible: false,
  isRatingModalVisible: false,
  latestHotelBooked: null,
  latestBookingDate: '',
  latestBooking: null,
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_RATING_MINI_SHEET_VISIBILITY:
      return {
        ...state,
        ...payload,
      };

    case SET_RATING_MODAL_VISIBILITY:
      return {
        ...state,
        ...payload,
        isRatingMiniSheetVisible: false,
      };

    default:
      return state;
  }
};

export const setRatingMiniSheetVisible = (
  visible,
  latestHotelBooked,
  latestBookingDate,
  latestBooking,
) => ({
  type: SET_RATING_MINI_SHEET_VISIBILITY,
  payload: {
    isRatingMiniSheetVisible: visible,
    latestHotelBooked,
    latestBookingDate,
    latestBooking,
  },
});

export const setRatingModalVisible = (
  visible,
  latestHotelBooked,
  latestBookingDate,
  latestBooking,
) => ({
  type: SET_RATING_MODAL_VISIBILITY,
  payload: {
    isRatingModalVisible: visible,
    latestHotelBooked,
    latestBookingDate,
    latestBooking,
  },
});
