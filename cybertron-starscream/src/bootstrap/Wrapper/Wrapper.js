/* eslint-disable no-undef */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BackHandler, Linking, Platform } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-native';
import queryString from 'query-string';
import { ThemeProvider } from 'styled-components/native';
import { theme as leafUiTheme } from 'leaf-ui/native';
import * as searchActionCreators from '../../search/searchDuck';
import * as uiActionsCreators from '../../search/uiDuck';
import Container from '../../components/Container/Container';
import Toast from '../../components/Toast/Toast';
import NoInternetConnection from '../../components/NoInternetConnection/NoInternetConnection';
import InAppFeedback from '../../feedback/InAppFeedback/InAppFeedback';
import googleService from '../../google/googleService';
import { fontFamilies } from '../../components/NewLeaf/Text/Text';

leafUiTheme.fontFamily = fontFamilies;

class Wrapper extends Component {
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
  }

  componentDidMount() {
    googleService.initHandlers();
    Linking.getInitialURL().then(this.handleLinkingURL);
    Linking.addEventListener('url', (event) => this.handleLinkingURL(event.url));
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
    Linking.removeEventListener('url', (event) => this.handleLinkingURL(event.url));
  }

  handleLinkingURL = (url) => {
    const { uiActions } = this.props;
    let pathname;
    let query = {};

    if (!url) {
      return;
    }

    if (Platform.OS === 'android') {
      const regExp = /(http[s]?:\/\/)?(www.)?treebo.com(.*)/;
      const match = regExp.exec(url);
      if (match) {
        const parts = match[3].split('?');
        [pathname] = parts;
        if (parts.length > 1) {
          query = queryString.parse(parts[1]);
        }
      }
    } else {
      const regExp = /treebo:\/\/(.*)/;
      const match = regExp.exec(url);
      if (match) {
        const parts = match[1].split('?');
        [pathname] = parts;
        if (parts.length > 1) {
          query = queryString.parse(parts[1]);
        }
      }
    }
    uiActions.setSearchParameters(query);
    if (pathname) {
      const { history, searchActions } = this.props;
      searchActions.setSearchState(query);
      history.push({
        pathname,
        query: {
          ...query,
        },
      });
    }
  };

  handleBack = () => {
    const { history } = this.props;
    if (history.index === 0) {
      return false; // home screen
    }
    history.goBack();
    return true;
  };

  render() {
    const { children } = this.props;
    return (
      <ThemeProvider theme={leafUiTheme}>
        <Container>
          {children}
          <Toast />
          <NoInternetConnection />
          <InAppFeedback />
        </Container>
      </ThemeProvider>
    );
  }
}

Wrapper.propTypes = {
  children: PropTypes.element.isRequired,
  history: PropTypes.object,
  searchActions: PropTypes.object,
  uiActions: PropTypes.object,

};

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  uiActions: bindActionCreators(uiActionsCreators, dispatch),
});

export default withRouter(connect(null, mapDispatchToProps)(Wrapper));
