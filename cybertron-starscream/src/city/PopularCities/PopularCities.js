import React, { Component } from 'react';
import { ImageBackground, InteractionManager } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import * as searchActionCreators from '../../search/searchDuck';
import List from '../../components/List/List';
import PopularCity from '../../components/PopularCity/PopularCity';
import LandingPageLayout from '../../home/LandingPage/LandingPageLayout';
import Text from '../../components/NewLeaf/Text/Text';
import analyticsService from '../../analytics/analyticsService';

const NearbyImage = require('../../search/NearbyImage.png');
const AllCitiesImage = require('../popular_all_cities.png');

const CitiesList = styled(List)`
  paddingLeft: 16;
  paddingTop: 24;
`;

const CityContainer = styled.View`
  marginRight: 24;
`;

const OverlayView = styled.View`
  width: 72;
  height: 72;
  borderRadius: 36;
  backgroundColor: 'rgba(0, 0, 0, 0.6)';
  justifyContent: center;
  alignItems: center;
`;

const TouchableContainer = styled.TouchableOpacity`
  marginRight: 20;
  marginBottom: 20;
`;

const ViewAllCityImage = styled(ImageBackground)`
  width: 72;
  aspectRatio: 1;
  borderRadius: 36;
`;

const NearbyImageContainer = styled(ImageBackground)`
  width: 72;
  aspectRatio: 1;
  borderRadius: 36;
`;

const NearbyText = styled(Text)`
  marginTop: 12;
  alignSelf: center;
`;

class PopularCities extends Component {
  onPress = (city, page) => {
    const { searchActions, history } = this.props;
    const analyticsProps = {
      name: city.name.toLowerCase(),
      page,
    };
    const place = {
      q: city.name,
      area: {
        city: city.name,
      },
    };
    analyticsService.segmentTrackEvent('City Clicked', analyticsProps);
    searchActions.searchLocationChange(place);
    history.push(`/hotels-in-${city.slug}/`);
  };

  onNearMeClicked = () => {
    const { searchActions, history } = this.props;
    const query = {
      q: 'Near Me',
      type: 'near-me',
    };

    searchActions.searchLocationChange(query);
    analyticsService.segmentTrackEvent('Near Me Clicked', { source: 'Landing Page Icon' });
    history.push('/search/');
  }

  onViewAllClicked = () => {
    const { history } = this.props;
    InteractionManager.runAfterInteractions(() => {
      analyticsService.segmentTrackEvent('View All Cities Clicked');
      history.push('/cities/');
    });
  }

  getDataSet = () => {
    const { cities } = this.props;
    return [
      { nearme: true, name: 'Near Me' },
      ...cities,
      { viewAll: true, name: 'View All' },
    ];
  };

  renderCity = (data) => {
    const city = data.item;
    if (city.viewAll) {
      return (
        <TouchableContainer
          key={city.name}
          onPress={this.onViewAllClicked}
          testID="view-all-cities-button"
          accessibilityLabel="view-all-cities-button"
        >
          <ViewAllCityImage
            source={AllCitiesImage}
          >
            <OverlayView>
              <Text size="s" weight="normal" color="white">
                View All
              </Text>
            </OverlayView>
          </ViewAllCityImage>
        </TouchableContainer>
      );
    } else if (city.nearme) {
      return (
        <TouchableContainer
          key={city.name}
          onPress={this.onNearMeClicked}
        >
          <NearbyImageContainer source={NearbyImage} />
          <NearbyText size="s" weight="medium" >
            Near Me
          </NearbyText>
        </TouchableContainer>
      );
    }
    return (
      <CityContainer key={city.name}>
        <PopularCity
          city={city}
          page="home"
          onPress={this.onPress}
        />
      </CityContainer>
    );
  };

  render() {
    return (
      <LandingPageLayout.Section>
        <LandingPageLayout.SectionTitle>
          <Text size="l" weight="bold" color="greyDarker">Quick Search</Text>
        </LandingPageLayout.SectionTitle>
        <CitiesList
          data={this.getDataSet()}
          keyExtractor={(item) => item.name || item.image}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={this.renderCity}
          testID="popular-cities-list"
          accessibilityLabel="popular-cities-list"
        />
      </LandingPageLayout.Section>
    );
  }
}

PopularCities.propTypes = {
  cities: PropTypes.array.isRequired,
  history: PropTypes.object.isRequired,
  searchActions: PropTypes.object.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
});

export default withRouter(connect(null, mapDispatchToProps)(PopularCities));
