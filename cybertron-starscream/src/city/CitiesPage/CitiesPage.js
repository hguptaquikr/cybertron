import React, { Component } from 'react';
import { Platform, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-native';
import styled from 'styled-components/native';
import { Spacer } from 'leaf-ui/native';
import * as searchActionCreators from '../../search/searchDuck';
import BackButtonHeader from '../../components/Header/BackButtonHeader';
import Content from '../../components/Content/Content';
import Container from '../../components/Container/Container';
import List from '../../components/List/List';
import PopularCity from '../../components/PopularCity/PopularCity';
import Text from '../../components/NewLeaf/Text/Text';
import color from '../../theme/color';
import { pluralize } from '../../utils/utils';
import analyticsService from '../../analytics/analyticsService';

const CitiesContainer = styled.View`
  paddingVertical: 24;
  paddingHorizontal: 16;
`;

const PopularCitiesRow = styled.View`
  flexDirection: row;
  flex: 1;
  justifyContent: space-between;
  marginBottom: 24;
`;

const Separator = styled.View`
  height: 1;
  background: ${color.tertiary};
`;

const TouchableContainer = styled.TouchableOpacity`
  paddingTop: 20;
  paddingBottom: 20;
  flexDirection: row;
  justifyContent: space-between;
  alignItems: center;
`;

class CitiesPage extends Component {
  componentDidMount() {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('dark-content');
    } else {
      StatusBar.setBackgroundColor(color.black);
    }
    analyticsService.segmentTrackEvent('View All Cities Clicked');
  }

  onPress(city) {
    const { history, searchActions } = this.props;
    const { name, slug } = city;
    const props = {
      name: name.toLowerCase(),
      page: 'city page',
    };
    const place = {
      q: name,
      area: {
        city: name,
      },
    };
    analyticsService.segmentTrackEvent('City Clicked', props);
    searchActions.searchLocationChange(place);
    history.push(`/hotels-in-${slug}/`);
  }

  renderAllCities = () => {
    const { history, content: { cities }, searchActions } = this.props;
    return (
      <List
        data={cities}
        showsHorizontalScrollIndicator={false}
        itemSeparatorComponent={Separator}
        keyExtractor={(item) => item.name}
        renderItem={(data) => {
          const { name, hotelCount, slug } = data.item;
          return (
            <TouchableContainer
              key={name}
              onPress={() => {
                const props = {
                  name: name.toLowerCase(),
                  page: 'city page',
                };
                const place = {
                  q: name,
                  area: {
                    city: name,
                  },
                };
                analyticsService.segmentTrackEvent('City Clicked', props);
                searchActions.searchLocationChange(place);
                history.push(`/hotels-in-${slug}/`);
              }}
              testID="city-btn"
              accessibilityLabel="city-btn"
            >
              <Text size="m" color="greyDarker" weight="normal">{name}</Text>
              {
                hotelCount ? (
                  <Text size="s" color="greyDarker" weight="normal">
                    {pluralize(hotelCount, 'hotel', null, true)}
                  </Text>
                ) : null
              }
            </TouchableContainer>
          );
        }}
      />
    );
  };

  renderPopularCities = (cities) => (
    cities.map((city) => (
      <PopularCity
        key={city.name}
        city={city}
        hotelCount={city.noOfHotels}
        small
        page="city page"
        onPress={() => this.onPress(city)}
      />
    ))
  );

  render() {
    const { content: { popularCities } } = this.props;

    return (
      <Container>
        <BackButtonHeader />
        <Content>
          <CitiesContainer>
            <Spacer padding={[0, 0, 2, 0]}>
              <Text color="greyDarker" size="l" weight="bold">
                Quick Search
              </Text>
            </Spacer>
            <PopularCitiesRow>
              {this.renderPopularCities(popularCities.slice(0, 4))}
            </PopularCitiesRow>
            <PopularCitiesRow>
              {this.renderPopularCities(popularCities.slice(4))}
            </PopularCitiesRow>
            <Text color="greyDarker" size="l" weight="bold">
              Other Cities
            </Text>
            {this.renderAllCities()}
          </CitiesContainer>
        </Content>
      </Container>
    );
  }
}

CitiesPage.propTypes = {
  content: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  searchActions: PropTypes.object,
};

const mapStateToProps = (state) => ({
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  searchActions: bindActionCreators(searchActionCreators, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CitiesPage));
