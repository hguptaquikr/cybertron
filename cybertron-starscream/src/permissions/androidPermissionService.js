import { PermissionsAndroid } from 'react-native';
import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';
import analyticsService from '../analytics/analyticsService';

const PERMISSION_GRANTED = 'granted';
export const requestSmsPermission = async () => {
  try {
    const permissions = [
      PermissionsAndroid.PERMISSIONS.RECEIVE_SMS,
      PermissionsAndroid.PERMISSIONS.READ_SMS,
    ];
    const results = await PermissionsAndroid.requestMultiple(permissions);
    return results[permissions[0]] === PermissionsAndroid.RESULTS.GRANTED
      && results[permissions[1]] === PermissionsAndroid.RESULTS.GRANTED;
  } catch (err) {
    return false;
  }
};

export const requestLocationPermission = async () => {
  try {
    const accessFineLocation = PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION;
    let isPermissionGranted = await PermissionsAndroid.check(accessFineLocation);
    if (!isPermissionGranted) {
      analyticsService.segmentTrackEvent('Location Permission Requested');
      isPermissionGranted = await PermissionsAndroid.request(accessFineLocation);
    }
    if (isPermissionGranted === true && PermissionsAndroid.RESULTS.GRANTED === 'granted') {
      return true;
    }
    return isPermissionGranted === PermissionsAndroid.RESULTS.GRANTED;
  } catch (err) {
    return false;
  }
};

export const checkAndroidLocationServices = async () => {
  try {
    const { enabled, alreadyEnabled } = await LocationServicesDialogBox
      .checkLocationServicesIsEnabled({
        message: 'Location services seem to be disabled. You need to enable them from your settings.',
        ok: 'Go to settings',
        cancel: 'Cancel',
        enableHighAccuracy: false,
        showDialog: true,
        openLocationServices: true,
        preventOutSideTouch: true,
        preventBackClick: true,
      });

    if (!alreadyEnabled) {
      analyticsService.segmentTrackEvent('Location Services Prompted');
      if (enabled) {
        analyticsService.segmentTrackEvent('Location Services Accepted');
      } else {
        analyticsService.segmentTrackEvent('Location Services Rejected');
      }
    }

    return enabled;
  } catch (err) {
    return false;
  }
};

export const checkAndRequestCallPermission = async () => {
  try {
    const callPermission = PermissionsAndroid.PERMISSIONS.CALL_PHONE;
    let isPermissionGranted = await PermissionsAndroid.check(callPermission);
    if (isPermissionGranted) {
      analyticsService.segmentTrackEvent('Direct Call Permission Already Granted');
      return true;
    }
    isPermissionGranted = await PermissionsAndroid.request(callPermission);
    if (isPermissionGranted === PERMISSION_GRANTED) {
      analyticsService.segmentTrackEvent('Direct Call Permission Granted');
      return true;
    }
    analyticsService.segmentTrackEvent('Direct Call Permission Rejected');
    return false;
  } catch (err) {
    return false;
  }
};
