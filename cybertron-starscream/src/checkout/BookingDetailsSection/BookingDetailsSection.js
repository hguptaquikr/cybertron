import React from 'react';
import PropTypes from 'prop-types';
import Review from '../Review/Review';

const BookingDetailsSection = (
  {
    hotel,
    price,
    search,
    ratePlan,
    searchActions,
  },
) => (
  <Review
    hotel={hotel}
    price={price}
    search={search}
    ratePlan={ratePlan}
    searchActions={searchActions}
    isDisabled
  />
);

BookingDetailsSection.propTypes = {
  hotel: PropTypes.object,
  price: PropTypes.object,
  search: PropTypes.object,
  ratePlan: PropTypes.string,
  searchActions: PropTypes.object,
};

export default BookingDetailsSection;
