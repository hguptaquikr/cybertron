import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import Text from '../../components/NewLeaf/Text/Text';
import Price from '../../components/NewLeaf/Price/Price';
import color from '../../theme/color';
import RatePlanTypeView from '../../price/RatePlanTypeView/RatePlanTypeView';

const CardView = styled.View`
  padding: 16px;
  backgroundColor: ${color.white};
`;

const TotalPriceContainer = styled.View`
  alignItems: flex-end;
`;

const View = styled.View``;

const Item = styled.View`
  flexDirection: row;
  paddingTop: 5px;
  paddingBottom: 5px;
  justifyContent: space-between;
  alignItems: center;
  flex: 1;
`;

const PriceDetailsCard = (
  {
    basePrice,
    totalPayable,
    baseDiscount,
    memberPriceDiscount,
    couponDiscountAmount,
    taxes,
    isNRP,
    isPartPay,
  },
) => (
  <CardView>
    <Item>
      <Text size="s" weight="normal" color="greyDarker">
          Room Tariff
      </Text>
      <Price price={basePrice} color="greyDarker" size="s" type="regular" />
    </Item>
    {
        baseDiscount > 0 ? (
          <Item>
            <Text size="s" weight="normal" color="greyDarker">
              Room Discount
            </Text>
            <Price price={baseDiscount} size="s" color="green" type="regular" negative />
          </Item>
        ) : null
      }
    {
        memberPriceDiscount > 0 ? (
          <Item>
            <Text size="s" weight="normal" color="greyDarker">
              Member Discount
            </Text>
            <Price price={memberPriceDiscount} size="s" color="green" type="regular" negative />
          </Item>
        ) : null
      }
    {
        couponDiscountAmount ? (
          <Item>
            <Text size="s" weight="normal" color="greyDarker">
              Coupon Discount
            </Text>
            <Price price={couponDiscountAmount} size="s" color="green" type="regular" negative />
          </Item>
        ) : null
      }
    {
        taxes ? (
          <Item>
            <Text size="s" weight="normal" color="greyDarker">
              Taxes
            </Text>
            <Price price={taxes} size="s" color="greyDarker" type="regular" />
          </Item>
        ) : null
      }
    <Item>
      <View>
        <Text size="s" weight="normal" color="greyDarker">
          {isPartPay ? 'Total Payable' : 'Total Paid'}
        </Text>
        <Text size="xs" weight="normal" color="grey">
            inc. of all taxes
        </Text>
      </View>
      <TotalPriceContainer>
        <Price price={totalPayable} size="l" color="greyDarker" type="regular" bold />
        <RatePlanTypeView isNRP={isNRP} />
      </TotalPriceContainer>
    </Item>
  </CardView>
);

PriceDetailsCard.propTypes = {
  basePrice: PropTypes.number,
  totalPayable: PropTypes.number,
  baseDiscount: PropTypes.number,
  memberPriceDiscount: PropTypes.number,
  couponDiscountAmount: PropTypes.number,
  taxes: PropTypes.number,
  isNRP: PropTypes.bool,
  isPartPay: PropTypes.bool,
};

export default PriceDetailsCard;
