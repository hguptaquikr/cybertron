import React, { Component } from 'react';
import { Linking, Share, StatusBar, Platform, BackHandler } from 'react-native';
import { Button, Spacer, Flex, theme } from 'leaf-ui/native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import get from 'lodash/get';
import { withRouter } from 'react-router-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import * as bookingActionCreators from '../../booking/bookingDuck';
import * as searchActionCreators from '../../search/searchDuck';
import * as hotelActionCreators from '../../hotel/hotelDuck';
import * as rateAndReviewActionCreators from '../../feedback/rateAndReviewDuck';
import Text from '../../components/NewLeaf/Text/Text';
import SlideInView from '../../components/SlideInView/SlideInView';
import storeHouse from '../../services/storeHouse';
import SpecialRateHotelsBanner from '../SpecialRateHotelsBanner/SpecialRateHotelsBanner';
import WhyLoginCard from '../../components/WhyLoginCard/WhyLoginCard';
import PointsCreditInfo from '../../components/PointsCreditInfo/PointsCreditInfo';
import BookingDetailsCard from './BookingDetailsCard';
import HotelDetailsCard from './HotelDetailsCard';
import PlanDetails from '../PlanDetails/PlanDetails';
import color from '../../theme/color';
import config from '../../config';
import analyticsService from '../../analytics/analyticsService';
import userService from '../../user/userService';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';
import NeedToKnowPolicy from '../../hotel/NeedToKnowPolicy/NeedToKnowPolicy';
import BrandCampaignAccordion from '../../brandCampaign/BrandCampaignAccordion';

const RootContainer = styled.ScrollView`
  flex: 1;
`;

const CBcontainer = styled.View`
  flex: 1;
  backgroundColor: ${color.veryLightGrey};
`;

const HeaderView = styled.View`
  backgroundColor: ${theme.color.teal};
  justifyContent: center;
  alignItems: center;
`;

const PayToBook = styled.View`
  backgroundColor: ${color.white};
  padding: 8px;
`;

const ViewBDbutton = styled.View`
  backgroundColor: ${color.veryLightGrey};
  padding: 8px;
`;

const BannerContainer = styled.View`
  marginLeft: 8;
  marginRight: 8;
`;

const WhyLoginToTreebo = styled(WhyLoginCard)`
  margin: 8px;
`;

const TreeboPointsInfo = styled(PointsCreditInfo)`
  margin: 8px;
`;

const BackToHomeButton = styled(Button)`
  backgroundColor: ${color.white};
`;

class ConfirmationPage extends Component {
  state = {
    isLoggedIn: true,
  };

  componentWillMount() {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('light-content');
    } else {
      StatusBar.setBackgroundColor(color.statusBarBackground);
    }
    BackHandler.addEventListener('hardwareBackPress', this.onBackHomePress);
  }

  componentDidMount() {
    const { booking, hotel, rateAndReviewActions: { setRatingMiniSheetVisible } } = this.props;
    analyticsService.segmentTrackEvent('Confirmation Page Viewed', this.getCommonEventProps());

    userService.isLoggedIn().then((isLoggedId) => {
      if (isLoggedId) {
        this.setState({ isLoggedIn: true });
      } else {
        this.setState({ isLoggedIn: false });
      }
    }).catch(() => this.setState({ isLoggedIn: false }));

    const bookingdate = moment();
    storeHouse.save([
      ['latest_booking', booking],
      ['latest_hotel_booked', hotel],
      ['latest_booking_date', bookingdate],
    ])
      .then(() => {
        storeHouse.get('is_feedback_submitted')
          .then((isFeedbackSubmitted) => {
            if (!isFeedbackSubmitted) {
              setTimeout(
                () => setRatingMiniSheetVisible(true, hotel, bookingdate, booking),
                2000,
              );
            }
          });
      });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  onBackHomePress = async () => {
    const {
      history,
      searchActions,
      query,
      hotelActions: { resetHotelState },
    } = this.props;
    const updatedQueryObject = {
      ...query,
      hotelId: undefined,
    };
    searchActions.setSearchState(updatedQueryObject);
    await history.go(1 - history.length);
    resetHotelState();
    return true;
  };

  getCommonEventProps = () => {
    const {
      booking: {
        dates,
        payment: { orderId },
      },
      hotel: { id, name },
      price: { totalPrice },
    } = this.props;
    return {
      hotel_id: id,
      af_content_id: id,
      hotel_name: name,
      price: totalPrice,
      checkin_date: dates.checkiIn,
      checkout_date: dates.checkOut,
      stay_duration: dates.numberOfNights,
      order_id: orderId,
    };
  };

  openPartialPaymentLink = (link) => {
    analyticsService.segmentTrackEvent('Part Pay Clicked', this.getCommonEventProps());
    Linking.openURL(link);
  };

  shareDetails = () => {
    const {
      booking: {
        dates,
        payment: { orderId },
      },
      hotel: { name, coordinates },
    } = this.props;
    const url = `${config.baseUrl}/account/booking-history/${orderId}/`;
    const mapsUrl = `https://maps.google.com/?q=${coordinates.lat},${coordinates.lng}`;
    const content = {
      message: `Hi, Here's my Treebo Hotel booking info -
                ${url} Checkin Date: ${dates.checkIn} Checkout Date: ${dates.checkOut} Hotel Name: ${name} Location: ${mapsUrl}`,
      url,
      title: `Checkout ${name}`,
      dialogTitle: `Checkout ${name}`,
    };
    Share.share(content);
  };

  render() {
    const {
      booking,
      booking: {
        guest: { mobile },
        payment: {
          orderId,
          partialPaymentAmount,
          partialPaymentLink,
        },
      },
      hotel,
      price,
      coupon: { appliedCouponCode },
    } = this.props;

    const { isLoggedIn } = this.state;
    const shouldCollectPartialPayment = !!partialPaymentLink;
    return (
      <RootContainer
        showsVerticalScrollIndicator={false}
        automaticallyAdjustContentInsets={false}
      >
        <HeaderView>
          <Spacer padding={[6, 0, 1, 0]}>
            <Flex justifyContent="center" alignItems="center">
              {getIcon('booking-confirmation', color.white, 40, 40)}
            </Flex>
          </Spacer>
          <Text
            size="xxl"
            color="white"
            weight="bold"
            testID="booking-created-text"
            accessibilityLabel="booking-created-text"
          >
            Booking {shouldCollectPartialPayment ? 'Created' : 'Confirmed'}!
          </Text>
          <Spacer padding={[2, 0, 4, 0]}>
            <Text
              size="xs"
              color="white"
              weight="normal"
              testID="booking-id-text"
              accessibilityLabel="booking-id-text"
            >
              Booking ID: {orderId}
            </Text>
          </Spacer>
        </HeaderView>
        <SlideInView style={{ flex: 1 }}>
          <CBcontainer>
            {!isLoggedIn && <WhyLoginToTreebo />}
            <HotelDetailsCard
              shareDetails={this.shareDetails}
              hotel={hotel}
            />
            <TreeboPointsInfo phoneNumber={mobile} />
            <BookingDetailsCard
              booking={booking}
            />
            <BannerContainer>
              <SpecialRateHotelsBanner hotelID={hotel.id} />
            </BannerContainer>
            <BrandCampaignAccordion
              history={this.props.history}
              callingPage="Confirmation"
            />
            <NeedToKnowPolicy hotelId={hotel.id} />
            <PlanDetails
              roomTarrif={price.pretaxPrice}
              memberPriceDiscount={get(price, 'memberDiscount.value', 0)}
              couponCode={appliedCouponCode}
              couponDiscountAmount={price.discountAmount}
              taxes={price.tax}
              totalTariff={price.sellingPrice}
              treeboPointsUsed={price.treeboPointsUsed}
              isUsingTreeboPoints={price.isUsingTreeboPoints}
              totalPayable={price.totalPrice}
              isNRP={price.code === 'NRP'}
              isPaid={!shouldCollectPartialPayment}
            />
            <ViewBDbutton>
              <BackToHomeButton
                kind="outlined"
                block
                onPress={this.onBackHomePress}
                testID="back-to-home-button"
                accessibilityLabel="back-to-home-button"
              >
                <Text size="m" weight="medium" color="green">
                  BACK TO HOME
                </Text>
              </BackToHomeButton>
            </ViewBDbutton>
            {
              shouldCollectPartialPayment &&
                <PayToBook>
                  <Button
                    block
                    onPress={() => this.openPartialPaymentLink(partialPaymentLink)}
                    testID="part-pay-button"
                    accessibilityLabel="part-pay-button"
                  >
                    <Text size="m" weight="medium" color="white">
                      Pay &#x20b9;{partialPaymentAmount} To Confirm
                    </Text>
                  </Button>
                </PayToBook>
            }
          </CBcontainer>
        </SlideInView>
      </RootContainer>
    );
  }
}

ConfirmationPage.propTypes = {
  booking: PropTypes.object.isRequired,
  hotel: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  searchActions: PropTypes.object,
  hotelActions: PropTypes.object,
  query: PropTypes.object,
  coupon: PropTypes.object.isRequired,
  rateAndReviewActions: PropTypes.object,
};

const mapStateToProps = (state, { location: { query } }) => ({
  booking: state.booking,
  hotel: state.hotel.results[state.booking.hotelId],
  price: query.price,
  coupon: state.coupon,
  ...query,
});

const mapDispatchToProps = (dispatch) => ({
  bookingActions: bindActionCreators(bookingActionCreators, dispatch),
  searchActions: bindActionCreators(searchActionCreators, dispatch),
  hotelActions: bindActionCreators(hotelActionCreators, dispatch),
  rateAndReviewActions: bindActionCreators(rateAndReviewActionCreators, dispatch),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(ConfirmationPage));
