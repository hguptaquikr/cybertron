import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import Text from '../../components/NewLeaf/Text/Text';
import { pluralize, capitalizeFirstLetter } from '../../utils/utils';
import color from '../../theme/color';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const Row = styled.View`
  flexDirection: row;
  marginBottom: ${({ marginBottom }) => marginBottom || 8};
`;

const Container = styled.View`
  paddingHorizontal: 16;
  paddingVertical: 24;
`;

const FlexChild = styled.View`
  flex: 1;
`;

const DurationView = styled.View`
  flex: 1;
  alignItems: center;
`;

const CardView = styled.View`
  backgroundColor: ${color.white};
`;

const BookingDetailsCard = ({
  booking: {
    dates,
    guest,
    room,
  },
}) => (
  <CardView
    testID="booking-details-section"
    accessibilityLabel="booking-details-section"
  >
    <Container>
      <Row>
        <FlexChild>
          <Text size="xs" color="greyDark" weight="normal">Check In</Text>
        </FlexChild>
        <DurationView>
          {getIcon('time', color.disabledGrey)}
        </DurationView>
        <FlexChild>
          <Text size="xs" color="greyDark" weight="normal">Check Out</Text>
        </FlexChild>
      </Row>
      <Row marginBottom={28}>
        <FlexChild>
          <Text
            size="s"
            color="greyDarker"
            weight="medium"
            family="medium"
            testID="booking-checkin-date-text"
            accessibilityLabel="booking-checkin-date-text"
          >
            {dates.checkIn}
          </Text>
        </FlexChild>
        <DurationView>
          <Text
            size="xs"
            color="grey"
            weight="normal"
            testID="booking-no-of-nights-text"
            accessibilityLabel="booking-no-of-nights-text"
          >
            {pluralize(dates.numberOfNights.count, 'Night', 's', true)}
          </Text>
        </DurationView>
        <FlexChild>
          <Text
            size="s"
            color="greyDarker"
            weight="medium"
            family="medium"
            testID="booking-checkout-date-text"
            accessibilityLabel="booking-checkout-date-text"
          >
            {dates.checkOut}
          </Text>
        </FlexChild>
      </Row>
      <Row>
        <Text size="xs" weight="normal" color="greyDark">Primary Traveller</Text>
      </Row>
      <Row marginBottom={28}>
        <Text
          size="s"
          color="greyDarker"
          weight="normal"
          testID="booking-guest-name-text"
          accessibilityLabel="booking-guest-name-text"
        >
          {guest.name}
        </Text>
      </Row>
      <Row>
        <FlexChild>
          <Text size="xs" color="greyDark" weight="normal">Guest</Text>
        </FlexChild>
        <FlexChild>
          <Text size="xs" color="greyDark" weight="normal">Room</Text>
        </FlexChild>
      </Row>
      <Row>
        <FlexChild>
          <Text
            size="s"
            color="greyDarker"
            weight="medium"
            family="medium"
            testID="booking-total-adults-children-text"
            accessibilityLabel="booking-total-adults-children-text"
          >
            {pluralize(room.totalAdults, 'Adult', 's', true)}, {pluralize(room.totalChildren, 'Kid', 's', true)}
          </Text>
        </FlexChild>
        <FlexChild>
          <Text
            size="s"
            color="greyDarker"
            weight="medium"
            family="medium"
            testID="booking-room-info-text"
            accessibilityLabel="booking-room-info-text"
          >
            {room.count} x {capitalizeFirstLetter(room.type)}
          </Text>
        </FlexChild>
      </Row>
    </Container>
  </CardView>
);

BookingDetailsCard.propTypes = {
  booking: PropTypes.object.isRequired,
};

export default BookingDetailsCard;
