/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import Text from '../../components/NewLeaf/Text/Text';
import hotelService from '../../hotel/hotelService';
import { openMaps } from '../../utils/utils';
import color from '../../theme/color';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const CardView = styled.View`
  padding: 16px;
  backgroundColor: ${color.white};
`;

const FlexParentRow = styled.View`
  flexDirection: row;
  marginBottom: 8px;
  alignItems: center;
`;

const FlexChild = styled.View`
  flex:1;
`;

const TouchableOpacity = styled.TouchableOpacity``;

const View = styled.View``;

const DirectionView = styled.View`
  paddingLeft: 5px;
`;

const HotelDetailsCard = ({ hotel, shareDetails }) => (
  <CardView
    testID="hotel-details-section"
    accessibilityLabel="hotel-details-section"
  >
    <FlexParentRow>
      <FlexChild>
        <Text
          size="m"
          weight="medium"
          family="medium"
          color="greyDarker"
          testID="hotel-name-text"
          accessibilityLabel="hotel-name-text"
        >
          {hotel.name}
        </Text>
      </FlexChild>
      <TouchableOpacity onPress={shareDetails}>
        {getIcon('share')}
      </TouchableOpacity>
    </FlexParentRow>
    <FlexParentRow>
      <Text
        size="s"
        weight="medium"
        color="greyDark"
        testID="hotel-address-text"
        accessibilityLabel="hotel-address-text"
      >
        {hotelService.constructAddress(hotel.address)}
      </Text>
    </FlexParentRow>
    <TouchableOpacity onPress={openMaps(hotel.coordinates.lat, hotel.coordinates.lng)}>
      <FlexParentRow>
        <View>
          <Text size="s" weight="normal" color="blue">View Direction</Text>
        </View>
        <DirectionView>
          {getIcon('arrow-next', color.blue)}
        </DirectionView>
      </FlexParentRow>
    </TouchableOpacity>
  </CardView>
);

HotelDetailsCard.propTypes = {
  hotel: PropTypes.object,
  shareDetails: PropTypes.func.isRequired,
};

export default HotelDetailsCard;
