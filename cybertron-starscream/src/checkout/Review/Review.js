import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Spacer } from 'leaf-ui/native';
import get from 'lodash/get';
import ImageLoad from '../../components/ImageLoad/ImageLoad';
import Price from '../../components/NewLeaf/Price/Price';
import Text from '../../components/NewLeaf/Text/Text';
import RatePlanTypeView from '../../price/RatePlanTypeView/RatePlanTypeView';
import DateAndGuestDetails from '../../hotel/DateAndGuestDetails/DateAndGuestDetails';
import color from '../../theme/color';
import SpecialRateHotelsBanner from '../SpecialRateHotelsBanner/SpecialRateHotelsBanner';
import { imageResolutions } from '../../utils/utils';

const View = styled.View``;

const RootContainer = styled.View`
  backgroundColor: ${color.white};
  padding: 16px;
`;
const Container = styled.View``;

const HDContainer = styled.View`
  flexDirection: row;
`;

const TextContainer = styled.View`
  flex: 2;
  paddingLeft: 16px;
  paddingRight: 16px;
`;

const ConditionContainer = styled.View`
  marginBottom: 8px;
`;

const PriceContainer = styled.View`
  flexDirection: row;
  marginTop: 8;
  alignItems: center;
  justifyContent: flex-start;
`;

const Review = (
  {
    search,
    hotel,
    price: {
      totalPrice,
    },
    ratePlan,
    searchActions,
    isDisabled,
  },
) => {
  const locality = get(hotel, 'address.locality', '');
  const addressJsx = get(hotel, 'address.locality', '') ? `${locality}, ${get(hotel, 'address.city', '')}` : `${get(hotel, 'address.city', '')}`;
  const url = get(hotel, 'images[0].url', '');
  return (
    <Container
      testID="booking-review-section"
      accessibilityLabel="booking-review-section"
    >
      <RootContainer>
        <HDContainer>
          <ImageLoad
            source={{ uri: `${url}?${imageResolutions.highres}` }}
          />
          <TextContainer>
            <View>
              <Text
                size="m"
                weight="normal"
                numberOfLines={1}
                testID="hotel-name-text"
                accessibilityLabel="hotel-name-text"
              >
                {get(hotel, 'name', '')}
              </Text>
              <Text
                size="s"
                weight="normal"
                testID="hotel-address-text"
                accessibilityLabel="hotel-address-text"
              >
                {addressJsx}
              </Text>
            </View>
            <PriceContainer>
              <Price
                testID="hotel-price-text"
                accessibilityLabel="hotel-price-text"
                price={totalPrice}
                size="l"
                color="greyDarker"
                type="regular"
                bold
              />
              <Spacer padding={[0, 1]}>
                <RatePlanTypeView isNRP={ratePlan === 'NRP'} />
              </Spacer>
            </PriceContainer>
            <ConditionContainer>
              <Text size="xs" color="grey">inc. of all taxes</Text>
            </ConditionContainer>
          </TextContainer>
        </HDContainer>
        <SpecialRateHotelsBanner hotelID={hotel.id} />
      </RootContainer>
      <DateAndGuestDetails
        search={search}
        searchActions={searchActions}
        isDisabled={isDisabled}
      />
    </Container>
  );
};

Review.propTypes = {
  hotel: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  search: PropTypes.object.isRequired,
  ratePlan: PropTypes.string.isRequired,
  searchActions: PropTypes.object.isRequired,
  isDisabled: PropTypes.bool,
};

Review.defaultProps = {
  isDisabled: false,
};

export default Review;
