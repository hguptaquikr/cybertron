import moment from 'moment';
import get from 'lodash/get';
import searchService from '../search/searchService';

export default {
  transformCheckoutApi({ data }) {
    const { hotel, room, price, date } = data;
    // Fixme: Need to remove all the defaults from the hotel object
    return {
      hotel: {
        id: hotel.id,
        name: hotel.name,
        address: {
          city: hotel.city,
          cityId: hotel.city_id,
          locality: hotel.locality,
          pincode: hotel.pincode,
          state: hotel.state,
          street: hotel.street,
        },
        rooms: [],
        coordinates: {},
        description: '',
        amenities: [],
        accessibilities: [],
        trilights: [],
        policies: hotel.hotel_policies,
        images: [{ url: hotel.image_url }],
      },
      booking: {
        bid: data.bid,
        room: {
          ...room,
          type: room.type.toLowerCase(),
          totalAdults: get(data, 'guest.adults', 0),
          totalChildren: get(data, 'guest.children', 0),
        },
        hotelId: hotel.id,
        dates: searchService.formattedDatePicker({
          start: moment(date.checkin),
          end: moment(date.checkout),
        }),
        isPrepaidOnly: 'is_prepaid' in data ? data.is_prepaid : false,
        payment: { orderId: '' },
      },
      price: {
        [room.type.toLowerCase()]: {
          ratePlans: price.all_rate_plans.reduce((roomPrice, ratePlan) => ({
            ...roomPrice,
            [ratePlan.rate_plan.code]: {
              code: ratePlan.rate_plan.code,
              tag: ratePlan.rate_plan.tag,
              description: ratePlan.rate_plan.description,
              type: ratePlan.rate_plan.code === 'NRP' ? 'Non Refundable' : 'Refundable',
              sellingPrice: +ratePlan.price.sell_price,
              basePrice: +ratePlan.price.base_price,
              totalPrice: +ratePlan.price.net_payable_amount,
              pretaxPrice: +ratePlan.price.pretax_price,
              tax: +ratePlan.price.tax,
              discountAmount: +ratePlan.price.coupon_discount,
              discountPercentage: +ratePlan.price.total_discount_percent,
              roomDiscount: +ratePlan.price.promo_discount,
              treeboPointsUsed: +ratePlan.price.wallet_deduction,
              isUsingTreeboPoints: ratePlan.price.wallet_applied,
              totalUsableTreeboPoints: +ratePlan.price.wallet_deductable_amount,
              couponCode: ratePlan.price.coupon_code,
              couponDiscount: ratePlan.price.coupon_discount,
              couponApplied: ratePlan.price.coupon_applied,
            },
          }), {}),
          memberDiscount: {
            value: price.selected_rate_plan.price.member_discount,
            isApplied: price.member_discount_applied,
            isAvailable: price.member_discount_available,
          },
          isAvailable: true,
        },
      },
      checkout: {
        hotelId: hotel.id,
        isPayAtHotelEnabled: 'pah_enabled' in data ? data.pah_enabled : true,
        payNowEnabled: 'paynow_enabled' in data ? data.paynow_enabled : true,
        isOtpEnabled: data.isOtpEnabled,
        selectedRatePlan: price.selected_rate_plan.rate_plan.code,
      },
      coupon: {
        results: {
          [data.coupon_code]: {
            description: data.coupon_desc,
          },
        },
        appliedCouponCode: data.coupon_code || '',
      },
    };
  },

  transformPayApi({ data }, isPayAtHotel) {
    return {
      orderId: data.order_id,
      gatewayOrderId: data.pg_order_id,
      serviceOrderId: data.ps_order_id,
      orderEventArgs: data.order_event_args,
      isPayAtHotel,
    };
  },

  buildPayData(
    { price: { results: prices }, booking: { bid, guest, room, hotelId }, gateway, ratePlan },
  ) {
    const totalPrice = get(prices, `${hotelId}.${room.type}.ratePlans.${ratePlan}.totalPrice`, 0);
    // TODO: Check for the guest obj returned in the confirmation page api
    return {
      name: guest.name,
      mobile: guest.mobile,
      email: guest.email,
      organization_name: guest.organizationName,
      organization_address: guest.organizationAddress,
      organization_taxcode: guest.organizationTaxcode,
      bid,
      check_in_time: '12:00',
      check_out_time: '11:00',
      message: '',
      actual_total_cost: totalPrice,
      gateway,
    };
  },

  buildConfirmData({ razorPayId, razorPaySignature, booking: { payment }, gateway }) {
    return {
      order_id: payment.orderId,
      ps_order_id: payment.serviceOrderId,
      pg_order_id: payment.gatewayOrderId,
      gateway,
      pg_meta: {
        pg_payment_id: razorPayId,
        pg_signature: razorPaySignature,
      },
    };
  },

  transformIsNumberVerifiedApi({ data }) {
    return {
      isOtpVerified: data.is_verified,
    };
  },
};
