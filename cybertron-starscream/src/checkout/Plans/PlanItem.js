/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import Price from '../../components/NewLeaf/Price/Price';
import Text from '../../components/NewLeaf/Text/Text';
import RatePlanTypeView from '../../price/RatePlanTypeView/RatePlanTypeView';
import CheckBox from '../../components/CheckBox/CheckBox';
import TouchableComponent from '../../components/Touchable/TouchableComponent';

const View = styled.View``;

const Container = styled.View``;

const RootContainer = styled.View`
  flexDirection: row;
  marginVertical: 16px;
`;

const PriceContainer = styled.View`
  flexDirection: row;
  alignItems: flex-end;
`;

const DetailsContainer = styled.View`
  flex: 2;
`;

const CheckboxContainer = styled.View`
  marginTop: 5px;
  marginRight: 12px;
`;

const RatePlanTypeContainer = styled.View`
  paddingTop: 10px;
  paddingBottom: 10px;
`;

const RatePlanTypeDetailContainer = styled.View`
`;

const PlanItem = ({ totalPrice, planType, selected, onPress, planLength }) => (
  <Container>
    {
      planLength > 1 ? (
        <TouchableComponent onPress={onPress} >
          <RootContainer>
            <CheckboxContainer>
              <CheckBox
                checked={selected}
                onPress={onPress}
              />
            </CheckboxContainer>
            <DetailsContainer>
              <PriceContainer>
                <Price price={totalPrice} size="l" color="greyDarker" bold />
              </PriceContainer>
              {
                planType === 'NRP' ? (
                  <View>
                    <RatePlanTypeContainer>
                      <RatePlanTypeView isNRP />
                    </RatePlanTypeContainer>
                    <RatePlanTypeDetailContainer >
                      <Text size="s" weight="normal" color="greyDark">
                        Tax Inclusive. Prepaid only, this booking is not refundable
                      </Text>
                    </RatePlanTypeDetailContainer>
                  </View>) : null
              }
              {
                planType === 'EP' ? (
                  <View>
                    <RatePlanTypeContainer>
                      <RatePlanTypeView />
                    </RatePlanTypeContainer>
                    <RatePlanTypeDetailContainer >
                      <Text size="s" weight="normal" color="greyDark">
                        Tax Inclusive. Pay now or later at hotel.
                        Zero cancellation fee within 24 hour of cancellation.
                      </Text>
                    </RatePlanTypeDetailContainer>
                  </View>) : null
              }
            </DetailsContainer>
          </RootContainer>
        </TouchableComponent>
      ) : null
    }
  </Container>
);

PlanItem.propTypes = {
  totalPrice: PropTypes.number,
  planType: PropTypes.string,
  selected: PropTypes.bool,
  onPress: PropTypes.func,
  planLength: PropTypes.number,
};

export default PlanItem;
