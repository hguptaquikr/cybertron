import React, { Component } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEqual from 'lodash/isEqual';
import styled from 'styled-components/native';
import PlanItem from './PlanItem';
import List from '../../components/List/List';
import color from '../../theme/color';

const ListContainer = styled.View`
  paddingBottom: 8;
  backgroundColor: ${color.disabledGreyOpacity50};
`;

const RootContainer = styled.View`
  backgroundColor: ${color.white};
  paddingHorizontal: 16;
  paddingVertical: 8;
`;

class Plans extends Component {
  state = {
    selectedRatePlan: this.props.selectedRatePlan,
  };

  componentWillReceiveProps(nextProps) {
    const { ratePlans, selectedRatePlan } = this.props;
    if (!isEqual(ratePlans, nextProps.ratePlans)
      && selectedRatePlan !== nextProps.selectedRatePlan) {
      this.setState({ selectedRatePlan: nextProps.selectedRatePlan });
    }
  }

  getRatePlansArray = (ratePlans) =>
    Object.keys(ratePlans).map((ratePlan) => ({
      code: ratePlans[ratePlan].code,
      sellingPrice: ratePlans[ratePlan].sellingPrice,
      totalPrice: ratePlans[ratePlan].totalPrice,
      strikedPrice: ratePlans[ratePlan].strikedPrice,
      discountPercentage: ratePlans[ratePlan].discountPercentage,
    }));

  renderItem = (ratePlansCount) => (item) => {
    const { getCheckoutDetails } = this.props;
    const { selectedRatePlan } = this.state;
    const selected = selectedRatePlan === item.item.code;
    return (
      <RootContainer>
        <PlanItem
          totalPrice={get(item, 'item.totalPrice', 0)}
          planType={get(item, 'item.code', 0)}
          selected={selected}
          onPress={selected ? null : () => {
            this.setState({ selectedRatePlan: item.item.code });
            getCheckoutDetails(null, item.item.code);
          }}
          planLength={ratePlansCount}
        />
      </RootContainer>
    );
  };

  render() {
    const { ratePlans } = this.props;
    const ratePlansArray = this.getRatePlansArray(ratePlans);
    const ratePlansCount = ratePlansArray.length;
    return (
      ratePlansCount > 1 ? (
        <ListContainer>
          <List
            keyExtractor={(item, index) => index}
            data={ratePlansArray}
            renderItem={this.renderItem(ratePlansCount)}
          />
        </ListContainer>
      ) : null
    );
  }
}

Plans.propTypes = {
  ratePlans: PropTypes.object.isRequired,
  selectedRatePlan: PropTypes.string.isRequired,
  getCheckoutDetails: PropTypes.func.isRequired,
};

export default Plans;
