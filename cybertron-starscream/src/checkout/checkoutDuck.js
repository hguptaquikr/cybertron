import { handle } from 'redux-pack';
import { Platform } from 'react-native';
import checkoutService from './checkoutService';
import apiService from '../api/apiService';

const CHECKOUT_CONFIRM_BOOKING = 'CHECKOUT_CONFIRM_BOOKING';
const CHECKOUT_PAY_AT_HOTEL = 'CHECKOUT_PAY_AT_HOTEL';
const CHECKOUT_PAY_NOW = 'CHECKOUT_PAY_NOW';
const CONTINUE_DETAILS = 'CONTINUE_DETAILS';
const GET_SEARCH_PRICES = 'GET_SEARCH_PRICES';
const GET_CHECKOUT_DETAILS = 'GET_CHECKOUT_DETAILS';
const GET_PAY_NOW_DETAILS = 'GET_PAY_NOW_DETAILS';
const GOTO_CHECKOUT_STEP = 'GOTO_CHECKOUT_STEP';
const IS_VERIFIED_NUMBER = 'IS_VERIFIED_NUMBER';
const SEND_CHECKOUT_OTP = 'SEND_CHECKOUT_OTP';
const UPDATE_CHECKOUT_MOBILE = 'UPDATE_CHECKOUT_MOBILE';
const VERIFY_CHECKOUT_OTP = 'VERIFY_CHECKOUT_OTP';
const APPLY_WALLET = 'APPLY_WALLET';
const REMOVE_WALLET = 'REMOVE_WALLET';

const initialState = {
  step: 1,
  hotelId: 0,
  isPayAtHotelEnabled: true,
  payNowEnabled: true,
  isOtpEnabled: true,
  isOtpVerified: false,
  ratePlans: {},
  isLoading: false,
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_CHECKOUT_DETAILS: return handle(state, action, {
      start: (s) => ({
        ...s,
        isLoading: true,
      }),
      success: (s) => ({ ...s, ...payload.checkout }),
      finish: (s) => ({ ...s, isLoading: false }),
    });

    case GOTO_CHECKOUT_STEP:
      return {
        ...state,
        ...payload,
      };

    case IS_VERIFIED_NUMBER: return handle(state, action, {
      success: (s) => ({
        ...s,
        ...payload,
      }),
    });

    case CONTINUE_DETAILS:
      return {
        ...state,
        step: payload.step,
      };

    case GET_SEARCH_PRICES:
      return {
        ...state,
        selectedRatePlan: '',
      };

    case APPLY_WALLET:
    case REMOVE_WALLET:
      return handle(state, action, {
        start: (s) => ({
          ...s,
          isLoading: true,
        }),
        finish: (s) => ({
          ...s,
          isLoading: false,
        }),
      });

    default:
      return state;
  }
};

export const sendCheckoutOtp = (mobile) => (dispatch, getState, { api }) =>
  dispatch({
    type: SEND_CHECKOUT_OTP,
    promise: api.post('/v2/otp/', { phone_number: mobile }),
  });

export const isVerifiedNumber = (mobile) => (dispatch, getState, { api }) =>
  dispatch({
    type: IS_VERIFIED_NUMBER,
    promise: api.post('/v2/otp/is-verified/', { phone_number: mobile })
      .then(checkoutService.transformIsNumberVerifiedApi),
  });

export const updateCheckoutMobile = (mobile) => (dispatch) => {
  dispatch({
    type: UPDATE_CHECKOUT_MOBILE,
    payload: { mobile },
  });
  return dispatch(sendCheckoutOtp(mobile));
};

export const verifyCheckoutOtp = (mobile, otp) => (dispatch, getState, { api }) => {
  const verificationData = {
    phone_number: mobile,
    verification_code: otp,
  };
  return dispatch({
    type: VERIFY_CHECKOUT_OTP,
    promise: api.post('/v2/otp/verify/', verificationData),
  });
};

export const getCheckoutDetails = (bid, ratePlan, applyWallet) =>
  (dispatch, getState, { api }) => {
    const {
      search: { searchInput, datePicker, roomConfig },
      coupon: { appliedCouponCode },
      ui,
    } = getState();

    let { hotelId } = searchInput.place;
    if (typeof hotelId === 'string') {
      hotelId = Number(hotelId.split('-').pop());
    }

    const itineraryQuery = {
      ...apiService.getSearchQuery({
        hotelId,
        range: datePicker.range,
        rooms: roomConfig.rooms,
        roomType: roomConfig.roomType,
        ratePlan,
      }),
      bid,
      channel: Platform.OS,
      apply_wallet: applyWallet ? 'True' : 'False',
      couponcode: appliedCouponCode,
      ...ui.utmParams,
    };

    return dispatch({
      type: GET_CHECKOUT_DETAILS,
      promise: api.get('/v6/checkout/itinerary/', itineraryQuery)
        .then(checkoutService.transformCheckoutApi),
    });
  };

export const goToStep = (step) => ({
  type: GOTO_CHECKOUT_STEP,
  payload: {
    step,
  },
});

export const getPayNowDetails = (bid) => (dispatch, getState, { api }) => dispatch({
  type: GET_PAY_NOW_DETAILS,
  promise: api.get('/v3/checkout/paynow/', { bid })
    .then(checkoutService.transformPayNowDetailsApi),
});

export const continueDetails = (details) => (dispatch) => {
  dispatch({
    type: CONTINUE_DETAILS,
    payload: {
      step: 3,
      details: { ...details },
    },
  });
};

export const initiateBooking = (ratePlan, gateway = 'razorpay') => (dispatch, getState, { api }) => {
  const { price, booking } = getState();
  const payData = checkoutService.buildPayData({ price, booking, gateway, ratePlan });
  /*
  const analyticsData = {
    ...payData,
    mode: 'pre-paid',
  };
  */
  return dispatch({
    type: CHECKOUT_PAY_NOW,
    promise: api.post('/v5/checkout/paynow/', payData)
      .then((res) => checkoutService.transformPayApi(res, false)),
    /*
    meta: {
      onSuccess: () => analyticsService.checkoutStep('3 - Payment Initiated', analyticsData),
    },
    */
  });
};

export const payAtHotel = (ratePlan) => (dispatch, getState, { api }) => {
  const { price, booking } = getState();
  const payData = checkoutService.buildPayData({ price, booking, gateway: null, ratePlan });
  const bookingData = checkoutService.buildConfirmData({ price, booking, gateway: null });
  // const analyticsData = {
  //   ...payData,
  //   mode: 'pay-at-hotel',
  // };
  // analyticsService.checkoutStep('3 - Payment Initiated', analyticsData);

  return dispatch({
    type: CHECKOUT_PAY_AT_HOTEL,
    promise: api.post('/v5/checkout/payathotel/', payData)
      .then((res) => checkoutService.transformPayApi(res, true)),
    meta: {
      onSuccess: (res) => {
        bookingData.order_id = bookingData.order_id || res.orderEventArgs.orderId;
        // if (androidService.isAndroid) {
        //   // send checkout details to webengage and appflyers
        //   androidService.track('Checkout step 4 - Payment Confirmed', bookingData);
        // }
        // analyticsService.checkoutStep('4 - Payment Confirmed', bookingData);
        // analyticsService.exposeGlobally({ confirm_order_analytics: res.orderEventArgs });
        // analyticsService.adwordsCompletedOrder(res.orderEventArgs);
      },
    },
  });
};

export const confirmBooking = (razorPayId, razorPaySignature, gateway = 'razorpay') =>
  (dispatch, getState, { api }) => {
    const { booking } = getState();
    const bookingData =
      checkoutService.buildConfirmData({ booking, razorPayId, razorPaySignature, gateway });

    return dispatch({
      type: CHECKOUT_CONFIRM_BOOKING,
      promise: api.post('/v5/checkout/confirmbooking/', bookingData)
        .then(checkoutService.transformPayApi),
    });
  };
