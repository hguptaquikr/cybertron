/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import moment from 'moment';
import color from '../../theme/color';
import Text from '../../components/NewLeaf/Text/Text';
import searchService from '../../search/searchService';

const BannerBackgroundView = styled.View`
  borderRadius: 2;
  marginTop: ${({ marginTop }) => marginTop};
  backgroundColor: ${color.pink};
  flexDirection: row;
  borderWidth: 0.5;
  borderColor: ${color.wildWatermelon};
`;

const View = styled.View``;

const getSpecialRatesArrayIfInRange = (hotelID, range, search, content) => {
  const specialRateHotelsObject = content.specialRateHotels;
  const specialDatesArray = get(specialRateHotelsObject, `[${hotelID}].special_rate_dates`, []);
  const displayTags = [];
  if (isEmpty(specialDatesArray)) {
    return displayTags;
  }
  let checkIn;
  let checkOut;
  if (isEmpty(range)) {
    checkIn = search.datePicker.range.start;
    checkOut = search.datePicker.range.end;
  } else {
    checkIn = moment(range.checkIn, 'YYYY-MM-DD');
    checkOut = moment(range.checkOut, 'YYYY-MM-DD');
  }
  for (let i = 0; i < specialDatesArray.length; i += 1) {
    if (searchService.areDatesInBetween(checkIn, checkOut,
      specialDatesArray[i].start_date, specialDatesArray[i].end_date)) {
      displayTags.push(specialDatesArray[i]);
    }
  }
  return displayTags;
};

const renderTagViews = (array, marginTop) => {
  const viewArray = [];
  for (let i = 0; i < array.length; i += 1) {
    viewArray.push(
      <BannerBackgroundView marginTop={i === 0 ? marginTop : 8} key={i}>
        <Text
          size="s"
          weight="normal"
          color={color.charcoalGrey}
        >
          {array[i].text}
        </Text>
      </BannerBackgroundView>,
    );
  }
  return <View>{viewArray}</View>;
};

const SpecialRateHotelsBanner = ({ hotelID, range, search, content, marginTop }) => {
  const specialRatesArray = getSpecialRatesArrayIfInRange(hotelID, range, search, content);
  return (
    !isEmpty(specialRatesArray) ? (
      renderTagViews(specialRatesArray, marginTop)
    ) : null
  );
};

SpecialRateHotelsBanner.propTypes = {
  hotelID: PropTypes.number,
  search: PropTypes.object,
  range: PropTypes.object,
  content: PropTypes.object,
  marginTop: PropTypes.number,
};

SpecialRateHotelsBanner.defaultProps = {
  marginTop: 8,
};

const mapStateToProps = (state) => ({
  content: state.content,
  search: state.search,
});

export default connect(
  mapStateToProps,
  null,
)(SpecialRateHotelsBanner);
