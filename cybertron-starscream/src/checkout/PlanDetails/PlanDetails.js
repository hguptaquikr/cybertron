import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Divider } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';
import Price from '../../components/NewLeaf/Price/Price';
import color from '../../theme/color';
import RatePlanTypeView from '../../price/RatePlanTypeView/RatePlanTypeView';

const RootContainer = styled.View`
  backgroundColor: ${color.white};
  paddingVertical: 14px;
  paddingHorizontal: 16px;
`;

const ViewContainer = styled.View``;

const View = styled.View`
  alignSelf: flex-start;
`;

const TotalPayablePriceContainer = styled.View`
  alignItems: flex-end;
`;

const Item = styled.View`
  flexDirection: row;
  paddingTop: 10px;
  paddingBottom: 10px;
  justifyContent: space-between;
  alignItems: center;
`;

const PlanDetails = ({
  roomTarrif,
  memberPriceDiscount,
  couponCode,
  couponDiscountAmount,
  taxes,
  totalTariff,
  treeboPointsUsed,
  isUsingTreeboPoints,
  totalPayable,
  isNRP,
  isPaid,
}) => (
  <RootContainer>
    <Item>
      <Text size="s" weight="normal" color="greyDarker">
        Room Tariff
      </Text>
      <Price
        price={roomTarrif}
        color="greyDarker"
        size="s"
        type="regular"
        roundUpTo={2}
        testID="room-tarrif-amount-text"
        accessibilityLabel="room-tarrif-amount-text"
      />
    </Item>
    {
      memberPriceDiscount > 0 ? (
        <Item>
          <Text size="s" weight="normal" color="greyDarker">
          Member Discount
          </Text>
          <Price
            price={memberPriceDiscount}
            size="s"
            color="green"
            type="regular"
            roundUpTo={2}
            negative
            testID="member-discount-amount-text"
            accessibilityLabel="member-discount-amount-text"
          />
        </Item>
      ) : null
    }
    {
      couponDiscountAmount ? (
        <Item>
          <Text size="s" weight="normal" color="greyDarker">
            {`"${couponCode}" `} Applied
          </Text>
          <Price
            price={couponDiscountAmount}
            size="s"
            color="green"
            type="regular"
            roundUpTo={2}
            negative
            testID="coupon-discount-amount-text"
            accessibilityLabel="coupon-discount-amount-text"
          />
        </Item>
      ) : null
    }
    {
      taxes ? (
        <Item>
          <Text size="s" weight="normal" color="greyDarker">
          Taxes
          </Text>
          <Price
            price={taxes}
            size="s"
            color="greyDarker"
            type="regular"
            roundUpTo={2}
            testID="taxes-amount-text"
            accessibilityLabel="taxes-amount-text"
          />
        </Item>
      ) : null
    }
    {
      isUsingTreeboPoints && totalTariff && treeboPointsUsed > 0 ? (
        <ViewContainer>
          <Divider />
          <Item>
            <Text size="s" weight="normal" color="greyDarker">
              Total Tariff
            </Text>
            <Price
              price={totalTariff}
              size="s"
              color="greyDarker"
              type="regular"
              roundUpTo={2}
              testID="total-tarrif-amount-text"
              accessibilityLabel="total-tarrif-amount-text"
            />
          </Item>
          <Divider />
          <Item>
            <Text size="s" weight="normal" color="greyDarker">
              Treebo Points Applied
            </Text>
            <Price
              price={treeboPointsUsed}
              size="s"
              color="green"
              type="regular"
              roundUpTo={2}
              negative
              testID="treebo-points-used-amount-text"
              accessibilityLabel="treebo-points-used-amount-text"
            />
          </Item>
        </ViewContainer>
      ) : null
    }
    <Divider />
    <Item>
      <View>
        <Text size="s" color="greyDarker" weight="medium">
          {isPaid ? 'Total Paid' : 'Total Payable'}
        </Text>
        <Text size="xs" weight="normal" color="grey">
          inc. of all taxes
        </Text>
      </View>
      <TotalPayablePriceContainer>
        <Price
          price={totalPayable}
          roundUpTo={2}
          size="l"
          color="greyDarker"
          type="regular"
          bold
          testID="total-payable-amount-text"
          accessibilityLabel="total-payable-amount-text"
        />
        <RatePlanTypeView isNRP={isNRP} />
      </TotalPayablePriceContainer>
    </Item>
  </RootContainer>
);

PlanDetails.propTypes = {
  roomTarrif: PropTypes.number,
  memberPriceDiscount: PropTypes.number,
  couponCode: PropTypes.string,
  couponDiscountAmount: PropTypes.number,
  taxes: PropTypes.number,
  totalTariff: PropTypes.number,
  treeboPointsUsed: PropTypes.number,
  isUsingTreeboPoints: PropTypes.bool,
  totalPayable: PropTypes.number,
  isNRP: PropTypes.bool,
  isPaid: PropTypes.bool,
};

PlanDetails.defaultProps = {
  isPaid: false,
};

export default PlanDetails;
