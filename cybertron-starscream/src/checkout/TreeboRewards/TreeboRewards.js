import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import Text from '../../components/NewLeaf/Text/Text';
import CheckBox from '../../components/CheckBox/CheckBox';
import TouchableComponent from '../../components/Touchable/TouchableComponent';
import color from '../../theme/color';

const Container = styled.View`
  padding: 16px;
  flexDirection: row;
  backgroundColor: ${color.white};
  alignItems: center;
`;

const TreeboRewards = ({
  isApplied,
  appliedAmount,
  onApplyRewards,
}) => (
  <TouchableComponent
    onPress={onApplyRewards}
    testID="use-treebo-points-button"
    accessibilityLabel="use-treebo-points-button"
  >
    <Container>
      <CheckBox
        onClick={onApplyRewards}
        square
        checked={isApplied}
        color={isApplied ? color.primary : color.warmGrey}
      />
      <Text
        size="s"
        family="medium"
        weight="medium"
        testID="treebo-points-text"
        accessibilityLabel="treebo-points-text"
      >
        {`Pay \u20B9${Math.round(appliedAmount)} using Treebo Rewards`}
      </Text>
    </Container>
  </TouchableComponent>
);

TreeboRewards.propTypes = {
  isApplied: PropTypes.bool.isRequired,
  appliedAmount: PropTypes.number.isRequired,
  onApplyRewards: PropTypes.func.isRequired,
};

export default TreeboRewards;
