import React, { Component } from 'react';
import { Platform, StatusBar, BackHandler, TextInput } from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-native';
import { Spacer, Button } from 'leaf-ui/native';
import styled from 'styled-components/native';
import * as userActionCreators from '../../user/userDuck';
import * as contentActionCreators from '../../content/contentDuck';
import * as toastActionCreators from '../../toast/toastDuck';
import Modal from '../../components/Modal/Modal';
import BackButtonHeader from '../../components/Header/BackButtonHeader';
import RadioGroup from '../../components/RadioGroup/RadioGroup';
import Content from '../../components/Content/Content';
import Container from '../../components/Container/Container';
import Text from '../../components/NewLeaf/Text/Text';
import Loader from '../../components/Loader/Loader';
import CancellationPolicty from '../BookingHistoryDetails/CancellationPolicy';
import color from '../../theme/color';

const FormContainer = styled.View`
  flex: 1;
  paddingTop: 32;
`;

const ConfirmButtonContainer = styled.View`
  backgroundColor: ${color.white};
  paddingVertical: 8;
  paddingHorizontal: 8;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
`;

const CancellationOptions = styled(RadioGroup)`
  paddingTop: 36;
  paddingHorizontal: 16;
  paddingBottom: 12;
`;

const View = styled.View``;

class BookingHistoryCancellationPage extends Component {
  state = {
    reason: '',
    otherReason: '',
    showMessageInput: false,
    showCancellationPolicyModal: false,
    showSpinner: false,
    otherReasonHeight: 100,
  };

  componentDidMount() {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('dark-content');
    } else {
      StatusBar.setBackgroundColor(color.black);
    }
    const {
      userActions,
      contentActions,
      cancellationPolicy,
      match: { params: { cancellationHash } },
    } = this.props;
    userActions.initiateCancellation(cancellationHash);
    if (!cancellationPolicy || !cancellationPolicy.description) {
      contentActions.getCancellationPolicy();
    }
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  onBackPress = () => {
    const { history } = this.props;
    history.goBack();
    return true;
  };

  onOtherReasonEntered = (otherReason) => {
    this.setState({ otherReason });
  };

  onContentSizeChange = (e) => {
    let { height } = e.nativeEvent.contentSize;
    height = height < 100 ? 100 : height;
    return this.setState({ otherReasonHeight: height });
  }

  getTextInputStyle = () => ({
    height: this.state.otherReasonHeight,
    marginHorizontal: 16,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: color.primary,
  })

  reasonChanged = (reason) => {
    const { bookingDetails: { cancellation: { reasons } } } = this.props;
    if (reason === reasons[reasons.length - 1].value) {
      this.setState({ reason, showMessageInput: true });
    } else {
      this.setState({ reason, showMessageInput: false });
    }
  };

  cancelBooking = () => {
    const {
      bookingDetails: { cancellation: { reasons } },
      match: { params: { cancellationHash } },
      bookingDetails: { bookingId },
      userActions,
      toastActions,
      history,
    } = this.props;
    const { reason, otherReason } = this.state;
    this.setState({ showSpinner: true });
    userActions.confirmCancellation(
      cancellationHash,
      bookingId,
      reason,
      reason === reasons[reasons.length - 1].value ? otherReason : '',
    ).then((res) => {
      this.setState({ showSpinner: false });
      if (res.error) {
        toastActions.showToast(res.payload.errors[0].message);
      } else {
        toastActions.showToast('Booking successfully cancelled');
        if (history.length > 3) {
          history.go(-2); // go to booking history
        } else {
          history.go(1 - history.length); // go to home
        }
      }
    }).catch(() => {
      this.setState({ showSpinner: false });
      toastActions.showToast('Something went wrong.Please try again later');
    });
  };

  isSubmittable = () => {
    const { bookingDetails: { cancellation: { reasons } } } = this.props;
    const { reason, otherReason } = this.state;
    if (!reason) {
      return false;
    }
    return reason !== reasons[reasons.length - 1].value || otherReason;
  };

  toggleCancellationPolicyModal = (showCancellationPolicyModal) => () => {
    this.setState({ showCancellationPolicyModal });
  };

  renderCancellationForm = () => {
    const { showMessageInput, otherReason } = this.state;
    const { bookingDetails: { cancellation } } = this.props;

    return (
      <FormContainer>
        <Spacer margin={[0, 2, 0, 2]}>
          <Text size="l" weight="bold">
            Refund will be processed if any
          </Text>
        </Spacer>
        <Spacer margin={[1.5, 2, 0, 2]}>
          <Text size="m" weight="bold" color="greyDark">
            Select a reason for your cancellation
          </Text>
        </Spacer>
        <Content>
          <CancellationOptions
            options={cancellation.reasons}
            setValue={this.reasonChanged}
          />
          {
            showMessageInput &&
            <TextInput
              value={otherReason}
              placeHolder="Leave a suggestion"
              validationError="Please enter a value"
              autoCapitalize="none"
              required
              multiline
              autoFocus
              numberOfLines={10}
              onChangeText={this.onOtherReasonEntered}
              style={this.getTextInputStyle}
              onContentSizeChange={this.onContentSizeChange}
            />
          }
          <Spacer
            padding={[1.5, 3, 1.5, 3]}
            margin={[2, 2, 2, 2]}
            style={{ backgroundColor: color.tertiary }}
          >
            <Text size="s" weight="normal" >
              {`Know more about our${' '}`}
              <Text
                size="s"
                weight="medium"
                color="blue"
                onPress={this.toggleCancellationPolicyModal(true)}
              >
                Cancellation Policy
              </Text>
            </Text>
          </Spacer>
          <View style={{ height: 120 }} />
        </Content>
        <ConfirmButtonContainer>
          <Button
            block
            disabled={!this.isSubmittable()}
            onPress={this.cancelBooking}
          >
            <Text size="m" weight="medium" color="white">CONFIRM</Text>
          </Button>
        </ConfirmButtonContainer>
      </FormContainer >
    );
  };

  renderCancellation = () => {
    const { bookingDetails: { cancellation } } = this.props;
    if (cancellation.status.toLowerCase() === 'reserved') {
      return this.renderCancellationForm();
    }
    return null;
  };

  render() {
    const { showCancellationPolicyModal, showSpinner } = this.state;
    const { cancellationPolicy, bookingDetails: { cancellation } } = this.props;
    return (
      <Container>
        <BackButtonHeader
          iconProps={{ name: 'close' }}
        />
        {
          !cancellation.status ? (
            <Loader />
          ) : (
              this.renderCancellation()
            )
        }
        {
          showSpinner ? (
            <Loader
              backgroundColor={color.transparent}
              loaderSize={50}
            />
          ) : null
        }
        <Modal
          isOpen={showCancellationPolicyModal}
          onClose={this.toggleCancellationPolicyModal(false)}
        >
          <CancellationPolicty calcellationPolicy={cancellationPolicy} />
        </Modal>
      </Container>
    );
  }
}

BookingHistoryCancellationPage.propTypes = {
  bookingDetails: PropTypes.object.isRequired,
  cancellationPolicy: PropTypes.object.isRequired,
  history: PropTypes.object,
  userActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
  toastActions: PropTypes.object.isRequired,
  match: PropTypes.object,
};

const mapStateToProps = (state, { match: { params } }) => ({
  bookingDetails: Object.values(state.user.bookingHistory.results)
    .find((booking) => booking.cancellation.hash === params.cancellationHash)
    || state.user.bookingHistory.results[0],
  cancellationPolicy: state.content.cancellationPolicy,
});

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
  toastActions: bindActionCreators(toastActionCreators, dispatch),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(BookingHistoryCancellationPage));
