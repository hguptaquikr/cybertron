import moment from 'moment';
import get from 'lodash/get';
import searchService from '../search/searchService';

export default {
  transformConfirmationApi({ data }) {
    // Fixme: Need to remove all the defaults from the hotel object
    return {
      booking: {
        guest: {
          name: data.user.name,
          mobile: data.user.contact_number,
          email: data.user.email,
        },
        room: {
          ...data.room,
          type: data.room.type.toLowerCase(),
          totalAdults: get(data, 'guest.adults', 0),
          totalChildren: get(data, 'guest.children', 0),
        },
        dates: searchService.formattedDatePicker({
          start: moment(data.date.checkin),
          end: moment(data.date.checkout),
        }),
        payment: {
          orderId: data.order_id,
          isPayAtHotel: data.pay_at_hotel,
          partialPaymentAmount: data.partial_payment_amount,
          partialPaymentLink: data.partial_payment_link,
        },
        hotelId: data.hotel.id,
      },
      hotel: {
        id: data.hotel.id,
        name: data.hotel.name,
        coordinates: data.hotel.coordinates,
        address: {
          locality: data.hotel.locality,
          street: data.hotel.street,
          city: data.hotel.city,
          pincode: data.hotel.pincode,
        },
        rooms: [],
        description: '',
        amenities: [],
        accessibilities: [],
        trilights: [],
        images: [],
        policies: data.hotel.hotel_policies,
      },
      price: {
        discountAmount: get(data.price.selected_rate_plan, 'price.total_discount', 0),
        sellingPrice: get(data.price.selected_rate_plan, 'price.sell_price', 0),
        tax: get(data.price.selected_rate_plan, 'price.tax', 0),
        pretaxPrice: get(data.price.selected_rate_plan, 'price.pretax_price', 0),
        code: get(data.price.selected_rate_plan, 'rate_plan.code', 0),
      },
    };
  },
};
