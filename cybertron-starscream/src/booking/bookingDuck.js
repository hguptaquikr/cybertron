import { handle } from 'redux-pack';
import bookingService from './bookingService';

const APPLY_REMOVE_COUPON = 'APPLY_REMOVE_COUPON';
const CHECKOUT_PAY_AT_HOTEL = 'CHECKOUT_PAY_AT_HOTEL';
const CHECKOUT_PAY_NOW = 'CHECKOUT_PAY_NOW';
const CONTINUE_DETAILS = 'CONTINUE_DETAILS';
const GET_CHECKOUT_DETAILS = 'GET_CHECKOUT_DETAILS';
const GET_CONFIRMATION_DETAILS = 'GET_CONFIRMATION_DETAILS';
const GET_HOTEL_DETAILS = 'GET_HOTEL_DETAILS';
const GET_PAY_NOW_DETAILS = 'GET_PAY_NOW_DETAILS';
const UPDATE_CHECKOUT_MOBILE = 'UPDATE_CHECKOUT_MOBILE';

const initialState = {
  guest: {
    name: '',
    mobile: '',
    email: '',
    organizationName: '',
    organizationTaxcode: '',
    organizationAddress: '',
  },
  room: {
    type: 'oak',
  },
  isPrepaidOnly: false,
  dates: {
    numberOfNights: {},
  },
  payment: {
    orderId: '',
    isPayAtHotel: '',
    partialPaymentAmount: '',
    partialPaymentLink: '',
    gatewayOrderId: '',
    serviceOrderId: '',
  },
  hotelId: 0,
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_HOTEL_DETAILS: return handle(state, action, {
      start: (s) => ({ ...s, ...payload }),
    });

    case GET_CONFIRMATION_DETAILS: return handle(state, action, {
      success: (s) => ({
        ...s,
        ...payload.booking,
      }),
    });

    case GET_CHECKOUT_DETAILS: return handle(state, action, {
      success: (s) => ({
        ...s,
        ...payload.booking,
      }),
    });

    case GET_PAY_NOW_DETAILS: return handle(state, action, {
      success: (s) => ({
        ...s,
        payment: {
          ...s.payment,
          ...payload,
        },
      }),
    });

    case CONTINUE_DETAILS:
      return {
        ...state,
        guest: {
          ...state.guest,
          ...payload.details,
        },
      };

    case CHECKOUT_PAY_AT_HOTEL: return handle(state, action, {
      success: (s) => ({
        ...s,
        payment: {
          ...s.payment,
          ...payload,
        },
      }),
    });

    case CHECKOUT_PAY_NOW: return handle(state, action, {
      success: (s) => ({
        ...s,
        payment: {
          ...s.payment,
          ...payload,
        },
      }),
    });

    case APPLY_REMOVE_COUPON: return handle(state, action, {
      success: (s) => ({ ...s, ...payload.booking }),
    });

    case UPDATE_CHECKOUT_MOBILE:
      return {
        ...state,
        guest: {
          ...state.guest,
          ...payload,
        },
      };

    default:
      return state;
  }
};

export const getConfirmationDetails = (bookingId) => (dispatch, getState, { api }) =>
  dispatch({
    type: GET_CONFIRMATION_DETAILS,
    promise: api.get(`/v5/checkout/confirmed-booking/${bookingId}/`)
      .then(bookingService.transformConfirmationApi),
  });
