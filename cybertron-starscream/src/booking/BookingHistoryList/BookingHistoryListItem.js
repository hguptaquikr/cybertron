import React from 'react';
import PropTypes from 'prop-types';
import { Linking } from 'react-native';
import { withRouter } from 'react-router-native';
import styled from 'styled-components/native';
import startCase from 'lodash/startCase';
import get from 'lodash/get';
import moment from 'moment';
import { Spacer, Flex } from 'leaf-ui/native';
import ImageLoad from '../../components/ImageLoad/ImageLoad';
import Text from '../../components/NewLeaf/Text/Text';
import color from '../../theme/color';
import { formatDate, imageResolutions } from '../../utils/utils';
import analyticsService from '../../analytics/analyticsService';

const TouchableWithoutFeedback = styled.TouchableWithoutFeedback``;

const Container = styled.View`
  borderBottomColor: ${color.darkGrey};
  borderBottomWidth: 2;
  backgroundColor: ${color.white};
  marginBottom: 8;
`;

const Row = styled.View`
  flexDirection: row;
`;

const Touchable = styled.TouchableOpacity``;

const HotelImage = styled(ImageLoad)`
  borderRadius: 0;
  aspectRatio: 2.5;
`;

const OverlayView = styled.View`
  width: auto;
  aspectRatio: 1.6;
  borderRadius: 0;
  backgroundColor: 'rgba(0, 0, 0, 0.6)';
`;

const BookingDetailContainer = styled.View`
  paddingTop: 24;
  paddingBottom: 16;
  paddingHorizontal: 16;
`;

const DatesContainer = Row.extend`
  marginTop: 20;
`;

const CheckinColumn = styled.View`
  marginRight: 24;
`;

const CheckoutColumn = styled.View``;

const PayNowContainer = styled.View`
  marginBottom: 16;
  marginHorizontal: 16;
  paddingVertical: 16;
  paddingHorizontal: 16;
  borderRadius: 2;
  backgroundColor: ${color.wildWatermelonOpacity12};
  flexDirection: row;
  justifyContent: space-between;
  alignItems: center;
`;

const BookingStatusView = styled.View`
  position: absolute;
  top: 0;
  left: 14;
  paddingVertical: 8;
  paddingHorizontal: 4;
  minWidth: 136;
  borderBottomLeftRadius: 2;
  borderBottomRightRadius: 2;
  justifyContent: center;
  alignItems: center;
  backgroundColor: ${({ bookingStatus }) => {
    if (bookingStatus === 'confirmed') {
      return color.blue;
    } else if (bookingStatus === 'temporary') {
      return color.wildWatermelon;
    }
    return color.secondary;
  }};
`;

const openMaps = (coordinates) => () => {
  const mapsUrl = `https://maps.google.com/?q=${get(coordinates, 'lat', 0)},${get(coordinates, 'lng', 0)}`;
  Linking.openURL(mapsUrl);
};

const payNow = (partPayLink) => () => {
  Linking.openURL(partPayLink);
};

const BookingHistoryItem = ({ history, bookingDetails }) => {
  const {
    status,
    id,
    hotel,
    dates,
    partPayAmount,
    partPayLink,
    isPastBooking,
  } = bookingDetails;

  const { coordinates } = hotel;

  let bookingStatus = 'cancelled';
  if (partPayLink && status.toLowerCase() === 'reserved') {
    bookingStatus = 'temporary';
  } else if (status.toLowerCase() === 'reserved') {
    bookingStatus = 'confirmed';
  }

  const hotelImageUrl = `${hotel.images[0]}?${imageResolutions.highres}${isPastBooking ? '&mono=868282' : ''}`;

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        requestAnimationFrame(() => {
          analyticsService.segmentTrackEvent('View Booking Details Clicked');
          history.push(`/account/booking-history/${id}/`);
        });
      }}
      testID="booking-history-list-item"
      accessibilityLabel="booking-history-list-item"
    >
      <Container>
        <HotelImage source={{ uri: hotelImageUrl }}>
          {
            isPastBooking ? (
              <OverlayView />
            ) : null
          }
        </HotelImage>
        {
          (bookingStatus !== 'confirmed' || !isPastBooking) && (
            <BookingStatusView bookingStatus={bookingStatus}>
              <Text size="s" color="white" family="medium" weight="medium">
                {
                  bookingStatus === 'temporary' ? (
                    'Payment Pending'
                  ) : (
                      startCase(`${bookingStatus} booking`)
                    )
                }
              </Text>
            </BookingStatusView>
          )
        }
        <BookingDetailContainer>
          <Row>
            <Flex flex={1}>
              <Spacer margin={[0, 2, 0, 0]}>
                <Text size="s" weight="bold">{hotel.name}</Text>
              </Spacer>
            </Flex>
            {
              !isPastBooking && coordinates.lat && coordinates.lng ? (
                <Touchable onPress={openMaps(coordinates)}>
                  <Text size="s" weight="bold" color="blue">
                    Get Directions
                  </Text>
                </Touchable>
              ) : null
            }
          </Row>
          <Text size="s" weight="normal" color="greyDark">
            {hotel.locality}
          </Text>
          <DatesContainer>
            <CheckinColumn>
              <Text size="s" weight="normal" color="greyDark" >
                Check in
              </Text>
              <Text size="s" color="greyDarker" weight="bold" >
                {formatDate(moment(dates.checkIn))}
              </Text>
            </CheckinColumn>
            <CheckoutColumn>
              <Text size="s" color="greyDark" weight="normal">
                Check out
              </Text>
              <Text size="s" color="greyDarker" weight="bold" >
                {formatDate(moment(dates.checkOut))}
              </Text>
            </CheckoutColumn>
          </DatesContainer>
        </BookingDetailContainer>
        {
          partPayAmount && !isPastBooking ? (
            <PayNowContainer>
              <Text size="s" weight="normal" color="red">
                {`Pay ₹${partPayAmount} to confirm booking`}
              </Text>
              <Touchable onPress={payNow(partPayLink)}>
                <Text size="s" weight="bold" color="blue">
                  Pay Now
                </Text>
              </Touchable>
            </PayNowContainer>
          ) : null
        }
      </Container>
    </TouchableWithoutFeedback>
  );
};

BookingHistoryItem.propTypes = {
  bookingDetails: PropTypes.object.isRequired,
  history: PropTypes.object,
};

export default withRouter(BookingHistoryItem);
