/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-native';
import styled from 'styled-components/native';
import { Spacer, Button } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';

const Container = styled.View`
  paddingVertical: 72;
  paddingHorizontal: 40;
  alignItems: center;
  justifyContent: center;
`;

const Image = styled.Image`
  width: auto;
  aspectRatio: 1.7
  marginHorizontal: 48;
`;

const EmptyBookingHistory = ({ history }) => (
  <Container>
    <Image
      source={require('../empty_state_no_booking_history.png')}
    />
    <Spacer margin={[6, 0, 0, 0]}>
      <Text size="l" weight="bold">No Bookings</Text>
    </Spacer>
    <Spacer margin={[2, 0, 3, 0]}>
      <Text size="s" weight="normal" color="greyLight">
        Looks like you don’t have any active bookings
      </Text>
    </Spacer>
    <Button
      kind="outlined"
      block
      onPress={() => history.go(1 - history.length)}
    >
      <Text size="m" weight="medium" color="green">START SEARCHING</Text>
    </Button>
  </Container>
);

EmptyBookingHistory.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(EmptyBookingHistory);
