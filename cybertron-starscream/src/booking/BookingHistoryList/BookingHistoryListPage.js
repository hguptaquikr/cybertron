import React, { Component } from 'react';
import { Platform, StatusBar, BackHandler } from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-native';
import { Spacer, theme } from 'leaf-ui/native';
import styled from 'styled-components/native';
import isEmpty from 'lodash/isEmpty';
import * as userActionCreators from '../../user/userDuck';
import SectionList from '../../components/SectionList/SectionList';
import Content from '../../components/Content/Content';
import BackButtonHeader from '../../components/Header/BackButtonHeader';
import Text from '../../components/NewLeaf/Text/Text';
import EmptyBookingHistory from './EmptyBookingHistory';
import BookingHistoryListItem from './BookingHistoryListItem';
import Loader from '../../components/Loader/Loader';
import color from '../../theme/color';
import { safeKeys } from '../../utils/utils';

const BookingsList = styled(SectionList)`
  flex: 1;
  paddingBottom: 24;
  backgroundColor: ${theme.color.greyLighter}
`;

const View = styled.View`
  flex: 1;
`;

class BookingHistoryListPage extends Component {
  state = {};

  componentDidMount() {
    const { userActions } = this.props;
    userActions.getBookingHistory();
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('dark-content');
    } else {
      StatusBar.setBackgroundColor(color.black);
    }
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  onBackPress = () => {
    const { history } = this.props;
    history.goBack();
    return true;
  };

  getBookingSections = () => {
    const bookingSections = [];
    const {
      bookingHistory: {
        upcomingSortOrder,
        pastSortOrder,
      },
    } = this.props;
    if (upcomingSortOrder.length > 0) {
      bookingSections.push({
        data: upcomingSortOrder,
        title: 'Upcoming Bookings',
      });
    }
    if (pastSortOrder.length > 0) {
      bookingSections.push({
        data: pastSortOrder,
        title: 'Previous Bookings',
      });
    }

    return bookingSections;
  };

  renderBookingHistoryList = () => {
    const {
      bookingHistory: { results },
    } = this.props;

    if (isEmpty(safeKeys(results))) {
      return <EmptyBookingHistory />;
    }
    return (
      <BookingsList
        sections={this.getBookingSections()}
        keyExtractor={(item) => item}
        lazy={3}
        renderItem={({ item }) => (
          <BookingHistoryListItem
            key={item}
            bookingDetails={results[item]}
          />
        )}
        renderSectionHeader={({ section }) => (
          <Spacer padding={[2.5, 2, 2.5, 2]}>
            <Text>{section.title}</Text>
          </Spacer>
        )}
      />
    );
  };

  render() {
    const {
      bookingHistory: {
        isBookingHistoryLoading,
      },
    } = this.props;

    return (
      <View>
        <BackButtonHeader middle="Booking History" />
        {
          isBookingHistoryLoading ? (
            <Loader />
          ) : (
            <Content>
              {
                this.renderBookingHistoryList()
              }
            </Content>
          )
        }
      </View>
    );
  }
}

BookingHistoryListPage.propTypes = {
  bookingHistory: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired,
  history: PropTypes.object,
};

const mapStateToProps = (state) => ({
  bookingHistory: state.user.bookingHistory,
});

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(BookingHistoryListPage));
