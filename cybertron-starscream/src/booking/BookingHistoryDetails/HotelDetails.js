/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import { Linking } from 'react-native';
import styled from 'styled-components/native';
import { Spacer, Flex } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';
import Section from './Section';
import color from '../../theme/color';
import { openMaps } from '../../utils/utils';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const TouchableOpacity = styled.TouchableOpacity`
  paddingHorizontal: 4;
  paddingVertical: 4;
`;

const Container = styled.View`
  paddingHorizontal: 16;
  paddingVertical: 24;
`;

const Row = styled.View`
  flexDirection: row;
  marginBottom: 8;
  alignItems: center;
`;

const contactHotel = (onContactHotel, hotelContact) => () => {
  if (onContactHotel) {
    onContactHotel();
  }
  const url = `tel:${hotelContact}`;
  Linking.canOpenURL(url).then((supported) => {
    if (supported) {
      return Linking.openURL(url).catch(() => null);
    }
    return Promise.reject('Supporting app not present');
  });
};

const viewDirections = (onViewDirections, lat, lng) => () => {
  openMaps(lat, lng)();
  if (onViewDirections) {
    onViewDirections();
  }
};

const HotelDetails = ({ hotel, onContactHotel, onViewDirections }) => (
  <Section>
    <Container>
      <Row>
        <Flex flex={1}>
          <Spacer margin={[0, 1, 0, 0]}>
            <Text size="m" family="medium" weight="medium">{hotel.name}</Text>
          </Spacer>
        </Flex>
        <TouchableOpacity onPress={contactHotel(onContactHotel, hotel.contact)}>
          {getIcon('call', color.primary)}
        </TouchableOpacity>
      </Row>
      <Spacer margin={[0, 5, 3, 0]}>
        <Text size="s" weight="normal" color="greyDark">{hotel.address}</Text>
      </Spacer>
      <TouchableOpacity
        onPress={viewDirections(onViewDirections, hotel.coordinates.lat, hotel.coordinates.lng)}
      >
        <Row>
          <Spacer margin={[0, 0.5, 0, 0]}>
            <Text size="s" family="normal" weight="normal" color="blue">View Direction</Text>
          </Spacer>
          {getIcon('arrow-next', color.blue)}
        </Row>
      </TouchableOpacity>
    </Container>
  </Section>
);

HotelDetails.propTypes = {
  hotel: PropTypes.object,
  onContactHotel: PropTypes.func.isRequired,
  onViewDirections: PropTypes.func.isRequired,
};

export default HotelDetails;
