import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import Text from '../../components/NewLeaf/Text/Text';
import Price from '../../components/NewLeaf/Price/Price';
import RatePlanTypeView from '../../price/RatePlanTypeView/RatePlanTypeView';
import Section from './Section';

const Container = styled.View`
  paddingHorizontal: 16;
  paddingVertical: 24;
`;

const TotalPayablePriceContainer = styled.View`
  alignItems: flex-end;
`;

const Item = styled.View`
  flexDirection: row;
  justifyContent: space-between;
  alignItems: center;
  marginBottom: 20;
`;

const View = styled.View``;

const PriceDetails = ({
  basePrice,
  discount,
  tax,
  amountPaid,
  payableAmount,
  ratePlanTag,
}) => (
  <Section>
    <Container>
      <Item>
        <Text size="s" weight="normal" color="greyDarker">
          Room Tariff
        </Text>
        <Price price={basePrice} color="greyDarker" size="s" type="regular" />
      </Item>
      {
        discount > 0 ? (
          <Item>
            <Text size="s" weight="normal" color="greyDarker">
              Room Discount
            </Text>
            <Price price={discount} size="s" color="green" type="regular" negative />
          </Item>
        ) : null
      }
      {
        tax ? (
          <Item>
            <Text size="s" weight="normal" color="greyDarker">
              Taxes
            </Text>
            <Price price={tax} size="s" color="greyDarker" type="regular" />
          </Item>
        ) : null
      }
      {
        amountPaid > 0 ? (
          <Item>
            <Text size="s" weight="normal" color="greyDarker">
              Amount Paid
            </Text>
            <Price price={amountPaid} size="s" color="greyDarker" type="regular" />
          </Item>
        ) : null
      }
      <Item>
        <View>
          <Text size="s" weight="normal" color="greyDarker">
            Total Payable
          </Text>
          <Text size="xs" weight="normal" color="grey">
            inc. of all taxes
          </Text>
        </View>
        <TotalPayablePriceContainer>
          <Price
            price={payableAmount}
            bold
            size="l"
            color="greyDarker"
            type="regular"
          />
          <RatePlanTypeView ratePlanTag={ratePlanTag} />
        </TotalPayablePriceContainer>
      </Item>
    </Container>
  </Section>
);

PriceDetails.propTypes = {
  basePrice: PropTypes.number.isRequired,
  discount: PropTypes.number,
  tax: PropTypes.number,
  amountPaid: PropTypes.number,
  payableAmount: PropTypes.number.isRequired,
  ratePlanTag: PropTypes.string.isRequired,
};

export default PriceDetails;
