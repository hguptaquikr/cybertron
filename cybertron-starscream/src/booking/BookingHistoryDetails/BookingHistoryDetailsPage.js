import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Linking, Share, Platform, StatusBar, BackHandler } from 'react-native';
import { Button } from 'leaf-ui/native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-native';
import styled from 'styled-components/native';
import startCase from 'lodash/startCase';
import * as userActionCreators from '../../user/userDuck';
import * as contentActionCreators from '../../content/contentDuck';
import ImageLoad from '../../components/ImageLoad/ImageLoad';
import BackButtonHeader from '../../components/Header/BackButtonHeader';
import CancelBookingOption from './CancelBookingOption';
import Content from '../../components/Content/Content';
import Container from '../../components/Container/Container';
import Text from '../../components/NewLeaf/Text/Text';
import CustomerCare from './CustomerCare';
import HotelDetails from './HotelDetails';
import BookingDetails from './BookingDetails';
import PriceDetails from './PriceDetails';
import SpecialRateHotelsBanner from '../../checkout/SpecialRateHotelsBanner/SpecialRateHotelsBanner';
import color from '../../theme/color';
import config from '../../config';
import analyticsService from '../../analytics/analyticsService';
import { imageResolutions } from '../../utils/utils';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const HotelImage = styled(ImageLoad)`
  borderRadius: 0;
  aspectRatio: 2.5;
`;

const BookingStatusView = styled.View`
  position: absolute;
  top: 0;
  left: 14;
  paddingVertical: 8;
  paddingHorizontal: 4;
  minWidth: 136;
  borderBottomLeftRadius: 2;
  borderBottomRightRadius: 2;
  justifyContent: center;
  alignItems: center;
  backgroundColor: ${({ bookingStatus }) => {
    if (bookingStatus === 'confirmed') {
      return color.blue;
    } else if (bookingStatus === 'temporary') {
      return color.wildWatermelon;
    }
    return color.secondary;
  }};
`;

const PayNowContainer = styled.View`
  backgroundColor: ${color.white};
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 8px;
`;

const BannerContainer = styled.View`
  marginLeft: 8;
  marginRight: 8;
  marginBottom: 8;
`;

const contentStyle = {
  backgroundColor: color.disabledGreyOpacity50,
  marginBottom: 40,
};

class BookingHistoryDetailsPage extends Component {
  componentDidMount() {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('dark-content');
    } else {
      StatusBar.setBackgroundColor(color.black);
    }
    const { userActions, contentActions, cancellationPolicy } = this.props;
    userActions.getBookingHistory();
    if (!cancellationPolicy || !cancellationPolicy.description) {
      contentActions.getCancellationPolicy();
    }
    analyticsService.segmentScreenViewed('Booking Details Page Viewed', this.getEventProps());
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress');
  }

  onBackPress = () => {
    const { history } = this.props;
    history.goBack();
    return true;
  };

  onContactHotel = () => {
    analyticsService.segmentTrackEvent('Call Hotel Clicked', this.getEventProps());
  };

  onViewDirections = () => {
    analyticsService.segmentTrackEvent('Get Directions Clicked', this.getEventProps());
  };

  onCancelBookingClicked = () => {
    analyticsService.segmentTrackEvent('Cancel Booking Clicked', this.getEventProps());
  };

  getHeaderRightIcon = () => (
    getIcon('share')
  );

  getEventProps = () => {
    const {
      bookingDetails: {
        id,
        amountPaid,
        partPayLink,
        partPayAmount,
        status,
        price: {
          totalPrice,
          memberDiscountPrice,
          pendingAmount,
        },
        coupon: { code },
        dates: {
          checkIn,
          checkOut,
        },
        hotel: {
          name,
        },
      },
    } = this.props;

    let bookingStatus = 'cancelled';
    if (partPayLink && status.toLowerCase() === 'reserved') {
      bookingStatus = 'temporary';
    } else if (status.toLowerCase() === 'reserved') {
      bookingStatus = 'confirmed';
    }

    return {
      order_id: id,
      checkin_date: checkIn,
      checkout_date: checkOut,
      hotel_name: name,
      price: totalPrice,
      payment_mode: pendingAmount > 0 ? 'PAH' : 'Prepaid', // TODO: check this logic
      booking_status: bookingStatus,
      amount_paid: amountPaid,
      amount_pending: pendingAmount,
      coupon: code,
      member_discount_perc: '', // TODO: get it from backend
      member_discount_amount: memberDiscountPrice,
      part_pay_pending: partPayLink && partPayAmount > 0,
    };
  };

  shareDetails = () => {
    const {
      history: { location: { pathname } },
      bookingDetails: {
        dates: {
          checkIn,
          checkOut,
        },
        hotel: {
          name,
          coordinates: {
            lat,
            lng,
          },
        },
      },
    } = this.props;
    const url = `${config.baseUrl}${pathname}`;
    const mapsUrl = `https://maps.google.com/?q=${lat},${lng}`;
    const content = {
      message: `Hi, Here's my Treebo Hotel booking info -
${url} Checkin Date: ${checkIn} Checkout Date: ${checkOut} Hotel Name: ${name} Location: ${mapsUrl}`,
      url,
      title: `Checkout ${name}`,
      dialogTitle: `Checkout ${name}`,
    };
    Share.share(content);
  };

  payNow = () => {
    const { bookingDetails: { partPayLink } } = this.props;
    Linking.openURL(partPayLink);
    analyticsService.segmentTrackEvent('Part Pay Clicked');
  };

  render() {
    const {
      bookingDetails: {
        id,
        discount,
        amountPaid,
        partPayLink,
        partPayAmount,
        isPastBooking,
        status,
        hotel,
        dates,
        price: {
          basePrice,
          tax,
          pendingAmount,
          memberDiscountPrice,
        },
        roomConfig,
        guest,
        cancellation,
        ratePlan: { tag },
      },
      cancellationPolicy,
    } = this.props;

    let bookingStatus = 'cancelled';
    if (partPayLink && status.toLowerCase() === 'reserved') {
      bookingStatus = 'temporary';
    } else if (status.toLowerCase() === 'reserved') {
      bookingStatus = 'confirmed';
    }

    return (
      <Container>
        <BackButtonHeader
          middle={
            <Text size="m" weight="normal" color="grey">
              Booking ID:
              <Text size="m" weight="normal" color="greyDarker">
                {` ${id}`}
              </Text>
            </Text>
          }
          right={this.getHeaderRightIcon()}
          onRightPress={this.shareDetails}
          backgroundColor="white"
        />

        <Content style={contentStyle}>
          <HotelImage source={{ uri: `${hotel.images[0]}?${imageResolutions.highres}` }} />
          {
              (bookingStatus !== 'confirmed' || !isPastBooking) &&
                <BookingStatusView bookingStatus={bookingStatus}>
                  <Text size="s" color="white" weight="medium" family="medium">
                    {
                      bookingStatus === 'temporary' ? (
                        'Payment Pending'
                      ) : (
                        startCase(`${bookingStatus} booking`)
                      )
                    }
                  </Text>
                </BookingStatusView>
          }
          <HotelDetails
            hotel={hotel}
            onContactHotel={this.onContactHotel}
            onViewDirections={this.onViewDirections}
          />
          <BookingDetails
            dates={dates}
            guest={guest}
            roomConfig={roomConfig}
          />
          {
            bookingStatus === 'confirmed' || !isPastBooking ? (
              <BannerContainer>
                <SpecialRateHotelsBanner
                  hotelID={hotel.id}
                  range={dates}
                  marginTop={0}
                />
              </BannerContainer>
            ) : null
          }
          <PriceDetails
            basePrice={+basePrice}
            discount={+discount + +memberDiscountPrice}
            tax={+tax}
            amountPaid={+amountPaid}
            payableAmount={+pendingAmount}
            ratePlanTag={tag}
          />
          <CustomerCare />
          {
            !isPastBooking && bookingStatus !== 'cancelled' && cancellation.hash !== 'None' ? (
              <CancelBookingOption
                cancellationHash={cancellation.hash}
                cancellationPolicy={cancellationPolicy}
                onCancelBookingClicked={this.onCancelBookingClicked}
              />
            ) : null
          }
        </Content>
        {
          partPayAmount && !isPastBooking ? (
            <PayNowContainer>
              <Button block onPress={this.payNow}>
                <Text size="s" weight="medium" family="medium" color="white">
                  {`Pay ₹${partPayAmount} to confirm booking`}
                </Text>
              </Button>
            </PayNowContainer>
          ) : null
        }
      </Container>
    );
  }
}

BookingHistoryDetailsPage.propTypes = {
  bookingDetails: PropTypes.object.isRequired,
  cancellationPolicy: PropTypes.object.isRequired,
  history: PropTypes.object,
  userActions: PropTypes.object.isRequired,
  contentActions: PropTypes.object.isRequired,
};

const mapStateToProps = (state, { match: { params } }) => ({
  bookingDetails:
  state.user.bookingHistory.results[params.bookingId]
  || state.user.bookingHistory.results[0],
  cancellationPolicy: state.content.cancellationPolicy,
});

const mapDispatchToProps = (dispatch) => ({
  userActions: bindActionCreators(userActionCreators, dispatch),
  contentActions: bindActionCreators(contentActionCreators, dispatch),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(BookingHistoryDetailsPage));
