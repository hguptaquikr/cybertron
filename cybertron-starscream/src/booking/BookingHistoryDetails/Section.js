import styled from 'styled-components/native';
import color from '../../theme/color';

const Section = styled.View`
  backgroundColor: ${color.white};
  marginBottom: 8;
`;

export default Section;
