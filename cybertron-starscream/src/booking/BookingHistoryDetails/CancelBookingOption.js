import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { withRouter } from 'react-router-native';
import { Spacer, Button } from 'leaf-ui/native';
import Modal from '../../components/Modal/Modal';
import Text from '../../components/NewLeaf/Text/Text';
import Section from './Section';
import CancellationPolicty from './CancellationPolicy';

const CancelBookingContainer = styled.View`
  paddingHorizontal: 8;
  paddingVertical: 24;
`;

class CancelBookingOption extends Component {
  state = {
    showCancellationPolicyModal: false,
  };

  cancelBooking = () => {
    const { cancellationHash, history, onCancelBookingClicked } = this.props;
    if (onCancelBookingClicked) {
      onCancelBookingClicked();
    }
    history.push(`/c-${cancellationHash}`);
  };

  toggelCancellationPolicyModal = (showCancellationPolicyModal) => () => {
    this.setState({ showCancellationPolicyModal });
  };

  render() {
    const { showCancellationPolicyModal } = this.state;
    const { cancellationPolicy } = this.props;

    return (
      <Section>
        <CancelBookingContainer>
          <Spacer margin={[0, 1, 3, 1]}>
            <Text size="s" weight="medium" family="medium" >
              {`Know more about our${' '}`}
              <Text
                size="s"
                family="medium"
                weight="medium"
                color="blue"
                onPress={this.toggelCancellationPolicyModal(true)}
              >
                Cancellation Policy
              </Text>
            </Text>
          </Spacer>
          <Button
            kind="outlined"
            block
            onPress={this.cancelBooking}
          >
            <Text size="m" weight="medium" family="medium" color="green">CANCEL BOOKING</Text>
          </Button>
        </CancelBookingContainer>
        <Modal
          isOpen={showCancellationPolicyModal}
          onClose={this.toggelCancellationPolicyModal(false)}
        >
          <CancellationPolicty calcellationPolicy={cancellationPolicy} />
        </Modal>
      </Section >
    );
  }
}

CancelBookingOption.propTypes = {
  cancellationHash: PropTypes.string.isRequired,
  cancellationPolicy: PropTypes.object.isRequired,
  onCancelBookingClicked: PropTypes.func.isRequired,
  history: PropTypes.object,
};

export default withRouter(CancelBookingOption);
