import React from 'react';
import { Linking } from 'react-native';
import styled from 'styled-components/native';
import Text from '../../components/NewLeaf/Text/Text';
import Section from './Section';
import config from '../../config';

const Container = styled.View`
  paddingHorizontal: 16;
  paddingTop: 24;
  paddingBottom: 16;
`;
const Touchable = styled.TouchableOpacity`
  paddingVertical: 8;
`;

const contactCustomerCare = () => {
  const url = `tel:${config.customerCareNumber}`;
  Linking.canOpenURL(url).then((supported) => {
    if (supported) {
      return Linking.openURL(url).catch(() => null);
    }
    return Promise.reject('Supporting app not present');
  });
};

const CustomerCare = () => (
  <Section>
    <Container>
      <Text size="s" weight="normal" color="greyDarker">
        For queries regarding booking & cancellation
      </Text>
      <Touchable onPress={contactCustomerCare}>
        <Text
          size="s"
          family="medium"
          weight="medium"
          color="blue"
        >
          Contact Customer Care
        </Text>
      </Touchable>
    </Container>
  </Section>
);

export default CustomerCare;
