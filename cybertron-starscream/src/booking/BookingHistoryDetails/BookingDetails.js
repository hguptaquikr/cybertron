import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import moment from 'moment';
import Text from '../../components/NewLeaf/Text/Text';
import { pluralize, formatDate } from '../../utils/utils';
import Section from './Section';
import color from '../../theme/color';
import { getIcon } from '../../utils/ComponentUtils/ComponentUtils';

const Row = styled.View`
  flexDirection: row;
  marginBottom: ${({ marginBottom }) => marginBottom || 8};
`;

const Container = styled.View`
  paddingHorizontal: 16;
  paddingVertical: 24;
`;

const FlexChild = styled.View`
  flex: 1;
`;

const DurationView = styled.View`
  flex: 1;
  alignItems: center;
`;

const BookingDetailsCard = ({
  dates,
  guest,
  roomConfig: {
    adults,
    kids,
    roomCount,
    roomType,
  },
}) => (
  <Section>
    <Container>
      <Row>
        <FlexChild>
          <Text size="xs" weight="normal" color="greyDark">Check In</Text>
        </FlexChild>
        <DurationView>
          {getIcon('time', color.disabledGrey)}
        </DurationView>
        <FlexChild>
          <Text size="xs" weight="normal" color="greyDark">Check Out</Text>
        </FlexChild>
      </Row>
      <Row marginBottom={28}>
        <FlexChild>
          <Text size="s" color="greyDarker" weight="medium" family="medium">{formatDate(moment(dates.checkIn))}</Text>
        </FlexChild>
        <DurationView>
          <Text size="xs" weight="normal" color="grey">
            {pluralize(dates.durationOfStay, 'Night', 's', true)}
          </Text>
        </DurationView>
        <FlexChild>
          <Text size="s" color="greyDarker" weight="medium" family="medium">{formatDate(moment(dates.checkOut))}</Text>
        </FlexChild>
      </Row>
      <Row>
        <Text size="xs" weight="normal" color="greyDark">Primary Traveller</Text>
      </Row>
      <Row marginBottom={28}>
        <Text size="s" color="greyDarker" weight="medium" family="medium">{guest.name}</Text>
      </Row>
      <Row>
        <FlexChild>
          <Text size="xs" weight="normal" color="greyDark">Guest</Text>
        </FlexChild>
        <FlexChild>
          <Text size="xs" weight="normal" color="greyDark">Room</Text>
        </FlexChild>
      </Row>
      <Row>
        <FlexChild>
          <Text size="s" color="greyDarker" weight="medium" family="medium">{adults} Adult, {kids} Kid</Text>
        </FlexChild>
        <FlexChild>
          <Text size="s" color="greyDarker" weight="medium" family="medium">{roomCount} x {roomType}</Text>
        </FlexChild>
      </Row>
    </Container>
  </Section>
);

BookingDetailsCard.propTypes = {
  dates: PropTypes.object.isRequired,
  guest: PropTypes.object.isRequired,
  roomConfig: PropTypes.object.isRequired,
};

export default BookingDetailsCard;
