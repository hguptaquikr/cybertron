import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { Spacer } from 'leaf-ui/native';
import Text from '../../components/NewLeaf/Text/Text';

const Container = styled.View`
  paddingHorizontal: 16;
  paddingVertical: 32;
`;

const CancellationPolicy = ({ calcellationPolicy }) => (
  <Container>
    <Text size="l" color="greyDarker" weight="bold">
      {calcellationPolicy.title}
    </Text>
    <Spacer margin={[2, 0, 0, 0]}>
      <Text size="s" weight="normal">
        {calcellationPolicy.description}
      </Text>
    </Spacer>
  </Container>
);

CancellationPolicy.propTypes = {
  calcellationPolicy: PropTypes.object.isRequired,
};

export default CancellationPolicy;
