import React from 'react';
import PropTypes from 'prop-types';
import {
  WebView,
  ActivityIndicator,
  Dimensions,
  ScrollView,
  Platform,
} from 'react-native';
import { withRouter } from 'react-router-native';
import { theme } from 'leaf-ui/native';

class BrowserPage extends React.Component {
  renderLoading = () => (
    <ActivityIndicator
      size="large"
      color={theme.color.green}
      style={{ marginTop: 40 }}
    />
  )

  render() {
    const { history } = this.props;

    return (
      <ScrollView
        style={{
          height: Dimensions.get('window').height,
          width: Dimensions.get('window').width,
        }}
      >
        <WebView
          source={{ uri: `https://www.treebo.com${history.location.pathname}` }}
          domStorageEnabled
          javaScriptEnabled
          scalesPageToFit
          startInLoadingState
          mixedContentMode="always"
          userAgent={Platform.OS}
          renderLoading={this.renderLoading}
          style={{
            height: Dimensions.get('window').height,
            width: Dimensions.get('window').width,
            zIndex: 10,
          }}
          onError={() => history.goBack()}
        />
      </ScrollView>
    );
  }
}

BrowserPage.propTypes = {
  history: PropTypes.object,
};

export default withRouter(BrowserPage);
