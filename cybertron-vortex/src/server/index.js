const express = require('express');
const expressHandlebars = require('express-handlebars');
const bodyParser = require('body-parser');

const app = express();

app.engine('.html', expressHandlebars({
  extname: '.html',
}));
app.set('view engine', '.html');
app.set('views', 'build');

app.use(bodyParser.json());

app.get('/api/health/', (req, res) => {
  res.send({ vortex: true });
});

app.get('/api/v1/generate/', (req, res) => {
  res.render(req.query.template);
});

app.post('/api/v1/generate/', (req, res) => {
  res.render(req.body.template, req.body.context);
});

app.listen(1447, () => {
  // eslint-disable-next-line
  console.info(`cybertron-vortex is running on port 1447`);
});
