import mockBookings from './mockBookings';

const typeDefs = `
  extend type Query {
    bookings(
      hotelId: String,
      fromDate: String,
      toDate: String,
      searchText: String,
      searchField: String,
      bookingStatus: String,
    ): [Booking]!

    bookingById(
      bookingId: String!,
    ): Booking!
  }
`;

const resolvers = {
  Query: {
    bookings: (root, args) => {
      if (args.hotelId === '0019395') {
        return [mockBookings.B1];
      }
      return [];
    },
  },
};

const mocks = {
  // Query: () => ({
  //   bookings: () => {},
  // }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
