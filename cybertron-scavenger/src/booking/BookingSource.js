import casual from 'casual';

const typeDefs = `
  type BookingSource {
    channelCode: String!
    subChannelCode: String!
    applicationCode: String!
  }
`;

const resolvers = {
  BookingSource: {
    applicationCode: (bookingSource) => bookingSource.application_code,
    channelCode: (bookingSource) => bookingSource.channel_code,
    subChannelCode: (bookingSource) => bookingSource.subchannel_code,
  },
};

const mocks = {
  BookingSource: () => ({
    applicationCode: () => casual.word,
    channelCode: () => casual.word,
    subChannelCode: () => casual.word,
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
