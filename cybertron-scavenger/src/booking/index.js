import deepMerge from 'cybertron-utils/deepMerge';
import Booking from './Booking';
import BookingSource from './BookingSource';
import bookings from './bookings';
import makeModel from './makeModel';

const typeDefs = `
  ${Booking.typeDefs}
  ${BookingSource.typeDefs}
  ${bookings.typeDefs}
`;

const resolvers = deepMerge(
  Booking.resolvers,
  BookingSource.resolvers,
  bookings.resolvers,
);

const mocks = deepMerge(
  Booking.mocks,
  BookingSource.mocks,
  bookings.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
  makeModel,
};
