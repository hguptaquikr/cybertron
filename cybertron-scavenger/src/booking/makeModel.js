import mockBookings from './mockBookings';

const makeModel = (request) => {
  const bookings = (query) =>
    request.get('http://', query);

  const bookingsByHotelId = ({ hotelId }) => {
    // request.get('http://');
    if (hotelId === '0019395') {
      return Promise.resolve([
        mockBookings.B1,
        mockBookings.B2,
      ]);
    }
    return [null];
  };

  return {
    bookings,
    bookingsByHotelId,
  };
};

export default makeModel;
