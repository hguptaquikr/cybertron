import casual from 'casual';
import { MockList } from 'graphql-tools';

const typeDefs = `
  type Booking {
    id: ID!
    referenceNumber: String!
    hotel: Hotel!
    bill: Bill!
    checkIn: String!
    checkOut: String!
    comments: String!
    extraInfo: JSON
    guests: [Guest!]!
    roomStays: [RoomStay!]!
    source: BookingSource!
    status: String!
    versionId: String!
  }
`;

const resolvers = {
  Booking: {
    id: (booking) => booking.booking_id,
    referenceNumber: (booking) => booking.reference_number,
    // hotel: (booking) => {},
    // bill: (booking) => {},
    checkIn: (booking) => booking.checkin_date,
    checkOut: (booking) => booking.checkout_date,
    comments: (booking) => booking.comments,
    extraInfo: (booking) => booking.extra_information,
    guests: () => new MockList([1, 3]),
    roomStays: (booking) => booking.room_stays,
    source: (booking) => booking.source,
    status: (booking) => booking.status,
    versionId: (booking) => booking.version_id,
  },
};

const mocks = {
  Booking: () => ({
    // id: () => `TRB-${casual.integer(0, 99999)}`,
    id: () => 'B1',
    checkIn: () => casual.date('YYYY-MM-DD'),
    checkOut: () => casual.date('YYYY-MM-DD'),
    comments: () => casual.sentence,
    guests: () => new MockList([1, 3]),
    referenceNumber: () => `TRB-${casual.integer(0, 99999)}`,
    roomStays: () => new MockList([1, 3]),
    status: () => casual.random_element([
      'CHECKED-IN',
      'CREATED',
      'RESERVED',
      'NO-SHOW',
      'CHECKED-OUT',
      'CANCELED',
    ]),
    versionId: () => casual.integer(1, 5),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
