const typeDefs = `
  input CreateBookingInput {
    status: String
  }

  extend type Mutation {
    createBooking(
      booking: CreateBookingInput!,
    ): Booking!
  }
`;

const resolvers = {
  Mutation: {
    createBooking: (parent, args) =>
      Promise.resolve({
        id: 'trb-345',
        status: args.booking.status,
      }),
  },
};

export default {
  typeDefs,
  resolvers,
};
