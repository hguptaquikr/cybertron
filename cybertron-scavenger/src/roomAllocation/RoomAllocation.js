import casual from 'casual';

const typeDefs = `
  type RoomAllocation {
    id: ID!
    assignedBy: String!
    checkIn: String!
    checkOut: String!
    roomId: String!
    roomTypeId: String!
  }
`;

const resolvers = {
  RoomAllocation: {
    id: (roomAllocation) => roomAllocation.id,
    assignedBy: (roomAllocation) => roomAllocation.assigned_by,
    checkIn: (roomAllocation) => roomAllocation.checkin_date,
    checkOut: (roomAllocation) => roomAllocation.checkout_date,
    roomId: (roomAllocaiton) => roomAllocaiton.room_id,
    roomTypeId: (roomAllocaiton) => roomAllocaiton.room_type_id,
  },
};

const mocks = {
  RoomAllocation: () => ({
    id: () => casual.integer(1, 99999),
    checkIn: () => casual.date('YYYY-MM-DD'),
    checkOut: () => casual.date('YYYY-MM-DD'),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
