import deepMerge from 'cybertron-utils/deepMerge';
import RoomAllocation from './RoomAllocation';
// import roomStays from './roomStays';

const typeDefs = `
  ${RoomAllocation.typeDefs}
`;
// ${roomStays.typeDefs}

const resolvers = deepMerge(
  RoomAllocation.resolvers,
  // roomStays.resolvers,
);

const mocks = deepMerge(
  RoomAllocation.mocks,
  // roomStays.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
};
