import casual from 'casual';

const typeDefs = `
  type Invoice {
    id: ID!
    billTo: String!
    date: String!
    dueDate: String!
    number: String!
    postTax: Float!
    preTax: Float!
    tax: Float!
    status: Int!
    url: Int!
  }
`;

const resolvers = {
  Invoice: {
    id: (invoice) => invoice.invoice_id,
    billTo: (invoice) => invoice.bill_to,
    date: (invoice) => invoice.invoice_date,
    dueDate: (invoice) => invoice.due_date,
    number: (invoice) => invoice.invoice_no,
    postTax: (invoice) => invoice.posttax_amount,
    preTax: (invoice) => invoice.pretax_amount,
    tax: (invoice) => invoice.tax_amount,
    status: (invoice) => invoice.status,
    url: (invoice) => invoice.url,
  },
};

const mocks = {
  Invoice: () => ({
    id: () => casual.integer(1, 99999),
    billTo: () => casual.word,
    date: () => casual.date('YYYY-MM-DD'),
    dueDate: () => casual.date('YYYY-MM-DD'),
    number: () => casual.word,
    postTax: () => casual.double(2000, 5000),
    preTax: () => casual.double(2500, 5500),
    tax: () => casual.double(0, 500),
    status: () => casual.status,
    url: () => casual.url,
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
