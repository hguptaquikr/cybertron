import deepMerge from 'cybertron-utils/deepMerge';
import Invoice from './Invoice';

const typeDefs = `
  ${Invoice.typeDefs}
`;

const resolvers = deepMerge(
  Invoice.resolvers,
);

const mocks = deepMerge(
  Invoice.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
};
