import casual from 'casual';
import { MockList } from 'graphql-tools';

const typeDefs = `
  type Bill {
    id: ID!
    charges: [Charge!]!
    payments: [Payment!]!
    invoices: [Invoice!]!
  }
`;

const resolvers = {
  Bill: {
    id: (bill) => bill.id,
    charges: (bill) => bill.charges,
    payments: (bill) => bill.payments,
    invoices: (bill) => bill.invoices,
  },
};

const mocks = {
  Bill: () => ({
    id: () => casual.integer(1, 99999),
    charges: () => new MockList([1, 10]),
    payments: () => new MockList([1, 4]),
    invoices: () => new MockList([1, 2]),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
