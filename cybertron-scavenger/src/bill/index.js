import deepMerge from 'cybertron-utils/deepMerge';
import Bill from './Bill';

const typeDefs = `
  ${Bill.typeDefs}
`;

const resolvers = deepMerge(
  Bill.resolvers,
);

const mocks = deepMerge(
  Bill.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
};
