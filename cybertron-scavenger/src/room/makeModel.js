const makeModel = (request) => {
  const roomsByHotelId = ({ hotelId }) =>
    request.get(`http://staging.treebohotels.com/cataloging-service/api/v1/properties/${hotelId}/rooms/`);

  const filterByRoomTypeName = ({
    rooms,
    roomTypeName,
  }) => rooms.filter((room) => room.room_type.type === roomTypeName);

  return {
    roomsByHotelId,
    filterByRoomTypeName,
  };
};

export default makeModel;
