import deepMerge from 'cybertron-utils/deepMerge';
import Room from './Room';
import makeModel from './makeModel';

const typeDefs = `
  ${Room.typeDefs}
`;

const resolvers = deepMerge(
  Room.resolvers,
);

const mocks = deepMerge(
  Room.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
  makeModel,
};
