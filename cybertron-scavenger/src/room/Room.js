import casual from 'casual';
import { MockList } from 'graphql-tools';

const typeDefs = `
  type Room {
    id: ID!
    size: Int!
    number: String!
    roomType: RoomType!
    availability(
      fromDate: String!,
      toDate: String!,
      roomConfig: String!,
    ): Availability!
    availabilityPerDate(
      fromDate: String!,
      toDate: String!,
      roomConfig: String!,
    ): [Availability!]!
  }
`;

const resolvers = {
  Room: {
    id: (room) => room.id,
    size: (room) => room.size,
    number: (room) => room.room_number,
    roomType: async (room, args, context) => {
      const roomTypes = await context.model.RoomType.roomTypesByHotelId({
        hotelId: context.hotel.id,
      });
      return context.model.RoomType.findByName({
        roomTypes,
        roomTypeName: room.room_type.type,
      });
    },
    availability: (room, args, context) =>
      context.model.Availability.physicalAvailability({
        hotelId: context.hotel.id,
        fromDate: args.fromDate,
        toDate: args.toDate,
        roomConfig: args.roomConfig,
        roomTypeName: room.room_type.type,
      }),
    availabilityPerDate: (room, args, context) =>
      context.model.Availability.physicalAvailabilityPerDate({
        hotelId: context.hotel.id,
        fromDate: args.fromDate,
        toDate: args.toDate,
        roomConfig: args.roomConfig,
        roomTypeName: room.room_type.type,
      }),
  },
};

const mocks = {
  Room: () => ({
    id: () => casual.integer(0, 99),
    size: () => casual.integer(0, 2500),
    number: () => `${casual.integer(0, 999)}${casual.letter}`,
    availabilityPerDate: () => new MockList([1, 4]),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
