import deepMerge from 'cybertron-utils/deepMerge';
import Guest from './Guest';
import guestsByBookingId from './guestsByBookingId';
import guestById from './guestById';

const typeDefs = `
  ${Guest.typeDefs}
  ${guestsByBookingId.typeDefs}
  ${guestById.typeDefs}
`;

const resolvers = deepMerge(
  Guest.resolvers,
  guestsByBookingId.resolvers,
  guestById.resolvers,
);

const mocks = deepMerge(
  Guest.mocks,
  guestsByBookingId.mocks,
  guestById.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
};
