// import { MockList } from 'graphql-tools';

const typeDefs = `
  extend type Query {
    guestsByBookingId(
      bookingId: ID!
    ): [Guest!]!
  }
`;

const resolvers = {
  Query: {
    // guestsByBookingId: (parent, args) => {},
  },
};

const mocks = {
  // Query: () => ({
  //   guestsByBookingId: () => new MockList([1, 1]),
  // }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
