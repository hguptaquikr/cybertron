import casual from 'casual';

const typeDefs = `
  enum GenderEnum {
    MALE
    FEMALE
  }

  type Guest {
    id: ID!
    age: Int!
    gender: GenderEnum!
    name: String!
    phone: String!
    email: String!
  }
`;

const resolvers = {
  Guest: {},
};

const mocks = {
  Guest: () => ({
    id: casual.integer(1000, 9999),
    age: casual.integer(18, 80),
    gender: casual.random_element(['MALE', 'FEMALE']),
    name: casual.name,
    phone: casual.phone,
    email: casual.email,
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
