const typeDefs = `
  extend type Query {
    guestById(
      id: ID!
    ): Guest!
  }
`;

const resolvers = {
  Query: {
    // guestById: async (parent, args) => {},
  },
};

const mocks = {

};

export default {
  typeDefs,
  resolvers,
  mocks,
};
