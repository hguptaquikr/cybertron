import { MockList } from 'graphql-tools';
import { SevenBoom } from 'graphql-apollo-errors';

const typeDefs = `
  extend type Query {
    dnrs(
      hotelId: String!,
      fromDate: String!,
      toDate: String!,
      source: String,
    ): [Dnr]!
  }
`;

const resolvers = {
  Query: {
    dnrs: (parent, args) => {
      if (args.source === 'incorrect') {
        throw SevenBoom.badRequest('Sorry, incorrect source', args, 'dnrs:sourceIncorrect');
      }
      return Promise.resolve([{
        id: '1',
        hotel_id: args.hotelId,
        from_date: args.fromDate,
        to_date: args.toDate,
        room_no: '2',
        subtype: 'sm-subtype',
        type: 'HELLO',
        source: args.source,
        assigned_by: 'pragya',
        comments: 'kewl',
      }]);
    },
  },
};

const mocks = {
  Query: () => ({
    dnrs: () => new MockList([0, 4]),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
