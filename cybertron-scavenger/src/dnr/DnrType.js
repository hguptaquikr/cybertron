const typeDefs = `
  enum DnrType {
    HELLO
    WORLD
    HELP
    ME
  }
`;

export default {
  typeDefs,
};
