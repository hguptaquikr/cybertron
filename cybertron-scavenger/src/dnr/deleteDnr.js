const typeDefs = `
  extend type Mutation {
    deleteDnr(
      id: String!,
    ): Dnr!
  }
`;

const resolvers = {
  Mutation: {
    deleteDnr: (parent, args) =>
      Promise.resolve({
        id: args.id,
        hotel_id: '2',
        from_date: '2018-06-20',
        to_date: '2018-06-25',
        room_no: '2',
        subtype: 'sm-subtype',
        type: 'HELP',
        source: 'sm-source',
        assigned_by: 'pragya',
        comments: 'kewl',
      }),
  },
};

export default {
  typeDefs,
  resolvers,
};
