import casual from 'casual';

const typeDefs = `
  type Dnr {
    id: String
    hotelId: String
    fromDate: String
    toDate: String
    roomNo: String
    source: String
    type: DnrType
    subtype: String
    assignedBy: String
    comments: String
  }
`;

const resolvers = {
  Dnr: {
    id: (dnr) => dnr.id,
    hotelId: (dnr) => dnr.hotel_id,
    fromDate: (dnr) => dnr.from_date,
    toDate: (dnr) => dnr.to_date,
    roomNo: (dnr) => dnr.room_no,
    source: (dnr) => dnr.source,
    type: (dnr) => dnr.type,
    subtype: (dnr) => dnr.subtype,
    assignedBy: (dnr) => dnr.assigned_by,
    comments: (dnr) => dnr.comments,
  },
};

const mocks = {
  Dnr: () => ({
    id: () => casual.letter,
    hotelId: () => casual.word,
    fromDate: () => casual.word,
    toDate: () => casual.word,
    roomNo: () => casual.word,
    source: () => casual.word,
    type: () => 'HELLO',
    subtype: () => casual.word,
    assignedBy: () => casual.word,
    comments: () => casual.word,
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
