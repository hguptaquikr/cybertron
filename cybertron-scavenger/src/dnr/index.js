import deepMerge from 'cybertron-utils/deepMerge';
import Dnr from './Dnr';
import DnrType from './DnrType';
import dnrs from './dnrs';
import deleteDnr from './deleteDnr';

const typeDefs = `
  ${Dnr.typeDefs}
  ${DnrType.typeDefs}
  ${dnrs.typeDefs}
  ${deleteDnr.typeDefs}
`;

const resolvers = deepMerge(
  Dnr.resolvers,
  DnrType.resolvers,
  dnrs.resolvers,
  deleteDnr.resolvers,
);

const mocks = deepMerge(
  Dnr.mocks,
  DnrType.mocks,
  dnrs.mocks,
  deleteDnr.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
};
