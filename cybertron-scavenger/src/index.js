import 'babel-polyfill';
import express from 'express';
import helmet from 'helmet';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { ApolloEngine } from 'apollo-engine';
import { makeRequest } from 'cybertron-utils/request';
import logger from 'cybertron-utils/logger';
import { formatError } from './error';
import schema from './schema';
import makeConnector from './makeConnector';
import availability from './availability';
import booking from './booking';
import hotel from './hotel';
import price from './price';
import room from './room';
import roomType from './roomType';

const PORT = 8888;
const app = express();
const engine = new ApolloEngine({ apiKey: 'service:cybertron-scavenger:ZjWaMLOCrKMKbZU5tLkO9A' });

app.use(helmet({
  dnsPrefetchControl: false,
}));

app.use(express.json());

app.use('/graphql/', graphqlExpress(() => {
  const request = makeRequest();
  request.get = makeConnector();

  return {
    schema,
    formatError,
    context: {
      request,
      model: {
        Availability: availability.makeModel(request),
        Booking: booking.makeModel(request),
        Hotel: hotel.makeModel(request),
        Price: price.makeModel(request),
        Room: room.makeModel(request),
        RoomType: roomType.makeModel(request),
      },
    },
    tracing: true,
    cacheControl: true,
  };
}));

app.use('/graphiql/', graphiqlExpress({
  endpointURL: '/graphql/',
}));

engine.listen({
  port: PORT,
  expressApp: app,
  graphqlPaths: ['/graphql/'],
}, () => {
  logger.info(`cybertron-scavenger is running as local on port ${PORT}`);
});
