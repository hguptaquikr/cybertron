import casual from 'casual';

const typeDefs = `
  type PriceRatePlan {
    name: String!
    tag: String!
    preTax: Float!
    tax: Float!
    postTax: Float!
    preTaxBreakup: PriceRatePlanPreTaxBreakup!
  }
`;

const resolvers = {
  PriceRatePlan: {
    name: (ratePlan) => ratePlan.name,
    tag: (ratePlan) => ratePlan.meta.tag,
    preTax: (ratePlan) => ratePlan.prices.final_price.pre_tax,
    tax: (ratePlan) => ratePlan.prices.final_price.tax,
    postTax: (ratePlan) => ratePlan.prices.final_price.post_tax,
    preTaxBreakup: (ratePlan) => ratePlan.prices.final_price.pre_tax_computation,
  },
};

const mocks = {
  PriceRatePlan: () => ({
    name: () => casual.letter + casual.letter,
    tag: () => casual.word,
    preTax: () => casual.double(1, 3000),
    tax: () => casual.double(1, 3000),
    postTax: () => casual.double(1, 3000),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
