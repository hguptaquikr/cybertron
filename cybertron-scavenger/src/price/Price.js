import casual from 'casual';
import { MockList } from 'graphql-tools';

const typeDefs = `
  type Price {
    date: String!
    ratePlans: [PriceRatePlan!]!
  }
`;

const resolvers = {
  Price: {
    date: (price) => price.booking_unit.date,
    ratePlans: (price) => price.rate_plans,
  },
};

const mocks = {
  Price: () => ({
    date: () => casual.date(),
    ratePlans: () => new MockList([1, 4]),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
