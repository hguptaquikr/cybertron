import casual from 'casual';

const typeDefs = `
  type PriceRatePlanPreTaxBreakup {
    basePrice: Float!
    promotionDiscount: Float
    couponDiscount: Float
    memberDiscount: Float
    extraAdultCharge: Float
  }
`;

const resolvers = {
  PriceRatePlanPreTaxBreakup: {
    basePrice: (ratePlanPricePreTaxBreakup) => (ratePlanPricePreTaxBreakup.find((breakup) => breakup.name === 'base_price') || {}).value,
    promotionDiscount: (ratePlanPricePreTaxBreakup) => (ratePlanPricePreTaxBreakup.find((breakup) => breakup.name === 'promo_discount') || {}).value,
    couponDiscount: (ratePlanPricePreTaxBreakup) => (ratePlanPricePreTaxBreakup.find((breakup) => breakup.name === 'coupon_discount') || {}).value,
    memberDiscount: (ratePlanPricePreTaxBreakup) => (ratePlanPricePreTaxBreakup.find((breakup) => breakup.name === 'member_discount') || {}).value,
    extraAdultCharge: (ratePlanPricePreTaxBreakup) => (ratePlanPricePreTaxBreakup.find((breakup) => breakup.name === 'extra_adult_charge') || {}).value,
  },
};

const mocks = {
  PriceRatePlanPreTaxBreakup: () => ({
    basePrice: () => casual.double(1, 3000),
    promotionDiscount: () => casual.random_element([null, casual.double(1, 3000)]),
    couponDiscount: () => casual.random_element([null, casual.double(1, 3000)]),
    memberDiscount: () => casual.random_element([null, casual.double(1, 3000)]),
    extraAdultCharge: () => casual.random_element([null, casual.double(1, 3000)]),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
