const makeModel = (request) => {
  const pricePerDate = async ({
    hotelId,
    fromDate,
    toDate,
    roomConfig,
    roomTypeName,
  }) => {
    const pricePerDateResponse = await request.get('http://price-staging.treebohotels.com/v2/price/', {
      hotel_catalog_id: hotelId,
      stay_start: fromDate,
      stay_end: toDate,
      room_config: roomConfig,
    });

    if (roomTypeName) {
      const pricePerDateByRoomTypeResponse = pricePerDateResponse.data.filter((item) =>
        item.booking_unit.room_type_code.toUpperCase() === roomTypeName);
      return pricePerDateByRoomTypeResponse;
    }

    return pricePerDateResponse;
  };

  const price = async ({
    hotelId,
    fromDate,
    toDate,
    roomConfig,
    roomTypeName,
  }) => {
    const pricePerDateResponse = await pricePerDate({
      hotelId,
      fromDate,
      toDate,
      roomConfig,
      roomTypeName,
    });

    const ratePlanMap = {};
    pricePerDateResponse.forEach((priceForDate) => {
      priceForDate.rate_plans.forEach((ratePlan) => {
        ratePlanMap[ratePlan.name] = [
          ...ratePlanMap[ratePlan.name],
          ratePlan,
        ].filter(Boolean);
      });
    });

    const ratePlans = Object.values(ratePlanMap).map((ratePlansPerName) =>
      ratePlansPerName.reduce((acc, ratePlan) => ({
        ...acc,
        prices: {
          ...acc.prices,
          final_price: {
            post_tax: acc.prices.final_price.post_tax + ratePlan.prices.final_price.post_tax,
            pre_tax: acc.prices.final_price.pre_tax + ratePlan.prices.final_price.pre_tax,
            tax: acc.prices.final_price.tax + ratePlan.prices.final_price.tax,
            pre_tax_computation: acc.prices.final_price.pre_tax_computation
              .map((computationEntity) => ({
                ...computationEntity,
                value: computationEntity.value + ratePlan.prices.final_price.pre_tax_computation
                  .find((x) => x.name === computationEntity.name).value,
              })),
          },
        },
      })));

    const priceResponse = {
      booking_unit: {
        date: `${fromDate}|${toDate}`,
      },
      rate_plans: ratePlans,
    };

    return priceResponse;
  };

  return {
    pricePerDate,
    price,
  };
};

export default makeModel;
