import deepMerge from 'cybertron-utils/deepMerge';
import Price from './Price';
import PriceRatePlan from './PriceRatePlan';
import PriceRatePlanPreTaxBreakup from './PriceRatePlanPreTaxBreakup';
import makeModel from './makeModel';

const typeDefs = `
  ${Price.typeDefs}
  ${PriceRatePlan.typeDefs}
  ${PriceRatePlanPreTaxBreakup.typeDefs}
`;

const resolvers = deepMerge(
  Price.resolvers,
  PriceRatePlan.resolvers,
  PriceRatePlanPreTaxBreakup.resolvers,
);

const mocks = deepMerge(
  Price.mocks,
  PriceRatePlan.mocks,
  PriceRatePlanPreTaxBreakup.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
  makeModel,
};
