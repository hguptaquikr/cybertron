import GarphQLJSON from 'graphql-type-json';

const typeDefs = `
  scalar JSON
`;

const resolvers = {
  JSON: GarphQLJSON,
};

export default {
  typeDefs,
  resolvers,
};
