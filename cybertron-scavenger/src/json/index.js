import deepMerge from 'cybertron-utils/deepMerge';
import json from './json';

const typeDefs = `
  ${json.typeDefs}
`;

const resolvers = deepMerge(
  json.resolvers,
);

export default {
  typeDefs,
  resolvers,
};
