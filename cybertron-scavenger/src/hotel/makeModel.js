const makeModel = (request) => {
  const hotels = (query) =>
    request.get('http://staging.treebohotels.com/cataloging-service/api/v1/properties/', query);

  const hotelById = ({ hotelId }) =>
    request.get(`http://staging.treebohotels.com/cataloging-service/api/v1/properties/${hotelId}`);

  return {
    hotels,
    hotelById,
  };
};

export default makeModel;
