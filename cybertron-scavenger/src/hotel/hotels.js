// import { MockList } from 'graphql-tools';

const typeDefs = `
  input CoordinatesInput {
    latitude: Float!
    longitude: Float!
  }

  extend type Query {
    hotels(
      state: String,
      city: String,
      locality: String,
      landmark: String,
      coordinates: CoordinatesInput,
    ): [Hotel!]!
  }
`;

const resolvers = {
  Query: {
    hotels: (parent, args, context) => context.model.Hotel.hotels({}),
  },
};

const mocks = {
  // Query: () => ({
  //   hotels: () => new MockList([1, 1]),
  // }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
