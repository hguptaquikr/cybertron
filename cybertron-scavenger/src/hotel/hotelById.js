const typeDefs = `
  extend type Query {
    hotelById(
      id: ID!
    ): Hotel!
  }
`;

const resolvers = {
  Query: {
    hotelById: async (parent, args, context) => {
      // eslint-disable-next-line
      context.hotel = { id: args.id };
      return context.model.Hotel.hotelById({
        hotelId: args.id,
      });
    },
  },
};

export default {
  typeDefs,
  resolvers,
};
