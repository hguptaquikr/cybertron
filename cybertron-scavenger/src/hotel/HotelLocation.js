import casual from 'casual';

const typeDefs = `
  type State {
    id: ID!
    name: String!
    coordinates: Coordinates!
  }

  type City {
    id: ID!
    name: String!
    coordinates: Coordinates!
  }

  type Locality {
    id: ID!
    name: String!
    coordinates: Coordinates!
  }

  type MicroMarket {
    id: ID!
    name: String!
    coordinates: Coordinates!
  }

  type Coordinates {
    latitude: Float!
    longitude: Float!
  }

  type HotelLocation {
    state: State!
    city: City!
    locality: Locality!
    microMarket: MicroMarket!
    pincode: Int!
    address: String!
    coordinates: Coordinates!
    mapLink: String!
  }
`;

const resolvers = {
  State: {
    id: (state) => state.id,
    name: (state) => state.name,
    coordinates: (state) => state.coordinates,
  },
  City: {
    id: (city) => city.id,
    name: (city) => city.name,
    coordinates: (city) => city.coordinates,
  },
  Locality: {
    id: (locality) => locality.id,
    name: (locality) => locality.name,
    coordinates: (locality) => locality.coordinates,
  },
  MicroMarket: {
    id: (microMarket) => microMarket.id,
    name: (microMarket) => microMarket.name,
    coordinates: (microMarket) => microMarket.coordinates,
  },
  Coordinates: {
    latitude: (coordinates) => coordinates.latitude,
    longitude: (coordinates) => coordinates.longitude,
  },
  HotelLocation: {
    state: (hotelLocation) => hotelLocation.state,
    city: (hotelLocation) => hotelLocation.city,
    locality: (hotelLocation) => hotelLocation.locality,
    microMarket: (hotelLocation) => hotelLocation.micro_market,
    pincode: (hotelLocation) => hotelLocation.pincode,
    address: (hotelLocation) => hotelLocation.postal_address,
    coordinates: (hotelLocation) => ({
      latitude: hotelLocation.latitude,
      longitude: hotelLocation.longitude,
    }),
    mapLink: (hotelLocation) => hotelLocation.maps_link,
  },
};

const mocks = {
  State: () => ({
    id: () => casual.integer(1, 1000),
    name: () => casual.state,
  }),
  City: () => ({
    id: () => casual.integer(1, 1000),
    name: () => casual.city,
  }),
  Locality: () => ({
    id: () => casual.integer(1, 1000),
    name: () => casual.word,
  }),
  MicroMarket: () => ({
    id: () => casual.integer(1, 1000),
    name: () => casual.street,
  }),
  Coordinates: () => ({
    latitude: () => casual.latitude,
    longitude: () => casual.longitude,
  }),
  HotelLocation: () => ({
    pincode: () => casual.integer(100000, 999999),
    address: () => casual.address,
    mapLink: () => casual.url,
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
