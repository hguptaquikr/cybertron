import deepMerge from 'cybertron-utils/deepMerge';
import Hotel from './Hotel';
import HotelLocation from './HotelLocation';
import hotels from './hotels';
import hotelById from './hotelById';
import makeModel from './makeModel';

const typeDefs = `
  ${Hotel.typeDefs}
  ${HotelLocation.typeDefs}
  ${hotels.typeDefs}
  ${hotelById.typeDefs}
`;

const resolvers = deepMerge(
  Hotel.resolvers,
  HotelLocation.resolvers,
  hotels.resolvers,
  hotelById.resolvers,
);

const mocks = deepMerge(
  Hotel.mocks,
  HotelLocation.mocks,
  hotels.mocks,
  hotelById.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
  makeModel,
};
