import casual from 'casual';
import { MockList } from 'graphql-tools';

const typeDefs = `
  type Hotel {
    id: ID!
    name: String!
    status: String!
    location: HotelLocation!
    rooms: [Room!]!
    roomTypes: [RoomType!]!
    bookings: [Booking]!
  }
`;

const resolvers = {
  Hotel: {
    id: (hotel) => hotel.id,
    name: (hotel) => hotel.name.new_name,
    status: (hotel) => hotel.status,
    rooms: (hotel, args, context) =>
      context.model.Room.roomsByHotelId({
        hotelId: hotel.id,
      }),
    roomTypes: (hotel, args, context) =>
      context.model.RoomType.roomTypesByHotelId({
        hotelId: hotel.id,
      }),
    bookings: (hotel, args, context) =>
      context.model.Booking.bookingsByHotelId({
        hotelId: hotel.id,
      }),
  },
};

const mocks = {
  Hotel: () => ({
    id: () => casual.integer(1, 999),
    name: () => `Treebo ${casual.company_name}`,
    status: () => casual.random_element(['LIVE', 'CHURNED']),
    rooms: () => new MockList([1, 4]),
    roomTypes: () => new MockList([1, 4]),
    bookings: () => new MockList([1, 1]),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
