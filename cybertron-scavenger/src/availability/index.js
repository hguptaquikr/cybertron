import deepMerge from 'cybertron-utils/deepMerge';
import Availability from './Availability';
import makeModel from './makeModel';

const typeDefs = `
  ${Availability.typeDefs}
`;

const resolvers = deepMerge(
  Availability.resolvers,
);

const mocks = deepMerge(
  Availability.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
  makeModel,
};
