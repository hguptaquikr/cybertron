const makeModel = (request) => {
  const physicalAvailabilityPerDate = async ({
    // hotelId,
    fromDate,
    toDate,
    // roomConfig,
    // roomTypeName,
  }) => Promise.resolve([{
    date: `${fromDate}`,
    count: 2,
  }, {
    date: `${toDate}`,
    count: 1,
  }]);

  const physicalAvailability = async ({
    // hotelId,
    fromDate,
    toDate,
    // roomConfig,
    // roomTypeName,
  }) => Promise.resolve({
    date: `${fromDate}|${toDate}`,
    count: 2,
  });

  const throttledAvailabilityPerDate = async ({
    hotelId,
    fromDate,
    toDate,
    roomConfig,
    roomTypeName,
    channel,
    subChannel,
  }) => {
    const response = await request.get('http://rms-staging.treebo.com/its/api/v1/availability/room-night', {
      hotels: hotelId,
      checkin: fromDate,
      checkout: toDate,
      roomconfig: roomConfig,
      channel,
      subchannel: subChannel,
    });

    const throttledAvailabilityPerDateResponse = response.data[0].date_wise_availability
      .map((dateWiseAvailability) => ({
        date: dateWiseAvailability.date,
        count: dateWiseAvailability.room_config_wise_availability
          .find((roomConfigAvailability) =>
            roomConfigAvailability.room_config === roomConfig).room_type_availability
          .find((roomTypeAvailability) =>
            roomTypeAvailability.room_type === roomTypeName).availability,
      }));

    return throttledAvailabilityPerDateResponse;
  };

  const throttledAvailability = async ({
    hotelId,
    fromDate,
    toDate,
    roomConfig,
    roomTypeName,
    channel,
    subChannel,
  }) => {
    const response = await request.get('http://rms-staging.treebo.com/its/api/v1/availability/minimal-across-all-config', {
      hotels: hotelId,
      checkin: fromDate,
      checkout: toDate,
      roomconfig: roomConfig,
      channel,
      subchannel: subChannel,
    });

    const throttledAvailabilityResponse = {
      date: `${fromDate}|${toDate}`,
      count: response.data[0].availability.find((roomTypeAvailability) =>
        roomTypeAvailability.room_type === roomTypeName).availability,
    };

    return throttledAvailabilityResponse;
  };

  return {
    physicalAvailabilityPerDate,
    physicalAvailability,
    throttledAvailabilityPerDate,
    throttledAvailability,
  };
};

export default makeModel;
