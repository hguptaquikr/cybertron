import casual from 'casual';

const typeDefs = `
  type Availability {
    date: String!
    count: Int!
  }
`;

const resolvers = {
  Availability: {
    date: (availability) => availability.date,
    count: (availability) => availability.count,
  },
};

const mocks = {
  Availability: () => ({
    date: () => casual.date(),
    count: () => casual.integer(0, 20),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
