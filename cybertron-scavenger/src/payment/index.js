import deepMerge from 'cybertron-utils/deepMerge';
import Payment from './Payment';

const typeDefs = `
  ${Payment.typeDefs}
`;

const resolvers = deepMerge(
  Payment.resolvers,
);

const mocks = deepMerge(
  Payment.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
};
