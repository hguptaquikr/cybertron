import casual from 'casual';

const typeDefs = `
  type Payment {
    id: ID!
    amount: Float!
    paidBy: String!
    paidTo: String!
    channel: String!
    mode: String!
    details: JSON
    referenceId: String!
    type: String!
    status: String!
  }
`;

const resolvers = {
  Payment: {
    id: (payment) => payment.payment_id,
    amount: (payment) => payment.amount,
    paidBy: (payment) => payment.paid_by,
    paidTo: (payment) => payment.paid_to,
    channel: (payment) => payment.payment_channel,
    mode: (payment) => payment.payment_mode,
    details: (payment) => payment.payment_details,
    referenceId: (payment) => payment.payment_ref_id,
    type: (payment) => payment.payment_type,
    status: (payment) => payment.status,
  },
};

const mocks = {
  Payment: () => ({
    id: () => casual.integer(1, 99999),
    amount: () => casual.double(2000, 5000),
    paidBy: () => casual.word,
    paidTo: () => casual.word,
    channel: () => casual.word,
    mode: () => casual.word,
    details: () => casual.string,
    referenceId: () => casual.integer(1, 99999),
    type: () => casual.word,
    status: () => casual.word,
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
