const makeModel = (request) => {
  const roomTypesByHotelId = ({ hotelId }) =>
    request.get(`http://staging.treebohotels.com/cataloging-service/api/v1/properties/${hotelId}/room-type-configs/`);

  const findByName = ({
    roomTypes,
    roomTypeName,
  }) => roomTypes.find((roomType) => roomType.room_type.type === roomTypeName);

  return {
    roomTypesByHotelId,
    findByName,
  };
};

export default makeModel;
