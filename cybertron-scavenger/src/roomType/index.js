import deepMerge from 'cybertron-utils/deepMerge';
import RoomType from './RoomType';
import RoomTypeMaxOccupancy from './RoomTypeMaxOccupancy';
import RoomTypeNameEnum from './RoomTypeNameEnum';
import makeModel from './makeModel';

const typeDefs = `
  ${RoomType.typeDefs}
  ${RoomTypeMaxOccupancy.typeDefs}
  ${RoomTypeNameEnum.typeDefs}
`;

const resolvers = deepMerge(
  RoomType.resolvers,
  RoomTypeMaxOccupancy.resolvers,
  RoomTypeNameEnum.resolvers,
);

const mocks = deepMerge(
  RoomType.mocks,
  RoomTypeMaxOccupancy.mocks,
  RoomTypeNameEnum.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
  makeModel,
};
