import casual from 'casual';
import { MockList } from 'graphql-tools';

const typeDefs = `
  type RoomType {
    id: ID!
    name: RoomTypeNameEnum!
    minSize: Int!
    maxOccupancy: RoomTypeMaxOccupancy!
    rooms: [Room!]!
    price(
      fromDate: String!,
      toDate: String!,
      roomConfig: String!,
    ): Price!
    pricePerDate(
      fromDate: String!,
      toDate: String!,
      roomConfig: String!,
    ): [Price!]!
    availability(
      fromDate: String!,
      toDate: String!,
      roomConfig: String!,
      channel: String!,
      subChannel: String!,
    ): Availability!
    availabilityPerDate(
      fromDate: String!,
      toDate: String!,
      roomConfig: String!,
      channel: String!,
      subChannel: String!,
    ): [Availability!]!
  }
`;

const resolvers = {
  RoomType: {
    id: (roomType) => roomType.id,
    name: (roomType) => roomType.room_type.type,
    minSize: (roomType) => roomType.min_room_size,
    maxOccupancy: (roomType) => ({
      adults: roomType.adults,
      children: roomType.children,
    }),
    rooms: async (roomType, args, context) => {
      const rooms = await context.model.Room.roomsByHotelId({
        hotelId: context.hotel.id,
      });
      return context.model.Room.filterByRoomTypeName({
        rooms,
        roomTypeName: roomType.room_type.type,
      });
    },
    price: (roomType, args, context) =>
      context.model.Price.price({
        hotelId: context.hotel.id,
        fromDate: args.fromDate,
        toDate: args.toDate,
        roomConfig: args.roomConfig,
        roomTypeName: roomType.room_type.type,
      }),
    pricePerDate: (roomType, args, context) =>
      context.model.Price.pricePerDate({
        hotelId: context.hotel.id,
        fromDate: args.fromDate,
        toDate: args.toDate,
        roomConfig: args.roomConfig,
        roomTypeName: roomType.room_type.type,
      }),
    availability: (roomType, args, context) =>
      context.model.Availability.throttledAvailability({
        hotelId: context.hotel.id,
        fromDate: args.fromDate,
        toDate: args.toDate,
        roomConfig: args.roomConfig,
        roomTypeName: roomType.room_type.type,
        channel: args.channel,
        subChannel: args.subChannel,
      }),
    availabilityPerDate: (roomType, args, context) =>
      context.model.Availability.throttledAvailabilityPerDate({
        hotelId: context.hotel.id,
        fromDate: args.fromDate,
        toDate: args.toDate,
        roomConfig: args.roomConfig,
        roomTypeName: roomType.room_type.type,
        channel: args.channel,
        subChannel: args.subChannel,
      }),
  },
};

const mocks = {
  RoomType: () => ({
    id: () => casual.integer(0, 99),
    name: () => casual.random_element(['ACACIA', 'OAK', 'MAPLE', 'MAHOGANY']),
    minSize: () => casual.integer(0, 2500),
    pricePerDate: () => new MockList([1, 4]),
    availabilityPerDate: () => new MockList([1, 4]),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
