import casual from 'casual';

const typeDefs = `
  type RoomTypeMaxOccupancy {
    adults: Int!
    children: Int!
  }
`;

const resolvers = {
  RoomTypeMaxOccupancy: {
    adults: (maxOccupancy) => maxOccupancy.adults,
    children: (maxOccupancy) => maxOccupancy.children,
  },
};

const mocks = {
  RoomTypeMaxOccupancy: () => ({
    adults: () => casual.integer(1, 8),
    children: () => casual.integer(1, 4),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
