const typeDefs = `
  enum RoomTypeNameEnum {
    ACACIA
    OAK
    MAPLE
    MAHOGANY
  }
`;

export default {
  typeDefs,
};
