import deepMerge from 'cybertron-utils/deepMerge';
import RoomStay from './RoomStay';
// import roomStays from './roomStays';

const typeDefs = `
  ${RoomStay.typeDefs}
`;
// ${roomStays.typeDefs}

const resolvers = deepMerge(
  RoomStay.resolvers,
  // roomStays.resolvers,
);

const mocks = deepMerge(
  RoomStay.mocks,
  // roomStays.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
};
