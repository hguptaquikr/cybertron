import casual from 'casual';

const typeDefs = `
  type RoomStay {
    id: ID!
    actualCheckIn: String!
    actualCheckOut: String!
    checkIn: String!
    checkOut: String!
    charges: [Charge]!
    booking: Booking!
    roomAllocation: RoomAllocation
    roomAllocationHistory: [RoomAllocation]!
  }
`;

const resolvers = {
  RoomStay: {
    id: (roomStay) => roomStay.id,
    actualCheckIn: (roomStay) => roomStay.actual_checkin_date,
    actualCheckOut: (roomStay) => roomStay.actual_checkout_date,
    checkIn: (roomStay) => roomStay.checkin_date,
    checkOut: (roomStay) => roomStay.checkout_date,
    roomAllocation: (roomStay) => roomStay.room_allocation,
    roomAllocationHistory: (roomStay) => roomStay.room_allocation_history,
  },
};

const mocks = {
  RoomStay: () => ({
    id: () => casual.integer(1, 99999),
    actualCheckIn: () => casual.date('YYYY-MM-DD'),
    actualCheckOut: () => casual.date('YYYY-MM-DD'),
    checkIn: () => casual.date('YYYY-MM-DD'),
    checkOut: () => casual.date('YYYY-MM-DD'),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
