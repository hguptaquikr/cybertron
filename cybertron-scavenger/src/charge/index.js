import deepMerge from 'cybertron-utils/deepMerge';
import Charge from './Charge';
import ChargeItem from './ChargeItem';
import ChargeSplit from './ChargeSplit';

const typeDefs = `
  ${Charge.typeDefs}
  ${ChargeItem.typeDefs}
  ${ChargeSplit.typeDefs}
`;

const resolvers = deepMerge(
  Charge.resolvers,
  ChargeItem.resolvers,
  ChargeSplit.resolvers,
);

const mocks = deepMerge(
  Charge.mocks,
  ChargeItem.mocks,
  ChargeSplit.mocks,
);

export default {
  typeDefs,
  resolvers,
  mocks,
};
