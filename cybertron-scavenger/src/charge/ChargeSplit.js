import casual from 'casual';

const typeDefs = `
  type ChargeSplit {
    amount: Float!
    chargeTo: String!
  }
`;

const resolvers = {
  ChargeSplit: {
    amount: (chargeSplit) => chargeSplit.amount,
    chargeTo: (chargeSplit) => chargeSplit.charge_to,
  },
};

const mocks = {
  ChargeSplit: () => ({
    amount: () => casual.double(2000, 4000),
    chargeTo: () => casual.word,
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
