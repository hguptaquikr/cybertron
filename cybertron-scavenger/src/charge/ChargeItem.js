import casual from 'casual';

const typeDefs = `
  type ChargeItem {
    name: String!
    skuCategoryId: ID!
    details: JSON
  }
`;

const resolvers = {
  ChargeItem: {
    name: (chargeItem) => chargeItem.name,
    skuCategoryId: (chargeItem) => chargeItem.sku_category_id,
    details: (chargeItem) => chargeItem.details,
  },
};

const mocks = {
  ChargeItem: () => ({
    name: () => casual.word,
    skuCategoryId: () => casual.integer(1, 99999),
    details: () => ({ something: casual.string }),
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
