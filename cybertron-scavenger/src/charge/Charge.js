import casual from 'casual';
import { MockList } from 'graphql-tools';

const typeDefs = `
  type Charge {
    id: ID!
    applicableDate: String!
    billToType: String!
    chargeSplits: [ChargeSplit!]!
    comments: String!
    creationChannel: String!
    isRefundable: Boolean!
    item: ChargeItem!
    postTax: Float!
    preTax: Float!
    status: String!
    type: String!
  }
`;

const resolvers = {
  Charge: {
    id: (charge) => charge.charge_id,
    applicableDate: (charge) => charge.applicable_date,
    billToType: (charge) => charge.bill_to_type,
    chargeSplits: (charge) => charge.charge_splits,
    comments: (charge) => charge.comments,
    creationChannel: (charge) => charge.creationChannel,
    isRefundable: (charge) => charge.is_refundable,
    item: (charge) => charge.item,
    postTax: (charge) => charge.posttax_amount,
    preTax: (charge) => charge.pretax_amount,
    status: (charge) => charge.status,
    type: (charge) => charge.type,
  },
};

const mocks = {
  Charge: () => ({
    id: () => casual.integer(1, 99999),
    applicableDate: () => casual.date('YYYY-MM-DD'),
    billToType: () => casual.word,
    chargeSplits: () => new MockList(1, 3),
    comments: () => casual.sentences(2),
    creationChannel: () => casual.word,
    isRefundable: () => casual.coin_flip,
    item: () => new MockList(1, 1),
    postTax: () => casual.double(2000, 5000),
    preTax: () => casual.double(2500, 5500),
    status: () => casual.word,
    type: () => casual.word,
  }),
};

export default {
  typeDefs,
  resolvers,
  mocks,
};
