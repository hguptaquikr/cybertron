import Dataloader from 'dataloader';
import queryString from 'query-string';
import request from 'cybertron-utils/request';

const makeConnector = (opts) => {
  const dataLoader = new Dataloader((urls) => {
    const requestGetPromises = urls.map((url) => {
      const options = {
        method: 'GET',
        ...opts,
      };
      return request(url, null, options);
    });
    return Promise.all(requestGetPromises);
  }, {
    batch: false,
  });

  return (url, data) => {
    const query = queryString.stringify(data, { arrayFormat: 'index' });
    return dataLoader.load(`${url}?${query}`);
  };
};

export default makeConnector;
