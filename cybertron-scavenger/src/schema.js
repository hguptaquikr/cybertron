import { makeExecutableSchema, addMockFunctionsToSchema, MockList } from 'graphql-tools';
import deepMerge from 'cybertron-utils/deepMerge';
import availability from './availability';
import bill from './bill';
import booking from './booking';
import charge from './charge';
import dnr from './dnr';
import guest from './guest';
// import guestAllocation from './guestAllocation';
import hotel from './hotel';
import invoice from './invoice';
import json from './json';
import payment from './payment';
import price from './price';
import room from './room';
import roomType from './roomType';
import roomStay from './roomStay';
import roomAllocation from './roomAllocation';

const typeDefs = `
  type Query
  type Mutation
  ${availability.typeDefs}
  ${bill.typeDefs}
  ${booking.typeDefs}
  ${charge.typeDefs}
  ${dnr.typeDefs}
  ${guest.typeDefs}
  ${hotel.typeDefs}
  ${invoice.typeDefs}
  ${json.typeDefs}
  ${payment.typeDefs}
  ${price.typeDefs}
  ${room.typeDefs}
  ${roomType.typeDefs}
  ${roomStay.typeDefs}
  ${roomAllocation.typeDefs}
`;

const resolvers = deepMerge(
  availability.resolvers,
  bill.resolvers,
  booking.resolvers,
  charge.resolvers,
  dnr.resolvers,
  guest.resolvers,
  hotel.resolvers,
  invoice.resolvers,
  json.resolvers,
  payment.resolvers,
  price.resolvers,
  room.resolvers,
  roomType.resolvers,
  roomStay.resolvers,
  roomAllocation.resolvers,
);

const mocks = deepMerge(
  availability.mocks,
  bill.mocks,
  booking.mocks,
  charge.mocks,
  dnr.mocks,
  guest.mocks,
  hotel.mocks,
  invoice.mocks,
  json.mocks,
  payment.mocks,
  price.mocks,
  room.mocks,
  roomType.mocks,
  roomStay.mocks,
  roomAllocation.mocks,
  {
    Query: () => ({
      bookings: () => new MockList([1, 5]),
      hotels: () => new MockList([1, 1]),
      guestsByBookingId: () => new MockList([1, 3]),
    }),
  },
);

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

addMockFunctionsToSchema({ schema, mocks });

export default schema;
